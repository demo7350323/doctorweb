FROM openjdk:8-jdk-alpine
ARG WAR_FILE=target/smDoctorWeb.war
WORKDIR /opt/git_projects/doctorwebsamvita
COPY ${WAR_FILE} smDoctorWeb.war
EXPOSE 8085
ENTRYPOINT [ "java", "-jar", "smDoctorWeb.war" ]

#code rabbit AI check