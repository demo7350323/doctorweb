<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Cache-Control", "no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);
 %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Patient List</title>
       <link rel="shortcut icon" href="resources/fonts/fontawesome-webfont.ttf">
        <link rel="apple-touch-icon" href="resources/fonts/fontawesome-webfont.woff">
        <link rel="shortcut icon" href="resources/fonts/fontawesome-webfont.woff2">
        
        <link rel="stylesheet" href="resources/css/bootstrap.min.css" />

<link href="resources/css/AdminLTE.css" rel="stylesheet" />
<link href="resources/css/_all-skins.min.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/font-awesome.min.css" />
<link rel="stylesheet" href="resources/css/material.css">
<link href="resources/css/toastr.min.css" rel="stylesheet">
        
<!--         <link type="text/css" rel="stylesheet" href="resources/css/bootstrap.min.css">
        <link href="resources/css/toastr.min.css" rel="stylesheet">
        <link rel="stylesheet" href="resources/css/AdminLTE.css">
       
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
           
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="resources/css/_all-skins.min.css">
<link rel="stylesheet" href="resources/script/material.min.css"> -->
 <link rel="stylesheet" href="resources/css/dataTables.bootstrap.css">
<link rel="stylesheet" href="resources/css/jquery.dataTables_themeroller.css">

        <script src="resources/js/jQuery-2.1.4.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/toastr.min.js"></script>
        <script src="resources/js/customHTML.js"></script>
        <script src="resources/js/bootstrap3-typeahead.js" type="application/javascript"></script>
        
        <script src="resources/js/material.js"></script>
        <script src="resources/js/material.min.js"></script> 
          <script src="resources/js/jquery.dataTables.min.js"></script>
    <script src="resources/js/dataTables.bootstrap.min.js"></script>
   
   
   <style>

    .navbar-t-centered {
      position: absolute;
left: 34%;
display: block;
text-align: center;
color: #fff;
top: 0px;
padding: 15px;
font-weight: 400;
font-size: 12px;

    }
    @media screen and (min-width:768px){
.navbar-t-centered {
        position: absolute;
        left: 38% ;
        display: block;
       /*  width: 160px; */
        text-align: center;
        color: #fff;
        top:0px;
        padding:15px;
        font-weight: 400;
        font-size:22px;

    }
    }
</style>     
<!--<div id="listTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
<div class="row">
<div class="col-sm-6"></div>
<div class="col-sm-6"><div id="listTable_filter" class="dataTables_filter">
<label>Search:
<input type="search" class="form-control input-sm" placeholder="" aria-controls="listTable"></label>
</div></div></div>  -->
    </head>
    <body class="hold-transition skin-blue sidebar-mini" >
        <div class="wrapper">
            <header class="main-header">
      <nav class="navbar navbar-static-top" role="navigation"
		style="margin-left:0% !important ;max-height:50px; background-color:#3c8dbc">
	<!-- Sidebar toggle button--> <img src="resources/images/logowhite.png"
		class="user-image" alt="User Image"
		style="height: 50px; padding-left: 5px; margin-left: 5px;">

	
		<span class ="navbar-t-centered" >Outreach Healthcare System</span>
	
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->

              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="resources/images/avatar.png" class="user-image" alt="User Image">
                  <span class="hidden-xs" id="loggedInUserFullName"></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header" style="height: 138px !important;">
                    <img src="resources/images/avatar.png" class="img-circle" alt="User Image">
                    <p><span id="loggedInUserFullNameSpan"></span>
                    </p>
                  </li>
                  <!-- Menu Body -->
                 <li class="user-body">
                 
                 <ul class="col-xs-12 text-center" id="activeUsersId">
                 
                 </ul>
                 
                 
                 
                 <!--  <div class="col-xs-7 text-center"><span style="font-size: 12px; margin-left: -33px;" id="nurseNameForActiveId"></span>
                                  <a id= "videoIconId" style="border-radius: 50%; ;margin-left: 5px;background-color: green" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                  				   <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a>
                                  </div> 
                                  <div class="col-xs-5 text-center">
                                    <a href="patientList">Patient List</a>
                                  </div> -->
                                </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                    </div>
                    <div class="pull-right">
                      <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             
            </ul>
          </div>
        </nav>
      </header>
         <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="margin-left: 0px !important;">
       
        <!-- Main content -->
        <section class="content">

          <div class="row">
            
            <div class="col-md-12" >
              <div class="nav-tabs-custom" style="min-height: 535px !important;">
                <ul class="nav nav-tabs">
                  <li class="active"><a class="highLId" id="patConId" href="#history" onclick="populateOrders()" data-toggle="tab">Patient Queue</a></li>
                  <li class="" onclick="viewMyList();"><a class="highLId" id="docConId" href="#myList" data-toggle="tab">My Consultation List</a></li>
                </ul>
               <div class="tab-content" style="overflow-y:auto;min-height:500px;">
                  <div class="active tab-pane" id="history">
                   <!-- <div class="box box-default"> -->
                 
              <div class="box-body">
                  <table class="table table-bordered table-striped" id="orderTable" >
                    <thead>
                    <tr style="height: 33px !important;">
                      <th style="text-align:center;">#</th>
                      <th style="text-align:center;">UHID</th>
                      <th style="text-align:center;">Patient Name</th>
                      <th style="text-align:center;width:50px">Age/Gender</th>
                      <th style="text-align:center;">Location/City</th>
                       
                       <th style="text-align:center;">Select Patient</th>
                       <th style="text-align:center;">Registration Date | Waiting Time</th>
                      </tr>
                      </thead>
                      <tbody id="orderDetails" ></tbody>
                  </table>
                  
                  <div class="col-md-12 text-center" id="orderDetailsNoList" style="display:none;padding-left: 0px;padding-right: 0px;">
                  <p style="padding-top: 10px;padding-bottom: 10px;border: 1px solid #f4f4f4;">No Patients</p>
                  </div>
                </div><!-- /.box-body -->
                  </div><!-- /.tab-pane -->

        
      <!--   <div class="modal tab-content " 
            role="dialog" style="padding:15px;margin-top: 50px;z-index: 1025;"> -->
           <div class="tab-pane" id="myList">
             <div class="box-body">
                  <table class="table table-bordered table-striped" id="listTable" >
                    <thead>
                    <tr style="height: 33px !important;">
                      <th style="text-align:center;width:9px;">#</th>
                      <th style="text-align:center;">UHID</th>
                      <th style="text-align:center;width:100px;">Patient Name</th>
                      <th style="text-align:center;">Age/Gender</th>
                      <th style="text-align:center;width:100px;">Location/City</th>
                      <th style="text-align:center;width:100px;">Start Time</th>
                       <th style="text-align:center;width:100px;">Elapsed Time</th>
                       <th style="text-align:center;width:100px;">De-Select</th>
                      </tr>
                      </thead>
                      <tbody id="orderConList" >
                      
                      
                      
                      </tbody>
                  </table>
                  <div class="col-md-12 text-center" id="orderNoList" style="display:none;padding-left: 0px;padding-right: 0px;">
                  <p style="padding-top: 10px;padding-bottom: 10px;border: 1px solid #f4f4f4;">No Patients</p>
                  </div>
                  
                </div><!-- /.box-body -->
            </div>
        </div>
                <!-- </div> --><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer col-md-12" style="margin-left:0% !important;bottom:0;position:fixed;">
        
          <div class="pull-right hidden-xs">
          <b>Version</b> 1.1.0
        </div>
        Copyright &copy; 2018-2019 <a href="http://wrizto.com/" style="text-decoration: none;color: #23527c">Wrizto Healthcare Pvt. Ltd.</a> All rights reserved.
      </footer>

      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    
    
    
    
    
    
    <div id="itemMasterModal" style="position: absolute; background: white; padding-top: 0px; padding-bottom: 8px;font-style:normal;
					overflow-y:hidden;"
					class="modal modal-fullscreen fade modal-open" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog" style="width:100%;margin-top: 0px; padding-right: 0px;">

							<!-- Modal content-->
							<div class="" style="height:100%">

								<div class="modal-header" id="headerChng"
									style="background-color:#3c8dbc;postion:fixed;width:100%; color: white; padding-left: 0px; margin: 0px;
									padding-top: 20px; height: 65px;z-index:10000;">

									<b class="modal-title"
										style="height: 24px; width: 100%; font-size: 20px;"> <i
										class="fa fa-arrow-left" aria-hidden="true" id="itemsMasBack"
										 title="Save" onclick="closeitemOpenMasterModal();"
										style="cursor: pointer; float: left; color: white; font-size: 20px; margin-top: 2px; padding-left: 17px;"></i>
										<b id="newUpddateId" style="color: white; padding-left: 35px;"> New
											Inventory</b><!--  onclick="closeitemOpenMasterModal();" -->

									</b>
								</div>

								<div class="modal-body" id="itemMaster"
									style="background: white;height:90%;overflow:auto;">



									<!----------------------------------------------------pabitra ------------------------------------------------------------->


									<form class="form-inline" role="form" id="itemMasterForm"
										name="" method="post" style="overflow:auto;height:">
										<input type="hidden" name="selectedInventoryStatus" id="inventoryStatus" value="" >
										<input type="hidden" name="userGroupId" value="NORM" class="clr">
										<input type="hidden" name="medicationId" id="medicationIdInvMas" class="clr">
										<input type="hidden" name="productId" id="productIds" class="clr">
										<input type="hidden" name="userId" id="userIds" class="clr"> <input
											type="hidden" name="manfId" id="manufacturerIdForMaster">
										<input type="hidden" name="inventoryGenericDrugId" value="-1"
											id="inventoryGenericDrugId">
											<input type="hidden" name="rackId" id="rackIdHidden" value="-1">
											<input type="hidden" name="binId" id="binIdHidden" value="-1">
										<div>
											<div class=""
												style="height: 100%; border-width: 0px 0px 0px; border-style: solid; border-color: rgb(217, 217, 217); -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; border-image: none;">
												<div class="form-group col-xs-12"
													style="padding-bottom: 15px; padding-top: 0px; margin-bottom: 0px;">
<!---------------------------------------------------------------------- Harshitha----------------------------------------------------------------------------->
<!--Add from excel-->
<!-- <div>
<span class="btn btn-info pull-right" style="width: 150px; height: 35px;background:#0db6c4" id="impExclId"onclick="importExcel();">Import from Excel</span>
</div>  -->

													<div class="col-xs-6" style="padding-top:0px">
														<div class="col-xs-12"
															style="padding-left: 0px; padding-right: 0px;">
															<div
																class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
																style="width: 100%; color: black">
																<input autocomplete="off" autocorrect="off" spellcheck="false"
																	class="mdl-textfield__input error-popover clearAll"
																	autocomplete="off" type="text" id="mandName" name="medicationName"
																	maxlength="100" style="text-transform: capitalize;">
																	<label class="mdl-textfield__label" for="sample3">Trade Name
																		<span style="color:red;">*</span>
																	</label>

															</div>
														</div>
													</div>
													 <div class="col-xs-6" style="display:none">
														<div class="col-xs-12"
															style="padding-left: 0px; padding-right: 0px;">
															<div
																class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
																style="width: 100%; color: black">
																<input autocomplete="off" autocorrect="off" spellcheck="false"
																	class="mdl-textfield__input error-popover clearAll makeDis CA"
																	type="text" id="strength" name="dosage" maxlength="10">
																<label class="mdl-textfield__label" for="sample3">Strength</label>

															</div>
														</div>
													</div> 
												 <div class="col-xs-6" style="display:none">
														<div class="col-xs-12"
															style="padding-left: 0px; padding-right: 0px;">
															<div
																class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
																style="width: 100%; color: black">
																<input autocomplete="off" autocorrect="off" spellcheck="false"
																	class="mdl-textfield__input error-popover clearAll makeDis CA"
																	autocomplete="off" type="text" id="genericDrug"
																	name="genericDrugName" maxlength="100"
																	style="text-transform: capitalize;"> <label
																	class="mdl-textfield__label" for="sample3">Generic
																	Drug</label>

															</div>
														</div>
													</div> 



												 <div class="col-xs-6 " style="display:none">
														<div class="col-xs-12"		style="padding-left: 0px; padding-right: 0px;">
															<p for="InputName" style="">Drug Type</p>
															<div class="dropdown">
																<select name="storesItemTypeId" id="classificationId"
																	style="height: 40px; width: 100%;">
																</select>
																<div id="validateDrugType" style="display: none;"></div>
																<p id="mandatoryCategory" style="display: none; color: red">Select
																	category</p>
															</div>
														</div>
														<input type="hidden" name="storesItemTypeId"
															id="storeids" value="-1" autocomplete="off" autocorrect="off" spellcheck="false">
														<div
															class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
															style="width: 100%">

<select name="" id="h_storesItemTypeId" style="width: 100%; background: white none repeat scroll 0% 0%; border-width: 0px 0px 1px;
													 border-style: solid; border-color: lightgrey; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none;
													 -moz-border-left-colors: none; border-image: none; padding-bottom: 6px;" id=""></select>
								
															<input onchange="getBackFocus('drug')" class="mdl-textfield__input makeDis CA" type="text" autocomplete="off" autocorrect="off" spellcheck="false"
																dat-at="-1" id="h_storesItemTypeId"
																value="Select Drug Type" readonly tabIndex="-1">
															<label for="h_storesItemTypeId"
																class="mdl-textfield__label ">Drug Type</label>
															<ul for="h_storesItemTypeId" id="classificationId"
																class="mdl-menu mdl-menu--bottom-left mdl-js-menu"
																style="width: 100%">


															</ul>
														</div>



													</div> 


										 <div class="col-xs-6" style="display:none">
											<div class="col-xs-12"
												style="padding-left: 0px; padding-right: 0px;">
												<div
													class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
													style="width: 100%; color: black">
													<input class="mdl-textfield__input error-popover clearAll makeDis CA"
														type="text" id="composition" name="composition" autocomplete="off" autocorrect="off"
														spellcheck="false" maxlength="100"
														style="text-transform: capitalize;"> <label
														class="mdl-textfield__label" for="sample3">Composition</label>

												</div>
											</div>
										</div> 
										<div class="col-xs-6" style="">
											<div class="col-xs-12"
												style="padding-left: 0px; padding-right: 0px;font-style:normal">
												<div
													class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
													style="width: 100%; color: black">
													<input class="mdl-textfield__input error-popover clearAll makeDis CA"
														autocomplete="off" type="text" autocomplete="off" autocorrect="off" spellcheck="false"
														id="manufacturerIdItemMaster" name="manufacturer"
														style="text-transform: uppercase;" maxlength="80"> <label
														class="mdl-textfield__label" for="sample3">Manufacturer<i
														style="color: red">*</i></label>

												</div>
											</div>
										</div>
										<div class="col-xs-6" style="">
											<div class="col-xs-12"
												style="padding-left: 0px; padding-right: 0px;font-style:normal">
												<div
													class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
													style="width: 100%; color: black">
													<input class="mdl-textfield__input error-popover clearAll makeDis CA"
														type="text" id="manufacturerCodeItemMaster" autocomplete="off" autocorrect="off"
														spellcheck="false" style="text-transform: uppercase;"name="manfCode" maxlength="20"> <label
														class="mdl-textfield__label" for="sample3">Manufacturer
														Code<i style="color: red">*</i>
													</label>

												</div>
											</div>
										</div>
									 <div class="col-xs-6 " style="display:none">
											<div class="col-xs-12"
												style="padding-left: 0px; padding-right: 0px;font-style:normal">

												<input type="hidden" name="medicationType" id="medicationTypeId" value="-1">
												<div
													class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
													style="width: 100%">
													
													<select name="medicationType" id="schedule" style="width: 100%; background: white none repeat scroll 0% 0%; border-width: 0px 0px 1px;
													 border-style: solid; border-color: lightgrey; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none;
													 -moz-border-left-colors: none; border-image: none; padding-bottom: 6px;" id="addVendorDet"></select>
													<input class="mdl-textfield__input makeDis" type="text" dat-at="-1" onchange="getBackFocus('sch')"
														id="h_schedule" value="Select Schedule" readonly autocomplete="off" autocorrect="off"
														spellcheck="false"
														tabIndex="-1" style="color: black;"> <label
														for="h_schedule" class="mdl-textfield__label">Schedule <i style="color:red;font-weight: bold;">*</i></label>
														<span style=" color: red;display: block; margin-top: -6%; margin-left: 7%;">*</span>
													<ul for="h_schedule" id="schedule"
														class="mdl-menu mdl-menu--bottom-left mdl-js-menu"
														style="width: 100%">
													</ul>
												</div>
											</div>
										</div> 
										<div class="col-xs-6" >
											<div class="col-xs-12"
												style="padding-left: 0px; padding-right: 0px;font-style:normal">
												<div id="thresholdDiv" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
													style="width: 100%; color: black">
													<input class="mdl-textfield__input error-popover clearAll CA"
														type="tel" id="threshold" name="threshold"  autocomplete="off" autocorrect="off"
														spellcheck="false" maxlength="4" value="100" readonly>
														<label class="mdl-textfield__label" for="sample3" >Threshold<i style="color: red">*</i></label>

												</div>
											</div>
										</div>

										<div class="col-xs-6" style="display:none">
											<div class="col-xs-12"
												style="padding-left: 0px; padding-right: 0px;font-style:normal">
												<div id="packageDiv" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
													style="width: 100%; color: black">
													<input class="mdl-textfield__input error-popover clearAll CA"
														type="tel" id="packaging" name="packaging" autocomplete="off" autocorrect="off" spellcheck="false"
														maxlength="4"> <label
														class="mdl-textfield__label" for="sample3">Package</label>

												</div>
											</div>
										</div>
											<div class="col-xs-6" id="">
												<div style="width: 100%; color: black" class="mdl-textfield mdl-js-textfield
													mdl-textfield--floating-label is-dirty is-upgraded" data-upgraded=",MaterialTextfield">
													<select name="vatPercentage" id="invVatPerc" type="text" style="padding-right: 0px;"
														class="mdl-textfield__input error-popover"
														attr="1" onchange="">
															<option value="0">0.00</option>
															<option value="5.00">5.00</option>
															<option value="12.00">12.00</option>
															<option value="18.00">18.00</option>
															<option value="28.00">28.00</option>
													</select>
													<label for="sample3" class="mdl-textfield__label">GST %</label>
												</div>
											</div>
										<div class="col-xs-4" style="display:none">
											<div class="col-xs-12"
												style="padding-left: 0px; padding-right: 0px;font-style:normal">
												<div id="" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
													style="width: 100%; color: black">
													<input class="mdl-textfield__input error-popover clearAll CA"
														type="text" id="rackID" name="rackName" autocomplete="off" autocorrect="off" spellcheck="false"
														> <label
														class="mdl-textfield__label" for="sample3">Rack</label>

												</div>
											</div>
										</div>
											<div class="col-xs-4" style="display:none">
											<div class="col-xs-12"
												style="padding-left: 0px; padding-right: 0px;font-style:normal">
												<div id="" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
													style="width: 100%; color: black">
													<input class="mdl-textfield__input error-popover clearAll CA"
														type="text" id="binID" name="bin" autocomplete="off" autocorrect="off" spellcheck="false"
														> <label
														class="mdl-textfield__label" for="sample3">Bin</label>

												</div>
											</div>
										</div>


										<div class="col-xs-12" style="font-style:normal">
											<div class="col-xs-12 text-center"
												style="padding-left: 0px; padding-right: 0px;">
												<button class="btn btn-info wideBtn" type="reset"
													onclick="closeInv('Inv');">Clear</button>
												<button id="saveInventoryBtn" class="btn btn-success wideBtn" type="button"
													onclick="saveInventory();">Save</button>
											</div>
										</div>

								</div>
							</div>
						</div>
						</form>


						<!-- --------------------------------------------------pabitra ------------------------------------------------------------>



</div>
			</div>

		</div>
	</div>
    <script type="text/javascript">
    var productId = '';var userIds = null;var globalStatus = null;var loggedInUserId = '';
     $(document).ready(function(){
    	 
        //var userName=localStorage.getItem("userName"); 
        //alert(userName);
        var loggedInUserFullName = localStorage.getItem("loggedInUserFullName");
        $("#loggedInUserFullNameSpan").text(loggedInUserFullName);
        $("#loggedInUserFullName").text(loggedInUserFullName);

        userIds =  localStorage.getItem("userId");
        loggedInUserId = localStorage.getItem("loggedInUserId");
        productId = localStorage.getItem("productId");
        //alert(productId);
        populateOrders();	
        
        if(isDuplexCall==true){
            var timeoutId = 0;
       		var profileID=localStorage.getItem("profileId");
       		var	serverKeyId=localStorage.getItem("serverKeyId");
            
       		var ajaxfn= function name(){
       			/* $.ajax({
       					type : "POST",
       					url :   serverPath + "/checkForCallv2?serverKeyId="+serverKeyId+"&profileId="+profileID+"&userId="+loggedInUserId,
       					headers: {
       					       "deviceId":"ADMIN",
       					   	 },
       					success : function(data){
       						if(data.errorCode=="1011"){
       							//alert("1001");nurseNameForActiveId
       							$('#showHideVideoIconId').hide();
       							$('#videoIconId').hide();
       							$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
       						}else if(data.code=="200"){
       							if(data.activeUsers!=""){
       								
       								$.each(data.activeUsers,function(index,option){
       									$('#nurseNameForActiveId').html(" "+option.name+" ");
       									$('#showHideVideoIconId').show();
       									$('#videoIconId').show();
       								});
       							
       							}
       							else{
       								$('#showHideVideoIconId').show();
       								$('#videoIconId').hide();
       								$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
       							}
       							
       							if(data.content != null) {
       								var callerName = data.content.name;
       								var roomName = data.content.roomName;
       								var roomId = data.content.id;
       								confPopUp(callerName, roomName, roomId);	
       							}
       						}
       						
       						
       						
       						timeoutId= setTimeout(ajaxfn,3000);	
       					},error:function(x,y,response){
       						$('#showHideVideoIconId').hide();
       						$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
       						timeoutId= setTimeout(ajaxfn,3000);	
       						
       					}
       					   	 
       			}); */
       			var name = '';var activeList = '';
       			$.ajax({
       			   type: "POST",
       			   headers:{"deviceId":"ADMIN"},
       			   url: serverPath + "/checkForCallv3?serverKeyId="+serverKeyId+"&profileId="+profileID+"&userId="+loggedInUserId,
       			   success : function(data){
       				   if(data.code == '200'){
       					   $("#activeUsersId").html('');
       					   name = '';activeList = '';
       					   if(data.activeUsers!=""){
       				$.each(data.activeUsers,function(index,option){
       							   
       							   if(option.name != null){
       								   name = option.name;								   ;
       							   }
       				activeList+='<li class="form-control" style="border-radius:6px;" ><span style="font-size: 12px;text-transform: capitalize;margin-left: -33px;" id="nurseNameForActiveId">'+name+'</span> '+
                       ' <a id= "videoIconId" style="border-radius: 50%;margin-left: 5px;float:right;background-color: green" class="btn btn-primary" title="videoCall" onclick="initiateCall('+option.profileId+')"><b> '+
       				' <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a></li> ';
       							  
       				$("#activeUsersId").html(activeList);
       				
       							   
       						   });
       						   
       					   }else{
       							//$('#showHideVideoIconId').show();
       							//$('#videoIconId').hide();
       							$("#activeUsersId").html("<li class='form-control' style='border-radius:6px;color:gray;'>No Active User</li>");
       						}
       					  
       					if(data.content != null) {
    						var callerName = data.content.name;
    						var roomName = data.content.roomName;
    						var roomId = data.content.id;
    						confPopUp(callerName, roomName, roomId);	
    					} 
       				   }
			   timeoutId= setTimeout(ajaxfn,3000);
			   
		   },error:function(x,y,response){
				//$('#showHideVideoIconId').hide();
				$("#activeUsersId").html("<li class='form-control' style='border-radius:6px;color:gray;'>No Active User</li>");
				timeoutId= setTimeout(ajaxfn,3000);	
				
			}
       			});
       			
       			
       		}
       		 ajaxfn();
       	}else{
       		$('#showHideVideoIconId').hide();
       	}
        
        
     });
     
     function confPopUp(callerName, roomName, roomId) {
    		var room = encodeURIComponent(roomName);
    		var r = confirm( callerName + " is calling you. Do you accept the call?");
    		if(r== true){
    			var win = window.open('initiateCall?roomName='+room, 'result', 'width=1024,height=768');
    			win.addEventListener("resize", function(){
    				win.resizeTo(1024, 768);
    	         });
    		}
    		$.ajax({
    			type : "POST",
    			url :   serverPath + "/intiateAndCloseCall?initiate=false&roomId="+roomId,
    			success : function(data){			
    			}
    		});
    		}
	
	
     function closeitemOpenMasterModal(){
   
     	$("#itemMasterModal").modal('hide');
    
 	} 

      function populateOrders(){
    	 // $(".highLId").removeAttr("style");
    	  //$("#patConId").css("font-weight","bold");
    	 // alert($('#orderTable').val());
    $('#orderTable').DataTable().destroy();
      $.ajax({
      type : "POST",
      url : serverPath+"/getPatientListForDoctor?productId="+productId,
      success : function(data) {
      if (data.code == "200") {
       $("#orderDetails").html("");$("#orderDetailsNoList").hide();
      var uhid = '-';var pName = '-';var age = '';var ageG = '';var gender = '';var rD = '-';
      var city = '-';var cnt = 0;var appendOrderData="";
      if(data.content != '' && data.content != null){
      $.each(data.content,function(id,option){
  city = '-';uhid = '-';pName = '-';age = '';ageG = '';gender = '';rD = '-';
      cnt++;
      if(option.uhid != null){
      uhid = option.uhid;
      }if(option.firstName != null){
      pName = option.firstName;
      }if(option.age != null){
      age = option.age +'/';
      }if(option.gender != null){
      gender = option.gender;
      ageG = age + gender;
      }if(option.city != null){
      city = option.city ;
      }if(option.strRegistrationDate != null && option.waitingTime != null){
      rD = option.strRegistrationDate +" " ;
      }
      //alert(option.waiting);
      if(option.waitingTime	 != null){
    	  
    	  var a = "24:00";
    	  var b = option.waitingTime;
    	  var aa1=a.split(":");
    	  var aa2=b.split(":");

    	  var d1=new Date(parseInt("2001",10),(parseInt("01",10))-1,parseInt("01",10),parseInt(aa1[0],10),parseInt(aa1[1],10));
    	  var d2=new Date(parseInt("2001",10),(parseInt("01",10))-1,parseInt("01",10),parseInt(aa2[0],10),parseInt(aa2[1],10));
    	  var dd1=d1.valueOf();
    	  var dd2=d2.valueOf();
    	  
    	  if(dd1>=dd2){
    		// alert("lesser than 24hr");
    		
    		  appendOrderData+="<tr><td style='text-align:center;'>"+cnt+"</td><td style='text-transform:capitalize;text-align:center;'>"+uhid+"</td> "+
          	" <td style='text-transform:capitalize;text-align:center;'>"+pName+"</td><td style='text-align:center;'>"+ageG+"</td><td style='text-align:center;'>"+city+"</td>"+
          	" <td align='center' valign='middle'><div class='btn-group'> "+
          	"<a href='#' onclick='slectConsulting("+option.userId+","+option.patientId+","+option.episodeId+","+option.appointmentId+","+option.ccpEpisodeId+")' data-toggle='tooltip' class='btn btn-primary'> "+
              " Consultation</a>"+
              "</td><td align='center' valign='middle'> "+rD+" <span >"+option.waitingTime+"hrs</span></td> </tr>";
    		 }else{
    		 
    			 appendOrderData+="<tr><td style='text-align:center;'>"+cnt+"</td><td style='text-transform:capitalize;text-align:center;'>"+uhid+"</td> "+
    	          	" <td style='text-transform:capitalize;text-align:center;'>"+pName+"</td><td style='text-align:center;'>"+ageG+"</td><td style='text-align:center;'>"+city+"</td>"+
    	          	" <td align='center' valign='middle'><div class='btn-group'> "+
    	          	"<a href='#' onclick='slectConsulting("+option.userId+","+option.patientId+","+option.episodeId+","+option.appointmentId+","+option.ccpEpisodeId+")' data-toggle='tooltip' class='btn btn-primary'> "+
    	              " Consultation</a>"+
    	              /* <span style='cursor: pointer;' class='fa fa-eye'></span> */
    	              /* "<button type='button' data-toggle='dropdown' style='padding-bottom: 11px;padding-top: 11px;' class='btn btn-primary dropdown-toggle'><span class='caret'></span></button><ul class='dropdown-menu dropdown-menu-right'>"+ */
    	              "</td><td align='center' valign='middle'> "+rD+" <span style='color:red'>"+option.waitingTime+"hrs</span></td> </tr>";
    		  
    	  }    	  
    	 
      }
      
        	/* appendOrderData+="<tr><td style='text-align:center;'>"+cnt+"</td><td style='text-transform:capitalize;text-align:center;'>"+uhid+"</td> "+
        	" <td style='text-transform:capitalize;text-align:center;'>"+pName+"</td><td style='text-align:center;'>"+ageG+"</td><td style='text-align:center;'>"+city+"</td>"+
        	" <td align='center' valign='middle'><div class='btn-group'> "+
        	"<a href='#' onclick='slectConsulting("+option.userId+","+option.patientId+","+option.episodeId+","+option.appointmentId+","+option.ccpEpisodeId+")' data-toggle='tooltip' class='btn btn-primary'> "+
            " Consultation</a>"+
            "</td><td align='center' valign='middle'> "+rD+" </td> </tr>"; */
          			
        						});
        						$("#orderDetails").html(appendOrderData);
        						$("#orderTable").DataTable( {
            				        "paging":   true,
            				        "ordering": true,
            				        "bLengthChange": false,
            				        "info":     false,
            				        "bDestroy": true,
            				        /* columnDefs: [
            				        	   { orderable: false, targets: -1 }
            				        	] */
            				    });
        						
        						$('.dataTables_filter').addClass('pull-left');
        						 var parentElement = $("#orderTable_filter").parent();
        						 var grandpa = $(parentElement).parent();
        						 var elder= $(grandpa).children(":first");
        						 //console.log(elder);
        						 $(elder).css( "display", "none" );
        						
        					}else if(data.content == null || data.content == 'null'){
              			    	 /* appendOrderData+='<tr role="row"><td colspan="7" align="center"><p>No Patients</p></td></tr>';
               			    	  $("#orderDetails").html(appendOrderData); */
        						$("#orderDetailsNoList").show();
               			      }
        					
        		        }
        		    }
                });
         }

         function slectConsulting(patientUserId,patientId,episodeId,appointmentId,ccpEpisodeId){
        	 //alert(appointmentId);
        	localStorage.setItem("patientUserId",patientUserId);
        	localStorage.setItem("patientId",patientId);
        	localStorage.setItem("patientepisodeId",episodeId);
        	localStorage.setItem("ccpEpisodeId",ccpEpisodeId);
        	
        	if(appointmentId == null || appointmentId == 'null'){
        		appointmentId = '';
        	} 
        	
        	$.ajax({
        		type:"POST",
        		url : serverPath+"/createAppointmentForPatient?userId="+patientUserId+"&doctorId="+loggedInUserId+"&productId="+productId+"&episodeId="+episodeId+"&appointmentId="+appointmentId,
        		success: function(data){
        		
        			if(data.code == '200'){
        				if(data.message != null && data.message != ''){
        	        		localStorage.setItem("appointmentId",data.message);
        	        		populateOrders();
        	        	}
        				window.location.href = 'dashBoard_D';
        			}if(data.code == '201'){
        				
        				toastr.error(data.message);
        			}
        		}
        	}); 
         }
         
         function viewMyList(){
        	 
        	 //$(".highLId").removeAttr("style");
        	 //$("#docConId").css("font-weight","bold");
        	 var appendOrderData="";
        	 $('#listTable').DataTable().destroy();
        	 $.ajax({
        		type:"POST",
        		url:serverPath+"/getMyAppointmentListByDoctorId?productId="+productId+"&doctorId="+loggedInUserId,
        		success:function(data){
        			$("#orderConList").show();$("#orderConList").html('');$("#orderNoList").show();
        			if(data.code == '200'){
        	appendOrderData='';

        				var uhid = '-';var pName = '-';var age = '';var ageG = '';var gender = '';var rD = '-';var wD = '-';
        			      var city = '-';var cnt = 0;var btn= '';
        			      //alert(data.content);
        			      
        			      if(data.content != '' && data.content != null){
        			    	  $("#orderNoList").hide();
        			          $.each(data.content,function(id,option){
        			        	  //alert(appendOrderData);
        			      city = '-';uhid = '-';pName = '-';age = '';ageG = '';gender = '';rD = '-';wD = '-';
        			          cnt++;
        			          if(option.uhid != null){
        			          uhid = option.uhid;
        			          }if(option.firstName != null){
        			          pName = option.firstName;
        			          }if(option.age != null){
        			          age = option.age +'/';
        			          }if(option.gender != null){
        			          gender = option.gender;
        			          ageG = age + gender;
        			          }if(option.city != null){
        			          city = option.city ;
        			          }if(option.strRegistrationDate != null){
        			          rD = option.strRegistrationDate /* + " | "+option.waitingTime+" hrs" */;
        			          
        			          wD = option.waitingTime +" hrs";
        			          }
        			          
        			          if(option.appointmentStatusCode == 'RESV'){
        			        	  //alert(option.appointmentStatusCode);
        			        	  var onk="viewDasB("+option.userId+","+option.patientId+","+option.episodeId+","+option.appointmentId+","+option.ccpEpisodeId+",'RESV')"; 
        			        	 
        			        	  btn ="<div class='btn-group'><a href='#' onclick='deslectConsulting("+option.episodeId+","+option.appointmentId+")' data-toggle='tooltip' class='btn btn-primary'>De-Select</a></div>"; 
        			        	  
        			        	 appendOrderData+="<tr  style='cursor:pointer;'><td style='text-align:center;'>"+cnt+"</td><td style='text-transform:capitalize;text-align:center;' onclick="+onk+">"+uhid+"</td> "+
        	        			    " <td style='text-align:center;text-transform:capitalize;' onclick="+onk+">"+pName+"</td><td style='text-align:center;'onclick="+onk+">"+ageG+"</td><td style='text-align:center;' onclick="+onk+">"+city+"</td>"+
        	        			    "</td><td align='center' valign='middle' onclick="+onk+"> "+rD+" </td><td align='center' valign='middle' onclick="+onk+"> "+wD+" </td> "+
        	        			    " <td align='center' valign='middle'>"+btn+"</td> "+
        	        			    
        	        			    " </tr>";
        			        	 
        			          }
        			          
        			          if(option.appointmentStatusCode == 'COMP'){
        			        	  //alert(option.appointmentStatusCode);
        			        	  var onk="viewDasB("+option.userId+","+option.patientId+","+option.episodeId+","+option.appointmentId+","+option.ccpEpisodeId+",'COMP')"; 
        			        	 
        			        	  btn =" - "; 
        			        	  
        			        	 appendOrderData+="<tr style='color:grey;cursor:pointer;'><td style='text-align:center;'>"+cnt+"</td><td style='text-transform:capitalize;text-align:center;' onclick="+onk+">"+uhid+"</td> "+
        	        			    " <td style='text-align:center;text-transform:capitalize;' onclick="+onk+">"+pName+"</td><td style='text-align:center;'onclick="+onk+">"+ageG+"</td><td style='text-align:center;' onclick="+onk+">"+city+"</td>"+
        	        			    "</td><td align='center' valign='middle' onclick="+onk+"> "+rD+" </td><td align='center' valign='middle' onclick="+onk+"> "+wD+" </td> "+
        	        			    " <td align='center' valign='middle'>"+btn+"</td> "+
        	        			    
        	        			    " </tr>";
        			        	
        			          }
        			          
        			     });
        			         
        			    $("#orderConList").html(appendOrderData);
      					$("#listTable").DataTable({
          				        "paging":   true,
          				        "ordering": true,
          				        "bLengthChange": false,
          				        "info":     false,
          				        "bDestroy": true,
        			      });
      					$('.dataTables_filter').addClass('pull-left');
						 var parentElement = $("#listTable_filter").parent();
						 var grandpa = $(parentElement).parent();
						 var elder= $(grandpa).children(":first");
						 $(elder).css( "display", "none" );
        			            						
        			    }else if(data.content == null || data.content == 'null'){
        			    	$("#orderNoList").show();
       			    	/*  appendOrderData+='<tr role="row"><td colspan="8" align="center"><p>No Patients</p></td></tr>';
      			    	  $("#orderConList").html(appendOrderData); */
      			      }
        				
        			}
        		} 
        	 });
        	 
         }
         
         function deslectConsulting(episodeId,appointmentId){
        	 $.ajax({
        		type:"POST",
        		url:serverPath+"/deselectPatient?episodeId="+episodeId+"&appointmentId="+appointmentId,
        		success:function(data){
        			if(data.code == '200'){
        				toastr.success("Patient De-Selected Successfully");
        				 $("#orderConList").empty();
        				 viewMyList();
        			}
        			
        		}
        		 
        	 });
        	 
         }
         
         function initiateCall(calleProfileId){
        	var proId = localStorage.getItem("productId");
         	if(proId == 1310){
         		alert("This Service is not enabled, Please contact Wenzins");	
         	} else {
            	var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
             	win.addEventListener("resize", function(){
            		win.resizeTo(1024, 768);
             	});
         	}
    	}
         
         function viewDasB(patientUserId,patientId,episodeId,appointmentId,ccpEpisodeId,status){
        	 //alert(status);
        	 localStorage.setItem("patientUserId",patientUserId);
        	 localStorage.setItem("patientId",patientId);
        	 localStorage.setItem("patientepisodeId",episodeId);
        	 localStorage.setItem("ccpEpisodeId",ccpEpisodeId);
        	 localStorage.setItem("status",status);
        	 //slectConsulting(patientUserId,patientId,episodeId,appointmentId);
        	 window.location.href='dashBoard_D'
        	 //alert();
         }
				</script>
    <input type="hidden" id="medhidden" value="-1">
            <button id="demo-show-snackbar" class="mdl-button mdl-js-button mdl-button--raised" style="display:none" type="button"></button>
<!-- <div id="demo-snackbar-example" class="mdl-js-snackbar mdl-snackbar" style="z-index:200000">
  <div class="mdl-snackbar__text"></div>
  <button class="mdl-snackbar__action" type="button"></button>
</div> -->	
 
  </body>
</html>