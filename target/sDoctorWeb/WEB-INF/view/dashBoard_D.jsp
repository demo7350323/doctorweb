<%@ include file="/WEB-INF/view/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title> DashBoard</title>

	<link rel="stylesheet" href="resources/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="resources/css/toastr.min.css"/>
	<link rel="stylesheet" href="resources/css/bootstrap-datepicker.css"/>
	<link href="resources/css/AdminLTE.css" rel="stylesheet" />
	<link rel="stylesheet" href="resources/css/material.css" />
	<link href="resources/css/_all-skins.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="resources/css/font-awesome.min.css" />
	<link rel="stylesheet" href="resources/css/bootstrap-select.css" />
	
	
   	<script src="resources/js/jQuery-2.1.4.min.js"></script>
   	<script src="resources/js/bootstrap.min.js"></script>
   	<script src="resources/js/fastclick.min.js"></script>
    <script src="resources/js/app.min.js"></script>
   	<script src="resources/js/demo.js"></script>
    <script defer src="resources/js/material.js"></script> 
	<script src="resources/js/bootstrap-select.js"></script>
    <link rel="stylesheet" href="resources/css/jquery-ui.css" />

	<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
    <script src="resources/js/bootstrap3-typeahead.js" 	type="application/javascript"></script> 
    <script src="resources/js/jquery.mask.js"></script>
    
     <script src="resources/js/toastr.min.js"></script>
     <script src="resources/js/customHTML.js"></script>
     <script src="resources/js/bootstrap-datepicker.js" type="application/javascript"></script>
    
    <style>

    .navbar-t-centered {
      position: absolute;
left: 34%;
display: block;
text-align: center;
color: #fff;
top: 0px;
padding: 15px;
font-weight: 400;
font-size: 12px;

    }
    @media screen and (min-width:768px){
.navbar-t-centered {
        position: absolute;
        left: 38% ;
        display: block;
       /*  width: 160px; */
        text-align: center;
        color: #fff;
        top:0px;
        padding:15px;
        font-weight: 400;
        font-size:22px;

    }
    }
    


.breadcrumb-arrow {
    height: 36px;
    padding: 0;
    line-height: 36px;
    list-style: none;
    background-color: #e6e9ed
}
.breadcrumb-arrow li:first-child a {
    border-radius: 4px 0 0 4px;
    -webkit-border-radius: 4px 0 0 4px;
    -moz-border-radius: 4px 0 0 4px
}
.breadcrumb-arrow li, .breadcrumb-arrow li a, .breadcrumb-arrow li span {
    display: inline-block;
    vertical-align: top
}
.breadcrumb-arrow li:not(:first-child) {
    margin-left: -5px
}
.breadcrumb-arrow li+li:before {
    padding: 0;
    content: ""
}
.breadcrumb-arrow li span {
    padding: 0 10px
}
.breadcrumb-arrow li a, .breadcrumb-arrow li:not(:first-child) span {
    height: 36px;
    padding: 0 10px 0 25px;
    line-height: 36px
}
.breadcrumb-arrow li:first-child a {
    padding: 0 10px
}
.breadcrumb-arrow li a {
    position: relative;
    color: #fff;
    text-decoration: none;
    background-color: #3bafda;
    border: 1px solid #3bafda
}
.breadcrumb-arrow li:first-child a {
    padding-left: 10px
}
.breadcrumb-arrow li a:after, .breadcrumb-arrow li a:before {
    position: absolute;
    top: -1px;
    width: 0;
    height: 0;
    content: '';
    border-top: 18px solid transparent;
    border-bottom: 18px solid transparent
}
.breadcrumb-arrow li a:before {
    right: -10px;
    z-index: 3;
    border-left-color: #3bafda;
    border-left-style: solid;
    border-left-width: 11px
}
.breadcrumb-arrow li a:after {
    right: -11px;
    z-index: 2;
    border-left: 11px solid #2494be
}
.breadcrumb-arrow li a:focus, .breadcrumb-arrow li a:hover {
    background-color: #4fc1e9;
    border: 1px solid #4fc1e9
}
.breadcrumb-arrow li a:focus:before, .breadcrumb-arrow li a:hover:before {
    border-left-color: #4fc1e9
}
.breadcrumb-arrow li a:active {
    background-color: #2494be;
    border: 1px solid #2494be
}
.breadcrumb-arrow li a:active:after, .breadcrumb-arrow li a:active:before {
    border-left-color: #2494be
}
.breadcrumb-arrow li span {
    color: #434a54
}
    
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
	<div id="load2"
			style="position: fixed; left: 50%; top: 40%; width: 120px; height: 100%; z-index: 9999;display:none">
			<img style="" src="resources/images/icons/loading.gif" alt="loading" />
			<br />
		</div>
        <header class="main-header">
      <nav class="navbar navbar-static-top" role="navigation"
		style="margin-left:0% !important ;max-height:50px; background-color:#3c8dbc">
	<!-- Sidebar toggle button--> 
	<img src="resources/images/logowhite.png" class="user-image" alt="User Image" style="height: 50px; padding-left: 5px; margin-left: 5px;">

	
		<span class ="navbar-t-centered" >Outreach Healthcare System</span>
	
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->

              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="resources/images/avatar.png" class="user-image" alt="User Image">
                  <span class="hidden-xs" id="loggedInUserFullName"></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header" style="height: 138px !important;">
                    <img src="resources/images/avatar.png" class="img-circle" alt="User Image">
                    <p><span id="loggedInUserFullNameSpan"></span>
                    </p>
                  </li>
                  <!-- Menu Body -->
                 <li class="user-body" style="padding:5px;">
                                  <ul class="col-xs-12 text-center" id="activeUsersId">
                                  
                                  <!-- <span style="font-size: 12px; margin-left: -33px;" id="nurseNameForActiveId"></span>
                                  <a id= "videoIconId" style="border-radius: 50%; ;margin-left: 5px;background-color: green" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                  				   <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a> -->
                                  </ul>
                                  
                                   <!-- <div class="col-xs-5 text-center">
                                    <a href="patientList">Patient List</a>
                                  </div> -->
                                </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                    </div>
                    <div class="pull-right">
                      <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="margin-left:0% !important;padding-bottom:40px;">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="display:inline-flex;width:100%;">
          
         <strong style="font-size:18px;padding-top: 7px;" id="dash_id"></strong>
        	
     <!--      <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">User profile</li>
          </ol> -->

			<div id="chronologicalView" class="pull-right text-right" style="width:83%;height:40px;float:right">
        		<!-- <button >OP:12/05/2018</button>
        		<button >OP:18/04/2018</button>
        		<button >OP:09/02/2018</button>
        		<button >OP:01/01/2018</button> -->
        	</div>
        	
        	
        </section>
       
<div class="container col-md-12" style="margin-left: 0px;margin-right: 0px;margin-bottom: -10px;">
    <ol class="breadcrumb breadcrumb-arrow">
		<li><a href="list">Patient Queue</a></li>
		<!-- <li><a href="#">Library</a></li> -->
		<li class="active"><span>Dashboard</span></li>
	</ol>
</div>


        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary ">
                <div class="box-body box-profile text-center" >
                  <img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">
                  <h5 class="profile-username text-center" id="addPrfName"style="margin-bottom: 5px;font-size:16px;"></h5>
                  <p class="text-muted text-center" id="uhidId" style="margin-bottom: 0px;"></p>

              <ul class="list-group list-group-unbordered" id="addDet" style="margin-bottom: 0px;">
                    
                  </ul>
 <div class="col-md-12" id="dashBoardHideShoeVideoIconId">
                   <a style="padding: 12px 12px 10px; border-radius: 50%;box-shadow: 4px 6px ;margin-left: -8px;" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                   <i style="color:white;font-size:20px" class="fa fa-video-camera"></i></b></a> 
                   <!-- <a style=" padding: 12px 12px 10px; border-radius: 50%;box-shadow: 4px 6px ;" class="btn btn-primary" title="Message"><b>
                   <i style="color:white;font-size:20px" class="fa fa-commenting"></i></b></a> -->
                   </div>
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              
              <!-- Consultation Summary -->
              
              <div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
               <div class="box box-primary">
               <div class="box-body box-profile" style="padding: 0px;padding-left: 5px;padding-right: 5px;">
               <div class="box-header" style="padding-left: 0px;padding-right: 0px;padding-top: 15px;padding-bottom: 15px;background:#f4f4f4;margin-left: -5px;margin-right: -5px;margin-bottom: 5px;">
			    <h6 class="box-title" style="font-size:14px;padding-left: 10px;">Consultation Summary</h6>
			   <!--  <i class="fa fa-plus-circle" onclick="viewAddAdv();" aria-hidden="true" style="padding-left: 10px;color:#367fa9"></i> -->
			   <div class="pull-right box-tools" style="right: 5px;" >
                    <button class="btn btn-primary btn-sm" onclick="completeConsultation();" id='displDis' style="margin-top: 5px;" data-toggle="tooltip" title="Complete Consultation" >
                    <i class="fa fa-file-text"></i></button>
               </div>
			   </div>
               
              <div class="box box-primary" style="border-top-width: 0px;">
			                <div class="box-header" style="padding-left: 5px;padding-right: 5px;">
			                	 <h6 class="box-title" style="font-size:14px">Advice</h6>
			                	 <i class="fa fa-plus-circle prevDis" onclick="viewAddAdv();" aria-hidden="true" style="padding-left: 10px;color:#367fa9"></i>
			               		<i class="fa fa-exclamation-circle" id="mandExId" aria-hidden="true" style="display:none;padding-left: 0px;color:red;float: right;"></i>
			               
			                </div><!-- /.box-header -->
			                <div class="box-body no-padding" style="overflow-y: auto;max-height: 70px;">
			                	<table class="table table-striped">
			                    	
			                    	<tbody id="adviseTTableBody"></tbody>
			                    	
			                  	</table>
			                </div><!-- /.box-body -->
			             </div>
			             
			<div class="box box-primary" style="border-top-width: 0px;">
                <div class="box-header ui-sortable-handle" style="padding-left: 5px;padding-right: 5px;">
                 <!--  <i class="ion ion-clipboard"></i> -->
                  <h6 class="box-title" style="font-size:14px">Revisit/Follow-Up</h6>
                  <i class="fa fa-plus-circle prevDis" onclick="viewAddRe();" aria-hidden="true" style="padding-left: 10px;color:#367fa9"></i>
                </div><!-- /.box-header -->
                
                <div class="box-body no-padding">
			                	<table class="table table-striped">
			                    	
			                    	<tbody id="revisitBody"></tbody>
			                    	
			                  	</table>
			                </div>
              </div>
              </div>
              </div>
              </div>

              <!-- About Me Box -->
              <!-- <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">About Me</h3>
                </div>/.box-header
                <div class="box-body">
                  <strong><i class="fa fa-book margin-r-5"></i>  Education</strong>
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                  </p>

                  <hr>

                  <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <p class="text-muted">Malibu, California</p>

                  <hr>

                  <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                  <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-success">Coding</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">Node.js</span>
                  </p>

                  <hr>

                  <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                </div>/.box-body
              </div> --><!-- /.box -->
              
              <!--  <div id="" class="pull-center text-center" style="width:100%%;height:40px;">
              <button type="button" id="completeBtnId" style="margin-left: 10px; background: rgb(54, 127, 169) none repeat scroll 0% 0%;" class="btn btn-primary" onclick="disable();">Complete</button>
              </div> --> 
              
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active" onclick="viewRefresh()"><a href="#activity" data-toggle="tab">Vitals</a></li>
                  <li onclick="getHistoryOfUser(null);"><a href="#timeline" data-toggle="tab">Advice</a></li>
                 
                   <button style="float: right; cursor: pointer; margin-top: 5px;background:#3c8dbc" class="btn btn-primary" onclick="EmrReport()" >Print Report <i class="" style="color:""></i></button>     
                   <button style="float: right; cursor: pointer; margin-top: 5px;background:white" id="refreshId" class="btn" onclick="refreshPage()" >Refresh <i class="fa fa-refresh" style="color:black"></i></button> 
                 <!--  <li><a href="#settings" data-toggle="tab">Settings</a></li> -->
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity" style="padding-left: 10px; padding-right: 10px;">
                         
                         
                          <div class="user-block" style="padding-top: 10px; padding-bottom: 15px;">
                         <!--   <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image"> -->
                        <span class='username' style="margin-left: 0px;">
                          <a href="#activity" style="font-size:16px;color:#3a86bd;display:none;">Vitals</a>
                          <a href='#' class='pull-right btn-box-tool'><!-- <i class='fa fa-times'></i> --></a>
						</span>																	
                        <span class='description' style="margin-left: 0px; padding-top: 10px; padding-left: 5px;" id="vitDate"></span>
                      </div><!-- /.user-block -->
                      
                      
                      <div class="row" id="addVitalsDet">
                    
                      </div>
                 
                    
                     <div class="user-block" style="padding-top: 10px; padding-bottom: 15px;">
                         <!--   <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image"> -->
                        <span class='username' style="margin-left: 0px;">
                          <a href="#" style="font-size:16px;color:#3a86bd;">Electrocardiogram (ECG)</a>
                          <a href='#' class='pull-right btn-box-tool'><!-- <i class='fa fa-times'></i> --></a>
                        </span>
                        <!-- <span class='description' style="margin-left: 0px; padding-top: 10px; padding-left: 5px;" id="addDates">NO RECORD FOUND</span> -->
                      </div><!-- /.user-block -->
                      
                      
                      
                  <div class="row" id="addEcgs">
      <!--      <div class="col-xs-12">
           <span>Lead 1 : </span>
    			<div class="info-box" style="display: inline-flex; padding-bottom: 10px;">
					<img style="width:50%" src="resources/images/Ecg1.png">
				</div>
			 </div>
			  <div class="col-xs-12">
			  <span>Lead 2 : </span>
    			<div class="info-box" style="display: inline-flex; padding-bottom: 10px;">
					<img style="width:50%" src="resources/images/Ecg1.png">
				</div>
			 </div>
    <div class="col-xs-12">
    <span>Lead 3 : </span>
    			<div class="info-box" style="display: inline-flex; padding-bottom: 10px;">
					<img style="width:50%" src="resources/images/Ecg1.png">
				</div>
			 </div>  -->
           
   
    
          </div>
                    <!-- Post -->
                    


                    <!-- Post -->
                    
                  </div><!-- /.tab-pane -->
         <div class="tab-pane" id="timeline">
					
			<div class="row" id="addMeInv" style="margin-left: 0px;margin-right: 0px;">
				<div class="col-md-6" style="padding-left: 0px;padding-right: 5px;">		
			  	<div class="box box-primary">
			                <div class="box-header">
			                	<!-- /<h3 class="box-title">PRESCRIPTION</h3> -->
			                	<!--<b style="font-size:12px">PRESCRIPTION</b>
			                	 <button class="pull-right btn-primary btn dcbutton" onclick="openMedicationModal();">Add Prescription</button> -->
			                	 <h6 class="box-title" style="font-size:14px">Prescription</h6>
			                	 <i class="fa fa-plus-circle prevDis" onclick="openMedicationModal();" aria-hidden="true" style="padding-left: 10px;color:#367fa9"></i>
			                </div><!-- /.box-header -->
			                <div class="box-body no-padding" style="overflow-y: auto;max-height: 185px;">
			                	<table class="table table-striped">
			                    	<!-- <tr>
			                      		<th style="width: 5%"></th>
			                      		<th style="width: 30%"></th>
			                      		<th style="width: 30%"></th>
			                      		<th style="width: 30%"></th>
			                    	</tr> -->
			                    	<tbody id="prescriptionTableBody"></tbody>
			                    	
			                  	</table>
			                </div><!-- /.box-body -->
			             </div>
             	 </div>
				<div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
			 	<div class="box box-primary">
			                <div class="box-header">
			                	<!--<b style="font-size:12px">Investigation</b>
			                	 <button class="pull-right btn-primary btn" onclick="openReportModal();">Add Report</button> -->
			                 <h6 class="box-title" style="font-size:14px">Investigation</h6>
			                <i class="fa fa-plus-circle prevDis" onclick="openReportModal();" aria-hidden="true" style="padding-left: 10px;color:#367fa9"></i>
			                </div>
			                <div class="box-body no-padding" style="overflow-y: auto;max-height: 185px;">
			                	<table class="table table-striped">
			                    	<!-- <thead>
                        <tr>
                          <th>Investigation Name </th>
                          <th>Investigation Type(Result)</th>
                          <th>Lab Name</th>
                          <th>Report Date</th>
                        </tr>
                      </thead> -->
			                    	<tbody id="adviseTableBody">
			                    	
			                    	
			                    	
			                    	</tbody>
			                    	
			                  	</table>
			                </div>
			             </div>	  	
			</div>
		</div>
                    	
			              
			              
			              <div class="row" id="addPrDi" style="margin-left: 0px;margin-right: 0px;">
					
					</div>
			              
			             
			  <div class="row" id="addPreliId" style="margin-left: 0px;margin-right: 0px;">
			   <div class="col-md-12" style="padding-left: 0px;padding-right: 5px;">
			   <div class="box box-primary">
			                <div class="box-header">
			                	 <h6 class="box-title" style="font-size:14px">Preliminary-Diagnosis</h6>
			                	 <i class="fa fa-plus-circle prevDis" onclick="viewAddDiagn();" aria-hidden="true" style="padding-left: 10px;color:#367fa9"></i>
			                </div><!-- /.box-header -->
			                <div class="box-body no-padding">
			                	<table class="table table-striped">
			                    	
			                    	<tbody id="prelTTableBody"></tbody>
			                    	
			                  	</table>
			                </div><!-- /.box-body -->
			             </div>
				<!-- <form id ="saveDiagnosisForm" method="post" enctype ='multipart/form-data' style="height:100%;">
					<input type="hidden" id="diagnoEpiId" name="episodeId" />
					<input type="hidden" id="idDiagno" name="userId" />
					<input type="hidden" id="" name="strPrescribedDate" value="06/12/2018" />
					<input type="hidden" name="documentTypeId" value="10"/>
					<input type="hidden" name="additionalDetailJson" id="diagnoForm" />
					<input type="hidden" name="medicalReportId" value="-1" />
					
					<div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <h6 class="box-title" style="font-size:14px">Preliminary-Diagnosis</h6>
                </div>/.box-header
                
                <div class="box-body" style="padding-top: 0px;">
                
                 <div style="position:relative;padding-left: 0px;" class="col-md-12">
				<select multiple id="ses" class="form-control diagnoJSON" onchange="changedSE();"  attr="DiagnosisType">
						<option value="-1" selected="">Select Category</option>
						<option value="Suspected BP">Suspected BP </option>
						<option value="Suspected Sugar">Suspected Sugar </option>
						<option value="Suspected Cardiac">Suspected Cardiac </option>
						
				</select>
				<textarea type="text" class="form-control" style="display: none; resize: none;margin-top: 10px;" 
				id="diagnosisComment" attr="Comment" placeholder="Comments"></textarea> 
				<div class="modal-footer" style="display:none;" id="footId">
						<button type="button" class="btn btn-default hidePopup" onclick="clearAllValues();" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="diagnosisValidation();" id="diagSave">Save</button>
					</div>
				
				</div>
                  
                </div>
              </div>
				</form>	
				 -->
				
				</div>
			    
			    <!-- <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
			    <div class="box box-primary">
			                <div class="box-header">
			                	 <h6 class="box-title" style="font-size:14px">Advice</h6>
			                	 <i class="fa fa-plus-circle" onclick="viewAddAdv();" aria-hidden="true" style="padding-left: 10px;color:#367fa9"></i>
			                </div>/.box-header
			                <div class="box-body no-padding">
			                	<table class="table table-striped">
			                    	<tr>
			                      		<th style="width: 5%"></th>
			                      		<th style="width: 50%"></th>
			                      		
			                    	</tr>
			                    	<tbody id="adviseTTableBody"></tbody>
			                    	
			                  	</table>
			                </div>/.box-body
			             </div>
			    	</div> -->
				
					</div>
			              
			   <!-- <div class="row" id="addRevId" style="margin-left: 0px;margin-right: 0px;">
					
			
					
					</div>  -->        
			              
		
			              
	                 </div><!-- /.tab-pane -->

                 
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer"
				style="display:block;padding-right:15px; margin-left:0% !important;position: fixed;bottom: 0px;margin-right: auto;margin-left: auto;width:100%;">
			<!--  <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>.</strong> All rights reserved. -->
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0.0
			</div>
			Copyright &copy; 2018-2019 <a href="http://www.wrizto.com/" target=_blank style="color: rgb(60, 141, 188);">Wrizto Healthcare Pvt. Ltd.</a> All rights reserved. </footer>
      <!-- Control Sidebar -->
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    
    
        
        
       <div id="addMedicationModal" name="addMedicationForm" class="modal modal-fullscreen fade" style="height:100%;padding-top: 0px;z-index:9999;overflow-y:hidden;" role="dialog"   data-keyboard="false">
       		<form id = "saveMedicationId" method="post" enctype ='multipart/form-data' style="height:100%;">
       				<input type="hidden" name="additionalDetailJson" id="additionalDetailJson" />
                    <input type="hidden" name="strPrescribedDate" id="strPrescribedDate" />
                   <!--  <input type="hidden"  id="serviceNameValue" name="serviceName" value=""> -->
                    <input type="hidden" name="documentTypeId" id="documentTypeId" value="" />
                    <input type="hidden" name="clinicId" id="clinicId" />
                    <input type="hidden" name="userId" id="idUser" value="" />
                    <input type="hidden" name="ccpId" id="idCcp" value="" />
		 			<input type="hidden" id="medEpisodeId"  class="episodeId" name="episodeId" value="" /> 
		 			<input type="hidden"  id="medicalReportId" name="medicalReportId" value="-1" /> 
		 			<input type="file" style="display:none;" name="files" id="reportFileId" class="getReport" onchange="setFileName(this)"></input>
		  			 
		 			<input type="hidden"  id="deletedRecords" name="deletedRecords" value="" />  
		 			<input type="hidden"  id="loggedIncreatedBy" name="createdBy" value="" />
		     		<input type="hidden"  id="documentTypeName" name="documentTypeName" value="Prescription" />
					<input type="hidden"  id="documentTypeName" name="documentTypeName" value="Prescription" />
					<input type="hidden" id="medicationIdFromDropdown" name="medicationHiddenId" />
		 			
		 			<input type="hidden" id="" name="laboratoryName" value=""/> 
		 			 <input class="getAdvice" type="hidden" name="laboratoryId" value=""/>
		 			 
		 			 <input type="hidden" id="noteComments" name="comments" value=""/>
		 			 <input type="hidden" name="dischargeStatus" value="1"/>
   
                    
         		<div class="modal-dialog" style="padding-top: 0px;height:100%;" >
					<div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
						<div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
							<h5 class="modal-title" >
							<b style="color:White">Medication</b></h5>
						</div>
						<div class="modal-body" style="background: white;padding-bottom: 0px;padding-top: 0px;;overflow-x:hidden;height:450px;">
							<div class="col-xs-12">
								<div class="col-xs-4 "
										style="margin-top: 0px; padding-left: 0px;">
										<div class="col-xs-12"
											style="padding-left: 0px; padding-right: 0px;">
											<div style="width: 100%; color: black" id ="isDirtyId"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
												<input spellcheck="false"
													class="mdl-textfield__input error-popover bClass Newpolic4"
													type="text" id="prescribedByPre" name="prescribedBy"
													maxlength="100" style="text-transform: capitalize;" /> <label class="mdl-textfield__label"
													for="sample3">Prescribed By<i style="color: red">*</i>
												</label>
											</div>
										</div>
								</div>
	
								<div class="col-xs-4"  style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input autocomplete="off" autocorrect="off" spellcheck="false"
											class="mdl-textfield__input error-popover clearAll bClassM Newpolic1"
										autocomplete="off" type="text" id="medicationNameId" name="medicationName"
											maxlength="100" style="text-transform: capitalize;margin-bottom: 8px;"/> 
											<label class="mdl-textfield__label" for="sample3">Medication Name
											<span style="color:red;">*</span>
											</label>
					
										</div>
									</div>
								</div>	
								
							<div class="col-xs-4" style="padding-right: 0px;padding-left: 0px;">	
							<div class="col-xs-6"
									style="margin-top: 0px; padding-left: 0px;padding-right: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black;margin-bottom: 8px;padding-bottom: 24px"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown" >
											<input class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="text" id="dosageID" name="dosage" onkeyup="checkDigit();"
												maxlength="3" /> 
											<input type="hidden" id="dosageHiddenId" />	
												<label class="mdl-textfield__label"
												for="sample3">Dosage 
											</label>
										</div>
									</div>
								</div>
								<div style="padding-left: 0px;padding-right: 0px;" class="col-xs-6 ">
						<p style="margin-bottom: 5px;" id=""></p> 
							<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
								<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
									<select class="form-control" id="masterfequencyType" name="frequency" style="margin-top: 10px;display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
										<option value="MG" selected="">MG</option>
										<option value="ML">ML</option>
										<option value="ML">MM</option>
									</select>
								</div>
							</div>
					</div>
					</div>		
							</div>
							<div class="col-xs-12">
								
								
				
								<div style="padding-left: 0px;" class="col-xs-6 ">
									<p style="margin-bottom: 0px; color: royalblue">Select Frequency</p>
										<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
											<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
												<select class="form-control" id="fequencyId" name="frequency" style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
												<option>0-0-1</option>
												<option>0-1-0</option>
												<option>1-0-1</option>
												<option>1-1-0</option>
												<option>1-1-1</option>
												<option>1-0-0</option>
												</select>
											</div>
										</div>
								</div>
								
								<div style="padding-left: 0px;padding-right: 0px;" class="col-xs-6 ">
									<p style="margin-bottom: 0px; color: royalblue">Instruction</p>
										<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
											<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
												<select class="form-control" id="instructionId" name="instruction" style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
												<option>Before Food</option>
												<option>After Food</option>
												</select>
											</div>
										</div>
								</div>
							
							</div>
							<div class="col-xs-12">
								
				
		            			<div class="col-xs-4"
									style="margin-top: 0px; padding-left: 0px; margin-bottom: 17px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input autocomplete="off" autocorrect="off" 
												spellcheck="false"
												class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="text" id="startId" name="strBeginDate"
												maxlength="100" /> <label class="mdl-textfield__label"
												for="sample3"/>Start Date<i style="color: red">*</i>
											</label>
										</div>
									</div>
								</div>
								
								<div class="col-xs-4"
									style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input class="mdl-textfield__input error-popover bClassM Newpolic1"
												maxlength="3" type="text" id="days" name="days" onkeyup="checkDays()" />
												 <label class="mdl-textfield__label"
												for="sample3">Days<i style="color: red">*</i>
											</label>
										</div>
									</div>
								</div>
									
								<div class="col-xs-4"
									style="margin-top: 0px; padding-right: 0px;padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input autocomplete="off" autocorrect="off"
												spellcheck="false"
												class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="text" id="commentsMedId" name="comments"
												maxlength="100"/> <label class="mdl-textfield__label"
												for="sample3">Comments<!-- <i style="color: red">*</i> -->
											</label>
										</div>
									</div>
								</div>
							</div>
							
							<!-- <div class="col-xs-12">	
								
							</div> -->
							
								<div class="col-xs-12 addedMedTable" id="dynTableMed" style="display:none;color:black;overflow-y: auto;max-height:170px;">
								<table class="table" >
									<tr>
										 <th>Name</th>
										<th>Dosage</th>
										<th>Freq</th>
										<th>Inst</th>
										<th>Start Date</th>
										<th>Days</th>
										<th>Comments</th>
										<th></th>
			 
									</tr><tbody id="medAdd"></tbody>
								</table>
							</div>
						</div>	
						<div id="addFilesMed">
                      		
                        </div>	
           	
						<div class="modal-footer text-center" style="padding: 5px;overflow-x:hidden">
							
							<div class="col-xs-12" style="position: absolute;bottom: 0;">
								<div class="text-center">
									<button type="button" class="btn btn-danger" style="width: 20%;margin-bottom: 6px;" onclick= "canMed();">Cancel</button>
									<button type="button" class="btn btn-warning" id="" style="width: 20%; margin-left: 6px; margin-bottom: 6px;" onclick="addMedication();" >ADD</button>
									<button type="button" class="btn btn-success" id="" style="width: 20%; margin-left: 6px; margin-bottom: 6px;" onclick="saveMedication(1);" >Save</button>
								</div>	
							</div>
						</div>
					</div>
				</div>
	  	 	</form>
    	</div>
        
         <div id="addVisitForm" class="modal fade main-fade-modal " role="dialog"  data-backdrop="static" data-keyboard="false">
  <form id = "saveAdviceId" method="post" enctype ='multipart/form-data' >
  <input class="getAdvice" type="hidden" name="documentTypeId" id="" value="2"/>
   <input class="getAdvice" type="hidden" name="documentTypeName" id="" value="Advice"/>
   <input class="getAdvice" type="hidden" name="serviceTypeIdFromDropdown" id="serviceTypeIdFromDropdown" value="Advice"/>
    <input class="getAdvice" type="hidden" name="servicesId" id ="investigationId"/> 
    <input type="hidden" name="additionalDetailJson" id="adviceJSON"/>
   <!--  <input class="getAdvice" type="hidden"  id="serviceNameId" name="servicesName" > -->
    <input class="getAdvice" type="hidden"  value="services" name="InvestigationType" />
    
   
    <!-- <input type="hidden"  value="30/03/2018" name="strPrescribedDate" > -->
    
       <div class="modal-dialog"  id="visitModalId">
		<div class="modal-content" style="border-radius: 4px;">
			<div class="modal-header" style="background-color:#5498D2;">
			<h5 class="modal-title" ><b style="color:White">Advice</b></h5>
			</div>
			<div class="modal-body"  style="padding-top: 0px;">
 			<div class="col-xs-12" style="margin-top: 0px; padding-left: 0px;" >
 			
 			
 			
 			<div class="col-xs-12 Newpolic3"
				style="margin-top: 0px; padding-left: 0px;">
				<div style="padding-left: 0px;" class="col-xs-12 ">
				
				<div style="width: 100%; color: black;"
							class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
						<input autocomplete="off" autocorrect="off"
										spellcheck="false" style="margin-bottom:8px;"
										class="mdl-textfield__input error-popover bClass Newpolic2 getAdvice"
										type="text" id="serviceTypeId" name="investigationName"
										maxlength="100"> <label class="mdl-textfield__label"
										for="sample3">Service Name<i style="color: red">*</i>
								</label>
											
						

					</div>
						<!-- <p style="margin-bottom: 0px; color: royalblue" id="masterServiceList">Service<i style="color: red">*</i></p>
							<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
								<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
									<select class="form-control getAdvice" id="serviceTypeId" name="serviceTypeId" 
									style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;padding-bottom: 7px;">
										
									</select>
								</div>
							</div> -->
					</div>
			</div>
			<div class="col-xs-12 Newpolic3"
					style="margin-top: 0px; padding-left: 0px;">
					<div class="col-xs-12"
						style="padding-left: 0px; padding-right: 0px;">
						<div id="addadvclassprenameID" style="width: 100%; color: black"
							class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
							<input autocomplete="off" autocorrect="off"
								spellcheck="false"
								class="mdl-textfield__input error-popover getAdvice"
								type="text" id="prescribedByAdv" name="prescribedBy"
								maxlength="100"> <label class="mdl-textfield__label"
								for="sample3">Adviced By<i style="color: red">*</i> 
							</label>
						</div>
					</div>
				</div>
			<div class="col-xs-12 Newpolic3"
									style="margin-top: 0px; padding-left: 0px;">
				<!-- <div class="col-xs-6 "
							style="margin-top: 0px; padding-left: 0px;"> -->
							<div class="col-xs-12"
								style="padding-left: 0px; padding-right: 0px;">
								<div style="width: 100%; color: black"
									class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
								<input autocomplete="off" autocorrect="off"
										spellcheck="false"
										class="mdl-textfield__input error-popover getAdvice"
										type="text" id="fromDate" name="dateTime"
										maxlength="100"> <label class="mdl-textfield__label"
										for="sample3" style="margin-bottom: 0px;">Date (DD/MM/YY)<i style="color: red">*</i>
									</label>
		
							</div>
						<!-- </div> -->
					</div>
				
				</div>
							
			<div class="col-xs-12 Newpolic3"
									style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<textarea autocomplete="off" autocorrect="off"
												spellcheck="false" placeholder="Advice"
												class="mdl-textfield__input error-popover getAdvice"
												 id="advComments" name="Labcomments"
												maxlength="100"></textarea>
										</div>
									</div>
								</div>
						
			</div>		
			<div class="modal-footer text-center">
				
				<!-- <button id="demo-show-snackbar" class="mdl-button mdl-js-button mdl-button--raised" style="display:none" type="button"></button> -->
				<button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 25%; margin-left: 5px; margin-bottom: 0px;" onclick="clearAdvModal()">Cancel</button>
				<!-- <button type="button" class="btn btn-success" id="otpButton" onclick="generateOtp()">Generate OTP</button> -->
				<!-- <button type="button" class="btn btn-warning" id="" style="width: 20%;" onclick="addVisit();" >ADD</button> -->
				<button type="button" class="btn btn-success" id="" style="width: 25%;" onclick="saveMedication(2);" >Save</button>
			</div>
		</div>
	</div>
  </div>         
</form></div>
  
        
        <!-- Observation Modal  -->
        
        <!-- Notes Modal  -->
    
    	<div class="example-modal" >
            <div class="modal" id="addNotesModal">
            	<div class="modal-dialog">
                	<div class="modal-content">
                		<form id="addNotesForm" method="post" enctype ='multipart/form-data'>
                		<input type="hidden" name="additionalDetailJson" />
                   
                    <input type="hidden" name="documentTypeId" value="8" />
                     <input type="hidden" name="clinicId" id="clinicIdNote" />
                     <input type="hidden" name="userId" id="idUserNote" value="" />
                     <input type="hidden" name="ccpId" id="idCcpUP" value="" />
		 <input type="hidden"  class="episodeId" name="episodeId" value="" id="episodeIdUp" /> 
		 <input type="hidden"  id="medicalReportId" name="medicalReportId" value="-1"/> 
		 
		  <input type="hidden"  name="deletedFiles" value=""/> 
		 <input type="hidden"   name="deletedRecords" value=""/>  
		 <input type="hidden"  name="episodeName" value="CCP"/>
		 <input type="hidden"   name="dischargeStatus" value="1"/>
		  <input type="hidden"   name="strPrescribedDate" value="30/03/2018"/>
		  
		 
		<input type="hidden"  name="documentTypeName" value="Notes"/>
                			
	                  		<!-- <div class="modal-header">
	                    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    		<span aria-hidden="true">&times;</span></button>
	                    		<h4 class="modal-title">Add Notes</h4>
	                  		</div> -->
	                  		<div class="modal-header" style="background-color:#5498D2;">
								<h5 class="modal-title" ><b style="color:White">Notes</b></h5>
							</div>
	                  		<div class="modal-body">
	                    		<textarea style="width:100%;height:100px;" id="noeComments" class="getNotes" name="comments"></textarea>
	                    		
	                  		</div>
	                  		<div class="modal-footer">
	                    		<button type="button" class="btn btn-danger" style="width: 25%;" onclick="canNote();">Cancel</button>
	                    		<button type="button" class="btn btn-success" style="width: 25%;" onclick="saveMedication(3);">Save</button>
	                  		</div>
	                  	</form>
                	</div><!-- /.modal-content -->
              	</div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
        
        <!-- Advice Modal  -->
        
        <div id="addAdviceModal" name="" class="modal modal-fullscreen fade" style="padding-top: 100px;height:100%;z-index:9999;background: none;" role="dialog"   data-keyboard="false">
       		<form id ="saveAdviceForm" method="post" enctype ='multipart/form-data' style="height:100%;">
			
			<!-- <input type="hidden" id="diagnoEpiId" name="episodeId" /> -->
					<input type="hidden" name="prescribedBy" value="" />
							
			<div >
                <div class="modal-dialog" style="padding-top: 0px;height:100%;" >
					<div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
						<div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
							<h5 class="modal-title" >
							<b style="color:White">Advice</b></h5>
						</div>
						
                <div class="box-body" style="padding-top: 0px;">
                 
                 <div class="row" style="display: block;margin-top: 8px;">
				<div class="col-sm-6">
				<textarea type="text" class="form-control" style="display: block; resize: none;" name="comments" 
				 id="genAdv" placeholder="General advice"></textarea>
				</div>
				<div class="col-sm-6">
				<div style="position:relative;">
				<label style="font-weight:normal;">
				Referral For:
				</label>
				<select class="form-control" name="serviceId" id="referId">
						<!-- <option value="-1" selected="">Referral For</option> -->
						<option value="2">Local</option>
						<option value="3">Abroad</option>
						
				</select>
				
				</div>
				</div>
				</div>	
                  
                 <!-- <div class="row" style="display:none;margin-left: 0px;margin-right: 0px;" id="viewPrevAdviceId">
                  <h6 class="box-title" style="font-size:14px">Recently Adviced Details</h6>
                  <p class="form-control" id="genAdID"></p>
                  
                  </div> -->
                  
                </div><!-- /.box-body -->
               <div class="modal-footer" >
						<button type="button" class="btn btn-default hidePopup" onclick="clearAllAdvice();" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="saveAdvice();" id="diagSave">Save</button>
					</div>
					</div>
					</div>
					
              </div>
				</form>	
    	</div>
        
        
        <!-- Advice MOdal -->
        
        <!-- Preliminary Diagnosis -->
        
        <div id="addDiagnosisModal" name=" " class="modal modal-fullscreen fade" style="padding-top: 100px;height:100%;z-index:9999" role="dialog"   data-keyboard="false">
       		<form id ="saveDiagnosisForm" method="post" enctype ='multipart/form-data' style="height:100%;">
					<input type="hidden" id="diagnoEpiId" name="episodeId" />
					<input type="hidden" id="idDiagno" name="userId" />
					<input type="hidden" id="" name="strPrescribedDate" value="06/12/2018" />
					<input type="hidden" name="documentTypeId" value="10"/>
					<input type="hidden" name="additionalDetailJson" id="diagnoForm" />
					<input type="hidden" name="medicalReportId" value="-1" />
							
			<div >
                <div class="modal-dialog" style="padding-top: 0px;" >
					<div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
						<div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
							<h5 class="modal-title" >
							<b style="color:White">Preliminary Diagnosis</b></h5>
						</div>
						
                <div class="box-body" style="padding-top: 0px;">
                 
                <!--  <div style="position:relative;padding-left: 0px;" class="col-md-12">
				<select multiple id="ses" class="form-control diagnoJSON" onchange="changedSE();"  attr="DiagnosisType">
						<option value="-1" selected="">Select Category</option>
						<option value="Suspected BP">Suspected BP </option>
						<option value="Suspected Sugar">Suspected Sugar </option>
						<option value="Suspected Cardiac">Suspected Cardiac </option>
						
				</select>
				<textarea type="text" class="form-control" style="display: none; resize: none;margin-top: 10px;" 
				id="diagnosisComment" attr="Comment" placeholder="Comments"></textarea> 
				</div> -->
		
		<!-- <div style="position:relative;padding-left: 0px;height:100%" class="col-md-12">
				
		<select class="selectpicker" multiple data-live-search="true" id="ses">
		 <option id="2" value="suspectedBp">suspectedBp</option>
		 <option id="2" value="suspectedBp">suspectedBp</option>
		 <option id="2" value="suspectedBp">suspectedBp</option>
		</select>
         </div> -->
         <div class="form-group masterDiagnosis" style="margin-top: 10px;" id="diagnoListId">
          
           <!-- <label style="padding-right: 25px;">
           <input type="checkbox" value="Suspected BP"  class="minimal"/>Suspected BP
           </label>
           <label style="padding-right: 25px;">
           <input type="checkbox" class="minimal" value="Suspected Sugar"/>
           <h style="padding-left: 5px;" >Suspected Sugar</h>
           </label> 
           <label style="padding-right: 25px;">
           <input type="checkbox" class="minimal" /><h style="padding-left: 5px;">Suspected Cardiac</h>
           </label> -->
           
         </div>
                  
         
       <!--  <div style="position:relative;padding-left: 0px;height:100%;" class="col-md-12" id="ses">
			
			
		
         </div>  -->
         
                </div><!-- /.box-body -->
               <div class="modal-footer" style="display:block;text-align:center !important;" id="footId">
						<button type="button" class="btn btn-default hidePopup" onclick="clearAllValues();">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="diagnosisValidation();" id="diagSave">Save</button>
					</div>
					</div>
					</div>
					
              </div>
				</form>	
    	</div>
        
        <!-- Preliminary Diagnosis -->
        
        <!-- Revisit -->
        
        <div id="addRevisitModal" name=" " class="modal modal-fullscreen fade" style="padding-top: 100px;height:100%;z-index:9999" role="dialog"   data-keyboard="false">
       		<form id ="saveRevisitForm" method="post" enctype ='multipart/form-data' style="height:100%;">
					<input type="hidden" id="revisitEpiId" name="episodeId" />
					<input type="hidden" id="idDiagno" name="userId" />
					<input type="hidden" id="" name="strPrescribedDate" value="06/12/2018" />
					<input type="hidden" name="documentTypeId" value="10"/>
					<input type="hidden" name="additionalDetailJson" id="diagnoForm" />
					<input type="hidden" name="medicalReportId" value="-1" />
							
			<div >
                <div class="modal-dialog" style="padding-top: 0px;height:100%;" >
					<div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
						<div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
							<h5 class="modal-title" >
							<b style="color:White">Revisit/Follow-Up</b></h5>
						</div>
						
                <div class="box-body" style="padding-top: 0px;">
                 
                 <div class="row" style="display: block;padding-top: 15px;">
				<div class="col-sm-6" style="display:inline-flex;">
				<h style="padding-top: 7px;padding-right: 5px;padding-left: 15px;width:52%;">Revisit After</h>
				<select class="form-control" id='media' onchange="calFunction()">
					<option value="-1" selected="">After</option>
						<!-- <option value="2 days">2 days</option>
						<option value="1 week">1 week</option>
						<option value="15 days">15 days</option> -->
						
				</select>
				
				</div>
				<div class="col-sm-6" style="text-align: center;">
				<div class="col-sm-1" style="padding-top: 7px;padding-left: 0px;">OR</div>
				<!-- <div class="col-sm-5 input-group date" style="padding-top: 0px;" onclick="viewCalendar()">
				<span class="label label-primary" style="padding-top: 15px;padding-bottom: 6px;">
				<i class="fa fa-calendar" style="font-size:24px!important;padding-right: 2px;padding-left: 2px;padding-bottom: 0px;padding-top: 2px;margin-top: 3px;"></i>
				</span>
				</div> -->
				</div>
				</div>	
                  
                  
                  <div class="col-xs-12" style="padding-top: 10px; margin-top: 10px;padding-left: 0px;display:block;" id="selectDaId">
					<div style="width: 100%; color: black">
					<input	class="mdl-textfield__input error-popover" placeholder="Please Select Exact Date For Appointment"
						style="padding-left: 15px;" type="text" id="datetimepicker1" name="" onclick="getDate()" />
										</div>
									</div>
                  
                </div><!-- /.box-body -->
               <div class="modal-footer" style="text-align:center!important">
						<button type="button" class="btn btn-default hidePopup" onclick="clearAllRevisit();">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="revisitValidation();" id="diagSave">Save</button>
					</div>
					</div>
					</div>
					
              </div>
				</form>	
    	</div>
        
        <!-- Revisit -->
        
        
        <!----------------------- Attachment Modal-------------------------  -->
            
            <div class="modal fade" id="attachDoc" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content"  style="">
				<div class="modal-header" style="padding-bottom: 0px;padding-top: 2px;">
				<h5>Documents</h5>
				</div>
				<div class="modal-body" style="padding-top: 0px;padding-bottom: 0px;" >
				<div id="attachBod" style="padding-top: 10px;">
				
				</div>

			     </div>
				   <div class="modal-footer" style="margin-top: 5px;text-align: center !important;padding-top: 10px;padding-bottom: 10px;">
					<button type="button" class="btn btn-danger" data-dismiss="modal" id="closeId" style="text-transform: capitalize;">Close</button>
   
                  </div>
             </div>
	    </div> 
	</div>
   <!---------------------------- Attachment Modal close----------------------------------------------  -->
   
        
        
        
    <div class="example-modal" >
       <div id="addReportModal" class="modal fade main-fade-modal " role="dialog" style="z-index:9999" data-backdrop="static" data-keyboard="false">
         <form id="saveLabModId" method="post" enctype='multipart/form-data'> 
          <input type="hidden" name="additionalDetailJson" id="additionalDetailJsonInv" />
          <input type="hidden" name="report" class="reportId" value="1" />
          <input type="hidden" name="documentTypeId" class="documentTypeId" value="2" />
          <input type="hidden" name="clinicId" class="clinicId" />
          <input type="hidden" name="userId" id="idUserLab" value="" />
          <input type="hidden"  id="deletedFiles" name="deletedFiles" value="" /> 
		  <input type="hidden"  id="deletedRecords" name="deletedRecords" value="" />
		  <input type="hidden"  id="documentTypeName" name="documentTypeName" value="Investigation" />
		  <input type="hidden" name="medicalReportId" value="-1" />
		   <input type="hidden" id="investigationNameIdFromDropdown" name="investigationNameHiddenId" />
		  <input type="hidden" id="labNameIdFromDropdown" name="laboratoryId" value="" />
		  <input type="hidden" name="investigationType" id="masterServiceSplitId" value="" /> 
           <input type="hidden" name="episodeId" id="invEpisodeId"/>
          
          <div class="modal-dialog" style="padding-top: 35px;" id="labModalId">
		<div class="modal-content" style="border-radius: 4px;">
			<div class="modal-header" style="background-color:#5498D2;border-bottom-width: 0px;">
			<h5 class="modal-title" ><b style="color:White">Investigation</b></h5>
			</div>
	           <div class="modal-body">
	              
	              <div class="col-xs-4" style="margin-top: 0px; padding-left: 0px;">
					<div class="form-group">
                    <label style="font-weight: normal;">Advice Date :</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input class="form-control Newpolic2" type="text" id="reportDate" name="strPrescribedDate"/>
                    </div><!-- /.input group -->
                  </div>
                  </div>
                  
                  <div class="col-xs-4" style="margin-top: 0px; padding-left: 0px;">
					<div class="form-group">
                    <label style="font-weight: normal;">Laboratory Name :</label>
                    <input type="text" id="labNameId" name="laboratoryName" class="form-control Newpolic2" type="text"/>
                  </div>
                  </div>
                  
                  <div style="padding-left: 0px;margin-top: -2px;" class="col-xs-4 ">
						<p style="margin-bottom: 0px;" id="masterServiceListType">Investigation Type</p>
							<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
								<div style="width: 100%;margin-top: 3px; color: black; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
									<select class="form-control" id="fequencyType" name="frequency" style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
										<option value="1~X-Ray" selected>X-Ray</option>
										<option value="2~Investigation">Investigation</option>
									</select>
								</div>
							</div>
					</div>
					
					<div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
					<div class="form-group">
                    <label style="font-weight: normal;">Investigation Name :</label>
                    <input type="text" id="investigationNameId" name="investigationName" class="form-control Newpolic2 invRemAAdd" type="text"/>
                  </div>
                  </div>  
                  
                  <div class="col-md-6" style="display:inline-flex;padding-left: 0px;padding-right: 0px;margin-top: 20px;margin-bottom: 15px;">
				<div class="radio col-md-6" style="padding-left: 0px;padding-right: 0px;">
                        <label>
                          <input type="radio" class="investigationType" name="investigationType" id="adviceOptId" value="Advice" style="margin-top: 2px;" />
                           &nbsp;&nbsp;Advice
                        </label>
                      </div>
                      
                      <div class="radio col-md-6" style="padding-left: 0px;padding-right: 0px;margin-top: 10px;">
                        <label>
                          <input type="radio" onclick="shoeReport();" class="investigationType" name="investigationType" id="reportOptId" value="Report" style="margin-top: 2px;" />
                          &nbsp;&nbsp;Report
                        </label>
                      </div>
                      </div> 
                      
                      <div style="padding-left: 0px;display:none;" class="col-xs-6" id="showResultDivId">
						<p style="margin-bottom: 5px;" id="masterServiceListType">Result</p>
							<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
								<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
									<select class="form-control" id="resultTypeId" name="result" style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
										<option value="1" selected>Positive</option>
										<option value="0">Negative</option>
									</select>
								</div>
							</div>
					</div>  
					
					<div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
					<div class="form-group">
                    <label style="font-weight: normal;">Comments :</label>
                    <input type="text" id="invcomments" name="Labcomments" class="form-control Newpolic2 invRemAAdd" type="text"/>
                  </div>
                  </div>   		
	                    		
	                    		
	                    		<!-- <div class="col-xs-12"
										style="margin-top: 0px; margin-bottom: 20px; padding-left: 0px;display:inline-flex; height: 35px">
										<div class="col-xs-12"
											style="padding-left: 0px; padding-right: 0px;">
											<div style="color: black;display:inline-flex;width:95%"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
												<i style="padding-top: 5px;margin-left: 0px;padding-right: 10px" class="fa fa-paperclip"></i>
												
												<button class="btn" type="button" onclick="getElementById('reportFileId').click()" style="border:1px solid #5498d2">Browse</button>
												<span style="width:80%;height:34px;border:1px solid #5498d2;padding-top: 6px; padding-left: 12px;" id="showReportFileName"></span>
											
											</div>
										</div>
	
									
		
									</div> -->
									<!-- <div class="col-xs-12"
									style="margin-top: 0px; margin-bottom: 0px; padding-left: 0px;display:inline-flex; height: 35px">
									<div class="col-xs-8"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="color: black;display:inline-flex;padding-top: 0px;width:100%"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<i style="padding-top: 5px;margin-left: 0px;padding-right: 10px" class="fa fa-paperclip"></i>
											<input type="file" style="margin-bottom: 0px;
											width:100%; border-radius: 4px; overflow:hidden;
											font-size:15px;color:royalblue;border:royalblue 1px solid;"
											name="files" id="firstinvfile" onchange="acceptInvType(this);" />
										</div>
									</div>

							<div style="color: black;padding-top: 0px;"
								class="col-xs-4 mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">

								<button type="button" onclick="getOtherInvFile()"
									style="width: 45%;float:left;margin-right: 5px;overflow: hidden;background: none repeat scroll 0% 0% transparent; color: royalblue !important; border-radius: 4px;  font-size: 11px; border: 1px solid royalblue; margin-bottom: 0px; margin-top: -1px; padding: 0px; height: 29px; margin-top: 0px;"
									class="btn right">Upload More</button>
									
									
									<button class="btn right" style="width: 45%;float:left;background: none repeat scroll 0% 0% transparent; 
									color: royalblue !important; border-radius: 4px;  font-size: 11px; 
									border: 1px solid royalblue; margin-bottom: 0px; margin-top: -1px;
									 padding: 0px; height: 29px; margin-top: 0px;" onclick="clearFiles()" type="button">Clear</button>
	
							</div>
							
						</div> -->				
					<div style="max-height:100px;padding-left: 0px;padding-right: 0px;overflow:auto" class="col-xs-12" id="addFilesRepo">
                      
                      </div>
								</div>
			
			<div class="modal-footer text-center" style="padding-left: 0px;border-top-width: 0px;">
			<div class="col-xs-12 addedInvTable" id="dynTableInv" style="display:none;color:black;max-height:150px;overflow:auto">
		    <table class="table" >
			<tr >
			<th style="padding-right: 0px;">Investigation Name</th>
			<th style="padding-right: 0px;">Type</th>
			<!-- <th>Result</th> -->
			<th>Comments</th>
			<th>Delete</th>
			
			</tr><tbody id="invAdd"></tbody>
			</table>
			</div>
			<div class="col-xs-12 text-center" style="position: relative;bottom: 0;padding-top: 15px;">
				<!-- <button id="demo-show-snackbar" class="mdl-button mdl-js-button mdl-button--raised" style="display:none" type="button"></button> -->
				<button type="button" class="btn btn-danger" style="width: 22%;margin-left: 5px; margin-bottom: 0px;text-transform: capitalize;" onclick="clearInvModal()">Cancel</button>
				<!-- <button type="button" class="btn btn-success" id="otpButton" onclick="generateOtp()">Generate OTP</button> -->
				<button type="button" class="btn btn-warning" id="" style="width: 22%;text-transform: capitalize;" onclick="addInvestigation();" >ADD</button>
				<button type="button" class="btn btn-success" id="" style="width: 22%;text-transform: capitalize;" onclick="saveInvestigation();" >Save</button>
			</div>
			
		</div>					
	                  		<!-- <div class="modal-footer" style="margin-top: 15%;">
	                    		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
	                    		<button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 25%;" onclick="clearRep();">Cancel</button>
	                    		<button type="button" class="btn btn-success" style="width: 25%;" onclick="saveMedication(4);">Upload</button>
	                  		</div> -->
	                  	
                	</div>
              	</div>
              	</form>
            </div>
        </div>
        </div>
    
    <div id="viAtImg" style="display:none;"></div>
    </body>
    
    


<script>

//var	serverPath ="http://192.168.1.45:9091/PopulationCarev2";
 var s =" ";
 var test=3;
 var episdeId =  "";
 var userId = "";
 var episodeId =  "";
 var loggedInUserId = '';
 var productId = '';
 var appointmentId = '';var ccpEpisodeId ='';var statusD = '';
 
//var patientId =  localStorage.getItem("patientId");
var patientId =  localStorage.getItem("patientId");

$(document).ready(function() {
	//alert(episodeId);alert(episdeId);
	//ccpEpisodeId ='';
	getPatientDetails();
	//getVitalDocument(-1);
	episdeId = "";statusD = '';
	if(isDuplexCall==true){
		
		ccpEpisodeId = localStorage.getItem("ccpEpisodeId");
		//alert(ccpEpisodeId);
		$('#dashBoardHideShoeVideoIconId').hide();
		var timeoutId = 0;
		var profileID=localStorage.getItem("profileId");
		var	serverKeyId=localStorage.getItem("serverKeyId");
		loggedInUserId = localStorage.getItem("loggedInUserId");
		appointmentId = localStorage.getItem("appointmentId");
		//alert(appointmentId);
		
		statusD = localStorage.getItem("status");
		if(statusD != null && statusD == 'COMP'){
			$("#completeBtnId").attr("disabled", "disabled");	
		}
		
		$("#addAdviceModal").draggable({
		      handle: ".modal-header"
		  });
		
		$(".form-control diagnoJSON").change(function(){

	        var selectedCountry = $(this).children("option:selected").val();
	        //alert(selectedCountry);
		});
		
		
		$("#adviceOptId").click(function(){
			//alert();
			$("#showResultDivId").hide();
		});
		
		$("#datetimepicker1").datepicker({
			autoclose: true,
			todayHighlight: true,
			startDate: '0m',
			format: "dd/mm/yyyy"
		 }).on('change',function(e){
			 $("#datetimepicker1").attr("readonly", true);
		    	$("#datetimepicker1").parent().addClass('is-dirty');
				$(this).datepicker('hide');
			 });
		
		
		$("#reportDate").datepicker({
			autoclose: true,
			format: "dd/mm/yyyy"
		 }).on('change',function(e){
			 $("#reportDate").attr("readonly", true);
		    	$("#reportDate").parent().addClass('is-dirty');
				$(this).datepicker('hide');
			 });
		
		$('#labNameId').typeahead({
			source: function(query, process) {
				
				
				 var searchText = $('#labNameId').val();
				
				// console.log(searchParameter);
			 	 var $url = serverPath+"/getGeo_tags?searchText="+searchText+"&tagTypeId=2&userId="+userId;
				 var $datas = new Array;
				 $datas = [""];
				 $.ajax({
					 url: $url,
					 headers: {
				       "deviceId":"ADMIN",
				   	 },
					 dataType: "json",
					 type: "GET",
					 success: function(data) {
						   
						 $.map(data.content, function(data){
							 var group;
							 group = {
								 id: data.id,
								 name: data.name,
								
								 toString: function () {
								 	return JSON.stringify(this);
								 },
								 toLowerCase: function () {
								 	return this.name.toLowerCase();
								 },
								 indexOf: function (string) {
								 	return String.prototype.indexOf.apply(this.name, arguments);
								 },
								 replace: function (string) {
								 	var value = '';
									value += this.name;
									 if(typeof(this.name) != 'undefined') {
									 	value += ' <span class="pull-right muted">';
									 	value += this.name;
									 	value += '</span>';
									 }
									 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
								 }
							 };
							 $datas.push(group);
						 });
						 process($datas);
					 }
				 });
			 },
			 property: 'name',
			 items: 10,
			 minLength: 2,
			 updater: function (item) {
			 var item = JSON.parse(item);
			 $("#labNameIdFromDropdown").val('');
			 $("#labNameIdFromDropdown").val(item.id);
			 //alert(itemId);
			 //console.log( itemId);
			 return item.name;
			}
		});
		
		$('#investigationNameId').typeahead({
			source: function(query, process) {
				
				
				 var masterService = $('#fequencyType').val();
				 //alert(masterService);
				 var splitt = masterService.split("~");
		
				 //alert(masterService);
				 //alert(splitt[0]);
				// console.log(searchParameter);
			 	 var $url = serverPath+'/masterServiceList?serviceTypeId='+splitt[0];
				 var $datas = new Array;
				 $datas = [""];
				 $.ajax({
					 url: $url,
					 headers: {
				       "deviceId":"ADMIN",
				   	 },
					 dataType: "json",
					 type: "GET",
					 success: function(data) {
						   
						 $.map(data.content, function(data){
							 var group;
							 group = {
								 id: data.id,
								 name: data.value,
								
								 toString: function () {
								 	return JSON.stringify(this);
								 },
								 toLowerCase: function () {
								 	return this.name.toLowerCase();
								 },
								 indexOf: function (string) {
								 	return String.prototype.indexOf.apply(this.name, arguments);
								 },
								 replace: function (string) {
								 	var value = '';
									value += this.name;
									 if(typeof(this.name) != 'undefined') {
									 	value += ' <span class="pull-right muted">';
									 	value += this.name;
									 	value += '</span>';
									 }
									 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
								 }
							 };
							 $datas.push(group);
						 });
						 process($datas);
					 }
				 });
			 },
			 property: 'name',
			 items: 10,
			 minLength: 2,
			 updater: function (item) {
			 var item = JSON.parse(item);
			 $('#investigationNameIdFromDropdown').val('');
				//alert(item.id);
			 $('#investigationNameIdFromDropdown').val(item.id);
			 return item.name;
			}
		});
		
		 var ajaxfn= function name() {
			/* $.ajax({
					type : "POST",
					url :   serverPath + "/checkForCallv2?serverKeyId="+serverKeyId+"&profileId="+profileID+"&userId="+loggedInUserId,
					headers: {
					       "deviceId":"ADMIN",
					   	 },
					success : function(data){
						if(data.errorCode=="1011"){
							//alert("1001");nurseNameForActiveId
							$('#showHideVideoIconId').hide();
							$('#videoIconId').hide();
							$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
						}else if(data.code=="200"){
							if(data.activeUsers!=""){
								
								$.each(data.activeUsers,function(index,option){
									$('#nurseNameForActiveId').html(" "+option.name+" ");
									$('#showHideVideoIconId').show();
									$('#videoIconId').show();
								});
							
							}
							else{
								$('#showHideVideoIconId').show();
								$('#videoIconId').hide();
								$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
							}
							
							if(data.content != null) {
								var callerName = data.content.name;
								var roomName = data.content.roomName;
								var roomId = data.content.id;
								confPopUp(callerName, roomName, roomId);	
							}
						}
						
						
						
						timeoutId= setTimeout(ajaxfn,3000);	
					},error:function(x,y,response){
						$('#showHideVideoIconId').hide();
						$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
						timeoutId= setTimeout(ajaxfn,3000);	
						
					}
					   	 
			}); */
			
			var name = '';var activeList = '';
			$.ajax({
			   type: "POST",
			   headers:{"deviceId":"ADMIN"},
			   url: serverPath + "/checkForCallv3?serverKeyId="+serverKeyId+"&profileId="+profileID+"&userId="+loggedInUserId,
			   success : function(data){
				   if(data.code == '200'){
					   $("#activeUsersId").html('');
					   name = '';activeList = '';
					   if(data.activeUsers!=""){
				$.each(data.activeUsers,function(index,option){
							   
							   if(option.name != null){
								   name = option.name;								   ;
							   }
				activeList+='<li class="form-control" style="border-radius:6px;" ><span style="font-size: 12px; margin-left: -33px;" id="nurseNameForActiveId">'+name+'</span> '+
                ' <a id= "videoIconId" style="border-radius: 50%;margin-left: 5px;float:right;background-color: green" class="btn btn-primary" title="videoCall" onclick="initiateCall('+option.profileId+')"><b> '+
				' <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a></li> ';
							  
				$("#activeUsersId").html(activeList);
							   
				 });
				   
   					   }else{
   							$("#activeUsersId").html("<li class='form-control' style='border-radius:6px;color:gray;'>No Active User</li>");
   						}

						if(data.content != null) {
							var callerName = data.content.name;
							var roomName = data.content.roomName;
							var roomId = data.content.id;
							confPopUp(callerName, roomName, roomId);	
						}   
				   }
				   timeoutId= setTimeout(ajaxfn,3000);
				   
			   },error:function(x,y,response){
					//$('#showHideVideoIconId').hide();
					$("#activeUsersId").html("<li class='form-control' style='border-radius:6px;color:gray;'>No Active User</li>");
					timeoutId= setTimeout(ajaxfn,3000);	
					
				}
			});
			
			
		}
		 ajaxfn();
	}else{
		$('#showHideVideoIconId').hide();
	}
	
	
	 var loggedInUserFullName = localStorage.getItem("loggedInUserFullName");
		loggedInUserId = localStorage.getItem("loggedInUserId");
		
	var patientUserId= localStorage.getItem("patientUserId");	
     $("#loggedInUserFullName").text(loggedInUserFullName);
	 $("#loggedInUserFullNameSpan").text(loggedInUserFullName);
	
	 $("#loggedIncreatedBy").val(loggedInUserId);
	 $("#idUser").val(patientUserId);
	 $("#idUserLab").val(patientUserId);
	 $("#idUserNote").val(patientUserId);
	 $("#idDiagno").val(patientUserId);
	 
	var PatEPisoId = localStorage.getItem("patientepisodeId");
	//alert(PatEPisoId);
	
	 
	productId = localStorage.getItem("productId");
	var userGroupCode =  localStorage.getItem("userGroupCode");
	if(userGroupCode=="DOC"){
		
		 $("#dash_id").text("Doctor DashBoard");
		 $(".dcbutton").show();
	}
	else{
		$("#dash_id").text("Nurse DashBoard");
		$(".dcbutton").hide();
	}
	 
	$.ajax({
		type : "POST",
		url :   serverPath + "/getEpisodeList?patientId="+patientId,
		success : function(data){
			if(data.code=="200"){
				
				var size = data.content.length;
				s=size;
				
				var appendText = "";
				$.each(data.content,function(index,option){
        			if(index == 0){
        				episdeId = option.id;
        				episodeId = option.id;
        				userId = option.userId;
        				//alert(episdeId);
        				//alert(userId);
        			}
        			
        			
        			
       			 if(size>4){
  		    	   
          			if(index<4){  
          				
          				if(index==0){
          					appendText += '<button type="button" id="prevId" style="margin-left:10px;background:#B8B8B8;display:none;" class="btn btn-primary normalCol" onclick="previous('+option.id+');">'+"PREV"+'</button>'        					
          						
          				}
          				
          				appendText += '<button type="button" id="'+index+'" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="getHistoryOfUser('+option.id+');">'+option.episodeName+'</button>'		
          			}
          			else{
          				
          				appendText += '<button type="button" id="'+index+'" style="margin-left:10px;background:#B8B8B8; display:none;" class="btn btn-primary normalCol" onclick="getHistoryOfUser('+option.id+');">'+option.episodeName+'</button>'
          				if(index==size-1){
          					appendText += '<button type="button" id="nextId" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="next('+option.id+');">'+"NEXT"+'</button>'         				
          				}
          			}
         }
          			
          else{
          			
       appendText += '<button type="button" id="'+index+'" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="getHistoryOfUser('+option.id+');">'+option.episodeName+'</button>'		
          				
          			}
					appendText += '<button type="button" id="'+option.id+'" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="getHistoryOfUser('+option.id+');">'+option.episodeName+'</button>'
					
				});
				$("#chronologicalView").html(appendText);
				
				$( "#chronologicalView button" ).first().css( "background-color", "#367fa9" );
				$( "#chronologicalView button" ).first().addClass('isActive');
				getMasterVitals();
				getVitalDocument(episdeId);
				getAdviceDetails();
				getHistoryOfUser(episdeId);
				
			}
		}
	});
	//alert(episdeId);
	
	 $('#addVisitForm').on('show.bs.modal', function (e) {
				
			$("#fromDate").mask("99/99/99", {});
		});
	 $('#addMedicationModal').on('show.bs.modal', function (e) {
			
			//$("#startId").mask("99/99/99", {});
			//$("#medAdd").html('');
			//$("#dynTableMed").css("display","none");
	  });
	 
	 $('#startId').datepicker({
		 	autoclose: true,
		 	todayHighlight: true,
		 	startDate: '-0m',  
			format: "dd/mm/yyyy"
			}).on('changeDate', function(e){
				$("#startId").attr("readonly", true);
		    	$("#startId").parent().addClass('is-dirty');
				//$(this).datepicker('hide');
			});
	 $('#fromDate').datepicker({
	 	 autoclose: true,
	 	 todayHighlight: true,  
		 format: "dd/mm/yyyy",
		}).on('changeDate', function(e){
			$("#fromDate").attr("readonly", true);
	    	$("#fromDate").parent().addClass('is-dirty');
			$(this).datepicker('hide');
		});
	 
	 $('#serviceTypeId').typeahead({
			source: function(query, process) {
				
				
			 	 var $url = serverPath+'/masterServiceList?serviceType=services';
				 var $datas = new Array;
				 $datas = [""];
				 $.ajax({
					 url: $url,
					 headers: {
				       "deviceId":"ADMIN",
				   	 },
					 dataType: "json",
					 type: "GET",
					 success: function(data) {
						   
						 $.map(data.content, function(data){
							 var group;
							 group = {
								 id: data.id,
								 name: data.value,
								
								 toString: function () {
								 	return JSON.stringify(this);
								 },
								 toLowerCase: function () {
								 	return this.name.toLowerCase();
								 },
								 indexOf: function (string) {
								 	return String.prototype.indexOf.apply(this.name, arguments);
								 },
								 replace: function (string) {
								 	var value = '';
									value += this.name;
									 if(typeof(this.name) != 'undefined') {
									 	value += ' <span class="pull-right muted">';
									 	value += this.name;
									 	value += '</span>';
									 }
									 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
								 }
							 };
							 $datas.push(group);
						 });
						 process($datas);
					 }
				 });
			 },
			 property: 'name',
			 items: 10,
			 minLength: 2,
			 updater: function (item) {
			 var item = JSON.parse(item);
			 $('#investigationId').val('');
				//alert(item.id);
			// $('#serviceTypeIdFromDropdown').val(item.id);
			 $('#investigationId').val(item.id);
			 /* $("#servicesName").val(item.name);
			 alert( $('#servicesName').val()); */
			 return item.name;
			}
		});
	 
	$('#medicationNameId').typeahead({
		source: function(query, process) {
			
			
			 var searchParameter = $('#medicationNameId').val();
		 	 var $url = serverPath +'/getMediactionList?searchParameter='+searchParameter;
			 var $datas = new Array;
			 $datas = [""];
			 $.ajax({
				 url: $url,
				 dataType: "json",
				 type: "GET",
				 headers: {
				       "deviceId":"ADMIN",
				   	 },
				 success: function(data) {
					   //console.log(data.content);
					 if(data.content!=undefined && data.content!='undefined'){
					 $.map(data.content, function(data){
						 var group;
						 group = {
							 id: data.id,
							 name: data.name,
							 dosage: data.dosage,
							
							 toString: function () {
							 	return JSON.stringify(this);
							 },
							 toLowerCase: function () {
							 	return this.name.toLowerCase();
							 },
							 indexOf: function (string) {
							 	return String.prototype.indexOf.apply(this.name, arguments);
							 },
							 replace: function (string) {
							 	var value = '';
								value += this.name;
								 if(typeof(this.name) != 'undefined') {
								 	value += ' <span class="pull-right muted">';
								 	value += this.name;
								 	value += '</span>';
								 }
								 
								 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
							 }
						 };
						 $datas.push(group);
					 });}
					   else{
						   $datas = [""];
						   }
					 process($datas);
				 }
			 });
		 },
		 property: 'name',
		 items: 10,
		 minLength: 2,
		 updater: function (item) {
		 var item = JSON.parse(item);
		 $('#medicationIdFromDropdown').val('');
		 //alert(item.dosage);
		 $("#dosageHiddenId").val(item.dosage);
		 $("#dosageHiddenId").trigger('click');
		 $('#medicationIdFromDropdown').val(item.id);
		 return item.name;
		}
	});
	
	$("#dosageHiddenId").click(function(){
		
		$("#dosageID").val('');$("#masterfequencyType").val("MG");
		var hidVal = $("#dosageHiddenId").val();
		var val = hidVal.substr(-2, 2);
		var str = hidVal.replace(/[^0-9]+/ig,"");
		//alert(hidVal);alert(str);
		
		if(hidVal != '' && str != ''){ 
		$("#dosageID").parent().addClass('is-dirty');
		$("#dosageID").val(str);
		}
		
		
		if(val== 'MG' || val== 'mg' || val== 'Mg'){
			$("#masterfequencyType").val("MG");
		}else if(val== 'ML' || val== 'ml' || val== 'Ml'){
			$("#masterfequencyType").val("ML");
		}
	});
	
	 $('#serviceNameId').typeahead({
			source: function(query, process) {
				
				
				 var searchParameter = $('#serviceNameId').val();
			 	 var $url = 'getServicesList?searchParamater='+searchParameter;
				 var $datas = new Array;
				 $datas = [""];
				 $.ajax({
					 url: $url,
					 dataType: "json",
					 type: "GET",
					 success: function(data) {
						   //console.log(data.content);
						 if(data.content!=undefined && data.content!='undefined'){
						 $.map(data.content, function(data){
							 var group;
							 group = {
								 id: data.id,
								 name: data.name,
								
								 toString: function () {
								 	return JSON.stringify(this);
								 },
								 toLowerCase: function () {
								 	return this.name.toLowerCase();
								 },
								 indexOf: function (string) {
								 	return String.prototype.indexOf.apply(this.name, arguments);
								 },
								 replace: function (string) {
								 	var value = '';
									value += this.name;
									 if(typeof(this.name) != 'undefined') {
									 	value += ' <span class="pull-right muted">';
									 	value += this.name;
									 	value += '</span>';
									 }
									 
									 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
								 }
							 };
							 $datas.push(group);
						 });}
						   else{
							   $datas = [""];
							   }
						 process($datas);
					 }
				 });
			 },
			 property: 'name',
			 items: 10,
			 minLength: 2,
			 updater: function (item) {
			 var item = JSON.parse(item);
			 $('#serviceFromDropdown').val('');
			 $('#serviceFromDropdown').val(item.id);
			 return item.name;
			}
		});
	 
	 $("#closeId").click(function(){
     	
     	$("#attachBod").empty();
     });
	
}); //END of READY

function confPopUp(callerName, roomName, roomId) {
	var room = encodeURIComponent(roomName);
	var r = confirm( callerName + " is calling you. Do you accept the call?");
	if(r== true){
		var win = window.open('initiateCall?roomName='+room, 'result', 'width=1024,height=768');
		win.addEventListener("resize", function(){
			win.resizeTo(1024, 768);
         });
	}
	$.ajax({
		type : "POST",
		url :   serverPath + "/intiateAndCloseCall?initiate=false&roomId="+roomId,
		success : function(data){			
		}
	});
	}
	
function refreshPage(){
	location.reload();
} 

function sendPics(){
	
	/* 
	var form1 = document.getElementById('saveMedicationId');
		 var formData = new FormData(form1);
			
		$.ajax({
			type : "POST",
			url : "saveVitalDocument",
			headers: {
		        "deviceId":"ADMIN",
		    },

			data: formData,
			dataType:"json",
		    mimeType: "multipart/form-data",
			async : false,
		    cache : false,
			contentType : false,
		    processData : false,
			success : function(data){
				
			}
		}); */
	}
	
function clearAdvModal(){
	
	$("#addVisitForm").modal('hide');
	$("#serviceTypeId").val('1');
	$('.getAdvice').val('');
	$('.getAdvice').parent().removeClass('is-dirty');
}
	function getMasterVitals(){
		var addVitals = "";	
		
		$('#load2').show();
		$.ajax({
			type : "POST",
			url : serverPath + "/getMasterVitals",
			headers: {
		        "deviceId":"ADMIN",
		    },
		success : function(data){
				if(data.code=="200"){
					$.each(data.content,function(index,option){
            
            if(option.code == "Temp"){
                    
              addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="temphdr">'+
                '<div style="display:inline-flex" class="info-box">'+
                
                      ' <div style="margin-left: 20px;padding-left:0px" class="info-box-content">'+
                      ' <span class="info-box-text">TEMPERATURE</span><br> '
                      + '<span style="font-size:20px;" class=""><b style="color:#000000;font-weight: normal;margin-left:-36px;padding-left:35px;" id="highTemp" class="hereClass">--</b>/<b style="color:#000000;font-weight: normal;" id="lowTemp" class="hereClass">--</b></span><small id="temoExt" class="emptyExt"></small>'+
                   
                      
                    '</div>'+
                  '</div>'+
                '</div>';
                  
          }
          if(option.code == "PUL"){
      addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" id="puldr">'+
        '<div class="info-box" style="display:inline-flex">'+
            '<div style="margin-left: 20px;" class="info-box-content">'+
            '<span class="">PULSE</span><br><br> '+
            '<span class="hereClass" style="font-size:20px;color:#000000" id="pulId">--</span><small id="pulExt" class="emptyExt"></small>'+
          '</div>'+
       '</div>'+
      '</div>';
    }
    /* if(option.code == "SPO2"){
            addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="spo2dr">'+
              '<div class="info-box" style="display:inline-flex">'+
                '<div style="margin-left: 20px;" class="info-box-content">'+
                  '<span class="">SPO2</span><br><br> '+
                  '<span class="hereClass" style="font-size:20px;color:#000000" id="spo2Id">--</span><small id="spoExt" class="emptyExt"></small>'+
                '</div>'+
              '</div>'+
              '</div>';
            
                  } */
    
    if(option.code == "SPO2"){
        addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="spo2dr">'+
          '<div class="info-box" style="display:inline-flex">'+
            '<div style="margin-left: 20px;" class="info-box-content">'+
              '<span class=""><b>SPO2</b></span><br><br> '+
              '<span class="hereClass" style="font-size:20px;color:#000000" id="spo2Id">--</span><small id="spoExt" class="emptyExt"></small>'+
            '</div>'+
          '</div>'+
          '</div>';
        
              }
    if(option.code == "BP"){
              addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="bphdr">'+
                '<div class="info-box" style="display:inline-flex">'+
                     '<div style="margin-left: 20px;padding-left:0px" class="info-box-content">'+
                        '<span class="">BLOOD PRESSURE</span> <br><br>' +
                        '<span style="font-size:20px;" class=""><b style="color:#000000;font-weight: normal;margin-left:-36px;padding-left:35px;" id="highBp" class="hereClass">--</b>/<b style="color:#000000;font-weight: normal;" id="lowBp" class="hereClass">--</b></span><small id="bpExt" class="emptyExt"></small>'+
                      '</div>'+
                    '</div>'+
                  '</div>';
          }
      if(option.code == "BG"){
    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="bghdr">'+
      '<div class="info-box" style="display:inline-flex">'+
        '<div style="margin-left: 20px;" class="info-box-content">'+
          '<span class="">BLOOD GLUCOSE</span><br> <br>'+
          '<span class="hereClass" style="font-size:20px;color:#000000" id="bgId">--</span><small id="bgExt" class="emptyExt"></small>'+
        '</div>'+
      '</div>'+
      '</div>';
    
          }
    if(option.code == "WEI"){
              addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="weighthdr">'+
                  '<div class="info-box" style="display:inline-flex">'+
                       '<div style="margin-left: 20px;" class="info-box-content">'+
                        '<div style="display:inline-flex">'+
                          '<span class="">WEIGHT</span>'+
                          '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(1);"></i>'+
                          '</div><br><br>'+
                          '<span class="hereClass" style="font-size:20px;color:#000000" id="weightId">--</span><small class="emptyExt"  id="weightExt"> kg</small>'+
                        '</div>'+
                      '</div>'+
                    '</div>';
            }
      if(option.code == "HEI"){
              addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="heighthdr" >'+
                  '<div class="info-box" style="display:inline-flex">'+
                       '<div style="margin-left: 20px;" class="info-box-content">'+
                       ' <div style="display:inline-flex">'+
                          '<span class="">HEIGHT</span>'+
                      '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                          '</div> <br><br> '+
                          '<span class="hereClass" style="font-size:20px;color:#000000" id="heightId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                        '</div>'+
                      '</div>'+
                    '</div>';
            }
      if(option.code == "BMI"){
      addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" id="bmihdr" style="">'+
        '<div class="info-box" style="display:inline-flex">'+
              '<div style="margin-left: 20px;" class="info-box-content">'+
            '<span class="">BMI</span><br> <br>'+
            '<span class="hereClass bmiValue" style="font-size:20px;color:#000000" id="bmiId">--</span><small id="bmiExt" class="emptyExt"> kg/m2</small>'+
          '</div>'+
        '</div>'+
      '</div>';
  }
  if(option.code == "FAT"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="fathdr" style="">'+
                      '<div style="display:inline-flex" class="info-box">'+
                      
                            ' <div style="margin-left: 20px;" class="info-box-content">'+
                            ' <span class="info-box-text">FAT </span> <br>'
                            + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-16px" id="fatId">--</span><small id="fatExt" class="emptyExt"></small>'+
                            
                          '</div>'+
                        '</div>'+
                      '</div>';
                }

    //1.muscle
    //2.bone
    if(option.code == "MUS"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="musIdHrd" style="">'+
                    '<div style="display:inline-flex" class="info-box">'+
                    
                          ' <div style="margin-left: 20px;" class="info-box-content">'+
                          ' <span class="info-box-text">MUSCLE</span> <br>'
                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;"  id="musId">--</span><small id="musExt" class="emptyExt"> </small>'+
                          
                        '</div>'+
                      '</div>'+
                    '</div>';
              } 
    if(option.code == "BONE"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="boneIdHrd" style="">'+
                    '<div style="display:inline-flex" class="info-box">'+
                    
                          ' <div style="margin-left: 20px;" class="info-box-content">'+
                          ' <span class="info-box-text">BONE MASS</span> <br>'
                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;" id="boneId">--</span><small id="boneExt" class="emptyExt"> </small>'+
                          
                        '</div>'+
                      '</div>'+
                    '</div>';
              } 

						
		if(option.code == "WATER"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="waterhdr" style="">'+
                    '<div style="display:inline-flex" class="info-box">'+
                    
                          ' <div style="margin-left: 20px;" class="info-box-content">'+
                          ' <span class="info-box-text">WATER CONTENT</span> <br>'
                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;" id="waterId">--</span><small id="waterExt" class="emptyExt"> </small>'+
                          
                        '</div>'+
                      '</div>'+
                    '</div>';
              }				
						
						
		//3.prot
    if(option.code == "PRO"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="proteinIdHrd" style="">'+
                    '<div style="display:inline-flex" class="info-box">'+
                    
                          ' <div style="margin-left: 20px;" class="info-box-content">'+
                          ' <span class="info-box-text">PROTEIN</span> <br>'
                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;" id="proteinId">--</span><small id="proteinExt" class="emptyExt"> </small>'+
                          
                        '</div>'+
                      '</div>'+
                    '</div>';
              } 	
    //4.vfl
    if(option.code == "VFL"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="vftIdHrd" style="">'+
                    '<div style="display:inline-flex" class="info-box">'+
                    
                          ' <div style="margin-left: 20px;" class="info-box-content">'+
                          ' <span class="info-box-text">VISCERAL FAT LEVEL</span> <br>'
                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;" id="vftId">--</span><small id="vftExt" class="emptyExt"> </small>'+
                          
                        '</div>'+
                      '</div>'+
                    '</div>';
              } 
    //5.Bm			
						
		if(option.code == "BM"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="bmIdHrd" style="">'+
                    '<div style="display:inline-flex" class="info-box">'+
                    
                          ' <div style="margin-left: 20px;" class="info-box-content">'+
                          ' <span class="info-box-text">BASAL METABOLISM</span> <br>'
                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;" id="bmId">--</span><small id="bmExt" class="emptyExt"> </small>'+
                          
                        '</div>'+
                      '</div>'+
                    '</div>';
              }
		
		
		
		
		
        			});
					
					$("#addVitalsDet").append(addVitals);
					getEpisodeVitals(-1);
				}if(data.errorCode=="1010"){
					$('#load2').hide();
			    	toastr.warning("Server Error");
				}
			}
		});
  }
   
   function promtfun(ob){
	   if(ob == 0){
	    var height = prompt("Please enter your height(ft)", "");
       var arr = { "Height": height}; 
	   }
	   if(ob == 1){
		    var weight = prompt("Please enter your Weight(Kg)", "");
		       var arr = { "Weight": weight}; 
	   }
	   
	   var actHeight = $("#heightId").html();
	   var actWeight = $("#weightId").html();
       if (height != null && height !='' || weight != null && weight !='') {
	    	$.ajax({
				type : "POST",
				url : "saveEpisodeVitals?episodeId="+episdeId+"&patientId="+patientId+"&status=1&VitalArray="+JSON.stringify(arr),
				headers: {
			        "deviceId":"ADMIN",
			    },
			success : function(data){
				if(data.code=="200"){
					if(height != null && height !='' || weight != null && weight !=''){
						if(height != null && height !=''){
							actHeight = height;
						}
						if(weight != null && weight !=''){
							actWeight = weight;
						}
						
						var heightInMeter = actHeight*0.3048;
						var BMI = actWeight/(heightInMeter*heightInMeter);
			      		
			      		if(actHeight != null && actHeight != '' && actWeight != null && actWeight != ''){
			      			$("#bmihdr").css("display","block");
			      			$(".bmiValue").html('');
			      			$(".bmiValue").html(BMI);
			      			$("#bmiExt").html("Kg/m2");
			      		}
					}
					
		      		getEpisodeVitals(episdeId);
		      		getVitalDocument(episdeId);
		      		toastr.success("succesfully Edited");
				}
		
				}
			    ,error:function(x,y,response){
			    	$('#load2').hide();
			    	toastr.error("Server Error");
					
				}
			});
	    	
	    	
	    	
	    }else{
	    	/* toastr.warning("Please Enter the field"); */
	    }
	 
	    
	    
	    
    }
	
	function getEpisodeVitals(id){
		$('#load2').show();
		var epi = "";
		var useId="";
		if(id=="-1"){
		epi = episdeId;
		 /* Please dont change this this is not logged in user id */
			//alert("use"+useId);
			//alert("user"+userId);
		}else{epi=id;}
		useId=userId; 
		$('.hereClass').html("--");
		$('.emptyExt').html("");
		$.ajax({
			type : "POST",
			url : serverPath+ "/getConsumerVitals?userId="+useId+"&episodeId="+epi,		
			headers: {
		        "deviceId":"ADMIN",
		    },
		success : function(data){
			if(data.code=="200"){
				var height=""; var weight = "";
				$.each(data.content,function(index,option){
					
					if(option.vitalsName.toLowerCase() == "Weight".toLowerCase()){
						$('#weightId').html(option.vitalsValue);
						$('#weightExt').html(" Kg");
						weight = option.vitalsValue;
						//$('#weighthdr').show();
					}
					if(option.vitalsName.toLowerCase() == "Height".toLowerCase()){
						$('#heightId').html(option.vitalsValue);
						$('#heightExt').html(" cm");
						height = option.vitalsValue;
						//$('#heighthdr').show();
					}
		
					if(option.vitalsName.toLowerCase() == "Temperature".toLowerCase()){
						$('#highTemp').html(option.vitalsValue+'<b style="font-weight:normal;font-size:15px">C</b>');
						//$('#temoExt').html("C/F");
						var lowtemp = option.vitalsValue * 9/5 + 32 ;
						$('#lowTemp').html(lowtemp.toFixed(1)+'<b style="font-weight:normal;font-size:15px">F</b>');
						$('#temphdr').show();
					}
          if(option.vitalsName.toLowerCase() == "BMI".toLowerCase()){
            $('#bmiId').html(option.vitalsValue);
          //  $('#bmiExt').html(" cm");
            height = option.vitalsValue;
            //$('#heighthdr').show();
          }
					
					/*if(height != null && height !='' && weight != null && weight !=''){
						var heightInMeter = height*0.3048;
						var BMI = weight/(heightInMeter*heightInMeter);
			    
			      		$("#bmihdr").css("display","block");
			      		$(".bmiValue").html('');
			      		BMI = BMI.toFixed(2);
			      		$(".bmiValue").html(BMI);
			      		$('#bmiExt').html("kg/m2");
					}*/

					
				
					
			/* 		if(option.vitalsName == "Temperature"){
						$('#highTemp').html(option.vitalsValue+'<b style="font-weight:normal;font-size:15px">C</b>');
						//$('#temoExt').html("C/F");
						var lowtemp = option.vitalsValue * 9/5 + 32 ;
						$('#lowTemp').html(lowtemp.toFixed(1)+'<b style="font-weight:normal;font-size:15px">F</b>');
						$('#temphdr').show();
					} */																																																																																																																																																																																																																																																																																		
					if(option.vitalsName.toLowerCase() == "Pulse".toLowerCase()){
						$('#pulId').html(option.vitalsValue);
						$('#pulExt').html(" b/m");
						
						$('#puldr').show();
					}
					
					if(option.vitalsName.toLowerCase() == "BP".toLowerCase()){
						//$('#weightId').html(option.vitalsValue);
						var splt = (option.vitalsValue).split(",");
					$('#highBp').html(splt[0]);
					$('#lowBp').html(splt[1]);
					 
					$('#bpExt').html("mmhg");
					$('#bphdr').show();
					}
				/* 	if(option.vitalsName == "BMI"){
						$('#bmiId').html(option.vitalsValue);
						$('#bmiExt').html("kg/m2");
						
						//$('#bmihdr').show();
					} */
					
		
					if(option.vitalsName.toLowerCase() == "BMI".toLowerCase()){
						$('#bmiId').html(option.vitalsValue);
						//$('#bmiExt').html("kg/m2");
						
						//$('#bmihdr').show();
					}
					if(option.vitalsName.toLowerCase() == "BG".toLowerCase()){
					$('#bgId').html(option.vitalsValue);
					$('#bgExt').html("mg/dl");
					
					$('#bghdr').show();
				}
				/* 	if(option.vitalsName.toLowerCase() == "SPO2".toLowerCase()){
					$('#spo2Id').html(option.vitalsValue);
					$('#spoExt').html("%");
					
					$('#spo2dr').show();
				} */
					if(option.vitalsName.toLowerCase() == "SPO2".toLowerCase()){
						$('#spo2Id').html(option.vitalsValue);
						$('#spoExt').html("%");
						
						$('#spo2dr').show();
					}
					
					if(option.vitalsName.toLowerCase() == "FAT LEVEL".toLowerCase()){
					$('#fatId').html(option.vitalsValue.trim());
					$('#fatExt').html("%");
					
					//$('#fathdr').show();
				}if(option.vitalsName.toLowerCase() == "WATER CONTENT".toLowerCase()){
					$('#waterId').html(option.vitalsValue);
					$('#waterExt').html("%");
					
					//$('#waterhdr').show();
				}

        if(option.vitalsName.toLowerCase() == "MUSCLE".toLowerCase()){
            $('#musId').html(option.vitalsValue);
            $('#musExt').html(" Kg");
           // weight = option.vitalsValue;
            
          }
          if(option.vitalsName.toLowerCase() == "PROTEIN".toLowerCase()){
            $('#proteinId').html(option.vitalsValue);
            $('#proteinExt').html(" %");
           // weight = option.vitalsValue;
            
          }
          if(option.vitalsName.toLowerCase() == "VISCERAL FAT LEVEL".toLowerCase()){
            $('#vftId').html(option.vitalsValue);
           // $('#vftExt').html(" Kg");
           // weight = option.vitalsValue;
            
          }
          if(option.vitalsName.toLowerCase() == "BASAL METABOLISM".toLowerCase()){
            $('#bmId').html(option.vitalsValue);
            $('#bmExt').html(" Kcal");
           // weight = option.vitalsValue;
            
          }
          if(option.vitalsName.toLowerCase() == "BONE".toLowerCase()){
            $('#boneId').html(option.vitalsValue);
            $('#boneExt').html(" Kg");
           // weight = option.vitalsValue;
            
          }

					$('#vitDate').html(option.strCreatedDate);
				});
				$('#load2').hide();
			}if(data.errorCode=="1009"){
				$('#load2').hide();
		    	//toastr.warning("No Vitals Found");
			}if(data.errorCode=="1010"){
				$('#load2').hide();
		    	toastr.warning("Server Error");
			}
			}
		    ,error:function(x,y,response){
		    	$('#load2').hide();
		    	toastr.error("Server Error");
				
			}
		});
		
		$('#load2').hide();
		
	}

	function getVitalDocument(id){
		var histIds = "";
		if(id == null || id == "-1"){
			histIds = episdeId;
		}else{
			histIds = id;
		}
		var addDoc = "";
		var addEcgDoc = "";
		$.ajax({
			type : "POST",
			url : serverPath+ "/getEcgReports?episodeId="+histIds,
			headers: {
		        "deviceId":"ADMIN",
		    },
			success : function(data){
				if(data.code=="200"){
					if(data.content != null){
					    $.each(data.content,function(index,option){
							if(option.filePath!=null){
								if(option.fileType!="SPO2" && option.fileType!="ecg"){
									var tagvar=option.fileType;
									var arr=tagvar.split(":");
									if(arr[1] == undefined || arr[1] == null || arr[1] == "null"){
										arr[1] = '' ;										
							        }
				             	    addDoc+='<div class="col-xs-12">'+
				                    '<span style="display:inline-flex;"> <b style="text-transform:uppercase;">'+arr[0].trim()+':</b>&nbsp;<p style="color:#0000FF;">'+arr[1]+'</p></span>'+
		    			            '<div class="info-box" style="display: inline-flex; padding-bottom: 10px;">'+
							        '<img  src="'+imagePath+option.filePath+'" >'+
						            '</div>'+
					                '</div>';
							    } else if(option.fileType == "ecg"){
							        addEcgDoc+='<div class="col-xs-12">'+
							        '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="'+imagePath+option.filePath+'" target="_blank"><b style="text-transform:uppercase;">ECG Report</b></a></span>'+
                                    '</div>';
							    }
				            }
				            $('#addDates').html(option.strCreatedDate);
			            });
					}
					$('#addEcgs').html(addDoc + addEcgDoc);
				}if(data.errorCode=="1090"){
					$('#load2').hide();
			    	//toastr.warning("No ECG Found");
				}if(data.errorCode=="1010"){
					$('#load2').hide();
			    	toastr.warning("Server Error");
				}
			}
		});
	}

	function logout(){
		window.location.href="logout";
	} 
	
   function	EmrReport(){
	   ccpEpisodeId = localStorage.getItem("ccpEpisodeId");
	//  alert(ccpEpisodeId);
	   var emrreport = serverPath+"/getEmrPdfReport?patientId="+patientId+"&userId="+userId+"&productId="+productId+"&episodeId="+episdeId+"&ccpEpisodeId="+ccpEpisodeId;
	
//alert(emrreport);
	   window.open(emrreport);


   }
	
	function openMedicationModal(){
		
		$("#addMedicationModal").modal('show');
		
		var prescribedBy = $("#loggedInUserFullName").text();
		$("#prescribedByPre").parent().addClass('is-dirty');
		$("#prescribedByPre").val(prescribedBy);
	}
	function openObservationModal(){
		$("#addObservationModal").modal('show');
		
	}function openAdviceModal(){
		//$("#addObservationModal").modal('show');
		$("#addVisitForm").modal('show');
		var prescribedBy = $("#loggedInUserFullName").text();
		$("#prescribedByAdv").parent().addClass('is-dirty');
		$("#prescribedByAdv").val(prescribedBy);
		
	}
	
	function openNotesModal(){
		$("#addNotesModal").modal('show');
	}

	function openReportModal(){
		$("#addReportModal").modal('show');
		$("#firstmedfile").val('');
	}
	function acceptType(ob){

		if(ob.files[0].type == "image/jpeg" || ob.files[0].type == "application/vnd.ms-excel" || ob.files[0].type =="application/pdf" 
			|| ob.files[0].type =="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || ob.files[0].type =="image/png"){

			}else{
				  $("#firstmedfile").val('');
				  /* mes = "File format not supported";
				  $('#demo-show-snackbar').trigger('click'); */
				 // alert("File format not supported");
				
				}
	}
	var count=0;
    function getOtherMedFile(){
  	  var element1="";
  	  var filecountid='';
  	  
        if($("#firstmedfile").val() != '')
        {
	      	count++;
	    	element1 = '<p style="display:inline-flex;margin-bottom: 5px;" ><i class="fa fa-paperclip" style="padding-top: 5px;margin-left: 15px;color:black;padding-right: 10px"></i> '
	         +'<input type="file" id="filecountid'+count+' "class="browserfield" name="files" onchange="" style="margin-bottom: 0px; border-radius: 4px; font-family:Roboto Medium;font-size:15px;color:#5498D2;border:#5498D2 1px solid">'
	         +'<i style="cursor: pointer;color:black; padding-top: 7px;" class="fa fa-times" onclick="removeFiles(this)"></p>';
        }
     
        var checkFile = false;
    	$(".browserfield").each(function(){
    	    if($(this).val()!=""){
    		      
    		 }else{
    		         checkFile = true;
    			}
    	});
    	if(checkFile!=true){ 
	       $("#addFilesMed").append(element1);
		}
   
    }
	medData = '';
	function addMedication(){
		//alert();
		var valName = $("#medicationNameId").val();
		
		//alert(valName);
		var medicationId = $("#medicationIdFromDropdown").val();
		
		if($("#dosageID").val() != '' ){
			var valDosage = $("#dosageID").val()+ $("#masterfequencyType").val();	
		}else{
			var valDosage = '';
		}
		
		if(valName != ''){
		if(medicationId == null || medicationId == ""){
			//alert();
			 $.ajax({
				type : "POST",
				url: "addMedication?medicationName="+valName+"&dosage="+valDosage,
				
				success : function(data) {
					if(data.code=="200"){
						medicationId = data.content.id;
					}
				}
		 	});
		 	//alert("here");
		} 
		}
		

		var valFreq = $("#fequencyId").val();
		var valInst = $("#instructionId").val();
		var valStart = $("#startId").val();
		var valDays = $("#days").val();
		var valComm = $("#commentsMedId").val();

		 if(valName != '' && valStart != '' && valDays != '' && valDays > 0 ){
			 
			$(".addedMedTable").show();
			var medData = '<tr><td class="getJson" style="display:none" attr="medicationId">'+medicationId+'</td><td class="getJson" attr="medicationName">'+valName+'</td>'
							+'<td class="getJson" attr="dosage">'+valDosage+'</td><td class="getJson" attr="frequency">'+valFreq+'</td>'
							+' <td class="getJson" attr="instructions">'+valInst+'</td><td class="getJson" attr="beginDate">'+valStart+'</td>'
							+'<td class="getJson" attr="Days">'+valDays+'</td><td class="getJson" attr="comment">'+valComm+'</td> '
							+'<td><i style="cursor:pointer" class="fa fa-times" aria-hidden="true" onclick="removeFileMed(this)"></i></td></tr>';
			
			$("#medAdd").append(medData);
			$('.Newpolic1').val('');
			$('.Newpolic1').parent().removeClass('is-dirty');
			$('#fequencyId').val('0-0-1');
			$('#instructionId').val('Before Food');
		 }else{
				toastr.warning('Please fill mandatory fields');
			 }
		
		
		
	}
	function removeFileMed(ob){
		 
		$(ob).parent().parent().remove();
		
		if($("#medAdd tr").length < 1){
			$('#dynTableMed').hide();
		}
		if($("#invAdd tr").length < 1){
			$('#dynTableInv').hide();
		}
	}
	function addmedicationjson(){
		var isexist=0;
	 	 var ordr=1;
	 	additionalDetailJson =  '';additonalDetails = '';
		 medicationJSON = [];MedicationFinalJSON = [];
		$(".getJson").each(function() {
	 		//alert($(this).val());
			//console.log($(this).attr("attr"));
 			 
 			if($(this).attr("attr")=="medicationName"){
 				 medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
 			 if($(this).attr("attr")=="dosage"){
 				 medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="frequency"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="instructions"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="beginDate"){
 				$('#strPrescribedDate').val($(this).text());
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="Days"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			} 
	 		if($(this).attr("attr")=="comment"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}   
 			if($(this).attr("attr")=="medicationId"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text() });
 			} 
 			//console.log(ordr);
 			if(ordr%8==0 && ordr>1){	
 				//console.log("inside"+ordr);
 				medicationJSON.push({keyName : "id",value : 0 });
 			 	medicationJSON.push({keyName : "status",value : 1 });
 			 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
 			 medicationJSON.push({keyName : "medicalReportId",value : -1 });
 			 console.log(medicationJSON);
 			 MedicationFinalJSON.push(medicationJSON);
 			 medicationJSON=[];
 			}	 
 		 ordr++;
 		 });
	} 
	function addAdvicejson(){
		datetime = '';
		datetime = $("#fromDate").val();
		additionalDetailJson =  '';additonalDetails = '';
	 medicationJSON = [];MedicationFinalJSON = [];
		$(".getAdvice").each(function() {
	 		
			//console.log($(this).attr("attr"));
			if($(this).attr("name")=="investigationName"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
 			if($(this).attr("name")=="prescribedBy"){
 				 medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
 			 if($(this).attr("name")=="InvestigationType"){
 				 medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		 if($(this).attr("name")=="dateTime"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		 if($(this).attr("name")=="Labcomments"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		if($(this).attr("name")=="investigationId"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		
	 		
	 		
 		 
 		 //ordr++;
		 
 		 });
		
		medicationJSON.push({keyName : "id",value : 0 });
	 	medicationJSON.push({keyName : "status",value : 1 });
	 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
		 medicationJSON.push({keyName : "medicalReportId",value : -1 });
		 console.log(medicationJSON);
		 MedicationFinalJSON.push(medicationJSON);
		 medicationJSON=[];
	} 
	
	var currentdate = new Date();
	/* var datetime = currentdate.getDay() + "/"+currentdate.getMonth() 
	+ "/" + currentdate.getFullYear(); */
	var datetime = currentdate.getDate() + "/" + (currentdate.getMonth()+1) + "/" + currentdate.getFullYear();
	//var dateAdvice = $("#").val();
	//alert(datetime);
	
	//alert(datetime);
	function addNotesjson(){
		 //alert( $("#noeComments").val());
		additionalDetailJson =  '';additonalDetails = '';
		 medicationJSON = [];MedicationFinalJSON = [];
		$(".getNotes").each(function() {
	 		if($(this).attr("name")=="comments"){
	 			medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 				//alert($(this).val());
 				var notCom = $(this).val();
 				//alert(notCom);
 				$("#noteComments").val(notCom);
 				//alert($("#noteComments").val())
 			}
	 		medicationJSON.push({keyName : "addedTime",value : datetime });		 
 		 });
		
		medicationJSON.push({keyName : "id",value : 0 });
	 	medicationJSON.push({keyName : "status",value : 1 });
	 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
		 medicationJSON.push({keyName : "medicalReportId",value : -1 });
		 console.log(medicationJSON);
		 MedicationFinalJSON.push(medicationJSON);
		 medicationJSON=[];
	}
	function addReportjson(){
		additionalDetailJson =  '';additonalDetails = '';
		 medicationJSON = [];MedicationFinalJSON = [];
		
		$(".getReport").each(function() {
	 		if($(this).attr("name")=="files"){
	 			medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 				//alert($(this).val());
 			}
	 		medicationJSON.push({keyName : "addedTime",value : datetime });		 
 		 });
		
		medicationJSON.push({keyName : "id",value : 0 });
	 	medicationJSON.push({keyName : "status",value : 1 });
	 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
		 medicationJSON.push({keyName : "medicalReportId",value : -1 });
		 MedicationFinalJSON.push(medicationJSON);
	}
	
	var medicationJSON = [];
	var MedicationFinalJSON = [];
	prefixStrForPrescription='{\"additionalDetail\":'; 
	additionalDetailJson =  '';
	additonalDetails = '';
	
	function saveMedication(ob){
		//alert($(ob).val());
		$("#additionalDetailJson").val('');
		additionalDetailJson =  '';additonalDetails = '';
	 	 medicationJSON = [];MedicationFinalJSON = [];
	 	 var isexist=0;
	 	 var ordr=1;
	 	// alert(episodeId);
	 	 //alert($("#noteComments").text());
	 	 $("#medEpisodeId").val(episdeId);
	 	
	 	 //$("#diagnoEpiId").val(episdeId);
	 	 if(ob==1){
	 		 var medName = $("#medicationNameId").val();
	 		 var stDaMAn = $("#startId").val();
	 		var stDaysMAn = $("#days").val();
	 		if($(".addedMedTable").css('display')  == 'none'){
	 			//alert('none');
	 			addMedication();
	 			addmedicationjson();
	 			$('#documentTypeId').val(1);
		 		$('#serviceNameValue').val(null);
	 			
	 		}else{
	 			//alert($(".addedMedTable").css('display'));
	 			addmedicationjson(); 
		 		$('#documentTypeId').val(1);
		 		$('#serviceNameValue').val(null);
	 		}
	 		/* if(medName != ''||stDaMAn != ''||stDaysMAn != ''){ */
	 		
	 		/* }else{
				toastr.warning('Please fill mandatory fields');
			} */
	 	 }
	 	 if(ob==2){
	 		var serviceD = $("#serviceTypeId").val();
	 		var servDat = $("#fromDate").val();
	 		var advicePres = $("#prescribedByAdv").val();
	 		 if(serviceD != "" && servDat != "" && advicePres != ""){
	 		addAdvicejson();
	 	    
	 		var date = new Date().toJSON().slice(0, 10).split("-").reverse().join("/");
			console.log(date);
	 		$('#strPrescribedDate').val(date); 
	 		$('#documentTypeId').val(2);
	 		//$('#serviceNameValue').val($('#serviceNameId').val());
	 		 }else{
					toastr.warning('Please fill mandatory fields');
				}
	 	 } 
	 	if(ob==3){
	 		notMan = $("#noeComments").val();
	 		//alert(notMan);
	 		if(notMan != ''){ 
	 		addNotesjson();
	 		var date = new Date().toJSON().slice(0, 10).split("-").reverse().join("/");
			console.log(date);
	 		$('#strPrescribedDate').val(date); 
	 		$('#documentTypeId').val(8);
	 		//$('#serviceNameValue').val($('#serviceNameId').val());
	 		 }else{
				toastr.warning('Please fill mandatory fields');
			}
	 	 }
		if(ob==4){
			var fileMan=$(".getReport").val();
			//alert(fileMan);
			 if(fileMan != ''){
			addReportjson();
			var date = new Date().toJSON().slice(0, 10).split("-").reverse().join("/");
			console.log(date);
	 		$('#strPrescribedDate').val(date); 
			$('#documentTypeId').val(9);
			}else{
				toastr.warning('Please attach report');
			} 
		}
		
		 $('#dynTableMed').show();
	 	
	 	  if(MedicationFinalJSON.length > 0){
	 	        var additonalDetails = prefixStrForPrescription+JSON.stringify(MedicationFinalJSON)+"}";
	 	        $("#additionalDetailJson").val(additonalDetails);
	 	       medicationJSON = [];MedicationFinalJSON = [];
	 	    } 
	
			data = $("#additionalDetailJson").val();
			console.log(data);
			//alert(data);
		 	  if(data != null && data != ''){
		 	 
	 		 var form1 = document.getElementById('saveMedicationId');
	 		 var formData = new FormData(form1);
	 			
	 		
	 		  //alert();
	 		  
	 		$.ajax({
	 			type : "POST",
	 			url : serverPath+"/addDocuments",
	 			headers: {
	 		        "deviceId":"ADMIN",
	 		    },

	 			data: formData,
	 			dataType:"json",
	 		    mimeType: "multipart/form-data",
	 			async : false,
	 		    cache : false,
	 			contentType : false,
	 		    processData : false,
	 			success : function(data){
	 				//alert(data.messageCode);
	 				if(data.messageCode== '200'){
	 					//alert();
	 					$("#additionalDetailJson").val('');
	 					
	 					toastr.success('Added Successfully','');
	 					
	 					canMed();
	 					clearAdvModal();
	 					canNote();
	 					clearRep();
	 					/* $('.modal').modal('hide');
	 					$(".getAdvice").val('');
	 					$('.getAdvice').parent().removeClass("is-dirty"); */
	 					getHistoryOfUser(episdeId);
	 					
	 				
	 					}
	 				if(data.errorCode  == '1011'){
						toastr.warning('Blank Parameter','');
					}
	 				
	 				}

              });
	 		
	 		
	 	    }else{
		 	  
	 	    	 $('#dynTableMed').hide();
		 	     mes = "Please Add the mandatory data";
			     $("#demo-show-snackbar").trigger('click');
				}

	}
	

	
function getPatientDetails(){
	var addDoc = "";var other = "";var gender = "";var mob = "";var address="";var email = "";var addaDet = "";var age = '';
	var uhid = "";
	$.ajax({
		type : "POST",
		url :  serverPath + "/getPatientDetails?patientId="+patientId,
		headers: {
	        "deviceId":"ADMIN",
	    },
	success : function(data){
	if(data.code=="200"){
        var option = data.content;
		var patName = "";
				if(option.patientFirstName !=null){
					patName+=option.patientFirstName;
				}
				if(option.patientLastName !=null){
					patName+=" "+option.patientLastName;
				}
				if(option.bloodGroup !=null){
					other = option.bloodGroup;
					
				}if(option.gender =="Male"){
					gender = '<img src="resources/images/male.png" style="width:12px">';
					
				}if(option.gender =="Female"){
					gender = '<img src="resources/images/femenine.png" style="width:12px">';
					
				}if(option.mobileNo !=null){
					mob = option.mobileNo;
				}
				if(option.address !=null){
					address = option.address;
				}
				if(option.emailId !=null){
					email = option.emailId;
				}
				if(option.uhid != null){
				    uhid = option.uhid;
				}
				age = option.age;

			$('#addPrfName').html(patName);
			$('#uhidId').html(uhid);
			 addaDet+='<li class="list-group-item text-center" style="margin-bottom:0px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 0px; padding-bottom: 0px;font-size:12px">'+
				'<b>'+age+' / '+other+' / '+gender+' </b>'+ 
				'</li>';
			if(mob!=""){
				 addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 0px; padding-bottom: 0px;font-size:12px">'+
					'<b>'+mob+' </b>'+ 
					'</li>';
			}if(address!=""){
				 addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 0px; padding-bottom: 0px;">'+
					'<b>'+address+' </b>'+ 
					'</li>';
			}if(email!=""){
				 addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 0px; padding-bottom: 0px;">'+
					'<b>'+email+' </b>'+ 
					'</li>';
			}	
			
			$('#addDet').html(addaDet);
	}if(data.errorCode=="1010"){
		$('#load2').hide();
    	toastr.warning("Server Error");
	}
		}
	});
}
/* <li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">
<b>71 / <img src="resources/images/male.png" style="width:12px">  </b> 
</li>
<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">
<b id="addMobile">9900864016/9998984932</b>
</li>
<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">
<b id="addMobile">#653,  2nd cross
	Vidya nagar
	Banglore 					
</b>
</li>
<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">
<b><img src="resources/images/veg.png" style="width: 20px; margin-right: 3px;" > <img src="resources/images/cigarette.png" > <img src="resources/images/glass.png"></b>
</li> */
	
var histId = "";
	function getHistoryOfUser(id){
		//alert(id);
		if(id != null){
		if(id == $(".isActive").attr('id')){
			$(".prevDis").css('display','inline-flex');
			$("#displDis").prop('disabled', false);
		}else{
			$(".prevDis").css('display','none');
			$("#displDis").attr("disabled","disabled");
		}
		}
		getAdviceDeatils();
		
		histId = "";
		
		if(id == null || id == "undefined"){
			//alert(episodeId);alert(episdeId);
			$("#refreshId").hide();
			histId = episodeId;
		}else{
			
			histId = id;
			//jk
			episdeId=id;
			
			$(".normalCol").css("background","#B8B8B8");
			//$("#id").remove("class","btn-primary");
			$("#"+id).css("background","#367fa9");
		}
		//alert(histId);
	 getEpisodeVitals(histId);
	 getVitalDocument(histId);

	$("#prescriptionTableBody").html('');$("#notesTableBody").html('');
	$("#adviseTableBody").html('');$("#reportsTableBody").html('');
	var trStart = '<tr style="width:100%;">';
	var trEnd = '</tr>';
	
	
		$.ajax({
			type : "POST",
			url : serverPath+"/getDetailsByEpisodeIdv3?EpisodeId="+histId,
			headers: {
		        "deviceId":"ADMIN",
		    },
		    dataType: "json",
			type: "GET",
			success : function(data){
				$("#prelTTableBody").html('');
				if(data.code=="200"){
					//style="width:50%"	
					var presList = "";var adviseList ="";var notesList = "";var reportList="";var reporfilename='';
					//For Prescription
					$.each(data.content,function(index,option){
						
						if(option.documentTypeId == 1){
							//presList += trStart;
						if(option.medicationDetails != null && option.medicationDetails != 0){
						
					$.each(option.medicationDetails,function(index1,option1){
						
							//var obj = jQuery.parseJSON(option1.details);
							if(option1.medicationName !=''){
								
								var MedName = option1.medicationName;
							}
							if(option1.frequency !=''){
								
								var Freq = option1.frequency;
							}
							if(option1.strBeginDate !=''){
								
								var Start = option1.strBeginDate;
							}
							
							if(option1.totalNoOfDays != null && option1.totalNoOfDays > '1' && option1.totalNoOfDays != '0'){
								
								var Days = option1.totalNoOfDays +'days';
							}
							if(option1.totalNoOfDays != null && option1.totalNoOfDays <= '1' && option1.totalNoOfDays == '1'&& option1.totalNoOfDays != '0'){
								
								var Days = option1.totalNoOfDays +'day';
							}

							
							presList+='<tr class="viewMed"><td><a>'+MedName+'</a></td><td style="text-align:center">'+Freq+'</td><td>'+Start+'</td><td>'+Days+'</td></tr>';
							
							/* var forum = obj.additionalDetail;
							 if(option.documentTypeName == "Prescription"){
					        	
					        } strBeginDate
							 for (var i = 0; i < forum.length; i++) {
							    var object = forum[i]; 
							    
							    for (property in object) {
							        var value = object[property];
							        //console.log(value.keyName+"--"+value.value);
							        
							        if(value.keyName == "medicationName"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'</td>';
								        }
							        if(value.keyName == "frequency"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        if(value.keyName == "beginDate"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'</td>';
							        } 
							        if(value.keyName == "Days"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'Days</td></tr><tr>';
							        }
							    }  
							   // presList += trEnd;	
							} 
							//alert(option.documentTypeName);
							if(option.documentTypeName == "Prescription"){
					       	presList += trEnd;						        	
					        }*/
						    $("#prescriptionTableBody").html(presList);
							
						
						});
						}
						} 
					
					//For Advise/Investigation
					
					if(option.documentTypeId == 2){	
						
						//alert(option.investigationDetails);
					if(option.investigationDetails != null && option.investigationDetails != 0){
						var LabCom = '';var CrPB = '';var atach = '-';
					$.each(option.investigationDetails,function(index2,option2){
						//alert(option.documentTypeId);
						
							/* var obj = jQuery.parseJSON(option.details);
							var forum = obj.additionalDetail;
							if(option.documentTypeName == "Investigation"){
					        	adviseList += trStart;
					        }
							for (var i = 0; i < forum.length; i++) {
							    var object = forum[i];
							    
							    for (property in object) {
							        var value = object[property];
							        //console.log(value.keyName+"--"+value.value);
							        if(value.keyName == "servicesName"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        if(value.keyName == "prescribedBy"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        if(value.keyName == "comments"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        if(value.keyName == "createdAt"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        
							    }  
							}
							if(option.documentTypeName == "Investigation"){
								adviseList += trEnd;						        	
					        } */
					        
						if(option2.serviceName !=''){
							
							var SerName = option2.serviceName;
						}
						if(option2.investigationType == 'Report'){
							if(option2.result == '1' && option2.result != '0'){
								CrPB = option2.investigationType+'<h style="font-size:12px;">(Positive)</h>';
							}else if(option2.result == '0' && option2.result != '1'){
								CrPB= option2.investigationType+'<h style="font-size:12px;">(Negative)</h>';
							}
							}else if(option2.investigationType == 'Advice'){
								CrPB= option2.investigationType;
							}
						if(option2.labName !=''){
							
							 LabCom = option2.labName;
						}else{
							
							LabCom = '';
						}
						if(option2.strPrescribedDate !=''){
							
							var CrDate = option2.strPrescribedDate;
						}
						
						
						if(option.filePath != null){
						var cnt = 0;$("#viAtImg").empty();	var imgInv = '';						
						$.each(option.filePath,function(index,optionF){
								cnt++;
								//alert(optionF.filePath);
						imgInv = '<input type="hidden" class="inves'+option.id+'" value="&quot;'+optionF+'&quot;">';
						$("#viAtImg").append(imgInv);
						//atach = cnt+"document attached";
						atach = '<i class="fa fa-file-text" aria-hidden="true"></i>';

							});
						//alert(atach);
							
						}
					/* if(option2.prescribedBy !=''){
							
							CrPB = option2.prescribedBy;
						}else{
							
							CrPB = '';
						} */
						
						/* if(option2.totalNoOfDays != undefined || option1.totalNoOfDays != ''){
							
							var Days = option1.totalNoOfDays;
						} */
 
/* <td style="cursor:pointer;" onclick="viewInvesti('+option.id+');">'+atach+'</td> */ //attachment td
 						adviseList+='<tr class="viewInv"><td width="30%";><a>'+SerName+'</a></td><td style="">'+CrPB+'</td><td >'+LabCom+'</td><td>'+CrDate+'</td></tr>';
						
						    $("#adviseTableBody").html(adviseList);
					
 				
					});
					}
					}
					
					//For Notes
					if(option.documentTypeId == 8){	
						//alert(option.notesDetails);
					if(option.notesDetails != null && option.notesDetails != 0){
						
					$.each(option.notesDetails,function(index3,option3){
						//alert(option.documentTypeId);
						/* if(option.documentTypeId == 8){
							var obj = jQuery.parseJSON(option.details);
							var forum = obj.additionalDetail;
							if(option.documentTypeName == "Notes"){
								notesList += trStart;
					        }
							for (var i = 0; i < forum.length; i++) {
							    var object = forum[i];
							    
							    for (property in object) {
							        var value = object[property];
							        //console.log(value.keyName+"--"+value.value);
							        
							        if(value.keyName == "comments"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	notesList += '<td style="width:85%">'+value.value+'</td>';
							        }
							        if(value.keyName == "addedTime"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	notesList += '<td style="width:15%">'+value.value+'</td>';
							        }
							         
							    }  
							}
							if(option.documentTypeName == "Notes"){
								notesList += trEnd;						        	
					        } 
						    $("#notesTableBody").html(notesList);
						 } */
						if(option3.strBeginDate !=''){
							
							var NotDate = option3.strBeginDate;
						}
						 
						if(option3.comments !='' && option3.comments != undefined && option3.comments!= ','){
							
							var Note = option3.comments;
						}
						if(option3.createdBy != null){
							
							var notCB = option3.createdBy;
						}
						/* if(option3.createdOn != null){
							
							var notCO = option3.createdOn;
							var now = notCO;
							alert( now.customFormat( "#DD#/#MM#/#YYYY# #hh#:#mm#:#ss#" ) );
							//alert(datFor);
						}  */
						//alert(notCB);
						notesList+='<tr><td width="40%";>'+Note+'</td><td class="text-left" width="30%";>'+NotDate+'</td></tr>';
						
						$("#notesTableBody").html(notesList);
					});
				}
			}
					
					//For Report
					/* if(option.documentTypeId == 9){	
						
						if(option.filePath != null){
							//alert();
							var	x =  option.filePath +'';
							//alert(x);
							var splt = x.split("/");
							var splRep = splt[splt.length-1];
							//alert(splRep);
							reportList += '<tr><td style="cursor:pointer;" "width:100%" onclick="openFileLocation(&quot;'+x+'&quot;);">'+splRep+'</td></tr>';
						
						}
			
						
				$("#reportsTableBody").html(reportList);
				
					} */
					
					//For Diagnosis
					
					if(option.documentTypeId == 10){
						diagnoList = '';
						//alert(option.investigationDetails);
					if(option.diagnosisDetails != null && option.diagnosisDetails != 0){
						var LabCom = '';var CrPB = '';
					$.each(option.diagnosisDetails,function(index2,option2){
						//$("#prelTTableBody").html('');
					        
						if(option2.diagnosisType !=''){
							
							var SerName = option2.diagnosisType;
						}
						if(option2.comments !=''){
							
							
							 LabCom = option2.comments.split(',');
							 // alert(LabCom[0],LabCom[1],LabCom[2]);
							 //alert(LabCom);
						}else{
							
							LabCom = '-';
						}
						if(option2.strBeginDate !=''){
							
							var CrDate = option2.strBeginDate;
						}
						var tr = '';
						for(var i=0;i<LabCom.length;i++){
							if(LabCom[i] != ''){
						tr= "<tr><td class='valId'>"+LabCom[i]+"</td></tr>";
						$("#prelTTableBody").append(tr);
							}
						}
						
						//console.log(tr);
						
						/* $("#prelTTableBody").append('<tr><td width="30%"; class="valId">'+LabCom[0]+'</td><td width="30%"; class="valId">'+LabCom[1]+'</td> '+
						'<td width="30%"; class="valId">'+LabCom[2]+'</td></tr>');
						 */
						
						
						
						/* <td style="">'+CrPB+'</td><td >'+CrDate+'</td><td>'+LabCom+'</td> */
						
						   // console.log(diagnoList);
 				
					});
				//	$("#prelTTableBody").html(diagnoList);
					}
					}
					
				//alert(reportList);
				 });
		
				}
				
			}
		});
	}

	function setFileName(input){
		//alert($(input).val());
		var file= $(input).val().substring($(input).val().lastIndexOf("\\") + 1, $(input).val().length);
		//alert(file);
		$("#showReportFileName").text(file);
	}
	function openFileLocation(filePathLocation){
		//alert(filePathLocation);
		window.open(imagePath+filePathLocation);
	}

	function initiateCall(calleProfileId){
		var proId = localStorage.getItem("productId");
    	if(proId == 1310){
    		alert("This Service is not enabled, Please contact Wenzins");	
    	} else {
        	var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
         	win.addEventListener("resize", function(){
        		win.resizeTo(1024, 768);
         	});
		}
	}
	
	function canMed(){
		
		$("#addMedicationModal").modal('hide');
		 //dosageID startId days commentsMedId
		 $(".bClassM").val('');
		 $(".bClassM").parent().removeClass('is-dirty');
		 $('#fequencyId').val('0-0-1');
		$('#instructionId').val('Before Food');
		$("#dynTableMed").hide();
		$("#medAdd").empty();
		$("#masterfequencyType").val("MG");
		$("#dosageID").val("");
		$("#dosageHiddenId").val("");
		 /* $("#startId").val('');
		 $("#dosageID").val('');
		 $("#days").val('');
		 $("#commentsMedId").val(''); */
		 
	}
	function canNote(){
		//alert();
		$("#addNotesModal").modal('hide');
		//$('.getNotes').text('');
		$('textarea').val('')
	}
	function clearRep(){
		$("#addReportModal").modal('hide');
		$('#reportFileId').val('');
		$(".getReport").empty();
		$('#showReportFileName').text('');
	}
	function reportAdvice(episodeId,docId,id)
	   {
		// alert("Fdgfg");
		  var appendPrescriptionImage = ""; 
		  if(docId==1){
			  $("#addreport").html('');
			  $(".preReport").each(function(){
				  
				  var spltAttr = $(this).attr("attr");
				  if(spltAttr==id){
				  var splitHere =  $(this).val().split("/");
				  var splitHere1 = $(this).val().split(","); 
				  
               appendPrescriptionImage+= "<br><a class='text-left' path='&quot;"+splitHere1+"&quot'' "
               +"style='cursor:pointer;padding-top:0px; padding-bottom: 5px;color:rgb(63, 81, 181);"
               +"font-size: 15px;' onclick='javascript:showInOtherPage("+id+",&quot;"+splitHere1+"&quot;)' "
               +" >"+splitHere[splitHere.length-1]+"</a>";
				  }
 		  });
			  
			  $("#addreport").html(appendPrescriptionImage);
			  
			 
			  if(appendPrescriptionImage==""){
 			  
			    mes = "No records found";
				$('#demo-show-snackbar').trigger('click');
		  }else{
				$("#reportAdvice").modal('show');
		  }
		  }
		  
		
		  if(docId==2){
			  $("#addreport").html(''); var appendPrescriptionImage = ""; 
			  $(".repReport").each(function(){
				  var spltAttr = $(this).attr("attr");
				  if(spltAttr==id){
				  var splitHere =  $(this).val().split("/");
				  var splitHere1 = $(this).val().split(",");
				  
				  
				  
               appendPrescriptionImage+= "<br><a class='text-left' path='&quot;"+splitHere1+"&quot'' "
               +"style='cursor:pointer;padding-top:0px; padding-bottom: 5px;color:rgb(63, 81, 181);"
               +"font-size: 15px;' onclick='javascript:showInvestigationImg("+id+",&quot;"+splitHere1+"&quot;)' "
               +">"+splitHere[splitHere.length-1]+"</a>";
				  }
				  
				  
				  
 		  });
			  
			  $("#addreport").html(appendPrescriptionImage);
			  
			  if(appendPrescriptionImage == ""){
				  console.log(appendPrescriptionImage);
				    mes = "No records found";
   				$('#demo-show-snackbar').trigger('click');
   		  }else{
   				$("#reportAdvice").modal('show');
   		  }
			
		  }
		
		  
}
	var check = false;
	function completeConsultation(){
		//alert(appointmentId);
		/* var form1 = document.getElementById('adviceForm');
		var formData = new FormData(form1); */
		//alert($(".viewMed").text());
		
		check = false;
		$(".viewAdvice").each(function(){
			
			if($(this).text() != ''){
				check = true;
				if($(".viewMed").text() == '' && $(".viewInv").text() == ''){
					toastr.warning("Prescription and Pre-Diagnosis aren't added");
				    
				}
			}
		});
		if(check){
			
		$.ajax({
		   type:"POST",
		   url: serverPath+"/completeConsultation?episodeId="+episodeId+"&appointmentId="+appointmentId,
		   data:$("#adviceForm").serialize(),
		   success: function(data){
			   if(data.code == '200'){
				   //alert(data.message);
				  
				   toastr.success("Appointment Completed Successfully");
				   window.location.href= 'list';
				   
			   }
			   
		   },error: function(response){
				toastr.error('Server/network not reachable', 'Error');
			}
		}); 
		}else{
			$("#mandExId").show();
			toastr.warning("Please Fill Advice of Consultation Summary");
		}
	}
	
	function changedSE(){
		//alert($(this).children("option:selected").val());
		
		//$("#diagnosisComment").show();
		$("#footId").show();
	}
	
	function viewRefresh(){
		$("#refreshId").show();
	}
	
	/* var bValue = false;
	function checkDiagno(name){
		console.log(name);
		//alert(name);
		$(".valId").each(function(index,option){
			//alert($("#ses").val());
			if($(this).text() == name){
				bValue = true;
				return false;
				
			}else{
				
				bValue = false;
			}
		});
		 if(bValue == true){
			toastr.warning($("#ses").val() + 'is already added');
			return false; 
		}else{
			
			diagnosisValidation();
		} 
		
	} */
	
	var prefixStrForDiagnosis = '';
	var additionalDetailJson =  '';var additonalDetails = '';
	var diagnosisJSON = [];var diagnosisFinalJSON = [];
	function diagnosisValidation(){
		
		/* if($("#diagnosisComment").val() != '' && $("#ses").val() != -1){ */
			
			additionalDetailJson =  '';additonalDetails = '';
			diagnosisJSON = [];diagnosisFinalJSON = [];
			var details = '';
			$("#diagnoEpiId").val(episdeId);
			//$(".diagnoJSON").each(function(){
				var comment = "";var cnt = 0;
				 $('.minimal:checked').each(function () {
					cnt++;
					if(cnt>1){
						comment+=","+$(this).val();
					}else if(cnt == 2){
							
					}else if(cnt == 1){
					 comment+=$(this).val();	
					}
					//alert(comment);
       }); 
				 
					
					
				if(comment != '' && comment != 'undefined'){	
					
				diagnosisJSON.push({keyName : "Comment",value:comment});
				diagnosisJSON.push({keyName : "DiagnosisType",value :"Preliminary-Diagnosis" });
				diagnosisJSON.push({keyName : "id",value: 0 });
				diagnosisJSON.push({keyName : "status",value: 1 });
				//diagnosisJSON.push({keyName : "medicalReportId",value: -1 });
				
				diagnosisFinalJSON.push(diagnosisJSON);
				
				
				diagnosisJSON=[];
		}else{
			toastr.warning("Please Select Data Before Saving");
		}
			console.log(diagnosisFinalJSON);
			if(diagnosisFinalJSON.length > 0){
				//alert('empty');
				prefixStrForDiagnosis='{\"additionalDetail\":';
				additonalDetails = prefixStrForDiagnosis+JSON.stringify(diagnosisFinalJSON)+"}";
	 	        details = $("#diagnoForm").val(additonalDetails);
	 	        console.log(details);
	 	       diagnosisJSON = [];diagnosisFinalJSON = [];
		
			console.log(details);
			
			var form1 = document.getElementById('saveDiagnosisForm');
	 		 var formData = new FormData(form1);
	 		
			$.ajax({
				type : "POST",
	 			url : serverPath+"/addDocuments",
	 			headers: {
	 		        "deviceId":"ADMIN",
	 		    },
	 			data: formData,
	 			dataType:"json",
	 		    mimeType: "multipart/form-data",
	 			async : false,
	 		    cache : false,
	 			contentType : false,
	 		    processData : false,
	 			success : function(data){
	 				if(data.messageCode == '200'){
	 					
	 					toastr.success("Premilinary diagnosis added Successfully");
	 					
	 					clearAllValues();
	 					getHistoryOfUser(data.message);
	 				}else if(data.errorCode == '1012'){
	 					toastr.error('Server/network not reachable', 'Error');
	 				}
	 			},error: function(response){
					toastr.error('Server/network not reachable', 'Error');
				}
			});  
		 } 
		
	}
	
	function saveAdvice(){
		//("#dropdownid option:selected").text(); 
		//alert($("#referId option:selected").text());
		
		/* var form1 = document.getElementById('saveAdviceForm');
		var formData = new FormData(form1); */
		
		if($("#genAdv").val() != ''){
		
		$.ajax({
			type:"POST",
			url:serverPath + "/saveCCPAdviceDetails?adviceStatusId=1&ccpEpisodeId="+ccpEpisodeId+"&serviceName="+$("#referId option:selected").text(),
			data: $("#saveAdviceForm").serialize(),
			success : function(data){
				if(data.code == '200'){
					
					toastr.success("Advice Added successfully");
					$("#genAdv").val('');
					$("#addAdviceModal").modal('hide');
					getAdviceDeatils();
				}
				
			},error: function(response){
				toastr.error('Server/network not reachable', 'Error');
			}
		});
	}else{
		toastr.warning("Please Add Advice");
	}
			
	}
	
	function getAdviceDeatils(){
		//alert();
		$.ajax({
			type:"POST",
			url:serverPath+"/getCCPAdviceList?ccpEpisodeId="+ccpEpisodeId,
			success:function(data){
				if(data.code == '200'){
					
					var serviceName = '';var Advice = '';var adviseList='';var diagnoList = '';
					if(data.content != null && data.content != ''){
						$("#adviseTTableBody").html('');			//$("#viewPrevAdviceId").show();
						$.each(data.content,function(index,option){
							 
							if(option.serviceId != null && option.serviceId == '2'){
								serviceName = 'Referred For'+' Local';
							}else if(option.serviceId != null && option.serviceId == '3'){
								serviceName = 'Referred For'+' Abroad';
							}
							if(option.comments != null && option.comments != ''){
								
								Advice = option.comments;
								
							}
							
							adviseList+='<tr><td title="'+Advice+'" class="viewAdvice" style="text-transform:capitalize;">'+Advice+'</td><td val='+option.serviceId+' style="">'+serviceName+'</td></tr>';
							$("#adviseTTableBody").html(adviseList);
							//$("#genRefID").html();
						});
						
					}
					
				}
			},error: function(response){
				toastr.error('Server/network not reachable', 'Error');
			}
		});
		
	}
	function viewAddAdv(){
		$("#mandExId").hide();
		$("#addAdviceModal").modal();
	}
	function viewAddDiagn(){
		
		$("#addDiagnosisModal").modal();
		getMasterDiagnosis();
	}
	
	
function viewAddRe(){
		
		$("#addRevisitModal").modal();
		revisitList();
	}
	
	function clearInvModal(){
		
		$("#adviceOptId").attr("checked",true);
		//$("#showResultDivId").hide();
		$("#addReportModal").modal('hide');
		$('.Newpolic2').val('');
		$('.invClear').val('');
		$("#invAdd").empty();
		$("#invData").val('');
		$("#addFilesRepo").empty();
		$(".addedInvTable").hide();
		$("#firstinvfile").val('');
		$(".validInv").empty();
		$("#additionalDetailJsonInv").val('');
		$(".visitTClassId").val('Out Patient');
		}
	
	function shoeReport(){

		$("#showResultDivId").show();
		
	}
	
	var count=0;
	var cnt=0;
	 function getOtherInvFile(){
			
	     var checkInv = false;
	     
	    if($("#firstinvfile").val() == ''){
	    	//alert($("#firstinvfile").val());
	    	
	   	  checkInv = true;
	     }

	     $('.validInv').each(function(){
				
	 			if($(this).val() == ''){
	 				
	 				checkInv = true;
	 			}
	     });
	 			if(checkInv){

	 			toastr.error("Please attach a file");

	 			}else{
	 				
	 				cnt++;
	 		    	//alert(cnt);
	 		    	
	 		    	if(cnt <= 4	){
	 					  var element2 = '<p style="display:inline-flex;margin-bottom: 5px;width: 94%;" ><i class="fa fa-paperclip" style="padding-top: 5px;margin-left: 0px;padding-right: 10px;color:black"></i> '
	 			              +'<input type="file" name="files" class="validInv" onchange="acceptInvType(this)" style="margin-bottom: 0px; border-radius: 4px; font-size:15px;color:royalblue;border:royalblue 1px solid">'
	 			              +'<i style="cursor: pointer; padding-top: 7px;color:black" class="fa fa-times plustime" onclick="removeFiles(this)"></p>';
	 			         

	 			          $("#addFilesRepo").append(element2);
	 		    			}else{
	 		    				toastr.warning("Exceeding attachment limit");
	 		    			}
	 					}
	 				
	    
	 }
	 
	 function clearFiles(){
		 	$('#firstinvfile').val('');
		 	$('#addFilesRepo').empty();
		 	
		 	$('#validInv').val('');
		 }
	 
	 function acceptInvType(ob){
			if(ob.files[0].type == "image/jpeg" || ob.files[0].type == "image/jpg" || ob.files[0].type =="application/x-download" ||
					ob.files[0].type == "application/vnd.ms-excel" || ob.files[0].type =="application/pdf" 
					|| ob.files[0].type =="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || ob.files[0].type =="image/png"){
			       }else{
						$(ob).val('');
						toastr.error("File format not supported");
						}
			        }
	 
	 function removeFiles(ob){
	  	$(ob).parent().remove();
	 }
	 
	 function addInvestigation(){
		 
		 $("#invEpisodeId").val(episdeId);
		 
			var valDateTime = $("#reportDate").val();
			
	     var  valInvNameId= $("#investigationNameIdFromDropdown").val();
      var valLabNameId= $("#labNameIdFromDropdown").val();
			
			var valLabName= $("#labNameId").val();
			var valInvName = $("#investigationNameId").val();
			var valStatus = $("#status").parent().val();
			var valInvestigationType = $("input[name='investigationType']:checked").val();
			
			//alert(valInvestigationType);
			 var masterService = $('#fequencyType').val();
			 var splitt = masterService.split("~");
			 //var val = splitt[1];
			 var val = valInvestigationType;
			var valRepo = '';
			var valRep = '';
			
			 if(val=="Advice"){
				 valRepo=0;
			}else  if(val=="Report"){
				invesNameType="report";
				var term = $("#resultTypeId").val();
				//alert(term);
				 if(term == 1){
					 valRepo = 1;
					 }else{
					 valRepo = 0;
						 }

				}
			
			var valComm = $("#invcomments").val();
			
			if(valDateTime != '' && valInvName != '' && valLabName != ''){
		 
			$(".addedInvTable").show();
			
			var invData = '<tr><td style="float:left;" class="getJson1" idAttr="'+valInvNameId+'" attr="investigationName">'+valInvName+'</td>'
			+'<td style="text-align:left;" class="getJson1" attr="InvestigationType">'+val+'</td>'
		    +'<td style="text-align:left;" class="getJson1" attr="Labcomments">'+valComm+'</td> '
		    +'<td style="text-align:left;display:none;" class="getJson1" attr="Result">'+valRepo+'</td> '
			+'<td style="text-align:left;"><i style="cursor:pointer" class="fa fa-times getJson1" attr="last" aria-hidden="true" onclick="removeFileMed(this)"></i></td></tr>';
			$("#invAdd").append(invData);
			$('.invRemAAdd').val('');
			$('.Newpolic2').parent().removeClass('is-dirty');
			$("#adviceOptId").attr('checked', true);
			$("#showResultDivId").hide();
	 			}else{
	 				//alert();
	 				 toastr.warning("Please Fill Mandatory Fields");
		 			}
			}
	 
	 var investigationJSON = [];
		var InvestigationFinalJSON = [];
		prefixStrForInvestigation='{\"additionalDetail\":'; 
		 var epParaId = '';
	function saveInvestigation(){ 
		
		investigationJSON = [];
		InvestigationFinalJSON = [];
		 var ordrinv=0;
	
		 var masterService = $('#fequencyType').val();
		 
		 var splitt = masterService.split("~");
		 var splitmasterService = splitt[0];
		 $("#masterServiceSplitId").val(splitmasterService);
		// $("#additionalDetailJsonInv").html('');
		
		 $(".getJson1").each(function(){

		
			if($(this).attr("attr") == "InvestigationType"){
				investigationJSON.push({keyName: $(this).attr("attr"),value: $(this).text()});
				
				}
			if($(this).attr("attr") == "investigationName"){
				investigationJSON.push({keyName: $(this).attr("attr"),value: $(this).text()});
				
				}
			if($(this).attr("attr") == "investigationName"){
				investigationJSON.push({keyName: "investigationId",value: $(this).attr("idAttr")});
				
				}
	
			if($(this).attr("attr") == "Result"){
				investigationJSON.push({keyName: $(this).attr("attr"),value: $(this).text()})
				}
			if($(this).attr("attr") == "Labcomments"){
					investigationJSON.push({keyName: $(this).attr("attr"),value: $(this).text()})
					}
			if($(this).attr("attr") == "last"){
				
				investigationJSON.push({keyName : "id",value : 0 });
				investigationJSON.push({keyName : "status",value : 1 });
				/* investigationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDateInv').val() });
				 */investigationJSON.push({keyName : "medicalReportId",value : -1 });
				InvestigationFinalJSON.push(investigationJSON);
				
				investigationJSON=[];

				 
			}
		//	}
			ordrinv++;	
			
		 });
		
		$('#dynTableInv').show();
			 
			  if(InvestigationFinalJSON.length > 0){
			        var additonalDetails = prefixStrForPrescription+JSON.stringify(InvestigationFinalJSON)+"}";
			        $("#additionalDetailJsonInv").val(additonalDetails);
			        //console.log("additonalDetails"+additonalDetails);
			    } 

			  data = $("#additionalDetailJsonInv").val();

		 	  if(data != null && data != ''){
		 		  
				 var form1 = document.getElementById('saveLabModId');
				 var formData = new FormData(form1);
					
				$.ajax({
					type : "POST",
					url : serverPath + "/addDocuments",
					headers: {
				        "deviceId":"ADMIN",
				    },

					data: formData,
					dataType:"json",
				    mimeType: "multipart/form-data",
					async : false,
				    cache : false,
					contentType : false,
				    processData : false,
					success : function(data){

						if(data.messageCode=="200"){
							 $("#loaderGif-HOSEmp").hide();
							var episodeId = localStorage.getItem("episodeIde");
							
							clearInvModal();getHistoryOfUser(data.message);
							//viewEpisodeDetails(latestEpisodeId);
							additonalDetails = '';
							toastr.success("Report saved successfully");
							
						}
						else if(data.errorCode=="1012"){
							 $("#loaderGif-HOSEmp").hide();
							mes= "Server error, please try again";
							$("#demo-show-snackbar").trigger('click');
						}
					},error: function(response){
						
						toastr.error('Server/Network not reachable','error')
					}
				});
		 	  }else{
		 		 $('#dynTableInv').hide();
		 		toastr.warning("Please Add Mandatory Data");
				  
			 	  }
			}
			
			function viewCalendar(){
				
				$("#selectDaId").show();
			}
			var comments = '';
			function revisitValidation(){
				comments = '';
				
				$("#revisitEpiId").val(episdeId);
				//alert( $("#datetimepicker1").val());
				if($("#media").val() != -1 || $("#datetimepicker1").val() != ''){
					//alert($("#media").val());
					
					if($("#media").val() != '-1'){
						var data = $("#media").val();
						comments = 'Revisit after '+ data;
						
					}else if($("#datetimepicker1").val() != ''){
						var data1 = $("#datetimepicker1").val();
						comments = 'Revisit On '+ data1;
					}
					
				$.ajax({
					type:"POST",
					url: serverPath + "/saveRevisitDetails?comments="+comments+"&adviceStatusId=9&ccpEpisodeId="+ccpEpisodeId,
					success:function(data){
					
						if(data.code == '200'){
							toastr.success("Re-Visit Saved Successfully ");
							getAdviceDetails();
							clearAllRevisit();
							
						}else if(data.errorCode == '1012'){
		 					toastr.error('Server/network not reachable', 'Error');
		 				}
					},error: function(response){
						toastr.error('Server/network not reachable', 'Error');
					}
				}); 
			}else{
				toastr.warning("Please Enter Re-Visit Date ");
			}
				
			}
			
	function getMasterDiagnosis(){
		var val = '';$("#ses").html('');
		$.ajax({
		   type:"POST",
		   url:serverPath + "/getDiagnosisList",
		   success:function(data){
			   if(data.code == '200'){
				   //alert(data.code);
				   if(data.content != null){
					   $.each(data.content,function(index,option){
						   
		val = '<label style="padding-right: 15px;margin-right: 10px;"><input type="checkbox" id="'+option.id+'" onclick="checkResu('+option.id+',&quot;'+option.value+'&quot;);" class="minimal" value="'+option.value+'" /><h style="padding-left: 5px;">'+option.value+'</h> '+
				 ' </label>';
	                      
						   $("#diagnoListId").append(val);
					   });
					   
					   
					   
				   }
			   }else if(data.errorCode == '1012'){
					toastr.error('Server/network not reachable', 'Error');
				}
			   
		   },error: function(response){
				toastr.error('Server/network not reachable', 'Error');
			}
			
		});
	}
	
	function checkDigit(){
		//console.log('clicked');
		var pattern =new RegExp('[a-zA-Z]');

			var qry = $("#dosageID").val();
			if(qry.match(pattern)) {
				toastr.error("Please Add Valid Dosage");
				$("#dosageID").val('');
			   // alert('invalid');
			}
	}
	
function checkDays(){
		//alert();
		var pattern =new RegExp('[a-zA-Z]');

			var qry = $("#days").val();
			if(qry.match(pattern)) {
				toastr.error("Please Add Valid Days");
				$("#days").val(''); 
			}else if($("#days").val() > 90){
				
				toastr.warning("Days should not exceed 90 days");
				$("#days").val('');
			}				
			
	}
	
	var appendMod = '';
	function viewInvesti(id){
		appendMod = '';//alert(('.inves'+id));
		 $("#attachBod").empty();	
		$('.inves'+id).each(function(){
			
			var str = $(this).val().substring(1, $(this).val().length-1);
			//console.log(str);
			var splitHere =  str.split("/");
			//alert(splitHere);
			var splitHere1 = str.split(",");
			//alert(splitHere1);
			appendMod+= "<a class='text-left' path='&quot;"+splitHere1+"&quot'' "
            +"style='text-decoration: underline;cursor:pointer;padding-top:0px; padding-bottom: 5px;color:rgb(63, 81, 181);"
            +"font-size: 15px;' onclick='javascript:showInvestigationImg("+id+",&quot;"+splitHere1+"&quot;)' "
            +" >"+splitHere[splitHere.length-1]+"</a><br>";
            
            $("#attachBod").html(appendMod);		
			
		});
		
		if(appendMod == ""){
			  //console.log(appendImage);
			  toastr.warning('No document attached');
		  }else{
				$("#attachDoc").modal('show');
		  }
	 
	
	}
	
	function showInvestigationImg(id,image){
		//alert(imagePath);
		window.open(imagePath+ image, '_blank');
		
	}
	 var bValue = false;
	function checkResu(id,value){
		
		//alert(value);
		var cnt=0;
		$(".valId").each(function(index,option){
			//alert($(this).text());
			if($(this).text() == value){
				
				$("#"+id).prop("checked", false);
				$("#"+id).addClass('disabled');
				bValue = true;
				return false;
			}else{
				bValue = false;
				
			}
		});
		
		if(bValue == true){
			toastr.warning(value + " is already added");
			return false; 
		}
	}
	
	function clearAllValues(){
		
		$("#diagnoListId").empty();
		$("#diagnoForm").val('');
		$("#addDiagnosisModal").modal('hide');
	}
	
	var revisit = '';var comments = '';var splitCR = '';
	function getAdviceDetails(){
		revisit = '';splitCR = '';
		$.ajax({
			type:"POST",
			url:serverPath +"/getRevisitDetails?adviceStatusId=9&ccpEpisodeId="+ccpEpisodeId,
			success:function(data){
				if(data.code == '200'){
					$("#revisitBody").html('');
					
					if(data.content != null){
						$(data.content).each(function(index,option){
							
							if(option.comments != ''){
								var myJSON = JSON.parse(option.comments);
								comments = myJSON.comments;
							}
							
							if(option.strCreatedAt != null){
								
								splitCR = option.strCreatedAt.split(" ");
								//alert(split[0]);
							}
							
							revisit+='<tr><td title="'+comments+'" class="viewRevisit" style="text-transform:capitalize;width: 100%;">'+comments+'</td><td>'+splitCR[0]+'</td></tr>';			
							
							
						});
						$("#revisitBody").append(revisit);
						
						
					}	
				}
				
			},error: function(response){
				toastr.error('Server/network not reachable', 'Error');
			}		
			
		});
	}
	
	function clearAllAdvice(){
		
		$("#genAdv").val('');
		$("#referId").val('2');
		
	}
	
	function clearAllRevisit(){
		
		$("#media").val(-1);
		$("#datetimepicker1").val('');
		$("#addRevisitModal").modal('hide');
	}
	
	function revisitList(){
		var list = ''; 
		$.ajax({
		  type:"POST",
		  url:serverPath + "/getRevisitMasterList",
		  success: function(data){
			  
			  if(data.code == '200'){
				 list+= '<option value="-1" id="-1" selected="" >Select Days</option>';
				  $(data.content).each(function(index,option){
					 
					  list+= '<option value="'+option.value+'" id="'+option.id+'" >'+option.value+'</option>';
					 
				  });
				  $("#media").html(list);
				  //alert(data.content.value);
			  }else if(data.errorCode == '1010'){
				  toastr.warning("Server Error");
			  }
		  }
		  
		  
		});
		
	}
	
	
	function next(id){
		
		if(test==s-2){
			document.getElementById("nextId").disabled = true;	
		}
		
		
		document.getElementById("prevId").disabled = false;
			
		test=test+1;
		var previous=test-4;
			
			document.getElementById(previous).style.display = "none";
			document.getElementById(test).style.display = '';
			
			document.getElementById("prevId").style.display = "";           //to display prev button
		}
		
	
	function previous(id){
		
		document.getElementById("nextId").disabled = false;	
			
			 if(test==4){
				document.getElementById("prevId").disabled = true;
			} 
			
			var previous=test-4;
		
				document.getElementById(test).style.display = 'none';
				 document.getElementById(previous).style.display = ''; 
				
			test=test-1;
			}
	
	function calFunction(){
		//alert();
		if($("#media").val() != -1){
			
			$("#datetimepicker1").val('');
		}
	}
	
	function getDate(){
		
		if($("#media").val() != -1){
			//alert();
			$("#media").val('-1');
		}
	}
	/* function disable(){
		
		$(".prevDis").css('display', 'none');
		$("#displDis").attr("disabled","disabled");
		//$(".prevDis").addClass('isDisabled');
	} */
	
</script>
<!-- <tr>
	<td>4.</td>
	<td>Fix and squish bugs</td>
	<td>
 		<div class="progress progress-xs progress-striped active">
   			<div class="progress-bar progress-bar-success" style="width: 90%"></div>
 		</div>
	</td>
	<td><span class="badge bg-green">90%</span></td>
</tr> -->
</html>
