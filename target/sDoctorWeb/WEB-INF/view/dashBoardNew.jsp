<%@ include file="/WEB-INF/view/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title> DashBoard</title>

	<link rel="stylesheet" href="resources/css/bootstrap.min.css"/>
	
	
	<link rel="stylesheet" href="resources/css/toastr.min.css"/>
	<link rel="stylesheet" href="resources/css/bootstrap-datepicker.css">

	<link href="resources/css/AdminLTE.css" rel="stylesheet"/>
	<link rel="stylesheet" href="resources/css/material.css"/>
	<link href="resources/css/_all-skins.min.css" rel="stylesheet"/>
		<link rel="stylesheet" href="resources/css/font-awesome.min.css"/>
	    <script src="resources/js/jQuery-2.1.4.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/fastclick.min.js"></script>
    <script src="resources/js/app.min.js"></script>
    <script src="resources/js/demo.js"></script>
    <script defer src="resources/js/material.js"></script> 
    
    <script src="resources/js/bootstrap3-typeahead.js" 	type="application/javascript"></script> 
    <script src="resources/js/jquery.mask.js"></script>
    
     <script src="resources/js/toastr.min.js"></script>
     <script src="resources/js/customHTML.js"></script>
     <script src="resources/js/bootstrap-datepicker.js" type="application/javascript"></script>
    
    <style>

  
    .navbar-t-centered {
      position: absolute;
left: 34%;
display: block;
text-align: center;
color: #fff;
top: 0px;
padding: 15px;
font-weight: 400;
font-size: 12px;

    }
    @media screen and (min-width:768px){
.navbar-t-centered {
        position: absolute;
        left: 38% ;
        display: block;
       /*  width: 160px; */
        text-align: center;
        color: #fff;
        top:0px;
        padding:15px;
        font-weight: 400;
        font-size:22px;

    }
    }
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
	<div id="load2"
			style="position: fixed; left: 50%; top: 40%; width: 120px; height: 100%; z-index: 9999;display:none">
			<img style="" src="resources/images/icons/loading.gif" alt="loading" />
			<br />
		</div>
        <header class="main-header">
      <nav class="navbar navbar-static-top" role="navigation"
		style="margin-left:0% !important ;max-height:50px; background-color:#3c8dbc">
	<!-- Sidebar toggle button--> <img src="resources/images/logowhite.png"
		class="user-image" alt="User Image"
		style="height: 50px; padding-left: 5px; margin-left: 5px;">

	
		<span class ="navbar-t-centered" >Wrizto Healthcare System</span>
	
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->

              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="resources/images/avatar.png" class="user-image" alt="User Image">
                  <span class="hidden-xs" id="loggedInUserFullName"></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="resources/images/avatar.png" class="img-circle" alt="User Image">
                    <p><span id="loggedInUserFullNameSpan"></span>
                    </p>
                  </li>
                  <!-- Menu Body -->
                 <li class="user-body">
                 <ul class="col-xs-12 text-center" id="activeUsersId"> </ul>
                                  <!-- 
                                  <div class="col-xs-7 text-center"><span style="font-size: 12px; margin-left: -33px;" id="nurseNameForActiveId"></span>
                                  <a id= "videoIconId" style="border-radius: 50%; ;margin-left: 5px;background-color: green" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                  				   <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a>
                                  </div>
                                  <div class="col-xs-5 text-center">
                                    <a href="patientList">Patient List</a>
                                  </div>
                                   -->
                   </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                    </div>
                    <div class="pull-right">
                      <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="margin-left:0% !important;padding-bottom:40px;">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="display:inline-flex;width:100%;">
        
         <strong style="font-size:18px;" id="dash_id"></strong>
    

			<div id="chronologicalView" class="pull-right text-right" style="width:83%;height:40px;float:right">
        		
        	</div>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary ">
                <div class="box-body box-profile text-center" >
                  <img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">
                  <h5 class="profile-username text-center" id="addPrfName"style="margin-bottom: 15px;font-size:16px;"></h5>
                  <p class="text-muted text-center" id="uhidId" style="margin-bottom: 5px;"></p>

              <ul class="list-group list-group-unbordered" id="addDet">
                    
                  </ul>
 <div class="col-md-12" id="dashBoardHideShoeVideoIconId">
                   <a style="padding: 12px 12px 10px; border-radius: 50%;box-shadow: 4px 6px ;margin-left: -8px;" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                   <i style="color:white;font-size:20px" class="fa fa-video-camera"></i></b></a> 
                   <!-- <a style=" padding: 12px 12px 10px; border-radius: 50%;box-shadow: 4px 6px ;" class="btn btn-primary" title="Message"><b>
                   <i style="color:white;font-size:20px" class="fa fa-commenting"></i></b></a> -->
                   </div>
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#	" data-toggle="tab">Activity</a></li>
                  <li onclick="getHistoryOfUser(null);"><a href="#timeline" data-toggle="tab">Advice</a></li>
                 
                   <button style="float: right; cursor: pointer; margin-top: 5px;background:#3c8dbc" class="btn btn-primary" onclick="EmrReport()" >EMR Report <i class="" style="color:""></i></button>     
                   <button style="float: right; cursor: pointer; margin-top: 5px;background:white" class="btn" onclick="refreshPage()" >Refresh <i class="fa fa-refresh" style="color:black"></i></button> 
                 <!--  <li><a href="#settings" data-toggle="tab">Settings</a></li> -->
                </ul>
                
                <div class="tab-content">
                  <div class="active tab-pane" id="activity" style="padding-left: 10px; padding-right: 10px;">
                         
                         
                          <div class="user-block" style="padding-top: 10px; padding-bottom: 15px;">
                         <!--   <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image"> -->
                        <span class='username' style="margin-left: 0px;">
                          <a href='#' class='pull-right btn-box-tool'><!-- <i class='fa fa-times'></i> --></a>
																																																													                        </span>																	
                        <span class='description' style="margin-left: 0px; padding-top: 10px; padding-left: 5px;" id="vitDate"></span>
                      </div><!-- /.user-block -->
                      
                      
                      <div class="row" id="addVitalsDet">
                    
                      </div>
                  
                    
                    
                    
                    
                     <div class="user-block" style="padding-top: 10px; padding-bottom: 15px;">
                         <!--   <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image"> -->
                        <span class='username' style="margin-left: 0px;">
                          <a href="#" style="font-size:16px;color:#3a86bd;">Electrocardiogram (ECG)</a>
                          <a href='#' class='pull-right btn-box-tool'><!-- <i class='fa fa-times'></i> --></a>
                        </span>
                        <!-- <span class='description' style="margin-left: 0px; padding-top: 10px; padding-left: 5px;" id="addDates">NO RECORD FOUND</span> -->
                      </div><!-- /.user-block -->
                      
                      
                      
                  <div class="row" id="addEcgs">
     
                    
                  </div><!-- /.tab-pane -->
                  

                 
                </div><!-- /.tab-content -->
                
                <div class="tab-pane" id="timeline">
					<!-- SP EX -->
					<!-- <div class="row" id="addSpEx" style="margin-left: 0px;margin-right: 0px;">
					
					<div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <h6 class="box-title" style="font-size:14px">Specific Examination</h6>
                </div> --><!-- /.box-header -->
                
                <!-- <div class="box-body" style="padding-top: 0px;">
                  <ul class="todo-list ui-sortable">
                    <li>
                      drag handle
                     
                      <input type="checkbox" value="" checked="checked">
                      <span class="text">ECG</span>
                      
                      <input type="checkbox" value="" name="">
                      <span class="text">Dermatoscope</span>
                      
                      <input type="checkbox" value="" name="">
                      <span class="text">Retinoscope </span>
                      
                      <input type="checkbox" value="" name="">
                      <span class="text">Stethoscope</span>
                      
                      <input type="checkbox" value="" name="">
                      <span class="text">Otoscope</span>
                      
                    </li>
                  </ul>
                  <div class="row" style="display: block;margin-top:2%;" id="specificImages">
				<div class="col-sm-8">
				<canvas width="370" height="370" id="can" style="border:5px solid;cursor:grab;"></canvas></div>
				<img id="canvasimg" style="position: absolute; top: 10%; left: 52%; display: none;">
				</div>
				<div class="col-sm-4 colorshow" style="padding-left: 0px;">
				<div id="colors">
				<span style="float: left;">Choose Color</span><br>
				<div style="top: 50%; left: 45%; width: 20px; height: 20px; background: green;" id="green" onclick="color(this)"></div>
				<div style="top: 50%; left: 50%; width: 20px; height: 20px; background: blue; margin-top: -20px; margin-left: 25px;" id="blue" onclick="color(this)"></div>
				<div style="top: 50%; left: 55%; width: 20px; height: 20px; background: red; margin-top: -20px; margin-left: 50px;" id="red" onclick="color(this)"></div>
				<div style="top: 50%; left: 60%; width: 20px; height: 20px; background: black; margin-top: -20px; margin-left: 75px;" id="black" onclick="color(this)"></div>
				<div style="top: 50%; left: 65%; width: 20px; height: 20px; background: pink; margin-top: -20px; margin-left: 100px;" id="pink" onclick="color(this)"></div>
			   	<div style="top: 50%; left: 70%; width: 20px; height: 20px; background: orange; margin-top: -20px; margin-left: 125px;" id="orange" onclick="color(this)"></div>
			    </div>
				<br>
				<textarea type="text" class="form-control" style="display: block; resize: none;" id="ImagesDetails" placeholder="Comments"></textarea><br>
				</div>
				</div>
                  
                  
                </div> --><!-- /.box-body -->
                <!-- <div class="box-footer clearfix no-border">
                  <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                </div> -->
            <!--   </div>
					</div> -->
                    <!-- SP EX -->	
			              
			        <!-- Prelim Diagnosis --> 
			        
			      <!--   <div class="row" id="addPrDi" style="margin-left: 0px;margin-right: 0px;">
					
					<div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <h6 class="box-title" style="font-size:14px">Preliminary-Diagnosis</h6>
                </div>/.box-header
                
                <div class="box-body" style="padding-top: 0px;">
                 
                 <div style="position:relative;">
				<select id="ses" class="form-control " onchange="changedSE();">
						<option value="-1" selected="">Select Image</option>
						<option value="1">Oral</option><option value="2">Pharynx_Larynx</option>
						<option value="3">Throat</option>
						<option value="4">Ear</option>
						<option value="12">Right Neck</option>
						<option value="13">Left Neck</option>
						<option value="14">Breast</option>
				</select>
				
				</div>	
                  
                  
                </div>/.box-body
                <div class="box-footer clearfix no-border">
                  <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                </div>
              </div>
					
					</div> 
			 -->	        
			        
			        <!-- Prelim Diagnosis -->
			           
			           <!-- Med & Inv  -->   
			
	<div class="row" id="addMeInv" style="margin-left: 0px;margin-right: 0px;">
			<div class="col-md-15" style="padding-left: 0px;padding-right: 5px;">		
			  <div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <h6 class="box-title" style="font-size:14px">Prescription</h6>
                <div class="box-tools pull-right" style="top: 10px;">
                    	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  		</div>
                </div><!-- /.box-header -->
                
                <div class="box-body" style="padding-top: 0px;">
                 
                 
               <!--   <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Medication </th>
                          <th>Frequency</th>
                          <th>Instruction</th>
                          <th>Start - End Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><a >Crocin 250MG</a></td>
                          <td>1-1-1</td>
                          <td>After Food</td>
                          <td>30/11/2018 - 02/12/2018</td>
                        </tr>
                        <tr>
                          <td><a >Dolo 650MG</a></td>
                          <td>1-1-1</td>
                          <td>After Food</td>
                          <td>31/11/2018 - 02/12/2018</td>
                        </tr>
                        
                      </tbody>
                    </table>
                 -->
                  
                  
                </div><!-- /.box-body -->
                <!-- <div class="box-footer clearfix no-border">
                  <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                </div> -->
              </div>
              </div>
				<!-- <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
				  <div class="box box-primary">
                	<div class="box-header ui-sortable-handle" style="cursor: move;">
                 	 <i class="ion ion-clipboard"></i>
                  		<h6 class="box-title" style="font-size:14px">Investigation</h6>
                  		<div class="box-tools pull-right" style="top: 10px;">
                    	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  		</div>
               	 	</div>/.box-header
                
                <div class="box-body" style="padding-top: 0px;">
                 <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Investigation Name </th>
                          <th>Investigation Type</th>
                          <th>Result</th>
                          <th>Report Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><a>CBC</a></td>
                          <td>Advice</td>
                          <td> - </td>
                          <td>30/11/2018</td>
                        </tr>
                        <tr>
                          <td><a >Thyroid Profile</a></td>
                          <td>Report</td>
                          <td>Negative</td>
                          <td>30/11/2018</td>
                        </tr>
                        
                      </tbody>
                    </table>
                
                  
                  
                </div>/.box-body
                <div class="box-footer clearfix no-border">
                  <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                </div>
              </div>	
			</div> -->
		</div> 
			              
		<!-- Med & Inv -->
		
		<!-- Advice -->	 
		
		<div class="row" id="addAdviceId" style="margin-left: 0px;margin-right: 0px;">
					
			<div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <h6 class="box-title" style="font-size:14px">Advice</h6>
                </div><!-- /.box-header -->
                
                <div class="box-body" style="padding-top: 0px;">
                 
                 <div class="row" style="display: block;">
				<div class="col-sm-15">
				<textarea type="text" class="form-control" style="display: block; resize: none;" id="" placeholder="General advice"></textarea>
				</div>
				<!-- <div class="col-sm-4">
				<div style="position:relative;">
				<label style="font-weight:normal;">
				Referral For:
				</label>
				<select class="form-control">
						<option value="-1" selected="">Referral For</option>
						<option value="1">Local</option>
						<option value="2">Abroad</option>
						
				</select>
				
				</div>
				</div> -->
				</div>	
                  
                  
                </div><!-- /.box-body -->
                <!-- <div class="box-footer clearfix no-border">
                  <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                </div> -->
              </div>
					
					</div>          
			           
		<!-- Advice -->	
		
		<!-- Revisit  -->
		
		<!-- <div class="row" id="addRevId" style="margin-left: 0px;margin-right: 0px;">
					
			<div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <h6 class="box-title" style="font-size:14px">Revisit/Follow-Up</h6>
                </div>/.box-header
                
                <div class="box-body" style="padding-top: 0px;">
                 
                 <div class="row" style="display: block;">
				<div class="col-sm-6">
				<select class="form-control">
				<option value="-1" selected="">Revisit after</option>
						<option value="1">2 days</option>
						<option value="2">1 week</option>
						<option value="2">15 days</option>
						
				</select>
				
				</div>
				<div class="col-sm-6">
				<div class="col-sm-1" style="padding-top: 7px;padding-left: 0px;">OR</div>
				<div class="col-sm-5" style="padding-top: 0px;" id="">
				<span class="label label-primary" style="padding-top: 15px;padding-bottom: 6px;">
				<i class="fa fa-calendar" style="font-size:24px!important;padding-right: 2px;padding-left: 2px;padding-bottom: 0px;padding-top: 2px;margin-top: 3px;"></i>
				</span>
				<input style="display:none;" name="" id="revisitId" >
				</div>
				</div>
				</div>	
                  
                  
                </div>/.box-body
                <div class="box-footer clearfix no-border">
                  <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                </div>
              </div>
					
					</div>
		 -->
		<!-- Revisit -->              
		
		<div class="box box-primary">
			                <div class="box-header">
			                	<b style="font-size:12px">NOTES</b>
			                	<button class="pull-right btn-primary btn dcbutton" onclick="openNotesModal();">Add Notes</button>
			                </div><!-- /.box-header -->
			                <div class="box-body no-padding">
			                	<table class="table table-striped">
			                    	<tr>
			                      		<th style="width: 5%"></th>
			                      		<th style="width: 60%"></th>
			                      		<th style="width: 10%"></th>
			                      		<th style="width: 20%"></th>
			                    	</tr>
			                    	<tbody id="notesTableBody"></tbody>
			                    	<!-- <tr>
			                      		<td>1.</td>
			                      		<td>Update software</td>
				                      	<td>
					                        <div class="progress progress-xs">
					                        	<div class="progress-bar progress-bar-danger" style="width: 55%"></div>
					                        </div>
				                      	</td>
				                      	<td><span class="badge bg-red">55%</span></td>
			                    	</tr> -->
			                    	<!-- <tr>
			                      		<td>2.</td>
			                      		<td>Clean database</td>
			                      		<td>
			                        		<div class="progress progress-xs">
			                          			<div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
			                        		</div>
			                      		</td>
			                      		<td><span class="badge bg-yellow">70%</span></td>
			                    	</tr> -->
			                    	<!-- <tr>
			                      		<td>3.</td>
			                      		<td>Cron job running</td>
			                      		<td>
			                        		<div class="progress progress-xs progress-striped active">
			                          			<div class="progress-bar progress-bar-primary" style="width: 30%"></div>
			                        		</div>
			                      		</td>
			                      		<td><span class="badge bg-light-blue">30%</span></td>
			                    	</tr> -->
			                    	<!-- <tr>
			                      		<td>4.</td>
			                      		<td>Fix and squish bugs</td>
			                      		<td>
			                        		<div class="progress progress-xs progress-striped active">
			                          			<div class="progress-bar progress-bar-success" style="width: 90%"></div>
			                        		</div>
			                      		</td>
			                      		<td><span class="badge bg-green">90%</span></td>
			                    	</tr> -->
			                  	</table>
			                </div><!-- /.box-body -->
			             </div>
			              
			              <div class="box box-primary">
			                <div class="box-header">
			                	<b style="font-size:12px">REPORT</b>
			                	<button class="pull-right btn-primary btn" onclick="openReportModal();">Add Report</button>
			                </div><!-- /.box-header -->
			                <div class="box-body no-padding">
			                	<table class="table table-striped">
			                    	<tr>
			                      		<th style="width: 100%"></th>
			                      		<!-- <th style="width: 60%"></th>
			                      		<th style="width: 10%"></th>
			                      		<th style="width: 20%"></th> -->
			                    	</tr>
			                    	<tbody id="reportsTableBody"></tbody>
			                    	<!-- <tr>
			                      		<td>1.</td>
			                      		<td>Update software</td>
				                      	<td>
					                        <div class="progress progress-xs">
					                        	<div class="progress-bar progress-bar-danger" style="width: 55%"></div>
					                        </div>
				                      	</td>
				                      	<td><span class="badge bg-red">55%</span></td>
			                    	</tr> -->
			                    	<!-- <tr>
			                      		<td>2.</td>
			                      		<td>Clean database</td>
			                      		<td>
			                        		<div class="progress progress-xs">
			                          			<div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
			                        		</div>
			                      		</td>
			                      		<td><span class="badge bg-yellow">70%</span></td>
			                    	</tr> -->
			                    	<!-- <tr>
			                      		<td>3.</td>
			                      		<td>Cron job running</td>
			                      		<td>
			                        		<div class="progress progress-xs progress-striped active">
			                          			<div class="progress-bar progress-bar-primary" style="width: 30%"></div>
			                        		</div>
			                      		</td>
			                      		<td><span class="badge bg-light-blue">30%</span></td>
			                    	</tr> -->
			                    	<!-- <tr>
			                      		<td>4.</td>
			                      		<td>Fix and squish bugs</td>
			                      		<td>
			                        		<div class="progress progress-xs progress-striped active">
			                          			<div class="progress-bar progress-bar-success" style="width: 90%"></div>
			                        		</div>
			                      		</td>
			                      		<td><span class="badge bg-green">90%</span></td>
			                    	</tr> -->
			                  	</table>
			                </div><!-- /.box-body -->
			             </div>
	                 </div><!-- /.tab-pane -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer"
				style="display:block;padding-right:15px; margin-left:0% !important;position: fixed;bottom: 0px;margin-right: auto;margin-left: auto;width:100%;">
			<!--  <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>.</strong> All rights reserved. -->
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0.0
			</div>
			Copyright &copy; 2018-2019 <a href="http://www.wrizto.com/" target=_blank style="color: rgb(60, 141, 188);">Wrizto Healthcare Pvt. Ltd.</a> All rights reserved. </footer>
      <!-- Control Sidebar -->
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    
    
    	<!-- <div class="example-modal" >
            <div class="modal" id="addMedicationModal">
            	<div class="modal-dialog">
                	<div class="modal-content">
                  		<form id="addMedicationForm" method="post" enctype ='multipart/form-data'>
                  			<div class="modal-header" style="background-color:#3c8dbc;">
	                    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    		<span aria-hidden="true">&times;</span></button>
	                    		<h4 class="modal-title" style="color:white;">Add Medication</h4>
	                  		</div>
	                  		<div class="modal-body" style="height:400px;">
	                  			<div class="pull-right">
	                  				<button type="button" onclick="addNewFormBlock();">ADD</button>
	                  			</div>
	                    		<div id="formBlock" class="" style="">
		                    		<div class="col-md-12" style="margin-top:10px;">
		                    			<div class="col-md-6 " >
		                    				<label>Prescribed By :</label>
		                    				<input type="text" class="form-control getJson" placeholder="Prescribed By" attr="prescribedBy"></input>
		                    			</div>
		                    			<div class="col-md-6" >
		                    				<label>Medication Name :</label>
		                    				<input type="text" class="form-control getJson" placeholder="Medication Name" attr="medicationName"></input>
		                    			</div>
		                    		</div>
		                    		<div class="col-md-12" style="margin-top:10px;">
		                    			<div class="col-md-6">
		                    				<label>Dosage :</label>
		                    				<input type="text" class="form-control getJson" placeholder="Dosage" attr="dosage"></input>
		                    			</div>
		                    			<div class="col-md-6 " >
		                    				<label>Frequency :</label><br>
		                    				<select style="height:35px;width:100%;" class="getJson" attr="frequency">
		                    					<option>Select Frequency</option>
		                    					<option>0-0-1</option>
		                    					<option>0-1-0</option>
		                    					<option>1-0-0</option>
		                    					<option>1-0-1</option>
		                    					<option>1-1-0</option>
		                    					<option>1-1-1</option>
		                    				</select>
		                    			</div>
		                    		</div>
		                    		
		                    		<div class="col-md-12" style="margin-top:10px;">
		                    			<div class="col-md-6 " >
		                    				<label>Instruction :</label><br>
		                    				<select style="height:35px;width:100%;" class="getJson" attr="instructions">
		                    					<option>Select Instruction</option>
		                    					<option>Before Food</option>
		                    					<option>After Food</option>
		                    				</select>
		                    			</div>
		                    			<div class="col-md-6 " >
		                    				<label>Start Date :</label>
		                    				<input type="text" class="form-control getJson" placeholder="Strat Date" attr="beginDate"></input>
		                    			</div>
		                    		</div>
		                    		
		                    		<div class="col-md-12" style="margin-top:10px;">
		                    			<div class="col-md-6 " >
		                    				<label>Days :</label><br>
		                    				<input type="text" class="form-control getJson" placeholder="Days" attr="days"></input>
		                    			</div>
		                    			<div class="col-md-6" >
		                    				<label>Comment :</label>
		                    				<input type="text" class="form-control getJson" placeholder="Comment" attr="comment"></input>
		                    			</div>
		                    		</div>
	                    		
	                    		</div>
	                    		
	                  		</div>
	                  		
	                  		<div class="modal-footer">
	                    		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
	                    		<button type="button" class="btn btn-primary" onclick="savePrescription();">Save</button>
	                  		</div>
                  		</form>
                	</div>/.modal-content
              	</div>/.modal-dialog
            </div>/.modal
        </div> -->
        
        
        
       <div id="addMedicationModal" name="addMedicationForm" class="modal modal-fullscreen fade" style="height:100%;padding-top: 0px;z-index:9999" role="dialog"   data-keyboard="false">
       		<form id = "saveMedicationId" method="post" enctype ='multipart/form-data' style="height:100%;">
       				<input type="hidden" name="additionalDetailJson" id="additionalDetailJson">
                    <input type="hidden" name="strPrescribedDate" id="strPrescribedDate">
                   <!--  <input type="hidden"  id="serviceNameValue" name="serviceName" value=""> -->
                    <input type="hidden" name="documentTypeId" id="documentTypeId" value="">
                    <input type="hidden" name="clinicId" id="clinicId" >
                    <input type="hidden" name="userId" id="idUser" value="">
                    <input type="hidden" name="ccpId" id="idCcp" value="">
		 			<input type="hidden" id="medEpisodeId"  class="episodeId" name="episodeId" value=""> 
		 			<input type="hidden"  id="medicalReportId" name="medicalReportId" value="-1"> 
		 			<input type="file" style="display:none;" name="files" id="reportFileId" class="getReport" onchange="setFileName(this)"></input>
		  			 
		 			<input type="hidden"  id="deletedRecords" name="deletedRecords" value="">  
		 			<input type="hidden"  id="loggedIncreatedBy" name="createdBy" value="">
		     		<input type="hidden"  id="documentTypeName" name="documentTypeName" value="Prescription">
					<input type="hidden"  id="documentTypeName" name="documentTypeName" value="Prescription">
					<input type="hidden" id="medicationIdFromDropdown" name="medicationHiddenId">
		 			
		 			<input type="hidden" id="" name="laboratoryName" value="">
		 			 <input class="getAdvice" type="hidden" name="laboratoryId" value="">
		 			 
		 			 <input type="hidden" id="noteComments" name="comments" value="">
		 			 <input type="hidden" name="dischargeStatus" value="1">
   
                    
         		<div class="modal-dialog" style="padding-top: 0px;height:100%;" >
					<div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
						<div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
							<h5 class="modal-title" >
							<b style="color:White">Medication</b></h5>
						</div>
						<div class="modal-body" style="background: white;padding-bottom: 0px;padding-top: 0px;;overflow-x:hidden;height:450px;">
							<div class="col-xs-12">
								<div class="col-xs-6 "
										style="margin-top: 0px; padding-left: 0px;">
										<div class="col-xs-12"
											style="padding-left: 0px; padding-right: 0px;">
											<div style="width: 100%; color: black" id ="isDirtyId"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
												<input spellcheck="false"
													class="mdl-textfield__input error-popover bClass Newpolic4"
													type="text" id="prescribedByPre" name="prescribedBy"
													maxlength="100" style="text-transform: capitalize;"> <label class="mdl-textfield__label"
													for="sample3">Prescribed By<i style="color: red">*</i>
												</label>
											</div>
										</div>
								</div>
	
								<div class="col-xs-6"  style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input autocomplete="off" autocorrect="off" spellcheck="false"
											class="mdl-textfield__input error-popover clearAll bClassM Newpolic1"
											autocomplete="off" type="text" id="medicationNameId" name="medicationName"
											maxlength="100" style="text-transform: capitalize;margin-bottom: 8px; "> 
											<label class="mdl-textfield__label" for="sample3">Medication Name
											<span style="color:red;">*</span>
											</label>
					
										</div>
									</div>
								</div>			
							</div>
							<div class="col-xs-12">
								<div class="col-xs-6"
									style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black;margin-bottom: 8px"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown" >
											<input autocomplete="off" autocorrect="off"
												spellcheck="false"
												class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="text" id="dosageID" name="dosage"
												maxlength="100"> <label class="mdl-textfield__label"
												for="sample3">Dosage MG/ML/MM
											</label>
										</div>
									</div>
								</div>
				
								<div style="padding-left: 0px;" class="col-xs-6 ">
									<p style="margin-bottom: 0px; color: royalblue">Select Frequency</p>
										<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
											<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
												<select class="form-control" id="fequencyId" name="frequency" style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
												<option>0-0-1</option>
												<option>0-1-0</option>
												<option>1-0-1</option>
												<option>1-1-0</option>
												<option>1-1-1</option>
												<option>1-0-0</option>
												</select>
											</div>
										</div>
								</div>
							
							</div>
							<div class="col-xs-12">
								<div style="padding-left: 0px;" class="col-xs-6 ">
									<p style="margin-bottom: 0px; color: royalblue">Instruction</p>
										<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
											<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
												<select class="form-control" id="instructionId" name="instruction" style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
												<option>Before Food</option>
												<option>After Food</option>
												</select>
											</div>
										</div>
								</div>
				
		            			<div class="col-xs-6"
									style="margin-top: 0px; padding-left: 0px; margin-bottom: 17px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="text" id="startId" name="strBeginDate"
												maxlength="100"> <label class="mdl-textfield__label"
												for="sample3">DD/MM/YY(Start Date)<i style="color: red">*</i>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12">	
								<div class="col-xs-6"
									style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input autocomplete="off" autocorrect="off"
												spellcheck="false"
												class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="number" id="days" name="days"
												maxlength="100"> <label class="mdl-textfield__label"
												for="sample3">Days<i style="color: red">*</i>
											</label>
										</div>
									</div>
								</div>
									
								<div class="col-xs-6"
									style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input autocomplete="off" autocorrect="off"
												spellcheck="false"
												class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="text" id="commentsMedId" name="comments"
												maxlength="100"> <label class="mdl-textfield__label"
												for="sample3">Comments<!-- <i style="color: red">*</i> -->
											</label>
										</div>
									</div>
								</div>
							</div>
							
								<!-- <div class="col-xs-12"
										style="margin-top: 0px; margin-bottom: 20px; padding-left: 0px;display:inline-flex; height: 35px">
										<div class="col-xs-8"
											style="padding-left: 0px; padding-right: 0px;">
											<div style="color: black;display:inline-flex;"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
												<i style="padding-top: 5px;margin-left: 0px;padding-right: 10px" class="fa fa-paperclip"></i>
												<input type="file" style="margin-bottom: 0px;
												width:100%; border-radius: 4px; font-family:Roboto Medium;overflow:hidden;
												font-size:15px;color:#5498D2;border:#5498D2 1px solid"
												name="files" id="firstmedfile" onchange="acceptType(this);">
											</div>
										</div>
	
									<div style="color: black"
										class="col-xs-4 mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
		
										<button type="button" onclick="getOtherMedFile()"
											style="width: 45%;float:left; background: none repeat scroll 0% 0% transparent; color: #5498D2 !important; border-radius: 4px; font-family: Roboto Medium; font-size: 11px; border: 1px solid #5498D2; margin-bottom: 0px; margin-top: -1px; padding: 0px; height: 29px; margin-top: 0px;"
											class="btn right">Upload More</button>
		
									</div>
								</div> -->
								<div class="col-xs-12 addedMedTable" id="dynTableMed" style="display:none;color:black;">
								<table class="table" >
									<tr>
									<!-- <th >id</th> -->
										<th>Name</th>
										<th>Dosage</th>
										<th>Freq</th>
										<th>Inst</th>
										<th>Start Date</th>
										<th>Days</th>
										<th>Comments</th>
										<th></th>
			
									</tr><tbody id="medAdd"></tbody>
								</table>
							</div>
						</div>	
						<div id="addFilesMed">
                      		
                        </div>	
           	
						<div class="modal-footer text-center" style="padding: 5px;overflow-x:hidden">
							
							<div class="col-xs-12" style="position: absolute;bottom: 0;">
								<div class="text-center">
									<button type="button" class="btn btn-danger" style="width: 20%;margin-bottom: 6px;" onclick= "canMed();">Cancel</button>
									<button type="button" class="btn btn-warning" id="" style="width: 20%; margin-left: 6px; margin-bottom: 6px;" onclick="addMedication();" >ADD</button>
									<button type="button" class="btn btn-success" id="" style="width: 20%; margin-left: 6px; margin-bottom: 6px;" onclick="saveMedication(1);" >Save</button>
								</div>	
							</div>
						</div>
					</div>
				</div>
	  	 	</form>
    	</div>
        
         <div id="addVisitForm" class="modal fade main-fade-modal " role="dialog"  data-backdrop="static" data-keyboard="false">
  <form id = "saveAdviceId" method="post" enctype ='multipart/form-data' >
  <input class="getAdvice" type="hidden" name="documentTypeId" id="" value="2">
   <input class="getAdvice" type="hidden" name="documentTypeName" id="" value="Advice">
   <input class="getAdvice" type="hidden" name="serviceTypeIdFromDropdown" id="serviceTypeIdFromDropdown" value="Advice">
    <input class="getAdvice" type="hidden" name="servicesId" id ="investigationId"> 
    <input type="hidden" name="additionalDetailJson" id="adviceJSON">
   <!--  <input class="getAdvice" type="hidden"  id="serviceNameId" name="servicesName" > -->
    <input class="getAdvice" type="hidden"  value="services" name="InvestigationType" >
    
   
    <!-- <input type="hidden"  value="30/03/2018" name="strPrescribedDate" > -->
    
       <div class="modal-dialog"  id="visitModalId">
		<div class="modal-content" style="border-radius: 4px;">
			<div class="modal-header" style="background-color:#5498D2;">
			<h5 class="modal-title" ><b style="color:White">Advice</b></h5>
			</div>
			<div class="modal-body"  style="padding-top: 0px;">
 			<div class="col-xs-12" style="margin-top: 0px; padding-left: 0px;" >
 			
 			
 			
 			<div class="col-xs-12 Newpolic3"
				style="margin-top: 0px; padding-left: 0px;">
				<div style="padding-left: 0px;" class="col-xs-12 ">
				
				<div style="width: 100%; color: black;"
							class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
						<input autocomplete="off" autocorrect="off"
										spellcheck="false" style="margin-bottom:8px;"
										class="mdl-textfield__input error-popover bClass Newpolic2 getAdvice"
										type="text" id="serviceTypeId" name="investigationName"
										maxlength="100"> <label class="mdl-textfield__label"
										for="sample3">Service Name<i style="color: red">*</i>
								</label>
											
						

					</div>
						<!-- <p style="margin-bottom: 0px; color: royalblue" id="masterServiceList">Service<i style="color: red">*</i></p>
							<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
								<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
									<select class="form-control getAdvice" id="serviceTypeId" name="serviceTypeId" 
									style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;padding-bottom: 7px;">
										
									</select>
								</div>
							</div> -->
					</div>
			</div>
			<div class="col-xs-12 Newpolic3"
					style="margin-top: 0px; padding-left: 0px;">
					<div class="col-xs-12"
						style="padding-left: 0px; padding-right: 0px;">
						<div id="addadvclassprenameID" style="width: 100%; color: black"
							class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
							<input autocomplete="off" autocorrect="off"
								spellcheck="false"
								class="mdl-textfield__input error-popover getAdvice"
								type="text" id="prescribedByAdv" name="prescribedBy"
								maxlength="100"> <label class="mdl-textfield__label"
								for="sample3">Adviced By<i style="color: red">*</i> 
							</label>
						</div>
					</div>
				</div>
			<div class="col-xs-12 Newpolic3"
									style="margin-top: 0px; padding-left: 0px;">
				<!-- <div class="col-xs-6 "
							style="margin-top: 0px; padding-left: 0px;"> -->
							<div class="col-xs-12"
								style="padding-left: 0px; padding-right: 0px;">
								<div style="width: 100%; color: black"
									class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
								<input autocomplete="off" autocorrect="off"
										spellcheck="false"
										class="mdl-textfield__input error-popover getAdvice"
										type="text" id="fromDate" name="dateTime"
										maxlength="100"> <label class="mdl-textfield__label"
										for="sample3" style="margin-bottom: 0px;">Date (DD/MM/YY)<i style="color: red">*</i>
									</label>
		
							</div>
						<!-- </div> -->
					</div>
				
				</div>
							
			<div class="col-xs-12 Newpolic3"
									style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<textarea autocomplete="off" autocorrect="off"
												spellcheck="false" placeholder="Advice"
												class="mdl-textfield__input error-popover getAdvice"
												 id="advComments" name="Labcomments"
												maxlength="100"></textarea>
										</div>
									</div>
								</div>
						
			</div>		
			<div class="modal-footer text-center">
				
				<!-- <button id="demo-show-snackbar" class="mdl-button mdl-js-button mdl-button--raised" style="display:none" type="button"></button> -->
				<button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 25%; margin-left: 5px; margin-bottom: 0px;" onclick="clearAdvModal()">Cancel</button>
				<!-- <button type="button" class="btn btn-success" id="otpButton" onclick="generateOtp()">Generate OTP</button> -->
				<!-- <button type="button" class="btn btn-warning" id="" style="width: 20%;" onclick="addVisit();" >ADD</button> -->
				<button type="button" class="btn btn-success" id="" style="width: 25%;" onclick="saveMedication(2);" >Save</button>
			</div>
		</div>
	</div>
  </div>         
</form></div>
  
        
        <!-- Observation Modal  -->
        
        <!-- Notes Modal  -->
    
    	<div class="example-modal" >
            <div class="modal" id="addNotesModal">
            	<div class="modal-dialog">
                	<div class="modal-content">
                		<form id="addNotesForm" method="post" enctype ='multipart/form-data'>
                		<input type="hidden" name="additionalDetailJson" >
                   
                    <input type="hidden" name="documentTypeId" value="8">
                     <input type="hidden" name="clinicId" id="clinicIdNote" >
                     <input type="hidden" name="userId" id="idUserNote" value="">
                     <input type="hidden" name="ccpId" id="idCcpUP" value="">
		 <input type="hidden"  class="episodeId" name="episodeId" value="" id="episodeIdUp"> 
		 <input type="hidden"  id="medicalReportId" name="medicalReportId" value="-1"> 
		 
		  <input type="hidden"  name="deletedFiles" value=""> 
		 <input type="hidden"   name="deletedRecords" value="">  
		 <input type="hidden"  name="episodeName" value="CCP">
		 <input type="hidden"   name="dischargeStatus" value="1">
		  <input type="hidden"   name="strPrescribedDate" value="30/03/2018">
		  
		 
		<input type="hidden"  name="documentTypeName" value="Notes">
                			
	                  		<!-- <div class="modal-header">
	                    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    		<span aria-hidden="true">&times;</span></button>
	                    		<h4 class="modal-title">Add Notes</h4>
	                  		</div> -->
	                  		<div class="modal-header" style="background-color:#5498D2;">
								<h5 class="modal-title" ><b style="color:White">Notes</b></h5>
							</div>
	                  		<div class="modal-body">
	                    		<textarea style="width:100%;height:100px;" id="noeComments" class="getNotes" name="comments"></textarea>
	                    		
	                  		</div>
	                  		<div class="modal-footer">
	                    		<button type="button" class="btn btn-danger" style="width: 25%;" onclick="canNote();">Cancel</button>
	                    		<button type="button" class="btn btn-success" style="width: 25%;" onclick="saveMedication(3);">Save</button>
	                  		</div>
	                  	</form>
                	</div><!-- /.modal-content -->
              	</div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
        <div class="example-modal" >
            <div class="modal" id="addReportModal">
            	<div class="modal-dialog">
                	<div class="modal-content">
                		<form id="addReportForm" method="post" enctype ='multipart/form-data'>
                			
	                  		<!-- <div class="modal-header">
	                    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    		<span aria-hidden="true">&times;</span></button>
	                    		<h4 class="modal-title">Add Notes</h4>
	                  		</div> -->
	                  		<div class="modal-header" style="background-color:#5498D2;">
								<h5 class="modal-title" ><b style="color:White">Report</b></h5>
							</div>
	                  		<div class="modal-body">
	                    		<div class="col-xs-12"
										style="margin-top: 0px; margin-bottom: 20px; padding-left: 0px;display:inline-flex; height: 35px">
										<div class="col-xs-12"
											style="padding-left: 0px; padding-right: 0px;">
											<div style="color: black;display:inline-flex;width:95%"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
												<i style="padding-top: 5px;margin-left: 0px;padding-right: 10px" class="fa fa-paperclip"></i>
												
												<button class="btn" type="button" onclick="getElementById('reportFileId').click()" style="border:1px solid #5498d2">Browse</button>
												<span style="width:80%;height:34px;border:1px solid #5498d2;padding-top: 6px; padding-left: 12px;" id="showReportFileName"></span>
											
											</div>
										</div>
	
									
		
									</div>
								</div>
	                  		<div class="modal-footer" style="margin-top: 15%;">
	                    		<!-- <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button> -->
	                    		<button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 25%;" onclick="clearRep();">Cancel</button>
	                    		<button type="button" class="btn btn-success" style="width: 25%;" onclick="saveMedication(4);">Upload</button>
	                  		</div>
	                  	</form>
                	</div>
              	</div>
            </div>
        </div>
        </div>
    </body>

<script>

var bp;
var pulse;
var s =" ";
var test=3;

 var episdeId =  "";
 var userId = "";
 var episodeId =  "";
 var loggedInUserId = '';
 var productId = '';
 
 
 $(document).ready(function(){
	    $(".dropdown").hover(function(){
	        var dropdownMenu = $(this).children(".dropdown-menu");
	        if(dropdownMenu.is(":visible")){
	            dropdownMenu.parent().toggleClass("open");
	        }
	    });
	});     


var patientId =  localStorage.getItem("patientId");

$(document).ready(function() {
	//alert(episodeId);alert(episdeId);
	getPatientDetails();
	//getVitalDocument(-1);
	episdeId = "";
	if(isDuplexCall==true){
		
		$('#dashBoardHideShoeVideoIconId').hide();
		var timeoutId = 0;
		var profileID=localStorage.getItem("profileId");
		var	serverKeyId=localStorage.getItem("serverKeyId");
		loggedInUserId = localStorage.getItem("loggedInUserId");
		 var ajaxfn= function name() {
			/* $.ajax({
					type : "POST",
					url :   serverPath + "/checkForCallv2?serverKeyId="+serverKeyId+"&profileId="+profileID+"&userId="+loggedInUserId,
					headers: {
					       "deviceId":"ADMIN",
					   	 },
					success : function(data){
						if(data.errorCode=="1011"){
							//alert("1001");nurseNameForActiveId
							$('#showHideVideoIconId').hide();
							$('#videoIconId').hide();
							$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
						}else if(data.code=="200"){
							if(data.activeUsers!=""){
								
								$.each(data.activeUsers,function(index,option){
									$('#nurseNameForActiveId').html(" "+option.name+" ");
									$('#showHideVideoIconId').show();
									$('#videoIconId').show();
								});
							
							}
							else{
								$('#showHideVideoIconId').show();
								$('#videoIconId').hide();
								$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
							}
							
							if(data.content != null) {
								var callerName = data.content.name;
								var roomName = data.content.roomName;
								var roomId = data.content.id;
								confPopUp(callerName, roomName, roomId);	
							}
						}
						
						
						
						timeoutId= setTimeout(ajaxfn,3000);	
					},error:function(x,y,response){
						$('#showHideVideoIconId').hide();
						$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
						timeoutId= setTimeout(ajaxfn,3000);	
						
					}
					   	 
			}); */
			
			 var name = '';var activeList = '';
 			$.ajax({
 			   type: "POST",
 			   headers:{"deviceId":"ADMIN"},
 			   url: serverPath + "/checkForCallv3?serverKeyId="+serverKeyId+"&profileId="+profileID+"&userId="+loggedInUserId,
 			   success : function(data){
 				   if(data.code == '200'){
 					   $("#activeUsersId").html('');
 					   name = '';activeList = '';
 					   if(data.activeUsers!=""){
 				$.each(data.activeUsers,function(index,option){
 							   
 							   if(option.name != null){
 								   name = option.name;								   ;
 							   }
 				activeList+='<li class="form-control" style="border-radius:6px;" ><span style="font-size: 12px;text-transform: capitalize;margin-left: -33px;" id="nurseNameForActiveId">'+name+'</span> '+
                 ' <a id= "videoIconId" style="border-radius: 50%;margin-left: 5px;float:right;background-color: green" class="btn btn-primary" title="videoCall" onclick="initiateCall('+option.profileId+')"><b> '+
 				' <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a></li> ';
 							  
 				$("#activeUsersId").html(activeList);
 				
 							   
 						   });
 						   
 					   }else{
 							//$('#showHideVideoIconId').show();
 							//$('#videoIconId').hide();
 							$("#activeUsersId").html("<li class='form-control' style='border-radius:6px;color:gray;'>No Active User</li>");
 						}
 					  
 					if(data.content != null) {
						var callerName = data.content.name;
						var roomName = data.content.roomName;
						var roomId = data.content.id;
						confPopUp(callerName, roomName, roomId);	
					} 
 				   }
			   timeoutId= setTimeout(ajaxfn,3000);
			   
		   },error:function(x,y,response){
				//$('#showHideVideoIconId').hide();
				$("#activeUsersId").html("<li class='form-control' style='border-radius:6px;color:gray;'>No Active User</li>");
				timeoutId= setTimeout(ajaxfn,3000);	
				
			}
 			});
		}
		 ajaxfn();
	}else{
		$('#showHideVideoIconId').hide();
	}
	
	
	 var loggedInUserFullName = localStorage.getItem("loggedInUserFullName");
		loggedInUserId = localStorage.getItem("loggedInUserId");
     $("#loggedInUserFullName").text(loggedInUserFullName);
	 $("#loggedInUserFullNameSpan").text(loggedInUserFullName);
	
	 $("#loggedIncreatedBy").val(loggedInUserId);
	 $("#idUser").val(loggedInUserId);
	 $("#idUserNote").val(loggedInUserId);
	 
	productId = localStorage.getItem("productId");
	var userGroupCode =  localStorage.getItem("userGroupCode");
	if(userGroupCode=="DOC"){
		
		 $("#dash_id").text("Doctor DashBoard");
		 $(".dcbutton").show();
	}
	else{
		$("#dash_id").text("Nurse DashBoard");
		$(".dcbutton").hide();
	}
	 
	
	var i=0;
	
	$.ajax({
		type : "POST",
		url :   serverPath + "/getEpisodeList?patientId="+patientId,
		success : function(data){
			if(data.code=="200"){
				
				var size = data.content.length;
				s=size;
				
				var appendText = "";
				$.each(data.content,function(index,option){
        			if(index == 0){
        				episdeId = option.id;
        				episodeId = option.id;
        				userId = option.userId;
        				time = option.created;
        			}        		       		
        			if(size>4){        		    	   
             			if(index<4){            				
             				if(index==0){
             					appendText += '<button type="button" id="prevId" style="margin-left:10px;background:#B8B8B8;display:none;" class="btn btn-primary normalCol" onclick="previous('+option.id+');">'+"PREV"+'</button>'            						
             				}             				
             				appendText += '<button type="button" id="'+index+'" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="getHistoryOfUser('+option.id+');">'+option.episodeName+'</button>'		
             			}
             			else{             				
             				appendText += '<button type="button" id="'+index+'" style="margin-left:10px;background:#B8B8B8; display:none;" class="btn btn-primary normalCol" onclick="getHistoryOfUser('+option.id+');">'+option.episodeName+'</button>'
             				if(index==size-1){
             					appendText += '<button type="button" id="nextId" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="next('+option.id+');">'+"NEXT"+'</button>'         				
             				}
             			}
		            }       			
		             else{            			
		          		appendText += '<button type="button" id="'+index+'" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="getHistoryOfUser('+option.id+');">'+option.episodeName+'</button>'		             				
		             }        			
				});
				$("#chronologicalView").html(appendText);				
				$( "#chronologicalView button" ).first().css( "background-color", "#367fa9" );
				getMasterVitals();
				getVitalDocument(episdeId);
			}
		}
	});
	//alert(episdeId);
	
	 $('#addVisitForm').on('show.bs.modal', function (e) {
				
			$("#fromDate").mask("99/99/99", {});
		});
	 $('#addMedicationModal').on('show.bs.modal', function (e) {
			
			//$("#startId").mask("99/99/99", {});
			//$("#medAdd").html('');
			//$("#dynTableMed").css("display","none");
	  });
	 
	 $('#startId').datepicker({
		 	autoclose: true,
		 	todayHighlight: true,
		 	//startDate: '-0m',  
			format: "dd/mm/yyyy"
			}).on('changeDate', function(e){
				$("#startId").attr("readonly", true);
		    	$("#startId").parent().addClass('is-dirty');
				//$(this).datepicker('hide');
			});
	 
	 $('#revisitId').datepicker({
		 	autoclose: true,
		 	todayHighlight: true,
		 	//startDate: '-0m',  
			format: "dd/mm/yyyy"
			}).on('changeDate', function(e){
				$("#revisitId").attr("readonly", true);
				//$(this).datepicker('hide');
			});
	 
	 $('#fromDate').datepicker({
	 	 autoclose: true,
	 	 todayHighlight: true,  
		 format: "dd/mm/yyyy",
		}).on('changeDate', function(e){
			$("#fromDate").attr("readonly", true);
	    	$("#fromDate").parent().addClass('is-dirty');
			$(this).datepicker('hide');
		});
	 
	 $('#serviceTypeId').typeahead({
			source: function(query, process) {
				
				
			 	 var $url = serverPath+'/masterServiceList?serviceType=services';
				 var $datas = new Array;
				 $datas = [""];
				 $.ajax({
					 url: $url,
					 headers: {
				       "deviceId":"ADMIN",
				   	 },
					 dataType: "json",
					 type: "GET",
					 success: function(data) {
						   
						 $.map(data.content, function(data){
							 var group;
							 group = {
								 id: data.id,
								 name: data.value,
								
								 toString: function () {
								 	return JSON.stringify(this);
								 },
								 toLowerCase: function () {
								 	return this.name.toLowerCase();
								 },
								 indexOf: function (string) {
								 	return String.prototype.indexOf.apply(this.name, arguments);
								 },
								 replace: function (string) {
								 	var value = '';
									value += this.name;
									 if(typeof(this.name) != 'undefined') {
									 	value += ' <span class="pull-right muted">';
									 	value += this.name;
									 	value += '</span>';
									 }
									 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
								 }
							 };
							 $datas.push(group);
						 });
						 process($datas);
					 }
				 });
			 },
			 property: 'name',
			 items: 10,
			 minLength: 2,
			 updater: function (item) {
			 var item = JSON.parse(item);
			 $('#investigationId').val('');
				//alert(item.id);
			// $('#serviceTypeIdFromDropdown').val(item.id);
			 $('#investigationId').val(item.id);
			 /* $("#servicesName").val(item.name);
			 alert( $('#servicesName').val()); */
			 return item.name;
			}
		});
	 
	$('#medicationNameId').typeahead({
		source: function(query, process) {
			
			
			 var searchParameter = $('#medicationNameId').val();
		 	 var $url = serverPath +'/getMediactionList?searchParameter='+searchParameter;
			 var $datas = new Array;
			 $datas = [""];
			 $.ajax({
				 url: $url,
				 dataType: "json",
				 type: "GET",
				 success: function(data) {
					   //console.log(data.content);
					 if(data.content!=undefined && data.content!='undefined'){
					 $.map(data.content, function(data){
						 var group;
						 group = {
							 id: data.id,
							 name: data.name,
							
							 toString: function () {
							 	return JSON.stringify(this);
							 },
							 toLowerCase: function () {
							 	return this.name.toLowerCase();
							 },
							 indexOf: function (string) {
							 	return String.prototype.indexOf.apply(this.name, arguments);
							 },
							 replace: function (string) {
							 	var value = '';
								value += this.name;
								 if(typeof(this.name) != 'undefined') {
								 	value += ' <span class="pull-right muted">';
								 	value += this.name;
								 	value += '</span>';
								 }
								 
								 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
							 }
						 };
						 $datas.push(group);
					 });}
					   else{
						   $datas = [""];
						   }
					 process($datas);
				 }
			 });
		 },
		 property: 'name',
		 items: 10,
		 minLength: 2,
		 updater: function (item) {
		 var item = JSON.parse(item);
		 $('#medicationIdFromDropdown').val('');
			//alert(item.id);
		 $('#medicationIdFromDropdown').val(item.id);
		 return item.name;
		}
	});
	
	
	 $('#serviceNameId').typeahead({
			source: function(query, process) {
				
				
				 var searchParameter = $('#serviceNameId').val();
			 	 var $url = 'getServicesList?searchParamater='+searchParameter;
				 var $datas = new Array;
				 $datas = [""];
				 $.ajax({
					 url: $url,
					 dataType: "json",
					 type: "GET",
					 success: function(data) {
						   //console.log(data.content);
						 if(data.content!=undefined && data.content!='undefined'){
						 $.map(data.content, function(data){
							 var group;
							 group = {
								 id: data.id,
								 name: data.name,
								
								 toString: function () {
								 	return JSON.stringify(this);
								 },
								 toLowerCase: function () {
								 	return this.name.toLowerCase();
								 },
								 indexOf: function (string) {
								 	return String.prototype.indexOf.apply(this.name, arguments);
								 },
								 replace: function (string) {
								 	var value = '';
									value += this.name;
									 if(typeof(this.name) != 'undefined') {
									 	value += ' <span class="pull-right muted">';
									 	value += this.name;
									 	value += '</span>';
									 }
									 
									 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
								 }
							 };
							 $datas.push(group);
						 });}
						   else{
							   $datas = [""];
							   }
						 process($datas);
					 }
				 });
			 },
			 property: 'name',
			 items: 10,
			 minLength: 2,
			 updater: function (item) {
			 var item = JSON.parse(item);
			 $('#serviceFromDropdown').val('');
			 $('#serviceFromDropdown').val(item.id);
			 return item.name;
			}
		});
	
}); //END of READY

function confPopUp(callerName, roomName, roomId) {
	var room = encodeURIComponent(roomName);
	var r = confirm( callerName + " is calling you. Do you accept the call?");
	if(r== true){
		var win = window.open('initiateCall?roomName='+room, 'result', 'width=1024,height=768');
		win.addEventListener("resize", function(){
			win.resizeTo(1024, 768);
         });
	}
	$.ajax({
		type : "POST",
		url :   serverPath + "/intiateAndCloseCall?initiate=false&roomId="+roomId,
		success : function(data){			
		}
	});
	}
	
function refreshPage(){
	location.reload();
} 

function sendPics(){
	
	/* 
	var form1 = document.getElementById('saveMedicationId');
		 var formData = new FormData(form1);
			
		$.ajax({
			type : "POST",
			url : "saveVitalDocument",
			headers: {
		        "deviceId":"ADMIN",
		    },

			data: formData,
			dataType:"json",
		    mimeType: "multipart/form-data",
			async : false,
		    cache : false,
			contentType : false,
		    processData : false,
			success : function(data){
				
			}
		}); */
	}
	
function clearAdvModal(){
	
	$("#addVisitForm").modal('hide');
	$("#serviceTypeId").val('1');
	$('.getAdvice').val('');
	$('.getAdvice').parent().removeClass('is-dirty');
}
	function getMasterVitals(){
		var addVitals = "";	
		
		$('#load2').show();
		$.ajax({
			type : "POST",
			url : serverPath + "/getMasterVitals",
			headers: {
		        "deviceId":"ADMIN",
		    },
		success : function(data){
				if(data.code=="200"){
					// var arr = [];
					$.each(data.content,function(index,option){
           
						
						
          /* var joindata=	'<select name="cars" id="cars">';
						
						for( var i=0;i<3;i++)
							
							{
							
						
							joindata+='<option >'+name +'</option>'
							    
							 
							
							
							}
					
						joindata=' </select>';
						
						var name = "Rakesh";*/
						/*  if(option.code == "Temp"){
			                    
				              addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="temphdr">'+
				                '<div style="display:inline-flex" class="info-box">'+
				                
				                      ' <div style="margin-left: 20px;padding-left:0px" class="info-box-content">'+
				                      ' <span class="info-box-text"><b>TEMPERATURE</b></span><br> '+
				                      '<select <span class="hereClass" style="font-size:17px;color:#000000" id="highTemp"></span></select><small id="bp	Ext" class="emptyExt"></small>'+
				                    '</div>'+
				                  '</div>'+
				                '</div>';
				                  
				          } */
				          if(option.code == "Temp"){
			                    
				              addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="temphdr">'+
				                '<div style="display:inline-flex" class="info-box">'+
				                
				                      ' <div style="margin-left: 20px;padding-left:0px" class="info-box-content">'+
				                      ' <span class="info-box-text"><b>TEMPERATURE</b></span><br> '
				                      + '<span style="font-size:20px;" class=""><b style="color:#000000;font-weight: normal;margin-left:-36px;padding-left:35px;" id="highTemp" class="hereClass">--</b>/<b style="color:#000000;font-weight: normal;" id="lowTemp" class="hereClass">--</b></span><small id="temoExt" class="emptyExt"></small>'+
				                    '</div>'+
				                  '</div>'+
				                '</div>';
				          }
				          if(option.code == "PUL"){
				              addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" id="puldr">'+
				                '<div class="info-box" style="display:inline-flex">'+
				                    '<div style="margin-left: 20px;" class="info-box-content">'+
				                    '<span class="info-box-text"><b>PULSE</b></span><br> '+
				                    '<span class="hereClass" style="font-size:20px;color:#000000" id="pulId">--</span><small id="pulExt" class="emptyExt"></small>'+
				                  '</div>'+
				               '</div>'+
				              '</div>';
				            } 
				            if(option.code == "SPO2"){
				                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="spo2dr">'+
				                      '<div class="info-box" style="display:inline-flex">'+
				                        '<div style="margin-left: 20px;" class="info-box-content">'+
				                          '<span class="info-box-text"><b>SPO2</b></span><br> '+
				                          '<span class="hereClass" style="font-size:20px;color:#000000" id="spo2Id">--</span><small id="spoExt" class="emptyExt"></small>'+
				                        '</div>'+
				                      '</div>'+
				                      '</div>';
				                  
				                          }
				            if(option.code == "BP"){
				                      addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="bphdr">'+
				                        '<div class="info-box" style="display:inline-flex">'+
				                             '<div style="margin-left: 20px;padding-left:0px" class="info-box-content">'+
				                                '<span class="info-box-text"><b>BLOOD PRESSURE</b></span><br>' +
				                                '<span style="font-size:20px;" class=""><b style="color:#000000;font-weight: normal;margin-left:-36px;padding-left:35px;" id="highBp" class="hereClass">--</b>/<b style="color:#000000;font-weight: normal;" id="lowBp" class="hereClass"></b></span><small id="bpExt" class="emptyExt"></small>'+
				                              '</div>'+
				                            '</div>'+	
				                          '</div>';
				                  }
				            
				            
				            if(option.code == "BG"){
				                addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="bghdr">'+
				                  '<div class="info-box" style="display:inline-flex">'+
				                    '<div style="margin-left: 20px;" class="info-box-content">'+
				                      '<span class="info-box-text"><b>BLOOD GLUCOSE</b></span><br>'+
				                      '<span class="hereClass" style="font-size:20px;color:#000000" id="bgId">--</span><small id="bgExt" class="emptyExt"></small>'+
				                      
				                   
				                   '</div>'+
				                      '</div>'+
				                  '</div>';
				                
				                      }
				          
				          
				          
        /*   if(option.code == "PUL"){
      addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" id="puldr">'+
        '<div class="info-box" style="display:inline-flex">'+
            '<div style="margin-left: 20px;" class="info-box-content">'+
            '<span class=""><b>PULSE</b></span><br><br> '+
            '<select <span class="hereClass" style="font-size:17px;color:#000000" id="pulId"></span></select><small id="pulExt" class="emptyExt"></small>'+
          '</div>'+
       '</div>'+
      '</div>';
    }
    if(option.code == "SPO2"){
            addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="spo2dr">'+
              '<div class="info-box" style="display:inline-flex">'+
                '<div style="margin-left: 20px;" class="info-box-content">'+
                  '<span class=""><b>SPO2</b></span><br><br> '+
                  '<select <span class="hereClass" style="font-size:17px;color:#000000" id="spo2Id"></span></select><small id="spoExt" class="emptyExt"></small>'+
                '</div>'+
              '</div>'+
              '</div>';
            
                  }
    if(option.code == "BP"){
              addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="bphdr">'+
                '<div class="info-box" style="display:inline-flex">'+
                     '<div style="margin-left: 20px;padding-left:0px" class="info-box-content">'+
                        '<span class=""><b>BLOOD PRESSURE</b></span> <br><br>' +
                        '<select <span class="hereClass" style="font-size:17px;color:#000000" id="highBp"></span></select><small id="bp	Ext" class="emptyExt"></small>'+
                        '</div>'+
                    '</div>'+
                  '</div>';
          }
      if(option.code == "BG"){
    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="bghdr">'+
      '<div class="info-box" style="display:inline-flex">'+
        '<div style="margin-left: 20px;" class="info-box-content">'+
          '<span class=""><b>BLOOD GLUCOSE</b></span><br> <br>'+
          '<select <span class="hereClass" style="font-size:17px;color:#000000" id="bgId"></span></select><small id="spoExt" class="emptyExt"></small>'+
          '<small id="bgExt" class="emptyExt"></small>'+
       
       '</div>'+
          '</div>'+
      '</div>'+
      '</div>';
    
          } */
    if(option.code == "WEI"){
              addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="weighthdr">'+
                  '<div class="info-box" style="display:inline-flex">'+
                       '<div style="margin-left: 20px;" class="info-box-content">'+
                        '<div style="display:inline-flex">'+
                          '<span class=""><b>WEIGHT</b></span>'+
                          '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(1);"></i>'+
                          '</div><br><br>'+
                          '<span class="hereClass" style="font-size:20px;color:#000000" id="weightId">--</span><small class="emptyExt"  id="weightExt"> kg</small>'+
                        '</div>'+
                      '</div>'+
                    '</div>';
            }
      if(option.code == "HEI"){
              addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="heighthdr" >'+
                  '<div class="info-box" style="display:inline-flex">'+
                       '<div style="margin-left: 20px;" class="info-box-content">'+
                       ' <div style="display:inline-flex">'+
                          '<span class=""><b>HEIGHT</b></span>'+
                      '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                          '</div> <br><br> '+
                          '<span class="hereClass" style="font-size:20px;color:#000000" id="heightId">--</span><small class="emptyExt" id="heightExt"></small>'+
                        '</div>'+
                      '</div>'+
                    '</div>';
            }
      if(option.code == "BMI"){
      addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" id="bmihdr" style="">'+
        '<div class="info-box" style="display:inline-flex">'+
              '<div style="margin-left: 20px;" class="info-box-content">'+
            '<span class=""><b>BMI</b></span><br> <br>'+
            '<span class="hereClass bmiValue" style="font-size:20px;color:#000000" id="bmiId">--</span><small id="bmiExt" class="emptyExt"> kg/m2</small>'+
          '</div>'+
        '</div>'+
      '</div>';
  }
  if(option.code == "FAT"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="fathdr" style="">'+
                      '<div style="display:inline-flex" class="info-box">'+
                      
                            ' <div style="margin-left: 20px;" class="info-box-content">'+
                            ' <span class="info-box-text"><b>FAT</b> </span> <br>'
                            + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-16px" id="fatId">--</span><small id="fatExt" class="emptyExt"></small>'+
                            
                          '</div>'+
                        '</div>'+
                      '</div>';
                }

    //1.muscle
    //2.bone
    if(option.code == "MUS"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="musIdHrd" style="">'+
                    '<div style="display:inline-flex" class="info-box">'+
                    
                          ' <div style="margin-left: 20px;" class="info-box-content">'+
                          ' <span class="info-box-text"><b>MUSCLE</b></span> <br>'
                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;"  id="musId">--</span><small id="musExt" class="emptyExt"> </small>'+
                          
                        '</div>'+
                      '</div>'+
                    '</div>';
              } 
    if(option.code == "BONE"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="boneIdHrd" style="">'+
                    '<div style="display:inline-flex" class="info-box">'+
                    
                          ' <div style="margin-left: 20px;" class="info-box-content">'+
                          ' <span class="info-box-text"><b>BONE</b></span> <br>'
                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;" id="boneId">--</span><small id="boneExt" class="emptyExt"> </small>'+
                          
                        '</div>'+
                      '</div>'+
                    '</div>';
              } 

						
		if(option.code == "WATER"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="waterhdr" style="">'+
                    '<div style="display:inline-flex" class="info-box">'+
                    
                          ' <div style="margin-left: 20px;" class="info-box-content">'+
                          ' <span class="info-box-text"><b>WATER CONTENT</b></span> <br>'
                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;" id="waterId">--</span><small id="waterExt" class="emptyExt"> </small>'+
                          
                        '</div>'+
                      '</div>'+
                    '</div>';
              }				
		
		//3.prot
	    if(option.code == "PRO"){
	                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="proteinIdHrd" style="">'+
	                    '<div style="display:inline-flex" class="info-box">'+
	                    
	                          ' <div style="margin-left: 20px;" class="info-box-content">'+
	                          ' <span class="info-box-text"><b>PROTEIN</b></span> <br>'
	                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;" id="proteinId">--</span><small id="proteinExt" class="emptyExt"> </small>'+
	                          
	                        '</div>'+
	                      '</div>'+
	                    '</div>';
	              } 	
	    //4.vfl
	    if(option.code == "VFL"){
	                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="vftIdHrd" style="">'+
	                    '<div style="display:inline-flex" class="info-box">'+
	                    
	                          ' <div style="margin-left: 20px;" class="info-box-content">'+
	                          ' <span class="info-box-text"><b>VISCERAL FAT LEVEL</b></span> <br>'
	                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;" id="vftId">--</span><small id="vftExt" class="emptyExt"> </small>'+
	                          
	                        '</div>'+
	                      '</div>'+
	                    '</div>';
	              } 
	    //5.Bm			
							
			if(option.code == "BM"){
	                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital"  id="bmIdHrd" style="">'+
	                    '<div style="display:inline-flex" class="info-box">'+
	                    
	                          ' <div style="margin-left: 20px;" class="info-box-content">'+
	                          ' <span class="info-box-text"><b>BASAL METABOLISM</b></span> <br>'
	                          + '&nbsp;&nbsp;&nbsp;&nbsp;<span class="hereClass" style="font-size:20px;color:#000000;margin-left:-36px;padding-left:25px;" id="bmId">--</span><small id="bmExt" class="emptyExt"> </small>'+
	                          
	                        '</div>'+
	                      '</div>'+
	                    '</div>';
	              }
	    
	    
			 if(option.code == "D-DIMER"){
	             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="dimmerdr" >'+
	                 '<div class="info-box" style="display:inline-flex">'+
	                      '<div style="margin-left: 20px;" class="info-box-content">'+
	                      ' <div style="display:inline-flex">'+
	                         '<span class="info-box-text"><b>D-DIMER</b></span>'+
	                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
	                         '</div> <br><br> '+
	                         '<span class="hereClass" style="font-size:20px;color:#000000" id="dimmerId">--</span><small class="emptyExt" id="dimmerExt"> cm</small>'+
	                       '</div>'+
	                     '</div>'+
	                   '</div>';
	           }
			
			//========================Blood Analyzer====================================================
	
				if(option.code == "CHOLES"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="chldr" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class=""><b>CHOLESTEROL</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="cholesId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 
		 if(option.code == "TRIGLY"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class=""><b>TRIGLYCERIDES</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="triglyId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 if(option.code == "HDL CHOLES"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>HDL CHOLESTEROL</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="hdlcholesId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "LDL CHOLES"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>LDL CHOLESTEROL</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="ldlcholesId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "CHOLES/HDL RATIO"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>CHOLESTEROL/HDL RATIO</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="ratioId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "DENGUE"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>DENGUE</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="dengueId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "HEPATITIES B"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>HEPATITIES B</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="hepatitiesId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "HIV"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>HIV</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="hivId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "HBA1C"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>HBA1C</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="hba1cId">--</span><small class="emptyExt" id="hba1ltExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "CHIKUNGUNYA"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class=""><b>CHIKUNGUNYA</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="chikungunyaId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "MALARIA"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>MALARIA</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="malariaId">--</span><small class="emptyExt" id=""> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "CRP"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="crprd" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>CRP</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="crpId">--</span><small class="emptyExt" id="crpExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "NS- 1"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>NS-1</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="nsId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "VitD"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>VitD</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="vitdId">--</span><small class="emptyExt" id="vitdExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "T4"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>T4</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="tId">--</span><small class="emptyExt" id="tExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "TSH"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>TSH</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="tshId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "DENGUE IgG"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>DENGUE IgG</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="DENGUEIgGId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "DENGUE IgM"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>DENGUE IgM</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="DENGUEIgMId">--</span><small class="emptyExt" id="heightExt"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "PCT"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>PCT</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="PCTId">--</span><small class="emptyExt" id=""> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "RR"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>RR</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="rrId">--</span><small class="emptyExt" id=""> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           }
		 
		 if(option.code == "T3"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>T3</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="T3Id">--</span><small class="emptyExt" id="T3Ext"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           
		 
        			}
		 if(option.code == "COVID"){
             addVitals+='<div class="col-md-3 col-sm-6 col-xs-12 checkVital" style=""  id="" >'+
                 '<div class="info-box" style="display:inline-flex">'+
                      '<div style="margin-left: 20px;" class="info-box-content">'+
                      ' <div style="display:inline-flex">'+
                         '<span class="info-box-text"><b>COVID</b></span>'+
                     '<i class=" " style="width: 130px; margin-left: 15px;cursor:pointer" onclick="promtfun(0);"></i>'+
                         '</div> <br><br> '+
                         '<span class="hereClass" style="font-size:20px;color:#000000" id="covidId">--</span><small class="emptyExt" id="T3Ext"> cm</small>'+
                       '</div>'+
                     '</div>'+
                   '</div>';
           
		 
        			}
		
		
        			});
					
					$("#addVitalsDet").append(addVitals);
					getEpisodeVitals(-1);
				}if(data.errorCode=="1010"){
					$('#load2').hide();
			    	toastr.warning("Server Error");
				}
			}
		});
  }
   
   function promtfun(ob){
	   if(ob == 0){
	    var height = prompt("Please enter your height(ft)", "");
       var arr = { "Height": height}; 
	   }
	   if(ob == 1){
		    var weight = prompt("Please enter your Weight(Kg)", "");
		       var arr = { "Weight": weight}; 
	   }
	   
	   var actHeight = $("#heightId").html();
	   var actWeight = $("#weightId").html();
       if (height != null && height !='' || weight != null && weight !='') {
	    	$.ajax({
				type : "POST",
				url : "saveEpisodeVitals?episodeId="+episdeId+"&patientId="+patientId+"&status=1&VitalArray="+JSON.stringify(arr),
				headers: {
			        "deviceId":"ADMIN",
			    },
			success : function(data){
				if(data.code=="200"){
					if(height != null && height !='' || weight != null && weight !=''){
						if(height != null && height !=''){
							actHeight = height;
						}
						if(weight != null && weight !=''){
							actWeight = weight;
						}
						
						var heightInMeter = actHeight*0.3048;
						var BMI = actWeight/(heightInMeter*heightInMeter);
			      		
			      		if(actHeight != null && actHeight != '' && actWeight != null && actWeight != ''){
			      			$("#bmihdr").css("display","block");
			      			$(".bmiValue").html('');
			      			$(".bmiValue").html(BMI);
			      			$("#bmiExt").html("Kg/m2");
			      		}
					}
					
		      		getEpisodeVitals(episdeId);
		      		getVitalDocument(episdeId);
		      		toastr.success("succesfully Edited");
				}
		
				}
			    ,error:function(x,y,response){
			    	$('#load2').hide();
			    	toastr.error("Server Error");
					
				}
			});
	    	
	    	
	    	
	    }else{
	    	/* toastr.warning("Please Enter the field"); */
	    }
	 
    }
	
	function getEpisodeVitals(id){
		$('#load2').show();
		var epi = "";
		var useId="";
		if(id=="-1"){
		epi = episdeId;
		 /* Please dont change this this is not logged in user id */
			//alert("use"+useId);
			//alert("user"+userId);
		}else{epi=id;}
		useId=userId; 
	//	$('.hereClass').html("--");
		$('.emptyExt').html("");
		$.ajax({
			type : "POST",
			url : serverPath+ "/getConsumerVitals?userId="+useId+"&episodeId="+epi,		
			headers: {
		        "deviceId":"ADMIN",
		    },
		success : function(data){
			if(data.code=="200"){
				var height=""; var weight = "";
				$.each(data.content,function(index,option){
					
					if(option.vitalsName.toLowerCase() == "Weight".toLowerCase()){
						$('#weightId').html(option.vitalsValue);
						$('#weightExt').html(" Kg");
						weight = option.vitalsValue;
					}
					if(option.vitalsName.toLowerCase() == "Height".toLowerCase()){
						$('#heightId').html(option.vitalsValue);
						$('#heightExt').html("cm");
						height = option.vitalsValue;
					}
		
					if(option.vitalsName.toLowerCase() == "Temperature".toLowerCase()){
						//var vitMeasureDateandTime =  option.strMonitoredDate;
					//	var time = vitMeasureDateandTime.substring(11,vitMeasureDateandTime.length-3);
						$('#highTemp').html(option.vitalsValue+'<b style="font-weight:normal;font-size:15px">C</b>');
						var lowtemp = option.vitalsValue * 9/5 + 32 ;
						$('#lowTemp').html(lowtemp.toFixed(1)+'<b style="font-weight:normal;font-size:15px">F</b>');
						$('#temphdr').show();
						
						//$('#highTemp').append('<option>'+(time + " - " +option.vitalsValue+'C'+ "/" +lowtemp.toFixed(1)+'F')+'</option>');
						
						}
					
				          if(option.vitalsName.toLowerCase() == "BMI".toLowerCase()){
				            $('#bmiId').html(option.vitalsValue);
				           
				          }
																																																																																																																																																																																																																																																																																							
					if(option.vitalsName.toLowerCase() == "Pulse".toLowerCase()){
						//var vitMeasureDateandTime =  option.strMonitoredDate;
						//var time = vitMeasureDateandTime.substring(11,vitMeasureDateandTime.length-3);
						$('#pulId').html(option.vitalsValue);
						$('#pulExt').html(" b/m");
						//$('#pulId').append('<option >'+(time + " - " +option.vitalsValue)+'<i style="font-weight:bold;font-size:2px"> b/m</i>'+'</option>');
						$('#puldr').show();
					}
					
					if(option.vitalsName.toLowerCase() == "BP".toLowerCase()){
					//	var vitMeasureDateandTime =  option.strMonitoredDate;
					//	var time = vitMeasureDateandTime.substring(11,vitMeasureDateandTime.length-3);
						var splt = (option.vitalsValue).split(",");
						$('#highBp').html(splt[0]);
						$('#lowBp').html(splt[1]);
						//bp = option.vitalsValue;
						$('#bpExt').html("mmhg");
						$('#bphdr').show();
						
						/* $('#highBp').append(
								'<option >'+(time + " - " +splt[0] + ' mmhg')+'</option>'); */
						
					}
				
					if(option.vitalsName.toLowerCase() == "BG".toLowerCase()){
					//	var vitMeasureDateandTime =  option.strMonitoredDate;
						//var time = vitMeasureDateandTime.substring(11,vitMeasureDateandTime.length-3);
						$('#bgId').html(option.vitalsValue);
						$('#bgExt').html("mg/dl");
						//$('#bgId').append('<option >'+(time + " - " +option.vitalsValue+'<small style="font-weight:normal;font-size:10px"> mg/dl</small>')+'</option>');
						$('#bghdr').show();
				}
					
					if(option.vitalsName.toLowerCase() == "RR".toLowerCase()){
						$('#rrId').html(option.vitalsValue);
												
					}
					
					if(option.vitalsName.toLowerCase() == "SPO2".toLowerCase()){
						//var vitMeasureDateandTime =  option.strMonitoredDate;
					//	var time = vitMeasureDateandTime.substring(11,vitMeasureDateandTime.length-3);
						$('#spo2Id').html(option.vitalsValue);
						$('#spoExt').html("%");
						// $('#spo2Id').append('<option >'+(time + " - " +option.vitalsValue+'<small style="font-weight:normal;font-size:5px"> %</small>')+'</option>');
						$('#spo2dr').show();
				}
					
					if(option.vitalsName.toLowerCase() == "FAT LEVEL".toLowerCase()){
					$('#fatId').html(option.vitalsValue.trim());
					$('#fatExt').html("%");
					
				}
					if(option.vitalsName.toLowerCase() == "WATER CONTENT".toLowerCase()){
					
					$('#waterId').html(option.vitalsValue);
					$('#waterExt').html("%");
					
				}

			        if(option.vitalsName.toLowerCase() == "MUSCLE".toLowerCase()){
			            $('#musId').html(option.vitalsValue);
			            $('#musExt').html(" Kg");
			            
			          }
			          if(option.vitalsName.toLowerCase() == "PROTEIN".toLowerCase()){
			            $('#proteinId').html(option.vitalsValue);
			            $('#proteinExt').html(" %");
			           // weight = option.vitalsValue;
			            
			          }
			          if(option.vitalsName.toLowerCase() == "VISCERAL FAT LEVEL".toLowerCase()){
			            $('#vftId').html(option.vitalsValue);
			          }
			          if(option.vitalsName.toLowerCase() == "BASAL METABOLISM".toLowerCase()){
			            $('#bmId').html(option.vitalsValue);
			            $('#bmExt').html(" Kcal");
			            
			          }
			          if(option.vitalsName.toLowerCase() == "BONE".toLowerCase()){
			            $('#boneId').html(option.vitalsValue);
			            $('#boneExt').html(" Kg");
			            
			          }
			          
			          //=============================================Blood Analyzer====================================================
			         
			          if(option.vitalsName.toLowerCase() == "D-DIMER".toLowerCase()){
			              $('#dimmerId').html(option.vitalsValue);
			              $('#dimmerExt').html(" ng/mL");
			            }
					  if(option.vitalsName.toLowerCase() == "T4".toLowerCase()){
			              $('#tId').html(option.vitalsValue);
			              $('#tExt').html("  �g/dL");
			            }
			          if(option.vitalsName.toLowerCase() == "TSH".toLowerCase()){
			              $('#tshId').html(option.vitalsValue);
			            }
			          if(option.vitalsName.toLowerCase() == "DENGUE IgG".toLowerCase()){
			              $('#DENGUEIgGId').html(option.vitalsValue);
			            }
			          if(option.vitalsName.toLowerCase() == "DENGUE IgM".toLowerCase()){
			        	 
			              $('#DENGUEIgMId').html(option.vitalsValue);
			            }
			          
			          if(option.vitalsName.toLowerCase() == "PCT".toLowerCase()){
			              $('#PCTId').html(option.vitalsValue);
			          }			          
			          if(option.vitalsName.toLowerCase() == "T3".toLowerCase()){
			              $('#T3Id').html(option.vitalsValue);
			              $('#T3Ext').html(" ng/mL");
			              
			            }
			          
			          if(option.vitalsName.toLowerCase() == "CHOLESTEROL".toLowerCase()){
			              $('#cholesId').html(option.vitalsValue.trim());
			              $('#chldr').show();
			            }
			          
			          if(option.vitalsName.toLowerCase() == "TRIGLYCERIDES".toLowerCase()){
			              $('#triglyId').html(option.vitalsValue);
			            }
			          
			          if(option.vitalsName.toLowerCase() == "HDL CHOLESTEROL".toLowerCase()){
			              $('#hdlcholesId').html(option.vitalsValue);
		            }
			          
			          if(option.vitalsName.toLowerCase() == "LDL CHOLESTEROL".toLowerCase()){
			              $('#ldlcholesId').html(option.vitalsValue);
			            }
			          
			          if(option.vitalsName.toLowerCase() == "CHOLESTEROL/HDL RATIO".toLowerCase()){
			              $('#ratioId').html(option.vitalsValue);
			          }			          
			          if(option.vitalsName.toLowerCase() == "DENGUE".toLowerCase()){
			              $('#dengueId').html(option.vitalsValue);
			            }
			          
			          
			          if(option.vitalsName.toLowerCase() == "HEPATITIES B".toLowerCase()){
			              $('#hepatitiesId').html(option.vitalsValue);
			            }
			          
			          if(option.vitalsName.toLowerCase() == "HIV".toLowerCase()){
			              $('#hivId').html(option.vitalsValue);
			            }
			          
			          if(option.vitalsName.toLowerCase() == "HBA1C".toLowerCase()){
			              $('#hba1cId').html(option.vitalsValue);
			              $('#hba1ltExt').html("%");
			            }
			          
			          if(option.vitalsName.toLowerCase() == "CHIKUNGUNYA".toLowerCase()){
			              $('#chikungunyaId').html(option.vitalsValue);
			            }
			          
			          if(option.vitalsName.toLowerCase() == "MALARIA".toLowerCase()){
			              $('#malariaId').html(option.vitalsValue);
			            }
			          
			          //Covid
			          if(option.vitalsName.toLowerCase() == "COVID".toLowerCase()){
			              $('#covidId').html(option.vitalsValue);
			            }
			          
			          if(option.vitalsName.toLowerCase() == "CRP".toLowerCase()){
			              $('#crpId').html(option.vitalsValue);
			              $('#crpExt').html(" mg/L");
			              
			            }
			          if(option.vitalsName.toLowerCase() == "NS- 1".toLowerCase()){
			              $('#nsId').html(option.vitalsValue);
			            }
			          if(option.vitalsName.toLowerCase() == "VitD".toLowerCase()){
			              $('#vitdId').html(option.vitalsValue);
			              $('#vitdExt').html("  ng/mL");
			            }
			          
			          $('#vitDate').html(option.strCreatedDate);
							});
							$('#load2').hide();
						}if(data.errorCode=="1009"){
							$('#load2').hide();
						}if(data.errorCode=="1010"){
							$('#load2').hide();
					    	toastr.warning("Server Error");
						}
			}
		    ,error:function(x,y,response){
		    	$('#load2').hide();
		    	toastr.error("Server Error");
				
			}
		});
		
		$('#load2').hide();
		
	}

	function getVitalDocument(id){
		var histIds = "";
		if(id == null || id == "-1"){
			histIds = episdeId;
		}else{
			histIds = id;
		}
		var addDoc = "";
		var addEcgDoc = "";
		$.ajax({
			type : "POST",
			url : serverPath+ "/getEcgReports?episodeId="+histIds,
			headers: {
		        "deviceId":"ADMIN",
		    },
			success : function(data){
				if(data.code=="200"){
					if(data.content != null){
					    $.each(data.content,function(index,option){
							if(option.filePath!=null){
								if(option.fileType!="SPO2" && option.fileType!="ecg"){
									var tagvar=option.fileType;
									var arr=tagvar.split(":");
									if(arr[1] == undefined || arr[1] == null || arr[1] == "null"){
										arr[1] = '' ;										
							        }
				             	    addDoc+='<div class="col-xs-12">'+
				                    '<span style="display:inline-flex;"> <b style="text-transform:uppercase;">'+arr[0].trim()+':</b>&nbsp;<p style="color:#0000FF;">'+arr[1]+'</p></span>'+
		    			            '<div class="info-box" style="display: inline-flex; padding-bottom: 10px;">'+
							        '<img  src="'+imagePath+option.filePath+'" >'+
						            '</div>'+
					                '</div>';
							    } else if(option.fileType == "ecg"){
							        addEcgDoc+='<div class="col-xs-12">'+
							        '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="'+imagePath+option.filePath+'" target="_blank"><b style="text-transform:uppercase;">ECG Report</b></a></span>'+
                                    '</div>';
							    }
				            }
				            $('#addDates').html(option.strCreatedDate);
			            });
					}
					$('#addEcgs').html(addDoc + addEcgDoc);
				}if(data.errorCode=="1090"){
					$('#load2').hide();
			    	//toastr.warning("No ECG Found");
				}if(data.errorCode=="1010"){
					$('#load2').hide();
			    	toastr.warning("Server Error");
				}
			}
		});
	}

	function logout(){
		window.location.href="logout";
	} 
	
   function	EmrReport(){
	  
	   var emrreport = serverPath+"/getEmrPdfReport?patientId="+patientId+"&userId="+userId+"&productId="+productId+"&episodeId="+episdeId;
	

	   window.open(emrreport);


   }
	
	function openMedicationModal(){
			
		$("#addMedicationModal").modal('show');
		
		var prescribedBy = $("#loggedInUserFullName").text();
		$("#prescribedByPre").parent().addClass('is-dirty');
		$("#prescribedByPre").val(prescribedBy);
	}
	function openObservationModal(){
		$("#addObservationModal").modal('show');
		
	}function openAdviceModal(){
		//$("#addObservationModal").modal('show');
		$("#addVisitForm").modal('show');
		var prescribedBy = $("#loggedInUserFullName").text();
		$("#prescribedByAdv").parent().addClass('is-dirty');
		$("#prescribedByAdv").val(prescribedBy);
		
	}
	
	function openNotesModal(){
		$("#addNotesModal").modal('show');
	}

	function openReportModal(){
		$("#addReportModal").modal('show');
		$("#firstmedfile").val('');
	}
	function acceptType(ob){

		if(ob.files[0].type == "image/jpeg" || ob.files[0].type == "application/vnd.ms-excel" || ob.files[0].type =="application/pdf" 
			|| ob.files[0].type =="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || ob.files[0].type =="image/png"){

			}else{
				  $("#firstmedfile").val('');
				  /* mes = "File format not supported";
				  $('#demo-show-snackbar').trigger('click'); */
				 // alert("File format not supported");
				
				}
	}
	var count=0;
    function getOtherMedFile(){
  	  var element1="";
  	  var filecountid='';
  	  
        if($("#firstmedfile").val() != '')
        {
	      	count++;
	    	element1 = '<p style="display:inline-flex;margin-bottom: 5px;" ><i class="fa fa-paperclip" style="padding-top: 5px;margin-left: 15px;color:black;padding-right: 10px"></i> '
	         +'<input type="file" id="filecountid'+count+' "class="browserfield" name="files" onchange="" style="margin-bottom: 0px; border-radius: 4px; font-family:Roboto Medium;font-size:15px;color:#5498D2;border:#5498D2 1px solid">'
	         +'<i style="cursor: pointer;color:black; padding-top: 7px;" class="fa fa-times" onclick="removeFiles(this)"></p>';
        }
     
        var checkFile = false;
    	$(".browserfield").each(function(){
    	    if($(this).val()!=""){
    		      
    		 }else{
    		         checkFile = true;
    			}
    	});
    	if(checkFile!=true){ 
	       $("#addFilesMed").append(element1);
		}
   
    }
	medData = '';
	function addMedication(){
		
		var valName = $("#medicationNameId").val();
		var medicationId = $("#medicationIdFromDropdown").val();
		var valDosage = $("#dosageID").val();
		if(valName != ''){
		if(medicationId == null || medicationId == ""){
			//alert();
			 $.ajax({
				type : "POST",
				url: "addMedication?medicationName="+valName,
				
				success : function(data) {
					if(data.code=="200"){
						medicationId = data.content.id;
					}
				}
		 	});
		 	//alert("here");
		} 
		}
		

		var valFreq = $("#fequencyId").val();
		var valInst = $("#instructionId").val();
		var valStart = $("#startId").val();
		var valDays = $("#days").val();
		var valComm = $("#commentsMedId").val();

		 if(valName != '' && valStart != '' && valDays != ''){
			 
			$(".addedMedTable").show();
			var medData = '<tr><td class="getJson" style="display:none" attr="medicationId">'+medicationId+'</td><td class="getJson" attr="medicationName">'+valName+'</td>'
							+'<td class="getJson" attr="dosage">'+valDosage+'</td><td class="getJson" attr="frequency">'+valFreq+'</td>'
							+' <td class="getJson" attr="instructions">'+valInst+'</td><td class="getJson" attr="beginDate">'+valStart+'</td>'
							+'<td class="getJson" attr="Days">'+valDays+'</td><td class="getJson" attr="comment">'+valComm+'</td> '
							+'<td><i style="cursor:pointer" class="fa fa-times" aria-hidden="true" onclick="removeFileMed(this)"></i></td></tr>';
			
			$("#medAdd").append(medData);
			$('.Newpolic1').val('');
			$('.Newpolic1').parent().removeClass('is-dirty');
			$('#fequencyId').val('0-0-1');
			$('#instructionId').val('Before Food');
		 }else{
				toastr.warning('Please fill mandatory fields');
			 }
		
		
		
	}
	function removeFileMed(ob){
		 
		$(ob).parent().parent().remove();
		
		if($("#medAdd tr").length < 1){
			$('#dynTableMed').hide();
		}
		if($("#invAdd tr").length < 1){
			$('#dynTableInv').hide();
		}
	}
	function addmedicationjson(){
		var isexist=0;
	 	 var ordr=1;
	 	dditionalDetailJson =  '';additonalDetails = '';
		 medicationJSON = [];MedicationFinalJSON = [];
		$(".getJson").each(function() {
	 		//alert($(this).val());
			//console.log($(this).attr("attr"));
 			 
 			if($(this).attr("attr")=="medicationName"){
 				 medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
 			 if($(this).attr("attr")=="dosage"){
 				 medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="frequency"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="instructions"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="beginDate"){
 				$('#strPrescribedDate').val($(this).text());
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="Days"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			} 
	 		if($(this).attr("attr")=="comment"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}   
 			if($(this).attr("attr")=="medicationId"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text() });
 			} 
 			//console.log(ordr);
 			if(ordr%8==0 && ordr>1){	
 				//console.log("inside"+ordr);
 				medicationJSON.push({keyName : "id",value : 0 });
 			 	medicationJSON.push({keyName : "status",value : 1 });
 			 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
 			 medicationJSON.push({keyName : "medicalReportId",value : -1 });
 			 console.log(medicationJSON);
 			 MedicationFinalJSON.push(medicationJSON);
 			 medicationJSON=[];
 			}	 
 		 ordr++;
 		 });
	} 
	function addAdvicejson(){
		datetime = '';
		datetime = $("#fromDate").val();
		additionalDetailJson =  '';additonalDetails = '';
	 medicationJSON = [];MedicationFinalJSON = [];
		$(".getAdvice").each(function() {
	 		
			//console.log($(this).attr("attr"));
			if($(this).attr("name")=="investigationName"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
 			if($(this).attr("name")=="prescribedBy"){
 				 medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
 			 if($(this).attr("name")=="InvestigationType"){
 				 medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		 if($(this).attr("name")=="dateTime"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		 if($(this).attr("name")=="Labcomments"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		if($(this).attr("name")=="investigationId"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		
	 		
	 		
 		 
 		 //ordr++;
		 
 		 });
		
		medicationJSON.push({keyName : "id",value : 0 });
	 	medicationJSON.push({keyName : "status",value : 1 });
	 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
		 medicationJSON.push({keyName : "medicalReportId",value : -1 });
		 console.log(medicationJSON);
		 MedicationFinalJSON.push(medicationJSON);
		 medicationJSON=[];
	} 
	
	var currentdate = new Date();
	/* var datetime = currentdate.getDay() + "/"+currentdate.getMonth() 
	+ "/" + currentdate.getFullYear(); */
	var datetime = currentdate.getDate() + "/" + (currentdate.getMonth()+1) + "/" + currentdate.getFullYear();
	//var dateAdvice = $("#").val();
	//alert(datetime);
	
	//alert(datetime);
	function addNotesjson(){
		 //alert( $("#noeComments").val());
		additionalDetailJson =  '';additonalDetails = '';
		 medicationJSON = [];MedicationFinalJSON = [];
		$(".getNotes").each(function() {
	 		if($(this).attr("name")=="comments"){
	 			medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 				//alert($(this).val());
 				var notCom = $(this).val();
 				//alert(notCom);
 				$("#noteComments").val(notCom);
 				//alert($("#noteComments").val())
 			}
	 		medicationJSON.push({keyName : "addedTime",value : datetime });		 
 		 });
		
		medicationJSON.push({keyName : "id",value : 0 });
	 	medicationJSON.push({keyName : "status",value : 1 });
	 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
		 medicationJSON.push({keyName : "medicalReportId",value : -1 });
		 console.log(medicationJSON);
		 MedicationFinalJSON.push(medicationJSON);
		 medicationJSON=[];
	}
	function addReportjson(){
		additionalDetailJson =  '';additonalDetails = '';
		 medicationJSON = [];MedicationFinalJSON = [];
		
		$(".getReport").each(function() {
	 		if($(this).attr("name")=="files"){
	 			medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 				//alert($(this).val());
 			}
	 		medicationJSON.push({keyName : "addedTime",value : datetime });		 
 		 });
		
		medicationJSON.push({keyName : "id",value : 0 });
	 	medicationJSON.push({keyName : "status",value : 1 });
	 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
		 medicationJSON.push({keyName : "medicalReportId",value : -1 });
		 MedicationFinalJSON.push(medicationJSON);
	}
	
	var medicationJSON = [];
	var MedicationFinalJSON = [];
	prefixStrForPrescription='{\"additionalDetail\":'; 
	additionalDetailJson =  '';
	additonalDetails = '';
	
	function saveMedication(ob){
		//alert($(ob).val());
		$("#additionalDetailJson").val('');
		additionalDetailJson =  '';additonalDetails = '';
	 	 medicationJSON = [];MedicationFinalJSON = [];
	 	 var isexist=0;
	 	 var ordr=1;
	 	// alert(episodeId);
	 	 //alert($("#noteComments").text());
	 	 $("#medEpisodeId").val(episdeId);
	 	 if(ob==1){
	 		 var medName = $("#medicationNameId").val();
	 		 var stDaMAn = $("#startId").val();
	 		var stDaysMAn = $("#days").val();
	 		/* if(medName != ''||stDaMAn != ''||stDaysMAn != ''){ */
	 		addmedicationjson(); 
	 		$('#documentTypeId').val(1);
	 		$('#serviceNameValue').val(null);
	 		/* }else{
				toastr.warning('Please fill mandatory fields');
			} */
	 	 }
	 	 if(ob==2){
	 		var serviceD = $("#serviceTypeId").val();
	 		var servDat = $("#fromDate").val();
	 		var advicePres = $("#prescribedByAdv").val();
	 		 if(serviceD != "" && servDat != "" && advicePres != ""){
	 		addAdvicejson();
	 	    
	 		var date = new Date().toJSON().slice(0, 10).split("-").reverse().join("/");
			console.log(date);
	 		$('#strPrescribedDate').val(date); 
	 		$('#documentTypeId').val(2);
	 		//$('#serviceNameValue').val($('#serviceNameId').val());
	 		 }else{
					toastr.warning('Please fill mandatory fields');
				}
	 	 } 
	 	if(ob==3){
	 		notMan = $("#noeComments").val();
	 		//alert(notMan);
	 		if(notMan != ''){ 
	 		addNotesjson();
	 		var date = new Date().toJSON().slice(0, 10).split("-").reverse().join("/");
			console.log(date);
	 		$('#strPrescribedDate').val(date); 
	 		$('#documentTypeId').val(8);
	 		//$('#serviceNameValue').val($('#serviceNameId').val());
	 		 }else{
				toastr.warning('Please fill mandatory fields');
			}
	 	 }
		if(ob==4){
			var fileMan=$(".getReport").val();
			//alert(fileMan);
			 if(fileMan != ''){
			addReportjson();
			var date = new Date().toJSON().slice(0, 10).split("-").reverse().join("/");
			console.log(date);
	 		$('#strPrescribedDate').val(date); 
			$('#documentTypeId').val(9);
			}else{
				toastr.warning('Please attach report');
			} 
		}
		
		 $('#dynTableMed').show();
	 	
	 	  if(MedicationFinalJSON.length > 0){
	 	        var additonalDetails = prefixStrForPrescription+JSON.stringify(MedicationFinalJSON)+"}";
	 	        $("#additionalDetailJson").val(additonalDetails);
	 	       medicationJSON = [];MedicationFinalJSON = [];
	 	    } 
	
			data = $("#additionalDetailJson").val();
			console.log(data);
			//alert(data);
		 	  if(data != null && data != ''){
		 	 
	 		 var form1 = document.getElementById('saveMedicationId');
	 		 var formData = new FormData(form1);
	 			
	 		
	 		  //alert();
	 		  
	 		$.ajax({
	 			type : "POST",
	 			url : serverPath+"/addDocuments",
	 			headers: {
	 		        "deviceId":"ADMIN",
	 		    },

	 			data: formData,
	 			dataType:"json",
	 		    mimeType: "multipart/form-data",
	 			async : false,
	 		    cache : false,
	 			contentType : false,
	 		    processData : false,
	 			success : function(data){
						//alert(data.code);
	 				if(data.code="200"){
	 					
	 				
	 					$("#additionalDetailJson").val('');
	 					
	 					toastr.success('Added Successfully','');
	 					
	 					canMed();
	 					clearAdvModal();
	 					canNote();
	 					clearRep();
	 					/* $('.modal').modal('hide');
	 					$(".getAdvice").val('');
	 					$('.getAdvice').parent().removeClass("is-dirty"); */
	 					getHistoryOfUser(episdeId);
	 					
	 				
	 					}
	 				}

              });
	 		
	 		
	 	    }else{
		 	  
	 	    	 $('#dynTableMed').hide();
		 	     mes = "Please Add the mandatory data";
			     $("#demo-show-snackbar").trigger('click');
				}

	}
	

	
function getPatientDetails(){
	var addDoc = "";var other = "";var gender = "";var mob = "";var address="";var email = "";var addaDet = "";var age = '';
	var uhid = "";
	$.ajax({
		type : "POST",
		url :  serverPath + "/getPatientDetails?patientId="+patientId,
		headers: {
	        "deviceId":"ADMIN",
	    },
	success : function(data){
	if(data.code=="200"){
        var option = data.content;
		var patName = "";
				if(option.patientFirstName !=null){
					patName+=option.patientFirstName;
				}
				if(option.patientLastName !=null){
					patName+=" "+option.patientLastName;
				}
				if(option.bloodGroup !=null){
					other = option.bloodGroup;
					
				}if(option.gender =="Male"){
					gender = '<img src="resources/images/male.png" style="width:12px">';
					
				}if(option.gender =="Female"){
					gender = '<img src="resources/images/femenine.png" style="width:12px">';
					
				}if(option.mobileNo !=null){
					mob = option.mobileNo;
				}
				if(option.address !=null){
					address = option.address;
				}
				if(option.emailId !=null){
					email = option.emailId;
				}
				if(option.uhid != null){
				    uhid = option.uhid;
				}
				age = option.age;

			$('#addPrfName').html(patName);
			$('#uhidId').html(uhid);
			 addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;font-size:12px">'+
				'<b>'+age+' / '+other+' / '+gender+' </b>'+ 
				'</li>';
			if(mob!=""){
				 addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;font-size:12px">'+
					'<b>'+mob+' </b>'+ 
					'</li>';
			}if(address!=""){
				 addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'+
					'<b>'+address+' </b>'+ 
					'</li>';
			}if(email!=""){
				 addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'+
					'<b>'+email+' </b>'+ 
					'</li>';
			}	
			
			$('#addDet').html(addaDet);
	}if(data.errorCode=="1010"){
		$('#load2').hide();
    	toastr.warning("Server Error");
	}
		}
	});
}
	
var histId = "";
	function getHistoryOfUser(id){		
		console.log("#"+id);
		histId = "";
		
		if(id == null || id == "undefined"){
			//alert(episodeId);alert(episdeId);
			histId = episodeId;
		}else{
			
			histId = id;
			//jk
			episdeId=id;
			
			$(".normalCol").css("background","#B8B8B8");
			//$("#id").remove("class","btn-primary");
			$("#"+id).css("background","#367fa9");			
		}
		//alert(histId);
	 getEpisodeVitals(histId);
	 getVitalDocument(histId);

	$("#prescriptionTableBody").html('');$("#notesTableBody").html('');
	$("#adviseTableBody").html('');$("#reportsTableBody").html('');
	var trStart = '<tr style="width:100%;">';
	var trEnd = '</tr>';
	
	
		$.ajax({
			type : "POST",
			url : serverPath+"/getDetailsByEpisodeIdv3?EpisodeId="+histId,
			headers: {
		        "deviceId":"ADMIN",
		    },
		    dataType: "json",
			type: "GET",
			success : function(data){
				if(data.code=="200"){
					//style="width:50%"	
					var presList = "";var adviseList ="";var notesList = "";var reportList="";var reporfilename='';
					//For Prescription
					$.each(data.content,function(index,option){
						//alert(option.documentTypeId);
						if(option.documentTypeId == 1){
							//presList += trStart;
						if(option.medicationDetails != null && option.medicationDetails != 0){
						
					$.each(option.medicationDetails,function(index1,option1){
						
							//var obj = jQuery.parseJSON(option1.details);
							if(option1.medicationName !=''){
								
								var MedName = option1.medicationName;
							}
							if(option1.frequency !=''){
								
								var Freq = option1.frequency;
							}
							if(option1.strBeginDate !=''){
								
								var Start = option1.strBeginDate;
							}
							
							if(option1.totalNoOfDays != null && option1.totalNoOfDays > '1' && option1.totalNoOfDays != '0'){
								
								var Days = option1.totalNoOfDays +'days';
							}
							if(option1.totalNoOfDays != null && option1.totalNoOfDays <= '1' && option1.totalNoOfDays == '1'&& option1.totalNoOfDays != '0'){
								
								var Days = option1.totalNoOfDays +'day';
							}

							
							presList+='<tr><td>'+MedName+'</td><td style="text-align:center">'+Freq+'</td><td>'+Start+'</td><td>'+Days+'</td></tr>';
							
							/* var forum = obj.additionalDetail;
							 if(option.documentTypeName == "Prescription"){
					        	
					        } strBeginDate
							 for (var i = 0; i < forum.length; i++) {
							    var object = forum[i]; 
							    
							    for (property in object) {
							        var value = object[property];
							        //console.log(value.keyName+"--"+value.value);
							        
							        if(value.keyName == "medicationName"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'</td>';
								        }
							        if(value.keyName == "frequency"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        if(value.keyName == "beginDate"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'</td>';
							        } 
							        if(value.keyName == "Days"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'Days</td></tr><tr>';
							        }
							    }  
							   // presList += trEnd;	
							} 
							//alert(option.documentTypeName);
							if(option.documentTypeName == "Prescription"){
					       	presList += trEnd;						        	
					        }*/
						    $("#prescriptionTableBody").html(presList);
							
						
						});
						}
						} 
					
					//For Advise/Investigation
					
					if(option.documentTypeId == 2){	
						//alert(option.investigationDetails);
					if(option.investigationDetails != null && option.investigationDetails != 0){
						var LabCom = '';var CrPB = '';
					$.each(option.investigationDetails,function(index2,option2){
						//alert(option.documentTypeId);
						
							/* var obj = jQuery.parseJSON(option.details);
							var forum = obj.additionalDetail;
							if(option.documentTypeName == "Investigation"){
					        	adviseList += trStart;
					        }
							for (var i = 0; i < forum.length; i++) {
							    var object = forum[i];
							    
							    for (property in object) {
							        var value = object[property];
							        //console.log(value.keyName+"--"+value.value);
							        if(value.keyName == "servicesName"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        if(value.keyName == "prescribedBy"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        if(value.keyName == "comments"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        if(value.keyName == "createdAt"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        
							    }  
							}
							if(option.documentTypeName == "Investigation"){
								adviseList += trEnd;						        	
					        } */
					        
						if(option2.serviceName !=''){
							
							var SerName = option2.serviceName;
						}
						if(option2.comments !=''){
							
							 LabCom = option2.comments;
						}else{
							
							LabCom = '';
						}
						if(option2.strPrescribedDate !=''){
							
							var CrDate = option2.strPrescribedDate;
						}
					if(option2.prescribedBy !=''){
							
							CrPB = option2.prescribedBy;
						}else{
							
							CrPB = '';
						}
						/* 
						if(option1.totalNoOfDays != undefined || option1.totalNoOfDays != ''){
							
							var Days = option1.totalNoOfDays;
						}
 */
						
 						adviseList+='<tr><td width="30%";>'+SerName+'</td><td style="">'+CrPB+'</td><td >'+CrDate+'</td><td>'+LabCom+'</td></tr>';
						
						    $("#adviseTableBody").html(adviseList);
					
 				
					});
					}
					}
					
					//For Notes
					if(option.documentTypeId == 8){	
					if(option.notesDetails != null && option.notesDetails != 0){
						
					$.each(option.notesDetails,function(index3,option3){
						//alert(option.documentTypeId);
						/* if(option.documentTypeId == 8){
							var obj = jQuery.parseJSON(option.details);
							var forum = obj.additionalDetail;
							if(option.documentTypeName == "Notes"){
								notesList += trStart;
					        }
							for (var i = 0; i < forum.length; i++) {
							    var object = forum[i];
							    
							    for (property in object) {
							        var value = object[property];
							        //console.log(value.keyName+"--"+value.value);
							        
							        if(value.keyName == "comments"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	notesList += '<td style="width:85%">'+value.value+'</td>';
							        }
							        if(value.keyName == "addedTime"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	notesList += '<td style="width:15%">'+value.value+'</td>';
							        }
							         
							    }  
							}
							if(option.documentTypeName == "Notes"){
								notesList += trEnd;						        	
					        } 
						    $("#notesTableBody").html(notesList);
						 } */
						if(option3.strBeginDate !=''){
							
							var NotDate = option3.strBeginDate;
						}
						 
						if(option3.comments !='' && option3.comments != undefined && option3.comments!= ','){
							
							var Note = option3.comments;
						}
						if(option3.createdBy != null){
							
							var notCB = option3.createdBy;
						}
						/* if(option3.createdOn != null){
							
							var notCO = option3.createdOn;
							var now = notCO;
							alert( now.customFormat( "#DD#/#MM#/#YYYY# #hh#:#mm#:#ss#" ) );
						}  */
						notesList+='<tr><td width="40%";>'+Note+'</td><td class="text-left" width="30%";>'+NotDate+'</td></tr>';
						
						$("#notesTableBody").html(notesList);
					});
				}
			}
					
					//For Report
					if(option.documentTypeId == 9){	
						
						if(option.filePath != null){
							var	x =  option.filePath +'';
							var splt = x.split("/");
							var splRep = splt[splt.length-1];
							reportList += '<tr><td style="cursor:pointer;" "width:100%" onclick="openFileLocation(&quot;'+x+'&quot;);">'+splRep+'</td></tr>';
						
						}
			
						
				$("#reportsTableBody").html(reportList);
				
					}
				 });
		
				}
				
			}
		});
	}

	function setFileName(input){
		$("#showReportFileName").text($(input).val());
	}
	function openFileLocation(filePathLocation){
		window.open(imagePath+filePathLocation);
	}
/*
	
	function initiateCall(){
		var proId = localStorage.getItem("productId");
    	if(proId == 1310){
    		alert("This Service is not enabled, Please contact Wenzins");	
    	} else {
        	var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
         	win.addEventListener("resize", function(){
        		win.resizeTo(1024, 768);
         	});
    	}
	}
*/
function initiateCall(calleProfileId){
	var proId = localStorage.getItem("productId");
	if(proId == 1310){
		alert("This Service is not enabled, Please contact Wenzins");	
	} else {
    	/* var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
    	win.addEventListener("resize", function(){
   	 		win.resizeTo(1024, 768);
    	}); */
		$.ajax({
    		type : "POST",
    		url :   serverPath + "/intiateAndCloseCallv4?profileId="+localStorage.getItem("profileId")+"&calleProfileId="+calleProfileId+"&serverKeyId="+localStorage.getItem("serverKeyId")+"&initiate="+true,
    		success : function(data){
    		//	console.log(data);
    			 roomid = data.roomid;
    		//	alert(data);
    		//	console.log("new  "+data.content.weconurl);
    			
    			videocalllink =  data.content.weconurl + data.roomid ;
    			
    		/* 	 $.each(data.content,function(index, option) {	
    				 console.log(option);
    				 var url = option.weconurl;
    			//	 alert(url);
    				 console.log("link"+url);
    				 var win = window.open(url); 
    				  videocalllink =  url + roomid ;
    		//	var win = window.open(videocalllink , 'result', 'width=1024,height=768'); 
    	 }); */
    		//	 window.open(a);
    			 var win = window.open(videocalllink,'result', 'width=300,height=300');
    			
    			 win.addEventListener("resize", function(){
          	 		win.resizeTo(1024, 768);
          	 	});
    		}
    	});	
	}
}
	
	function canMed(){
		
		$("#addMedicationModal").modal('hide');
		 //dosageID startId days commentsMedId
		 $(".bClassM").val('');
		 $(".bClassM").parent().removeClass('is-dirty');
		 $('#fequencyId').val('0-0-1');
		$('#instructionId').val('Before Food');
		$("#dynTableMed").hide();
		$("#medAdd").empty();
		 /* $("#startId").val('');
		 $("#dosageID").val('');
		 $("#days").val('');
		 $("#commentsMedId").val(''); */
		 
	}
	//=======================NEXT AND PREVIOUS BUTTON FOR EPISODE============================
	function next(id){
		
		if(test==s-2){
			document.getElementById("nextId").disabled = true;	
		}
		
		
		document.getElementById("prevId").disabled = false;
			
		test=test+1;
		var previous=test-4;
			
			document.getElementById(previous).style.display = "none";
			document.getElementById(test).style.display = '';
			
			document.getElementById("prevId").style.display = "";           //to display prev button
		}
		
	
	function previous(id){
		
		document.getElementById("nextId").disabled = false;	
			
			 if(test==4){
				document.getElementById("prevId").disabled = true;
			} 
			
			var previous=test-4;
		
				document.getElementById(test).style.display = 'none';
				 document.getElementById(previous).style.display = ''; 
				
			test=test-1;
			}
	//============================================================================================================
	
		function canNote(){
		//alert();
		$("#addNotesModal").modal('hide');
		//$('.getNotes').text('');
		$('textarea').val('')
	}
	function clearRep(){
		$("#addReportModal").modal('hide');
		$('#reportFileId').val('');
		$(".getReport").empty();
		$('#showReportFileName').text('');
	}
	function reportAdvice(episodeId,docId,id)
	   {
		// alert("Fdgfg");
		  var appendPrescriptionImage = ""; 
		  if(docId==1){
			  $("#addreport").html('');
			  $(".preReport").each(function(){
				  
				  var spltAttr = $(this).attr("attr");
				  if(spltAttr==id){
				  var splitHere =  $(this).val().split("/");
				  var splitHere1 = $(this).val().split(","); 
				  
               appendPrescriptionImage+= "<br><a class='text-left' path='&quot;"+splitHere1+"&quot'' "
               +"style='cursor:pointer;padding-top:0px; padding-bottom: 5px;color:rgb(63, 81, 181);"
               +"font-size: 15px;' onclick='javascript:showInOtherPage("+id+",&quot;"+splitHere1+"&quot;)' "
               +" >"+splitHere[splitHere.length-1]+"</a>";
				  }
 		  });
			  
			  $("#addreport").html(appendPrescriptionImage);
			  
			 
			  if(appendPrescriptionImage==""){
 			  
			    mes = "No records found";
				$('#demo-show-snackbar').trigger('click');
		  }else{
				$("#reportAdvice").modal('show');
		  }
		  }
		  
		
		  if(docId==2){
			  $("#addreport").html(''); var appendPrescriptionImage = ""; 
			  $(".repReport").each(function(){
				  var spltAttr = $(this).attr("attr");
				  if(spltAttr==id){
				  var splitHere =  $(this).val().split("/");
				  var splitHere1 = $(this).val().split(",");
				  
				  
				  
               appendPrescriptionImage+= "<br><a class='text-left' path='&quot;"+splitHere1+"&quot'' "
               +"style='cursor:pointer;padding-top:0px; padding-bottom: 5px;color:rgb(63, 81, 181);"
               +"font-size: 15px;' onclick='javascript:showInOtherPage("+id+",&quot;"+splitHere1+"&quot;)' "
               +">"+splitHere[splitHere.length-1]+"</a>";
				  }
				  
				  
				  
 		  });
			  
			  $("#addreport").html(appendPrescriptionImage);
			  
			  if(appendPrescriptionImage == ""){
				  console.log(appendPrescriptionImage);
				    mes = "No records found";
   				$('#demo-show-snackbar').trigger('click');
   		  }else{
   				$("#reportAdvice").modal('show');
   		  }
			
		  }
		
		  
}
	
</script>
<!-- <tr>
	<td>4.</td>
	<td>Fix and squish bugs</td>
	<td>
 		<div class="progress progress-xs progress-striped active">
   			<div class="progress-bar progress-bar-success" style="width: 90%"></div>
 		</div>
	</td>
	<td><span class="badge bg-green">90%</span></td>
</tr> -->
</html>
