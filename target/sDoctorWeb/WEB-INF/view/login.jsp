<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="/WEB-INF/view/includes.jsp" %>

    <head>
        <meta charset="UTF-8">
        <title>Login</title>

        <link rel="stylesheet" href="resources/css/bootstrap.min.css" />

        <link href="resources/css/AdminLTE.css" rel="stylesheet" />
        <link href="resources/css/_all-skins.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="resources/css/font-awesome.min.css" />
        <link rel="stylesheet" href="resources/css/material.css">
        <link href="resources/css/toastr.min.css" rel="stylesheet">

        <script src="resources/js/jQuery-2.1.4.min.js"></script>
        <script src="resources/js/toastr.min.js"></script>
        <script src="resources/js/customHTML.js"></script>
        <style>
            .navbar-t-centered {
                position: absolute;
                left: 34%;
                display: block;
                text-align: center;
                color: #fff;
                top: 0px;
                padding: 15px;
                font-weight: 400;
                font-size: 12px;

            }

            @media screen and (min-width:768px) {
                .navbar-t-centered {
                    position: absolute;
                    left: 38%;
                    display: block;
                    /*  width: 160px; */
                    text-align: center;
                    color: #fff;
                    top: 0px;
                    padding: 15px;
                    font-weight: 400;
                    font-size: 22px;

                }
            }
        </style>
    </head>

    <body id="bodyId">
        <header class="main-header"> <!-- Logo --> <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation"
                style="margin-left:0% !important ;max-height:50px; background-color:#3c8dbc">
                <!-- Sidebar toggle button-->
                <img src="resources/images/WriztoLogoBox.jpg" class="user-image" alt="User Image"
                    style="height: 50px; padding-left: 5px; margin-left: 5px;">


                <span class="navbar-t-centered">Wrizto Healthcare System</span>


            </nav>
        </header>

        <div class="row" style="margin-top:8%;padding-bottom:50px;">
            <div class="col-md-12">
                <div class="col-md-2">
                </div>
                <!-- <div class="col-md-3"></div> -->
                <div class="col-md-4" id="logRegId"
                    style="padding: 0px; color: #fff; border-radius: 20px 20px 20px 20px; margin-bottom: 4%; opacity: 1; filter: alpha(opacity = 100);">

                    <div class="" style="border-top-width: 0px; padding: 0px; margin-top: 30px;">
                        <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">



                            <div class="" id="loginDiv" style="">


                                <form class="form-inline" role="form" id="loginForm" name="user_login" method="post">
                                    <input type="hidden" name="productTypeCode" value="VK" id="productTypeCode">
                                    <div class=""
                                        style="height: 100%; border-width: 0px 0px 0px; border-style: solid; border-color: rgb(217, 217, 217); -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; border-image: none;">
                                        <div class="form-group col-xs-12"
                                            style="padding-bottom: 15px; padding-top: 15px; margin-bottom: 0px;">
                                            <div class="row">
                                                <!-- <div class="col-xs-2 col-md-2"></div> -->
                                                <div class="col-xs-12 col-md-12" style="color: rgb(60, 141, 188);">
                                                    <h4>Doctor Login</h4>
                                                    <!-- <p style="color: rgb(60, 141, 188); font-size: 18px;margin-left: 97px; margin-top: -5px;">Doctor</p> -->
                                                </div>
                                            </div>
                                            <!-- <div class="row" style="margin-top: 12%;">
											<div class="col-xs-2 col-md-2"></div>
											<div class="col-xs-6 col-md-6">
												<span>
													<p style="color: #868a8a; font-size: 22px;padding-left: 5px;">LOGIN</p>
												</span>
											</div>
										</div> -->
                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                <!-- <div class="col-xs-2"></div> -->
                                                <div class="col-xs-12" style="padding-left: 10px; padding-right: 0px;">
                                                    <div style="width: 100%; color: black"
                                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input autocomplete="off" autocorrect="off"
                                                            placeholder="USERNAME" spellcheck="false"
                                                            class="mdl-textfield__input error-popover" type="text"
                                                            id="j_username" name="userName">
                                                    </div>


                                                </div>
                                                <div class="col-xs-12" style="padding-right: 0px;padding-left: 10px;">
                                                    <!-- <div class="col-xs-2"></div> -->
                                                    <div class="col-xs-12"
                                                        style="padding-left: 0px; padding-right: 0px;">
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
                                                            style="width: 100%; color: black">
                                                            <input autocomplete="off" autocorrect="off"
                                                                placeholder="PASSWORD" spellcheck="false"
                                                                class="mdl-textfield__input error-popover"
                                                                type="password" id="j_password" name="password"
                                                                maxlength="64"> <span id="ShowHidePassword"
                                                                class="glyphicon glyphicon-eye-close pull-right"
                                                                style="margin-top: -6%; cursor: pointer; margin-right: 2%; color: black; font-size: 15px;"
                                                                onclick="ShowHidePassword('j_password', 'ShowHidePassword');"></span>


                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 4%;">
                                                    <!-- <div class="col-xs-2 col-md-2"></div> -->
                                                    <div class="col-xs-12 col-md-12" style="padding-left: 10px;">
                                                        <button id="felix-widget-button-btnSignIn"
                                                            class="pull-right mdl-button mdl-js-button mdl-button--raised mdl-button--accent"
                                                            onclick="loginUserSamVita();" data-style="expand-left"
                                                            title="Sign In" name="btnSignIn" type="button"
                                                            style="color: white; background: rgb(60, 141, 188); width: 100%">
                                                            <!-- background: #24bac1; -->
                                                            <span>SIGN IN</span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <button id="demo-show-snackbar"
                                                    class="mdl-button mdl-js-button mdl-button--raised"
                                                    style="display: none" type="button"></button>
                                                <div id="demo-snackbar-example" class="mdl-js-snackbar mdl-snackbar">
                                                    <div class="mdl-snackbar__text"></div>
                                                    <button class="mdl-snackbar__action" type="button"></button>
                                                </div>

                                                <div class="row" style="margin-top: 8%;">
                                                    <div class="col-xs-6 col-md-6"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>


                        </div>


                    </div>

                </div>
                <div class="col-md-4"
                    style="padding: 0px; color: #fff; border-radius: 20px 20px 20px 20px; margin-bottom: 4%; opacity: 1; filter: alpha(opacity = 100);">
                    <div class="" style="border-top-width: 0px; padding: 0px; margin-top: 30px;">
                        <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                            <div class=""
                                style="height: 100%; border-width: 0px 0px 0px; border-style: solid; border-color: rgb(217, 217, 217); -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; border-image: none;">
                                <div class="form-group col-xs-12"
                                    style="padding-bottom: 15px; padding-top: 15px; margin-bottom: 0px;">
                                    <div class="row">
                                        <!-- <div class="col-xs-2 col-md-2"></div> -->
                                        <div class="col-xs-12 col-md-12"
                                            style="background-color: #e6e9ed;border-radius: 15px;">

                                            <!-- <p style="color: rgb(60, 141, 188); font-size: 18px;margin-left: 97px; margin-top: -5px;">Doctor</p> -->
                                            <h4 style="color: rgb(60, 141, 188);margin-left:45px;">Welcome To Doctor
                                                Portal</h4>
                                            <h6 style="color: rgb(60, 141, 188);margin-left:45px;margin-top: 0px;">As a
                                                doctor you can do these actions</h6>


                                            <ul style="color:#333; list-style: none;">
                                                <li
                                                    style="padding:10px;padding-bottom: 2px;padding-top: 0px;padding-left:15px;">
                                                    <img src="resources/images/ecg-lines.png"
                                                        style="width: 30px;border-radius: 50%;background: white; padding:0px;padding-right:0px;"><span
                                                        style="margin-left:0px;padding-left: 5px;">Review Vital
                                                        Results</span>
                                                </li>
                                                <li style="padding:10px;padding-top:0px;padding-bottom:0px;"><img
                                                        src="resources/images/Dr_worklist.png"
                                                        style="width:40px; padding:5px; border-radius: 100px;">Advice &
                                                    Refer</li>
                                                <li style="padding:10px;padding-top:0px;padding-bottom:0px;"><img
                                                        src="resources/images/video.png"
                                                        style="width:40px; padding:5px;padding-right:10px; -webkit-filter: grayscale(100%);">Video
                                                    Consultation</li>
                                                <li style="padding:10px;padding-top:0px;padding-bottom:0px;"><img
                                                        src="resources/images/management-1.png"
                                                        style="width:40px; padding:5px;;border-radius: 100px;">Expert
                                                    Video Consultation</li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-2">
                </div>

            </div>
        </div>

        <!-- <div id="footer" style="right:5px;  position:fixed;  bottom:0px;" class="customfooter">

        Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>. All rights reserved.
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
</div> -->






        <style>
            .bg-custom {
                color: black;
            }

            .red-tooltip+.tooltip>.tooltip-inner {
                background-color: #f00;
            }

            .realperson-regen {
                text-align: left;
            }
        </style>
        <script type="text/javascript">


            $(document).ready(function () {

            window.addEventListener("offline", (event) => {
            toastr.error("Poor Network");
            });

            window.addEventListener("online", (event) => {
              toastr.success("ONLINE");
            });

                // if (localStorage.getItem('samPT') === 'samvita') {
                //   console.log('Do nothing');
                // } else {
                var a = "${userName}", b = "${password}", c = "${productType}";
                console.log('userName: ', a);
               console.log('password: ', b);
                console.log('productType: ', c);
                $("#j_username").val(a);
                $("#j_password").val(b);
                if (c === 'samvita') {
                    localStorage.setItem('samPT', 'samvita');
                    //loginUser();
                    loginUserSamVita();
                }
                // }

            });

            //var	serverPath ="http://192.168.1.45:9090/PopulationCarev2";
            $("#j_password").keyup(function (event) {
                if (event.keyCode === 13) {
                    $("#felix-widget-button-btnSignIn").click();
                }
            });
            var userGroupCode = '';
            function loginUser() {
                userGroupCode = '';
                var userN = $("#j_username").val();
                var password = $("#j_password").val();
                if (userN != "" && password != "") {
                    $.ajax({
                        type: "POST",
                        url: serverPath + "/agentLogin",
                        data: $("#loginForm").serialize(),
                        headers: {
                            "deviceId": "CUSTOMER"
                        },
                        success: function (data) {
                            if (data.code == "200") {
                                var userId = data.content.id;
                                var productCode = $("#productTypeCode").val();
                                $.ajax({
                                    type: "POST",
                                    url: serverPath + '/doctorProfileDetailsByProductCode?userId=' + userId + '&productCode=' + productCode,
                                    headers: { "deviceId": "CUSTOMER" },
                                    success: function (data) {
                                        if (data.code == "200" || data.code == "202") {

                                            localStorage.setItem("loggedInUserFullName", data.content.firstname + " " + data.content.lastname);
                                            localStorage.setItem("userGroupCode", data.content.userGroupCode);
                                            localStorage.setItem("profileId", data.content.profileId);
                                            localStorage.setItem("loggedInUserId", data.content.id);
                                            localStorage.setItem("productId", data.content.productId);
                                            localStorage.setItem("serverKeyId", data.serverKeyId);
                                            if (data.content.userGroupCode != null) {

                                                userGroupCode = data.content.userGroupCode;
                                            }
                                            if (data.code == "202") {
                                                //toastr.warning(data.message);
                                                //alert(data.message);
                                            }
                                            $.ajax({
                                                type: "POST",
                                                url: 'signIn?userId=' + userId + '&userGroupCode=' + userGroupCode,
                                                success: function (data) {
                                                    if (data.code == "200") {

                                                        toastr.success('SignIn successful');

                                                        if (userGroupCode == 'DOC') {
                                                            if (listDashboard) {
                                                                window.location.href = "list";
                                                            } else {
                                                                <!-- window.location.href = "patientList";-->
                                                                window.location.href = "home2";
                                                            }
                                                        } else if (userGroupCode == 'SADM') {
                                                            window.location.href = "admin";
                                                        } else {
                                                            window.location.href = "patientList";
                                                        }
                                                        //alert('sf'+userGroupCode);
                                                    }
                                                }, error: function (response) {
                                                }
                                            });
                                        } else if (data.code == "201") {
                                            toastr.error(data.message);
                                            console.log(data.message);
                                            //alert(data.message);
                                        }
                                    }, error: function (response) {
                                        toastr.error('Server/network not reachable');
                                    }
                                });
                            } else if (data.code == "102") {
                                toastr.error('Invalid UserName OR Password');
                            } else {
                                toastr.warning('Something Wrong,Please try again');
                            }
                        },
                        error: function (response) {
                            toastr.error(
                                'Server/network not reachable',
                                'Error');
                        }
                    });
                } else {
                    toastr.error("Username/Password are required");
                }

            }
            function loginUserSamVita() {


                userGroupCode = '';
                var userN = $("#j_username").val();
                var password = $("#j_password").val();
                let temp = JSON.stringify({
                    "userName": userN,
                    "password": password
                });
                console.log('loginUserSamVita: ', userN, password, $("#loginForm").serialize(), temp);
                if (userN != "" && password != "") {
                    $.ajax({
                        type: "POST",
                        url: serverPath + "/loginByUserName",
                        contentType: "application/json",
                        data: temp,
                        success: function (data) {
                            console.log("Success: ", data)
                            if (data.code == "200") {
                                console.log("login--->",data.content);
                                localStorage.setItem("loggedInUserFullName", data.content.fullName);//data.content.fullName);
                                localStorage.setItem("loginUsername",data.content.userName);
                                localStorage.setItem("roleName", data.content.roleName); //Doctor Group id
                                //localStorage.setItem("profileId",data.content.profileId);
                                localStorage.setItem("loggedInUserId", data.content.userId);
                                localStorage.setItem("clientId", data.content.clientId);
                                // localStorage.setItem("phoneNo", phoneNo);
                                localStorage.setItem("DW_JWT_TOKEN", data.content.token);
                                localStorage.setItem("RESET_PASSWORD", data.content.passwordChangeRequired);
                                let RESET_PASSWORD = data.content.passwordChangeRequired;
                                let DW_JWT_TOKEN = localStorage.getItem("DW_JWT_TOKEN");
                                console.log("TOKEN LENGTH",DW_JWT_TOKEN.length);
                                var kitId = [], tempKitNum="";
                                kitId = data.content.kitPermission
                                for (var index = 0;  index < kitId.length; index++){
                                    if(kitId.length == 1) tempKitNum += kitId[index].kitSerialNum;
                                    else {
                                        if(!tempKitNum) tempKitNum = tempKitNum  + kitId[index].kitSerialNum;
                                        else tempKitNum += "," + kitId[index].kitSerialNum;
                                    }
                                }
                                console.log(tempKitNum);
                                localStorage.setItem("kitPermission",tempKitNum);

                                // localStorage.setItem("loggedInUserFullName", data.content.firstname + " " + data.content.lastname);
                                // localStorage.setItem("userGroupCode", data.content.userGroupCode);
                                // localStorage.setItem("profileId", data.content.profileId);
                                // localStorage.setItem("loggedInUserId", data.content.id);
                                // localStorage.setItem("productId", data.content.productId);
                                // localStorage.setItem("serverKeyId", data.serverKeyId);
                                console.log('Doc: ', data.content.roleName, (data.content.roleName == 'Doctor'))
                                if ((data.content.roleName == 'Doctor' &&  RESET_PASSWORD == true && DW_JWT_TOKEN.length !== 0) || (data.content.roleName == 'Client Admin' && RESET_PASSWORD == true && DW_JWT_TOKEN.length !== 0) || (data.content.roleName == 'Nurse' && RESET_PASSWORD == true && DW_JWT_TOKEN.length !== 0)) {
                                    console.log("inside condition--->",RESET_PASSWORD)
                                    window.location.href = "resetPassword";
                                    //window.location.href = "home2";
                                    localStorage.setItem("userGroupCode", "DOC"); //Doctor Group id
                                    localStorage.getItem("kitPermission");
                                    console.log('Kit Serial Number: ',localStorage.getItem("kitPermission"))
                                }else if(data.content.roleName == 'Doctor' && RESET_PASSWORD == false && DW_JWT_TOKEN.length !== 0){
                                console.log("inside else condition--->",RESET_PASSWORD)
                                window.location.href = "home2";
                                localStorage.setItem("userGroupCode", "DOC"); //Doctor Group id
                                localStorage.getItem("kitPermission");
                                console.log('Kit Serial Number: ',localStorage.getItem("kitPermission"))
                                }
                                else if (data.content.roleName == 'Client Admin' && RESET_PASSWORD == false && DW_JWT_TOKEN.length !== 0) {
                                console.log("inside else condition--->",RESET_PASSWORD)
                                    window.location.href = "home2";
                                    localStorage.setItem("userGroupCode", "ADMIN"); //Doctor Group id
                                } else if (data.content.roleName == 'Nurse' && RESET_PASSWORD == false && DW_JWT_TOKEN.length !== 0) {
                                console.log("inside else condition--->",RESET_PASSWORD)
                                    window.location.href = "patientList";
                                    localStorage.setItem("userGroupCode", "NUR"); //Doctor Group id
                                }
                                // var userId = data.content.id;
                                // var productCode = $("#productTypeCode").val();
                                // $.ajax({
                                //     type: "POST",
                                //     url: serverPath + '/doctorProfileDetailsByProductCode?userId=' + userId + '&productCode=' + productCode,
                                //     headers: { "deviceId": "CUSTOMER" },
                                //     contentType: "application/json",
                                //     success: function (data) {
                                //         if (data.code == 200 || data.code == 202) {

                                //             localStorage.setItem("loggedInUserFullName", data.content.fullName);//data.content.fullName);
                                //             localStorage.setItem("userGroupCode", "DOC"); //Doctor Group id
                                //             //localStorage.setItem("profileId",data.content.profileId);
                                //             localStorage.setItem("loggedInUserId", data.content.userId);
                                //             localStorage.setItem("clientId", data.content.clientId);
                                //             // localStorage.setItem("phoneNo", phoneNo);
                                //             localStorage.setItem("token", data.content.token);
                                //             localStorage.setItem("kitPermission", data.content.kitPermission);

                                //             // localStorage.setItem("loggedInUserFullName", data.content.firstname + " " + data.content.lastname);
                                //             // localStorage.setItem("userGroupCode", data.content.userGroupCode);
                                //             // localStorage.setItem("profileId", data.content.profileId);
                                //             // localStorage.setItem("loggedInUserId", data.content.id);
                                //             // localStorage.setItem("productId", data.content.productId);
                                //             // localStorage.setItem("serverKeyId", data.serverKeyId);
                                //             if (data.content.role == 'Doctor') {
                                //                 window.location.href = "home2";
                                //             } else if (data.content.role == 'Client Admin') {
                                //                 window.location.href = "home2";
                                //             } else if (data.content.role == 'Nurse') {
                                //                 window.location.href = "patientList";
                                //             }
                                //         } else {
                                //             toastr.error(data.message);
                                //             //alert(data.message);
                                //         }
                                //     }, error: function (response) {
                                //         toastr.error('Server/network not reachable');
                                //     }
                                // });
                            }else if(data.code !== "200"){
                                window.location.href = "login";
                            } else if (data.code == "102") {
                                toastr.error('Invalid UserName OR Password');
                            } else {
                                toastr.warning(data.message);
                            }
                        },
                        error: function (xhr, status, error) {
                            if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                                // if (xhr.responseJSON.code === 401) {
                                //     (async () => {
                                //         toastr.error(xhr.responseJSON.message);
                                //         await sleep();
                                //         window.location.href = './login';
                                //     })()
                                // }
                                toastr.error(xhr.responseJSON.message);
                            } else {
                                toastr.error("Server Error");
                            }
                        }
                    });
                } else {
                    toastr.error("Username/Password are required");
                }
                return false;

            }
            function ShowHidePassword(inputId, iconId) {
                        var inputElement = $('#' + inputId);
                        var iconElement = $('#' + iconId);
                            if ($('#j_password').attr('type') == "password") {
                                $('#j_password').attr('type', 'text');
                                iconElement.removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
                            } else {
                                $('#j_password').attr('type', 'password');
                                iconElement.removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
                            }
                        }


        </script>

        <footer class="main-footer"
            style="display:block;padding-right:15px; margin-left:0% !important;position: fixed;bottom: 0px;margin-right: auto;margin-left: auto;width:100%;">
            <!--  <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>.</strong> All rights reserved. -->
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0.0
            </div>
            Copyright &copy; 2021-2022 <a href="http://www.wrizto.com/" target=_blank
                style="color: rgb(60, 141, 188);">Wrizto Healthcare Pvt. Ltd.</a> All rights reserved.
        </footer>
    </body>

</html>