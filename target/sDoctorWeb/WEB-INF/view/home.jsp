<%@ include file="/WEB-INF/view/includes.jsp" %>
    <!DOCTYPE>
    <html>

    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title>Statistics</title>

        <link rel="stylesheet" href="resources/css/bootstrap.min.css" />
        <link rel="stylesheet" href="resources/css/toastr.min.css" />
        <link href="resources/css/AdminLTE.css" rel="stylesheet" />
        <link href="resources/css/_all-skins.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="resources/css/font-awesome.min.css" />
        <link rel="stylesheet" href="resources/css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="resources/css/material.css">

        <!-- <link rel="stylesheet" href="resources/css/apexcharts.css"> -->
        <!-- <script src="resources/js/apexcharts.min.js"></script> -->



        <script src="resources/js/jQuery-2.1.4.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/fastclick.min.js"></script>
        <!-- <script src=".resources/js/app.min.js"></script> -->
        <script src="resources/js/demo.js"></script>
        <script src="resources/js/customHTML.js"></script>
        <script src="resources/js/toastr.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>

        <script src="resources/customJS/statistics1.js"></script>
        <script src="resources/customJS/videoCall.js"></script>
        <link rel="stylesheet" href="resources/customCSS/statistics.css">

        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>


        <style type="text/css">
            .backImage {
                background-image: url("resources/images/profile.jpg");
            }
        </style>
        <style>
            .toggleSwitch {
                position: relative;
                display: inline-block;
                width: 120px;
                height: 34px;
            }

            .toggleSwitch .toggleInput {
                display: none;
            }

            .dateSlider {
                position: absolute;
                cursor: pointer;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: #2ab934;
                -webkit-transition: .4s;
                transition: .4s;
            }

            .dateSlider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

            .toggleInput:checked+.dateSlider {
                background-color: #ca2222;
            }

            .toggleInput:focus+.slider {
                box-shadow: 0 0 1px #2196F3;
            }

            .toggleInput:checked+.dateSlider:before {
                -webkit-transform: translateX(85px);
                -ms-transform: translateX(85px);
                transform: translateX(85px);
            }

            /*------ ADDED CSS ---------*/
            .on {
                display: none;
            }

            .on,
            .off {
                color: white;
                position: absolute;
                transform: translate(-50%, -50%);
                top: 50%;
                left: 50%;
                font-size: 12px;
                font-family: Verdana, sans-serif;
            }

            .toggleInput:checked+.dateSlider .on {
                display: block;
            }

            .toggleInput:checked+.dateSlider .off {
                display: none;
            }

            /*--------- END --------*/

            /* Rounded sliders */
            .dateSlider.round {
                border-radius: 34px;
            }

            .dateSlider.round:before {
                border-radius: 50%;
            }

            .navbar-t-centered {
                position: absolute;
                left: 34%;
                display: block;
                text-align: center;
                color: #fff;
                top: 0px;
                padding: 15px;
                font-weight: 400;
                font-size: 12px;
            }




            @media screen and (min-width:768px) {
                .navbar-t-centered {
                    position: absolute;
                    left: 38%;
                    display: block;
                    /*  width: 160px; */
                    text-align: center;
                    color: #fff;
                    top: 0px;
                    padding: 15px;
                    font-weight: 400;
                    font-size: 22px;
                }
            }
        </style>
        <script type="text/javacript">



</script>
        <script type="text/javascript">
            //  var	serverPath ="http://192.168.1.225:9090/PopulationCarev2";
            var productId = localStorage.getItem("productId");
            var loggedInUserId = '';
            // const DEF_DELAY = 1000;
            // function sleep(ms) {
            //     return new Promise(resolve => setTimeout(resolve, ms || DEF_DELAY));
            // }


            $(document).ready(function () {

                //$(window).off('beforeunload');

                $("#patReportBtn").hide();
                if (localStorage.getItem("userGroupCode") === 'NUR') {
                    window.location.href = 'patientList';
                }
                if (localStorage.getItem("userGroupCode") === 'DOC') {
                    pingCallForVC();
                }
                if (localStorage.getItem("roleName") === 'Client Admin') {
                    $("#patReportBtn").show();
                    $("#dwnldPdfPulse").show();
                    $("#dwnldCsvPulse").show();
                    $("#dwnldPdfTemp").show();
                    $("#dwnldCsvTemp").show();
                    $("#dwnldPdfBp").show();
                    $("#dwnldCsvBp").show();
                    $("#dwnldPdfSpo2").show();
                    $("#dwnldCsvSpo2").show();
                    $("#dwnldPdfBmi").show();
                    $("#dwnldCsvBmi").show();
                    $("#dwnldPdfBg").show();
                    $("#dwnldCsvBg").show();

                    $("#dwnldPdfPulseGender").show();
                    $("#dwnldCsvPulseGender").show();
                    $("#dwnldPdfTempGender").show();
                    $("#dwnldCsvTempGender").show();
                    $("#dwnldPdfBpGender").show();
                    $("#dwnldCsvBpGender").show();
                    $("#dwnldPdfSpo2Gender").show();
                    $("#dwnldCsvSpo2Gender").show();
                    $("#dwnldPdfBmiGender").show();
                    $("#dwnldCsvBmiGender").show();
                    $("#dwnldPdfBgGender").show();
                    $("#dwnldCsvBgGender").show();
                }

                //var hash = window.location.hash;
                //console.log("On Home load:", hash)
                //$('#homeTab a[href="' + hash + '"]').tab('show');

                var loggedInUserFullName = localStorage
                    .getItem("loggedInUserFullName");
                $("#loggedInUserFullName").text(loggedInUserFullName);
                $("#loggedInUserFullNameSpan").text(
                    loggedInUserFullName);



                $.ajax({
                    type: "POST",
                    // url : serverPath+ "/getPatientList?productId="+ productId,
                    url: serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId") + '&size=10000',
                    headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
                    success: function (data) {
                        console.log('---getPatientListByClient?SUCCS --', data.content)

                        if (data.code == '200') {
                            var listOfCards = "";
                            $("#searchedInput").val("");
                            let { patientList } = data.content;
                            console.log('---getPatientListByClient?patientList --', patientList)
                            if (patientList) {
                                let { contentArray } = patientList;
                                //console.log('---getPatientListByClient?contentArray --', contentArray, ', leng: ', contentArray.length)
                                if (contentArray && contentArray.length > 0) {
                                    $.each(contentArray, function (index, option) {
                                        var lastName = "";
                                        var uhid = option.uhid;
                                        if (option.uhid == null
                                            || option.uhid == ""
                                            || option.uhid == "undefined"
                                            || option.uhid == "null") {
                                            uhid = " ";
                                        }
                                        if (option.bloodGroup == null) {
                                            option.bloodGroup = "";
                                        }
                                        if (option.maritalStatus == null) {
                                            option.maritalStatus = "";
                                        }
                                        if (option.patientLastName != null) {
                                            lastName = option.patientLastName;
                                        }
                                        listOfCards += '<div class="col-md-3 col-sm-6 col-xs-12" style="margin-top:10px;">'
                                            + '<div class="box box-primary">'
                                            + '<div class="box-body box-profile" style="height:304px;">'
                                            + '<img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">'
                                            + '<h5 style="font-size: 16px;" class="text-center">'
                                            + option.patientFirstName
                                            + ' '
                                            + lastName
                                            + '</h5>'
                                            + '<p class="text-muted text-center" style="margin: 0 0 10px;">'
                                            + uhid
                                            + '</p>'

                                            + '<ul class="list-group list-group-unbordered" style="font-size: 12px;">'

                                            + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                            + '<b>'
                                            + option.mobileNo
                                            + '</b>'
                                            + '</li>'
                                            + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                            + '<b>'
                                            + option.bloodGroup
                                            + ' / '
                                            + option.maritalStatus
                                            + ' '
                                            + '</b>'
                                            + '</li>'

                                            + '</ul>'
                                            + '<a href="#" class="btn btn-primary btn-block" onclick="openDashboard('
                                            + option.id + ',' + option.userId
                                            + ');"><b>More</b></a>'
                                            + '</div>'
                                            + '</div>'
                                            + '</div>'
                                    });
                                    $("#patientListCard").html(listOfCards);
                                } else {
                                    $("#patientListCard").html("<h5  class='text-center'>Patient table empty!</h5>");
                                }
                            } else {
                                $("#patientListCard").html("<h5  class='text-center'>No Patients found</h5>");
                            }
                        } else {
                            $("#patientListCard").html("<h5  class='text-center'>No Patient Available for today !</h5>");
                        }

                    },
                    error: function (xhr, status, error) {
                        console.log('xhr1: ', xhr, ' -->Status: ', status, ' -->Error: ', error);
                        if (xhr.status == 0) window.location.href = './login';
                        if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                            if (xhr.responseJSON.code === 401) {
                                (async () => {
                                    toastr.error(xhr.responseJSON.message);
                                    await sleep();
                                    window.location.href = './login';
                                })()
                            }
                            toastr.error(xhr.responseJSON.message);
                        } else {
                            toastr.error("No Data Found!");
                        }
                    }
                });
            })

            function openDashboard(patientId, patUId) {
                localStorage.setItem("patientId", patientId);
                localStorage.setItem("patientUserId", patUId);
                window.location.href = "dashBoard";
                return;
            }
            function openRegistration() {
                window.location.href = "doctorRegistration";
            }

            function logout() {
                localStorage.removeItem("DW_JWT_TOKEN");
                localStorage.removeItem("roleName");
                localStorage.removeItem("clientId");
                localStorage.removeItem("userGroupCode");
                localStorage.removeItem("loggedInUserFullName");
                localStorage.removeItem("kitPermission");
                localStorage.removeItem("patientId");
                localStorage.removeItem("patientUserId");
                window.location.href = "logout";
            }


            $(document).ready(function () {

                $('#birthDat').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,//.date
                    endDate: '1d'
                }).on('changeDate', function (e) {
                    $("#birthDat").attr("readonly", true);
                    $('#birthDat').parent().addClass("is-dirty");
                    $(this).datepicker('hide');
                });
                // clearReg();
            });
            let DOWNLOAD_CSV_TOGGLE_STATE = 'TODAY';
            $(document).ready(function () {
                $(".toggleSwitch .toggleInput").on("change", function (e) {
                    const isOn = e.currentTarget.checked;
                    //console.log('Toggle btn change: ',isOn, DOWNLOAD_CSV_TOGGLE_STATE)

                    if (isOn) {
                        $(".hideme").show();
                        DOWNLOAD_CSV_TOGGLE_STATE = 'CUSTOM';
                    } else {
                        $(".hideme").hide();
                        DOWNLOAD_CSV_TOGGLE_STATE = 'TODAY';
                    }
                });
            });

            function searchForPatient() {

                /* var servicePath = "http://localhost:9090/Doctor"; */
                var searchedInput = $("#searchedInput").val();
                //alert(searchedInput);
                $
                    .ajax({
                        type: "POST",
                        // url : serverPath + "/getPatientList?searchParameter=" + searchedInput + "&productId=" + productId,
                        url: serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId") + '&size=10000' + "&searchParameter=" + searchedInput,
                        headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
                        success: function (data) {
                            console.log('---getPatientListByClient?SUCCS --', data.content)

                            if (data.code == '200') {
                                var listOfCards = "";
                                $("#searchedInput").val("");
                                let { patientList } = data.content;
                                console.log('---getPatientListByClient?patientList --', patientList)
                                if (patientList) {
                                    let { contentArray } = patientList;
                                    console.log('---getPatientListByClient?contentArray --', contentArray, ', leng: ', contentArray.length)
                                    if (contentArray && contentArray.length > 0) {
                                        $.each(contentArray, function (index, option) {
                                            var lastName = "";
                                            var uhid = option.uhid;
                                            if (option.uhid == null
                                                || option.uhid == ""
                                                || option.uhid == "undefined"
                                                || option.uhid == "null") {
                                                uhid = " ";
                                            }
                                            if (option.bloodGroup == null) {
                                                option.bloodGroup = "";
                                            }
                                            if (option.maritalStatus == null) {
                                                option.maritalStatus = "";
                                            }
                                            if (option.patientLastName != null) {
                                                lastName = option.patientLastName;
                                            }
                                            listOfCards += '<div class="col-md-3 col-sm-6 col-xs-12" style="margin-top:10px;">'
                                                + '<div class="box box-primary">'
                                                + '<div class="box-body box-profile" style="height:304px;">'
                                                + '<img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">'
                                                + '<h5 style="font-size: 16px;" class="text-center">'
                                                + option.patientFirstName
                                                + ' '
                                                + lastName
                                                + '</h5>'
                                                + '<p class="text-muted text-center" style="margin: 0 0 10px;">'
                                                + uhid
                                                + '</p>'

                                                + '<ul class="list-group list-group-unbordered" style="font-size: 12px;">'

                                                + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                + '<b>'
                                                + option.mobileNo
                                                + '</b>'
                                                + '</li>'
                                                + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                + '<b>'
                                                + option.bloodGroup
                                                + ' / '
                                                + option.maritalStatus
                                                + ' '
                                                + '</b>'
                                                + '</li>'

                                                + '</ul>'
                                                + '<a href="#" class="btn btn-primary btn-block" onclick="openDashboard('
                                                + option.id + ',' + option.userId
                                                + ');"><b>More</b></a>'
                                                + '</div>'
                                                + '</div>'
                                                + '</div>'
                                        });
                                        $("#patientListCard").html(listOfCards);
                                    } else {
                                        $("#patientListCard").html("<h5  class='text-center'>Patient table empty!</h5>");
                                    }
                                } else {
                                    $("#patientListCard").html("<h5  class='text-center'>No Patients found</h5>");
                                }
                            } else {
                                $("#patientListCard").html("<h5  class='text-center'>No Patient Available for today !</h5>");
                            }

                        },
                        error: function (xhr, status, error) {
                            console.log('xhr: ', xhr, ' -->Status: ', status, ' -->Error: ', error)
                            if (xhr.status == 0) window.location.href = './login';
                            if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                                if (xhr.responseJSON.code === 401) {
                                    (async () => {
                                        toastr.error(xhr.responseJSON.message);
                                        await sleep();
                                        window.location.href = './login';
                                    })()
                                }
                                toastr.error(xhr.responseJSON.message);
                            } else {
                                toastr.error("No Data Found!");
                            }
                        }
                    });

            }


            function searchForDate() {

                var searchInput = $("#birthDat").val();
                if(!searchInput){
                   toastr.error('Selected date is null');
                   return;
                }

                var values = searchInput.split("/");
                var fromDate = new Date(values[2], (values[1] - 1), values[0]).setHours(0, 0, 0, 0, 0);
                var toDate = new Date(values[2], (values[1] - 1), values[0]).setHours("23", "59", 59, 0);

                if (searchInput != '') {
                    $
                        .ajax({
                            type: "POST",
                            // url: serverPath + "/getByDatePatientList?date="+ searchInput + "&productId=" + productId,
                            url: serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId") + '&size=10000' + "&startDate=" + fromDate+ "&endDate=" + toDate,
                            headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
                            success: function (data) {
                                console.log('---getPatientListByClient?SUCCS --', data.content)

                                if (data.code == '200') {
                                    var listOfCards = "";
                                    $("#searchedInput").val("");
                                    let { patientList } = data.content;
                                    console.log('---getPatientListByClient?patientList --', patientList)
                                    if (patientList) {
                                        let { contentArray } = patientList;
                                        console.log('---getPatientListByClient?contentArray --', contentArray, ', leng: ', contentArray.length)
                                        if (contentArray && contentArray.length > 0) {
                                            $.each(contentArray, function (index, option) {
                                                var lastName = "";
                                                var uhid = option.uhid;
                                                if (option.uhid == null
                                                    || option.uhid == ""
                                                    || option.uhid == "undefined"
                                                    || option.uhid == "null") {
                                                    uhid = " ";
                                                }
                                                if (option.bloodGroup == null) {
                                                    option.bloodGroup = "";
                                                }
                                                if (option.maritalStatus == null) {
                                                    option.maritalStatus = "";
                                                }
                                                if (option.patientLastName != null) {
                                                    lastName = option.patientLastName;
                                                }
                                                listOfCards += '<div class="col-md-3 col-sm-6 col-xs-12" style="margin-top:10px;">'
                                                    + '<div class="box box-primary">'
                                                    + '<div class="box-body box-profile" style="height:304px;">'
                                                    + '<img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">'
                                                    + '<h5 style="font-size: 16px;" class="text-center">'
                                                    + option.patientFirstName
                                                    + ' '
                                                    + lastName
                                                    + '</h5>'
                                                    + '<p class="text-muted text-center" style="margin: 0 0 10px;">'
                                                    + uhid
                                                    + '</p>'

                                                    + '<ul class="list-group list-group-unbordered" style="font-size: 12px;">'

                                                    + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                    + '<b>'
                                                    + option.mobileNo
                                                    + '</b>'
                                                    + '</li>'
                                                    + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                    + '<b>'
                                                    + option.bloodGroup
                                                    + ' / '
                                                    + option.maritalStatus
                                                    + ' '
                                                    + '</b>'
                                                    + '</li>'

                                                    + '</ul>'
                                                    + '<a href="#" class="btn btn-primary btn-block" onclick="openDashboard('
                                                    + option.id + ',' + option.userId
                                                    + ');"><b>More</b></a>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>'
                                            });
                                            $("#patientListCard").html(listOfCards);
                                        } else {
                                            $("#patientListCard").html("<h5  class='text-center'>Patient table empty!</h5>");
                                        }
                                    } else {
                                        $("#patientListCard").html("<h5  class='text-center'>No Patients found</h5>");
                                    }
                                } else {
                                    $("#patientListCard").html("<h5  class='text-center'>No Patient Available for today !</h5>");
                                }

                            },
                            error: function (xhr, status, error) {
                                if (xhr.status == 0) window.location.href = './login';
                                if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                                    if (xhr.responseJSON.code === 401) {
                                        (async () => {
                                            toastr.error(xhr.responseJSON.message);
                                            await sleep();
                                            window.location.href = './login';
                                        })()
                                    }
                                    toastr.error(xhr.responseJSON.message);
                                } else {
                                    toastr.error("No Data Found!");
                                }
                            }
                        });
                } else {
                    toastr.warning('Please enter date');
                }

            }

            function confPopUp(callerName, roomName, roomId, weconUrl) {
                var room = encodeURIComponent(roomName);
                videocalllink = weconUrl + roomId;
                var win;
                var r = confirm(callerName + " is calling you. Do you accept the call?");
                //	alert(r);
                if (r == true) {
                    win = window.open(videocalllink, 'result', 'width=300,height=300');

                    /* win.addEventListener("resize", function(){
                        win.resizeTo(1024, 768);
                    }); */
                }
                $.ajax({
                    type: "POST",
                    url: serverPath + "/intiateAndCloseCallv4?initiate=false&roomId="
                        + roomId,
                    success: function (data) {
                        //win.close();
                    }
                });
            }

            function initiateCall(calleProfileId) {
                var proId = localStorage.getItem("productId");
                if (proId == 1310) {
                    alert("This Service is not enabled, Please contact Wenzins");
                } else {
                    /* var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
                    win.addEventListener("resize", function(){
                            win.resizeTo(1024, 768);
                    }); */
                    $.ajax({
                        type: "POST",
                        url: serverPath + "/intiateAndCloseCallv4?profileId="
                            + localStorage.getItem("profileId")
                            + "&calleProfileId=" + calleProfileId + "&serverKeyId="
                            + localStorage.getItem("serverKeyId") + "&initiate="
                            + true,
                        success: function (data) {
                            //	console.log(data);
                            roomid = data.roomid;
                            //	alert(data);
                            //	console.log("new  "+data.content.weconurl);

                            videocalllink = data.content.weconurl + data.roomid;

                            /* 	 $.each(data.content,function(index, option) {
                                     console.log(option);
                                     var url = option.weconurl;
                                //	 alert(url);
                                     console.log("link"+url);
                                     var win = window.open(url);
                                      videocalllink =  url + roomid ;
                            //	var win = window.open(videocalllink , 'result', 'width=1024,height=768');
                            }); */
                            //	 window.open(a);
                            var win = window.open(videocalllink, 'result',
                                'width=300,height=300');

                            win.addEventListener("resize", function () {
                                win.resizeTo(1024, 768);
                            });
                        }
                    });
                }
            }


            $(function () {
                $("#datepicker3Val").datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    endDate: "today"
                }).datepicker('update', new Date());
            });
            $(function () {
                $("#datepicker4Val").datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    endDate: "today"
                }).datepicker('update', new Date());
            });

            $(function () {
                $("#datepicker1").datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    endDate: "today"
                }).datepicker('update', new Date());
            });

            $(function () {
                $("#datepicker2").datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    endDate: "today"
                }).datepicker('update', new Date());
            });

function downloadPatientCsv() {

    console.log('downloadPatientCsv->', $('#datepicker1Val').val(), ', ', $('#datepicker2Val').val(), DOWNLOAD_CSV_TOGGLE_STATE)

    var profileID = localStorage.getItem("profileId");

    var serverKeyId2 = localStorage.getItem("serverKeyId");



    if (DOWNLOAD_CSV_TOGGLE_STATE === 'TODAY') {

        fromDate = new Date();

        toDate = new Date();

        fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(), 0, 0, 0, fromDate.getMilliseconds());

        toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(), 23, 59, 59, toDate.getMilliseconds());

        // console.log('---TODAY: ', fromDate, ', ', toDate);

    } else {



        var values = $('#datepicker1Val').val().split("/");

        fromDate = new Date(values[2], (values[1] - 1), values[0]);

        var values2 = $('#datepicker2Val').val().split("/");

        toDate = new Date(values2[2], (values2[1] - 1), values2[0]);



        // console.log('---CUSTOM: ', fromDate, ', ', toDate);



        var today = new Date().setHours("23", "59", 59, 0);

        if (toDate == null || toDate == 'Invalid Date' || fromDate == null || fromDate == 'Invalid Date') {

            toastr.error('Selected date is null');

            return;

        }



        if (toDate.getTime() > today || fromDate.getTime() > today) {

            toastr.error('Date Cannot Be Greater Than Todays Date');

            return;

        }

        if (toDate.getTime() < fromDate.getTime()) {

            toastr.error('To Date cannot be Lesser Than From Date');

            return;

        }

        fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),

            0, 0, 0, fromDate.getMilliseconds());

        toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),

            23, 59, 59, toDate.getMilliseconds());

        //fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),0, 0, 0, fromDate.getMilliseconds());

        //toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),23, 59, 59, toDate.getMilliseconds());

    }



    // console.log("----DATE-----", fromDate, toDate)

    //var fromDate, toDate, today = new Date().setHours("23", "59", 59, 0);

    fromDate = new Date(fromDate).getTime();

    toDate = new Date(toDate).getTime();



    console.log("downloadPatientCsv-> ", "From: ", fromDate, "To: ", toDate);

    //return;

    var newURL = serverPath + "/getPatientReportByClientId?clientId=" + localStorage.getItem("clientId") + "&startEpoch=" + fromDate + "&endEpoch=" + toDate;

    //console.log(newURL);

    // return newURL;

    window.location.href = newURL;



};

//LOGOUT WHEN BACK BUTTON CLICKED

// if (performance.navigation.type == 2) {

// let text = " Do you want to Logout ?...Your Session will be closed";

// if (confirm(text) == true){

//            localStorage.removeItem("DW_JWT_TOKEN");
//            localStorage.removeItem("roleName");
//            localStorage.removeItem("clientId");
//            localStorage.removeItem("userGroupCode");
//            localStorage.removeItem("loggedInUserFullName");
//            localStorage.removeItem("kitPermission");
//            localStorage.removeItem("patientId");
//            localStorage.removeItem("patientUserId");
//            localStorage.removeItem("productId");
//            window.location.href = "logout";
//     }else {

//             window.location.href = "home";
//           }

//  }



        </script>


    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div>

            <header class="main-header" style="padding-bottom:10px;background:#ecf0f5">
                <nav class="navbar navbar-static-top" role="navigation"
                    style="margin-left: 0% !important; max-height: 50px; background-color: #3c8dbc">
                    <!-- Sidebar toggle button-->
                    <img src="resources/images/WriztoLogoBox.jpg" class="user-image" alt="User Image"
                        style="height: 50px; padding-left: 5px; margin-left: 5px;"> <span
                        class="navbar-t-centered">Wrizto Healthcare System </span>


                    <!-- <button type="button" class="btn btn-default.button4 {padding: 32px 16px;} px-3"><i class="fas fa-balance-scale" aria-hidden="true"></i></button>
	 -->


                    <div class="navbar-custom-menu">

                        <ul class="nav navbar-nav">
                            <!--  <li style="top: 8px;"><label class="switch"><input id="someSwitchOptionInfo" type="checkbox" checked> <span class="slider round"></span></label></li>
             -->
                            <!-- Messages: style can be found in dropdown.less-->

                            <!-- Patient Report csv download -->
                            <li>
                                <!-- Trigger the modal with a button -->

                                <button type="button" class="btn btn-lg" style="background-color: transparent;"
                                    id="patReportBtn" data-toggle="modal" data-target="#myModal">
                                    <img src="resources/images/file2.png"
                                        style="width: 25px; height: 25px; margin-top: 3px;" alt="FIle Icon" />
                                </button> <!-- Modal -->
                                <div id="myModal" class="modal fade" role="dialog" data-backdrop="false">
                                    <div class="modal-dialog modal-sm">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header" style="border-bottom: 3px solid #3c8dbc;">
                                                <button type="button" class="close" onClick="resetReportDatePicker()"
                                                    data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Report</h4>
                                            </div>
                                            <div class="modal-body">


                                                <label class="toggleSwitch"> <input type="checkbox" id="togBtn"
                                                        class="toggleInput">
                                                    <div class="dateSlider round">
                                                        <!--ADDED HTML -->
                                                        <span class="on">Custom</span> <span class="off">Today</span>
                                                        <!--END-->
                                                    </div>
                                                </label>

                                                <div class="hideme" style="display:none;">
                                                    <label>Start : </label>
                                                    <div id="datepicker1" class="input-group date"
                                                        data-date-format="dd/mm/yyyy">
                                                        <input id="datepicker1Val" class="form-control" type="text"
                                                            readonly /> <span class="input-group-addon"><i
                                                                class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>

                                                    <label>End : </label>
                                                    <div id="datepicker2" class="input-group date"
                                                        data-date-format="dd/mm/yyyy">
                                                        <input id="datepicker2Val" class="form-control" type="text"
                                                            readonly /> <span class="input-group-addon"><i
                                                                class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" style="background-color:#3c8dbc; color:white"
                                                    class="btn btn-default"
                                                    onClick="downloadPatientCsv();">Download</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </li>



                            <!-- User menu dropdown -->
                            <li class="dropdown user user-menu"><a href="#" class="dropdown-toggle"
                                    data-toggle="dropdown"> <img src="resources/images/avatar.png" class="user-image"
                                        alt="User Image"> <span class="hidden-xs" id="loggedInUserFullName"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->

                                    <li class="user-header"><img src="resources/images/avatar.png" class="img-circle"
                                            alt="User Image">
                                        <p>
                                            <span id="loggedInUserFullNameSpan"></span>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body"
                                        style="padding-top: 0px; padding-right: 0px; padding-left: 0px;">
                                        <!-- <div class="col-xs-7 text-center"><span style="font-size: 12px; margin-left: -33px;" id="nurseNameForActiveId"></span>
                                  <a id= "videoIconId" style="border-radius: 50%; ;margin-left: 5px;background-color: green" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                  				   <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a>
                                  </div>
                                  <div class="col-xs-5 text-center">
                                    <a href="patientList">Patient List</a>
                                  </div> activeUsersId  activeUsersId-->

                                        <ul class="col-xs-12 text-center" id="activeUsersId">
                                            <!-- <li class="form-control" style="border-radius:6px;" ><span style="font-size: 12px;text-transform: capitalize;margin-left: -33px;" id="nurseNameForActiveId">Wrizto Nurse</span>
                 			 <a  id= "videoIconId" style="border-radius: 50%;margin-left: 5px;float:right;background-color: green" class="btn btn-primary" title="videoCall" onclick="initiateCall(1358)"><b>
                 			 	<i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a></li>
                 				 -->

                                        </ul> <br>
                                        <ul>
                                            <br>
                                            <!--  <span style="top: 4px;"> <button style="padding: 10px 10px; type="button" class="btn btn-success"><i class="fa fa-video-camera fa-lg" aria-hidden="true"></i>&nbsp;<span><b>Wecon </b></span></button>
                 				  <button style="padding: 10px 10px; type="button" class="btn btn-success "><i class="fa fa-video-camera fa-lg" aria-hidden="true"></i>&nbsp;<span><b>Mscp</b></span></button>
                 				 </span> -->

                                        </ul>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left"></div>
                                        <div class="pull-right">
                                            <a onclick="logout();" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->

                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->


            <!--
		<div class="col-md-12" id="patientListCard"
			style="padding-bottom: 60px;"></div>
		<!-- Content Wrapper. Contains page content
		<div class="content-wrapper"
			style="margin-left: 0% !important; margin-bottom: 10px;"></div>
		<input type="hidden" id="abcdefg" value="">
		-->

            <div>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" style="border-bottom:1px solid lightblue" id="homeTab">
                        <li onclick="openCity(event, 'London','activity')" id="statsTab" class="tablinks active"> <a
                                href="#activity" data-toggle="tab">Statistics</a> </li>
                        <li onclick="openCity(event, 'Paris', 'timeline')" id="patientTab" class="tablinks"> <a
                                href="#timeline" data-toggle="tab">Patients</a> </li>
                    </ul>
                    <div id="London" class="tabcontent">
                        <div class="active tab-pane" id="activity">

                            <!--------------------------------------------------- Statistics Page content --------------------------------------------->
                            <div class="stats-conatiner container-fluid">

                                <div class="row">
                             <div class="col-sm-12" style="display:flex;justify-content:end; margin:10px 0px">
                              <div class="col-md-4" style=" width: 100%; display: inline-flex;">
                              <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                              <label class="custom-control-label" for="customRadioInline1">Vilage</label>
                                              <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                                              <label class="custom-control-label" for="customRadioInline2">Pincode</label>
                                            </div>
                              <div class="float-right" style="display:block;">
                                              <div class="input-group " style="margin-left:10px;justify-content:start;padding-right: 30px;" >
                                                  <input type="text" id="searchedRegion" class="form-control" placeholder="Search">
                                                  <span class="input-group-btn">
                                                      <button class="btn btn-search" type="button"
                                                          style="background-color: #3c8dbc; color: white;"
                                                          onclick="searchForPatient();">
                                                          <i class="fa fa-search fa-fw"></i> Search
                                                      </button>
                                                  </span>
                                              </div>
                                              </div>
                                          </div>

                                        <button type="button" id="d1" style="margin-left:10px;background:#B8B8B8;"
                                            class="btn btn-primary normalCol"
                                            onclick="getPatientStats('d1');">Today</button>
                                        <button type="button" id="d2" style="margin-left:10px;background:#367fa9;"
                                            class="btn btn-primary normalCol"
                                            onclick="getPatientStats('d2');">Weekly</button>
                                        <button type="button" id="d3" data-toggle="modal" data-target="#statsModal"
                                            style="margin-left:10px;background:#B8B8B8;"
                                            class="btn btn-primary normalCol"
                                            onclick="buttonTest('d3');">Custom</button>



                                        </div>
                                </div>

                                <!--Patient Statistics Table Modal -->
                                <div class="modal fade" id="tableModalCenter" tabindex="-1" role="dialog"
                                    aria-labelledby="tableModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="tableModalLongTitle"></h5>
                                                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button> -->
                                            </div>
                                            <div class="modal-body tableFixHead" style="padding:0;" id="statsTableId">
                                                <table id="patientStatsTable1" class="table table-striped">
                                                    <thead id="patientStatsHeader">
                                                    </thead>
                                                    <tbody id="patientStatsBody">
                                                    </tbody>
                                                </table>
                                                <div id="statsTableEmpty"
                                                    style="display:flex;justify-content:center;align-items:center;height:100%;">
                                                    No data!</div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Pulse Rate Statistics</div>
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip1"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (72 ~ 100 bpm)</p><p>- Abnormal (<72 / >100 bpm)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn1"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn1"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('Pulse');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfPulse"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('Pulse','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvPulse"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('Pulse','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart1" class="chartContainer"></div>
                                            <div id="donutChart1-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Temperature Statistics</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip2"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (36 ~ 38 <span>&#8451;</span>)</p><p>- Abnormal (<36 / >38 <span>&#8451;</span>)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn2"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn2"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('TEMPERATURE');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfTemp"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('TEMPERATURE','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvTemp"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('TEMPERATURE','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart2" class="chartContainer"></div>
                                            <div id="donutChart2-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Blood Pressure Statistics</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip3"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>-Optimal (SYS <120, DIA <80 mm Hg)</p><p>- Normal (SYS <130, DIA <85 mm Hg)</p><p>High-Normal (SYS 130~139, DIA 85~89 mm Hg)</p>
										<p>-G1-Hypertension (SYS 140~159, DIA 90~99 mm Hg)</p><p>-G2-Hypertension (SYS 160~179, DIA 100~109 mm Hg)</p><p>-G3-Hypertension (SYS >=180, DIA >=110 mm Hg)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn3"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu list-right"
                                                            aria-labelledby="dropdownMenuBtn3"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BP');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBp"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BP','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBp"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BP','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart3" class="chartContainer"></div>
                                            <div id="donutChart3-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Pulse Rate Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip7"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (72 ~ 100 bpm)</p><p>- Abnormal (<72 / >100 bpm)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn7"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn7"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('Pulse');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfPulseGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('Pulse','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvPulseGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('Pulse','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart1" class="chartContainer"></div>
                                            <div id="barChart1-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Temperature Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip8"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (36 ~ 38 <span>&#8451;</span>)</p><p>- Abnormal (<36 / >38 <span>&#8451;</span>)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn8"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn8"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('TEMPERATURE');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfTempGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('TEMPERATURE','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvTempGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('TEMPERATURE','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart2" class="chartContainer"></div>
                                            <div id="barChart2-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Blood Pressure Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip9"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>-Optimal (SYS <120, DIA <80 mm Hg)</p><p>- Normal (SYS <130, DIA <85 mm Hg)</p><p>High-Normal (SYS 130~139, DIA 85~89 mm Hg)</p>
										<p>-G1-Hypertension (SYS 140~159, DIA 90~99 mm Hg)</p><p>-G2-Hypertension (SYS 160~179, DIA 100~109 mm Hg)</p><p>-G3-Hypertension (SYS >=180, DIA >=110 mm Hg)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn9"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu list-right"
                                                            aria-labelledby="dropdownMenuBtn9"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BP');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBpGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BP','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBpGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BP','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart3" class="chartContainer"></div>
                                            <div id="barChart3-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">SPO2 Statistics</div>
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip4"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (95 ~ 100 %)</p><p>- Abnormal (<95 / >100 %)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn4"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn4"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('SPO2');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfSp02"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('SPO2','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvSp02"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('SPO2','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart4" class="chartContainer"></div>
                                            <div id="donutChart4-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">BMI Statistics</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip5"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Underweight(<18.5 kg/m<sup>2</sup>)</p><p>- Normal(18.5~24 kg/m<sup>2</sup>)</p><p>- Overweight(25~29 kg/m<sup>2</sup>)</p>
										<p>- Obese(>=30 kg/m<sup>2</sup>)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn5"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn5"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BMI');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBmi"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BMI','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBmi"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BMI','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart5" class="chartContainer"></div>
                                            <div id="donutChart5-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Blood Glucose Statistics</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip6"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>-Lowsugar (51~70 mg/dL)</p><p>-Normal (71~100 mg/dL)</p><p>- Prediabetes (101-125 mg/dL)</p><p>- Diabetes (>126 mg/dL)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn6"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu list-right"
                                                            aria-labelledby="dropdownMenuBtn6"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BG');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBg"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BG','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBg"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BG','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart6" class="chartContainer"></div>
                                            <div id="donutChart6-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">SPO2 Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip10"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (95 ~ 100 %)</p><p>- Abnormal (<95 / >100 %)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn10"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn10"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('SPO2');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfSp02Gender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('SPO2','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvSp02Gender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('SPO2','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart4" class="chartContainer"></div>
                                            <div id="barChart4-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">BMI Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip11"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Underweight(<18.5 kg/m<sup>2</sup>)</p><p>- Normal(18.5~24 kg/m<sup>2</sup>)</p><p>- Overweight(25~29 kg/m<sup>2</sup>)</p>
										<p>- Obese(>=30 kg/m<sup>2</sup>)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn11"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn11"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BMI');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBmiGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BMI','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBmiGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BMI','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart5" class="chartContainer"></div>
                                            <div id="barChart5-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Blood Glucose Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip12"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>-Lowsugar (51~70 mg/dL)</p><p>-Normal (71~100 mg/dL)</p><p>- Prediabetes (101-125 mg/dL)</p><p>- Diabetes (>126 mg/dL)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn12"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu list-right"
                                                            aria-labelledby="dropdownMenuBtn12"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BG');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBgGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BG','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBgGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BG','csv');"
                                                                href="#">Download CSV</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart6" class="chartContainer"></div>
                                            <div id="barChart6-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="row">

                                    <!--
		            <div class="col-sm-6" >
		            <div class="box-shadow-card">
		            	<div class="stats-chart-box">
		                    <div class="stats-header-box">
		                        <div class="stats-header">ECG Statistics</div>
		                        <div class="stats-header-icon">
		                            <span class="glyphicon glyphicon-info-sign" id="toolTip13" data-placement="left" data-toggle="tooltip" data-html="true"
										title="<div class='tooltipTitle'><p>NIRF - No irregular rhythm found</p><p>SLFB - Suspected a little fast beat</p><p>SFB - Suspected fast beat</p><p>SSRFB - Suspected short run of fast beat</p><p>SLSB - Suspected a little slow beat</p><p>SSB - Suspected slow beat</p><p>SIBI - Suspected irregular beat interval</p><p>SFBWSBI - Suspected fast beat with short beat intervel</p><p>SSBWSB - Suspected slow beat with short beat</p><p>SSBWIBI - Suspected slow beat with ireegular beat interval</p><p>SSBI - Suspected short beat interval</p><p>SFBWBW - Suspected fast beat with baseline wander</p><p>SSBWBW - Suspected slow beat with baseline wander</p><p>SSBIWBW - Suspected short beat interval with baseline wander</p><p>SIBIWBW - Suspected irregular beat interval with baseline wander</p></div>"
										>
									</span>
		                        </div>
		                    </div>
		                </div>
		                <div id="barChart8" class="chartContainer"></div>
		                <div id="barChart8-ND" class="emptyChart" style="display:none"> No Data!</div>
		            </div>
		            </div>
		             -->

                                    <div class="col-sm-6">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">ECG Statistics</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip13"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>NIRF - No irregular rhythm found</p><p>SLFB - Suspected a little fast beat</p><p>SFB - Suspected fast beat</p><p>SSRFB - Suspected short run of fast beat</p><p>SLSB - Suspected a little slow beat</p><p>SSB - Suspected slow beat</p><p>SIBI - Suspected irregular beat interval</p><p>SFBWSBI - Suspected fast beat with short beat intervel</p><p>SSBWSB - Suspected slow beat with short beat</p><p>SSBWIBI - Suspected slow beat with ireegular beat interval</p><p>SSBI - Suspected short beat interval</p><p>SFBWBW - Suspected fast beat with baseline wander</p><p>SSBWBW - Suspected slow beat with baseline wander</p><p>SSBIWBW - Suspected short beat interval with baseline wander</p><p>SIBIWBW - Suspected irregular beat interval with baseline wander</p></div>">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="pieChart1" class="chartContainer"></div>
                                            <div id="pieChart1-ND" class="emptyChart" style="display:none;height:268">
                                                No Data!</div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">ECG Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip14"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>NIRF - No irregular rhythm found</p><p>SLFB - Suspected a little fast beat</p><p>SFB - Suspected fast beat</p><p>SSRFB - Suspected short run of fast beat</p><p>SLSB - Suspected a little slow beat</p><p>SSB - Suspected slow beat</p><p>SIBI - Suspected irregular beat interval</p><p>SFBWSBI - Suspected fast beat with short beat intervel</p><p>SSBWSB - Suspected slow beat with short beat</p><p>SSBWIBI - Suspected slow beat with ireegular beat interval</p><p>SSBI - Suspected short beat interval</p><p>SFBWBW - Suspected fast beat with baseline wander</p><p>SSBWBW - Suspected slow beat with baseline wander</p><p>SSBIWBW - Suspected short beat interval with baseline wander</p><p>SIBIWBW - Suspected irregular beat interval with baseline wander</p></div>">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart7" class="chartContainer"></div>
                                            <div id="barChart7-ND" class="emptyChart" style="display:none;height:268">
                                                No Data!</div>
                                        </div>
                                    </div>

                                </div>

                                <div style="margin:60px;"></div>

                            </div>
                        </div>
                    </div>

                    <!-- ************** Date picker Modal for Statistics Custom button **************** -->
                    <div id="statsModal" class="modal fade" role="dialog" data-backdrop="false">
                        <div class="modal-dialog modal-sm">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header" style="border-bottom: 3px solid #3c8dbc;">
                                    <button id="btnSubmit" type="button" class="close" onClick="resetReportDatePicker()"
                                        data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Select Date</h4>
                                </div>
                                <div class="modal-body">

                                    <label>Start : </label>
                                    <div id="datepicker3" class="input-group date" data-date-format="dd/mm/yyyy">
                                        <input id="datepicker3Val" class="form-control" type="text" readonly /> <span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-calendar"></i></span>
                                    </div>

                                    <label>End : </label>
                                    <div id="datepicker4" class="input-group date" data-date-format="dd/mm/yyyy">
                                        <input id="datepicker4Val" class="form-control" type="text" readonly /> <span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-calendar"></i></span>
                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" style="background-color:#3c8dbc; color:white"
                                        class="btn btn-default" onClick="getPatientStats('d3');">Submit</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--------------------------------------------------- Patient List content --------------------------------------------->
                    <div id="Paris" class="tabcontent" style="display:none">
                        <div class="tab-pane" id="timeline">
                            <div class="col-md-12"
                                style="background-color: white !important; width: 100%; padding-bottom: 30px; display: inline-flex;">
                                <div class="input-group" style="margin-top: 10px;">
                                    <input type="text" id="searchedInput" class="form-control" placeholder="Search">
                                    <span class="input-group-btn">
                                        <button class="btn btn-search" type="button"
                                            style="background-color: #3c8dbc; color: white;"
                                            onclick="searchForPatient();">
                                            <i class="fa fa-search fa-fw"></i> Search
                                        </button>
                                    </span>
                                </div>

                                <div class=""></div>

                                <!-- <label>Date of Birth:<i style="color: red">*</i>
				</label> -->
                                <div class="input-group" style="margin-top: 10px; padding-left: 40px;">
                                    <div class="input-group-addon" style="background-color: #3c8dbc">
                                        <i style="color: white" class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control clearAll" id="birthDat" name="dateOfBirth"
                                        data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                    </input> <span class="input-group-btn">
                                        <button class="btn btn-search" type="button"
                                            style="background-color: #3c8dbc; color: white;" onclick="searchForDate();">
                                            <i class="fa fa-search fa-fw"></i> Search
                                        </button>
                                    </span>
                                </div>




                            </div>

                            <div class="col-md-12" id="patientListCard" style="padding-bottom: 60px;"></div>
                            <!-- Content Wrapper. Contains page content -->


                            <div class="content-wrapper" style="margin-left: 0% !important; margin-bottom: 10px;"></div>
                            <input type="hidden" id="abcdefg" value="">
                        </div>
                    </div>
                </div>

            </div>







            <footer class="main-footer"
                style="margin-left: 0% !important; position: fixed; bottom: 0px; margin-right: auto; margin-left: auto; width: 100%;">
                <!--  <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>.</strong> All rights reserved. -->
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                Copyright &copy; 2021-2022 <a href="http://www.wrizto.com/" target=_blank
                    style="color: rgb(60, 141, 188);">Wrizto
                    Healthcare Pvt. Ltd.</a> All rights reserved.
            </footer>

            <!-- Control Sidebar -->

            <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
    </body>

    </html>