var donutChart1, donutChart2, donutChart3, donutChart4, donutChart5, donutChart6, barChart1,
barChart2, barChart3, barChart4, barChart5, barChart6, barChart7, barChart8, pieChart1;
var statsDateButtonId = null;
var clientId = localStorage.getItem("clientId");
console.log("Client id local storage",clientId);
var kitId = localStorage.getItem("kitPermission");

var fromDate,toDate
var SEARCH_PARAM = "";
const DEF_DELAY = 1000;

//------- Chart Functions
	function pieChartOptions(data, chartType, height, labels, dataBGcolor, legendPosition='bottom'){
			var options = {
				noData: {
				    text: 'Loading...'
				    //text: ''
				 },
				colors: dataBGcolor,
	        	series: data,
		        chart: {
			        type: chartType,
			        height:height,
			        /*
			        events: {
				      click: function(event, chartContext, config) {
					console.log(event, chartContext, config);
				        alert("Hello");
				      }
				    }
			        */
		      	},
	      	plotOptions: {
		        pie: {
			expandOnClick:false,
		          startAngle: -90,
		          endAngle: 270,
		          donut: {
					size:'50%',
		            labels: {
		              show: true,
		               name: {
		              show: true,
		              fontSize: '15px',
		              fontFamily: 'Rubik',
		              color: '#dfsda',
		              offsetY: -10
		            },
		            value: {
		              show: true,
		              fontSize: '12px',
		              fontFamily: 'Helvetica, Arial, sans-serif',
		              color: undefined,
		              offsetY: 0,
		              formatter: function (val) {
		                return val
		              }
		            },
		              total: {
						showAlways: true,
		              show: true,
		              label: 'Total',
		              fontSize: '12px',
		              color: '#373d3f',
		              formatter: function (w) {
		                return w.globals.seriesTotals.reduce((a, b) => {
		                  return a + b
		                }, 0)
		              }
		            }
		            }
		          }
		        }
	      	},
		      dataLabels: {
			     enabled: true,
			        style: {
			            fontSize: '8px',
			            fontFamily: 'Helvetica, Arial, sans-serif',
			        },
			        formatter: function(value) {
				//console.log("DONUT: ",value);
				if(Number.isInteger(value)){
					return value + "%";
				} else{
					return  value.toFixed(1) + "%";
				}

			            //return  Math.round(value) + "%";
			        }
			  },
		       fill: {
		        type: 'gradient',
		        colors: dataBGcolor
		       },
		       legend: {
		        position: legendPosition
		       },
		      	labels: labels,
	      };
	      return options;
		}

	function stckedBarChartOptions(data, chartType, height, stacked,xAxisLabels, dataBGcolor, horizontal=false) {
		//height = 169;
			var options2 = {
				noData: {
				    text: 'Loading...'
				    //text: ''
				},
				colors: dataBGcolor,
			    series: data,
			    chart: {
			      type: chartType,
			      height: height,
			      stacked: stacked,
			      parentHeightOffset: 0,
			      toolbar: {
			        show: false
			      },
			      // zoom: {
			      //   enabled: true
			      // }
			    },

			    // responsive: [{
			    //   breakpoint: 480,
			    //   options: {
			    //     legend: {
			    //       position: 'bottom',
			    //       offsetX: -15,
			    //       offsetY: 0
			    //     }
			    //   }
			    // }],
			     plotOptions: {
			       bar: {
			         horizontal: horizontal,
			         //borderRadius: 10
			       },
			     },
			    xaxis: {
			      // type: 'datetime',
			      categories: xAxisLabels,

			    },
			    legend: {
			      position: 'top',

			    },
			    fill: {
			      opacity: 1,
			      colors:dataBGcolor
			    }
			  };
		    return options2;
		  }
//-------
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms || DEF_DELAY));
}


async function doAjax(args) {
    let result;

    try {
        result = await $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: args
        });

        return result;
    } catch (error) {
        console.error(error);
    }
}

function resetReportDatePicker(){
		$("#datepicker1").datepicker('update', new Date());
		$("#datepicker2").datepicker('update', new Date());
		$("#datepicker3").datepicker('update', new Date());
		$("#datepicker4").datepicker('update', new Date());
	};


function downloadStatsPdfOrCsv(vitalDownloadName,format){


    let requestpdfURL = serverPath + "/getPatientStatsByVitalName?clientId=" + clientId + "&startEpoch=" + fromDate + "&endEpoch=" + toDate + "&vitalName=" + vitalDownloadName + "&output=" + format;
        if (kitId) {
            requestpdfURL += "&kitId=" + kitId;
        }

        if($("#searchedRegion").val() != ""){
            requestpdfURL += "&searchParameter="+ SEARCH_PARAM;
      }

	//let url = serverPath + "/getPatientStatsByVitalName?serverKeyId="
    let url = requestpdfURL
	return url;
};
function getPatientStatsByVitalName(vitalNameRxd){
	let vitalName = vitalNameRxd;
		//console.log("getPatientStatsByVitalName: ", vitalName);
		let vitalTableHeader="<tr> <th class='text-center'>No.</th> <th >Name</th> <th class='text-center'>Age</th> <th>Gender</th> <th class='text-center'>Result</th> <th class='text-center'>Unit</th> <th class='text-center'>Category</th> <th class='text-center'>Recorded At</th> </tr>";
		if(vitalName == "BG") vitalName = "Blood Glucose";
		else if(vitalName=="BP") vitalName = "Blood Pressure";
		else if(vitalName=="TEMPERATURE") vitalName = "Temperature";
		else if(vitalName=="Pulse") vitalName = "Pulse Rate"
		$("#tableModalLongTitle").html("<h5 style='color:#367fa9;font-size:25;text-align:center;margin:5px 0px;'>Patient Statistics</h5><h5 style='color:black;font-size:13;text-align:center;margin:5px 0px;'><span style='padding:2;box-shadow:-5px -5px 20px #3c8dbc inset;'>" + vitalName + "</span></h5>");
		$("#statsTableEmpty").show();
		$("#statsTableEmpty").html("Loading...");

		// $('#tableModalCenter').on('show.bs.modal', function (e) {
		//     let $trigger = $(e.relatedTarget);
		//     // alert($trigger.data('button'));
		//     vitalModalName=$trigger.data('button');
		// 	//console.log("getPatient "+vitalName);

		// });

		//Search Parameter
        //        if("SEARCH_PARAM" && ""){
        //            reqURL += + "&searchParameter="+ SEARCH_PARAM;
        //        }
        let requestURL = serverPath + "/getPatientStatsByVitalName?clientId=" + clientId + "&startEpoch=" + fromDate + "&endEpoch=" + toDate + "&vitalName=" + vitalNameRxd;
        if (kitId) {
            requestURL += "&kitId=" + kitId;
        }

        if($("#searchedRegion").val() != ""){
            requestURL += "&searchParameter="+ SEARCH_PARAM;
      }


		$.ajax({
			type : "GET",
			// url : serverPath + "/getPatientStatsByVitalName?serverKeyId="
			// 		+ localStorage.getItem("serverKeyId") + "&startEpoch="
            url : requestURL,
			headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
			success : function(data){
				// console.log("getPatientStatsByVitalName: success");
                console.log("Kit ID and Serach Parameter",kitId,SEARCH_PARAM);
				$("#statsTableEmpty").hide();
				$("#patientStatsTable1").show();
				let align = "style=\"text-align: center;\""

				let vitalTableBody = "", vitalMeasuredTime="";
				let i = 0;
				$.each(data.content.patientVitalsList,function(index,option){
					//console.log("strMonitoredDate->",option)
					if(option.monitoredDate != null && option.monitoredDate != ""){

						// vitalMeasuredTime = getCustomDatetime(strToDate(option.monitoredDate));
						vitalMeasuredTime = option.monitoredDate;
					}
					// let patientName = option.patientFirstName + " " + option.patientLastName;
					let patientName = option.patientName;
					let vitalTableRow;
					if(vitalName!=="BMI") vitalTableRow = "<tr> <td " +align+">" + ++i + "</td> <td>" + patientName + "</td> <td "+align+">" + option.age + "</td> <td>" + option.gender + "</td> <td "+align+">" + option.vitalValue + "</td> <td "+align+">" + option.vitalUnit + "</td> <td "+align+">" + option.category + "</td> <td "+align+">" + vitalMeasuredTime + "</td></tr>";
					else vitalTableRow = "<tr> <td " +align+">"+ ++i + "</td> <td>" + patientName + "</td> <td "+align+">" + option.age + "</td> <td>" + option.gender + "</td> <td "+align+">" + option.vitalValue + "</td> <td "+align+">" + option.vitalUnit + "<sup>2</td> <td "+align+">" + option.category + "</td> <td "+align+">" + vitalMeasuredTime + "</td></tr>";
					vitalTableBody += vitalTableRow;

				});
				$("#patientStatsHeader").html(vitalTableHeader);
				$("#patientStatsBody").html(vitalTableBody);
			},
			error : function(xhr, status, error){
				console.log("getPatientStatsByVitalName: error", error);
				if (xhr.status == 0) window.location.href = './login';
				$("#patientStatsTable1").hide();
				$("#statsTableEmpty").show();
				$("#statsTableEmpty").html("No Data!");
				if(xhr.status == 0) window.location.href = './login';
				if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
					if (xhr.responseJSON.code === 401) {
						(async () => {
							toastr.error(xhr.responseJSON.message);
							await sleep();
							window.location.href = './login';
						})()
					}
					toastr.error(xhr.responseJSON.message);
				} else {
					toastr.error("No Data Found!");
				}

			}

	})
};


//API CALL:
	 function getPatientStats(buttonId, allowApiCall=false){
		//------Verification
		console.log("buttonTest: ", buttonId,", api: ", allowApiCall, (!allowApiCall && (buttonId == statsDateButtonId && buttonId != "d3")));
		$(".normalCol").css("background","#B8B8B8");
		$("#"+buttonId).css("background","#367fa9");
		if(!allowApiCall && (buttonId == statsDateButtonId && buttonId != "d3")) {
			console.log("Same button cliked: ", buttonId);
			return;
		} else {
			statsDateButtonId = buttonId;
		}

        console.log('From: ', $('#datepicker3Val').val(), ', To: ', $('#datepicker4Val').val());

        var values = $('#datepicker3Val').val().split("/");
        fromDate = new Date(values[2], (values[1]-1), values[0]);
        var values2 = $('#datepicker4Val').val().split("/");
        toDate = new Date(values2[2], (values2[1]-1), values2[0]);
        // fromDate = new Date (Date.parse( $('#datepicker3Val').val().replace("-", "/") ));
        // toDate = new Date(Date.parse( $('#datepicker4Val').val().replace("-", "/") ));
		console.log("DatePicker Today: ","From: ",fromDate, "To: ", toDate, '---YEAR---',values[2], '-',values2[2]);


		if(buttonId == "d2"){
			//fromDate = new Date().getTime() - 60 * 60 * 24 * 7 * 1000;
			//toDate = new Date().getTime() - 60 * 60 * 24 * 1 * 1000;
			var lastWeekDay = new Date(new Date().getTime() - 60 * 60 * 24 * 7 * 1000);
			var yesterday = new Date(new Date().getTime() - 60 * 60 * 24 * 1 * 1000);
			fromDate = Date.UTC(lastWeekDay.getFullYear(), lastWeekDay.getMonth(), lastWeekDay.getDate(),
			    0, 0, 0, lastWeekDay.getMilliseconds());
			toDate = Date.UTC(yesterday.getFullYear(), yesterday.getMonth(), yesterday.getDate(),
			    23, 59, 59, yesterday.getMilliseconds());
			console.log("LastWeek: ","From: ",fromDate, "To: ", toDate);
		}
		else if(buttonId == "d3"){

			var today = new Date().setHours("23", "59", 59, 0);
			if (toDate == null || toDate=='Invalid Date' || fromDate == null || fromDate == 'Invalid Date') {
				toastr.error('Selected date is null');
				return;
			}
            console.log('Inside CUSTOM-> ', today, ', from: ', fromDate, ', to: ', toDate)

			if (toDate.getTime() > today || fromDate.getTime() > today) {
				toastr.error('Date Cannot Be Greater Than Todays Date');
				return;
			}
			if (toDate.getTime() < fromDate.getTime()) {
				toastr.error('To Date cannot be Lesser Than From Date');
				return;
			}
			fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),
			    0, 0, 0, fromDate.getMilliseconds());
			toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),
			    23, 59, 59, toDate.getMilliseconds());
			$('#statsModal').modal('hide');
			//resetReportDatePicker();
		}
		else {
			//fromDate = new Date().setHours(0, 0, 0,0, 0);
			//toDate = new Date().setHours(23, 59, 59, 0);
			var today = new Date();
			fromDate = Date.UTC(today.getFullYear(), today.getMonth(), today.getDate(),
			    0, 0, 0, today.getMilliseconds());
			toDate = Date.UTC(today.getFullYear(), today.getMonth(), today.getDate(),
			    23, 59, 59, today.getMilliseconds());
			//console.log("Today: ", "From: ", fromDate, "To: ", toDate);
		}

        // console.log("Custom1: ", "From: ", fromDate, "To: ", toDate);
        fromDate =new Date(fromDate).getTime();
        toDate = new Date(toDate).getTime();
        var SEARCH_PARAM = $("#searchedRegion").val();
         console.log("Searched for reagion---------->", SEARCH_PARAM);

		// GMT DATE TIMESTAMP convertion:
		//fromDate = fromDate + new Date().getTimezoneOffset() * 60000;
		//toDate = toDate + new Date().getTimezoneOffset() * 60000;
		//fromDate = new Date(Date.UTC(2021, 12,30, 0, 0, 59)).getTime();
		//toDate = new Date(Date.UTC(2021, 12,30, 23, 59, 59)).getTime();
		//console.log("UTC:",new Date().getTimezoneOffset(), ", ",toDate);

		//console.log("NEW DATE: ", "From: ", new Date(fromDate), "To: ", new Date(toDate));


		//console.log( "ProfileId: ", profileID, "ServerKey: ", serverKeyId2);
		//------ API Call

		donutChart1.updateSeries([]);
		donutChart2.updateSeries([]);
		donutChart3.updateSeries([]);
		donutChart4.updateSeries([]);
		donutChart5.updateSeries([]);
		donutChart6.updateSeries([]);
		//donutChart7.updateSeries([]);
		barChart1.updateSeries([]);
		barChart2.updateSeries([]);
		barChart3.updateSeries([]);
		barChart4.updateSeries([]);
		barChart5.updateSeries([]);
		barChart6.updateSeries([]);
		//barChart7.updateSeries([]);


		$("#donutChart1").show();
        $("#donutChart1-ND").hide();
        $("#donutChart2").show();
        $("#donutChart2-ND").hide();
        $("#donutChart3").show();
        $("#donutChart3-ND").hide();
        $("#donutChart4").show();
        $("#donutChart4-ND").hide();
        $("#donutChart5").show();
        $("#donutChart5-ND").hide();
        $("#donutChart6").show();
        $("#donutChart6-ND").hide();
        //$("#donutChart7").show();
        //$("#donutChart7-ND").hide();
		$("#barChart1").show();
		$("#barChart1-ND").hide();
		$("#barChart2").show();
		$("#barChart2-ND").hide();
		$("#barChart3").show();
		$("#barChart3-ND").hide();
		$("#barChart4").show();
		$("#barChart4-ND").hide();
		$("#barChart5").show();
		$("#barChart5-ND").hide();
		$("#barChart6").show();
		$("#barChart6-ND").hide();
		$("#barChart7").show();
		$("#barChart7-ND").hide();

		$('#dropdownMenuBtn1').css({"opacity": 1})
		$('#dropdownMenuBtn2').css({"opacity": 1})
		$('#dropdownMenuBtn3').css({"opacity": 1})
		$('#dropdownMenuBtn4').css({"opacity": 1})
		$('#dropdownMenuBtn5').css({"opacity": 1})
		$('#dropdownMenuBtn6').css({"opacity": 1})
		$('#dropdownMenuBtn7').css({"opacity": 1})
		$('#dropdownMenuBtn8').css({"opacity": 1})
		$('#dropdownMenuBtn9').css({"opacity": 1})
		$('#dropdownMenuBtn10').css({"opacity": 1})
		$('#dropdownMenuBtn11').css({"opacity": 1})
		$('#dropdownMenuBtn12').css({"opacity": 1})


        let reqURL = serverPath + "/getPatientVitalAnalysis?clientId="+ clientId + "&startEpoch="+ fromDate + "&endEpoch=" + toDate;
        if(kitId){
        //Comma separated KitID
            reqURL += "&kitId=" + kitId;
        }
        //Search Parameter
       if($("#searchedRegion").val() != ""){
           reqURL += "&searchParameter="+ SEARCH_PARAM;
     }
        console.log("KIT ID: ", kitId,SEARCH_PARAM,reqURL);

           // download button disabled
           document.querySelector('#dwndAllCharts').classList.add('disabled');
                 $('#dwndAllCharts').css({"opacity": 0.3})

		//var serverPath = "http://localhost:8081/sPopulationCare";
		$.ajax({
        			type : "GET",
        			// url : serverPath + "/getPatientVitalAnalysis?serverKeyId="
                    //+ 7 + "&startEpoch="+ 1636482601000 + "&endEpoch=" + 1636741799000,
                    // + localStorage.getItem("serverKeyId") + "&startEpoch="+ fromDate + "&endEpoch=" + toDate,
                    url : reqURL,
        			headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
        			success : function(data) {
        					//console.log("Success: ",data);
                  if(data["error"] === false){
                      let variable =false

        			if(data.content.hasOwnProperty("pulseStats")){
                      var pulseKey = data.content.pulseStats;
        			  if(document.querySelector('#dropdownMenuBtn1').classList.contains('disabled')){
        				  document.querySelector('#dropdownMenuBtn1').classList.remove('disabled');
        			     $('#dropdownMenuBtn1').css({"opacity": 1})
        				}

                      donutChart1.updateSeries([pulseKey.normal, pulseKey.abnormal]);
                      if(pulseKey.normal==0 && pulseKey.abnormal==0){
        				$("#donutChart1").hide();
                      	$("#donutChart1-ND").show();
        				document.querySelector('#dropdownMenuBtn1').classList.add('disabled');
        				$('#dropdownMenuBtn1').css({"opacity": 0.2})

        			  } else {$("#donutChart1").show();
                            variable=true;
        			  }
                    }



                    if(data.content.hasOwnProperty("temperatureStats")){
                      var tempKey = data.content.temperatureStats;

        			  if(document.querySelector('#dropdownMenuBtn2').classList.contains('disabled')){
        				  document.querySelector('#dropdownMenuBtn2').classList.remove('disabled');
        				  $('#dropdownMenuBtn2').css({"opacity": 1})
        				}

        			  donutChart2.updateSeries([tempKey.normal, tempKey.abnormal]);
                      if(tempKey.normal==0 && tempKey.abnormal==0){
        				$("#donutChart2").hide();
                      	$("#donutChart2-ND").show();
        				document.querySelector('#dropdownMenuBtn2').classList.add('disabled');
        				$('#dropdownMenuBtn2').css({"opacity": 0.2})

        			  } else {$("#donutChart2").show();

        			   variable=true;
        			  }
                    }

                    if(data.content.hasOwnProperty("systolicBPStats")){
                      var bpKey = data.content.systolicBPStats;

        			  if(document.querySelector('#dropdownMenuBtn3').classList.contains('disabled')){
        				document.querySelector('#dropdownMenuBtn3').classList.remove('disabled');
        				$('#dropdownMenuBtn3').css({"opacity": 1})
        			  }

                      donutChart3.updateSeries([bpKey.optimal, bpKey.normal, bpKey.highNormal, bpKey.g1Hypertension, bpKey.g2Hypertension, bpKey.g3Hypertension]);

                      if(bpKey.optimal==0 && bpKey.normal==0 && bpKey.highNormal==0 && bpKey.g1Hypertension==0 && bpKey.g2Hypertension==0 && bpKey.g3Hypertension==0){
        				$("#donutChart3").hide();
                      	$("#donutChart3-ND").show();

        				document.querySelector('#dropdownMenuBtn3').classList.add('disabled');
        				$('#dropdownMenuBtn3').css({"opacity": 0.2})
        			  } else {$("#donutChart3").show();
        			  variable=true;   }

                    }

                    if(data.content.hasOwnProperty("spo2Stats")){
                      var spo2Key = data.content.spo2Stats;

        			  if(document.querySelector('#dropdownMenuBtn4').classList.contains('disabled')){
        				  document.querySelector('#dropdownMenuBtn4').classList.remove('disabled');
        				  $('#dropdownMenuBtn4').css({"opacity": 1})
        				}

                      donutChart4.updateSeries([spo2Key.normal, spo2Key.abnormal]);
                      if(spo2Key.normal==0 && spo2Key.abnormal==0){
        				$("#donutChart4").hide();
                      	$("#donutChart4-ND").show();
        				document.querySelector('#dropdownMenuBtn4').classList.add('disabled');
        				$('#dropdownMenuBtn4').css({"opacity": 0.2})
        			  } else {$("#donutChart4").show();
        			   variable=true;   }
                    }



                    if(data.content.hasOwnProperty("bloodGlucoseStats")){
                      var bgKey = data.content.bloodGlucoseStats;

        			  if(document.querySelector('#dropdownMenuBtn6').classList.contains('disabled')){
        				  document.querySelector('#dropdownMenuBtn6').classList.remove('disabled');
        				  $('#dropdownMenuBtn6').css({"opacity": 1})
        				}

                      donutChart6.updateSeries([bgKey.lowSugar, bgKey.normal, bgKey.prediabetes, bgKey.diabetes ]);
                      if(bgKey.normal==0 && bgKey.prediabetes==0 && bgKey.diabetes==0 && bgKey.lowSugar==0){
        				$("#donutChart6").hide();
                      	$("#donutChart6-ND").show();
        				  document.querySelector('#dropdownMenuBtn6').classList.add('disabled');
        				  $('#dropdownMenuBtn6').css({"opacity": 0.2})
        			  } else {$("#donutChart6").show();
        			  variable=true;   }
                    }

                    if(data.content.hasOwnProperty("bmiStats")){
                      var bmiKey = data.content.bmiStats;

        			  if(document.querySelector('#dropdownMenuBtn5').classList.contains('disabled')){
        				  document.querySelector('#dropdownMenuBtn5').classList.remove('disabled');
        				  $('#dropdownMenuBtn5').css({"opacity": 1})
        				}

                      donutChart5.updateSeries([bmiKey.underweight, bmiKey.normal, bmiKey.overweight, bmiKey.obese]);
                      //console.log("TEST----",(bmiKey.normal==0 && bmiKey.overweight==0 && bmiKey.obese==0 && bmiKey.underweight==0))
                      if(bmiKey.normal==0 && bmiKey.overweight==0 && bmiKey.obese==0 && bmiKey.underweight==0){
        				$("#donutChart5").hide();
                      	$("#donutChart5-ND").show();
        				document.querySelector('#dropdownMenuBtn5').classList.add('disabled');
        				$('#dropdownMenuBtn5').css({"opacity": 0.2})


        			  }
        			  else {$("#donutChart5").show();
        			 variable=true;   }
                    }

                    // ------ Bar Cahrt ------
                    if(data.content.hasOwnProperty("pulseGenderStats")){
                      var bmiGenderKey = data.content.pulseGenderStats;

        			  if(document.querySelector('#dropdownMenuBtn7').classList.contains('disabled')){
        				  document.querySelector('#dropdownMenuBtn7').classList.remove('disabled');
        				  $('#dropdownMenuBtn7').css({"opacity": 1})
        				}

                      if(bmiGenderKey.hasOwnProperty("male") && bmiGenderKey.hasOwnProperty("female")){
        				barChart1.updateSeries([{name:"Male", data: [bmiGenderKey.male.normal, bmiGenderKey.male.abnormal]},
        				{name:"Female", data: [bmiGenderKey.female.normal, bmiGenderKey.female.abnormal]}]);
        			  }
        			  if(bmiGenderKey.male.normal==0 && bmiGenderKey.male.abnormal==0 &&
        			  bmiGenderKey.female.normal==0 && bmiGenderKey.female.abnormal==0){
        				$("#barChart1").hide();
                      	$("#barChart1-ND").show();
        				document.querySelector('#dropdownMenuBtn7').classList.add('disabled');
        				$('#dropdownMenuBtn7').css({"opacity": 0.2})
        			  } else {$("#barChart1").show();
        			   variable=true;  }
                    }

                    if(data.content.hasOwnProperty("temperatureGenderStats")){
                      var bmiGenderKey = data.content.temperatureGenderStats;

        			  if(document.querySelector('#dropdownMenuBtn8').classList.contains('disabled')){
        				  document.querySelector('#dropdownMenuBtn8').classList.remove('disabled');
        				  $('#dropdownMenuBtn8').css({"opacity": 1})
        				}

                      if(bmiGenderKey.hasOwnProperty("male") && bmiGenderKey.hasOwnProperty("female")){
        				barChart2.updateSeries([{name:"Male", data: [bmiGenderKey.male.normal, bmiGenderKey.male.abnormal]},
        				{name:"Female", data: [bmiGenderKey.female.normal, bmiGenderKey.female.abnormal]}]);
        			  }
        			  if(bmiGenderKey.male.normal==0 && bmiGenderKey.male.abnormal==0 &&
        			  bmiGenderKey.female.normal==0 && bmiGenderKey.female.abnormal==0){
        				$("#barChart2").hide();
                      	$("#barChart2-ND").show();
        				document.querySelector('#dropdownMenuBtn8').classList.add('disabled');
        				$('#dropdownMenuBtn8').css({"opacity": 0.2})
        			  } else {$("#barChart2").show();
        			  variable=true;}
                    }

                    if(data.content.hasOwnProperty("spo2GenderStats")){
                      var bmiGenderKey = data.content.spo2GenderStats;

        			  if(document.querySelector('#dropdownMenuBtn10').classList.contains('disabled')){
        				  document.querySelector('#dropdownMenuBtn10').classList.remove('disabled');
        				  $('#dropdownMenuBtn10').css({"opacity": 1})
        				}

                      if(bmiGenderKey.hasOwnProperty("male") && bmiGenderKey.hasOwnProperty("female")){
        				barChart4.updateSeries([{name:"Male", data: [bmiGenderKey.male.normal, bmiGenderKey.male.abnormal]},
        				{name:"Female", data: [bmiGenderKey.female.normal, bmiGenderKey.female.abnormal]}]);
        			  }
        			  if(bmiGenderKey.male.normal==0 && bmiGenderKey.male.abnormal==0 &&
        			  bmiGenderKey.female.normal==0 && bmiGenderKey.female.abnormal==0){
        				$("#barChart4").hide();
                      	$("#barChart4-ND").show();
        				document.querySelector('#dropdownMenuBtn10').classList.add('disabled');
        				$('#dropdownMenuBtn10').css({"opacity": 0.2})
        			  } else {$("#barChart4").show();
        			  variable=true;  }
                    }

                    //donutChart3.updateSeries([bpKey.optimal, bpKey.normal, bpKey.highNormal, bpKey.g1Hypertension, bpKey.g2Hypertension, bpKey.g3Hypertension]);
                    if(data.content.hasOwnProperty("bloodGlucoseGenderStats")){
                      var bgGenderKey = data.content.bloodGlucoseGenderStats;

        			  if(document.querySelector('#dropdownMenuBtn12').classList.contains('disabled')){
        				  document.querySelector('#dropdownMenuBtn12').classList.remove('disabled');
        				  $('#dropdownMenuBtn12').css({"opacity": 1})
        				}

                      if(bgGenderKey.hasOwnProperty("male") && bgGenderKey.hasOwnProperty("female")){
        				barChart6.updateSeries([{name:"Male", data: [bgGenderKey.male.lowSugar, bgGenderKey.male.normal, bgGenderKey.male.prediabetes, bgGenderKey.male.diabetes ]},
        				{name:"Female", data:[bgGenderKey.female.lowSugar, bgGenderKey.female.normal, bgGenderKey.female.prediabetes, bgGenderKey.female.diabetes]}]);
        			  }
        			  if(bgGenderKey.male.normal==0 && bgGenderKey.male.prediabetes==0 && bgGenderKey.male.diabetes==0 && bgGenderKey.male.lowSugar==0 &&
        			  	bgGenderKey.female.normal==0 && bgGenderKey.female.prediabetes==0 && bgGenderKey.female.diabetes==0 && bgGenderKey.female.lowSugar==0){
        				$("#barChart6").hide();
                      	$("#barChart6-ND").show();
        				document.querySelector('#dropdownMenuBtn12').classList.add('disabled');
        				$('#dropdownMenuBtn12').css({"opacity": 0.2})
        			  } else {$("#barChart6").show();
        			variable=true;  }
                    }
                    if(data.content.hasOwnProperty("systolicBPGenderStats")){
                      var bpKey = data.content.systolicBPGenderStats;

        			  if(document.querySelector('#dropdownMenuBtn9').classList.contains('disabled')){
        				document.querySelector('#dropdownMenuBtn9').classList.remove('disabled');
        				$('#dropdownMenuBtn9').css({"opacity": 1})
        			  }

                      if(bpKey.hasOwnProperty("male") && bpKey.hasOwnProperty("female")){
        				barChart3.updateSeries([{name:"Male", data:[bpKey.male.optimal, bpKey.male.normal, bpKey.male.highNormal, bpKey.male.g1Hypertension,bpKey.male.g2Hypertension,bpKey.male.g3Hypertension]},
        				{name:"Female", data:[bpKey.female.optimal, bpKey.female.normal, bpKey.female.highNormal, bpKey.female.g1Hypertension,bpKey.female.g2Hypertension,bpKey.female.g3Hypertension]}]);
        			  }
        			  //console.log("systolicBPGenderStats-> ", bpKey);

        			  if(bpKey.male.optimal==0 && bpKey.male.normal==0 && bpKey.male.highNormal==0 && bpKey.male.g1Hypertension==0 && bpKey.male.g2Hypertension==0 && bpKey.male.g3Hypertension==0 &&
        			  	bpKey.female.optimal==0 && bpKey.female.normal==0 && bpKey.female.highNormal==0 && bpKey.female.g1Hypertension==0 && bpKey.female.g2Hypertension==0 && bpKey.female.g3Hypertension==0){
        				$("#barChart3").hide();
                      	$("#barChart3-ND").show();
        				document.querySelector('#dropdownMenuBtn9').classList.add('disabled');
        				$('#dropdownMenuBtn9').css({"opacity": 0.2})
        			  } else {$("#barChart3").show();
        			  variable=true;   }

                    }

                    if(data.content.hasOwnProperty("bmiGenderStats")){
                      var bmiGenderKey = data.content.bmiGenderStats;

        			  if(document.querySelector('#dropdownMenuBtn11').classList.contains('disabled')){
        				document.querySelector('#dropdownMenuBtn11').classList.remove('disabled');
        				$('#dropdownMenuBtn11').css({"opacity": 1})
        			}

                      if(bmiGenderKey.hasOwnProperty("male") && bmiGenderKey.hasOwnProperty("female")){
        				barChart5.updateSeries([{name:"Male", data: [bmiGenderKey.male.underweight, bmiGenderKey.male.normal, bmiGenderKey.male.overweight, bmiGenderKey.male.obese]},
        				{name:"Female", data: [bmiGenderKey.female.underweight, bmiGenderKey.female.normal, bmiGenderKey.female.overweight, bmiGenderKey.female.obese]}]);
        			  }
        			  if(bmiGenderKey.male.normal==0 && bmiGenderKey.male.overweight==0 && bmiGenderKey.male.obese==0 && bmiGenderKey.male.underweight==0 &&
        			  bmiGenderKey.female.normal==0 && bmiGenderKey.female.overweight==0 && bmiGenderKey.female.obese==0 && bmiGenderKey.female.underweight==0){
        				$("#barChart5").hide();
                      	$("#barChart5-ND").show();

        				document.querySelector('#dropdownMenuBtn11').classList.add('disabled');
        				$('#dropdownMenuBtn11').css({"opacity": 0.2})
        			  } else {$("#barChart5").show();
        			  variable=true;
        			   }
                    }
              if(variable===true){
                document.querySelector('#dwndAllCharts').classList.remove('disabled')
                         $('#dwndAllCharts').css({"opacity": 1})
              }
              else{
                document.querySelector('#dwndAllCharts').classList.add('disabled');
                                      $('#dwndAllCharts').css({"opacity": 0.3})

              }

                         <!--Download charts as Image.png -->



                         function downloadDivAsImage1(divId) {
                          const divToDownload = document.getElementById(divId);
//                           let  chartIDs=[];
//                            function storeChartID(className) {
//                              const chartElements = document.querySelectorAll('.' + className);
//                              for (const chartElement of chartElements) {
//                                if (!chartIDs.includes(chartElement.id)) {
//                                  chartIDs.push(chartElement.id);
//                                }
//                              }
//                              }
//
//                            if($('#donutChart1').is(":hidden")){storeChartID('chart1')}
//                            if($('#donutChart2').is(":hidden")){storeChartID('chart2')}
//                            if($('#donutChart3').is(":hidden")){storeChartID('chart3')}
//                            if($("#barChart1").is(":hidden")){storeChartID('chart4')}
//                            if($("#barChart2").is(":hidden")){storeChartID('chart5')}
//                            if($("#barChart3").is(":hidden")){storeChartID('chart6')}
//                            if($('#donutChart4').is(":hidden")){ storeChartID('chart7') }
//                            if($('#donutChart5').is(":hidden")){ storeChartID('chart8')}
//                            if($('#donutChart6').is(":hidden")){storeChartID('chart9')}
//                             if($("#barChart4").is(":hidden")){storeChartID('chart10')}
//                             if($("#barChart5").is(":hidden")){storeChartID('chart11')}
//                             if($("#barChart6").is(":hidden")){storeChartID('chart12')}

                             if (!divToDownload) {
                                 console.error(`Element with id '${idToDownload}' not found.`);
                                 return;
                             }
                             const linksToExclude = Array.from(divToDownload.querySelectorAll('.dropdown-menu'));

                             html2canvas(divToDownload, {
                                 ignoreElements: function (element) {
                                     // Exclude elements with the specified classes
//                                     if (linksToExclude.includes(element) || chartIDs.includes(element.id)) {
//                                         return true;
//                                     }
                                      if (linksToExclude.includes(element) ) {
                                             return true;
                                        }
                                     return false;
                                 }
                             }).then(function (canvas) {
                                 canvas.toBlob(function (blob) {
                                     const fileName = `${divId}_image.png`;
                                     const link = document.createElement('a');
                                     link.href = URL.createObjectURL(blob);
                                     link.download = fileName;
                                     document.body.appendChild(link);
                                     link.click();
                                     document.body.removeChild(link);

                                 });
                             });
                         }


                    var dwndAllCharts = document.getElementById('dwndAllCharts');

                          dwndAllCharts.onclick = function(event) {
                              if (!this.classList.contains('disabled')) {
                                  console.log("the button is enabled");

                                  downloadDivAsImage1('All charts');

                              } else {
                                  console.log("the button is disabled");
                                  event.preventDefault();
                              }
                          };
                  } else{
        				$("#donutChart1").hide();
                      	$("#donutChart1-ND").show();
                      	$("#donutChart2").hide();
                      	$("#donutChart2-ND").show();
                      	$("#donutChart3").hide();
                      	$("#donutChart3-ND").show();
                      	$("#donutChart4").hide();
                      	$("#donutChart4-ND").show();
                      	$("#donutChart5").hide();
                      	$("#donutChart5-ND").show();
                      	$("#donutChart6").hide();
                      	$("#donutChart6-ND").show();
        				$("#barChart1").hide();
        	            $("#barChart1-ND").show();
        				$("#barChart2").hide();
        	            $("#barChart2-ND").show();
        	            $("#barChart3").hide();
        	            $("#barChart3-ND").show();
        	            $("#barChart4").hide();
        	            $("#barChart4-ND").show();
        	            $("#barChart5").hide();
        	            $("#barChart5-ND").show();
        	            $("#barChart6").hide();
        	            $("#barChart6-ND").show();

        				document.querySelector('#dropdownMenuBtn1').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn2').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn3').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn4').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn5').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn6').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn7').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn8').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn9').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn10').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn11').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn12').classList.add('disabled');

        				$('#dropdownMenuBtn1').css({"opacity": 0.2})
        				$('#dropdownMenuBtn2').css({"opacity": 0.2})
        				$('#dropdownMenuBtn3').css({"opacity": 0.2})
        				$('#dropdownMenuBtn4').css({"opacity": 0.2})
        				$('#dropdownMenuBtn5').css({"opacity": 0.2})
        				$('#dropdownMenuBtn6').css({"opacity": 0.2})
        				$('#dropdownMenuBtn7').css({"opacity": 0.2})
        				$('#dropdownMenuBtn8').css({"opacity": 0.2})
        				$('#dropdownMenuBtn9').css({"opacity": 0.2})
        				$('#dropdownMenuBtn10').css({"opacity": 0.2})
        				$('#dropdownMenuBtn11').css({"opacity": 0.2})
        				$('#dropdownMenuBtn12').css({"opacity": 0.2})
        			}


        			},
        			error : function(xhr, status, error) {
        				//console.log("error:", data);
        				$("#donutChart1").hide();
                      	$("#donutChart1-ND").show();
                      	$("#donutChart2").hide();
                      	$("#donutChart2-ND").show();
                      	$("#donutChart3").hide();
                      	$("#donutChart3-ND").show();
                      	$("#donutChart4").hide();
                      	$("#donutChart4-ND").show();
                      	$("#donutChart5").hide();
                      	$("#donutChart5-ND").show();
                      	$("#donutChart6").hide();
                      	$("#donutChart6-ND").show();
        				$("#barChart1").hide();
        	            $("#barChart1-ND").show();
        				$("#barChart2").hide();
        	            $("#barChart2-ND").show();
        	            $("#barChart3").hide();
        	            $("#barChart3-ND").show();
        	            $("#barChart4").hide();
        	            $("#barChart4-ND").show();
        	            $("#barChart5").hide();
        	            $("#barChart5-ND").show();
        	            $("#barChart6").hide();
        	            $("#barChart6-ND").show();

        				document.querySelector('#dropdownMenuBtn1').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn2').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn3').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn4').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn5').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn6').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn7').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn8').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn9').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn10').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn11').classList.add('disabled');
        				document.querySelector('#dropdownMenuBtn12').classList.add('disabled');

        				$('#dropdownMenuBtn1').css({"opacity": 0.2})
        				$('#dropdownMenuBtn2').css({"opacity": 0.2})
        				$('#dropdownMenuBtn3').css({"opacity": 0.2})
        				$('#dropdownMenuBtn4').css({"opacity": 0.2})
        				$('#dropdownMenuBtn5').css({"opacity": 0.2})
        				$('#dropdownMenuBtn6').css({"opacity": 0.2})
        				$('#dropdownMenuBtn7').css({"opacity": 0.2})
        				$('#dropdownMenuBtn8').css({"opacity": 0.2})
        				$('#dropdownMenuBtn9').css({"opacity": 0.2})
        				$('#dropdownMenuBtn10').css({"opacity": 0.2})
        				$('#dropdownMenuBtn11').css({"opacity": 0.2})
        				$('#dropdownMenuBtn12').css({"opacity": 0.2})
        				if(xhr.status == 0) window.location.href = './login';
        				if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
        					if (xhr.responseJSON.code === 401) {
        						(async () => {
        							toastr.error(xhr.responseJSON.message);
        							await sleep();
        							window.location.href = './login';
        						})()
        					}
        					//toastr.error(xhr.responseJSON.message);
        				} else {
        					toastr.error("No Data Found!");
        				}
        			}
        		});
		//Search Parameter
        //        if("SEARCH_PARAM" && ""){
        //            reqURL += + "&searchParameter="+ SEARCH_PARAM;
        //        }
        let reqEcgURL = serverPath + "/getPatientEcgAnalysis?clientId="+ clientId + "&startEpoch="+ fromDate + "&endEpoch=" + toDate;
        if(kitId){
        //Comma separated KitID
            reqEcgURL += "&kitId=" + kitId;
        }
        //Search Parameter
       if($("#searchedRegion").val() != ""){
           reqEcgURL += "&searchParameter=" + SEARCH_PARAM;
     }
        console.log("KIT ID: ", kitId,SEARCH_PARAM);


		$.ajax({
			type : "GET",
			// url : serverPath + "/getPatientEcgAnalysis?serverKeyId=" + localStorage.getItem("serverKeyId")
            url : reqEcgURL,
			headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
			success : function(data) {
				console.log("Success: ",data);

			let ecgData = data.content;

			$("#pieChart1").hide();$("#pieChart1-ND").show();
			$("#barChart7").hide();$("#barChart7-ND").show();
			// $("#barChart7").hide();$("#barChart7-ND").show();

			const ecgMap = new Map();
			const ecgMaleMap = new Map();
			const ecgFemaleMap = new Map();

			$.each(ecgData,function(index){
				// console.log('ecgData', index);
				if(index==="ecgStats"){
					$.each(ecgData.ecgStats,function(index,option){
						// console.log('ecgStats--> ',index, ', val: ',option);
						switch(index){
							case "normal"					:ecgMap.set("NIRF",option);break;
							case "irregularBaseLine"		:ecgMap.set("SWBW",option);break;
							case "fastBeat"					:ecgMap.set("SFB",option);break;
							case "slowBeatBaselineWander"	:ecgMap.set("SSBWBW",option);break;
							case "irregularBeatInterval"	:ecgMap.set("SIBI",option);break;
							case "slowBeat"					:ecgMap.set("SSB",option);break;
							case "fastBeatShortInterval"	:ecgMap.set("SFBWSBI",option);break;
							case "littleSlowBeat"			:ecgMap.set("SLSB",option);break;
							case "littleFastBeat"			:ecgMap.set("SLFB",option);break;
							case "shortRunFastBeat"			:ecgMap.set("SSRFB",option);break;
							case "slowBeatShortBeat"		:ecgMap.set("SSBWSB",option);break;
							case "fastBeatBaselineWander"	:ecgMap.set("SFBWBW",option);break;
							case "shortBeatBaseLineWander"	:ecgMap.set("SSBIWBW",option);break;
							case "baseLineWander"			:ecgMap.set("SIBIWBW",option);break;
							case "shortBeatInterval"		:ecgMap.set("SSBI",option);break;
							case "slowBeatIrregularInterval":ecgMap.set("SSBWIBI",option);break;
						}
					});
					// console.log('ecgMap-SIZE--> ',ecgMap.size);

					ecgMap.forEach((values,keys)=>{if(values!==0){$("#pieChart1-ND").hide();$("#pieChart1").show();}})
                    pieChart1.updateSeries([ecgMap.get('NIRF'), ecgMap.get('SWBW'), ecgMap.get('SLFB'), ecgMap.get('SFB'), ecgMap.get('SSRFB'), ecgMap.get('SLSB'), ecgMap.get('SSB'), ecgMap.get('SIBI'),ecgMap.get('SFBWSBI'), ecgMap.get('SSBWSB'), ecgMap.get('SSBWIBI'), ecgMap.get('SSBI'), ecgMap.get('SFBWBW'), ecgMap.get('SSBWBW'), ecgMap.get('SSBIWBW'), ecgMap.get('SIBIWBW')])
				}
                if(index==="ecgGenderStats"){
                    $.each(ecgData.ecgGenderStats,function(genderIndex,option){
                        if(genderIndex==="male"){
                            $.each(ecgData.ecgGenderStats.male,function(malendex,option){
                                switch(malendex){
                                    case "normal"					:ecgMaleMap.set("NIRF",option);break;
                                    case "irregularBaseLine"		:ecgMaleMap.set("SWBW",option);break;
                                    case "fastBeat"					:ecgMaleMap.set("SFB",option);break;
                                    case "slowBeatBaselineWander"	:ecgMaleMap.set("SSBWBW",option);break;
                                    case "irregularBeatInterval"	:ecgMaleMap.set("SIBI",option);break;
                                    case "slowBeat"					:ecgMaleMap.set("SSB",option);break;
                                    case "fastBeatShortInterval"	:ecgMaleMap.set("SFBWSBI",option);break;
                                    case "littleSlowBeat"			:ecgMaleMap.set("SLSB",option);break;
                                    case "littleFastBeat"			:ecgMaleMap.set("SLFB",option);break;
                                    case "shortRunFastBeat"			:ecgMaleMap.set("SSRFB",option);break;
                                    case "slowBeatShortBeat"		:ecgMaleMap.set("SSBWSB",option);break;
                                    case "fastBeatBaselineWander"	:ecgMaleMap.set("SFBWBW",option);break;
                                    case "shortBeatBaseLineWander"	:ecgMaleMap.set("SSBIWBW",option);break;
                                    case "baseLineWander"			:ecgMaleMap.set("SIBIWBW",option);break;
                                    case "shortBeatInterval"		:ecgMaleMap.set("SSBI",option);break;
                                    case "slowBeatIrregularInterval":ecgMaleMap.set("SSBWIBI",option);break;
                                }
                            });
                        }
                        else if(genderIndex==="female"){
                            $.each(ecgData.ecgGenderStats.female,function(femaleIndex,option){
                                switch(femaleIndex){
                                    case "normal"					:ecgFemaleMap.set("NIRF",option);break;
                                    case "irregularBaseLine"		:ecgFemaleMap.set("SWBW",option);break;
                                    case "fastBeat"					:ecgFemaleMap.set("SFB",option);break;
                                    case "slowBeatBaselineWander"	:ecgFemaleMap.set("SSBWBW",option);break;
                                    case "irregularBeatInterval"	:ecgFemaleMap.set("SIBI",option);break;
                                    case "slowBeat"					:ecgFemaleMap.set("SSB",option);break;
                                    case "fastBeatShortInterval"	:ecgFemaleMap.set("SFBWSBI",option);break;
                                    case "littleSlowBeat"			:ecgFemaleMap.set("SLSB",option);break;
                                    case "littleFastBeat"			:ecgFemaleMap.set("SLFB",option);break;
                                    case "shortRunFastBeat"			:ecgFemaleMap.set("SSRFB",option);break;
                                    case "slowBeatShortBeat"		:ecgFemaleMap.set("SSBWSB",option);break;
                                    case "fastBeatBaselineWander"	:ecgFemaleMap.set("SFBWBW",option);break;
                                    case "shortBeatBaseLineWander"	:ecgFemaleMap.set("SSBIWBW",option);break;
                                    case "baseLineWander"			:ecgFemaleMap.set("SIBIWBW",option);break;
                                    case "shortBeatInterval"		:ecgFemaleMap.set("SSBI",option);break;
                                    case "slowBeatIrregularInterval":ecgFemaleMap.set("SSBWIBI",option);break;
                                }
                            });
                        }
                    });
                    ecgMaleMap.forEach((values,keys)=>{if(values!==0){$("#barChart7-ND").hide();$("#barChart7").show();}})
                    ecgFemaleMap.forEach((values,keys)=>{if(values!==0){$("#barChart7-ND").hide();$("#barChart7").show();}})
                    barChart7.updateSeries([{name:"Male", data: [ecgMaleMap.get('NIRF'), ecgMaleMap.get('SWBW'), ecgMaleMap.get('SLFB'), ecgMaleMap.get('SFB'), ecgMaleMap.get('SSRFB'), ecgMaleMap.get('SLSB'), ecgMaleMap.get('SSB'), ecgMaleMap.get('SIBI'),ecgMaleMap.get('SFBWSBI'), ecgMaleMap.get('SSBWSB'), ecgMaleMap.get('SSBWIBI'), ecgMaleMap.get('SSBI'), ecgMaleMap.get('SFBWBW'), ecgMaleMap.get('SSBWBW'), ecgMaleMap.get('SSBIWBW'), ecgMaleMap.get('SIBIWBW')]},
                                {name:"Female", data: [ecgFemaleMap.get('NIRF'), ecgFemaleMap.get('SWBW'), ecgFemaleMap.get('SLFB'), ecgFemaleMap.get('SFB'), ecgFemaleMap.get('SSRFB'), ecgFemaleMap.get('SLSB'), ecgFemaleMap.get('SSB'), ecgFemaleMap.get('SIBI'),ecgFemaleMap.get('SFBWSBI'), ecgFemaleMap.get('SSBWSB'), ecgFemaleMap.get('SSBWIBI'), ecgFemaleMap.get('SSBI'), ecgFemaleMap.get('SFBWBW'), ecgFemaleMap.get('SSBWBW'), ecgFemaleMap.get('SSBIWBW'), ecgFemaleMap.get('SIBIWBW')]}]);
                }

			});
			},
			error : function(xhr, status, error) {
				// console.log("error:", data);
				  //$("#donutChart7").hide();
				  //$("#donutChart7-ND").show();
				$("#pieChart1").hide();
				$("#pieChart1-ND").show();
				$("#barChart7").hide();
				$("#barChart7-ND").show();
				if(xhr.status == 0) window.location.href = './login';
				if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
					if (xhr.responseJSON.code === 401) {
						(async () => {
							toastr.error(xhr.responseJSON.message);
							await sleep();
							window.location.href = './login';
						})()
					}
					//toastr.error(xhr.responseJSON.message);
				} else {
					toastr.error("No Data Found!");
				}
			}
		});
	}
	//------------------------
	function openCity(evt, cityName, id) {
	  //console.log("openCity: ",window.location.hash, cityName, id);
	  var i, tabcontent, tablinks;
	  tabcontent = document.getElementsByClassName("tabcontent");
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	  }
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" active", "");
	    //console.log("tablinks.class: ", tablinks[i].className);
	    //window.location.hash = id;
	  }
	  document.getElementById(cityName).style.display = "block";
	  //console.log("evt.class: ",evt.currentTarget.className, evt.currentTarget);
	  evt.currentTarget.className += " active";
	  //$("#"+tabId).addClass(" active");
	}

	function buttonTest(buttonId){
		//console.log("buttonTest: ", buttonId);
		$(".normalCol").css("background","#B8B8B8");
		$("#"+buttonId).css("background","#367fa9");

	}

$(function() {
		$("#datepicker3").datepicker({
			autoclose : true,
			todayHighlight : true
		}).datepicker('update', new Date());
	});

	$(function() {
		$("#datepicker4").datepicker({
			autoclose : true,
			todayHighlight : true
		}).datepicker('update', new Date());
	});

//-------------------- When Document is Ready ---------------------------
$(document).ready(
	function(){

		 $('#toolTip1').tooltip();
		 $('#toolTip2').tooltip();
		 $('#toolTip3').tooltip();
		 $('#toolTip4').tooltip();
		 $('#toolTip5').tooltip();
		 $('#toolTip6').tooltip();
		 $('#toolTip7').tooltip();
		 $('#toolTip8').tooltip();
		 $('#toolTip9').tooltip();
		 $('#toolTip10').tooltip();
		 $('#toolTip11').tooltip();
		 $('#toolTip12').tooltip();
		 $('#toolTip13').tooltip();
		 $('#toolTip14').tooltip();

		var donut1Labels = ["Normal","Abnormal"];
		var donut1data = [];
		var donut1BGcolor = ["#008450", "#B81D13"];
		var donut1Options = pieChartOptions(donut1data,"donut", 200, donut1Labels, donut1BGcolor);
		donutChart1 = new ApexCharts(document.querySelector("#donutChart1"), donut1Options);
        donutChart1.render();

		var donut2Labels = ["Normal","Abnormal"];
		var donut2data = [];
		var donut2BGcolor = ["#008450", "#B81D13"];
		var donut2Options = pieChartOptions(donut2data,"donut", 200, donut2Labels, donut2BGcolor);
		donutChart2 = new ApexCharts(document.querySelector("#donutChart2"), donut2Options);
        donutChart2.render();

        var donut3Labels = ["Optimal","Normal","High-Normal","G1-Hypertension", "G2-Hypertension", "G3-Hypertension"];
		var donut3data = [];
		var donut3BGcolor = ["#00ff00","#008450", "#EFB700", '#ff9999',"#ff0000","#B81D13"];
		var donut3Options = pieChartOptions(donut3data,"donut", 200, donut3Labels, donut3BGcolor);
		donutChart3 = new ApexCharts(document.querySelector("#donutChart3"), donut3Options);
        donutChart3.render();

        var donut4Labels = ["Normal","Abnormal"];
		var donut4data = [];
		var donut4BGcolor = ["#008450", "#B81D13"];
		var donut4Options = pieChartOptions(donut4data,"donut", 200, donut4Labels, donut4BGcolor);
		donutChart4 = new ApexCharts(document.querySelector("#donutChart4"), donut4Options);
        donutChart4.render();

        var donut5Labels = ["Underweight", "Normal","Overweight","Obese"];
		var donut5data = [];
		var donut5BGcolor = ["#800080", "#008450", "#EFB700", '#ff0000'];
		var donut5Options = pieChartOptions(donut5data,"donut", 200, donut5Labels, donut5BGcolor);
		donutChart5 = new ApexCharts(document.querySelector("#donutChart5"), donut5Options);
        donutChart5.render();

        var donut6Labels = ["Lowsugar","Normal","Prediabetes","Diabetes"];
		var donut6data = [];
		var donut6BGcolor = ["#800080", "#008450", "#EFB700", '#ff0000'];
		var donut6Options = pieChartOptions(donut6data,"donut", 200, donut6Labels, donut6BGcolor);
		donutChart6 = new ApexCharts(document.querySelector("#donutChart6"), donut6Options);
        donutChart6.render();

        //var donut7Labels = ["Normal","Abnormal"];
		//var donut7data = [];
		//var donut7BGcolor = ["#008450", "#B81D13"];
		//var donut7Options = pieChartOptions(donut7data,"donut", 200, donut7Labels, donut7BGcolor);
		//donutChart7 = new ApexCharts(document.querySelector("#donutChart7"), donut7Options);
        //donutChart7.render();



        //Bar Chart
        var bar1Data = [];
		var bar1xAxisLabel= ["Normal","Abnormal"];
		var bar1BGcolor = ["#005A8C", "#FF69B4"];
		var bar1Options = stckedBarChartOptions(bar1Data,"bar", 169, true, bar1xAxisLabel, bar1BGcolor);
		barChart1 = new ApexCharts(document.querySelector("#barChart1"), bar1Options);
        barChart1.render();

        var bar2Data = [];
		var bar2xAxisLabel= ["Normal","Abnormal"];
		var bar2BGcolor = ["#005A8C", "#FF69B4"];
		var bar2Options = stckedBarChartOptions(bar2Data,"bar", 169, true, bar2xAxisLabel, bar2BGcolor);
        barChart2 = new ApexCharts(document.querySelector("#barChart2"), bar2Options);
        barChart2.render();

        var bar3Data = [];
		var bar3xAxisLabel= ["Optimal","NOR","HI-NOR","G1-HTN", "G2-HTN", "G3-HTN"];
		var bar3BGcolor = ["#005A8C", "#FF69B4"];
		var bar3Options = stckedBarChartOptions(bar3Data,"bar", 169, true, bar3xAxisLabel, bar3BGcolor);
        barChart3 = new ApexCharts(document.querySelector("#barChart3"), bar3Options);
        barChart3.render();

		var bar4Data = [];
		var bar4xAxisLabel= ["Normal","Abnormal"];
		var bar4BGcolor = ["#005A8C", "#FF69B4"];
		var bar4Options = stckedBarChartOptions(bar4Data,"bar", 169, true, bar4xAxisLabel, bar4BGcolor);
        barChart4 = new ApexCharts(document.querySelector("#barChart4"), bar4Options);
        barChart4.render();

        var bar5Data = [];
		var bar5xAxisLabel= ["Underweight", "Normal","Overweight","Obese" ];
		var bar5BGcolor = ["#005A8C", "#FF69B4"];
		var bar5Options = stckedBarChartOptions(bar5Data,"bar", 169, true, bar5xAxisLabel, bar5BGcolor);
        barChart5 = new ApexCharts(document.querySelector("#barChart5"), bar5Options);
        barChart5.render();

        var bar6Data = [];
		var bar6xAxisLabel= ["Lowsugar", "Normal","Prediabetes","Diabetes"];
		var bar6BGcolor = ["#005A8C", "#FF69B4"];
		var bar6Options = stckedBarChartOptions(bar6Data,"bar", 169, true, bar6xAxisLabel, bar6BGcolor);
        barChart6 = new ApexCharts(document.querySelector("#barChart6"), bar6Options);
        barChart6.render();

        var bar7Data = [{
          name: 'Male',
          data: []
        }, {
          name: 'Female',
          data: []
        }];
        //var bar7Data = [];
		//var bar7xAxisLabel= ['NIRF', 'SLFB', 'SFB', 'SSRFB', 'SLSB', 'SSB', 'SIBI','SFBWSBI', 'SSBWSB', 'SSBWIBI', 'SSBI', 'SFBWBW', 'SSBWBW', 'SSBIWBW', 'SIBIWBW'];
		var bar7xAxisLabel= ['NIRF', 'SWBW', 'SLFB', 'SFB', 'SSRFB', 'SLSB', 'SSB', 'SIBI','SFBWSBI', 'SSBWSB', 'SSBWIBI', 'SSBI', 'SFBWBW', 'SSBWBW', 'SSBIWBW', 'SIBIWBW'];
		var bar7BGcolor = ["#005A8C", "#FF69B4"];
		//var bar7Options = stckedBarChartOptions(bar7Data,"bar", 200, true, bar7xAxisLabel, bar7BGcolor);
		var bar7Options = stckedBarChartOptions(bar7Data,"bar", 268, true, bar7xAxisLabel, bar7BGcolor);
        barChart7 = new ApexCharts(document.querySelector("#barChart7"), bar7Options);
        barChart7.render();

        /*
        var bar8Data = [{
          data: [44, 55, 41, 37, 22, 43, 21, 8, 9, 10, 11, 12, 13, 14, 15]
        }];
		var bar8xAxisLabel= ['NIRF', 'SLFB', 'SFB', 'SSRFB', 'SLSB', 'SSB', 'SIBI','SFBWSBI', 'SSBWSB', 'SSBWIBI', 'SSBI', 'SFBWBW', 'SSBWBW', 'SSBIWBW', 'SIBIWBW'];
		var bar8BGcolor = ["#005A8C"];
		var bar8Options = stckedBarChartOptions(bar8Data,"bar", 200, false, bar8xAxisLabel, bar8BGcolor, false);
        barChart8 = new ApexCharts(document.querySelector("#barChart8"), bar8Options);
        barChart8.render();
        */

        // PIE Chart :
        //var pie1Labels = ['NIRF', 'SLFB', 'SFB', 'SSRFB', 'SLSB', 'SSB', 'SIBI','SFBWSBI', 'SSBWSB', 'SSBWIBI', 'SSBI', 'SFBWBW', 'SSBWBW', 'SSBIWBW', 'SIBIWBW'];
        let pie1Labels = ['NIRF', 'SWBW', 'SLFB', 'SFB', 'SSRFB', 'SLSB', 'SSB', 'SIBI','SFBWSBI', 'SSBWSB', 'SSBWIBI', 'SSBI', 'SFBWBW', 'SSBWBW', 'SSBIWBW', 'SIBIWBW'];
		var pie1data = [];
		//var pie1BGcolor = ["#9FE2BF", "#40E0D0", "#6495ED", '#CCCCFF', '#DFFF00', '#FFBF00', '#FF7F50', '#DE3163', '#800080', '#FF00FF', '#000080', '#0000FF', '#008080', '#00FFFF', '#808000'];
		let pie1BGcolor = ["#9FE2BF", "#40E0D0", "#6495ED", '#CCCCFF', '#DFFF00', '#FFBF00', '#FF7F50', '#DE3163', '#800080', '#FF00FF', '#000080', '#0000FF', '#008080', '#00FFFF', '#808000', '#808080'];
		//var pie1Options = pieChartOptions(pie1data,"pie", 167, pie1Labels, pie1BGcolor, 'right');
		var pie1Options = pieChartOptions(pie1data,"pie", 300, pie1Labels, pie1BGcolor);
		pieChart1 = new ApexCharts(document.querySelector("#pieChart1"), pie1Options);
        pieChart1.render();

		//getPatientStats("d2");


	}
);
function strToDate(dtStr) {
	//console.log("Inside strToDate()->",dtStr)
  if (!dtStr) return null
  let dateParts = dtStr.split("/");
  let timeParts = dateParts[2].split(" ")[1].split(":");
  dateParts[2] = dateParts[2].split(" ")[0];
  // month is 0-based, that's why we need dataParts[1] - 1
  return dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0], timeParts[0], timeParts[1], timeParts[2]);
}
function getCustomDatetime(dateTime){
	return dateTime.getDate() + '/' + dateTime.getMonth()+1 + '/' + dateTime.getFullYear() + ' ' + getCustomTime(dateTime);
}

function getCustomTime(time) {
	//console.log("Inside getCustomTime()->",time)
  let ampm = "am";

  var h = time.getHours();
  var m = time.getMinutes();
  // add a zero in front of numbers<10
  if (h >= 12) {
    ampm = "pm";
    h = h % 12 === 0 ? h : h % 12;
  }

  h = checkTime(h);
  m = checkTime(m);

  //return `${h}:${m} ${ampm}`;
  return h + ':' + m  + ' ' + ampm;
}

function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function searchStatsParameter() {

console.log("Search Stats Parameter Old: ",SEARCH_PARAM, ", NEW: ", $("#searchedRegion").val() );
    SEARCH_PARAM = $("#searchedRegion").val();
    getPatientStats(statsDateButtonId, true);
}

//Download Charts

function downloadDivAsImage(divId) {
    const divToDownload = document.getElementById(divId );
    if (!divToDownload) {
        console.error(`Element with id '${idToDownload}' not found.`);
        return;
      }
    const linksToExclude = divToDownload.querySelectorAll('.dropdown-menu');

    // Create an array of elements to be ignored
    const elementsToIgnore = Array.from(linksToExclude);

    html2canvas(divToDownload, {
      ignoreElements: function (element) {
        // Ignore elements with the class 'dropdown-menu'
        return elementsToIgnore.includes(element);
      }
    }).then(function (canvas) {
      canvas.toBlob(function (blob) {
        const fileName = `${divId}_image.png`;
        const link = document.createElement('a');
        link.href = URL.createObjectURL(blob);
        link.download = fileName;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      });
    });
  }
