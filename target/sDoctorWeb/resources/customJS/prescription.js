let clientId = localStorage.getItem("clientId");
let token = localStorage.getItem("token");
//let userId1 = localStorage.getItem("userId");
let userId1 = localStorage.getItem("patientUserId");
//let drEpisodeId, episodeBtnsIndex=0;
const DEF_DELAY = 1000;
let SELECTED_EPI_ID = 0, SELECTED_EPI_BTN_INDEX = 0;

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms || DEF_DELAY));
}

function getDrEpisodeList() {

	//$('#chronologicalView').hide();
	$('#monthTrendButton').hide();
	$('#weekTrendButton').hide();
	$('#todayTrendButton').hide();

	$.ajax({
		type: "GET",
		url: serverPath + "/getDrEpisodeList?clientId=" + clientId + "&patientId=" + userId1,
		headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
		success: function (data) {
			if (data.code == "200") {

				//$('#drChronologicalView').show();

				let size = data.content.drEpisodes.length;
				s = size;

				let appendText = "";

				$.each(data.content.drEpisodes, function (index, option) {
					if (index == 0) {
						drEpisodeId = option.id;
						episodeId = option.id;
						time = option.lastUpdated;
					}
					// console.log(option.lastUpdated);
					// console.log(dayjs(option.lastUpdated).format("DD/MM/YYYY"));
					// console.log(dayjs(option.lastUpdated).format("HH:mm:ss"));
					let episodeDate = dayjs(option.lastUpdated).utc(true).format("DD/MM/YYYY");
					let episodeTime = dayjs(option.lastUpdated).utc(true).format("HH:mm:ss");

					if (size > 4) {
						if (index < 4) {
							if (index == 0) {
								appendText += '<button type="button" id="drPrevId" style="margin-left:10px;background:#B8B8B8;display:none;" class="btn btn-primary normalCol" onclick="drPrevious(' + option.id + ');">' + "PREV" + '</button>'
							}
							appendText += '<button type="button" id="dr' + index + '" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol greyOut" onclick="getDrEpisodeDetails(' + option.id + ',' + index + ');">' + '<p style="margin:0px">dr-' + episodeDate + '</p><p style="margin:0px; font-size:12px; padding: 0px; line-height: 15px;">' + episodeTime + '</p></button>'
						}
						else {
							appendText += '<button type="button" id="dr' + index + '" style="margin-left:10px;background:#B8B8B8; display:none;" class="btn btn-primary normalCol greyOut" onclick="getDrEpisodeDetails(' + option.id + ',' + index + ');">' + '<p style="margin:0px">dr-' + episodeDate + '</p><p style="margin:0px; font-size:12px; padding: 0px; line-height: 15px;">' + episodeTime + '</p></button>'
							if (index == size - 1) {
								appendText += '<button type="button" id="drNextId" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="drNext(' + option.id + ');">' + "NEXT" + '</button>'
							}
						}
					}
					else {
						appendText += '<button type="button" id="dr' + index + '" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol greyOut" onclick="getDrEpisodeDetails(' + option.id + ',' + index + ');">' + '<p style="margin:0px">dr-' + episodeDate + '</p><p style="margin:0px; font-size:12px; padding: 0px; line-height: 15px;">' + episodeTime + '</p></button>'
					}
				});
				// $("#drChronologicalView").html(appendText);

				let listIndex = 0;
				$("#dr" + listIndex).css("background", "#367fa9");

				getDrEpisodeDetails(drEpisodeId, listIndex, true);

			}
		},
		error: function (xhr, status, error) {
			if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
				if (xhr.responseJSON.code === 401) {
					(async () => {
						toastr.error(xhr.responseJSON.message);
						await sleep();
						window.location.href = './login';
					})()
				}
				//toastr.error(xhr.responseJSON.message);
			} else {
				toastr.error("No Data Found!");
			}
		}
	});
}

let currentEpisodeDate, drAdviceMongoId;
let firstLoad = 0;
let adviceOptions, followUpOptions, prelimDiagOptions, investigationOptions, complaintOptions, historyOptions, clinicalExaminOptions;

function getDrEpisodeDetails(episodeId, listIndex) {
	console.log('Inside getDrEpisodeDetails: ', doctorEpisodeId, ', ', episodeId, ', ', listIndex,
		', SELECTED_EPI_ID: ', SELECTED_EPI_ID, ', SELECTED_EPI_BTN: ', SELECTED_EPI_BTN_INDEX);

	SELECTED_EPI_ID = episodeId;
	SELECTED_EPI_BTN_INDEX = listIndex;

	if (!doctorEpisodeId) {
		console.log('Dr Episode not found: ', doctorEpisodeId);


        $("#chiefComplaintEmptyRow").show();
        $("#historyEmptyRow").show();
        $("#clinicalExaminEmptyRow").show();
		$("#prescriptionEmptyRow").show();
		$("#adviseEmptyRow").show();
		$("#followUpEmptyRow").show();
		$("#notesEmptyRow").show();
		$("#reportsEmptyRow").show();


        $("#chiefComplaintTableHeader").html("");
        $("#historyTableHeader").html("");
        $("#clinicalExaminTableHeader").html("");
		$("#prescriptionTableHeader").html("");
		$("#adviseTableHeader").html("");
		$("#followUpTableHeader").html("");
		$("#notesTableHeader").html("");
		$("#reportsTableHeader").html("");


        $("#chiefComplaintTableBody").html("");
        $("#historyTableBody").html("");
        $("#clinicalExaminTableBody").html("");
		$("#prescriptionTableBody").html("");
		$("#adviseTableBody").html("");
		$("#followUpTableBody").html("");
		$("#notesTableBody").html("");
		$("#reportsTableBody").html("");

		$(".updateButton").show();
        // $("#saveComplaintButton").show();
        // $("#saveHistoryButton").show();
        // $("#addClinicalExaminButton").show();
        $("#addChiefComplaintButton").show();
        $("#addHistoryButton1").show();
        $("#addHistoryButton2").show();
		$("#addPrescriptionButton").show();
		$("#addAdviceButton").show();
		$("#addFollowUpButton").show();
		$("#addPrelimDiagButton").show();
		$("#addInvestigationButton").show();
		return;
	}

    $("#chiefComplaintEmptyRow").show();
    $("#historyEmptyRow").show();
    $("#clinicalExaminEmptyRow").show();
	$("#prescriptionEmptyRow").show();
	$("#adviseEmptyRow").show();
	$("#followUpEmptyRow").show();
	$("#notesEmptyRow").show();
	$("#reportsEmptyRow").show();

    $("#chiefComplaintTableHeader").html("");
    $("#historyTableHeader").html("");
    $("#clinicalExaminTableHeader").html("");
	$("#prescriptionTableHeader").html("");
	$("#adviseTableHeader").html("");
	$("#followUpTableHeader").html("");
	$("#notesTableHeader").html("");
	$("#reportsTableHeader").html("");

    $("#chiefComplaintTableBody").html("");
    $("#historyTableBody").html("");
    $("#clinicalExaminTableBody").html("");
	$("#prescriptionTableBody").html("");
	$("#adviseTableBody").html("");
	$("#followUpTableBody").html("");
	$("#notesTableBody").html("");
	$("#reportsTableBody").html("");

	$.ajax({
		type: "GET",
		url: serverPath + "/getDrEpisodeDetails?drEpisodeId=" + Number(doctorEpisodeId),
		headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
		success: function (data) {
			if (data.code == "200") {

				console.log(data);
				// console.log(data.content.drEpisodeDetails.drAdvice);
				// console.log(data.content.drEpisodeDetails.drPrescription);

				let presList = "";
				presHeader = '<tr><th>Medicine</th><th>Medicine Type</th><th style="text-align:center">Frequency</th><th>Instructions</th><th style="text-align:center">Begin On</th><th style="text-align:center">Days</th><th>Symptoms</th><th>Prescribed By</th><th>Status</th></tr>';

				if (data.content.drEpisodeDetails.drPrescription.length > 0) {
					$.each(data.content.drEpisodeDetails.drPrescription, function (index, option) {
						// console.log(index," ",option);
						presList += '<tr><td>' + option.medicationName + '</td><td style="text-align:center">' + ((option.medicationType != "" && option.medicationType != null) ? option.medicationType : '-') + '</td><td style="text-align:center">' + option.frequency + '</td>';
						presList += '<td>' + option.instruction + '</td><td style="text-align:center">' + dayjs(option.beginDate).utc(true).format('DD/MM/YYYY') + '</td>';
						presList += '<td style="text-align:center">' + option.days + '</td>';
						presList += '<td>' + ((option.chiefComplaints != "" && option.chiefComplaints != null) ? option.chiefComplaints : ' ') + '</td>';
						presList += '<td>' + option.prescribedBy + '</td><td>' + (option.status === "active" ? "Active" : "Inactive") + '</td>';
						presList += "<td><button  class='pull-right btn-primary btn dcbutton' onclick=\'updateMedication(" + JSON.stringify(option) + ");\'>Update</button></td>";
						presList += '</tr>';
					})

					$("#prescriptionEmptyRow").hide();
					$("#prescriptionTableHeader").html(presHeader);
					$("#prescriptionTableBody").html(presList);
				}

				let adviceList, followUpList, prelimDiagList, investigateList, complaintList, historyList, clinicalExaminList = '';


                complaintHeader = '<tr><th width="30%";>Complaint</th><th >Date</th></tr>';
                historyHeader = '<tr><th width="30%";>History</th><th >Date</th></tr>';
                clinicalExaminHeader = '<tr><th width="30%";>Test Name</th><th >Observation</th></tr>';
				adviceHeader = '<tr><th width="30%";>Advice</th><th >Date</th></tr>';
				followUpHeader = '<tr><th width="50%">Revisit On/After</th><th width="50%" style="text-align:center">Created Date</th></tr>';
				prelimDiagHeader = '<tr><th width="50%">Diagnosis</th><th width="50%" style="text-align:center">Created Date</th></tr>';
				investigateHeader = '<tr><th width="50%">Investigation</th><th width="50%" style="text-align:center">Created Date</th></tr>';

				$.each(data.content.drEpisodeDetails.drAdvice, function (index, option) {

                    if (index === "chiefComplaints" && option != null) {
                        $("#addChiefComplaintButton").hide();
						complaintOptions = option;
						$.each(complaintOptions, function (index, option) {
							complaintList += '<tr><td width="30%";>' + option.complaints + '</td><td >' + dayjs(option.createdAt).utc(true).format('DD/MM/YYYY') + '</td>';
							complaintList += "<td><button class='pull-right btn-primary btn dcbutton' onclick=\'updateComplaint(" + JSON.stringify(option) + ");\'>Update</button></td>";
							complaintList += '</tr>';
						})

						$("#chiefComplaintEmptyRow").hide();
						$("#chiefComplaintTableHeader").html(complaintHeader);
						$("#chiefComplaintTableBody").html(complaintList);
					}
                    else if (index === "history" && option != null) {
                        if(option.length > 0){
                            $("#addHistoryButton1").hide();
                        }
						historyOptions = option;
						$.each(historyOptions, function (index, option) {
							historyList += '<tr><td width="60%"; style="overflow-wrap: break-word;">' + option.history + '</td><td >' + dayjs(option.createdAt).utc(true).format('DD/MM/YYYY') + '</td>';
							historyList += "<td><button class='pull-right btn-primary btn dcbutton' onclick=\'updateHistory(" + JSON.stringify(option) + ");\'>Update</button></td>";
							historyList += '</tr>';
						})

						$("#historyEmptyRow").hide();
						$("#historyTableHeader").html(historyHeader);
						$("#historyTableBody").html(historyList);
					}
                    else if (index === "clinicalExaminations" && option != null) {
                        if(option.length > 0){
                            $("#addHistoryButton2").hide();
                        }
						clinicalExaminOptions = option;
						$.each(clinicalExaminOptions, function (index, option) {
						clinicalExaminList += "<tr><td><button class=' btn-primary btn dcbutton text-left'  style='float: right  left: 300px;;  ' onclick=\'updateClinicalExamin(" + JSON.stringify(option) + ");\'>Update</button></td></tr>";
							clinicalExaminList += '<tr><td width="30%";>' + "RS" + '</td><td >' + option["RS"] + '</td>';
							clinicalExaminList += '<tr><td width="30%";>' + "GE" + '</td><td >' + option["GE"] + '</td>';
							clinicalExaminList += '<tr><td width="30%";>' + "P/A" + '</td><td >' + option["GUT"] + '</td>';
							clinicalExaminList += '<tr><td width="30%";>' + "GUT" + '</td><td >' + option["P/A"] + '</td>';
							clinicalExaminList += '<tr><td width="30%";>' + "CVS" + '</td><td >' + option["CVS"] + '</td>';
							clinicalExaminList += '<tr><td width="30%";>' + "CNS" + '</td><td >' + option["CNS"] + '</td>';
							clinicalExaminList += '<tr><td width="30%";>' + "Others" + '</td><td >' + option["Other"] + '</td>';

//							clinicalExaminList += '</tr>';
						})

						$("#clinicalExaminEmptyRow").hide();
						$("#clinicalExaminTableHeader").html(clinicalExaminHeader);
						$("#clinicalExaminTableBody").html(clinicalExaminList);
					}
					// console.log(index," ",option);
				    else if (index === "advice" && option != null) {
						adviceOptions = option;
						$.each(adviceOptions, function (index, option) {
							adviceList += '<tr><td width="30%";>' + option.advice + '</td><td >' + dayjs(option.createdAt).utc(true).format('DD/MM/YYYY') + '</td>';
							adviceList += "<td><button class='pull-right btn-primary btn dcbutton' onclick=\'updateAdvice(" + JSON.stringify(option) + ");\'>Update</button></td>";
							adviceList += '</tr>';
						})

						$("#adviseEmptyRow").hide();
						$("#adviseTableHeader").html(adviceHeader);
						$("#adviseTableBody").html(adviceList);
					}
					else if (index === "followUp" && option != null) {
						followUpOptions = option;
						if (followUpOptions.length > 0) {
							$("#addFollowUpButton").hide();
							$.each(followUpOptions, function (index, option) {
								//console.log("revisitDate ",option.revisitDate,"revisitAfter ",option.revisitAfter)
								if (index == 0) {
									console.log("revisitDate ", option.revisitDate, "Status: ", (option.revisitDate != undefined), dayjs(option.revisitDate).format('DD/MM/YYYY'))
									followUpList += '<tr><td width="50%">' + ((option.revisitDate != undefined) ? option.revisitDate : (option.revisitAfter > 1) ? option.revisitAfter + ' days' : option.revisitAfter + ' day') + '</td><td width="50%" style="text-align:center">' + dayjs(option.createdAt).utc(true).format('DD/MM/YYYY') + '</td>';
									followUpList += "<td><button class='pull-right btn-primary btn dcbutton' onclick=\'updateFollowUp(" + JSON.stringify(option) + ");\'>Update</button></td>";
									followUpList += '</tr>';
								}
							})
						}
						$("#followUpEmptyRow").hide();
						$("#followUpTableHeader").html(followUpHeader);
						$("#followUpTableBody").html(followUpList);
					}
					else if (index === "preliminaryDiagnosis" && option != null) {
						prelimDiagOptions = option;
						$("#addPrelimDiagButton").hide();
						if (prelimDiagOptions.length > 0) {
							$.each(prelimDiagOptions, function (index, option) {
								if (index == 0) {
									prelimDiagList += '<tr><td width="50%">' + option.diagnosis + '</td><td width="50%" style="text-align:center">' + dayjs(option.createdAt).utc(true).format('DD/MM/YYYY') + '</td>';
									prelimDiagList += "<td><button class='pull-right btn-primary btn dcbutton' onclick=\'updatePrelimDiag(" + JSON.stringify(option) + ");\'>Update</button></td>";
									prelimDiagList += '</tr>';
								}
							})
						}


						$("#notesEmptyRow").hide();
						$("#notesTableHeader").html(prelimDiagHeader);
						$("#notesTableBody").html(prelimDiagList);
					}
					else if (index === "investigation" && option != null) {
					investigationOptions = option;

					console.log("------INVEST----",option.length);
					if(option.length > 0){
					 $("#addInvestigationButton").hide();
                     $.each(investigationOptions, function (index, option) {
                     							investigateList += '<tr><td width="50%">' + option.investigate + '</td><td width="50%" style="text-align:center">' + dayjs(option.createdAt).utc(true).format('DD/MM/YYYY') + '</td>';
                     							investigateList += "<td><button class='pull-right btn-primary btn dcbutton' onclick=\'updateInvestigation(" + JSON.stringify(option) + ");\'>Update</button></td>";
                     							investigateList += '</tr>';
                     						})
                     }



						$("#reportsEmptyRow").hide();
						$("#reportsTableHeader").html(investigateHeader);
						$("#reportsTableBody").html(investigateList);
					}
					else if (index === "advicedDate" && option != null) {
						currentEpisodeDate = option;
					}
					else if (index === "id" && option != null) {
						drAdviceMongoId = option;
					}
				})
				 console.log(dayjs(new Date()).format('DD/MM/YYYY'));
				// console.log(dayjs(currentEpisodeDate).format("DD/MM/YYYY"));
				 console.log(currentEpisodeDate);
				if (dayjs(currentEpisodeDate).utc(true).format("DD/MM/YYYY") === dayjs(new Date()).utc(true).format('DD/MM/YYYY')) {
					$(".updateButton").show();
					$("#addPrescriptionButton").show();
					$("#addAdviceButton").show();
					//                    $("#addFollowUpButton").show();
					//                    $("#addPrelimDiagButton").show();
					//$("#addInvestigationButton").show();

				}
				// else if(listIndex === 0){
				//     $("#addPrescriptionButton").show();
				//     $("#addAdviceButton").show();
				//    $("#addFollowUpButton").show();
				//    $("#addPrelimDiagButton").show();
				//     $("#addInvestigationButton").show();
				// }
				else {
					$(".updateButton").hide();
                    $("#addChiefComplaintButton").hide();
                    $("#addHistoryButton1").hide();
                    $("#addHistoryButton2").hide();
					$("#addPrescriptionButton").hide();
					$("#addAdviceButton").hide();
					$("#addFollowUpButton").hide();
					$("#addPrelimDiagButton").hide();
					$("#addInvestigationButton").hide();
				}
			}
		},
		error: function (xhr, status, error) {
			if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
				if (xhr.responseJSON.code === 401) {
					(async () => {
						toastr.error(xhr.responseJSON.message);
						await sleep();
						window.location.href = './login';
					})()
				}
				//toastr.error(xhr.responseJSON.message);
			} else {
				toastr.error("No Data Found!");
			}
			$("#emrPdfBtn").hide();
		}
	})
}


function getDayDiff(startDate, endDate) {
	const msInDay = 24 * 60 * 60 * 1000;

	return Math.round(
		Math.abs(endDate - startDate) / msInDay
	);
}

function updateMedication(prescriptionRow) {
	console.log(prescriptionRow);

	$("#addMedicationModal").modal('show');

	$(".bClassM").parent().addClass('is-dirty');
	$('#fequencyId').val(prescriptionRow.frequency);
	$('#medicationType').val(prescriptionRow.medicationType);
	$('#instructionId').val(prescriptionRow.instruction);
	$('#medicationNameId').val(prescriptionRow.medicationName);
	$('#prescribedByPre').val(prescriptionRow.prescribedBy);
	$("#prescribedByPre").parent().addClass('is-dirty');
	$('#startId').datepicker("setDate", new Date(prescriptionRow.beginDate));
	$('#days').val(prescriptionRow.days);
	if (prescriptionRow.chiefComplaints != '') {
		$('#commentsMedId').val(prescriptionRow.chiefComplaints);
	}

	$("#fequencyId").attr("disabled", false);
	$("#instructionId").attr("disabled", false);
	$("#medicationType").attr("disabled", false);
	$("#medicationNameId").attr("disabled", false);
	$("#prescribedByPre").attr("disabled", true);
	$("#startId").attr("disabled", false);
	$("#days").attr("disabled", false);
	$("#commentsMedId").attr("disabled", false);


//	$('#prescriptionStatus').val(prescriptionRow.status);

	$('#saveMedicationButton').hide();
	$('#addMedicationButton').hide();
	$('#updateMedicationButton').show();
//	$('#statusSelector').show();


	$('#updateMedicationButton').unbind().click(function () {
//		 console.log("update pres row: ",prescriptionRow);
		// console.log(drEpisodeId);
						let medicationName = $('#medicationNameId').val();
                		let frequency = $('#fequencyId').val();
                		let medicinType = $('#medicationType').val();
                		let instruction = $('#instructionId').val();
                		//let medicationType = $('#medicationTypeId').val();
                		let startDate = $('#startId').val();
                		let days = $('#days').val();
                		let chiefComplaints;
                		if($('#commentsMedId').val() != ''){
                		    chiefComplaints = $('#commentsMedId').val();
                		}

//				let medicationName = self.find("td:eq(1)").text().trim();
//        		let frequency = self.find("td:eq(2)").text().trim();
//        		let instruction = self.find("td:eq(3)").text().trim();
//        		let startDate = self.find("td:eq(4)").text().trim();
//        		let days = self.find("td:eq(5)").text().trim();
//        		let chiefComplaints = self.find("td:eq(6)").text().trim();
        		let prescription = {
        			"medicationName": medicationName,
        			"medicationType": medicinType,
        			"frequency": frequency,
        			"instruction": instruction,
        			"startDate": dayjs(startDate, 'DD/MM/YYYY').utc(true).valueOf(),
        			"days": Number(days),
        			"chiefComplaints": chiefComplaints
        		}

                const prescriptionArray = [];
        		prescriptionArray.push(prescription);
        		console.log("update prescription: ",prescriptionArray);
		let data = JSON.stringify({
			"drEpisodeId": Number(doctorEpisodeId),
			"mongoId": prescriptionRow.id,
//			"status": $('#prescriptionStatus').val(),
            "prescriptionJson": prescriptionArray,
		});
		 console.log("update data: ",data);


		if (data != null && data != '') {
			$("#saveMedicationButton").attr("disabled", true);
			$("#updateMedicationButton").attr("disabled", true);

			$.ajax({
				type: "POST",
				url: serverPath + "/updateDrPrescription",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: data,
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {

						toastr.success('Prescription Updated Successfully', '');

						canMed();

						$("#saveMedicationButton").attr("disabled", false);
						$("#updateMedicationButton").attr("disabled", false);

						// getDrEpisodeList();
						// console.log(drEpisodeId);
//						location.reload();
//						getDrEpisodeDetails(doctorEpisodeId);
                        getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);

					}
				},
				error: function (xhr, status, error) {
					$("#saveMedicationButton").attr("disabled", false);
					$("#updateMedicationButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			});

		} else {

			$('#dynTableMed').hide();
		}

	})

}

function updateAdvice(adviceRow) {
	//    console.log(adviceRow);
	//    console.log("adviceOptions: ",adviceOptions);


	let newAdviceOptions = adviceOptions.filter(function (object) { return object.createdAt != adviceRow.createdAt });

	console.log("newAdviceOptions: ", newAdviceOptions);
	//        return;
	let advice = adviceRow.advice;
	let referredBy = adviceRow.referredBy;
	let createdAt = adviceRow.createdAt;

	openAdviceModal(true);

	$('#serviceTypeId').val(advice);
	$('#advComments').val(referredBy);
	$('.Newpolic2').parent().addClass('is-dirty');

	$('#saveAdviceButton').hide();
	$('#updateAdviceButton').show();

	$('#updateAdviceButton').unbind().click(function () {

		//        saveAdvice(true, newAdviceOptions);
		saveAdvice(true, adviceRow);
	})
}

function updateFollowUp(followUpRow) {
	console.log("update follow up : ",followUpRow, (followUpRow.revisitDate));
	// console.log(followUpOptions);

	let newFollowUpOptions = followUpOptions.filter(function (object) { return object.createdAt != followUpRow.createdAt });

    let revisit =0;

    if(followUpRow.revisitDate){
        var array = followUpRow.revisitDate.split("/");

        var create_epoch = ((new Date(followUpRow.createdAt)).getTime()) /1000;
        var revisit_epoch = ((new Date(array[1]+"/"+array[0]+"/"+array[2])).getTime()) /1000;
        var revis = revisit_epoch-create_epoch;
        var revDate = new Date(0)
        revDate.setUTCSeconds(revis);
//        console.log("rev date: ",revDate);
        revisit = revDate.getDate();
//        console.log("day dif : "+dt);
//        revisit = getDayDiff(new Date(followUpRow.createdAt), new Date(array[1]+"/"+array[0]+"/"+array[2]));
//        console.log("revisit follow up: ",revisit);
    }else{
        revisit = followUpRow.revisitAfter;
    }

	let createdAt = followUpRow.createdAt;
	let dateRegex = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([1][26]|[2468][048]|[3579][26])00))))$/g;


	openFollowUpModal(true);

//	$('#saveFollowUpButton').hide();
//	$('#updateFollowUpButton').show();

		$('#followUpAfterTextId').val(revisit);
//		var today1 = new Date();
//		today1.setDate(revisit);
//		$('#followUpAfterId').datepicker("setDate", revisit);

//        $('#followUpAfterId').datepicker("setDate", new Date(dayjs(revisit, 'DD/MM/YYYY').utc(false)));
        var revDt = new Date(followUpRow.createdAt);
        revDt.setDate(revDt.getDate() + revisit);
//        $('#followUpAfterId').val(revDt);
        console.log("revisit after: ",revDt);
        const yyyy = revDt.getFullYear();
        let mm = revDt.getMonth() + 1; // Months start at 0!
        let dd = revDt.getDate();

        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        document.getElementById("followUpAfterId").value = yyyy + '-' + mm + '-' + dd;

//	if (!dateRegex.test(revisit)) {
//		$('#followUpAfterTextId').val(revisit);
////		$('#followUpAfterId').val('');
////        var array = followUpRow.revisitDate.split("/");
//        $('#followUpAfterId').datepicker("setDate", new Date(dayjs(revisit, 'DD/MM/YYYY').utc(true)));
//	}
//	else {
//		// $('#followUpAfterId').val(revisit);
//		$('#followUpAfterId').datepicker("setDate", new Date(dayjs(revisit, 'DD/MM/YYYY').utc(true)));
//	}
	$('.Newpolic1').parent().addClass('is-dirty');

	$('#updateFollowUpButton').unbind().click(function () {
		saveFollowUp(true, newFollowUpOptions);
	})
}

function updatePrelimDiag(prelimDiagRow) {
	// console.log(prelimDiagRow);

	let newPrelimDiagOptions = prelimDiagOptions.filter(function (object) { return object.createdAt != prelimDiagRow.createdAt });

	let diagnosis = prelimDiagRow.diagnosis;
	let createdAt = prelimDiagRow.createdAt;

//	$('#savePrelimDiagButton').hide();
//	$('#updatePrelimDiagButton').show();

	openNotesModal(true);

	$('#noeComments').val(diagnosis);

	$('#updatePrelimDiagButton').unbind().click(function () {
		savePrelimDiagnostics(true, newPrelimDiagOptions);
	})

}

function updateInvestigation(investigationRow) {
	// console.log(investigationRow);

	let newInvestigationRow = investigationOptions.filter(function (object) { return object.createdAt != investigationRow.createdAt });

	let investigation = investigationRow.investigate;
	let createdAt = investigationRow.createdAt;

//	$('#saveInvestigationButton').hide();
//	$('#updateInvestigationButton').show();

	openReportModal(true);

	$('#investigationTextId').val(investigation);
	$('.Newpolic1').parent().addClass('is-dirty');

	$('#updateInvestigationButton').unbind().click(function () {
		saveInvestigation(true, investigationRow);
	})

}

let doctorName = localStorage.getItem('loggedInUserFullName');
function addMedicationNew() {

	let valName = $("#medicationNameId").val();
	let medicationId = $("#medicationIdFromDropdown").val();
	let valDosage = $("#dosageID").val();

	if (valName != '') {
		if (medicationId == null || medicationId == "") {

			$.ajax({
				type: "POST",
				url: "addMedication?medicationName=" + valName,
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				success: function (data) {
					if (data.code == "200") {
						medicationId = data.content.id;
						// console.log(medicationId);
					}
				},
				error: function (xhr, status, error) {
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
//								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
//						toastr.error("No Data Found!");
					}
				}
			});

		}
	}

	let valFreq = $("#fequencyId").val();
	let valInst = $("#instructionId").val();
	let valStart = $("#startId").val();
	let valDays = $("#days").val();
	let valComm = $("#commentsMedId").val();
	let medicationType = $("#medicationType").val();

	if (valDays.length > 3) {
		alert("Please enter valid days");
		return;
	}

	if (valName.replace(/ /g, '') != '' && valStart.replace(/ /g, '') != '' && valDays.replace(/ /g, '') != '' && doctorName.replace(/ /g, '') != '') {

		$(".addedMedTable").show();

		let medData = '<tr><td class="getJson" style="display:none" attr="medicationId">' + medicationId + '</td><td class="getJson" attr="medicationName">' + valName + '</td>'
			+'<td class="getJson" attr="MedicineTYpe">'+medicationType+'</td>'
			+ '<td class="getJson" attr="frequency">' + valFreq + '</td>'
			+ ' <td class="getJson" attr="instructions">' + valInst + '</td><td class="getJson" attr="beginDate">' + valStart + '</td>'
			+ '<td class="getJson" attr="Days">' + valDays + '</td><td class="getJson" attr="comment">' + valComm + '</td> '
			+ '<td><i style="cursor:pointer" class="fa fa-times" aria-hidden="true" onclick="removeFileMed(this)"></i></td></tr>';

		$("#medAdd").append(medData);
		$('.Newpolic1').val('');
		$('.Newpolic1').parent().removeClass('is-dirty');
		$('#fequencyId').val('0-0-1');
		$("#medicationType").val('Tablet');
		$('#instructionId').val('Before Food');
		$('#saveMedicationButton').show();

	} else {
		toastr.warning('Please fill mandatory fields', '', { timeOut: 3000 });
		$("#saveMedicationButton").attr("disabled", true);
		$("#updateMedicationButton").attr("disabled", true);
		$("#addMedicationButton").attr("disabled", true);

		setTimeout((function () {
			$("#saveMedicationButton").attr("disabled", false);
			$("#updateMedicationButton").attr("disabled", false);
			$("#addMedicationButton").attr("disabled", false);
		}), 2000);

		if ($('#medAdd').html() == "") {
			$('#saveMedicationButton').hide();
		}
	}

}

 function removeFileMed(ob) {

        $(ob).parent().parent().remove();

        if ($("#medAdd tr").length < 1) {
            $('#saveMedicationButton').hide();
            $('#dynTableMed').hide();
        }
        if ($("#invAdd tr").length < 1) {
            $('#dynTableInv').hide();
        }
    }

function saveMedicationNew() {

    $('#updateMedicationButton').hide();

//	$('#saveMedicationButton').show();
//	$('#addMedicationButton').show();
//	$('#updateMedicationButton').hide();

	const prescriptionArray = [];

	$('#medAdd tr').each(function () {
		let self = $(this);
		// var col_1_value = self.find("td:eq(0)").text().trim();
		let medicationName = self.find("td:eq(1)").text().trim();
		let medicinType = self.find("td:eq(2)").text().trim();
		let frequency = self.find("td:eq(3)").text().trim();
		let instruction = self.find("td:eq(4)").text().trim();
		let startDate = self.find("td:eq(5)").text().trim();
		let days = self.find("td:eq(6)").text().trim();
		let chiefComplaints = self.find("td:eq(7)").text().trim();
		let prescription = {
			"medicationName": medicationName,
			"medicationType": medicinType,
			"frequency": frequency,
			"instruction": instruction,
			"startDate": dayjs(startDate, 'DD/MM/YYYY').utc(true).valueOf(),
			"days": Number(days),
			"chiefComplaints": chiefComplaints
		}
		prescriptionArray.push(prescription);
		// var result = col_1_value + " - " + col_2_value + " - " + col_3_value + " - " + col_4_value + " - " + col_5_value + " - " + col_6_value + " - " + col_7_value;
		// console.log(result);
	});
	// console.log(prescriptionArray);
//	console.log("episodeId: ",SELECTED_EPI_ID);
	let data = JSON.stringify({
		"clientId": Number(localStorage.getItem("clientId")),
		"patientUserId": Number(userId1),
		"doctorName": doctorName,
		"prescriptionJson": prescriptionArray,
		"episodeId": SELECTED_EPI_ID
	});
console.log(data);


	if (data != null && data != '') {
		$("#saveMedicationButton").attr("disabled", true);
		$("#updateMedicationButton").attr("disabled", true);
		$.ajax({
			type: "POST",
			url: serverPath + "/addDrPrescription",
			headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
			data: data,
			contentType: "application/json",
			success: function (data) {
				if (data.code = "200") {
					let savedDrEpisode = data.content.drEpisodeId;
					toastr.success('Prescription Saved Successfully', '');
					canMed();
					$("#saveMedicationButton").attr("disabled", false);
					$("#updateMedicationButton").attr("disabled", false);
					console.log("Episode id: ",SELECTED_EPI_ID , " Dr episode id: ",doctorEpisodeId);
					//getDrEpisodeList();
					if (!doctorEpisodeId) {
					    $("#" + SELECTED_EPI_BTN_INDEX).attr('onclick', '');
//					    $(document).find("#" + SELECTED_EPI_BTN_INDEX).attr({"onclick": patEpisodeChange(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX, false, savedDrEpisode)
//                        });
						$("#" + SELECTED_EPI_BTN_INDEX).attr("onclick", patEpisodeChange(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX, false, savedDrEpisode));
					}
					doctorEpisodeId = savedDrEpisode;
					getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
				}
			},
			error: function (xhr, status, error) {
				$("#saveMedicationButton").attr("disabled", true);
				$("#updateMedicationButton").attr("disabled", false);
				if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
					if (xhr.responseJSON.code === 401) {
						(async () => {
							toastr.error(xhr.responseJSON.message);
							await sleep();
							window.location.href = './login';
						})()
					}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
					//toastr.error(xhr.responseJSON.message);
				} else {
					toastr.error("No Data Found!");
				}
			}

		});


	} else {

		$('#dynTableMed').hide();
		toastr.error("Please Add the mandatory data");
	}

}

// serviceTypeId,advComments

function saveAdvice(update = false, newAdviceOptions) {
	let adviceText = $("#serviceTypeId").val();
	let adviceReferred = $("#advComments").val();

	//    console.log("row",newAdviceOptions, drEpisodeId, drAdviceMongoId);
	//    console.log("advicetxt",adviceText);
	//    console.log("referred",adviceReferred);
	const adviceArray = [];


	let createdAt;
	if (update === false) {
		createdAt = dayjs(new Date()).utc(true).valueOf();
	}
	else {
		createdAt = 'createdAt' in newAdviceOptions && newAdviceOptions.createdAt ? newAdviceOptions.createdAt : dayjs(new Date()).utc(true).valueOf();
	}

	let adviceJson = {
		"createdAt": createdAt,
		"advice": adviceText,
		"referredBy": adviceReferred
	}

	adviceArray.push(adviceJson);
    console.log("add advice episodeId: ",SELECTED_EPI_ID);
	let data = {
		"clientId": Number(localStorage.getItem("clientId")),
		"patientUserId": Number(userId1),
		"doctorName": doctorName,
		"adviceJson": adviceArray,
		"episodeId": SELECTED_EPI_ID
	};

	if (update === true) {
		Object.assign(data, {
			"drEpisodeId": Number(doctorEpisodeId),
			"drAdviceId": drAdviceMongoId
		});
	}

	// console.log(data);
	if (adviceText.replace(/ /g, '') != "") {
		//$("#saveAdviceButton").attr("disabled", true);
		//$("#updateAdviceButton").attr("disabled", true);
		if (update === false) {
			$.ajax({
				type: "POST",
				url: serverPath + "/addDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {
						let savedDrEpisode = data.content.drEpisodeId;

						$("#saveAdviceButton").attr("disabled", false);
						$("#updateAdviceButton").attr("disabled", false);

						toastr.success('Advice Saved Successfully', '');
						// getDrEpisodeList();
						console.log('-------ADVIC:SAVE-SUC:-',doctorEpisodeId);
						if (!doctorEpisodeId) {
						    $("#" + SELECTED_EPI_BTN_INDEX).attr('onclick', '');
							$("#" + SELECTED_EPI_BTN_INDEX).attr("onclick", patEpisodeChange(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX, false, savedDrEpisode));
						}
						doctorEpisodeId = savedDrEpisode;
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
						clearAdvModal();
					}
				},
				error: function (xhr, status, error) {
					$("#saveAdviceButton").attr("disabled", false);
					$("#updateAdviceButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		} else {
			$.ajax({
				type: "POST",
				url: serverPath + "/updateDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {

						clearAdvModal();

						$("#saveAdviceButton").attr("disabled", false);
						$("#updateAdviceButton").attr("disabled", false);

						toastr.success('Advice Updated Successfully', '');
//						location.reload();

						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
					}
//					else{
//					console.log("error data: ",data);
//					    toastr.error(data.message);
//					}
				},
				error: function (xhr, status, error) {
//				console.log("error data: ",xhr.responseJSON.message);
					$("#saveAdviceButton").attr("disabled", false);
					$("#updateAdviceButton").attr("disabled", false);

					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}else{
						    toastr.error(xhr.responseJSON.message);
						}

					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		}

	} else {
		toastr.warning('Please fill mandatory fields', '', { timeOut: 3000 });
		$("#saveFollowUpButton").attr("disabled", true);
		$("#updateFollowUpButton").attr("disabled", true);

		setTimeout((function () {
			$("#saveFollowUpButton").attr("disabled", false);
			$("#updateFollowUpButton").attr("disabled", false);
		}), 2000);
	}
}

function saveFollowUp(update = false, newFollowUpOptions) {
	let followUpText = $("#followUpAfterTextId").val();
	let followUpDate = $("#followUpAfterId").val();
	console.log("followupText : ",followUpText);
	console.log("followUpDate : ",followUpDate);

	if (followUpText != '' && followUpDate != '') {
		toastr.error("Enter only one field");
		$("#followUpAfterTextId").val('');
		$("#followUpAfterId").val('');
		return;
	}

	const followUpArray = [];

	let createdAt = dayjs(new Date()).utc(true).valueOf();

	let followUpJson = { "createdAt": createdAt }

	if (followUpText) {
		Object.assign(followUpJson,
			{
				"revisitAfter": Number(followUpText)
			}
		)
	}
	else {
	console.log(followUpDate, "  revisit date : ",dayjs(followUpDate).utc(true).format("DD/MM/YYYY"));
		Object.assign(followUpJson,
			{
				"revisitDate": followUpDate ? dayjs(followUpDate).utc(true).format("DD/MM/YYYY") : '' //dayjs().add(Number(followUpText),'day').valueOf()
			}
		)
	}

	followUpArray.push(followUpJson);

	if (newFollowUpOptions !== undefined) {
		if (newFollowUpOptions.length > 0 && update === true) {
			for (let row of newFollowUpOptions) {
				followUpArray.push(row);
				// console.log("row",row);
			}
		}
	}

	let data = {
		"clientId": Number(localStorage.getItem("clientId")),
		"patientUserId": Number(userId1),
		"doctorName": doctorName,
		"followUpJson": followUpArray,
		"episodeId": SELECTED_EPI_ID
	};

	if (update === true) {
		Object.assign(data, {
			"drEpisodeId": Number(doctorEpisodeId),
			"drAdviceId": drAdviceMongoId
		});
	}

	// console.log(data);
	if ((followUpText.replace(/ /g, '') != "" || followUpDate != "") && !isNaN(followUpText)) {
		$("#saveFollowUpButton").attr("disabled", true);
		$("#updateFollowUpButton").attr("disabled", true);

		if (update === false) {
			$.ajax({
				type: "POST",
				url: serverPath + "/addDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {
						let savedDrEpisode = data.content.drEpisodeId;
						$("#saveFollowUpButton").attr("disabled", false);
						$("#updateFollowUpButton").attr("disabled", false);
						toastr.success('Follow Up Saved Successfully', '');
						//getDrEpisodeList();
						if (!doctorEpisodeId) {
						    $("#" + SELECTED_EPI_BTN_INDEX).attr('onclick', '');
							$("#" + SELECTED_EPI_BTN_INDEX).attr("onclick", patEpisodeChange(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX, false, savedDrEpisode));
						}
						doctorEpisodeId = savedDrEpisode;
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
						closeFollowUpModal();
					}
				},
				error: function (xhr, status, error) {
					$("#saveFollowUpButton").attr("disabled", false);
					$("#updateFollowUpButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		}
		else {
			$.ajax({
				type: "POST",
				url: serverPath + "/updateDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {
						closeFollowUpModal();
						$("#saveFollowUpButton").attr("disabled", false);
						$("#updateFollowUpButton").attr("disabled", false);
						toastr.success('Follow Up Updated Successfully', '');
//						location.reload();
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
					}
				},
				error: function (xhr, status, error) {
					$("#saveFollowUpButton").attr("disabled", false);
					$("#updateFollowUpButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		}

	} else {

		if (isNaN(followUpText)) {
			toastr.warning('Please enter number of days as a Number', '', { timeOut: 3000 });
			$("#saveFollowUpButton").attr("disabled", true);
			$("#updateFollowUpButton").attr("disabled", true);

			setTimeout((function () {
				$("#saveFollowUpButton").attr("disabled", false);
				$("#updateFollowUpButton").attr("disabled", false);
			}), 2000);
		}
		else {
			toastr.warning('Please fill mandatory fields', '', { timeOut: 3000 });
			$("#saveFollowUpButton").attr("disabled", true);
			$("#updateFollowUpButton").attr("disabled", true);

			setTimeout((function () {
				$("#saveFollowUpButton").attr("disabled", false);
				$("#updateFollowUpButton").attr("disabled", false);
			}), 2000);
		}
	}
}


function savePrelimDiagnostics(update = false, newPrelimDiagOptions) {
	let prelimDiagText = $("#noeComments").val();

	if (prelimDiagText == '') {
		toastr.warning('Please enter preliminary diagnosis', '', { timeOut: 3000 });
		$("#savePrelimDiagButton").attr("disabled", true);
		$("#updatePrelimDiagButton").attr("disabled", true);

		setTimeout((function () {
			$("#savePrelimDiagButton").attr("disabled", false);
			$("#updatePrelimDiagButton").attr("disabled", false);
		}), 1000);
		return;
	}

	const prelimArray = [];

	let createdAt = dayjs(new Date()).utc(true).valueOf();

	let prelimJson = {
		"createdAt": createdAt,
		"diagnosis": prelimDiagText,
	}

	prelimArray.push(prelimJson);

	if (newPrelimDiagOptions !== undefined) {
		if (newPrelimDiagOptions.length > 0 && update === true) {
			for (let row of newPrelimDiagOptions) {
				prelimArray.push(row);
				// console.log("row",row);
			}
		}
	}


	let data = {
		"clientId": Number(localStorage.getItem("clientId")),
		"patientUserId": Number(userId1),
		"doctorName": doctorName,
		"diagnosisJson": prelimArray,
		"episodeId": SELECTED_EPI_ID
	};

	if (update === true) {
		Object.assign(data, {
			"drEpisodeId": Number(doctorEpisodeId),
			"drAdviceId": drAdviceMongoId
		});
	}

	// console.log(data);
	if (prelimDiagText.replace(/ /g, '') != "") {
		$("#savePrelimDiagButton").attr("disabled", true);
		$("#updatePrelimDiagButton").attr("disabled", true);
		if (update === false) {
			$.ajax({
				type: "POST",
				url: serverPath + "/addDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {
						let savedDrEpisode = data.content.drEpisodeId;
						$("#noeComments").val('');
						$("#savePrelimDiagButton").attr("disabled", false);
						$("#updatePrelimDiagButton").attr("disabled", false);
						toastr.success('Diagnosis Saved Successfully', '');
						if (!doctorEpisodeId) {
						    $("#" + SELECTED_EPI_BTN_INDEX).attr('onclick', '');
							$("#" + SELECTED_EPI_BTN_INDEX).attr("onclick", patEpisodeChange(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX, false, savedDrEpisode));
						}
						doctorEpisodeId = savedDrEpisode;
						// getDrEpisodeList();
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
						canNote();
					}
				},
				error: function (xhr, status, error) {
					$("#savePrelimDiagButton").attr("disabled", false);
					$("#updatePrelimDiagButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		}
		else {
			$.ajax({
				type: "POST",
				url: serverPath + "/updateDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {
						$("#noeComments").val('');
						canNote();
						$("#savePrelimDiagButton").attr("disabled", false);
						$("#updatePrelimDiagButton").attr("disabled", false);
						toastr.success('Diagnosis Updated Successfully', '');
//						location.reload();
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
					}
				},
				error: function (xhr, status, error) {
					$("#savePrelimDiagButton").attr("disabled", false);
					$("#updatePrelimDiagButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		}

	}
}

function saveInvestigation(update = false, newInvestigationOptions) {
	let investigationText = $("#investigationTextId").val();


	const investigationArray = [];

	//    let createdAt = dayjs(new Date()).utc(true).valueOf();

	let createdAt;
	if (update === false) {
		createdAt = dayjs(new Date()).utc(true).valueOf();
	}
	else {
		createdAt = 'createdAt' in newInvestigationOptions && newInvestigationOptions.createdAt ? newInvestigationOptions.createdAt : dayjs(new Date()).utc(true).valueOf();
	}

	let investigateJson = {
		"createdAt": createdAt,
		"investigate": investigationText,
	}

	investigationArray.push(investigateJson);

	let data = {
		"clientId": Number(localStorage.getItem("clientId")),
		"patientUserId": Number(userId1),
		"doctorName": doctorName,
		"investigationJson": investigationArray,
		"episodeId": SELECTED_EPI_ID
	};

	if (update === true) {
		Object.assign(data, {
			"drEpisodeId": Number(doctorEpisodeId),
			"drAdviceId": drAdviceMongoId,
		});
	}

	console.log('----INV-SAVE---',data)
	// console.log(data);
	if (investigationText.replace(/ /g, '') != "") {
		$("#saveInvestigationButton").attr("disabled", true);
		$("#updateInvestigationButton").attr("disabled", true);
		if (update === false) {
			$.ajax({
				type: "POST",
				url: serverPath + "/addDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {
						let savedDrEpisode = data.content.drEpisodeId;
						toastr.success('Investigation Saved Successfully', '');
						// getDrEpisodeList(drEpisodeId);
						$("#saveInvestigationButton").attr("disabled", false);
						$("#updateInvestigationButton").attr("disabled", false);
						if (!doctorEpisodeId) {
						    $("#" + SELECTED_EPI_BTN_INDEX).attr('onclick', '');
							$("#" + SELECTED_EPI_BTN_INDEX).attr("onclick", patEpisodeChange(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX, false, savedDrEpisode));
						}
						doctorEpisodeId = savedDrEpisode;
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
						clearRep();
					}
				},
				error: function (xhr, status, error) {
					$("#saveInvestigationButton").attr("disabled", false);
					$("#updateInvestigationButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		}
		else {
			$.ajax({
				type: "POST",
				url: serverPath + "/updateDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {
						clearRep();
						$("#saveInvestigationButton").attr("disabled", false);
						$("#updateInvestigationButton").attr("disabled", false);

						toastr.success('Investigation Updated Successfully', '');
//						location.reload();

						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
					}
				},
				error: function (xhr, status, error) {
					$("#saveInvestigationButton").attr("disabled", false);
					$("#updateInvestigationButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		}
	} else {
		toastr.warning('Please enter investigation details', '', { timeOut: 3000 });
		$("#saveInvestigationButton").attr("disabled", true);
		$("#updateInvestigationButton").attr("disabled", true);

		setTimeout((function () {
			$("#saveInvestigationButton").attr("disabled", false);
			$("#updateInvestigationButton").attr("disabled", false);
		}), 2000);
	}
}

$(document).ready(function () {
	$('#vitalsTab').unbind().click(function () {
		if (!$('#vitalsTab').hasClass('active')) {
			refreshVitalsPage();
		}
	})
})







//ADD HISTORY API INTEGRATION

function addHistoryNew(update = false, newAdviceOptions) {
	let historyText = $("#formHistory").val();
	if ($("#formHistory").val() == '') {
	toastr.warning("History field is empty");
	return;
	}
	if (historyText.length < 2) {
	toastr.warning("Text fields contains atleast 2 characters!");
	return;
	}
	//let adviceReferred = $("#advComments").val();

	//    console.log("row",newAdviceOptions, drEpisodeId, drAdviceMongoId);
	//    console.log("advicetxt",adviceText);
	//    console.log("referred",adviceReferred);
	const historyArray = [];


	let createdAt;
	if (update === false) {
		createdAt = dayjs(new Date()).utc(true).valueOf();
	}
	else {
		createdAt = 'createdAt' in newAdviceOptions && newAdviceOptions.createdAt ? newAdviceOptions.createdAt : dayjs(new Date()).utc(true).valueOf();
	}

	let historyJson = {
		"createdAt": createdAt,
		"history": historyText

	}

	historyArray.push(historyJson);
    console.log("add history episodeId: ",SELECTED_EPI_ID);
	let data = {
		"clientId": Number(localStorage.getItem("clientId")),
		"patientUserId": Number(userId1),
		"doctorName": doctorName,
		"historyJson": historyArray,
		"episodeId": SELECTED_EPI_ID
	};

	if (update === true) {
		Object.assign(data, {
			"drEpisodeId": Number(doctorEpisodeId),
			"drAdviceId": drAdviceMongoId
		});
	}
	console.log("Pat History: ",data);


	// console.log(data);
	if (historyText.replace(/ /g, '') != "") {
		$("#saveHistoryButton").attr("disabled", true);
		$("#updateHistoryButton").attr("disabled", true);
		if (update === false) {
			$.ajax({
				type: "POST",
				url: serverPath + "/addDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {
						let savedDrEpisode = data.content.drEpisodeId;

						$("#saveHistoryButton").attr("disabled", false);
						$("#updateHistoryButton").attr("disabled", false);

						toastr.success('History Saved Successfully');
						canHistory();
                        $('#addHistoryButton').hide();
						// getDrEpisodeList();
						console.log('-------ADVIC:SAVE-SUC:-',doctorEpisodeId);
						if (!doctorEpisodeId) {
						    $("#" + SELECTED_EPI_BTN_INDEX).attr('onclick', '');
							$("#" + SELECTED_EPI_BTN_INDEX).attr("onclick", patEpisodeChange(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX, false, savedDrEpisode));
						}
						doctorEpisodeId = savedDrEpisode;
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
						canHistory();
					}
				},
				error: function (xhr, status, error) {
					$("#saveHistoryButton").attr("disabled", false);
					$("#updateHistoryButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		} else {
			$.ajax({
				type: "POST",
				url: serverPath + "/updateDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {

						clearAdvModal();

						$("#saveHistoryButton").attr("disabled", false);
						$("#updateHistoryButton").attr("disabled", false);

						toastr.success('History Updated Successfully', '');
//						location.reload();
                        canHistory();
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
					}
//					else{
//					console.log("error data: ",data);
//					    toastr.error(data.message);
//					}
				},
				error: function (xhr, status, error) {
//				console.log("error data: ",xhr.responseJSON.message);
					$("#saveHistoryButton").attr("disabled", false);
					$("#updateHistoryButton").attr("disabled", false);

					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}else{
						    toastr.error(xhr.responseJSON.message);
						}

					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		}

	} else {
		toastr.warning('Please fill mandatory fields', '', { timeOut: 3000 });
		$("#saveFollowUpButton").attr("disabled", true);
		$("#updateFollowUpButton").attr("disabled", true);

		setTimeout((function () {
			$("#saveFollowUpButton").attr("disabled", false);
			$("#updateFollowUpButton").attr("disabled", false);
		}), 2000);
	}
}


//UPDATE HISTORY API INTEGRATION

function updateHistory(historyRow) {
	  console.log("Update history modal",historyRow);
	//    console.log("adviceOptions: ",adviceOptions);


	let newhistoryOptions = historyOptions.filter(function (object) { return object.createdAt != historyRow.createdAt });

	//console.log("newAdviceOptions: ", newAdviceOptions);
	//        return;
	let history = historyRow.history;

	let createdAt = historyRow.createdAt;

	openHistoryModal(true);

	$('#formHistory').val(history);
	//$('#advComments').val(referredBy);
	$('.Newpolic2').parent().addClass('is-dirty');

	$('#saveHistoryButton').hide();
	$('#updateHistoryButton').show();

	$('#updateHistoryButton').unbind().click(function () {

		//        saveAdvice(true, newAdviceOptions);
		addHistoryNew(true, historyRow);
	})
}




//ADD CHIEF COMPLAINTS API INTEGRATION

function addComplaintNew(update = false, newcomplaintOptions) {
console.log("chief complaint modal-----------",newcomplaintOptions);
	let complaintText = $("#complaintId").val();
	if ($("#complaintId").val() == ""){
	toastr.error("Complaint field is empty");
	return;
	}else
	if(complaintText.length < 2){
	toastr.error("Complaint Field Contains atleast 2 characters");
    return;
	}
	//let adviceReferred = $("#advComments").val();

	//    console.log("row",newAdviceOptions, drEpisodeId, drAdviceMongoId);
	//    console.log("advicetxt",adviceText);
	//    console.log("referred",adviceReferred);
	const complaintArray = [];


	let createdAt;
	if (update === false) {
		createdAt = dayjs(new Date()).utc(true).valueOf();
	}
	else {
		createdAt = 'createdAt' in newcomplaintOptions && newcomplaintOptions.createdAt ? newcomplaintOptions.createdAt : dayjs(new Date()).utc(true).valueOf();
	}

	let chiefComplaintsJson = {
		"createdAt": createdAt,
		"complaints": complaintText

	}

	complaintArray.push(chiefComplaintsJson);
    console.log("add complaint episodeId: ",SELECTED_EPI_ID);
	let data = {
		"clientId": Number(localStorage.getItem("clientId")),
		"patientUserId": Number(userId1),
		"doctorName": doctorName,
		"chiefComplaintsJson": complaintArray,
		"episodeId": SELECTED_EPI_ID
	};

	if (update === true) {
		Object.assign(data, {
			"drEpisodeId": Number(doctorEpisodeId),
			"drAdviceId": drAdviceMongoId
		});
	}

	 console.log(data);

	if (complaintText.replace(/ /g, '') != "") {
		$("#saveComplaintButton").attr("disabled", true);
		$("#updateComplaintButton").attr("disabled", true);
		if (update === false) {
			$.ajax({
				type: "POST",
				url: serverPath + "/addDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {
						let savedDrEpisode = data.content.drEpisodeId;

						$("#saveComplaintButton").attr("disabled", false);
						$("#updateComplaintButton").attr("disabled", false);

						toastr.success('Compliant Saved Successfully', '');
                        $("#chief-complaint-Modal").modal('hide');
                        //$('#addChiefComplaintButton').hide();
						// getDrEpisodeList();
						console.log('-------ADVIC:SAVE-SUC:-',doctorEpisodeId);
						if (!doctorEpisodeId) {
						    $("#" + SELECTED_EPI_BTN_INDEX).attr('onclick', '');
							$("#" + SELECTED_EPI_BTN_INDEX).attr("onclick", patEpisodeChange(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX, false, savedDrEpisode));
						}
						doctorEpisodeId = savedDrEpisode;
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
						clearAdvModal();
					}
				},
				error: function (xhr, status, error) {
					$("#saveComplaintButton").attr("disabled", false);
					$("#updateComplaintButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		} else {
			$.ajax({
				type: "POST",
				url: serverPath + "/updateDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {

						//clearAdvModal();

						$("#saveComplaintButton").attr("disabled", false);
						$("#updateComplaintButton").attr("disabled", false);

						toastr.success('Complaint Updated Successfully', '');
						$("#chief-complaint-Modal").modal('hide');
                        //$('#addChiefComplaintButton').hide();
//						location.reload();

						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
					}
//					else{
//					console.log("error data: ",data);
//					    toastr.error(data.message);
//					}
				},
				error: function (xhr, status, error) {
//				console.log("error data: ",xhr.responseJSON.message);
					$("#saveComplaintButton").attr("disabled", false);
					$("#updateComplaintButton").attr("disabled", false);

					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}else{
						    toastr.error(xhr.responseJSON.message);
						}

					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		}

	} else {
		toastr.warning('Please fill mandatory fields', '', { timeOut: 3000 });
		$("#saveFollowUpButton").attr("disabled", true);
		$("#updateFollowUpButton").attr("disabled", true);

		setTimeout((function () {
			$("#saveFollowUpButton").attr("disabled", false);
			$("#updateFollowUpButton").attr("disabled", false);
		}), 2000);
	}
}


//UPDATE CHIEF COMPLAINTS

function updateComplaint(complaintRow) {
	//    console.log(adviceRow);
	//    console.log("adviceOptions: ",adviceOptions);


	let newComplaintOptions = complaintOptions.filter(function (object) { return object.createdAt != complaintRow.createdAt });

	console.log("newComplaintOptions: ", newComplaintOptions);
	//        return;
	let complaint = complaintRow.complaints;

	let createdAt = complaintRow.createdAt;

	openChiefComplaintModal(true);

	$('#complaintId').val(complaint);
	//$('#advComments').val(referredBy);
	$('.Newpolic2').parent().addClass('is-dirty');

	$('#saveComplaintButton').hide();
	$('#updateComplaintButton').show();

	$('#updateComplaintButton').unbind().click(function () {

		//        saveAdvice(true, newAdviceOptions);
		addComplaintNew(true, complaintRow);
	})
}

//  ADD CLINICAL EXAMINATION
function addClinicalExaminNew(update = false, newClinicalExaminOptions) {
	var upRSval = '', upGEval = '',upPAval = '', upGUTval = '', upCVSval = '', upCNSval = '', otherval = '';


var rstextField = $("#rsExamineText").val();
var geTextField = $("#geExamineText").val();
var paTextField = $("#PAExamineText").val();
var gutTextField = $("#gutExamineText").val();
var cvsTextField = $("#cvsExamineText").val();
var cnsTextField = $("#cnsExamineText").val();
var otherFindText = $("#otherFindings").val();

//FIELD VALIDATION FOR CLINICAL EXAMINATION
if($("#rsExamine").val() == "Other Findings" && $("#rsExamineText").val() == "" ){
	toastr.warning("Please fill Other Findings enabled text field!");
	return;
}else if($("#rsExamine").val() == "Other Findings" && rstextField.length < 2 ) {
	toastr.warning  ("Text field contain atleast 2 characters!");
	return;
}
 if($("#geExamine").val() == "Other Findings" && $("#geExamineText").val() == "" ){
		toastr.warning("Please fill Other Findings enabled text field!");
		return;
	}else if($("#geExamine").val() == "Other Findings" && geTextField.length < 2 ) {
		toastr.warning  ("Text field contain atleast 2 characters!");
		return;
	}
if($("#paExamine").val() == "Other Findings" && $("#PAExamineText").val() == "" ){
	toastr.warning("Please fill Other Findings enabled text field!");
	return;
}else if($("#paExamine").val() == "Other Findings" && paTextField.length < 2 ) {
	toastr.warning  ("Text field contain atleast 2 characters!");
	return;
}
if($("#gutExamine").val() == "Other Findings" && $("#gutExamineText").val() == "" ){
	toastr.warning("Please fill Other Findings enabled text field!");
	return;
}else if($("#gutExamine").val() == "Other Findings" && gutTextField.length < 2 ) {
	toastr.warning  ("Text field contain atleast 2 characters!");
	return;
}
 if($("#cvsExamine").val() == "Other Findings" && $("#cvsExamineText").val() == "" ){
		toastr.warning("Please fill Other Findings enabled text field!");
		return;
	}else if($("#cvsExamine").val() == "Other Findings" && cvsTextField.length < 2 ) {
		toastr.warning  ("Text field contain atleast 2 characters!");
		return;
 }
if($("#cnsExamine").val() == "Other Findings" && $("#cnsExamineText").val() == "" ){
	 toastr.warning("Please fill Other Findings enabled text field!");
	 return;
 }else if($("#cnsExamine").val() == "Other Findings" && cnsTextField.length < 2 ) {
	 toastr.warning  ("Text field contain atleast 2 characters!");
	 return;
 }else if(otherFindText.length == 1 ){
 toastr.warning("Text field contain atleast 2 characters!");
 return;
 }

 //SETTING JSON VARIABLES

 if($("#rsExamine").val() == "Other Findings"){
    upRSval = $("#rsExamineText").val();
    }else{
    upRSval = $("#rsExamine").val();
    }

    if($("#geExamine").val() == "Other Findings"){
        upGEval = $("#geExamineText").val();
        }else{
        upGEval = $("#geExamine").val();
        }
 if($("#paExamine").val() == "Other Findings"){
        upPAval = $("#PAExamineText").val();
        }else{
        upPAval = $("#paExamine").val();
        }
 if($("#gutExamine").val() == "Other Findings"){
        upGUTval = $("#gutExamineText").val();
        }else{
        upGUTval = $("#gutExamine").val();
        }

 if($("#cvsExamine").val() == "Other Findings"){
        upCVSval = $("#cvsExamineText").val();
        }else{
        upCVSval = $("#cvsExamine").val();
        }

 if($("#cnsExamine").val() == "Other Findings"){
        upCNSval = $("#cnsExamineText").val();
        }else{
        upCNSval = $("#cnsExamine").val();
        }

	otherval = $("#otherFindings").val();

	const clinicalExaminArray = [];


	let createdAt;
	if (update === false) {
		createdAt = dayjs(new Date()).utc(true).valueOf();
	}
	else {
		createdAt = 'createdAt' in newClinicalExaminOptions && newClinicalExaminOptions.createdAt ? newClinicalExaminOptions.createdAt : dayjs(new Date()).utc(true).valueOf();
	}



	let clinicalJson = {
		"createdAt": createdAt,
        "RS": upRSval,
        "GE": upGEval,
		"P/A": upPAval,
		"GUT": upGUTval,
        "CVS": upCVSval,
        "CNS": upCNSval,
        "Other": otherval
	}

	clinicalExaminArray.push(clinicalJson);
    console.log("add clinical examin episodeId: ",SELECTED_EPI_ID);
	let data = {
		"clientId": Number(localStorage.getItem("clientId")),
		"patientUserId": Number(userId1),
		"doctorName": doctorName,
		"clinicalJson": clinicalExaminArray,
		"episodeId": SELECTED_EPI_ID
	};

	if (update === true) {
		Object.assign(data, {
			"drEpisodeId": Number(doctorEpisodeId),
			"drAdviceId": drAdviceMongoId
		});
	}
	console.log("Pat clinical examination: ",data);


	// console.log(data);


		if (update === false) {
			$.ajax({
				type: "POST",
				url: serverPath + "/addDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {
						let savedDrEpisode = data.content.drEpisodeId;
						medicationId = data.content.id;
						toastr.success("Clinical Examination Added successfully!")
                        $("#clinical-examination-modal").modal('hide');


                        $('#addHistoryButton2').hide();
						// getDrEpisodeList();
						console.log('-------ADVIC:SAVE-SUC:-',doctorEpisodeId);
						if (!doctorEpisodeId) {
						    $("#" + SELECTED_EPI_BTN_INDEX).attr('onclick', '');
							$("#" + SELECTED_EPI_BTN_INDEX).attr("onclick", patEpisodeChange(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX, false, savedDrEpisode));
						}
						doctorEpisodeId = savedDrEpisode;
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
					}
				},
				error: function (xhr, status, error) {
					$("#addClinicalExaminButton").attr("disabled", false);
					$("#updateClinicalExaminButton").attr("disabled", false);
					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}
						else{
						    toastr.error(xhr.responseJSON.message);
						}
						//toastr.error(xhr.responseJSON.message);
					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		} else {
			$.ajax({
				type: "POST",
				url: serverPath + "/updateDrAdvice",
				headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
				data: JSON.stringify(data),
				contentType: "application/json",
				success: function (data) {
					if (data.code = "200") {

						canClinicalExamin();

						$("#addClinicalExaminButton").attr("disabled", false);
						$("#updateClinicalExaminButton").attr("disabled", false);

						toastr.success('Clinical Examination Updated Successfully', '');
                            //$("#clinical-examination-modal").modal('hide');
						getDrEpisodeDetails(SELECTED_EPI_ID, SELECTED_EPI_BTN_INDEX);
					}
//					else{
//					console.log("error data: ",data);
//					    toastr.error(data.message);
//					}
				},
				error: function (xhr, status, error) {
//				console.log("error data: ",xhr.responseJSON.message);
					$("#addClinicalExaminButton").attr("disabled", false);
					$("#updateClinicalExaminButton").attr("disabled", false);

					if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
						if (xhr.responseJSON.code === 401) {
							(async () => {
								toastr.error(xhr.responseJSON.message);
								await sleep();
								window.location.href = './login';
							})()
						}else{
						    toastr.error(xhr.responseJSON.message);
						}

					} else {
						toastr.error("No Data Found!");
					}
				}
			}
			)
		}


}


//UPDATE CLINICAL EXAMINATION

function updateClinicalExamin(clinicalExaminRow) {
	console.log("Update clinical examin modal",clinicalExaminRow);
  //    console.log("adviceOptions: ",adviceOptions);


  //let newClinicalExaminOptions = newClinicalExaminOptions.filter(function (object) { return object.createdAt != clinicalExaminRow.createdAt });

  let createdAt = clinicalExaminRow.createdAt;



  openclinicallyExamine(true);

//let paValuePopup = clinicalExaminRow.


    if(clinicalExaminRow.RS && clinicalExaminRow.RS !== "Clinically Normal"){
    		$('#rsExamine').val("Other Findings");
    		$('#rsExamineText').attr("disabled", false);
            $('#rsExamineText').val(clinicalExaminRow.RS);
    	} else $('#rsExamine').val("Clinically Normal");

	if(clinicalExaminRow.GE && clinicalExaminRow.GE !== "Clinically Normal"){
		$('#geExamine').val("Other Findings");
		$('#geExamineText').attr("disabled", false);
        $('#geExamineText').val(clinicalExaminRow.GE);
	} else $('#geExamine').val("Clinically Normal");

	if(clinicalExaminRow["P/A"] && clinicalExaminRow["P/A"] !== "Clinically Normal"){
    		$('#paExamine').val("Other Findings");
    		$('#PAExamineText').attr("disabled", false);
            $('#PAExamineText').val(clinicalExaminRow["P/A"]);
    	} else $('#paExamine').val("Clinically Normal");

        if(clinicalExaminRow.GUT && clinicalExaminRow.GUT !== "Clinically Normal"){
		$('#gutExamine').val("Other Findings");
		$('#gutExamineText').attr("disabled", false);
        $('#gutExamineText').val(clinicalExaminRow.GUT);
	    } else $('#gutExamine').val("Clinically Normal");

        if(clinicalExaminRow.CVS && clinicalExaminRow.CVS !== "Clinically Normal"){
        		$('#cvsExamine').val("Other Findings");
        		$('#cvsExamineText').attr("disabled", false);
                $('#cvsExamineText').val(clinicalExaminRow.CVS);
       } else $('#cvsExamine').val("Clinically Normal");


     if(clinicalExaminRow.CNS && clinicalExaminRow.CNS !== "Clinically Normal"){
            		$('#cnsExamine').val("Other Findings");
            		$('#cnsExamineText').attr("disabled", false);
                    $('#cnsExamineText').val(clinicalExaminRow.CVS);
           } else $('#cnsExamine').val("Clinically Normal");

    $('#otherFindings').val(clinicalExaminRow.Other);

  //$('#advComments').val(referredBy);
  $('.Newpolic2').parent().addClass('is-dirty');

  $('#addClinicalExaminButton').hide();
  $('#updateClinicalExaminButton').show();

  $('#updateClinicalExaminButton').unbind().click(function () {

	  //        saveAdvice(true, newAdviceOptions);
	  addClinicalExaminNew(true, clinicalExaminRow);
  })
}