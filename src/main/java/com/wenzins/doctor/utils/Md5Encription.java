package com.wenzins.doctor.utils;

import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class Md5Encription {

    //private static Cipher cipher;
    //private static Key key;
    // private final BASE64Encoder b64Encoder = new BASE64Encoder();
    // private final Base64 b64Decoder = Base64.;
	public static void main(String[] args){

	}
	
    static Cipher initilizeCipher() {
        Cipher	cipher=null;
        try {
            //Key key;
            cipher = Cipher.getInstance("AES");
            byte[] raw = { (byte) 0xA5, (byte) 0x01, (byte) 0x7B, (byte) 0xE5, (byte) 0x23, (byte) 0xCA, (byte) 0xD4,
                    (byte) 0xD2, (byte) 0xC6, (byte) 0x5F, (byte) 0x7D, (byte) 0x8B, (byte) 0x0B, (byte) 0x9A,
                    (byte) 0x3C, (byte) 0xF1 };
            //key = new SecretKeySpec(raw, "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

        return cipher;
    }
    static Key initilizekey() {
        Key key=null;
        try {


            byte[] raw = { (byte) 0xA5, (byte) 0x01, (byte) 0x7B, (byte) 0xE5, (byte) 0x23, (byte) 0xCA, (byte) 0xD4,
                    (byte) 0xD2, (byte) 0xC6, (byte) 0x5F, (byte) 0x7D, (byte) 0x8B, (byte) 0x0B, (byte) 0x9A,
                    (byte) 0x3C, (byte) 0xF1 };
            key = new SecretKeySpec(raw, "AES");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return key;
    }


    public static String encrypt(String aData) {
        String result = "";
        try {
            if(aData!=null &&!aData.contentEquals("")){
                Cipher cipher=initilizeCipher();
                Key key= initilizekey();
                cipher.init(Cipher.ENCRYPT_MODE, key);
                byte[] utf8 = aData.getBytes("UTF8");
                byte[] encryptedData = cipher.doFinal(utf8);
                result = Base64.encode(encryptedData);// this.b64Encoder.encode(encryptedData);
            }
        } catch (InvalidKeyException oException) {
        } catch (IllegalBlockSizeException oException) {
        } catch (BadPaddingException oException) {
        } catch (IOException oException) {
        }
        return result;
    }

    public static String decrypt(String aData) {
        String result = "";
        try {
            if(aData!=null &&!aData.contentEquals("")){
                Cipher cipher=initilizeCipher();
                Key key= initilizekey();
                cipher.init(Cipher.DECRYPT_MODE, key);
                byte[] decodedData = Base64.decode(aData);// this.b64Decoder.decodeBuffer(aData);
                byte[] utf8 = cipher.doFinal(decodedData);
                result = new String(utf8, "UTF8");
            }
        } catch (InvalidKeyException oException) {
        } catch (Base64DecodingException oException) {
        } catch (IllegalBlockSizeException oException) {
        } catch (BadPaddingException oException) {
        } catch (UnsupportedEncodingException oException) {
        }
        return result;
    }
}