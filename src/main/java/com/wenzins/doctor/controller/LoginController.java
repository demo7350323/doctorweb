/**
 *
 */
package com.wenzins.doctor.controller;

import com.wenzins.doctor.utils.Md5Encription;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * @author wenzins
 */

@Controller
public class LoginController {

    private ResourceBundle resourceBundle = ResourceBundle.getBundle("messages_en");
    private String serverPath = resourceBundle.getString("label.serverPath");

//    @RequestMapping(value = {"/home"}, method = {RequestMethod.GET, RequestMethod.POST})
//    public String statistics(HttpServletRequest request, HttpServletResponse response) {
//
//        return "home";
//    }

    @RequestMapping(value = {"/home2"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String statistics(HttpServletRequest request, HttpServletResponse response) {

        return "home2";
    }
//    @RequestMapping(value = {"/dummyHome"}, method = {RequestMethod.GET, RequestMethod.POST})
//    public String statistics(HttpServletRequest request, HttpServletResponse response) {
//
//        return "dummyHome";
//    }
    @RequestMapping(value = {"/", "/login"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String login(String userName, String password, String productType, HttpServletRequest request, HttpServletResponse response, Model model) {
//        System.out.println("LoginCTR: " + userName);

        model.addAttribute("userName", userName);
        model.addAttribute("password", password);
        model.addAttribute("productType", productType);
        return "login";
    }


    @RequestMapping(value = "/logout", method = {RequestMethod.GET, RequestMethod.POST})
    public String logout(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        session.invalidate();
        return "login";
    }
    
    @RequestMapping(value = "/adminLogout", method = {RequestMethod.GET, RequestMethod.POST})
    public String adminLogout(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        session.invalidate();
        return "admin";
    }

    @RequestMapping(value = "/patientList", method = RequestMethod.GET)
    public String patientList() {
        return "patientList";
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
    public String resetPassword() {
        return "resetPassword";
    }

    @RequestMapping(value = "/dashBoard", method = RequestMethod.GET)
    public String registration() {
        return "dashBoard";
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list() {
        return "list";
    }
    
    @RequestMapping(value = "/dashBoardNew", method = RequestMethod.GET)
    public String dashBoardNew() {
        return "dashBoardNew";
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home2() {
        return "home";
    }
    
    @RequestMapping(value = "/dashBoard_D", method = RequestMethod.GET)
    public String dashBoard_D() {
        return "dashBoard_D";
    }
    
    @RequestMapping(value = "/adminPage", method = RequestMethod.GET)
    public String adminPage() {
        return "adminPage";
    }
    
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin() {
        return "admin";
    }

    @RequestMapping(value = "/initiateCall", method = {RequestMethod.GET, RequestMethod.POST})
    public String initiateCall(ModelMap modal, Long serverKeyId, Long profileId, String roomName,Long calleProfileId) {
        try {
            if (roomName == null) {
                String uri = serverPath + "/intiateAndCloseCallv3";

                MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
                parameters.add("serverKeyId", serverKeyId + "");
                parameters.add("profileId", profileId + "");
                parameters.add("initiate", true + "");
                parameters.add("calleProfileId",calleProfileId+"");

                HttpHeaders headers = new HttpHeaders();
                headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);

                HttpEntity<MultiValueMap<String, String>> entity =
                        new HttpEntity<MultiValueMap<String, String>>(parameters, headers);

                ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
                RestTemplate restTemplate = new RestTemplate(requestFactory);

                String response = restTemplate.postForObject(uri, entity, String.class);
                if (response != null) {
                    roomName = Md5Encription.encrypt(response);
                    roomName = roomName.replaceAll("[^a-zA-Z0-9]", "");
                    modal.addAttribute("roomName", roomName);
                    return "initiateCall";
                } else {
                    return "dashBoard";
                }
            } else {
                roomName = roomName.replaceAll("[^a-zA-Z0-9]", "");
                modal.addAttribute("roomName", roomName);
                return "initiateCall";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "dashBoard";
        }

    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        int timeout = 30000;
        RequestConfig config =
                RequestConfig.custom()
                        .setConnectTimeout(timeout)
                        .setConnectionRequestTimeout(timeout)
                        .setSocketTimeout(timeout)
                        .build();
        CloseableHttpClient client =
                HttpClientBuilder.create().setDefaultRequestConfig(config).build();
        return new HttpComponentsClientHttpRequestFactory(client);
    }


    @RequestMapping(value = "/signIn", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public HashMap<String, Object> signIn(HttpServletRequest request, Long userId,String userGroupCode) {
        HttpSession session = request.getSession();
        session.setAttribute("user", userId);
        session.setAttribute("userGroupCode", userGroupCode);
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("code", "200");
        hashMap.put("content", "success");
        return hashMap;
    }

    @RequestMapping(value = "/expiry", method = {RequestMethod.GET, RequestMethod.POST})
    public String expiry() {
        return "expiry";
    }
    
    @RequestMapping(value="/sharePage",method = RequestMethod.GET)
	public String sharePage( Model model,HttpSession session) {
    	model.addAttribute("hashId", session.getAttribute("hashId"));
    	
		return "sharePage";
	}
    
    @RequestMapping(value="/sharedDocument/{hashId}",method = RequestMethod.GET)
	public String sharedDocument(@PathVariable("hashId") String hashId, HttpSession session) {
		try {
			
			session.setAttribute("hashId", hashId);
			
		
			
			
			
			
			return "redirect:/sharePage";
		} catch (Exception e) {
			e.printStackTrace();
			
		}
        return "redirect:/errorPage";
    }
    
    
    

}
