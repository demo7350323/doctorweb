package com.wenzins.doctor.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;


public class LoginHandler implements HandlerInterceptor {
	
	private ResourceBundle resourceBundle = ResourceBundle.getBundle("messages_en");
	private String listDashboard = resourceBundle.getString("label.listDashboard");

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        HttpSession session = request.getSession();

        session.getAttribute("user");
        // TODO Auto-generated method stub
        Date date = new Date();
        String strExpiryDate = "20/12/2019";

        Date expiryDate = new SimpleDateFormat("dd/MM/yyyy").parse(strExpiryDate);
//        if (date.compareTo(expiryDate) > 0) {
//            //response.sendRedirect("expiry");			// JIRA ticket issue (SAMVITA-106)
//            response.sendRedirect("");
//            return false;
//        }/*else {
//        	 return true;
//        }*/
//
//        if (session.getAttribute("user") != null){
//        	if(session.getAttribute("userGroupCode").equals("DOC") && listDashboard.contentEquals("TRUE")) {
//        		System.out.println(listDashboard);
//        		System.out.println("userGroupCode");
//        		response.sendRedirect("list");
//        		return false;
//        	}else {
//        		//response.sendRedirect("patientList");
//        		response.sendRedirect("statistics");
//        		return false;
//        	}
//
//        } else {
//        	//response.sendRedirect("patientList");
//            return true;
//        }

        return true;

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        // TODO Auto-generated method stub

    }

}
