<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="/WEB-INF/view/includes.jsp" %>

    <head>
        <meta charset="UTF-8">
        <title>Login</title>

        <link rel="stylesheet" href="resources/css/bootstrap.min.css" />
        <!-- Toastr -->
        <link rel="stylesheet" href="resources/css/toastr.min.css">
        <!-- Toastr style-->
        <link rel="stylesheet" href="resources/css/toastr.min.css">

        <link href="resources/css/AdminLTE.css" rel="stylesheet" />
        <link href="resources/css/_all-skins.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="resources/css/font-awesome.min.css" />
        <link rel="stylesheet" href="resources/css/material.css">
        <link href="resources/css/toastr.min.css" rel="stylesheet">

        <script src="resources/js/jQuery-2.1.4.min.js"></script>
        <script src="resources/js/toastr.min.js"></script>
        <script src="resources/js/customHTML.js"></script>
        <!-- Toastr -->
        <script src="resources/js/toastr.min.js"></script>
        <!-- Toastr -->
        <script src="resources/js/toastr.min.js"></script>
        <style>
            .navbar-t-centered {
                position: absolute;
                left: 34%;
                display: block;
                text-align: center;
                color: #fff;
                top: 0px;
                padding: 15px;
                font-weight: 400;
                font-size: 12px;

            }

            @media screen and (min-width:768px) {
                .navbar-t-centered {
                    position: absolute;
                    left: 38%;
                    display: block;
                    /*  width: 160px; */
                    text-align: center;
                    color: #fff;
                    top: 0px;
                    padding: 15px;
                    font-weight: 400;
                    font-size: 22px;

                }
            }
        </style>
        <script type="text/javascript">


                var DW_JWT_TOKEN = localStorage.getItem('DW_JWT_TOKEN');
                  var loggedInUserFullName = localStorage.getItem('loggedInUserFullName');
                  var USER_NAME =  localStorage.getItem('loginUsername');
                  var ROLL_NAME = localStorage.getItem('roleName');
                  var RESET_PASSWORD = localStorage.getItem('RESET_PASSWORD');

                  // Check if the user is logged in
                  if (DW_JWT_TOKEN === null) {
                    // Redirect to the logout page or login page
                    window.location.href = 'logout';
                  } else if (RESET_PASSWORD === 'true') {
                    // The user is logged in and needs to reset their password
                    console.log('RESET PASSWORD PAGE');
                  } else {
                    // The user is logged in and not resetting their password
                    console.log('OTHER PAGE');
                  }
        </script>
    </head>

    <body id="bodyId">
        <header class="main-header"> <!-- Logo --> <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation"
                style="margin-left:0% !important ;max-height:50px; background-color:#3c8dbc">
                <!-- Sidebar toggle button-->
                <img src="resources/images/WriztoLogoBox.jpg" class="user-image" alt="User Image"
                    style="height: 50px; padding-left: 5px; margin-left: 5px;">


                <span class="navbar-t-centered">Wrizto Healthcare System</span>


            </nav>
        </header>

        <div class="row" style="margin-top:8%;padding-bottom:50px;">
            <div class="col-md-12">
                <div class="col-md-2">
                </div>
                <!-- <div class="col-md-3"></div> -->
                <div class="col-md-4" id="logRegId"
                    style="padding: 0px; color: #fff; border-radius: 20px 20px 20px 20px; margin-bottom: 4%; opacity: 1; filter: alpha(opacity = 100);">

                    <div class="" style="border-top-width: 0px; padding: 0px; margin-top: 30px;">
                        <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">



                            <div class="" id="loginDiv" style="">


                                <form class="form-inline" role="form" id="loginForm" name="user_login" method="post">
                                    <input type="hidden" name="productTypeCode" value="VK" id="productTypeCode">
                                    <div class=""
                                        style="height: 100%; border-width: 0px 0px 0px; border-style: solid; border-color: rgb(217, 217, 217); -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; border-image: none;">
                                        <div class="form-group col-xs-12"
                                            style="padding-bottom: 15px; padding-top: 15px; margin-bottom: 0px;">
                                            <div class="row">
                                                <!-- <div class="col-xs-2 col-md-2"></div> -->
                                                <div class="col-xs-12 col-md-12" style="color: rgb(60, 141, 188);">
                                                    <h4>Change Password</h4>
                                                    <!-- <p style="color: rgb(60, 141, 188); font-size: 18px;margin-left: 97px; margin-top: -5px;">Doctor</p> -->
                                                </div>
                                            </div>
                                            <!-- <div class="row" style="margin-top: 12%;">
											<div class="col-xs-2 col-md-2"></div>
											<div class="col-xs-6 col-md-6">
												<span>
													<p style="color: #868a8a; font-size: 22px;padding-left: 5px;">LOGIN</p>
												</span>
											</div>
										</div> -->
                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                <!-- <div class="col-xs-2"></div> -->

<div class="col-xs-12" style="padding-left: 10px; padding-right: 0px;">
    <div style="width: 100%; color: black" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input autocomplete="off" autocorrect="off" placeholder="NEW PASSWORD" spellcheck="false"
            class="mdl-textfield__input error-popover" type="password" id="inviteNewPassword" name="newPaswd"
            maxlength="64">
        <span id="ShowHidePassword" class="glyphicon glyphicon-eye-close pull-right"
            style="margin-top: -6%; cursor: pointer; margin-right: 2%; color: black; font-size: 15px;"
            onclick="ShowHidePassword('inviteNewPassword', 'ShowHidePassword');"></span>
    </div>
</div>

<div class="col-xs-12" style="padding-right: 0px;padding-left: 10px;">
    <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 100%; color: black">
            <input autocomplete="off" autocorrect="off" placeholder="CONFIRM PASSWORD" spellcheck="false"
                class="mdl-textfield__input error-popover" type="password" id="inviteConfirmPassword"
                name="confirmpassword" maxlength="64">
            <span id="ShowHideConfirmPassword" class="glyphicon glyphicon-eye-close pull-right"
                style="margin-top: -6%; cursor: pointer; margin-right: 2%; color: black; font-size: 15px;"
                onclick="ShowHidePassword('inviteConfirmPassword', 'ShowHideConfirmPassword');"></span>
        </div>
    </div>
</div>

                                                <div class="row" style="margin-top: 4%;">
                                                    <!-- <div class="col-xs-2 col-md-2"></div> -->
                                                    <div class="col-xs-12 col-md-12" style="padding-left: 10px;">
                                                        <button id="felix-widget-button-btn-resetPaswd"
                                                            class="pull-right mdl-button mdl-js-button mdl-button--raised mdl-button--accent"
                                                            onclick="resetPassword();" data-style="expand-left"
                                                            title="Reset" name="btnSignIn" type="button"
                                                            style="color: white; background: rgb(60, 141, 188); width: 100%">
                                                            <!-- background: #24bac1; -->
                                                            <span>RESET</span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <button id="demo-show-snackbar"
                                                    class="mdl-button mdl-js-button mdl-button--raised"
                                                    style="display: none" type="button"></button>
                                                <div id="demo-snackbar-example" class="mdl-js-snackbar mdl-snackbar">
                                                    <div class="mdl-snackbar__text"></div>
                                                    <button class="mdl-snackbar__action" type="button"></button>
                                                </div>

                                                <div class="row" style="margin-top: 8%;">
                                                    <div class="col-xs-6 col-md-6"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>


                        </div>


                    </div>

                </div>
                <div class="col-md-4"
                    style="padding: 0px; color: #fff; border-radius: 20px 20px 20px 20px; margin-bottom: 4%; opacity: 1; filter: alpha(opacity = 100);">
                    <div class="" style="border-top-width: 0px; padding: 0px; margin-top: 30px;">
                        <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                            <div class=""
                                style="height: 100%; border-width: 0px 0px 0px; border-style: solid; border-color: rgb(217, 217, 217); -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; border-image: none;">
                                <div class="form-group col-xs-12"
                                    style="padding-bottom: 15px; padding-top: 15px; margin-bottom: 0px;">
                                    <div class="row">
                                        <!-- <div class="col-xs-2 col-md-2"></div> -->
                                        <div class="col-xs-12 col-md-12"
                                            style="background-color: #e6e9ed;border-radius: 15px;">

                                            <!-- <p style="color: rgb(60, 141, 188); font-size: 18px;margin-left: 97px; margin-top: -5px;">Doctor</p> -->
                                            <h4 style="color: rgb(60, 141, 188);margin-left:45px;">Welcome To Doctor
                                                Portal</h4>
                                            <h6 style="color: rgb(60, 141, 188);margin-left:45px;margin-top: 0px;">As a
                                                doctor you can do these actions</h6>


                                            <ul style="color:#333; list-style: none;">
                                                <li
                                                    style="padding:10px;padding-bottom: 2px;padding-top: 0px;padding-left:15px;">
                                                    <img src="resources/images/ecg-lines.png"
                                                        style="width: 30px;border-radius: 50%;background: white; padding:0px;padding-right:0px;"><span
                                                        style="margin-left:0px;padding-left: 5px;">Review Vital
                                                        Results</span>
                                                </li>
                                                <li style="padding:10px;padding-top:0px;padding-bottom:0px;"><img
                                                        src="resources/images/Dr_worklist.png"
                                                        style="width:40px; padding:5px; border-radius: 100px;">Advice &
                                                    Refer</li>
                                                <li style="padding:10px;padding-top:0px;padding-bottom:0px;"><img
                                                        src="resources/images/video.png"
                                                        style="width:40px; padding:5px;padding-right:10px; -webkit-filter: grayscale(100%);">Video
                                                    Consultation</li>
                                                <li style="padding:10px;padding-top:0px;padding-bottom:0px;"><img
                                                        src="resources/images/management-1.png"
                                                        style="width:40px; padding:5px;;border-radius: 100px;">Expert
                                                    Video Consultation</li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-2">
                </div>

            </div>
        </div>

        <!-- <div id="footer" style="right:5px;  position:fixed;  bottom:0px;" class="customfooter">

        Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>. All rights reserved.
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
</div> -->






        <style>
            .bg-custom {
                color: black;
            }

            .red-tooltip+.tooltip>.tooltip-inner {
                background-color: #f00;
            }

            .realperson-regen {
                text-align: left;
            }
        </style>

        <script type="text/javascript">

function resetPassword() {
       toastr.options.timeOut = 6000;
            let NEW_PASSWORD = $("#inviteNewPassword").val();
            let CONFIRM_PASSWORD = $("#inviteConfirmPassword").val();
var hasNumber = /\d/.test(NEW_PASSWORD);
    var hasUpperCase = /[A-Z]/.test(NEW_PASSWORD);
    var hasSpecialChar = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/.test(NEW_PASSWORD);
    var hasLowerCase = /[a-z]/.test(NEW_PASSWORD);
    var hasValidLength = NEW_PASSWORD.length >= 8;

var jsonData = JSON.stringify({
    userName: USER_NAME,
	newPassword: NEW_PASSWORD,
    confirmNewPassword: CONFIRM_PASSWORD,
    resetPassword: false
	});


 console.log("reset password")
    console.log("reset password json",jsonData)

    if($("#inviteNewPassword").val() == ''){
        toastr.error("New Password field is Empty!");
        return;
    }else if(!hasNumber){
             toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
             return;
         }else if(!hasUpperCase){
          toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
          return;
         }else if(!hasLowerCase){
          toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
          return;
          }else if(!hasSpecialChar){
           toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
           return;
           }
          else if(!hasValidLength){
          toastr.error("Password must contain atleast 8 characters!");
          return;
          }
    if($("#inviteConfirmPassword").val() == ''){
        toastr.error("Confirm Password field is Empty!")
        return;
    }




//$('#resetPasswdSpinner').show();
    //API CALL

    $.ajax({
        type:'POST',
        contentType: "application/json",
        url: serverPath + "/resetAdminPassword",
        data:jsonData,
        headers: { "Authorization": 'Bearer ' + DW_JWT_TOKEN },
        success: function(result) {


            let data = result.error;
            console.log('success: ',data);

            if(result.error == false){
                console.log("success block",ROLL_NAME, RESET_PASSWORD)
                toastr.success("Password Reset Completed, Please login again by using new password ")
                setTimeout(function() {
                console.log("TIMEOUT SET TOASTER")
                 window.location.href = "logout";
                }, 8000);
                if(ROLL_NAME == 'Doctor'){

                    window.location.href = "logout";

                }else if(ROLL_NAME == 'Client Admin'){

                    window.location.href = "logout";

                }else if(ROLL_NAME == 'Nurse'){

                    window.location.href = "logout";
                }
                $('#felix-widget-button-btn-resetPaswd').prop('disabled', false);

            }else if(result.error == true){
            //$('#resetPasswdSpinner').hide();
                console.log("error block")
                toastr.error(result.message);
                var url = "./login ";
                window.location.href= url;
            }




            //console.log("Appointments and packages",appointments,packages)


        },
        error: function (xhr, status, error) {
            console.log("change password-->3 : ",error);

            $('#felix-widget-button-btn-resetPaswd').prop('disabled', false);
            //$('#resetPasswdSpinner').hide();
            if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                if (xhr.responseJSON.code !== 200) {
                    (async () => {
                        toastr.error(xhr.responseJSON.message);

                    })()
                } else {
                    toastr.error(xhr.responseJSON.message);
                }
            } else {
                toastr.error('Failed add user.Internet Disconnected/Server error');
            }

        }
    });

}
//HIDE AND SHOW PASSWORD EYE ICON


    function ShowHidePassword(inputId, iconId) {
        var inputElement = $('#' + inputId);
        var iconElement = $('#' + iconId);

        if (inputElement.attr('type') === 'password') {
            inputElement.attr('type', 'text');
            iconElement.removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
        } else {
            inputElement.attr('type', 'password');
            iconElement.removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
        }
    }

function ShowHideConfirmPassword(ID) {

                if ($('#inviteConfirmPassword').attr('type') == "password") {
                    $('#inviteConfirmPassword').attr('type', 'text');
                } else {
                    $('#inviteConfirmPassword').attr('type', 'password');
                }
            }
$("#inviteConfirmPassword").keyup(function (event) {
                if (event.keyCode === 13) {
                    $("#felix-widget-button-btn-resetPaswd").click();
                }
            });
        </script>

        <footer class="main-footer"
            style="display:block;padding-right:15px; margin-left:0% !important;position: fixed;bottom: 0px;margin-right: auto;margin-left: auto;width:100%;">
            <!--  <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>.</strong> All rights reserved. -->
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0.0
            </div>
            Copyright &copy; 2021-2022 <a href="http://www.wrizto.com/" target=_blank
                style="color: rgb(60, 141, 188);">Wrizto Healthcare Pvt. Ltd.</a> All rights reserved.
        </footer>
    </body>

</html>