<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title> DashBoard</title>

	<link rel="stylesheet" href="resources/css/bootstrap.min.css"/>


	<link rel="stylesheet" href="resources/css/toastr.min.css"/>
	<link rel="stylesheet" href="resources/css/bootstrap-datepicker.css">

	<link href="resources/css/AdminLTE.css" rel="stylesheet"/>
	<link rel="stylesheet" href="resources/css/material.css"/>
	<link href="resources/css/_all-skins.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="resources/css/font-awesome.min.css"/>
	    <script src="resources/js/jQuery-2.1.4.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/fastclick.min.js"></script>
    <script src="resources/js/app.min.js"></script>
    <script src="resources/js/demo.js"></script>
    <script defer src="resources/js/material.js"></script>

    <script src="resources/js/bootstrap3-typeahead.js" 	type="application/javascript"></script>
    <script src="resources/js/jquery.mask.js"></script>

     <script src="resources/js/toastr.min.js"></script>
     <script src="resources/js/customHTML.js"></script>
     <script src="resources/js/bootstrap-datepicker.js" type="application/javascript"></script>
     <script src="resources/customJS/bloodVitals.js"></script>

    <style>


.panel-title {
  position: relative;
  cursor:pointer;
}

.panel-body{
  padding:0px;
}
.panel-heading {
    padding: 4px 10px 4px 0px;
}

.panel-title::after {
  content: "\f107";
  color: #333;
  top: -2px;
  right: 0px;
  position: absolute;
  font-family: "FontAwesome";
}

.panel-title[aria-expanded="true"]::after {
  content: "\f106";
}

/*
 * Added 12-27-20 to showcase full title clickthrough
 */

.panel-heading-full.panel-heading {
  padding: 0;
}

.panel-heading-full .panel-title {
  padding: 10px 15px;
}

.panel-heading-full .panel-title::after {
  top: 10px;
  right: 15px;
}



    .navbar-t-centered {
      position: absolute;
		left: 34%;
		display: block;
		text-align: center;
		color: #fff;
		top: 0px;
		padding: 15px;
		font-weight: 400;
		font-size: 12px;

    }
    @media screen and (min-width:768px){
	.navbar-t-centered {
	        position: absolute;
	        left: 38% ;
	        display: block;
	       /*  width: 160px; */
	        text-align: center;
	        color: #fff;
	        top:0px;
	        padding:15px;
	        font-weight: 400;
	        font-size:22px;

	    }
    }

	.card-main-conatiner{
	  width: 192px;
	  height: 96px;
	  margin: 5px;
	  box-shadow: 5px 10px 10px lightgray;
	}
	.card-scroll-container{
	  /*height: 105px;*/
	 /* max-width: 300px;*/
	  margin: unset;
	  /*border: 1px solid sienna;*/
	  overflow-x: scroll;
	  overflow-y: hidden;
	  margin: 5px;
	}

	.card-scroll-container::-webkit-scrollbar {
	  height: 5px;
	}

	.card-scroll-container::-webkit-scrollbar-track {
	  /*background-color: lightgray;*/
	}

	.card-scroll-container::-webkit-scrollbar-thumb {
	  /*box-shadow: inset 0 0 6px #3c8dbc;*/
	  background-color: #3c8dbc;
	  border-radius: 8px;
	}

	.card-inner {
	  height: 100%;
	  white-space:nowrap;
	  text-align: center;
	}

	.card-floatLeft {
	  width: 140px;
	  margin:3px 0px;
	  display: inline-block;
	  /*border: 1px solid lightgray;*/
	  border-left: 1px solid lightgray;
	  border-right: 1px solid lightgray;
	  text-align: center;
	 /*box-shadow: 2px 2px 2px lightgray; */
	}

	.card-floatLeft-2digitValue{
	  width: 120px;
	  margin:3px 0px;
	  display: inline-block;
	  border-left: 1px solid lightgray;
	  border-right: 1px solid lightgray;
	  text-align: center;
	}

	.card-floatLeft-bloodAnalyserValue{
	  width: 120px;
	  margin:3px 0px;
	  display: inline-block;
	  text-align: center;
	}

	.vital-title{
	  font-size: 14px;
	  text-align: center;
	  font-weight: 500;
	  margin: 4px;
	}

	.vital-actual-value{
	  margin: 2px;
	  font-weight: 600;
	  font-size: 16px;
	}

	.vital-recorded-time{
	  margin: unset;
	  font-size: 12px;
	}

	.bloodVitalsMainHeader{
	/* color:#3c8dbc;*/
	color:blue;
	margin:0px;
	}
	.accBtnCardiac, .accBtnHarmone, .accBtnInfection, .accBtnDiabetic, .accBtnRespiratory, .accBtnVector, .accBtnFecal{
	color:#0437F2;
	}

	.bvLevelImg{
		width:50%;
	}
	.bvResult{
		color:blue;
	}
	.columnLevl {
  float: left;
  width: 25%;
  padding: 5px;
}

.tooltip {
  font-family: Georgia;
  white-space: normal;

}

.tooltipTitle p{
	margin-bottom:0px;
	font-size:12px;
	color: black;
}

.tooltip.left  .tooltip-inner{
	text-align:start;
	/* background-color: #367fa9;*/
	background-color: white;
	border-radius:5px;
	border:1px solid gray;
	font-size:2px;
}

</style>
</head>

<script>

function myfun()
{
   //alert("The page has been refreshed.");
   console.log("******On unload***********");
}
</script>
<body class="hold-transition skin-blue sidebar-mini" onbeforeunload="myfun()">
    <div class="wrapper">
	<div id="load2"
			style="position: fixed; left: 50%; top: 40%; width: 120px; height: 100%; z-index: 9999;display:none">
			<img style="" src="resources/images/icons/loading.gif" alt="loading" />
			<br />
		</div>
        <header class="main-header">
      <nav class="navbar navbar-static-top" role="navigation"
		style="margin-left:0% !important ;max-height:50px; background-color:#3c8dbc">
	<!-- Sidebar toggle button-->
	<!-- <img src="resources/images/logowhite.png" class="user-image" alt="User Image" style="height: 50px; padding-left: 5px; margin-left: 5px;"> -->
		<img src="resources/images/WriztoLogoBox.jpg" onclick="navigateHome();" class="user-image" alt="User Image" style="cursor: pointer;height: 50px; padding-left: 5px; margin-left: 5px;">


		<span class ="navbar-t-centered" >Wrizto Healthcare System</span>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->

              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="resources/images/avatar.png" class="user-image" alt="User Image">
                  <span class="hidden-xs" id="loggedInUserFullName"></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="resources/images/avatar.png" class="img-circle" alt="User Image">
                    <p><span id="loggedInUserFullNameSpan"></span>
                    </p>
                  </li>
                  <!-- Menu Body -->
                 <li class="user-body">
                                  <div class="col-xs-8 text-center" id="nurseNameForActive" style="padding-left: 0px;padding-right: 0px;"><span style="font-size: 12px; margin-left: -33px;" ></span>
                                  <!-- <a id= "videoIconId" style="border-radius: 50%; ;margin-left: 5px;background-color: green" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                  				   <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a> -->
                                  </div>
                                  <!--
                                  <div class="col-xs-4 text-right" style="padding-left: 5px;padding-right: 0px;padding-top:5px;">
                                    <a href="patientList">Patient List</a>
                                  </div>
                                   -->
                                </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                    </div>
                    <div class="pull-right" id='patListLogout'>
                      <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->

            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="margin-left:0% !important;padding-bottom:40px;">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="display:inline-flex;width:100%;">

         <strong style="font-size:18px;" id="dash_id"></strong>
     <!--      <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">User profile</li>
          </ol> -->

			<div id="chronologicalView" class="pull-right text-right" style="width:83%;height:40px;float:right">
        		<!-- <button >OP:12/05/2018</button>
        		<button >OP:18/04/2018</button>
        		<button >OP:09/02/2018</button>
        		<button >OP:01/01/2018</button> -->
        	</div>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary ">
                <div class="box-body box-profile text-center" >
                  <img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">
                  <h5 class="profile-username text-center" id="addPrfName"style="margin-bottom: 15px;font-size:16px;"></h5>
                  <p class="text-muted text-center" id="uhidId" style="margin-bottom: 5px;"></p>

              <ul class="list-group list-group-unbordered" id="addDet">

                  </ul>
 					<div class="col-md-12" id="dashBoardHideShoeVideoIconId">
                   <a style="padding: 12px 12px 10px; border-radius: 50%;box-shadow: 4px 6px ;margin-left: -8px;" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                   <i style="color:white;font-size:20px" class="fa fa-video-camera"></i></b></a>
                   <!-- <a style=" padding: 12px 12px 10px; border-radius: 50%;box-shadow: 4px 6px ;" class="btn btn-primary" title="Message"><b>
                   <i style="color:white;font-size:20px" class="fa fa-commenting"></i></b></a> -->
                   </div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <!-- About Me Box -->
              <!-- <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">About Me</h3>
                </div>/.box-header
                <div class="box-body">
                  <strong><i class="fa fa-book margin-r-5"></i>  Education</strong>
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                  </p>

                  <hr>

                  <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <p class="text-muted">Malibu, California</p>

                  <hr>

                  <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                  <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-success">Coding</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">Node.js</span>
                  </p>

                  <hr>

                  <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                </div>/.box-body
              </div> --><!-- /.box -->

             <!--  <div id="" class="pull-center text-center" style="width:100%%;height:40px;">
              <button type="button" id="" style="margin-left: 10px; background: rgb(54, 127, 169) none repeat scroll 0% 0%;" class="btn btn-primary normalCol" onclick="completeConsultation();">Complete Consultation</button>
              </div> -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li onclick="refreshVitalsPage()" class="active"><a href="#activity" data-toggle="tab">Vitals</a></li>
                  <li onclick="getHistoryOfUser(null);"><a href="#timeline" data-toggle="tab">Advice</a></li>

                   <button style="float: right; cursor: pointer; margin-top: 5px;background:#3c8dbc" class="btn btn-primary" onclick="EmrReport()" >EMR Report <i class="" style="color:""></i></button>
                   <!-- <button style="float: right; cursor: pointer; margin-top: 5px;background:white" class="btn" onclick="refreshPage()" >Refresh <i class="fa fa-refresh" style="color:black"></i></button> -->
                 <!--  <li><a href="#settings" data-toggle="tab">Settings</a></li> -->
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity" style="padding-left: 10px; padding-right: 10px;">


                          <div class="user-block" style="padding-top: 10px; padding-bottom: 15px;">
                         <!--   <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image"> -->
                        <span class='username' style="margin-left: 0px;">
                          <a href="#activity" style="font-size:16px;color:#3a86bd;display:none;">Vitals</a>
                          <a href='#' class='pull-right btn-box-tool'><!-- <i class='fa fa-times'></i> --></a>
						</span>
                        <span class='description' style="margin-left: 0px; padding-top: 10px; padding-left: 5px;" id="vitDate"></span>
                      </div><!-- /.user-block -->
					  <div class="row" id="addVitalsDet">
					</div>
					<div style="display:flex">
					<h6 style="font-size:16px;color:#3a86bd;flex: 0 0 95%;margin:10px 0px">Basic Vitals: </h6>
					<div style="display:flex;justify-content:end;align-items:center;flex:0 0 5%;">
						<span class="glyphicon glyphicon-info-sign" id="toolTipDb2" data-placement="left" data-toggle="tooltip" data-html="true"
						 title="<div class='tooltipTitle'><p style='color:#367fa9;text-align:center;font-weight:600;'>STATUS</p><p><img src='resources/images/rsz_dry-clean.png' alt='Snow' style='width:10%;margin-left:2px;'/> Normal</p> <p> <img src='resources/images/rsz_right-arrowhead.png' alt='Snow' style='width:10%;margin-left:2px;'/> Moderate</p> <p> <img src='resources/images/rsz_rhombus.png' alt='Snow' style='width:10%;margin-left:2px;'/> Abnormal</p> <p> <img src='resources/images/rsz_pentagon.png' alt='Snow' style='width:10%;margin-left:2px;'/> Very-Abnormal</p></div>" >
							<!-- title="<div class='tooltipTitle'><p>- Normal (72 ~ 100 bpm)</p><p>- Abnormal (<72 / >100 bpm)</p></div>" > -->

						</span>
					</div>

					</div>

					<div class="panel-group" id="accordionbv">

						<!-- Level Indicator Panel -->
						<!--
						<div class="panel panel-default">
						  <div class="panel-heading">
							<h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapseLvl"><button class="btn btn-link accBtnCardiac" type="button ">Level Indicator</button></h4>
						  </div>

						  <div id="collapseLvl" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="table-responsive">
								  <div id="levlStatusDiv"></div>
							  </div>
							</div>
						  </div>
						</div>
						 -->

						<!-- 1st Panel -->
						<div class="panel panel-default">
						  <div class="panel-heading">
							<h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapsebv" data-target="#collapseOnebv"><button class="btn btn-link accBtnCardiac" type="button ">BASIC VITALS</button></h4>
						  </div>

						  <div id="collapseOnebv" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="table-responsive">
								  <table class="table table-bordered table-striped">
									  <thead id="pulseHeader">
									  </thead>
									  <tbody id="pulseBody">
									  </tbody>
									</table>
							  </div>
							</div>
						  </div>
						</div>



                      <div class="row" id="addVitalsDet">
                      </div>
                      <div style="display:flex">
                      <h6 style="font-size:16px;color:#3a86bd;flex: 0 0 95%;margin:10px 0px">Blood Vitals: </h6>
                      <div style="display:flex;justify-content:end;align-items:center;flex:0 0 5%;">
	                      <span class="glyphicon glyphicon-info-sign" id="toolTipDb1" data-placement="left" data-toggle="tooltip" data-html="true"
	                       title="<div class='tooltipTitle'><p style='color:#367fa9;text-align:center;font-weight:600;'>STATUS</p><p><img src='resources/images/rsz_dry-clean.png' alt='Snow' style='width:10%;margin-left:2px;'/> Normal</p> <p> <img src='resources/images/rsz_right-arrowhead.png' alt='Snow' style='width:10%;margin-left:2px;'/> Moderate</p> <p> <img src='resources/images/rsz_rhombus.png' alt='Snow' style='width:10%;margin-left:2px;'/> Abnormal</p> <p> <img src='resources/images/rsz_pentagon.png' alt='Snow' style='width:10%;margin-left:2px;'/> Very-Abnormal</p></div>" >
	                      	<!-- title="<div class='tooltipTitle'><p>- Normal (72 ~ 100 bpm)</p><p>- Abnormal (<72 / >100 bpm)</p></div>" > -->

	                      </span>
                      </div>

                      </div>

                      <div class="panel-group" id="accordion">

                      	<!-- Level Indicator Panel -->
                      	<!--
                      	<div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapseLvl"><button class="btn btn-link accBtnCardiac" type="button ">Level Indicator</button></h4>
						    </div>

						    <div id="collapseLvl" class="panel-collapse collapse">
						      <div class="panel-body">
	      						<div class="table-responsive">
									<div id="levlStatusDiv"></div>
								</div>
						      </div>
						    </div>
						  </div>
                      	 -->

						  <!-- 1st Panel -->
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapseOne"><button class="btn btn-link accBtnCardiac" type="button ">CARDIAC MARKER</button></h4>
						    </div>

						    <div id="collapseOne" class="panel-collapse collapse">
						      <div class="panel-body">
	      						<div class="table-responsive">
									<table class="table table-bordered table-striped">
									    <thead id="cardiacHeader">
									    </thead>
									    <tbody id="cardiacBody">
									    </tbody>
									  </table>
								</div>
						      </div>
						    </div>
						  </div>

						  <!-- 2nd Panel -->
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapse2"><button class="btn btn-link accBtnHarmone" type="button ">HARMONE MARKER</button></h4>
						    </div>

						    <div id="collapse2" class="panel-collapse collapse">
						      <div class="panel-body">
						      	  <div class="table-responsive">
									  <table class="table table-bordered table-striped">
									    <thead id="harmoneHeader">
								        </thead>
								        <tbody id="harmoneBody">
								        </tbody>
									  </table>
								  </div>
						      </div>
						    </div>
						  </div>

						  <!-- 3rd Panel -->
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapse3"><button class="btn btn-link accBtnInfection" type="button ">INFECTION MARKER</button></h4>
						    </div>

						    <div id="collapse3" class="panel-collapse collapse">
						      <div class="panel-body">
	      						<div class="table-responsive">
									  <table class="table table-bordered table-striped">
									    <thead id="infectionHeader">
								        </thead>
								        <tbody id="infectionBody">
								        </tbody>
									  </table>
								</div>
						      </div>
						    </div>
						  </div>

						  <!-- 4th Panel -->
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapse4"><button class="btn btn-link accBtnDiabetic" type="button ">DIABETIC MARKER</button></h4>
						    </div>

						    <div id="collapse4" class="panel-collapse collapse">
						      <div class="panel-body">
	      						<div class="table-responsive">
									  <table class="table table-bordered table-striped">
									    <thead id="diabeticHeader">
								        </thead>
								        <tbody id="diabeticBody">
								        </tbody>
									  </table>
								</div>
						      </div>
						    </div>
						  </div>

						  <!-- 5th Panel -->
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapse5"><button class="btn btn-link accBtnRespiratory" type="button ">RESPIRATORY MARKER</button></h4>
						    </div>

						    <div id="collapse5" class="panel-collapse collapse">
						      <div class="panel-body">
	      						<div class="table-responsive">
									  <table class="table table-bordered table-striped">
									    <thead id="respiratoryHeader">
								        </thead>
								        <tbody id="respiratoryBody">
								        </tbody>
									  </table>
								</div>
						      </div>
						    </div>
						  </div>

						  <!-- 6th Panel -->
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapse6"><button class="btn btn-link accBtnVector" type="button ">VECTOR BORNE MARKER</button></h4>
						    </div>

						    <div id="collapse6" class="panel-collapse collapse">
						      <div class="panel-body">
	      						<div class="table-responsive">
									  <table class="table table-bordered table-striped">
									    <thead id="vectorBorneHeader">
								        </thead>
								        <tbody id="vectorBorneBody">
								        </tbody>
									  </table>
								 </div>
						      </div>
						    </div>
						  </div>

						  <!-- 7th Panel -->
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapse7"><button class="btn btn-link accBtnFecal" type="button ">FAECAL MARKER</button></h4>
						    </div>

						    <div id="collapse7" class="panel-collapse collapse">
						      <div class="panel-body">
	      						<div class="table-responsive" >
									  <table class="table table-bordered table-striped">
									    <thead id="fecalHeader">
								        </thead>
								        <tbody id="fecalBody">
								        </tbody>
									  </table>
								 </div>
						      </div>
						    </div>
						  </div>

					   </div>




                     <div class="user-block" style="padding-top: 10px; padding-bottom: 15px;">
                         <!--   <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image"> -->
                         <h6 style="font-size:16px;color:#3a86bd;">Electrocardiogram (ECG): </h6>
                         <!--
                        <span class='username' style="margin-left: 0px;font-size:16px;color:#3a86bd;">
                          <a href="#" style="font-size:16px;color:#3a86bd;">Electrocardiogram (ECG)</a>
                          <a href='#' class='pull-right btn-box-tool'></a>
                          -->
                        </span>
                        <!-- <span class='description' style="margin-left: 0px; padding-top: 10px; padding-left: 5px;" id="addDates">NO RECORD FOUND</span> -->
                     </div><!-- /.user-block -->

                  <div class="row" id="addEcgs">
                  </div>

                  </div><!-- /.tab-pane -->


                  <div class="tab-pane" id="timeline">

                    	<div class="box box-primary">
			                <div class="box-header">
			                	<!-- /<h3 class="box-title">PRESCRIPTION</h3> -->
			                	<b style="font-size:12px">PRESCRIPTION</b>
			                	<button class="pull-right btn-primary btn dcbutton" onclick="openMedicationModal();">Add Prescription</button>
			                </div><!-- /.box-header -->
			                <div class="box-body no-padding">
			                	<table class="table table-striped">
			                    	<tr>
			                      		<th style="width: 15%"></th>
			                      		<th style="width: 15%"></th>
			                      		<th style="width: 15%"></th>
			                      		<th style="width: 15%"></th>
			                      		<th style="width: 15%"></th>
			                      		<th style="width: 15%"></th>
			                      		<th style="width: 2%"></th>
			                    	</tr>
			                    	<tbody id="prescriptionTableBody"></tbody>

			                  	</table>
			                </div><!-- /.box-body -->
			             </div>


			              <div class="box box-primary">
			                <div class="box-header">
			                	<b style="font-size:12px">INVESTIGATION</b>
			                	<button class="pull-right btn-primary btn dcbutton" onclick="openAdviceModal();">Add Advice</button>
			                </div><!-- /.box-header -->
			                <div class="box-body no-padding">
			                	<table class="table table-striped">
			                    	<tr>
			                      		<th style="width: 20%"></th>
			                      		<th style="width: 20%"></th>
			                      		<th style="width: 20%"></th>
			                      		<th style="width: 40%"></th>
			                      		<th style="width: 2%"></th>
			                    	</tr>
			                    	<tbody id="adviseTableBody"></tbody>

			                  	</table>
			                </div><!-- /.box-body -->
			             </div>



			             <div class="box box-primary">
			                <div class="box-header">
			                	<b style="font-size:12px">NOTES</b>
			                	<button class="pull-right btn-primary btn dcbutton" onclick="openNotesModal();">Add Notes</button>
			                </div><!-- /.box-header -->
			                <div class="box-body no-padding">
			                	<table class="table table-striped">
			                    	<tr>
			                      		<th style="width: 20%"></th>
			                      		<th style="width: 50%"></th>
			                      		<th style="width: 10%"></th>
			                      		<th style="width: 20%"></th>
			                      		<th style="width: 2%"></th>
			                    	</tr>
			                    	<tbody id="notesTableBody"></tbody>

			                  	</table>
			                </div><!-- /.box-body -->
			             </div>

			              <div class="box box-primary">
			                <div class="box-header">
			                	<b style="font-size:12px">REPORT</b>
			                	<button class="pull-right btn-primary btn" onclick="openReportModal();">Add Report</button>
			                </div><!-- /.box-header -->
			                <div class="box-body no-padding">
			                	<table class="table table-striped">
			                    	<tr>
			                      		<th style="width: 100%"></th>
			                      		<!-- <th style="width: 60%"></th>
			                      		<th style="width: 10%"></th>
			                      		<th style="width: 20%"></th> -->
			                    	</tr>
			                    	<tbody id="reportsTableBody"></tbody>

			                  	</table>
			                </div><!-- /.box-body -->
			             </div>

	                 </div><!-- /.tab-pane -->


                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer"
				style="display:block;padding-right:15px; margin-left:0% !important;position: fixed;bottom: 0px;margin-right: auto;margin-left: auto;width:100%;">
			<!--  <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>.</strong> All rights reserved. -->
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0.0
			</div>
			Copyright &copy; 2021-2022 <a href="http://www.wrizto.com/" target=_blank style="color: rgb(60, 141, 188);">Wrizto Healthcare Pvt. Ltd.</a> All rights reserved. </footer>
      <!-- Control Sidebar -->

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->






       <div id="addMedicationModal" name="addMedicationForm" class="modal modal-fullscreen fade" style="height:100%;padding-top: 0px;z-index:9999" role="dialog"   data-keyboard="false">
       		<form id = "saveMedicationId" method="post" enctype ='multipart/form-data' style="height:100%;">
       				<input type="hidden" name="additionalDetailJson" id="additionalDetailJson">
                    <input type="hidden" name="strPrescribedDate" id="strPrescribedDate">
                   <!--  <input type="hidden"  id="serviceNameValue" name="serviceName" value=""> -->
                    <input type="hidden" name="documentTypeId" id="documentTypeId" value="">
                    <input type="hidden" name="clinicId" id="clinicId" >
                    <input type="hidden" name="userId" id="idUser" value="">
                    <input type="hidden" name="ccpId" id="idCcp" value="">
                    <input type="hidden" name="prescribedBy" class ="prescribedBy" id="prescribedBy" value="">
		 			<input type="hidden" id="medEpisodeId"  class="episodeId" name="episodeId" value="">
		 			<input type="hidden" id="medicationDetailId"  class="medicationDetailId" name="medicationDetailId" value="">
		 			<input type="hidden" id="strPrescribedDate"  class="strPrescribedDate" name="strPrescribedDate" value="">
		 			<input type="hidden"  id="medicalReportId" name="medicalReportId" value="-1">
		 			<input type="file" style="display:none;" name="files" id="reportFileId" class="getReport" onchange="setFileName(this)"></input>

		 			<input type="hidden"  id="deletedRecords" name="deletedRecords" value="">
		 			<input type="hidden"  id="loggedIncreatedBy" name="createdBy" value="">
		     		<input type="hidden"  id="documentTypeName" name="documentTypeName" value="Prescription">
					<input type="hidden"  id="documentTypeName" name="documentTypeName" value="Prescription">
					<input type="hidden" id="medicationIdFromDropdown" name="medicationHiddenId">

		 			<input type="hidden" id="" name="laboratoryName" value="">
		 			 <input class="getAdvice" type="hidden" name="laboratoryId" value="">

		 			 <input type="hidden" id="noteComments" name="comments" value="">
		 			 <input type="hidden" name="dischargeStatus" value="1">


         		<div class="modal-dialog" style="padding-top: 0px;height:100%;" >
					<div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
						<div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
							<h5 class="modal-title" >
							<b style="color:White">Medication</b></h5>
						</div>
						<div class="modal-body" style="background: white;padding-bottom: 0px;padding-top: 0px;;overflow-x:hidden;height:450px;">
							<div class="col-xs-12">
								<div class="col-xs-6 "
										style="margin-top: 0px; padding-left: 0px;">
										<div class="col-xs-12"
											style="padding-left: 0px; padding-right: 0px;">
											<div style="width: 100%; color: black" id ="isDirtyId"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
												<input spellcheck="false" autocomplete="off" autocorrect="off"
													class="mdl-textfield__input error-popover bClass Newpolic4"
													type="text" id="prescribedByPre" name="prescribedBy"
													maxlength="100" style="text-transform: capitalize;"> <label class="mdl-textfield__label"
													for="sample3">Prescribed By<i style="color: red">*</i>
												</label>
											</div>
										</div>
								</div>

								<div class="col-xs-6"  style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input autocomplete="off" autocorrect="off" spellcheck="false"
											class="mdl-textfield__input error-popover clearAll bClassM Newpolic1"
											autocomplete="off" type="text" id="medicationNameId" name="medicationName"
											maxlength="100" style="text-transform: capitalize;margin-bottom: 8px; ">
											<label class="mdl-textfield__label" for="sample3">Medication Name
											<span style="color:red;">*</span>
											</label>

										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-6"
									style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black;margin-bottom: 8px"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown" >
											<input autocomplete="off" autocorrect="off"
												spellcheck="false"
												class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="text" id="dosageID" name="dosage"
												maxlength="100"> <label class="mdl-textfield__label"
												for="sample3">Dosage MG/ML/MM
											</label>
										</div>
									</div>
								</div>

								<div style="padding-left: 0px;" class="col-xs-6 ">
									<p style="margin-bottom: 0px; color: royalblue">Select Frequency</p>
										<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
											<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
												<select class="form-control" id="fequencyId" name="frequency" style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
												<option>0-0-1</option>
												<option>0-1-0</option>
												<option>1-0-1</option>
												<option>1-1-0</option>
												<option>1-1-1</option>
												<option>1-0-0</option>
												</select>
											</div>
										</div>
								</div>

							</div>
							<div class="col-xs-12">
								<div style="padding-left: 0px;" class="col-xs-6 ">
									<p style="margin-bottom: 0px; color: royalblue">Instruction</p>
										<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
											<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
												<select class="form-control" id="instructionId" name="instruction" style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
												<option>Before Food</option>
												<option>After Food</option>
												</select>
											</div>
										</div>
								</div>

		            			<div class="col-xs-6"
									style="margin-top: 0px; padding-left: 0px; margin-bottom: 17px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input autocomplete="off" autocorrect="off"
												spellcheck="false"
												class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="text" id="startId" name="strBeginDate"
												maxlength="100"> <label class="mdl-textfield__label"
												for="sample3">DD/MM/YY(Start Date)<i style="color: red">*</i>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-6"
									style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input autocomplete="off" autocorrect="off"
												spellcheck="false"
												class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="number" id="days" name="days"
												maxlength="100"> <label class="mdl-textfield__label"
												for="sample3">Days<i style="color: red">*</i>
											</label>
										</div>
									</div>
								</div>

								<div class="col-xs-6"
									style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<input autocomplete="off" autocorrect="off"
												spellcheck="false"
												class="mdl-textfield__input error-popover bClassM Newpolic1"
												type="text" id="commentsMedId" name="comments"
												maxlength="100"> <label class="mdl-textfield__label"
												for="sample3">Comments<!-- <i style="color: red">*</i> -->
											</label>
										</div>
									</div>
								</div>
							</div>

								<!-- <div class="col-xs-12"
										style="margin-top: 0px; margin-bottom: 20px; padding-left: 0px;display:inline-flex; height: 35px">
										<div class="col-xs-8"
											style="padding-left: 0px; padding-right: 0px;">
											<div style="color: black;display:inline-flex;"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
												<i style="padding-top: 5px;margin-left: 0px;padding-right: 10px" class="fa fa-paperclip"></i>
												<input type="file" style="margin-bottom: 0px;
												width:100%; border-radius: 4px; font-family:Roboto Medium;overflow:hidden;
												font-size:15px;color:#5498D2;border:#5498D2 1px solid"
												name="files" id="firstmedfile" onchange="acceptType(this);">
											</div>
										</div>

									<div style="color: black"
										class="col-xs-4 mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">

										<button type="button" onclick="getOtherMedFile()"
											style="width: 45%;float:left; background: none repeat scroll 0% 0% transparent; color: #5498D2 !important; border-radius: 4px; font-family: Roboto Medium; font-size: 11px; border: 1px solid #5498D2; margin-bottom: 0px; margin-top: -1px; padding: 0px; height: 29px; margin-top: 0px;"
											class="btn right">Upload More</button>

									</div>
								</div> -->
								<div class="col-xs-12 addedMedTable" id="dynTableMed" style="display:none;color:black;">
								<table class="table" >
									<tr>
									<!-- <th >id</th> -->
										<th>Name</th>
										<th>Dosage</th>
										<th>Freq</th>
										<th>Inst</th>
										<th>Start Date</th>
										<th>Days</th>
										<th>Comments</th>
										<th></th>

									</tr><tbody id="medAdd"></tbody>
								</table>
							</div>
						</div>
						<div id="addFilesMed">

                        </div>

						<div class="modal-footer text-center" style="padding: 5px;overflow-x:hidden">

							<div class="col-xs-12" style="position: absolute;bottom: 0;">
								<div class="text-center">
									<button type="button" class="btn btn-danger" style="width: 20%;margin-bottom: 6px;" onclick= "canMed();">Cancel</button>
									<button type="button" class="btn btn-warning" id="addMedicationButton" style="width: 20%; margin-left: 6px; margin-bottom: 6px;" onclick="addMedication();" >ADD</button>
									<button type="button" class="btn btn-success" id="saveMedicationButton" style="width: 20%; margin-left: 6px; margin-bottom: 6px;" onclick="saveMedication(1);" >Save</button>
									<button type="button" id="updateMedicationButton" class="btn btn-success" style="width: 20%; display: none;">Update</button>
								</div>
							</div>
						</div>
					</div>
				</div>
	  	 	</form>
    	</div>

         <div id="addVisitForm" class="modal fade main-fade-modal " role="dialog"  data-backdrop="static" data-keyboard="false">
  <form id = "saveAdviceId" method="post" enctype ='multipart/form-data' >
  <input class="getAdvice" type="hidden" name="documentTypeId" id="documentTypeIdAdv" value="2">
   <input class="getAdvice" type="hidden" name="documentTypeName" id="documentTypeName" value="Advice">
   <input class="getAdvice" type="hidden" name="serviceTypeIdFromDropdown" id="serviceTypeIdFromDropdown" value="Advice">
    <input class="getAdvice" type="hidden" name="investigationId" id ="investigationId">
    <input type="hidden" name="additionalDetailJson" id="adviceJSON">
   <!--  <input class="getAdvice" type="hidden"  id="serviceNameId" name="servicesName" > -->
    <input class="getAdvice" type="hidden"  value="services" name="InvestigationType" >
    <input class="getAdvice" type="hidden"  value="" name="comments" id="advice" >
    <input type="hidden" id="advEpisodeId"  class="episodeId" name="episodeId" value="">
    <input type="hidden" name="prescribedBy" class ="prescribedBy" id="prescribedByAdvice" value="">
    <input type="hidden" id="advUserId"  class="userId" name="userId" value="">
    <input type="hidden" id="strAdviceDate"  class="strPrescribedDate" name="strPrescribedDate" value="">



    <!-- <input type="hidden"  value="30/03/2018" name="strPrescribedDate" > -->

       <div class="modal-dialog"  id="visitModalId">
		<div class="modal-content" style="border-radius: 4px;">
			<div class="modal-header" style="background-color:#5498D2;">
			<h5 class="modal-title" ><b style="color:White">Advice</b></h5>
			</div>
			<div class="modal-body"  style="padding-top: 0px;">
 			<div class="col-xs-12" style="margin-top: 0px; padding-left: 0px;" >



 			<div class="col-xs-12 Newpolic3"
				style="margin-top: 0px; padding-left: 0px;">
				<div style="padding-left: 0px;" class="col-xs-12 ">

				<div style="width: 100%; color: black;"
							class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
						<input autocomplete="off" autocorrect="off"
										spellcheck="false" style="margin-bottom:8px;"
										class="mdl-textfield__input error-popover bClass Newpolic2 getAdvice"
										type="text" id="serviceTypeId" name="investigationName"
										maxlength="100"> <label class="mdl-textfield__label"
										for="sample3">Service Name<i style="color: red">*</i>
								</label>



					</div>
						<!-- <p style="margin-bottom: 0px; color: royalblue" id="masterServiceList">Service<i style="color: red">*</i></p>
							<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
								<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
									<select class="form-control getAdvice" id="serviceTypeId" name="serviceTypeId"
									style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;padding-bottom: 7px;">

									</select>
								</div>
							</div> -->
					</div>
			</div>
			<div class="col-xs-12 Newpolic3"
					style="margin-top: 0px; padding-left: 0px;">
					<div class="col-xs-12"
						style="padding-left: 0px; padding-right: 0px;">
						<div id="addadvclassprenameID" style="width: 100%; color: black"
							class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
							<input autocomplete="off" autocorrect="off"
								spellcheck="false"
								class="mdl-textfield__input error-popover getAdvice"
								type="text" id="prescribedByAdv" name="prescribedBy"
								maxlength="100"> <label class="mdl-textfield__label"
								for="sample3">Adviced By<i style="color: red">*</i>
							</label>
						</div>
					</div>
				</div>
			<div class="col-xs-12 Newpolic3"
									style="margin-top: 0px; padding-left: 0px;">
				<!-- <div class="col-xs-6 "
							style="margin-top: 0px; padding-left: 0px;"> -->
							<div class="col-xs-12"
								style="padding-left: 0px; padding-right: 0px;">
								<div style="width: 100%; color: black"
									class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
								<input autocomplete="off" autocorrect="off"
										spellcheck="false"
										class="mdl-textfield__input error-popover getAdvice"
										type="text" id="fromDate" name="dateTime"
										maxlength="100"> <label class="mdl-textfield__label"
										for="sample3" style="margin-bottom: 0px;">Date (DD/MM/YY)<i style="color: red">*</i>
									</label>

							</div>
						<!-- </div> -->
					</div>

				</div>

			<div class="col-xs-12 Newpolic3"
									style="margin-top: 0px; padding-left: 0px;">
									<div class="col-xs-12"
										style="padding-left: 0px; padding-right: 0px;">
										<div style="width: 100%; color: black"
											class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
											<textarea autocomplete="off" autocorrect="off"
												spellcheck="false" placeholder="Advice"
												class="mdl-textfield__input error-popover getAdvice"
												 id="advComments" name="Labcomments"
												maxlength="100"></textarea>
										</div>
									</div>
								</div>

			</div>
			<div class="modal-footer text-center">

				<!-- <button id="demo-show-snackbar" class="mdl-button mdl-js-button mdl-button--raised" style="display:none" type="button"></button> -->
				<button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 25%; margin-left: 5px; margin-bottom: 0px;" onclick="clearAdvModal()">Cancel</button>
				<!-- <button type="button" class="btn btn-success" id="otpButton" onclick="generateOtp()">Generate OTP</button> -->
				<!-- <button type="button" class="btn btn-warning" id="addAdviceButton" style="width: 20%;" onclick="addVisit();" >ADD</button> -->
				<button type="button" class="btn btn-success" id="saveAdviceButton" style="width: 25%;" onclick="saveMedication(2);" >Save</button>
				<button type="button" id="updateAdviceButton" class="btn btn-success" style="width: 20%; display: none;">Update</button>
			</div>
		</div>
	</div>
  </div>
</form></div>


        <!-- Observation Modal  -->

        <!-- Notes Modal  -->

    	<div class="example-modal" >
            <div class="modal" id="addNotesModal">
            	<div class="modal-dialog">
                	<div class="modal-content">
                		<form id="addNotesForm" method="post" enctype ='multipart/form-data'>
                		<input type="hidden" name="additionalDetailJson" id="notesJSON" >

                    <input type="hidden" name="documentTypeId" value="8">
                     <input type="hidden" name="clinicId" id="clinicIdNote" >
                     <input type="hidden" name="userId" id="idUserNote" value="">
                     <input type="hidden" name="ccpId" id="idCcpUP" value="">
                     <input type="hidden" name="notesId" id="notesId" value="">
		 <input type="hidden"  class="episodeId" name="episodeId" value="" id="episodeIdUp">
		 <input type="hidden"  id="medicalReportId" name="medicalReportId" value="-1">

		  <input type="hidden"  name="deletedFiles" value="">
		 <input type="hidden"   name="deletedRecords" value="">
		 <input type="hidden"  name="episodeName" value="CCP">
		 <input type="hidden"   name="dischargeStatus" value="1">
		  <input type="hidden"   name="strPrescribedDate" value="30/03/2018">
		<input type="hidden"  name="documentTypeName" value="Notes">

	                  		<!-- <div class="modal-header">
	                    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    		<span aria-hidden="true">&times;</span></button>
	                    		<h4 class="modal-title">Add Notes</h4>
	                  		</div> -->
	                  		<div class="modal-header" style="background-color:#5498D2;">
								<h5 class="modal-title" ><b style="color:White">Notes</b></h5>
							</div>
	                  		<div class="modal-body">
	                    		<textarea style="width:100%;height:100px;" id="noeComments" class="getNotes" name="comments" maxlength="160"></textarea>

	                  		</div>
	                  		<div class="modal-footer">
	                    		<button type="button" class="btn btn-danger" style="width: 25%;" onclick="canNote();">Cancel</button>
	                    		<button type="button" id="saveNotesButton" class="btn btn-success" style="width: 25%;" onclick="saveMedication(3);">Save</button>
	                    		<button type="button" id="updateNotesButton" class="btn btn-success" style="width: 20%; display: none;">Update</button>
	                  		</div>
	                  	</form>
                	</div><!-- /.modal-content -->
              	</div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
        <div class="example-modal" >
            <div class="modal" id="addReportModal">
            	<div class="modal-dialog">
                	<div class="modal-content">
                		<form id="addReportForm" method="post" enctype ='multipart/form-data'>

	                  		<!-- <div class="modal-header">
	                    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    		<span aria-hidden="true">&times;</span></button>
	                    		<h4 class="modal-title">Add Notes</h4>
	                  		</div> -->
	                  		<div class="modal-header" style="background-color:#5498D2;">
								<h5 class="modal-title" ><b style="color:White">Report</b></h5>
							</div>
	                  		<div class="modal-body">
	                    		<div class="col-xs-12"
										style="margin-top: 0px; margin-bottom: 20px; padding-left: 0px;display:inline-flex; height: 35px">
										<div class="col-xs-12"
											style="padding-left: 0px; padding-right: 0px;">
											<div style="color: black;display:inline-flex;width:95%"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
												<i style="padding-top: 5px;margin-left: 0px;padding-right: 10px" class="fa fa-paperclip"></i>

												<button class="btn" type="button" onclick="getElementById('reportFileId').click()" style="border:1px solid #5498d2">Browse</button>
												<span style="width:80%;height:34px;border:1px solid #5498d2;padding-top: 6px; padding-left: 12px;" id="showReportFileName"></span>

											</div>
										</div>

									</div>
								</div>
	                  		<div class="modal-footer" style="margin-top: 15%;">
	                    		<!-- <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button> -->
	                    		<button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 25%;" onclick="clearRep();">Cancel</button>
	                    		<button type="button" class="btn btn-success" style="width: 25%;" onclick="saveMedication(4);">Upload</button>
	                  		</div>
	                  	</form>
                	</div>
              	</div>
            </div>
        </div>
        </div>
    </body>


<script>

//var imagePath = "http://192.168.1.45:8080/image/phc/";

 var episdeId =  "";
 var userId = "";
 var episodeId =  "";
 var loggedInUserId = '';
 var productId = '';
 var appointmentId = '';
 var test=3;
 var vitalMap = new Map();
 var patGender='', patAge='';
 var prescriptionDetails = ''

//var patientId =  localStorage.getItem("patientId");
var patientId =  localStorage.getItem("patientId");

$(document).ready(function() {

if( localStorage.getItem("userGroupCode") === 'NUR')
           $("#patListLogout").hide();

if( localStorage.getItem("userGroupCode") === 'NUR')
        $("#timeline").hide();

	//alert(episodeId);alert(episdeId);
	getPatientDetails();
	//getVitalDocument(-1);
	episdeId = "";
	if(isDuplexCall==true){

		$('#dashBoardHideShoeVideoIconId').hide();
		var timeoutId = 0;
		var profileID=localStorage.getItem("profileId");
		var	serverKeyId=localStorage.getItem("serverKeyId");
		loggedInUserId = localStorage.getItem("loggedInUserId");
		appointmentId = localStorage.getItem("appointmentId");
		//alert(appointmentId);
		 var ajaxfn= function name() {
			/* $.ajax({
					type : "POST",
					url :   serverPath + "/checkForCallv2?serverKeyId="+serverKeyId+"&profileId="+profileID+"&userId="+loggedInUserId,
					headers: {
					       "deviceId":"ADMIN",
					   	 },
					success : function(data){
						if(data.errorCode=="1011"){
							//alert("1001");nurseNameForActiveId
							$('#showHideVideoIconId').hide();
							$('#videoIconId').hide();
							$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
						}else if(data.code=="200"){
							if(data.activeUsers!=""){

								$.each(data.activeUsers,function(index,option){
									$('#nurseNameForActiveId').html(" "+option.name+" ");
									$('#showHideVideoIconId').show();
									$('#videoIconId').show();
								});

							}
							else{
								$('#showHideVideoIconId').show();
								$('#videoIconId').hide();
								$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
							}

							if(data.content != null) {
								var callerName = data.content.name;
								var roomName = data.content.roomName;
								var roomId = data.content.id;
								confPopUp(callerName, roomName, roomId);
							}
						}



						timeoutId= setTimeout(ajaxfn,3000);
					},error:function(x,y,response){
						$('#showHideVideoIconId').hide();
						$('#nurseNameForActiveId').html("<font color='gray'>No Active User</font>");
						timeoutId= setTimeout(ajaxfn,3000);

					}

			}); */

			 var name = '';var activeList = '';
 			$.ajax({
 			   type: "POST",
 			   headers:{"deviceId":"ADMIN"},
 			   url: serverPath + "/checkForCallv4?serverKeyId="+serverKeyId+"&profileId="+profileID+"&userId="+loggedInUserId,
 			   success : function(data){
 				   if(data.code == '200'){
 					   $("#activeUsersId").html('');
 					   name = '';activeList = '';
 					   if(data.activeUsers!=""){
 						   //alert();
 				$.each(data.activeUsers,function(index,option){

 							   if(option.name != null){
 								   name = option.name;								   ;
 							   }
 				activeList+='<div class="form-control" style="border-radius:6px;padding-right: 5px;padding-left: 5px;padding-top: 3px;padding-bottom:6px" ><div class="col-xs-9" style="padding-left:0px;padding-right:0px;padding-top:5px;"><span style="font-size: 12px;text-transform: capitalize;margin-left: 0px;" id="nurseNameForActiveId">'+name+'</span></div> '+
                 '<div class="col-xs-3 text-right" style="padding-left:0px;padding-right:0px;"> <a id= "videoIconId" style="border-radius: 50%;margin-left: 5px;float:right;background-color: green" class="btn btn-primary" title="videoCall" onclick="initiateCall('+option.profileId+')"><b> '+
 				' <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a></div></div> ';

 				$("#nurseNameForActive").html(activeList);


 						   });

 					   }else{
 							//$('#showHideVideoIconId').show();
 							//$('#videoIconId').hide();
 							$("#nurseNameForActive").html("<li class='form-control' style='border-radius:6px;color:gray;'>No Active User</li>");
 						}

 					/*if(data.content != null) {
						var callerName = data.content.name;
						var roomName = data.content.roomName;
						var roomId = data.content.id;
						confPopUp(callerName, roomName, roomId);
					} */


                                              if(data.content != null) {

						var callerName = data.content.name;
						var roomName = data.content.roomName;
						var roomId = data.content.id;
						if(data.weconDetails != null) {
							var weconUrl = data.weconDetails.weconurl;
						confPopUp(callerName, roomName, roomId, weconUrl);
						}
					}


 				   }
			   timeoutId= setTimeout(ajaxfn,3000);

		   },error:function(x,y,response){
				//$('#showHideVideoIconId').hide();
				$("#nurseNameForActive").html("<li class='form-control' style='border-radius:6px;color:gray;'>No Active User</li>");
				timeoutId= setTimeout(ajaxfn,3000);

			}
 			});
		}
		 ajaxfn();
	}else{
		$('#showHideVideoIconId').hide();
	}


	 var loggedInUserFullName = localStorage.getItem("loggedInUserFullName");
		loggedInUserId = localStorage.getItem("loggedInUserId");
     $("#loggedInUserFullName").text(loggedInUserFullName);
	 $("#loggedInUserFullNameSpan").text(loggedInUserFullName);

	 $("#loggedIncreatedBy").val(loggedInUserId);
	 $("#idUser").val(loggedInUserId);
	 $("#idUserNote").val(loggedInUserId);

	productId = localStorage.getItem("productId");
	var userGroupCode =  localStorage.getItem("userGroupCode");
	if(userGroupCode=="DOC"){

		 $("#dash_id").text("Doctor DashBoard");
		 $(".dcbutton").show();
	}
	else{
		$("#dash_id").text("Nurse DashBoard");
		$(".dcbutton").hide();
	}

	$.ajax({
		type : "POST",
		url :   serverPath + "/getEpisodeList?patientId="+patientId,
		success : function(data){
if(data.code=="200"){

				var size = data.content.length;
				s=size;

				var appendText = "";
				$.each(data.content,function(index,option){
        			if(index == 0){
        				episdeId = option.id;
        				episodeId = option.id;
        				userId = option.userId;
        				time = option.created;
        			}
        			if(size>4){
             			if(index<4){
             				if(index==0){
             					appendText += '<button type="button" id="prevId" style="margin-left:10px;background:#B8B8B8;display:none;" class="btn btn-primary normalCol" onclick="previous('+option.id+');">'+"PREV"+'</button>'
             				}
             				appendText += '<button type="button" id="'+index+'" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="getHistoryOfUser('+option.id+','+index+');">'+option.episodeName+'</button>'
             			}
             			else{
             				appendText += '<button type="button" id="'+index+'" style="margin-left:10px;background:#B8B8B8; display:none;" class="btn btn-primary normalCol" onclick="getHistoryOfUser('+option.id+','+index+');">'+option.episodeName+'</button>'
             				if(index==size-1){
             					appendText += '<button type="button" id="nextId" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="next('+option.id+');">'+"NEXT"+'</button>'
             				}
             			}
		            }
		             else{
		          		appendText += '<button type="button" id="'+index+'" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="getHistoryOfUser('+option.id+','+index+');">'+option.episodeName+'</button>'
		             }
				});
				$("#chronologicalView").html(appendText);
				//$( "#chronologicalView button" ).first().css( "background-color", "#367fa9" );
				var listIndex = 0; tempListIndex=0;
				$("#"+listIndex).css("background","#367fa9");
				getMasterVitals();
				getVitalDocument(episdeId);
			}
		}
	});
	//alert(episdeId);

	 $('#addVisitForm').on('show.bs.modal', function (e) {

			$("#fromDate").mask("99/99/99", {});
		});
	 $('#addMedicationModal').on('show.bs.modal', function (e) {

			//$("#startId").mask("99/99/99", {});
			//$("#medAdd").html('');
			//$("#dynTableMed").css("display","none");
	  });

	 $('#startId').datepicker({
		 	autoclose: true,
		 	todayHighlight: true,
		 	//startDate: '-0m',
			format: "dd/mm/yyyy"
			}).on('changeDate', function(e){
				$("#startId").attr("readonly", true);
		    	$("#startId").parent().addClass('is-dirty');
				//$(this).datepicker('hide');
			});
	 $('#fromDate').datepicker({
	 	 autoclose: true,
	 	 todayHighlight: true,
		 format: "dd/mm/yyyy",
		}).on('changeDate', function(e){
			$("#fromDate").attr("readonly", true);
	    	$("#fromDate").parent().addClass('is-dirty');
			$(this).datepicker('hide');
		});

	 $('#serviceTypeId').typeahead({
			source: function(query, process) {


			 	 var $url = serverPath+'/masterServiceList?serviceType=services';
				 var $datas = new Array;
				 $datas = [""];
				 $.ajax({
					 url: $url,
					 headers: {
				       "deviceId":"ADMIN",
				   	 },
					 dataType: "json",
					 type: "GET",
					 success: function(data) {

						 $.map(data.content, function(data){
							 var group;
							 group = {
								 id: data.id,
								 name: data.value,

								 toString: function () {
								 	return JSON.stringify(this);
								 },
								 toLowerCase: function () {
								 	return this.name.toLowerCase();
								 },
								 indexOf: function (string) {
								 	return String.prototype.indexOf.apply(this.name, arguments);
								 },
								 replace: function (string) {
								 	var value = '';
									value += this.name;
									 if(typeof(this.name) != 'undefined') {
									 	value += ' <span class="pull-right muted">';
									 	value += this.name;
									 	value += '</span>';
									 }
									 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
								 }
							 };
							 $datas.push(group);
						 });
						 process($datas);
					 }
				 });
			 },
			 property: 'name',
			 items: 10,
			 minLength: 2,
			 updater: function (item) {
			 var item = JSON.parse(item);
			 $('#investigationId').val('');
				//alert(item.id);
			// $('#serviceTypeIdFromDropdown').val(item.id);
			 $('#investigationId').val(item.id);
			 /* $("#servicesName").val(item.name);
			 alert( $('#servicesName').val()); */
			 return item.name;
			}
		});

	$('#medicationNameId').typeahead({
		source: function(query, process) {


			 var searchParameter = $('#medicationNameId').val();
		 	 var $url = serverPath +'/getMediactionList?searchParameter='+searchParameter;
			 var $datas = new Array;
			 $datas = [""];
			 $.ajax({
				 url: $url,
				 dataType: "json",
				 type: "GET",
				 success: function(data) {
					   //console.log(data.content);
					 if(data.content!=undefined && data.content!='undefined'){
					 $.map(data.content, function(data){
						 var group;
						 group = {
							 id: data.id,
							 name: data.name,

							 toString: function () {
							 	return JSON.stringify(this);
							 },
							 toLowerCase: function () {
							 	return this.name.toLowerCase();
							 },
							 indexOf: function (string) {
							 	return String.prototype.indexOf.apply(this.name, arguments);
							 },
							 replace: function (string) {
							 	var value = '';
								value += this.name;
								 if(typeof(this.name) != 'undefined') {
								 	value += ' <span class="pull-right muted">';
								 	value += this.name;
								 	value += '</span>';
								 }

								 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
							 }
						 };
						 $datas.push(group);
					 });}
					   else{
						   $datas = [""];
						   }
					 process($datas);
				 }
			 });
		 },
		 property: 'name',
		 items: 10,
		 minLength: 2,
		 updater: function (item) {
		 var item = JSON.parse(item);
		 $('#medicationIdFromDropdown').val('');
			//alert(item.id);
		 $('#medicationIdFromDropdown').val(item.id);
		 return item.name;
		}
	});


	 $('#serviceNameId').typeahead({
			source: function(query, process) {


				 var searchParameter = $('#serviceNameId').val();
			 	 var $url = 'getServicesList?searchParamater='+searchParameter;
				 var $datas = new Array;
				 $datas = [""];
				 $.ajax({
					 url: $url,
					 dataType: "json",
					 type: "GET",
					 success: function(data) {
						   //console.log(data.content);
						 if(data.content!=undefined && data.content!='undefined'){
						 $.map(data.content, function(data){
							 var group;
							 group = {
								 id: data.id,
								 name: data.name,

								 toString: function () {
								 	return JSON.stringify(this);
								 },
								 toLowerCase: function () {
								 	return this.name.toLowerCase();
								 },
								 indexOf: function (string) {
								 	return String.prototype.indexOf.apply(this.name, arguments);
								 },
								 replace: function (string) {
								 	var value = '';
									value += this.name;
									 if(typeof(this.name) != 'undefined') {
									 	value += ' <span class="pull-right muted">';
									 	value += this.name;
									 	value += '</span>';
									 }

									 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
								 }
							 };
							 $datas.push(group);
						 });}
						   else{
							   $datas = [""];
							   }
						 process($datas);
					 }
				 });
			 },
			 property: 'name',
			 items: 10,
			 minLength: 2,
			 updater: function (item) {
			 var item = JSON.parse(item);
			 $('#serviceFromDropdown').val('');
			 $('#serviceFromDropdown').val(item.id);
			 return item.name;
			}
		});

}); //END of READY

/*function confPopUp(callerName, roomName, roomId) {
	var room = encodeURIComponent(roomName);
	var r = confirm( callerName + " is calling you. Do you accept the call?");
	if(r== true){
		var win = window.open('intiateAndCloseCallv4?roomName='+room, 'result', 'width=1024,height=768');
		win.addEventListener("resize", function(){
			win.resizeTo(1024, 768);
         });
	}
	$.ajax({
		type : "POST",
		url :   serverPath + "/intiateAndCloseCallv4?initiate=false&roomId="+roomId,
		success : function(data){
		}
	});
	}*/


  function confPopUp(callerName, roomName, roomId, weconUrl) {
    	var room = encodeURIComponent(roomName);
    	videocalllink =  weconUrl + roomId ;
    	var win;
    	var r = confirm( callerName + " is calling you. Do you accept the call?");
    //	alert(r);
    	if(r== true){
    		 win = window.open(videocalllink,'result', 'width=300,height=300');

    		 /* win.addEventListener("resize", function(){
     	 		win.resizeTo(1024, 768);
     	 	}); */
    	}
    	$.ajax({
    		type : "POST",
    		url :   serverPath + "/intiateAndCloseCallv4?initiate=false&roomId="+roomId,
    		success : function(data){
    			//win.close();
    		}
    	});
    	}


function refreshPage(){
	location.reload();
}

function refreshVitalsPage(){
	$("#addVitalsDet").empty();
	//location.reload();
	getMasterVitals(false);
	getEpisodeVitals(episdeId);
	getVitalDocument(episdeId);
}

function sendPics(){

	/*
	var form1 = document.getElementById('saveMedicationId');
		 var formData = new FormData(form1);

		$.ajax({
			type : "POST",
			url : "saveVitalDocument",
			headers: {
		        "deviceId":"ADMIN",
		    },

			data: formData,
			dataType:"json",
		    mimeType: "multipart/form-data",
			async : false,
		    cache : false,
			contentType : false,
		    processData : false,
			success : function(data){

			}
		}); */
	}

function clearAdvModal(){
	$("#addVisitForm").modal('hide');
	$("#serviceTypeId").val('1');
	$('.getAdvice').val('');
	$('.getAdvice').parent().removeClass('is-dirty');
}
	function getMasterVitals(callEpisodeVitalAPI = true){
		var addVitals = "";

		$('#load2').show();
		$.ajax({
			type : "POST",
			url : serverPath + "/getMasterVitals",
			headers: {
		        "deviceId":"ADMIN",
		    },
		success : function(data){
				if(data.code=="200"){
					$.each(data.content,function(index,option){
            //console.log("CODE-> ",option.code);

            var masterVitalName = option.name.toLowerCase();
            //console.log("NAME-> ",option.name,", LC=",option.name.toLowerCase());

            if(option.code == "Temp"){
            	addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pTemp">  <div class="card-main-conatiner">'+
            	'<p class="vital-title">TEMPERATURE</p>'+
            	'<div class="card-scroll-container"> <div class="card-inner" id="tempVitId"> </div>'+
            	'</div> </div> </div>';
            	vitalMap.set(masterVitalName, "pTemp");

           }
          if(option.code == "PUL"){
        	  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pPUL">  <div class="card-main-conatiner">'+
        	  '<p class="vital-title">PULSE</p>'+
        	  '<div class="card-scroll-container"> <div class="card-inner" id="pulseVitId"> </div>'+
        	  '</div> </div> </div>';
        	  vitalMap.set(masterVitalName, "pPUL");
    	  }
	      if(option.code == "SPO2"){
	    	addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pSPO2">  <div class="card-main-conatiner">'+
	    	'<p class="vital-title">SPO2</p>'+
	    	'<div class="card-scroll-container"> <div class="card-inner" id="spo2VitId"> </div>'+
	    	'</div> </div> </div>';
	    	vitalMap.set(masterVitalName, "pSPO2");
          }
	      if(option.code == "BP"){
	    	addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pBP">  <div class="card-main-conatiner">'+
	    	'<p class="vital-title">BLOOD PRESSURE</p>'+
	    	'<div class="card-scroll-container"> <div class="card-inner" id="bpVitId"> </div>'+
	    	'</div> </div> </div>';
	    	vitalMap.set(masterVitalName, "pBP");
          }

          if(option.code == "BG"){
	    	  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pBG">  <div class="card-main-conatiner">'+
	    	  '<p class="vital-title">BLOOD GLUCOSE</p>'+
	    	  '<div class="card-scroll-container"> <div class="card-inner" id="bldGluVitId"> </div>'+
	    	  '</div> </div> </div>';
	    	  vitalMap.set(masterVitalName, "pBG");
          }

	     if(option.code == "WEI"){
	    	addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pWEI">  <div class="card-main-conatiner">'+
	    	'<p class="vital-title">WEIGHT</p>'+
	    	'<div class="card-scroll-container"> <div class="card-inner" id="weightVitId"> </div>'+
	    	'</div> </div> </div>';
	    	vitalMap.set(masterVitalName, "pWEI");
	     }

	      if(option.code == "HEI"){
	    	  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pHEI">  <div class="card-main-conatiner">'+
	    	  '<p class="vital-title">HEIGHT</p>'+
	    	  '<div class="card-scroll-container"> <div class="card-inner" id="heightVitId"> </div>'+
	    	  '</div> </div> </div>';
	    	  vitalMap.set(masterVitalName, "pHEI");
	      }

	      if(option.code == "BMI"){
	    	  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pBMI">  <div class="card-main-conatiner">'+
	    	  '<p class="vital-title">BMI</p>'+
	    	  '<div class="card-scroll-container"> <div class="card-inner" id="bmiVitId"> </div>'+
	    	  '</div> </div> </div>';
	    	  vitalMap.set(masterVitalName, "pBMI");
	  	  }
	  	 if(option.code == "FAT"){
		  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pFAT">  <div class="card-main-conatiner">'+
		  '<p class="vital-title">BODY FAT</p>'+
		  '<div class="card-scroll-container"> <div class="card-inner" id="fatVitId"> </div>'+
		  '</div> </div> </div>';
		  vitalMap.set(masterVitalName, "pFAT");
	     }

	    if(option.code == "MUS"){
	    	addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pMUS">  <div class="card-main-conatiner">'+
	    	'<p class="vital-title">MUSCLE MASS</p>'+
	    	'<div class="card-scroll-container"> <div class="card-inner" id="muscleVitId"> </div>'+
	    	'</div> </div> </div>';
	    	vitalMap.set(masterVitalName, "pMUS");
	    }
        /*
	    if(option.code == "BONE"){
	    	addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pBONE">  <div class="card-main-conatiner">'+
	    	'<p class="vital-title">BONE</p>'+
	    	'<div class="card-scroll-container"> <div class="card-inner" id="boneVitId"> </div>'+
	    	'</div> </div> </div>';
	    	vitalMap.set(masterVitalName, "pBONE");
	    }
        */

		if(option.code == "WATER"){
			addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pWATER">  <div class="card-main-conatiner">'+
			'<p class="vital-title">WATER CONTENT</p>'+
			'<div class="card-scroll-container"> <div class="card-inner" id="watContVitId"> </div>'+
			'</div> </div> </div>';
			vitalMap.set(masterVitalName, "pWATER");
        }


       if(option.code == "PRO"){
    	addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pPRO">  <div class="card-main-conatiner">'+
    	'<p class="vital-title">PROTEIN(INTAKE)</p>'+
    	'<div class="card-scroll-container"> <div class="card-inner" id="protienVitId"> </div>'+
    	'</div> </div> </div>';
    	vitalMap.set(masterVitalName, "pPRO");
       }

       if(option.code == "hb"){
    	addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="phb">  <div class="card-main-conatiner">'+
    	'<p class="vital-title">HEMOGLOBIN</p>'+
    	'<div class="card-scroll-container"> <div class="card-inner" id="hbVitId"> </div>'+
    	'</div> </div> </div>';
    	vitalMap.set(masterVitalName, "phb");
       }

        /*
       if(option.code == "VFL"){
	    	addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pVFL">  <div class="card-main-conatiner">'+
	    	'<p class="vital-title">VISCERAL FAT LEVEL</p>'+
	    	'<div class="card-scroll-container"> <div class="card-inner" id="visFatVitId"> </div>'+
	    	'</div> </div> </div>';
	    	vitalMap.set(masterVitalName, "pVFL");
        }
		*/

		if(option.code == "BM"){
			addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pBM">  <div class="card-main-conatiner">'+
			'<p class="vital-title">BASAL METABOLIC RATE</p>'+
			'<div class="card-scroll-container"> <div class="card-inner" id="basalMetVitId"> </div>'+
			'</div> </div> </div>';
			vitalMap.set(masterVitalName, "pBM");
        }


		 if(option.code == "CHOLES"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pCHOLES">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">CHOLESTEROL</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="cholestrolVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pCHOLES");
           }


		 if(option.code == "TRIGLY"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pTRIGLY">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">TRIGLYCERIDES</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="triglyVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pTRIGLY");
           }
		 if(option.code == "HDL CHOLES"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pHDLCHOLES">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">HDL CHOLESTEROL</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="hdlCholVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pHDLCHOLES");
           }

		 if(option.code == "LDL CHOLES"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pLDLCHOLES">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">LDL CHOLESTEROL</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="ldlCholVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pLDLCHOLES");
           }

		 if(option.code == "CHOLES/HDL RATIO"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pCHOLESRATIO">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">CHOLESTEROL/HDL RATIO</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="CholRatioVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pCHOLESRATIO");
           }



		 if(option.code == "HEPATITIES B"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pHEPAB">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">HEPATITIES B</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="hepaBVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pHEPAB");
           }

		 if(option.code == "HIV"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pHIV">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">HIV</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="hivVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pHIV");
           }
		 /*
		 if(option.code == "COVID"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pCOVID">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">COVID 19 AG</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="covidVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pCOVID");
           }
		 if(option.code == "RSV_Ag"){
		    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pRSV_Ag">  <div class="card-main-conatiner">'+
		    '<p class="vital-title">RSV AG</p>'+
		    '<div class="card-scroll-container"> <div class="card-inner" id="RSV_AgId"> </div>'+
		    '</div> </div> </div>';
		    vitalMap.set(masterVitalName, "pRSV_Ag");
		}
		if(option.code == "Legionella_Ag"){
		    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pLegionella_Ag">  <div class="card-main-conatiner">'+
		    '<p class="vital-title">LEGIONELLA AG</p>'+
		    '<div class="card-scroll-container"> <div class="card-inner" id="Legionella_AgId"> </div>'+
		    '</div> </div> </div>';
		    vitalMap.set(masterVitalName, "pLegionella_Ag");
		}
		if(option.code == "S_pneumonia_Ag"){
		    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pS_pneumonia_Ag">  <div class="card-main-conatiner">'+
		    '<p class="vital-title">S PNEUMONIA AG</p>'+
		    '<div class="card-scroll-container"> <div class="card-inner" id="S_pneumonia_AgId"> </div>'+
		    '</div> </div> </div>';
		    vitalMap.set(masterVitalName, "pS_pneumonia_Ag");
		}
		 if(option.code == "strep_a_ag"){
		    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pstrep_a_ag">  <div class="card-main-conatiner">'+
		    '<p class="vital-title">STREP A AG</p>'+
		    '<div class="card-scroll-container"> <div class="card-inner" id="strep_a_agId"> </div>'+
		    '</div> </div> </div>';
		    vitalMap.set(masterVitalName, "pstrep_a_ag");
		}
		 */
		 /*
		 if(option.code == "CHIKUNGUNYA"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pCHIKANGUNYA">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">CHIKUNGUNYA IgM/IgG</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="chickenGunVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pCHIKANGUNYA");
           }
		 if(option.code == "DENGUE"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pDENGUE">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">DENGUE</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="dengueVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pDENGUE");
	       }
		 if(option.code == "DENGUE IgG"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pDENGUEIgG">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">DENGUE IgG</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="dengueIgGVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pDENGUEIgG");
	       }

		 if(option.code == "DENGUE IgM"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pDENGUEIgM">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">DENGUE IgM</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="dengueIgMVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pDENGUEIgM");
	       }
		 if(option.code == "Dengue_IgM/IgG"){
		    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pDengue_IgM_IgG">  <div class="card-main-conatiner">'+
		    '<p class="vital-title">DENGUE IgM/IgG</p>'+
		    '<div class="card-scroll-container"> <div class="card-inner" id="Dengue_IgMIgGId"> </div>'+
		    '</div> </div> </div>';
		    vitalMap.set(masterVitalName, "pDengue_IgM_IgG");
		}
		if(option.code == "Dengue_NS1"){
		    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pDengue_NS1">  <div class="card-main-conatiner">'+
		    '<p class="vital-title">DENGUE NS1</p>'+
		    '<div class="card-scroll-container"> <div class="card-inner" id="Dengue_NS1Id"> </div>'+
		    '</div> </div> </div>';
		    vitalMap.set(masterVitalName, "pDengue_NS1");
		}
		 */
		 if(option.code == "MALARIA"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pMALARIA">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">MALARIA</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="malariaVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pMALARIA");
           }

		 if(option.code == "NS- 1"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pNS-1">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">NS-1</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="ns1VitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pNS-1");
           }
		 /*
		 if(option.code == "VitD"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pVitD">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">VITAMIN D</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="vitDVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pVitD");
           }
		 if(option.code == "hCG"){
		    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="phCG">  <div class="card-main-conatiner">'+
		    '<p class="vital-title">&#946;-hCG</p>'+
		    '<div class="card-scroll-container"> <div class="card-inner" id="hCGId"> </div>'+
		    '</div> </div> </div>';
		    vitalMap.set(masterVitalName, "phCG");
		 }
		 if(option.code == "LH"){
		    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pLH">  <div class="card-main-conatiner">'+
		    '<p class="vital-title">LH</p>'+
		    '<div class="card-scroll-container"> <div class="card-inner" id="lhId"> </div>'+
		    '</div> </div> </div>';
		    vitalMap.set(masterVitalName, "pLH");
		 }
		 if(option.code == "T4"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pT4">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">T4</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="t4VitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pT4");
	     }

		 if(option.code == "TSH"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pTSH">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">TSH</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="tshVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pTSH");
	       }
		 if(option.code == "TSH WB"){
		    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pTSHWB">  <div class="card-main-conatiner">'+
		    '<p class="vital-title">TSH WB</p>'+
		    '<div class="card-scroll-container"> <div class="card-inner" id="tshWbId"> </div>'+
		    '</div> </div> </div>';
		    vitalMap.set(masterVitalName, "pTSHWB");
		}
		 if(option.code == "fT4"){
		    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pfT4">  <div class="card-main-conatiner">'+
		    '<p class="vital-title">fT4</p>'+
		    '<div class="card-scroll-container"> <div class="card-inner" id="fT4Id"> </div>'+
		    '</div> </div> </div>';
		    vitalMap.set(masterVitalName, "pfT4");
		}
		 */

		 /*
		 if(option.code == "PCT"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pPCT">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">PCT</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="pctMVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pPCT");
           }
		 if(option.code == "CRP"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pCRP">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">CRP</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="crpVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pCRP");
	       }
		 */

		 if(option.code == "T3"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pT3">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">T3</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="t3MVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pT3");
           }

		 	/*
			 if(option.code == "CK_MB"){
			    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pCK_MB">  <div class="card-main-conatiner">'+
			    '<p class="vital-title">CK MB</p>'+
			    '<div class="card-scroll-container"> <div class="card-inner" id="ckmbId"> </div>'+
			    '</div> </div> </div>';
			    vitalMap.set(masterVitalName, "pCK_MB");
			}
			if(option.code == "Troponin I"){
			    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pTroponinI">  <div class="card-main-conatiner">'+
			    '<p class="vital-title">TROPONIN I</p>'+
			    '<div class="card-scroll-container"> <div class="card-inner" id="tnlId"> </div>'+
			    '</div> </div> </div>';
			    vitalMap.set(masterVitalName, "pTroponinI");
			}
		   if(option.code == "D-DIMER"){
			 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pD-DIMER">  <div class="card-main-conatiner">'+
			 '<p class="vital-title">D-DIMER</p>'+
			 '<div class="card-scroll-container"> <div class="card-inner" id="dTimerVitId"> </div>'+
			 '</div> </div> </div>';
			 vitalMap.set(masterVitalName, "pD-DIMER");
	       }
			if(option.code == "NT proBNP"){
			    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pNTproBNP">  <div class="card-main-conatiner">'+
			    '<p class="vital-title">NT proBNP</p>'+
			    '<div class="card-scroll-container"> <div class="card-inner" id="ntProId"> </div>'+
			    '</div> </div> </div>';
			    vitalMap.set(masterVitalName, "pNTproBNP");
			}
			if(option.code == "hsCRP"){
			    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="phsCRP">  <div class="card-main-conatiner">'+
			    '<p class="vital-title">hsCRP</p>'+
			    '<div class="card-scroll-container"> <div class="card-inner" id="hsCRPId"> </div>'+
			    '</div> </div> </div>';
			    vitalMap.set(masterVitalName, "phsCRP");
			}
		 	*/

			/*
			if(option.code == "U_Albumin"){
			    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pU_Albumin">  <div class="card-main-conatiner">'+
			    '<p class="vital-title">U ALBUMIN</p>'+
			    '<div class="card-scroll-container"> <div class="card-inner" id="U_AlbuminId"> </div>'+
			    '</div> </div> </div>';
			    vitalMap.set(masterVitalName, "pU_Albumin");
			}
			if(option.code == "HBA1C"){
				 addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pHBA1C">  <div class="card-main-conatiner">'+
				 '<p class="vital-title">HBA1C</p>'+
				 '<div class="card-scroll-container"> <div class="card-inner" id="hba1cVitId"> </div>'+
				 '</div> </div> </div>';
				 vitalMap.set(masterVitalName, "pHBA1C");
	          }
			*/

            if(option.code == "lean_body_mass"){
			    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="plean_body_mass">  <div class="card-main-conatiner">'+
			    '<p class="vital-title">LEAN BODY MASS</p>'+
			    '<div class="card-scroll-container"> <div class="card-inner" id="lean_body_massId"> </div>'+
			    '</div> </div> </div>';
			    vitalMap.set(masterVitalName, "plean_body_mass");
			}
            if(option.code == "relative_fat_mass"){
			    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="prelative_fat_mass">  <div class="card-main-conatiner">'+
			    '<p class="vital-title">RELATIVE FAT MASS</p>'+
			    '<div class="card-scroll-container"> <div class="card-inner" id="relative_fat_massId"> </div>'+
			    '</div> </div> </div>';
			    vitalMap.set(masterVitalName, "prelative_fat_mass");
			}
            if(option.code == "waist"){
			    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pwaist">  <div class="card-main-conatiner">'+
			    '<p class="vital-title">WAIST CIRCUMFERENCE</p>'+
			    '<div class="card-scroll-container"> <div class="card-inner" id="waistId"> </div>'+
			    '</div> </div> </div>';
			    vitalMap.set(masterVitalName, "pwaist");
			}

			/*
			if(option.code == "iFOB"){
			    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="piFOB">  <div class="card-main-conatiner">'+
			    '<p class="vital-title">iFOB</p>'+
			    '<div class="card-scroll-container"> <div class="card-inner" id="iFOBId"> </div>'+
			    '</div> </div> </div>';
			    vitalMap.set(masterVitalName, "piFOB");
			}
			if(option.code == "h_pylori"){
			    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="ph_pylori">  <div class="card-main-conatiner">'+
			    '<p class="vital-title">h PYLORI</p>'+
			    '<div class="card-scroll-container"> <div class="card-inner" id="h_pyloriId"> </div>'+
			    '</div> </div> </div>';
			    vitalMap.set(masterVitalName, "ph_pylori");
			}

			*/

        			});

					$("#addVitalsDet").append(addVitals);
					if(callEpisodeVitalAPI){
						getEpisodeVitals(-1);
					}


				}if(data.errorCode=="1010"){
					$('#load2').hide();
			    	toastr.warning("Server Error");
				}
			}
		});
  }

   function promtfun(ob){
	   if(ob == 0){
	    var height = prompt("Please enter your height(ft)", "");
       var arr = { "Height": height};
	   }
	   if(ob == 1){
		    var weight = prompt("Please enter your Weight(Kg)", "");
		       var arr = { "Weight": weight};
	   }

	   var actHeight = $("#heightId").html();
	   var actWeight = $("#weightId").html();
       if (height != null && height !='' || weight != null && weight !='') {
	    	$.ajax({
				type : "POST",
				url : "saveEpisodeVitals?episodeId="+episdeId+"&patientId="+patientId+"&status=1&VitalArray="+JSON.stringify(arr),
				headers: {
			        "deviceId":"ADMIN",
			    },
			success : function(data){
				if(data.code=="200"){
					if(height != null && height !='' || weight != null && weight !=''){
						if(height != null && height !=''){
							actHeight = height;
						}
						if(weight != null && weight !=''){
							actWeight = weight;
						}

						var heightInMeter = actHeight*0.3048;
						var BMI = actWeight/(heightInMeter*heightInMeter);

			      		if(actHeight != null && actHeight != '' && actWeight != null && actWeight != ''){
			      			$("#bmihdr").css("display","block");
			      			$(".bmiValue").html('');
			      			$(".bmiValue").html(BMI);
			      			$("#bmiExt").html("Kg/m2");
			      		}
					}

		      		getEpisodeVitals(episdeId);
		      		getVitalDocument(episdeId);
		      		toastr.success("succesfully Edited");
				}

				}
			    ,error:function(x,y,response){
			    	$('#load2').hide();
			    	toastr.error("Server Error");

				}
			});



	    }else{
	    	/* toastr.warning("Please Enter the field"); */
	    }




    }

	function getEpisodeVitals(id){
		$('#load2').show();
		var epi = "";
		var useId="";
		if(id=="-1"){
		epi = episdeId;
		 /* Please dont change this this is not logged in user id */
			//alert("use"+useId);
			//alert("user"+userId);
		}else{epi=id;}
		useId=userId;
		$('.hereClass').html("--");
		$('.emptyExt').html("");
		$.ajax({
			type : "POST",
			url : serverPath+ "/getConsumerVitals?userId="+useId+"&episodeId="+epi,
			headers: {
		        "deviceId":"ADMIN",
		    },
		success : function(data){
			if(data.code=="200"){
				var height=""; var weight = ""; var vitalMeasuredTime = '';

				const myTempMap = new Map(data.content.map(obj => [ obj.vitalsName.toLowerCase(), obj.vitalsValue ]));
				vitalMap.forEach((values,keys)=>{
				      //console.log(keys,"<br>",values, ",id=","#"+values);
				      if(!myTempMap.has(keys)){
				    	  //console.log(keys,"<br>",values, ",id=","#"+values);
				    	  $("#"+values).remove();
				      }
				    })
				    updateWBioTable(data.content,patGender, patAge);
				    updateBasicVitalsTable(data.content,patGender, patAge);
				$.each(data.content,function(index,option){
					//console.log("VitalMongo= ", option.vitalsName, ", LC=",option.vitalsName.toLowerCase());
					var mongoVitalName= option.vitalsName.toLowerCase();
					//console.log("vitalMap.has(): ",mongoVitalName, " ? ",vitalMap.has(mongoVitalName));

					if(option.strMonitoredDate != null && option.strMonitoredDate != ""){
						//console.log("strMonitoredDate->",option.strMonitoredDate)
						vitalMeasuredTime = getCustomTime(strToDate(option.strMonitoredDate));
					}

					if(option.vitalsName.toLowerCase() == "Weight".toLowerCase()){
						var htmlNew = '<div class="card-floatLeft-2digitValue">'+
						'<p class="vital-actual-value">'+option.vitalsValue+ ' kg</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
						$('#weightVitId').prepend(htmlNew);
					}
					if(option.vitalsName.toLowerCase() == "Height".toLowerCase()){
						var htmlNew = '<div class="card-floatLeft-2digitValue">'+
						'<p class="vital-actual-value">'+option.vitalsValue+ ' cm</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
						$('#heightVitId').prepend(htmlNew);
					}

					if(option.vitalsName.toLowerCase() == "Temperature".toLowerCase()){
						var lowtemp = option.vitalsValue * 9/5 + 32 ;
						var htmlNew = '<div class="card-floatLeft">'+
						'<p class="vital-actual-value">'+option.vitalsValue+ ' C/'+ parseFloat(lowtemp).toFixed(2) + ' F</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
						$('#tempVitId').prepend(htmlNew);
					}

					if(option.vitalsName.toLowerCase() == "Pulse".toLowerCase()){
						var htmlNew = '<div class="card-floatLeft-2digitValue">'+
						'<p class="vital-actual-value">'+option.vitalsValue+ ' b/m</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
						$('#pulseVitId').prepend(htmlNew);
					}

					if(option.vitalsName.toLowerCase() == "BP".toLowerCase()){
						var splt = (option.vitalsValue).split(",");
						var htmlNew = '<div class="card-floatLeft">'+
			        	  '<p class="vital-actual-value">'+splt[0]+'/'+splt[1]+ ' mmhg</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
			        	  $('#bpVitId').prepend(htmlNew);
					}

					if(option.vitalsName.toLowerCase() == "BMI".toLowerCase()){
						var htmlNew = '<div class="card-floatLeft-2digitValue">'+
			        	  '<p class="vital-actual-value">'+option.vitalsValue+ ' kg/m2</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
			        	  $('#bmiVitId').prepend(htmlNew);
					}
					if(option.vitalsName.toLowerCase() == "BG".toLowerCase()){
						var htmlNew = '<div class="card-floatLeft-2digitValue">'+
			        	  '<p class="vital-actual-value">'+option.vitalsValue+ ' mg/dl</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
			        	  $('#bldGluVitId').prepend(htmlNew);
					}
					if(option.vitalsName.toLowerCase() == "SPO2".toLowerCase()){
						var htmlNew = '<div class="card-floatLeft-2digitValue">'+
		        	  '<p class="vital-actual-value">'+option.vitalsValue+ ' %</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
		        	  $('#spo2VitId').prepend(htmlNew);
					}
					if(option.vitalsName.toLowerCase() == "FAT LEVEL".toLowerCase()){
						var htmlNew = '<div class="card-floatLeft-2digitValue">'+
		        	  '<p class="vital-actual-value">'+option.vitalsValue.trim()+ ' %</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
						$('#fatVitId').prepend(htmlNew);
					}
					if(option.vitalsName.toLowerCase() == "WATER CONTENT".toLowerCase()){
						var htmlNew = '<div class="card-floatLeft-2digitValue">'+
		        	  '<p class="vital-actual-value">'+option.vitalsValue+ ' kg</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
		        	  $('#watContVitId').prepend(htmlNew);
					}

        			if(option.vitalsName.toLowerCase() == "MUSCLE".toLowerCase()){
        				var htmlNew = '<div class="card-floatLeft-2digitValue">'+
      	  				'<p class="vital-actual-value">'+option.vitalsValue+ ' kg/m2</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
      	  				$('#muscleVitId').prepend(htmlNew);
          			}
          if(option.vitalsName.toLowerCase() == "PROTEIN".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-2digitValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' g/day</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  $('#protienVitId').prepend(htmlNew);
          }
          if(option.vitalsName.toLowerCase() == "hb".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-2digitValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' g/dl</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  $('#hbVitId').prepend(htmlNew);
          }
          if(option.vitalsName.toLowerCase() == "VISCERAL FAT LEVEL".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-2digitValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  $('#visFatVitId').prepend(htmlNew);
          }
          if(option.vitalsName.toLowerCase() == "BASAL METABOLISM".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-2digitValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' Kcal/day</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  $('#basalMetVitId').prepend(htmlNew);
          }
          /*
          if(option.vitalsName.toLowerCase() == "BONE".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-2digitValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' kg</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  $('#boneVitId').prepend(htmlNew);
          }
          */
          if(option.vitalsName.toLowerCase() == "lean_body_mass".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-2digitValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' kg</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  $('#lean_body_massId').prepend(htmlNew);
          }
         if(option.vitalsName.toLowerCase() == "relative_fat_mass".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-2digitValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' %</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  $('#relative_fat_massId').prepend(htmlNew);
          }
         if(option.vitalsName.toLowerCase() == "waist".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-2digitValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' Inches</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  $('#waistId').prepend(htmlNew);
          }

          if(option.vitalsName.toLowerCase() == "NS- 1".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#ns1VitId').prepend(htmlNew);
        	  $('#ns1VitId').html(htmlNew);
            }
          /*
          if(option.vitalsName.toLowerCase() == "VitD".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/mL</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#vitDVitId').prepend(htmlNew);
        	  $('#vitDVitId').html(htmlNew);
            }
          if(option.vitalsName.toLowerCase() == "T4".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' nmol/L</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#t4VitId').prepend(htmlNew);
          	  $('#t4VitId').html(htmlNew);
            }
          if(option.vitalsName.toLowerCase() == "TSH".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' miU/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#tshVitId').prepend(htmlNew);
          	  $('#tshVitId').html(htmlNew);
            }
          if(option.vitalsName.toLowerCase() == "hCG".toLowerCase()){
	  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	  		  '<p class="vital-actual-value">'+option.vitalsValue+ ' mIU/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	  		  //$('#hCGId').prepend(htmlNew);
	  		  $('#hCGId').html(htmlNew);
	  	  }
	  	  if(option.vitalsName.toLowerCase() == "LH".toLowerCase()){
	  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	  		  '<p class="vital-actual-value">'+option.vitalsValue+ ' mIU/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	  		  //$('#lhId').prepend(htmlNew);
	  		  $('#lhId').html(htmlNew);
	  	  }
	  	  if(option.vitalsName.toLowerCase() == "fT4".toLowerCase()){
	  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	  		  '<p class="vital-actual-value">'+option.vitalsValue+ ' pmol/L</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	  		  //$('#fT4Id').prepend(htmlNew);
	  		  $('#fT4Id').html(htmlNew);
	  	  }
	  	  if(option.vitalsName.toLowerCase() == "TSH WB".toLowerCase()){
	  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	  		  '<p class="vital-actual-value">'+option.vitalsValue+ ' mIU/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	  		  //$('#tshWbId').prepend(htmlNew);
	  		  $('#tshWbId').html(htmlNew);
	  	  }
          */

          /*
          if(option.vitalsName.toLowerCase() == "DENGUE IgG".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#dengueIgGVitId').prepend(htmlNew);
          	  $('#dengueIgGVitId').html(htmlNew);
            }
          if(option.vitalsName.toLowerCase() == "DENGUE IgM".toLowerCase()){
        	var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#dengueIgMVitId').prepend(htmlNew);
          	  $('#dengueIgMVitId').html(htmlNew);
            }
          if(option.vitalsName.toLowerCase() == "DENGUE".toLowerCase()){
	    	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	      	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	      	  //$('#dengueVitId').prepend(htmlNew);
	      	  $('#dengueVitId').html(htmlNew);
	        }
	          if(option.vitalsName.toLowerCase() == "CHIKUNGUNYA".toLowerCase()){
	    	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	      	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	      	  //$('#chickenGunVitId').prepend(htmlNew);
	      	  $('#chickenGunVitId').html(htmlNew);
	        }
          if(option.vitalsName.toLowerCase() == "Dengue_IgM/IgG".toLowerCase()){
	  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	  		  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	  		  //$('#Dengue_IgMIgGId').prepend(htmlNew);
	  		  $('#Dengue_IgMIgGId').html(htmlNew);
	  	  }
	  	  if(option.vitalsName.toLowerCase() == "Dengue_NS1".toLowerCase()){
	  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	  		  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	  		  //$('#Dengue_NS1Id').prepend(htmlNew);
	  		  $('#Dengue_NS1Id').html(htmlNew);
	  	  }
          */

          /*
          if(option.vitalsName.toLowerCase() == "PCT".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#pctMVitId').prepend(htmlNew);
          	  $('#pctMVitId').html(htmlNew);
            }
          if(option.vitalsName.toLowerCase() == "CRP".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' mg/L</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#crpVitId').prepend(htmlNew);
          	  $('#crpVitId').html(htmlNew);
            }
          */

          if(option.vitalsName.toLowerCase() == "T3".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#t3VitId').prepend(htmlNew);
          	  $('#t3VitId').html(htmlNew);
            }

          if(option.vitalsName.toLowerCase() == "CHOLES".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#cholestrolVitId').prepend(htmlNew);
          	  $('#cholestrolVitId').html(htmlNew);
            }

          if(option.vitalsName.toLowerCase() == "TRIGLY".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#triglyVitId').prepend(htmlNew);
          	  $('#triglyVitId').html(htmlNew);
            }

          if(option.vitalsName.toLowerCase() == "HDL CHOLESTEROL".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#hdlCholVitId').prepend(htmlNew);
          	  $('#hdlCholVitId').html(htmlNew);
            }

          if(option.vitalsName.toLowerCase() == "LDL CHOLESTEROL".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#ldlCholVitId').prepend(htmlNew);
          	  $('#ldlCholVitId').html(htmlNew);
            }

          if(option.vitalsName.toLowerCase() == "CHOLESTEROL/HDL RATIO".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#CholRatioVitId').prepend(htmlNew);
          	  $('#CholRatioVitId').html(htmlNew);
            }




          if(option.vitalsName.toLowerCase() == "HEPATITIES B".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#hepaBVitId').prepend(htmlNew);
          	  $('#hepaBVitId').html(htmlNew);
            }

          if(option.vitalsName.toLowerCase() == "HIV".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#hivVitId').prepend(htmlNew);
          	  $('#hivVitId').html(htmlNew);
            }




          /*
          if(option.vitalsName.toLowerCase() == "COVID".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#covidVitId').prepend(htmlNew);
          	  $('#covidVitId').html(htmlNew);
            }
          if(option.vitalsName.toLowerCase() == "RSV_Ag".toLowerCase()){
	  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	  		  '<p class="vital-actual-value">'+option.vitalsValue+ ' </p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	  		  //$('#RSV_AgId').prepend(htmlNew);
	  		  $('#RSV_AgId').html(htmlNew);
	  	  }
	  	  if(option.vitalsName.toLowerCase() == "Legionella_Ag".toLowerCase()){
	  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	  		  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	  		  //$('#Legionella_AgId').prepend(htmlNew);
	  		  $('#Legionella_AgId').html(htmlNew);
	  	  }
	  	  if(option.vitalsName.toLowerCase() == "S_pneumonia_Ag".toLowerCase()){
	  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	  		  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	  		  //$('#S_pneumonia_AgId').prepend(htmlNew);
	  		  $('#S_pneumonia_AgId').html(htmlNew);
	  	  }
          if(option.vitalsName.toLowerCase() == "strep_a_ag".toLowerCase()){
	  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	  		  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	  		  //$('#strep_a_agId').prepend(htmlNew);
	  		  $('#strep_a_agId').html(htmlNew);
	  	  }
          */
          if(option.vitalsName.toLowerCase() == "MALARIA".toLowerCase()){
        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          	  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          	  //$('#malariaVitId').prepend(htmlNew);
          	  $('#malariaVitId').html(htmlNew);
            }



        	  /*
        	  if(option.vitalsName.toLowerCase() == "CK_MB".toLowerCase()){
	      		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	      		  '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	      		  //$('#ckmbId').prepend(htmlNew);
	      		  $('#ckmbId').html(htmlNew);
	      	  }
	          if(option.vitalsName.toLowerCase() == "Troponin I".toLowerCase()){
		  		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
		  		  '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/mL</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
		  		  //$('#tnlId').prepend(htmlNew);
		  		  $('#tnlId').html(htmlNew);
		  	  }
        	  if(option.vitalsName.toLowerCase() == "NT proBNP".toLowerCase()){
	      		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	      		  '<p class="vital-actual-value">'+option.vitalsValue+ ' pg/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	      		  //$('#ntProId').prepend(htmlNew);
	      		  $('#ntProId').html(htmlNew);
	      	  }
        	  if(option.vitalsName.toLowerCase() == "D-DIMER".toLowerCase()){
	        	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	          	  '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/mL</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	          	  //$('#dTimerVitId').prepend(htmlNew);
	          	  $('#dTimerVitId').html(htmlNew);
	          }
        	  if(option.vitalsName.toLowerCase() == "hsCRP".toLowerCase()){
        		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
        		  '<p class="vital-actual-value">'+option.vitalsValue+ ' mg/L</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
        		  //$('#hsCRPId').prepend(htmlNew);
        		  $('#hsCRPId').html(htmlNew);
        	  }
        	  */



        	  /*
        	  if(option.vitalsName.toLowerCase() == "iFOB".toLowerCase()){
        		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
        		  '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
        		  //$('#iFOBId').prepend(htmlNew);
        		  $('#iFOBId').html(htmlNew);
        	  }
        	  if(option.vitalsName.toLowerCase() == "h_pylori".toLowerCase()){
          		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
          		  '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
          		  //$('#h_pyloriId').prepend(htmlNew);
          		  $('#h_pyloriId').html(htmlNew);
          	  }
        	  */


        	  /*
        	  if(option.vitalsName.toLowerCase() == "HBA1C".toLowerCase()){
            	  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
              	  '<p class="vital-actual-value">'+option.vitalsValue+ ' %</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
              	  //$('#hba1cVitId').prepend(htmlNew);
              	  $('#hba1cVitId').html(htmlNew);
                }
        	  if(option.vitalsName.toLowerCase() == "U_Albumin".toLowerCase()){
	      		var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
	      		  '<p class="vital-actual-value">'+option.vitalsValue+ ' mg/L</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
	      		  //$('#U_AlbuminId').prepend(htmlNew);
	      		  $('#U_AlbuminId').html(htmlNew);
	      	  }
        	  */


					$('#vit	Date').html(option.strCreatedDate);
				});
				$('#load2').hide();
			}
			//$("#pTemp").remove();
			if(data.errorCode=="1009"){
				$('#load2').hide();
		    	//toastr.warning("No Vitals Found");
			}if(data.errorCode=="1010"){
				$('#load2').hide();
		    	toastr.warning("Server Error");
			}
			}
		    ,error:function(x,y,response){
		    	$('#load2').hide();
		    	toastr.error("Server Error");

			}
		});

		$('#load2').hide();

	}

	function getVitalDocument(id){
		var histIds = "";
		if(id == null || id == "-1"){
			histIds = episdeId;
		}else{
			histIds = id;
		}
		var addDoc = "";
		var addEcgDoc = "";
		$.ajax({
			type : "POST",
			url : serverPath+ "/getEcgReports?episodeId="+histIds,
			headers: {
		        "deviceId":"ADMIN",
		    },
			success : function(data){
				if(data.code=="200"){
					if(data.content != null){
					    $.each(data.content,function(index,option){
							if(option.filePath!=null){
								//console.log("ECG:", option, option.fileType,option.filePath);
								if(option.fileType!="SPO2" && option.fileType!="ecg" && option.fileType != "12Leadecg"
								&& option.fileType != "HdEcg"){
									var tagvar=option.fileType;
									var createdDate ='';
									if(option.createdAt != null && option.createdAt != ''){
										//createdDate= new Date(option.createdAt).toLocaleString();
										//createdDate= new Date(option.createdAt).toISOString();
										var m = new Date(option.createdAt);
										var dateString = m.getUTCFullYear() +"/"+ (m.getUTCMonth()+1) +"/"+ m.getUTCDate() + " " + m.getUTCHours() + ":" + m.getUTCMinutes() + ":" + m.getUTCSeconds();
										createdDate = m.getUTCFullYear() +"/"+ (m.getUTCMonth()+1) +"/"+ m.getUTCDate() + " " + getCustomTime(strToDate(dateString));
									}
									var arr=tagvar.split(":");
									if(arr[1] == undefined || arr[1] == null || arr[1] == "null"){
										arr[1] = '' ;
							        }
				             	    addDoc+='<div class="col-xs-12">'+
				                    '<span style="display:inline-flex;"> <b style="text-transform:uppercase;">'+arr[0].trim()+':</b>&nbsp;<span style="color:#0000FF;">'+arr[1]+'</span></span>'+
				                    '<span>, Date: '+createdDate+'</span>'+
		    			            '<div class="info-box" style="display: inline-flex; padding-bottom: 10px;">'+
							        '<img  src="'+imagePath+option.filePath+'" >'+
						            '</div>'+
					                '</div>';
							    } else if(option.fileType == "ecg"){
							        addEcgDoc+='<div class="col-xs-12">'+
							        '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="'+imagePath+option.filePath+'" target="_blank"><b style="text-transform:uppercase;">ECG Report</b></a></span>'+
                                    '</div>';
							    }else if (option.fileType == "ecgFilePath"){
							    	addEcgDoc+='<div class="col-xs-12">'+
							        '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="'+imagePath+option.filePath+'" target="_blank"><b style="text-transform:uppercase;">ECG Report</b></a></span>'+
                                    '</div>';
							    }else if (option.fileType == "12Leadecg"){
                                 	addEcgDoc+='<div class="col-xs-12">'+
             						'<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="'+imagePath+option.filePath+'" target="_blank"><b style="text-transform:uppercase;">6/12Lead ECG Report</b></a></span>'+
                                    '</div>';
                                }else if (option.fileType == "HdEcg"){
                                    addEcgDoc+='<div class="col-xs-12">'+
                                    '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="'+imagePath+option.filePath+'" target="_blank"><b style="text-transform:uppercase;">HDSTETH ECG Report</b></a></span>'+
                                    '</div>';
                                }
				            }
				            $('#addDates').html(option.strCreatedDate);
			            });
					}
					$('#addEcgs').html(addDoc + addEcgDoc);
				}if(data.errorCode=="1090"){
					$('#load2').hide();
			    	//toastr.warning("No ECG Found");
				}if(data.errorCode=="1010"){
					$('#load2').hide();
			    	toastr.warning("Server Error");
				}
			}
		});
	}

	function logout(){
		window.location.href="logout";
	}

   function	EmrReport(){

	   var emrreport = serverPath+"/getEmrPdfReport?patientId="+patientId+"&userId="+userId+"&productId="+productId+"&episodeId="+episdeId;


	   window.open(emrreport);


   }

	function openMedicationModal(){

		$("#addMedicationModal").modal('show');
		    $('#saveMedicationButton').show();
            $('#addMedicationButton').show();
            $('#updateMedicationButton').hide();

		var prescribedBy = $("#loggedInUserFullName").text();
		$("#prescribedByPre").parent().addClass('is-dirty');
		$("#prescribedByPre").val(prescribedBy);
		$("#prescribedByPre").attr("disabled", true);
	}
	function openObservationModal(){
		$("#addObservationModal").modal('show');

	}function openAdviceModal(){
		//$("#addObservationModal").modal('show');
		$("#addVisitForm").modal('show');
		var prescribedBy = $("#loggedInUserFullName").text();
		$("#prescribedByAdv").parent().addClass('is-dirty');
		$("#prescribedByAdv").val(prescribedBy);
		$("#prescribedByAdv").attr("disabled", true);

	}

	function openNotesModal(){
		$("#addNotesModal").modal('show');
	}

	function openReportModal(){
		$("#addReportModal").modal('show');
		$("#firstmedfile").val('');
	}
	function acceptType(ob){

		if(ob.files[0].type == "image/jpeg" || ob.files[0].type == "application/vnd.ms-excel" || ob.files[0].type =="application/pdf"
			|| ob.files[0].type =="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || ob.files[0].type =="image/png"){

			}else{
				  $("#firstmedfile").val('');
				  /* mes = "File format not supported";
				  $('#demo-show-snackbar').trigger('click'); */
				 // alert("File format not supported");

				}
	}
	var count=0;
    function getOtherMedFile(){
  	  var element1="";
  	  var filecountid='';

        if($("#firstmedfile").val() != '')
        {
	      	count++;
	    	element1 = '<p style="display:inline-flex;margin-bottom: 5px;" ><i class="fa fa-paperclip" style="padding-top: 5px;margin-left: 15px;color:black;padding-right: 10px"></i> '
	         +'<input type="file" id="filecountid'+count+' "class="browserfield" name="files" onchange="" style="margin-bottom: 0px; border-radius: 4px; font-family:Roboto Medium;font-size:15px;color:#5498D2;border:#5498D2 1px solid">'
	         +'<i style="cursor: pointer;color:black; padding-top: 7px;" class="fa fa-times" onclick="removeFiles(this)"></p>';
        }

        var checkFile = false;
    	$(".browserfield").each(function(){
    	    if($(this).val()!=""){

    		 }else{
    		         checkFile = true;
    			}
    	});
    	if(checkFile!=true){
	       $("#addFilesMed").append(element1);
		}

    }
	medData = '';
	function addMedication(){

		var valName = $("#medicationNameId").val();
		var medicationId = $("#medicationIdFromDropdown").val();
		var valDosage = $("#dosageID").val();
		if(valName != ''){
		if(medicationId == null || medicationId == ""){
			//alert();
			 $.ajax({
				type : "POST",
				url: "addMedication?medicationName="+valName,

				success : function(data) {
					if(data.code=="200"){
						medicationId = data.content.id;
					}
				}
		 	});
		 	//alert("here");
		}
		}
		var valFreq = $("#fequencyId").val();
		var valInst = $("#instructionId").val();
		var valStart = $("#startId").val();
		var valDays = $("#days").val();
		var valComm = $("#commentsMedId").val();
		//var advicePres1 = $("#prescribedByAdv").val();
		//console.log("TEST-> ",valName, ", " , valStart, "," ,  valDays, ", " ,advicePres1);
		var doctorName;
		 var form1 = document.getElementById('saveMedicationId');
 		 var formData = new FormData(form1);
 		//console.log("Before AJAX: ",formData);
 		for (var [key, value] of formData.entries()) {
 			if(key == "prescribedBy"){
 				//console.log(key, value);
 				doctorName = value;
 			}
 			}
 		if (!doctorName.replace(/\s/g, '').length) {
 			  console.log('string only contains whitespace (ie. spaces, tabs or line breaks)');
 			}
 		if (!doctorName.replace(/\./g, '').length) {
			  console.log('string only contains dots (ie. spaces, tabs or line breaks)');
			}
			if(valName == ''){
			    toastr.warning('Please fill mandatory fields');
			} else if(valStart == ''){
			    toastr.warning('Please fill mandatory fields');
			} else if(valDays == ''){
			    toastr.warning('Please fill mandatory fields');
			}else if(valName != '' && valStart != '' && valDays != ''  || ( doctorName != '' && (doctorName.replace(/\s/g, '').length > 0)  && (doctorName.replace(/\./g, '').length > 0)) ){

			$(".addedMedTable").show();
			var medData = '<tr><td class="getJson" style="display:none" attr="medicationId">'+medicationId+'</td><td class="getJson" attr="medicationName">'+valName+'</td>'
							+'<td class="getJson" attr="dosage">'+valDosage+'</td><td class="getJson" attr="frequency">'+valFreq+'</td>'
							+' <td class="getJson" attr="instructions">'+valInst+'</td><td class="getJson" attr="beginDate">'+valStart+'</td>'
							+'<td class="getJson" attr="Days">'+valDays+'</td><td class="getJson" attr="commentPrescription">'+valComm+'</td> '
							+'<td><i style="cursor:pointer" class="fa fa-times" aria-hidden="true" onclick="removeFileMed(this)"></i></td></tr>';

			$("#medAdd").append(medData);
			$('.Newpolic1').val('');
			$('.Newpolic1').parent().removeClass('is-dirty');
			$('#fequencyId').val('0-0-1');
			$('#instructionId').val('Before Food');
		 }else{
				toastr.warning('Please fill mandatory fields');
		    }
	}
	function removeFileMed(ob){

		$(ob).parent().parent().remove();

		if($("#medAdd tr").length < 1){
			$('#dynTableMed').hide();
		}
		if($("#invAdd tr").length < 1){
			$('#dynTableInv').hide();
		}
	}
	function addmedicationjson(){
		var isexist=0;
	 	 var ordr=1;
	 	dditionalDetailJson =  '';additonalDetails = '';
		 medicationJSON = [];MedicationFinalJSON = [];
		$(".getJson").each(function() {
	 		//alert($(this).val());
			//console.log($(this).attr("attr"));

 			if($(this).attr("attr")=="medicationName"){
 				 medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
 			 if($(this).attr("attr")=="dosage"){
 				 medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="frequency"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="instructions"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="beginDate"){
 				$('#strPrescribedDate').val($(this).text());
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		 if($(this).attr("attr")=="Days"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
	 		if($(this).attr("attr")=="commentPrescription"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text()  });
 			}
 			if($(this).attr("attr")=="medicationId"){
 				medicationJSON.push({keyName : $(this).attr("attr"),value : $(this).text() });
 			}
 			//console.log(ordr);
 			if(ordr%8==0 && ordr>1){
 				//console.log("inside"+ordr);
 				medicationJSON.push({keyName : "id",value : 0 });
 			 	medicationJSON.push({keyName : "status",value : 1 });
 			 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
 			 medicationJSON.push({keyName : "medicalReportId",value : -1 });
 			 console.log(medicationJSON);
 			 MedicationFinalJSON.push(medicationJSON);
 			 medicationJSON=[];
 			}
 		 ordr++;
 		 });
	}
	function addAdvicejson(){
		datetime = '';
		datetime = $("#fromDate").val();
		additionalDetailJson =  '';additonalDetails = '';
	 medicationJSON = [];MedicationFinalJSON = [];
		$(".getAdvice").each(function() {

			//console.log($(this).attr("attr"), ', ',($(this).attr("name")));
			if($(this).attr("name")=="investigationName"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
 			if($(this).attr("name")=="prescribedBy"){
 				 //medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 				 medicationJSON.push({keyName : "prescribedBy", value : $("#prescribedByAdv").val()  });
 			}
 			 if($(this).attr("name")=="InvestigationType"){
 				 medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		 if($(this).attr("name")=="dateTime"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		 if($(this).attr("name")=="Labcomments"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
	 		if($(this).attr("name")=="investigationId"){
 				medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 			}
 		 //ordr++;

 		 });

		medicationJSON.push({keyName : "id",value : 0 });
	 	medicationJSON.push({keyName : "status",value : 1 });
	 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
		 medicationJSON.push({keyName : "medicalReportId",value : -1 });
		 //console.log(medicationJSON);
		 MedicationFinalJSON.push(medicationJSON);
		 medicationJSON=[];
	}

	var currentdate = new Date();
	/* var datetime = currentdate.getDay() + "/"+currentdate.getMonth()
	+ "/" + currentdate.getFullYear(); */
	var datetime = currentdate.getDate() + "/" + (currentdate.getMonth()+1) + "/" + currentdate.getFullYear();
	//var dateAdvice = $("#").val();
	//alert(datetime);

	//alert(datetime);
	function addNotesjson(){
		 //alert( $("#noeComments").val());
		additionalDetailJson =  '';additonalDetails = '';
		 medicationJSON = [];MedicationFinalJSON = [];
		$(".getNotes").each(function() {
	 		if($(this).attr("name")=="comments"){
	 			medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 				//alert($(this).val());
 				var notCom = $(this).val();
 				//alert(notCom);
 				$("#noteComments").val(notCom);
 				//alert($("#noteComments").val())
 			}
	 		medicationJSON.push({keyName : "addedTime",value : datetime });
 		 });

		medicationJSON.push({keyName : "id",value : 0 });
	 	medicationJSON.push({keyName : "status",value : 1 });
	 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
		 medicationJSON.push({keyName : "medicalReportId",value : -1 });
		 //console.log(medicationJSON);
		 MedicationFinalJSON.push(medicationJSON);
		 medicationJSON=[];
	}
	function addReportjson(){
		additionalDetailJson =  '';additonalDetails = '';
		 medicationJSON = [];MedicationFinalJSON = [];

		$(".getReport").each(function() {
	 		if($(this).attr("name")=="files"){
	 			medicationJSON.push({keyName : $(this).attr("name"),value : $(this).val()  });
 				//alert($(this).val());
 			}
	 		medicationJSON.push({keyName : "addedTime",value : datetime });
 		 });

		medicationJSON.push({keyName : "id",value : 0 });
	 	medicationJSON.push({keyName : "status",value : 1 });
	 //medicationJSON.push({keyName : "strPrescribedDate",value : $('#strPrescribedDate').val() });
		 medicationJSON.push({keyName : "medicalReportId",value : -1 });
		 MedicationFinalJSON.push(medicationJSON);
	}

	var medicationJSON = [];
	var MedicationFinalJSON = [];
	prefixStrForPrescription='{\"additionalDetail\":';
	additionalDetailJson =  '';
	additonalDetails = '';

	function saveMedication(ob){
		    var form1 = document.getElementById('saveMedicationId');
		    var formData = new FormData(form1);
		    var advicePres = formData.get("prescribedByAdv");


		//alert($(ob).val());
		$("#additionalDetailJson").val('');
		additionalDetailJson =  '';additonalDetails = '';
	 	 medicationJSON = [];MedicationFinalJSON = [];
	 	 var isexist=0;
	 	 var ordr=1;

	 	// alert(episodeId);
	 	 //alert($("#noteComments").text());
	 	 $("#medEpisodeId").val(episdeId);
	 	 if(ob==1){
	 		 var medName = $("#medicationNameId").val();
	 		 var stDaMAn = $("#startId").val();
	 		var stDaysMAn = $("#days").val();
	 		var prescribedBy = $("#loggedInUserFullName").text();
	 		$("#prescribedBy").val(prescribedBy);
	 		console.log("formData: ",prescribedBy);
	 		/* if(medName != ''||stDaMAn != ''||stDaysMAn != ''){ */
	 		addmedicationjson();
	 		$('#documentTypeId').val(1);
	 		$('#serviceNameValue').val(null);

	 		/* }else{
				toastr.warning('Please fill mandatory fields');
			} */
	 	 }
	 	 if(ob==2){

	 		var serviceD = $("#serviceTypeId").val();
	 		var servDat = $("#fromDate").val();
	 		var advicePres = $("#prescribedByAdv").val();


	 		 if(serviceD != "" && servDat != "" && advicePres != "" ){
	 		addAdvicejson();

	 		var date = new Date().toJSON().slice(0, 10).split("-").reverse().join("/");
			//console.log(date);
	 		$('#strPrescribedDate').val(date);
	 		$('#documentTypeIdAdv').val(2);
	 		$('#documentTypeId').val(2);
	 		//$('#serviceNameValue').val($('#serviceNameId').val());
	 		 }else{
					toastr.warning('Please fill mandatory fields');
				}
	 	 }
	 	if(ob==3){
	 		notMan = $("#noeComments").val();
	 		//alert(notMan);
	 		if(notMan != ''){
	 		addNotesjson();
	 		var date = new Date().toJSON().slice(0, 10).split("-").reverse().join("/");
			//console.log(date);
	 		$('#strPrescribedDate').val(date);
	 		$('#documentTypeId').val(8);
	 		//$('#serviceNameValue').val($('#serviceNameId').val());
	 		 }else{
				toastr.warning('Please fill mandatory fields');
			}
	 	 }
		if(ob==4){
			var fileMan=$(".getReport").val();
			//alert(fileMan);
			 if(fileMan != ''){
			addReportjson();
			var date = new Date().toJSON().slice(0, 10).split("-").reverse().join("/");
			console.log(date);
	 		$('#strPrescribedDate').val(date);
			$('#documentTypeId').val(9);
			}else{
				toastr.warning('Please attach report');
			}
		}

		 $('#dynTableMed').show();

	 	  if(MedicationFinalJSON.length > 0){
	 	        var additonalDetails = prefixStrForPrescription+JSON.stringify(MedicationFinalJSON)+"}";
	 	        $("#additionalDetailJson").val(additonalDetails);
	 	       medicationJSON = [];MedicationFinalJSON = [];
	 	    }

			data = $("#additionalDetailJson").val();
			//console.log("data:  ",data);
			//alert(data);
		 	  if(data != null && data != ''){
		 		 var form1 = document.getElementById('saveMedicationId');
		 		 var formData = new FormData(form1);

	 		  //alert();
	 		  console.log("formdata: ",formData);
	 		$.ajax({
	 			type : "POST",
	 			url : serverPath+"/addDocuments",
	 			headers: {
	 		        "deviceId":"ADMIN",
	 		    },

	 			data: formData,
	 			dataType:"json",
	 		    mimeType: "multipart/form-data",
	 			async : false,
	 		    cache : false,
	 			contentType : false,
	 		    processData : false,
	 			success : function(data){
						//alert(data.code);
	 				if(data.code="200"){

	 					$("#additionalDetailJson").val('');

	 					toastr.success('Added Successfully','');

	 					canMed();
	 					clearAdvModal();
	 					canNote();
	 					clearRep();
	 					/* $('.modal').modal('hide');
	 					$(".getAdvice").val('');
	 					$('.getAdvice').parent().removeClass("is-dirty"); */
	 					getHistoryOfUser(episdeId,"",true);


	 					}
	 				}

              });


	 	    }else{

	 	    	 $('#dynTableMed').hide();
		 	     mes = "Please Add the mandatory data";
			     $("#demo-show-snackbar").trigger('click');
				}

	}


function getPatientDetails(){
	var addDoc = "";var other = "";var gender = "";var mob = "";var address="";var email = "";var addaDet = "";var age = '';
	var uhid = "";
	$.ajax({
		type : "POST",
		url :  serverPath + "/getPatientDetails?patientId="+patientId,
		headers: {
	        "deviceId":"ADMIN",
	    },
	success : function(data){
	if(data.code=="200"){
        var option = data.content;
		var patName = "";
		patGender= option.gender; patAge=option.age;
				if(option.patientFirstName !=null){
					patName+=option.patientFirstName;
				}
				if(option.patientLastName !=null){
					patName+=" "+option.patientLastName;
				}
				if(option.bloodGroup !=null){
					other = option.bloodGroup;

				}if(option.gender =="Male"){
					gender = '<img src="resources/images/male.png" style="width:12px">';

				}if(option.gender =="Female"){
					gender = '<img src="resources/images/femenine.png" style="width:12px">';

				}if(option.mobileNo !=null){
					mob = option.mobileNo;
				}
				if(option.address !=null){
					address = option.address;
				}
				if(option.emailId !=null){
					email = option.emailId;
				}
				if(option.uhid != null){
				    uhid = option.uhid;
				}
				age = option.age;

			$('#addPrfName').html(patName);
			$('#uhidId').html(uhid);
			if(other != ""){
				addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;font-size:12px">'+
				'<b>'+age+' / '+other+' / '+gender+' </b>'+
				'</li>';
			} else {
				addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;font-size:12px">'+
				'<b>'+age+' / '+gender+' </b>'+
				'</li>';
			}

			if(mob!=""){
				 addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;font-size:12px">'+
					'<b>'+mob+' </b>'+
					'</li>';
			}if(address!=""){
				 addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'+
					'<b>'+address+' </b>'+
					'</li>';
			}if(email!=""){
				 addaDet+='<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'+
					'<b>'+email+' </b>'+
					'</li>';
			}

			$('#addDet').html(addaDet);
	}if(data.errorCode=="1010"){
		$('#load2').hide();
    	toastr.warning("Server Error");
	}
		}
	});
}
/* <li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">
<b>71 / <img src="resources/images/male.png" style="width:12px">  </b>
</li>
<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">
<b id="addMobile">9900864016/9998984932</b>
</li>
<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">
<b id="addMobile">#653,  2nd cross
	Vidya nagar
	Banglore
</b>
</li>
<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">
<b><img src="resources/images/veg.png" style="width: 20px; margin-right: 3px;" > <img src="resources/images/cigarette.png" > <img src="resources/images/glass.png"></b>
</li> */

var histId = "", tempListIndex="";
	function getHistoryOfUser(id,listIndex,refresh=false){

		//console.log("Inside getHistoryOfUser(): id=",id,",listIndex= ",listIndex,",PreId= ",episdeId, ", tempListIndex= ",tempListIndex);
		//alert(id);
		histId = "";

		if(id == null || id == "undefined"){
			//alert(episodeId);alert(episdeId);
			histId = episodeId;
		}else{
			if(id == episdeId && refresh==false){
				console.log("Same episode button pressed!");
				return;
			}
			histId = id;
			//jk
			episdeId=id;

			$(".normalCol").css("background","#B8B8B8");
			//$("#id").remove("class","btn-primary");
			//$("#"+id).css("background","#367fa9");
			if(refresh == true){
				$("#"+tempListIndex).css("background","#367fa9");
			}else {
				console.log("Test = ",listIndex);
				$("#"+listIndex).css("background","#367fa9");
				tempListIndex=listIndex;
			}

		}
		//alert(histId);
		//Santhosh: Added to solve multi vitals display on click of episode multiple times
		$("#addVitalsDet").empty();
		getMasterVitals(false);
		//-----
	 getEpisodeVitals(histId);
	 getVitalDocument(histId);

	$("#prescriptionTableBody").html('');$("#notesTableBody").html('');
	$("#adviseTableBody").html('');$("#reportsTableBody").html('');
	var trStart = '<tr style="width:100%;">';
	var trEnd = '</tr>';


		$.ajax({
			type : "POST",
			url : serverPath+"/getDetailsByEpisodeIdv3?EpisodeId="+histId,
			headers: {
		        "deviceId":"ADMIN",
		    },
		    dataType: "json",
			type: "GET",
			success : function(data){
				if(data.code=="200"){
					//style="width:50%"
					var presList = "";var adviseList ="";var notesList = "";var reportList="";var reporfilename='';
					//For Prescription

					presList+='<tr><td style="font-weight:bold">'+"Medication Name"+'</td><td style="font-weight:bold">'+"Frequency"+'</td><td style="font-weight:bold">'+"Instruction"+'</td><td style="font-weight:bold">'+"Start Date"+'</td><td style="font-weight:bold">'+"Days"+'</td><td style="font-weight:bold">'+"Comments"+'</td><td style="font-weight:bold">'+""+'</td></tr>';
					adviseList+='<tr><td style="font-weight:bold">'+"Investigation"+'</td><td style="font-weight:bold">'+"Prescribed By"+'</td><td style="font-weight:bold">'+"Created At"+'</td><td style="font-weight:bold">'+"Advice"+'</td><td style="font-weight:bold">'+""+'</td></tr>';
					notesList+='<tr><td style="font-weight:bold">'+"Notes"+'</td><td style="font-weight:bold">'+"Created At"+'</td><td style="font-weight:bold">'+""+'</td><td style="font-weight:bold">'+""+'</td><td style="font-weight:bold">'+""+'</td></tr>';
					reportList+='<tr><td style="font-weight:bold">'+"Reports"+'</td></tr>';

					$.each(data.content,function(index,option){
						//alert(option.documentTypeId);
						if(option.documentTypeId == 1){
							//presList += trStart;
						if(option.medicationDetails != null && option.medicationDetails != 0){

					        $.each(option.medicationDetails,function(index1,option1){

							//var obj = jQuery.parseJSON(option1.details);
							if(option1.medicationName !='' && option1.dosage !=''){
								var MedName = option1.medicationName+" "+option1.dosage;
							}else if(option1.medicationName != "" ){
							    var MedName = option1.medicationName;
							}else{
							    var MedName = "-";
							}
							if(option1.frequency !=''){
								var Freq = option1.frequency;
							}else{
							    var Freq = "-"
							}
							if(option1.strBeginDate !=''){
								var Start = option1.strBeginDate;
							}else{
							    var Start = "-";
							}

							//if(option1.totalNoOfDays != null && option1.totalNoOfDays == '0'){
							//	var Days = 0 +' days';
							//}

							if(option1.totalNoOfDays != null && option1.totalNoOfDays > '1' && option1.totalNoOfDays != '0'){
								var Days = option1.totalNoOfDays +'days';
							}
							if(option1.totalNoOfDays != null && option1.totalNoOfDays <= '1' && option1.totalNoOfDays == '1'&& option1.totalNoOfDays != '0'){
								var Days = option1.totalNoOfDays +'day';
							}

							if(option1.instruction != ''){
							    var ins = option1.instruction;
							}else{
							    var ins = "-";
							}
							//console.log(option1.comments);
							if(option1.comments != ''){
							    var comment = option1.comments;
							}else{
							    comment = "-"
							}

							presList+='<tr><td>'+MedName+'</td><td>'+Freq+'</td><td>'+ins+'</td><td>'+Start+'</td><td>'+Days+'</td><td>'+comment+'</td>';
							presList += "<td><button style='' class='pull-right btn-primary btn dcbutton updateButton' onclick=\'updateMedication(" + JSON.stringify(option1) + ");\'>Update</button></td></tr>";
							/* var forum = obj.additionalDetail;
							 if(option.documentTypeName == "Prescription"){

					        } strBeginDate
							 for (var i = 0; i < forum.length; i++) {
							    var object = forum[i];

							    for (property in object) {
							        var value = object[property];
							        //console.log(value.keyName+"--"+value.value);

							        if(value.keyName == "medicationName"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'</td>';
								    }
							        else if(value.keyName == "frequency"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        else if(value.keyName == "instruction"){
							            presList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        else if(value.keyName == "beginDate"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        else if(value.keyName == "Days"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	presList += '<td style="width:25%">'+value.value+'Days</td></tr><tr>';
							        }
							        else if(value.keyName == "comments"){
							            presList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        else{
							            presList += '<td style="width:25%">""</td>';
							        }
							    }
							   // presList += trEnd;
							}
							//alert(option.documentTypeName);
							if(option.documentTypeName == "Prescription"){
					       	presList += trEnd;
					        }*/
						    $("#prescriptionTableBody").html(presList);


						});
						}
						}

					//For Advise/Investigation

					if(option.documentTypeId == 2){
					console.log("option:   ",option.investigationDetails);
						//alert(option.investigationDetails);
					if(option.investigationDetails != null && option.investigationDetails != 0){
						var LabCom = '';var CrPB = '';
					$.each(option.investigationDetails,function(index2,option2){
						//alert(option.documentTypeId);

							/* var obj = jQuery.parseJSON(option.details);
							var forum = obj.additionalDetail;
							if(option.documentTypeName == "Investigation"){
					        	adviseList += trStart;
					        }
							for (var i = 0; i < forum.length; i++) {
							    var object = forum[i];

							    for (property in object) {
							        var value = object[property];
							        //console.log(value.keyName+"--"+value.value);
							        if(value.keyName == "servicesName"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        else if(value.keyName == "prescribedBy"){
							        	console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        else if(value.keyName == "comments"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							        else if(value.keyName == "createdAt"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	adviseList += '<td style="width:25%">'+value.value+'</td>';
							        }
							    }
							}
							if(option.documentTypeName == "Investigation"){
								adviseList += trEnd;
					        } */

						if(option2.serviceName !=''){

							var SerName = option2.serviceName;
						}
						if(option2.comments !=''){

							 LabCom = option2.comments;
						}else{

							LabCom = '';
						}
						if(option2.strPrescribedDate !=''){

							var CrDate = option2.strPrescribedDate;
						}
					if(option2.prescribedBy !=''){

							CrPB = option2.prescribedBy;
						}else{

							CrPB = '';
						}
						/*
						if(option1.totalNoOfDays != undefined || option1.totalNoOfDays != ''){

							var Days = option1.totalNoOfDays;
						}
 */

 						adviseList+='<tr><td>'+SerName+'</td><td style="">'+CrPB+'</td><td >'+CrDate+'</td><td>'+LabCom+'</td>';
						adviseList += "<td><button style='' class='pull-right btn-primary btn dcbutton updateButton' onclick=\'updateAdvice(" + JSON.stringify(option2) + ");\'>Update</button></td></tr>";
						    $("#adviseTableBody").html(adviseList);


					});
					}
					}

					//For Notes
					if(option.documentTypeId == 8){
						//alert(option.notesDetails);
					if(option.notesDetails != null && option.notesDetails != 0){

					$.each(option.notesDetails,function(index3,option3){
						//alert(option.documentTypeId);
						/* if(option.documentTypeId == 8){
							var obj = jQuery.parseJSON(option.details);
							var forum = obj.additionalDetail;
							if(option.documentTypeName == "Notes"){
								notesList += trStart;
					        }
							for (var i = 0; i < forum.length; i++) {
							    var object = forum[i];

							    for (property in object) {
							        var value = object[property];
							        //console.log(value.keyName+"--"+value.value);

							        if(value.keyName == "comments"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	notesList += '<td style="width:85%">'+value.value+'</td>';
							        }
							        else if(value.keyName == "addedTime"){
							        	//console.log(value.keyName+"-->"+value.value);
							        	notesList += '<td style="width:15%">'+value.value+'</td>';
							        }

							    }
							}
							if(option.documentTypeName == "Notes"){
								notesList += trEnd;
					        }
						    $("#notesTableBody").html(notesList);
						 } */
						if(option3.strBeginDate !=''){

							var NotDate = option3.strBeginDate;
						}

						if(option3.comments !='' && option3.comments != undefined && option3.comments!= ','){

							var Note = option3.comments;
						}
						if(option3.createdBy != null){

							var notCB = option3.createdBy;
						}
						/* if(option3.createdOn != null){

							var notCO = option3.createdOn;
							var now = notCO;
							alert( now.customFormat( "#DD#/#MM#/#YYYY# #hh#:#mm#:#ss#" ) );
							//alert(datFor);
						}  */
						//alert(notCB);
						notesList+='<tr><td width="40%";>'+Note+'</td><td class="text-left" width="30%";>'+NotDate+'</td><td class="text-left" width="30%";>'+""+'</td><td class="text-left" width="30%";>'+""+'</td>';
						notesList += "<td><button style='' class='pull-right btn-primary btn dcbutton updateButton' onclick=\'updateNotes(" + JSON.stringify(option3) + ");\'>Update</button></td></tr>";
						$("#notesTableBody").html(notesList);
					});
				}
			}

					//For Report
					if(option.documentTypeId == 9){

						if(option.filePath != null){
							//alert();
							var	x =  option.filePath +'';
							//alert(x);
							var splt = x.split("/");
							var splRep = splt[splt.length-1];
							//alert(splRep);
							reportList += '<tr><td style="cursor:pointer;" "width:100%" onclick="openFileLocation(&quot;'+x+'&quot;);">'+splRep+'</td></tr>';

						}


				$("#reportsTableBody").html(reportList);

					}
				//alert(reportList);
				 });

				}

			}
		});
	}

	function navigateHome(){
	    if( localStorage.getItem("userGroupCode") === 'NUR')
	        window.location.href = 'patientList';
	    else
	        window.location.href = 'home';
	}

	function setFileName(input)
	{

		$("#showReportFileName").text($(input).val());
	}
	function openFileLocation(filePathLocation){
		//alert(filePathLocation);
		window.open(imagePath+filePathLocation);
	}

/* 	function initiateCall(calleProfileId){
		var proId = localStorage.getItem("productId");
    	if(proId == 1310){
    		alert("This Service is not enabled, Please contact Wenzins");
    	} else {
        	var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
        	win.addEventListener("resize", function(){
       	 		win.resizeTo(1024, 768);
        	});
    	}
	} */


    function initiateCall(calleProfileId){
		var proId = localStorage.getItem("productId");
    	if(proId == 1310){
    		alert("This Service is not enabled, Please contact Wenzins");
    	} else {
        	/* var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
        	win.addEventListener("resize", function(){
       	 		win.resizeTo(1024, 768);
        	}); */
    		$.ajax({
        		type : "POST",
        		url :   serverPath + "/intiateAndCloseCallv4?profileId="+localStorage.getItem("profileId")+"&calleProfileId="+calleProfileId+"&serverKeyId="+localStorage.getItem("serverKeyId")+"&initiate="+true,
        		success : function(data){
        		//	console.log(data);
        			 roomid = data.roomid;
        		//	alert(data);
        		//	console.log("new  "+data.content.weconurl);

        			videocalllink =  data.content.weconurl + data.roomid ;

        		/* 	 $.each(data.content,function(index, option) {
        				 console.log(option);
        				 var url = option.weconurl;
        			//	 alert(url);
        				 console.log("link"+url);
        				 var win = window.open(url);
        				  videocalllink =  url + roomid ;
        		//	var win = window.open(videocalllink , 'result', 'width=1024,height=768');
        	 }); */
        		//	 window.open(a);
        			 var win = window.open(videocalllink,'result', 'width=300,height=300');

        			 win.addEventListener("resize", function(){
              	 		win.resizeTo(1024, 768);
              	 	});
        		}
        	});
    	}
	}

function updateMedication(prescriptionRow) {
    console.log(prescriptionRow);

    $("#addMedicationModal").modal('show');
    $(".bClassM").parent().addClass('is-dirty');
    $('#fequencyId').val(prescriptionRow.frequency);
    $('#instructionId').val(prescriptionRow.instruction);
    $('#medicationNameId').val(prescriptionRow.medicationName);
    $('#prescribedByPre').val(prescriptionRow.prescribedBy);
    $("#prescribedByPre").parent().addClass('is-dirty');
    $('#startId').datepicker("setDate", new Date(prescriptionRow.beginDate));
    $('#days').val(prescriptionRow.days);
    if(prescriptionRow.dosage != ''){
        $("#dosageID").val(prescriptionRow.dosage);
    }
    if (prescriptionRow.comments != '') {
        $('#commentsMedId').val(prescriptionRow.comments);
    }

    $("#fequencyId").attr("disabled", false);
    $("#instructionId").attr("disabled", false);
    $("#medicationNameId").attr("disabled", false);
    $("#prescribedByPre").attr("disabled", true);
    $("#startId").attr("disabled", false);
    $("#days").attr("disabled", false);
    $("#commentsMedId").attr("disabled", false);


    $('#prescriptionStatus').val(prescriptionRow.status);

    $('#saveMedicationButton').hide();
    $('#addMedicationButton').hide();
    $('#updateMedicationButton').show();
    $('#statusSelector').show();


    $('#updateMedicationButton').unbind().click(function () {
    	 		if($("#medicationNameId").val() == ''){
    					toastr.warning('Please fill mandatory fields');
    					return;
    			}
    	 		if($("#days").val() == ''){
    					toastr.warning('Please fill mandatory fields');
    					return;
    			}

       	    $("#medEpisodeId").val(prescriptionRow.epsiodeId);
       	    $("#medicationDetailId").val(prescriptionRow.id);
       	    $("#documentTypeId").val(1);
       	    $("#strPrescribedDate").val(prescriptionRow.strPrescribedDate);
       	    $("#prescribedBy").val($("#prescribedByPre").val());

            		medicationJSON = [];MedicationFinalJSON = [];

            		medicationJSON.push({keyName : "id",value : 0 });
            		medicationJSON.push({keyName : "medicationId",value : ""});
            		medicationJSON.push({keyName : "medicationName",value : $("#medicationNameId").val()});
            		medicationJSON.push({keyName : "dosage", value : $("#dosageID").val()});
            		medicationJSON.push({keyName : "frequency", value : $('#fequencyId').val()});
            		medicationJSON.push({keyName : "instructions", value : $('#instructionId').val()});
            		medicationJSON.push({keyName : "beginDate", value : $('#startId').val()});
            		medicationJSON.push({keyName : "Days", value : $("#days").val()});
            		medicationJSON.push({keyName : "commentPrescription", value : $("#commentsMedId").val()});
            	 	medicationJSON.push({keyName : "status",value : 1 });
            		medicationJSON.push({keyName : "medicalReportId",value : -1 });
            		MedicationFinalJSON.push(medicationJSON);

                    prefixStrForPrescription='{\"additionalDetail\":';
            		var additonalDetails = prefixStrForPrescription+JSON.stringify(MedicationFinalJSON)+"}";
            		$("#additionalDetailJson").val(additonalDetails);

        var form1 = document.getElementById('saveMedicationId');
        console.log("form1: ",form1);
        var formData = new FormData(form1);

        if (formData != null && formData != '') {
            $("#saveMedicationButton").attr("disabled", true);
            $("#updateMedicationButton").attr("disabled", true);

            $.ajax({
                type: "POST",
                url: serverPath + "/updateDocuments",
	 			data: formData,
	 			dataType:"json",
	 		    mimeType: "multipart/form-data",
	 			async : false,
	 		    cache : false,
	 			contentType : false,
	 		    processData : false,
                success: function (data) {
                    if (data.code = "200") {

                        toastr.success('Prescription Updated Successfully', '');

                        canMed();

                        $("#saveMedicationButton").attr("disabled", false);
                        $("#updateMedicationButton").attr("disabled", false);

                        getHistoryOfUser(episdeId,"",true);

                    }
                },
                error: function (error) {

                    $("#saveMedicationButton").attr("disabled", false);
                    $("#updateMedicationButton").attr("disabled", false);

                    if(error.responseJSON.code === 401 && error.responseJSON.exception.includes("JWT expired")){
                        toastr.error("Session Expired! Redirecting to Login Page...");
                        setTimeout(function(){window.location.href = "logout";},3000);
                        return;
                    }
                    if (error.responseJSON.message != '' || error.responseJSON.message != null || error.responseJSON.message != undefined) {
                        toastr.error(error.responseJSON.message);
                    } else {
                        toastr.error("Server Error");
                    }
                }
            });
        } else {

            $('#dynTableMed').hide();
        }
    })
}

function updateAdvice(prescriptionRow) {
    console.log("Prescription: ",prescriptionRow);

    		$("#addVisitForm").modal('show');
    		var prescribedBy = $("#loggedInUserFullName").text();
    		$("#prescribedByAdv").parent().addClass('is-dirty');
    		$("#prescribedByAdv").val($("#loggedInUserFullName").text());
    		$("#serviceTypeId").parent().addClass('is-dirty');
    		$("#serviceTypeId").val(prescriptionRow.serviceName);
    		$("#fromDate").parent().addClass('is-dirty');
    		$("#fromDate").datepicker("setDate", new Date(prescriptionRow.prescribedDate));

    if (prescriptionRow.comments != '') {
            		$("#advComments").parent().addClass('is-dirty');
                    $("#advComments").val(prescriptionRow.comments);
    }

    $("#prescribedByAdv").attr("disabled", true);
    $("#fromDate").attr("disabled", false);
    $("#advComments").attr("disabled", false);

    $('#saveAdviceButton').hide();
    $('#addAdviceButton').hide();
    $('#updateAdviceButton').show();
    $('#statusSelector').show();

    $('#updateAdviceButton').unbind().click(function () {

        	 		if($("#serviceTypeId").val() == ''){
        					toastr.warning('Please fill mandatory fields');
        					return;
        			}
        	 		if($("#fromDate").val() == ''){
        					toastr.warning('Please fill mandatory fields');
        					return;
        			}

            $("#advUserId").val(prescriptionRow.userId);
       	    $("#advEpisodeId").val(prescriptionRow.epsiodeId);
       	    $("#investigationId").val(prescriptionRow.id);
       	    $("#documentTypeIdAdv").val(2);
       	    $("#strAdviceDate").val(prescriptionRow.strPrescribedDate);
       	    $("#prescribedByAdvice").val($("#loggedInUserFullName").text());
       	    $("#prescribedByAdv").val($("#loggedInUserFullName").text());

            		adviceJSON = [];adviceFinalJSON = [];
            		adviceJSON.push({keyName : "InvestigationType",value :"" });
            		adviceJSON.push({keyName : "investigationName",value : $("#serviceTypeId").val()});
            		adviceJSON.push({keyName : "prescribedBy",value : $("#loggedInUserFullName").text()});
            		adviceJSON.push({keyName : "dateTime", value : $("#fromDate").val()});
            		adviceJSON.push({keyName : "Labcomments", value : $("#advComments").val()});
            		adviceJSON.push({keyName : "id", value : ""});
            	 	adviceJSON.push({keyName : "status",value : 1 });
            		adviceJSON.push({keyName : "medicalReportId",value : -1 });
            		adviceFinalJSON.push(adviceJSON);
                    prefixStrForAdvice='{\"additionalDetail\":';
            		var additonalDetails = prefixStrForAdvice+JSON.stringify(adviceFinalJSON)+"}";
            		$("#adviceJSON").val(additonalDetails);

        var form1 = document.getElementById('saveAdviceId');
        console.log("form1: ",form1);
        var formData = new FormData(form1);

       	    $("#prescribedBy").val($("#loggedInUserFullName").text());
       	    $("#prescribedByAdv").val($("#loggedInUserFullName").text());
       	    console.log("prescribedby: ", $("#prescribedByAdvice").val(),", ",$("#prescribedByAdv").val());

        if (formData != null && formData != '') {
            $("#saveAdviceButton").attr("disabled", true);
            $("#updateAdviceButton").attr("disabled", true);

            $.ajax({
                type: "POST",
                url: serverPath + "/updateDocuments",
	 			data: formData,
	 			dataType:"json",
	 		    mimeType: "multipart/form-data",
	 			async : false,
	 		    cache : false,
	 			contentType : false,
	 		    processData : false,
                success: function (data) {
                    if (data.code = "200") {

                        toastr.success('Investigation Updated Successfully', '');

	 					clearAdvModal();

                        $("#saveAdviceButton").attr("disabled", false);
                        $("#updateAdviceButton").attr("disabled", false);

                        getHistoryOfUser(episdeId,"",true);
                    }
                },
                error: function (error) {
                    $("#saveAdviceButton").attr("disabled", false);
                    $("#updateAdviceButton").attr("disabled", false);

                    if(error.responseJSON.code === 401 && error.responseJSON.exception.includes("JWT expired")){
                        toastr.error("Session Expired! Redirecting to Login Page...");
                        setTimeout(function(){window.location.href = "logout";},3000);
                        return;
                    }
                    if (error.responseJSON.message != '' || error.responseJSON.message != null || error.responseJSON.message != undefined) {
                        toastr.error(error.responseJSON.message);
                    } else {
                        toastr.error("Server Error");
                    }
                }
            });
        } else {

            $('#dynTableMed').hide();
        }
    })
}

function updateNotes(prescriptionRow) {
    console.log(prescriptionRow);

    		$("#addNotesModal").modal('show');
    		$("#noeComments").val(prescriptionRow.comments);

    $("#noeComments").attr("disabled", false);

    $('#saveNotesButton').hide();
    $('#updateNotesButton').show();

    $('#updateNotesButton').unbind().click(function () {
           if($("#noeComments").val() == ''){
           		toastr.warning('Please fill mandatory fields');
 				return;
           }

            $("#idUserNote").val(prescriptionRow.userId);
       	    $("#episodeIdUp").val(prescriptionRow.epsiodeId);
       	    $("#notesId").val(prescriptionRow.id);
       	    $("#documentTypeId").val(8);
       	    $("#strPrescribedDate").val(prescriptionRow.strPrescribedDate);
       	    $("#prescribedBy").val(prescriptionRow.prescribedBy);

            		notesJSON = [];notesFinalJSON = [];
            		notesJSON.push({keyName : "comments",value :$("#noeComments").val() });
            		notesJSON.push({keyName : "addedTime",value : $("#fromDate").datepicker("setDate", new Date(prescriptionRow.createdOn))});
            		notesJSON.push({keyName : "id", value : ""});
            	 	notesJSON.push({keyName : "status",value : 1 });
            		notesJSON.push({keyName : "medicalReportId",value : -1 });
            		notesFinalJSON.push(notesJSON);

                    prefixStrForAdvice='{\"additionalDetail\":';
            		var additonalDetails = prefixStrForAdvice+JSON.stringify(notesFinalJSON)+"}";
            		$("#notesJSON").val(additonalDetails);

        var form1 = document.getElementById('addNotesForm');
        console.log("form1: ",form1);
        var formData = new FormData(form1);

        if (formData != null && formData != '') {
            $("#saveNotesButton").attr("disabled", true);
            $("#updateNotesButton").attr("disabled", true);

            $.ajax({
                type: "POST",
                url: serverPath + "/updateDocuments",
	 			data: formData,
	 			dataType:"json",
	 		    mimeType: "multipart/form-data",
	 			async : false,
	 		    cache : false,
	 			contentType : false,
	 		    processData : false,
                success: function (data) {
                    if (data.code = "200") {

                        toastr.success('Investigation Updated Successfully', '');

	 					canNote();

                        $("#saveNotesButton").attr("disabled", false);
                        $("#updateNotesButton").attr("disabled", false);

                        getHistoryOfUser(episdeId,"",true);
                    }
                },
                error: function (error) {
                    $("#saveNotesButton").attr("disabled", false);
                    $("#updateNotesButton").attr("disabled", false);

                    if(error.responseJSON.code === 401 && error.responseJSON.exception.includes("JWT expired")){
                        toastr.error("Session Expired! Redirecting to Login Page...");
                        setTimeout(function(){window.location.href = "logout";},3000);
                        return;
                    }
                    if (error.responseJSON.message != '' || error.responseJSON.message != null || error.responseJSON.message != undefined) {
                        toastr.error(error.responseJSON.message);
                    } else {
                        toastr.error("Server Error");
                    }
                }
            });
        } else {

            $('#dynTableMed').hide();
        }
    })
}


	//=======================NEXT AND PREVIOUS BUTTON FOR EPISODE============================
	function next(id){

		if(test==s-2){
			document.getElementById("nextId").disabled = true;
		}


		document.getElementById("prevId").disabled = false;

		test=test+1;
		var previous=test-4;

			document.getElementById(previous).style.display = "none";
			document.getElementById(test).style.display = '';

			document.getElementById("prevId").style.display = "";           //to display prev button
		}


	function previous(id){

		document.getElementById("nextId").disabled = false;

			 if(test==4){
				document.getElementById("prevId").disabled = true;
			}

			var previous=test-4;

				document.getElementById(test).style.display = 'none';
				 document.getElementById(previous).style.display = '';

			test=test-1;
			}
	//============================================================================================================

	function canMed(){

		$("#addMedicationModal").modal('hide');
		 //dosageID startId days commentsMedId
		 $(".bClassM").val('');
		 $(".bClassM").parent().removeClass('is-dirty');
		 $('#fequencyId').val('0-0-1');
		$('#instructionId').val('Before Food');
		$("#dynTableMed").hide();
		$("#medAdd").empty();
		 /* $("#startId").val('');
		 $("#dosageID").val('');
		 $("#days").val('');
		 $("#commentsMedId").val(''); */

	}
	function canNote(){
		//alert();
		$("#addNotesModal").modal('hide');
		//$('.getNotes').text('');
		$('textarea').val('')
	}
	function clearRep(){
		$("#addReportModal").modal('hide');
		$('#reportFileId').val('');
		$(".getReport").empty();
		$('#showReportFileName').text('');
	}
	function reportAdvice(episodeId,docId,id)
	   {
		// alert("Fdgfg");
		  var appendPrescriptionImage = "";
		  if(docId==1){
			  $("#addreport").html('');
			  $(".preReport").each(function(){

				  var spltAttr = $(this).attr("attr");
				  if(spltAttr==id){
				  var splitHere =  $(this).val().split("/");
				  var splitHere1 = $(this).val().split(",");

               appendPrescriptionImage+= "<br><a class='text-left' path='&quot;"+splitHere1+"&quot'' "
               +"style='cursor:pointer;padding-top:0px; padding-bottom: 5px;color:rgb(63, 81, 181);"
               +"font-size: 15px;' onclick='javascript:showInOtherPage("+id+",&quot;"+splitHere1+"&quot;)' "
               +" >"+splitHere[splitHere.length-1]+"</a>";
				  }
 		  });

			  $("#addreport").html(appendPrescriptionImage);


			  if(appendPrescriptionImage==""){

			    mes = "No records found";
				$('#demo-show-snackbar').trigger('click');
		  }else{
				$("#reportAdvice").modal('show');
		  }
		  }


		  if(docId==2){
			  $("#addreport").html(''); var appendPrescriptionImage = "";
			  $(".repReport").each(function(){
				  var spltAttr = $(this).attr("attr");
				  if(spltAttr==id){
				  var splitHere =  $(this).val().split("/");
				  var splitHere1 = $(this).val().split(",");



               appendPrescriptionImage+= "<br><a class='text-left' path='&quot;"+splitHere1+"&quot'' "
               +"style='cursor:pointer;padding-top:0px; padding-bottom: 5px;color:rgb(63, 81, 181);"
               +"font-size: 15px;' onclick='javascript:showInOtherPage("+id+",&quot;"+splitHere1+"&quot;)' "
               +">"+splitHere[splitHere.length-1]+"</a>";
				  }



 		  });

			  $("#addreport").html(appendPrescriptionImage);

			  if(appendPrescriptionImage == ""){
				  console.log(appendPrescriptionImage);
				    mes = "No records found";
   				$('#demo-show-snackbar').trigger('click');
   		  }else{
   				$("#reportAdvice").modal('show');
   		  }

		  }


}

	function completeConsultation(){
		//alert(appointmentId);
		$.ajax({
		   type:"POST",
		   url: serverPath+"/completeConsultation?appointmentId="+appointmentId,
		   success: function(data){
			   if(data.code == '200'){
				   //alert(data.message);
				   window.location.href= 'list';
				   toastr.success("Appointment Completed Successfully");
			   }
		   }
		})
	}

	//Santhosh: Get Time from given string date
function strToDate(dtStr) {
	//console.log("Inside strToDate()->",dtStr)
  if (!dtStr) return null
  let dateParts = dtStr.split("/");
  let timeParts = dateParts[2].split(" ")[1].split(":");
  dateParts[2] = dateParts[2].split(" ")[0];
  // month is 0-based, that's why we need dataParts[1] - 1
  return dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0], timeParts[0], timeParts[1], timeParts[2]);
}

function getCustomTime(time) {
	//console.log("Inside getCustomTime()->",time)
  let ampm = "am";

  var h = time.getHours();
  var m = time.getMinutes();
  // add a zero in front of numbers<10
  if (h >= 12) {
    ampm = "pm";
    h = h % 12 === 0 ? h : h % 12;
  }

  h = checkTime(h);
  m = checkTime(m);

  //return `${h}:${m} ${ampm}`;
  return h + ':' + m  + ' ' + ampm;
}

function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}
</script>
<!-- <tr>
	<td>4.</td>
	<td>Fix and squish bugs</td>
	<td>
 		<div class="progress progress-xs progress-striped active">
   			<div class="progress-bar progress-bar-success" style="width: 90%"></div>
 		</div>
	</td>
	<td><span class="badge bg-green">90%</span></td>
</tr> -->
</html>
