<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title> DashBoard</title>

    <link rel="stylesheet" href="resources/css/bootstrap.min.css" />


    <link rel="stylesheet" href="resources/css/toastr.min.css" />
    <link rel="stylesheet" href="resources/css/bootstrap-datepicker.css">

    <link href="resources/css/AdminLTE.css" rel="stylesheet" />
    <link rel="stylesheet" href="resources/css/material.css" />
    <link href="resources/css/_all-skins.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="resources/css/font-awesome.min.css" />
    <!----<link rel="stylesheet" href="resources/customCSS/vitalTrend.css" />
    <script src="resources/customJS/vitalTrend.js"></script>--->
    <script src="resources/js/jQuery-2.1.4.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>

    <script src="resources/js/fastclick.min.js"></script>
    <script src="resources/js/app.min.js"></script>
    <script src="resources/js/demo.js"></script>
    <script defer src="resources/js/material.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="resources/js/bootstrap3-typeahead.js" type="application/javascript"></script>
    <script src="resources/js/jquery.mask.js"></script>

    <script src="resources/js/toastr.min.js"></script>
    <script src="resources/js/customHTML.js"></script>
    <script src="resources/js/bootstrap-datepicker.js" type="application/javascript"></script>
    <script src="resources/customJS/bloodVitals.js"></script>
    <script src="resources/customJS/prescription.js"></script>
    <script src="resources/customJS/videoCall.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/dayjs"></script>
    <script src="https://cdn.jsdelivr.net/npm/dayjs/plugin/customParseFormat.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/dayjs/plugin/utc.js"></script>
    <script>dayjs.extend(window.dayjs_plugin_customParseFormat)</script>
    <script>dayjs.extend(window.dayjs_plugin_utc)</script>

    <style>
        .panel-title {
            position: relative;
            cursor: pointer;
        }

        .panel-body {
            padding: 0px;
        }

        .panel-heading {
            padding: 4px 10px 4px 0px;
        }

        .panel-title::after {
            content: "\f107";
            color: #333;
            top: -2px;
            right: 0px;
            position: absolute;
            font-family: "FontAwesome";
        }

        .panel-title[aria-expanded="true"]::after {
            content: "\f106";
        }

        /*
 * Added 12-27-20 to showcase full title clickthrough
 */

        .panel-heading-full.panel-heading {
            padding: 0;
        }

        .panel-heading-full .panel-title {
            padding: 10px 15px;
        }

        .panel-heading-full .panel-title::after {
            top: 10px;
            right: 15px;
        }



        .navbar-t-centered {
            position: absolute;
            left: 34%;
            display: block;
            text-align: center;
            color: #fff;
            top: 0px;
            padding: 15px;
            font-weight: 400;
            font-size: 12px;

        }

        @media screen and (min-width:768px) {
            .navbar-t-centered {
                position: absolute;
                left: 38%;
                display: block;
                /*  width: 160px; */
                text-align: center;
                color: #fff;
                top: 0px;
                padding: 15px;
                font-weight: 400;
                font-size: 22px;

            }
        }

        .card-main-conatiner {
            width: 192px;
            height: 96px;
            margin: 5px;
            box-shadow: 5px 10px 10px lightgray;
        }

        .card-scroll-container {
            /*height: 105px;*/
            /* max-width: 300px;*/
            margin: unset;
            /*border: 1px solid sienna;*/
            overflow-x: scroll;
            overflow-y: hidden;
            margin: 5px;
        }

        .card-scroll-container::-webkit-scrollbar {
            height: 5px;
        }

        .card-scroll-container::-webkit-scrollbar-track {
            /*background-color: lightgray;*/
        }

        .card-scroll-container::-webkit-scrollbar-thumb {
            /*box-shadow: inset 0 0 6px #3c8dbc;*/
            background-color: #3c8dbc;
            border-radius: 8px;
        }

        .card-inner {
            height: 100%;
            white-space: nowrap;
            text-align: center;
        }

        .card-floatLeft {
            width: 140px;
            margin: 3px 0px;
            display: inline-block;
            /*border: 1px solid lightgray;*/
            border-left: 1px solid lightgray;
            border-right: 1px solid lightgray;
            text-align: center;
            /*box-shadow: 2px 2px 2px lightgray; */
        }

        .card-floatLeft-2digitValue {
            width: 120px;
            margin: 3px 0px;
            display: inline-block;
            border-left: 1px solid lightgray;
            border-right: 1px solid lightgray;
            text-align: center;
        }

        .card-floatLeft-bloodAnalyserValue {
            width: 120px;
            margin: 3px 0px;
            display: inline-block;
            text-align: center;
        }

        .vital-title {
            font-size: 14px;
            text-align: center;
            font-weight: 500;
            margin: 4px;
        }

        .vital-actual-value {
            margin: 2px;
            font-weight: 600;
            font-size: 16px;
        }

        .vital-recorded-time {
            margin: unset;
            font-size: 12px;
        }

        .bloodVitalsMainHeader {
            /* color:#3c8dbc;*/
            color: blue;
            margin: 0px;
        }
        .accBtnBasic,
        .accBtnCardiac,
        .accBtnHarmone,
        .accBtnInfection,
        .accBtnDiabetic,
        .accBtnRespiratory,
        .accBtnVector,
        .accBtnFecal {
            color: #0437F2;
        }

        .bvLevelImg {
            width: 50%;
        }

        .bvResult {
            color: blue;
        }

        .columnLevl {
            float: left;
            width: 25%;
            padding: 5px;
        }

        .tooltip {
            font-family: Georgia;
            white-space: normal;

        }

        .tooltipTitle p {
            margin-bottom: 0px;
            font-size: 12px;
            color: black;
        }

        .tooltip.left .tooltip-inner {
            text-align: start;
            /* background-color: #367fa9;*/
            background-color: white;
            border-radius: 5px;
            border: 1px solid gray;
            font-size: 2px;
        }
        .close-button {
          background-color: royalblue;
          color: white;
          border: none;
          padding: 8px 16px;
          font-size: 14px;
          border-radius: 4px;
          cursor: pointer;
          outline: none;
          margin-top: 10px;
          margin-bottom: 10px;
        }

        .close-button:hover {
          background-color: darkred;
        }

    </style>
</head>

<script>

    function myfun() {
        //alert("The page has been refreshed.");
        console.log("******On unload***********");
    }
</script>

<body class="hold-transition skin-blue sidebar-mini" onbeforeunload="myfun()">
    <div class="wrapper">
        <div id="load2"
            style="position: fixed; left: 50%; top: 40%; width: 120px; height: 100%; z-index: 9999;display:none">
            <img style="" src="resources/images/icons/loading.gif" alt="loading" />
            <br />
        </div>
        <header class="main-header">
            <nav class="navbar navbar-static-top" role="navigation"
                style="margin-left:0% !important ;max-height:50px; background-color:#3c8dbc">
                <!-- Sidebar toggle button-->
                <!-- <img src="resources/images/logowhite.png" class="user-image" alt="User Image" style="height: 50px; padding-left: 5px; margin-left: 5px;"> -->
                <img src="resources/images/WriztoLogoBox.jpg" onclick="navigateHome();" class="user-image"
                    alt="User Image" style="cursor: pointer;height: 50px; padding-left: 5px; margin-left: 5px;">


                <span class="navbar-t-centered">Wrizto Healthcare System</span>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->

                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="resources/images/avatar.png" class="user-image" alt="User Image">
                                <span class="hidden-xs" id="loggedInUserFullName"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="resources/images/avatar.png" class="img-circle" alt="User Image">
                                    <p><span id="loggedInUserFullNameSpan"></span>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="col-xs-8 text-center" id="nurseNameForActive"
                                        style="padding-left: 0px;padding-right: 0px;"><span
                                            style="font-size: 12px; margin-left: -33px;"></span>
                                        <!-- <a id= "videoIconId" style="border-radius: 50%; ;margin-left: 5px;background-color: green" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                  				   <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a> -->
                                    </div>
                                    <!--
                                  <div class="col-xs-4 text-right" style="padding-left: 5px;padding-right: 0px;padding-top:5px;">
                                    <a href="patientList">Patient List</a>
                                  </div>
                                   -->
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                    </div>
                                    <div class="pull-right" id='patListLogout'>
                                        <a onclick="logout();" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->

                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="margin-left:0% !important;padding-bottom:40px;">
            <!-- Content Header (Page header) -->
            <section class="content-header" style="display:inline-flex;width:100%;">

                <strong style="font-size:18px;" id="dash_id"></strong>

                <div id="chronologicalView" class="pull-right text-right" style="width:83%;height:40px;float:right">
                </div>
            </section>

            <!-- Main content -->
            <section class="content">

                <div class="row">
                    <div class="col-md-3">

                        <!-- Profile Image -->
                        <div class="box box-primary ">
                            <div class="box-body box-profile text-center">
                                <img class="profile-user-img img-responsive img-circle"
                                    src="resources/images/user-silhouette.png" alt="User profile picture">
                                <h5 class="profile-username text-center" id="addPrfName"
                                    style="margin-bottom: 15px;font-size:16px;"></h5>
                                <p class="text-muted text-center" id="uhidId" style="margin-bottom: 5px;"></p>
                                 <p class="text-muted text-center" id="abhaNo" style="margin-bottom: 5px;"></p>

                                <ul class="list-group list-group-unbordered" style="margin-bottom:0px" id="addDet">
                                </ul>
                                <p class="text-center" id="patientComplaint"
                                  style="font-weight: 600"></p>
                                <div class="col-md-12" id="dashBoardHideShoeVideoIconId">
                                    <a style="padding: 12px 12px 10px; border-radius: 50%;box-shadow: 4px 6px ;margin-left: -8px;"
                                        class="btn btn-primary" title="videoCall" onclick="initiateCall();"><b>
                                            <i style="color:white;font-size:20px"
                                                class="fa fa-video-camera"></i></b></a>
                                    <!-- <a style=" padding: 12px 12px 10px; border-radius: 50%;box-shadow: 4px 6px ;" class="btn btn-primary" title="Message"><b>
                   <i style="color:white;font-size:20px" class="fa fa-commenting"></i></b></a> -->
                                </div>

                            </div><!-- /.box-body -->
                        </div><!-- /.box -->

                    </div><!-- /.col -->
                    <div class="col-md-9">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li onclick="refreshVitalsPage()" class="active"><a href="#activity"
                                        data-toggle="tab">Vitals</a></li>
                                <li onclick=""><a href="#timeline" data-toggle="tab">Advice</a>
                                 <!---<li onclick=""><a href="#vitaltrend" data-toggle="tab">Trends</a>--->
                                </li>

                                <button style="float: right; cursor: pointer; margin-top: 5px;background:#3c8dbc"
                                    class="btn btn-primary" onclick="EmrReport()">EMR Report <i class=""></i></button>
                                <!-- <button style=" float: right; cursor: pointer; margin-top: 5px;background:white" class="btn"
                                        onclick="refreshPage()">Refresh <i class="fa fa-refresh"
                                            style="color:black"></i></button> -->
                                <!--  <li><a href="#settings" data-toggle="tab">Settings</a></li> -->
                            </ul>
                            <div class="tab-content">
                                <div class="active tab-pane" id="activity"
                                    style="padding-left: 10px; padding-right: 10px;">


                                    <div class="user-block" style="padding-top: 10px; padding-bottom: 15px;">
                                        <!--   <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image"> -->
                                        <span class='username' style="margin-left: 0px;">
                                            <a href="#activity"
                                                style="font-size:16px;color:#3a86bd;display:none;">Vitals</a>
                                            <a href='#'
                                                class='pull-right btn-box-tool'><!-- <i class='fa fa-times'></i> --></a>
                                        </span>
                                        <span class='description'
                                            style="margin-left: 0px; padding-top: 10px; padding-left: 5px;"
                                            id="vitDate"></span>
                                    </div><!-- /.user-block -->


                                    <div class="row" id="addVitalsDet">
                                    </div>


                                    <div style="display:flex">
                                        <h6 style="font-size:16px;color:#3a86bd;flex: 0 0 95%;margin:10px 0px">Pathology
                                            Format: </h6>
                                        <div style="display:flex;justify-content:end;align-items:center;flex:0 0 5%;">
                                            <span class="glyphicon glyphicon-info-sign" id="toolTipDb2"
                                                data-placement="left" data-toggle="tooltip" data-html="true"
                                                title="<div class='tooltipTitle'><p style='color:#367fa9;text-align:center;font-weight:600;'>STATUS</p><p><img src='resources/images/rsz_dry-clean.png' alt='Snow' style='width:10%;margin-left:2px;'/> Normal</p> <p> <img src='resources/images/rsz_right-arrowhead.png' alt='Snow' style='width:10%;margin-left:2px;'/> Moderate</p> <p> <img src='resources/images/rsz_rhombus.png' alt='Snow' style='width:10%;margin-left:2px;'/> Abnormal</p> <p> <img src='resources/images/rsz_pentagon.png' alt='Snow' style='width:10%;margin-left:2px;'/> Very-Abnormal</p></div>">
                                                <!-- title="<div class='tooltipTitle'><p>- Normal (72 ~ 100 bpm)</p><p>- Abnormal (<72 / >100 bpm)</p></div>" > -->

                                            </span>
                                        </div>
                                    </div>

<div class="panel-group" id="accordionbv">

        <!-- Level Indicator Panel -->
        <!--
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapseLvl"><button class="btn btn-link accBtnBasic" type="button ">Level Indicator</button></h4>
          </div>

          <div id="collapseLvl" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="table-responsive">
                  <div id="levlStatusDiv"></div>
              </div>
            </div>
          </div>
        </div>
         -->

        <!-- 1st Panel -->
       <div class="panel panel-default">
                                                   <div class="panel-heading">
                                                       <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse"
                                                           data-target="#collapseOnebv"><button id="basicVitalAccordin"
                                                               class="btn btn-link accBtnBasic" type="button ">PATHOLOGY FORMAT</button></h4>
                                                   </div>

                                                   <div id="collapseOnebv" class="panel-collapse collapse">
                                                       <div class="panel-body">
                                                           <div class="table-responsive">
                                                               <table class="table table-bordered table-striped">
                                                                   <thead id="basicHeader">
                                                                   </thead>
                                                                   <tbody id="basicBody">
                                                                   </tbody>
                                                               </table>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                    </div>
                                    <div style="display:flex">
                                        <h6 style="font-size:16px;color:#3a86bd;flex: 0 0 95%;margin:10px 0px">Blood
                                            Vitals: </h6>
                                        <div style="display:flex;justify-content:end;align-items:center;flex:0 0 5%;">
                                            <span class="glyphicon glyphicon-info-sign" id="toolTipDb1"
                                                data-placement="left" data-toggle="tooltip" data-html="true"
                                                title="<div class='tooltipTitle'><p style='color:#367fa9;text-align:center;font-weight:600;'>STATUS</p><p><img src='resources/images/rsz_dry-clean.png' alt='Snow' style='width:10%;margin-left:2px;'/> Normal</p> <p> <img src='resources/images/rsz_right-arrowhead.png' alt='Snow' style='width:10%;margin-left:2px;'/> Moderate</p> <p> <img src='resources/images/rsz_rhombus.png' alt='Snow' style='width:10%;margin-left:2px;'/> Abnormal</p> <p> <img src='resources/images/rsz_pentagon.png' alt='Snow' style='width:10%;margin-left:2px;'/> Very-Abnormal</p></div>">
                                                <!-- title="<div class='tooltipTitle'><p>- Normal (72 ~ 100 bpm)</p><p>- Abnormal (<72 / >100 bpm)</p></div>" > -->

                                            </span>
                                        </div>

                                    </div>

                                    <div class="panel-group" id="accordion">

                                        <!-- Level Indicator Panel -->
                                        <!--
                      	<div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse" data-target="#collapseLvl"><button class="btn btn-link accBtnCardiac" type="button ">Level Indicator</button></h4>
						    </div>

						    <div id="collapseLvl" class="panel-collapse collapse">
						      <div class="panel-body">
	      						<div class="table-responsive">
									<div id="levlStatusDiv"></div>
								</div>
						      </div>
						    </div>
						  </div>
                      	 -->

                                        <!-- 1st Panel -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse"
                                                    data-target="#collapseOne"><button
                                                        class="btn btn-link accBtnCardiac" type="button" id="cardiacMarkerAccordin">CARDIAC
                                                        MARKER</button></h4>
                                            </div>

                                            <div id="collapseOne" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead id="cardiacHeader">
                                                            </thead>
                                                            <tbody id="cardiacBody">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- 2nd Panel -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse"
                                                    data-target="#collapse2"><button class="btn btn-link accBtnHarmone"
                                                        type="button" id="harmoneMarkerAccordin">HARMONE MARKER</button></h4>
                                            </div>

                                            <div id="collapse2" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead id="harmoneHeader">
                                                            </thead>
                                                            <tbody id="harmoneBody">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- 3rd Panel -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse"
                                                    data-target="#collapse3"><button
                                                        class="btn btn-link accBtnInfection" type="button" id="infectionMarkerAccordin">INFECTION
                                                        MARKER</button></h4>
                                            </div>

                                            <div id="collapse3" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead id="infectionHeader">
                                                            </thead>
                                                            <tbody id="infectionBody">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- 4th Panel -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse"
                                                    data-target="#collapse4"><button class="btn btn-link accBtnDiabetic"
                                                        type="button" id="diabeticMarkerAccordin">DIABETIC MARKER</button></h4>
                                            </div>

                                            <div id="collapse4" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead id="diabeticHeader">
                                                            </thead>
                                                            <tbody id="diabeticBody">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- 5th Panel -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse"
                                                    data-target="#collapse5"><button
                                                        class="btn btn-link accBtnRespiratory"
                                                        type="button" id="respiratoryMarkerAccordin">RESPIRATORY MARKER</button></h4>
                                            </div>

                                            <div id="collapse5" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead id="respiratoryHeader">
                                                            </thead>
                                                            <tbody id="respiratoryBody">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- 6th Panel -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse"
                                                    data-target="#collapse6"><button class="btn btn-link accBtnVector"
                                                        type="button" id="vectorBorneAccordin">VECTOR BORNE MARKER</button></h4>
                                            </div>

                                            <div id="collapse6" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead id="vectorBorneHeader">
                                                            </thead>
                                                            <tbody id="vectorBorneBody">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- 7th Panel -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title bloodVitalsMainHeader" data-toggle="collapse"
                                                    data-target="#collapse7"><button class="btn btn-link accBtnFecal"
                                                        type="button" id="faecalMarkerAccordin">FAECAL MARKER</button></h4>
                                            </div>

                                            <div id="collapse7" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead id="fecalHeader">
                                                            </thead>
                                                            <tbody id="fecalBody">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>




                                    <div id="electroCardioHeader" class="user-block" style="padding-top: 10px; padding-bottom: 15px;">
                                        <!--   <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image"> -->
                                        <h6 style="font-size:16px;color:#3a86bd;">Electrocardiogram (ECG): </h6>

                                       <div id="add3LeadEcgsHeader">  <h6 style="font-size:16px;color:#000000;">3 LEAD ECG </h6></div>
                                         <div class="row" id="add3LeadEcgs">

                                          </div>

                                        <!--
                                        <span class='username' style="margin-left: 0px;font-size:16px;color:#3a86bd;">
                                        <a href="#" style="font-size:16px;color:#3a86bd;">Electrocardiogram (ECG)</a>
                                        <a href='#' class='pull-right btn-box-tool'></a>
                                        -->
                                        </span>
                                        <!-- <span class='description' style="margin-left: 0px; padding-top: 10px; padding-left: 5px;" id="addDates">NO RECORD FOUND</span> -->
                                    <!-- /.user-block -->
                                     <div id="addEcgsHeader"><h6 style="font-size:16px;color:#000000;">ALIVECOR ECG </h6></div>

                                     <div id="iframeAlivecoreContainer">
                                     <div id="loader" style="display: none;">Loading...</div>
                                     </div>
                                    <div class="row" id="addEcgs">


                                    </div>


                                    <div id="addHdstethHeader"> <h6 style="font-size:16px;color:#000000;">HDSTETH ECG</h6></div>
                                    <div id="iframeHdsteth"></div>
                                     <div class="row" id="addHdsteth">

                                     </div>
                                     <div id="audioModal" data-backdrop="static" style="display: none; position: fixed; z-index: 1; left: 0; top: 0; width: 100%; height: 100%; overflow: auto; background-color: rgba(0, 0, 0, 0.4);">
                                        <div class="modal-content" style="background-color: #fefefe; margin: 15% auto; padding: 20px; border: 1px solid #888; width: 80%;">
                                          <span class="close" style="color: #aaa; float: right; font-size: 28px; font-weight: bold; cursor: pointer;">&times;</span>
                                          <audio id="audioPlayer" controls>
                                            <!-- Audio source will be dynamically set -->
                                            Your browser does not support the audio element.
                                          </audio>
                                        </div>
                                      </div>

                                      <div id="addimedrixHeader"> <h6 style="font-size:16px;color:#000000;">IMEDRIX ECG</h6></div>
                                      <div id="iframeImedrix"></div>
                                        <div class="row" id="addImedrix">

                                      </div>
                                    </div>
                                </div><!-- /.tab-pane -->


                                <div class="tab-pane" id="timeline">
                                     <div class="box box-primary">
                                        <div class="box-header">
                                            <!-- /<h3 class="box-title"></h3> -->
                                            <b style="font-size:12px">CHIEF COMPLAINTS</b>
                                            <button id="addChiefComplaintButton"
                                                class="pull-right btn-primary btn dcbutton"
                                                onclick="openChiefComplaintModal();">Add Complaint</button>
                                        </div><!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <tr id="chiefComplaintEmptyRow">
                                                    <th style="width: 100%"></th>
                                                </tr>
                                                <thead id="chiefComplaintTableHeader"></thead>
                                                <tbody id="chiefComplaintTableBody"></tbody>

                                            </table>
                                        </div><!-- /.box-body -->
                                    </div>



                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <!-- /<h3 class="box-title"></h3> -->
                                            <b style="font-size:12px">HISTORY</b>
                                            <button id="addHistoryButton1"
                                                class="pull-right btn-primary btn dcbutton"
                                                onclick="openHistoryModal();">Add History</button>
                                        </div><!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <tr id="historyEmptyRow">
                                                    <th style="width: 100%"></th>
                                                </tr>
                                                <thead id="historyTableHeader"></thead>
                                                <tbody id="historyTableBody"></tbody>

                                            </table>
                                        </div><!-- /.box-body -->
                                    </div>


                                     <div class="box box-primary">
                                        <div class="box-header">
                                            <!-- /<h3 class="box-title"></h3> -->
                                            <b style="font-size:12px">CLINICAL EXAMINATION</b>
                                            <button id="addHistoryButton2"
                                                class="pull-right btn-primary btn dcbutton"
                                                onclick="openclinicallyExamine();">Add</button>
                                        </div><!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <tr id="clinicalExaminEmptyRow">
                                                    <th style="width: 100%"></th>
                                                </tr>
                                                <thead id="clinicalExaminTableHeader"></thead>
                                                <tbody id="clinicalExaminTableBody"></tbody>

                                            </table>
                                        </div><!-- /.box-body -->
                                    </div>



                                     <div class="box box-primary">
                                        <div class="box-header">
                                            <b style="font-size:12px">PRELIMINARY DIAGNOSIS</b>
                                            <button id="addPrelimDiagButton" class="pull-right btn-primary btn dcbutton"
                                                onclick="openNotesModal();">Add Preliminary Diagnosis</button>
                                        </div><!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <tr id="notesEmptyRow">
                                                    <th style="width: 5%"></th>
                                                    <th style="width: 60%"></th>
                                                    <th style="width: 10%"></th>
                                                    <th style="width: 20%"></th>
                                                </tr>
                                                <thead id="notesTableHeader"></thead>
                                                <tbody id="notesTableBody"></tbody>

                                            </table>
                                        </div><!-- /.box-body -->
                                    </div>



                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <b style="font-size:12px">INVESTIGATION</b>
                                            <button id="addInvestigationButton" class="pull-right btn-primary btn"
                                                onclick="openReportModal();">Add Investigation</button>
                                        </div><!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <tr id="reportsEmptyRow">
                                                    <th style="width: 100%"></th>
                                                </tr>
                                                <thead id="reportsTableHeader"></thead>
                                                <tbody id="reportsTableBody"></tbody>

                                            </table>
                                        </div><!-- /.box-body -->
                                    </div>



                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <!-- /<h3 class="box-title">PRESCRIPTION</h3> -->
                                            <b style="font-size:12px">RX/PRESCRIPTION</b>
                                            <button id="addPrescriptionButton"
                                                class="pull-right btn-primary btn dcbutton"
                                                onclick="openMedicationModal();">Add Prescription</button>
                                        </div><!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <tr id="prescriptionEmptyRow">
                                                    <th style="width: 100%"></th>
                                                </tr>
                                                <thead id="prescriptionTableHeader"></thead>
                                                <tbody id="prescriptionTableBody"></tbody>

                                            </table>
                                        </div><!-- /.box-body -->
                                    </div>


                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <b style="font-size:12px">FOLLOW UP</b>
                                            <button id="addFollowUpButton" class="pull-right btn-primary btn dcbutton"
                                                onclick="openFollowUpModal();">Add Follow Up</button>
                                        </div><!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <tr id="followUpEmptyRow">
                                                    <th style="width: 100%"></th>
                                                </tr>
                                                <thead id="followUpTableHeader"></thead>
                                                <tbody id="followUpTableBody"></tbody>

                                            </table>
                                        </div><!-- /.box-body -->
                                    </div>

                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <b style="font-size:12px">ADVICE</b>
                                            <button id="addAdviceButton" class="pull-right btn-primary btn dcbutton"
                                                onclick="openAdviceModal();">Add Advice</button>
                                        </div><!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <tr id="adviseEmptyRow">
                                                    <th style="width: 100%"></th>
                                                </tr>
                                                <thead id="adviseTableHeader"></thead>
                                                <tbody id="adviseTableBody"></tbody>

                                            </table>
                                        </div><!-- /.box-body -->
                                    </div>




                                </div><!-- /.tab-pane -->

                                <div class="tab-pane" id="vitaltrend">

<div class="row">
    <div class="col-sm-6">
        <div class="box-shadow-card">
            <div class="stats-chart-box">
                <div class="stats-header-box">
                    <div class="stats-header">Temperature Trend</div>
                    <div class="stats-header-icon">
                        <span class="glyphicon glyphicon-info-sign" id="toolTip13"
                            data-placement="left" data-toggle="tooltip"
                            data-html="true"
                            title="<div class='tooltipTitle'><p>- Normal (36 ~ 38 C)</p><p>- Abnormal (<36 / >38 C)</p></div>">
                        </span>
                    </div>
                </div>
            </div>
            <div id="lineChart1" class="chartContainer"></div>
            <div id="lineChart1-ND" class="emptyChart"
                style="display:none; min-height: 150px;"> No Data!</div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="box-shadow-card">
            <div class="stats-chart-box">
                <div class="stats-header-box">
                    <div class="stats-header">Pulse Trend</div>
                    <div class="stats-header-icon">
                        <span class="glyphicon glyphicon-info-sign" id="toolTip14"
                            data-placement="left" data-toggle="tooltip"
                            data-html="true"
                            title="<div class='tooltipTitle'><p>- Normal (72 ~ 100 bpm)</p><p>- Abnormal (<72 / >100 bpm)</p></div>">
                        </span>
                    </div>
                </div>
            </div>
            <div id="lineChart2" class="chartContainer"></div>
            <div id="lineChart2-ND" class="emptyChart"
                style="display:none; min-height: 150px;"> No Data!</div>
        </div>
    </div>


</div>
<!--/.row-->

<div class="row">

    <div class="col-sm-6">
        <div class="box-shadow-card">
            <div class="stats-chart-box">
                <div class="stats-header-box">
                    <div class="stats-header">Blood Glucose Trend</div>
                    <div class="stats-header-icon">
                        <span class="glyphicon glyphicon-info-sign" id="toolTip15"
                            data-placement="left" data-toggle="tooltip"
                            data-html="true"
                            title="<div class='tooltipTitle'><p>-Lowsugar (51~70 mg/dL)</p><p>-Normal (71~100 mg/dL)</p><p>- Prediabetes (101-125 mg/dL)</p><p>- Diabetes (>126 mg/dL)</p></div>">
                        </span>
                    </div>
                </div>
            </div>
            <div id="lineChart3" class="chartContainer"></div>
            <div id="lineChart3-ND" class="emptyChart"
                style="display:none; min-height: 150px;"> No Data!</div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="box-shadow-card">
            <div class="stats-chart-box">
                <div class="stats-header-box">
                    <div class="stats-header">Oximeter(SPO2) Trend</div>
                    <div class="stats-header-icon">
                        <span class="glyphicon glyphicon-info-sign" id="toolTip16"
                            data-placement="left" data-toggle="tooltip"
                            data-html="true"
                            title="<div class='tooltipTitle'><p>- Normal (95 ~ 100 %)</p><p>- Abnormal (<95 / >100 %)</p></div>">
                        </span>
                    </div>
                </div>
            </div>
            <div id="columnChart1" class="chartContainer"></div>
            <div id="columnChart1-ND" class="emptyChart"
                style="display:none; min-height: 150px;"> No Data!</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6" >
        <div class="box-shadow-card">
<div class="stats-chart-box">
<div class="stats-header-box">
    <div class="stats-header">Blood Pressure Trend</div>
    <div class="stats-header-icon">
        <span class="glyphicon glyphicon-info-sign" id="toolTip17" data-placement="left" data-toggle="tooltip" data-html="true"
            title="<div class='tooltipTitle'><p>-Optimal (SYS <120, DIA <80 mm Hg)</p><p>- Normal (SYS <130, DIA <85 mm Hg)</p><p>High-Normal (SYS 130~139, DIA 85~89 mm Hg)</p>
            <p>-G1-Hypertension (SYS 140~159, DIA 90~99 mm Hg)</p><p>-G2-Hypertension (SYS 160~179, DIA 100~109 mm Hg)</p><p>-G3-Hypertension (SYS >=180, DIA >=110 mm Hg)</p></div>" >
        </span>
    </div>
</div>
</div>
<div id="candleChart1" class="chartContainer"></div>
<div id="candleChart1-ND" class="emptyChart" style="display:none; min-height: 150px;"> No Data!</div>
</div>
</div>
<div class="col-sm-6" >
<div class="box-shadow-card">
<div class="stats-chart-box">
<div class="stats-header-box">
    <div class="stats-header">BMI Trend</div>
    <div class="stats-header-icon">
        <span class="glyphicon glyphicon-info-sign" id="toolTip5"
            data-placement="left" data-toggle="tooltip" data-html="true"
            title="<div class='tooltipTitle'><p>- Underweight(<18.5 kg/m<sup>2</sup>)</p><p>- Normal(18.5~24 kg/m<sup>2</sup>)</p><p>- Overweight(25~29 kg/m<sup>2</sup>)</p><p>- Obese(>=30 kg/m<sup>2</sup>)</p></div>">
        </span>
    </div>
</div>
</div>
<div id="donutChart5" class="chartContainer"></div>
<div id="donutChart5-ND" class="emptyChart" style="display:none; min-height: 150px;"> No Data!</div>
</div>
</div>
</div>
<!--/.row-->

</div><!-- /.tab-pane -->


                            </div><!-- /.tab-content -->
                        </div><!-- /.nav-tabs-custom -->
                    </div><!-- /.col -->
                </div><!-- /.row -->

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
        <footer class="main-footer"
            style="display:block;padding-right:15px; margin-left:0% !important;position: fixed;bottom: 0px;margin-right: auto;margin-left: auto;width:100%;">
            <!--  <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>.</strong> All rights reserved. -->
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0.0
            </div>
            Copyright &copy; 2021-2022 <a href="http://www.wrizto.com/" target=_blank
                style="color: rgb(60, 141, 188);">Wrizto Healthcare Pvt. Ltd.</a> All rights reserved.
        </footer>
        <!-- Control Sidebar -->

        <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


    <div id="addMedicationModal" name="addMedicationForm" class="modal modal-fullscreen fade" data-backdrop="static"
        style="height:100%;padding-top: 0px;z-index:9999" role="dialog" data-keyboard="false">
        <!-- <form id = "saveMedicationId" method="post" enctype ='multipart/form-data' style="height:100%;">
       				<input type="hidden" name="additionalDetailJson" id="additionalDetailJson">
                    <input type="hidden" name="strPrescribedDate" id="strPrescribedDate">
                    <input type="hidden"  id="serviceNameValue" name="serviceName" value="">
                    <input type="hidden" name="documentTypeId" id="documentTypeId" value="">
                    <input type="hidden" name="clinicId" id="clinicId" >
                    <input type="hidden" name="userId" id="idUser" value="">
                    <input type="hidden" name="ccpId" id="idCcp" value="">
		 			<input type="hidden" id="medEpisodeId"  class="episodeId" name="episodeId" value="">
		 			<input type="hidden"  id="medicalReportId" name="medicalReportId" value="-1">
		 			<input type="file" style="display:none;" name="files" id="reportFileId" class="getReport" onchange="setFileName(this)"></input>

		 			<input type="hidden"  id="deletedRecords" name="deletedRecords" value="">
		 			<input type="hidden"  id="loggedIncreatedBy" name="createdBy" value="">
		     		<input type="hidden"  id="documentTypeName" name="documentTypeName" value="Prescription">
					<input type="hidden"  id="documentTypeName" name="documentTypeName" value="Prescription">
					<input type="hidden" id="medicationIdFromDropdown" name="medicationHiddenId">

		 			<input type="hidden" id="" name="laboratoryName" value="">
		 			 <input class="getAdvice" type="hidden" name="laboratoryId" value="">

		 			 <input type="hidden" id="noteComments" name="comments" value="">
		 			 <input type="hidden" name="dischargeStatus" value="1"> -->


        <div class="modal-dialog" style="padding-top: 0px;height:100%;">
            <div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
                <div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
                    <h5 class="modal-title">
                        <b style="color:White">Medication</b>
                    </h5>
                </div>
                <div class="modal-body"
                    style="background: white;padding-bottom: 0px;padding-top: 0px;;overflow-x:hidden;height:450px;">
                    <div class="col-xs-12">
                        <div class="col-xs-6 " style="margin-top: 0px; padding-left: 0px;">
                            <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                <div style="width: 100%; color: black" id="isDirtyId"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                    <input spellcheck="false" autocomplete="off" autocorrect="off"
                                        class="mdl-textfield__input error-popover bClass Newpolic4" type="text"
                                        id="prescribedByPre" name="prescribedBy" maxlength="100"
                                        style="text-transform: capitalize;"> <label class="mdl-textfield__label"
                                        for="sample3">Prescribed By<i style="color: red">*</i>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
                            <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                <div style="width: 100%; color: black"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                    <input autocomplete="off" autocorrect="off" spellcheck="false"
                                        class="mdl-textfield__input error-popover clearAll bClassM Newpolic1"
                                        autocomplete="off" type="text" id="medicationNameId" name="medicationName"
                                        maxlength="100" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)" style="text-transform: capitalize;margin-bottom: 8px; ">
                                    <label class="mdl-textfield__label" for="medicationNameId">Write medication with dosage
                                        <span style="color:red;">*</span>
                                    </label>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                    <div style="padding-left: 0px;" class="col-xs-6 ">
                                                <p style="margin-bottom: 0px; color: royalblue">Medicine Type</p>
                                                <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
                                                    <div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
                                                        id="" data-upgraded=",MaterialTextfield">
                                                        <select class="form-control" id="medicationType" name="medication"
                                                            style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
                                                            <option value="Tablet">Tablet</option>
                                                            <option value="Capsules">Capsules</option>
                                                            <option value="Liquid">Liquid</option>
                                                            <option value="Syrup">Syrup</option>
                                                             <option value="Injection">Injection</option>
                                                              <option value="Drop">Drop</option>
                                                             <option value="Inhaler">Inhaler</option>
                                                        </select>
                                                    </div>
                                                </div>
                                           </div>

                        <div style="padding-left: 0px;" class="col-xs-6 ">
                            <p style="margin-bottom: 0px; color: royalblue">Select Frequency</p>
                            <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
                                <div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
                                    id="" data-upgraded=",MaterialTextfield">
                                    <select class="form-control" id="fequencyId" name="frequency"
                                        style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
                                        <option>0-0-1</option>
                                        <option>0-1-0</option>
                                        <option>0-1-1</option>
                                        <option>1-0-1</option>
                                        <option>1-1-0</option>
                                        <option>1-1-1</option>
                                        <option>1-0-0</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div style="padding-left: 0px;" class="col-xs-6 ">
                            <p style="margin-bottom: 0px; color: royalblue">Instruction</p>
                            <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
                                <div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
                                    id="" data-upgraded=",MaterialTextfield">
                                    <select class="form-control" id="instructionId" name="instruction"
                                        style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
                                        <option>Before Food</option>
                                        <option>After Food</option>
                                    </select>
                                </div>
                            </div>
                        </div>

<div class="col-xs-6" style="margin-top: 0px; padding-left: 0px; margin-bottom: 17px;">
                            <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                <div style="width: 100%; color: black"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                    <input autocomplete="off" autocorrect="off" spellcheck="false"
                                        class="mdl-textfield__input error-popover bClassM Newpolic1" type="text"
                                        id="startId" name="strBeginDate" maxlength="100"> <label
                                        class="mdl-textfield__label" for="sample3">DD/MM/YY(Start Date)<i
                                            style="color: red">*</i>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">



                        <div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
                            <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                <div style="width: 100%; color: black"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                    <input autocomplete="off" autocorrect="off" spellcheck="false"
                                        class="mdl-textfield__input error-popover bClassM Newpolic1" type="number"
                                        id="days" name="days" maxlength="100"> <label class="mdl-textfield__label"
                                        for="sample3">Days<i style="color: red">*</i>
                                    </label>
                                </div>
                            </div>
                        </div>
                            <div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
                                      <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                          <div style="width: 100%; color: black"
                                              class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                              <input autocomplete="off" autocorrect="off" spellcheck="false"
                                                  class="mdl-textfield__input error-popover bClassM Newpolic1" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                                                  id="commentsMedId" name="comments" maxlength="100"> <label
                                                  class="mdl-textfield__label" for="sample3">Symptoms
                                              </label>
                                          </div>
                                      </div>
                                  </div>
                            </div>

                    <div class="col-xs-12">



                        <div id="statusSelector" style="display:none; padding-left: 0px;" class="col-xs-6 ">
                            <p style="margin-bottom: 0px; color: royalblue">Status</p>
                            <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
                                <div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
                                    id="" data-upgraded=",MaterialTextfield">
                                    <select class="form-control" id="prescriptionStatus" name="status"
                                        style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- <div class="col-xs-12"
										style="margin-top: 0px; margin-bottom: 20px; padding-left: 0px;display:inline-flex; height: 35px">
										<div class="col-xs-8"
											style="padding-left: 0px; padding-right: 0px;">
											<div style="color: black;display:inline-flex;"
												class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
												<i style="padding-top: 5px;margin-left: 0px;padding-right: 10px" class="fa fa-paperclip"></i>
												<input type="file" style="margin-bottom: 0px;
												width:100%; border-radius: 4px; font-family:Roboto Medium;overflow:hidden;
												font-size:15px;color:#5498D2;border:#5498D2 1px solid"
												name="files" id="firstmedfile" onchange="acceptType(this);">
											</div>
										</div>

									<div style="color: black"
										class="col-xs-4 mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">

										<button type="button" onclick="getOtherMedFile()"
											style="width: 45%;float:left; background: none repeat scroll 0% 0% transparent; color: #5498D2 !important; border-radius: 4px; font-family: Roboto Medium; font-size: 11px; border: 1px solid #5498D2; margin-bottom: 0px; margin-top: -1px; padding: 0px; height: 29px; margin-top: 0px;"
											class="btn right">Upload More</button>

									</div>
								</div> -->
                    <div class="col-xs-12 addedMedTable" id="dynTableMed" style="display:none;color:black;">
                        <table class="table">
                            <tr>
                                <!-- <th >id</th> -->
                                <th>Name</th>
                                 <th>Medicine Type</th>
                                <th>Freq</th>

                                <th>Instructions</th>
                                <th>Start Date</th>
                                <th>Days</th>
                                <th>Symptoms</th>
                                <th></th>

                            </tr>
                            <tbody id="medAdd"></tbody>
                        </table>
                    </div>
                </div>
                <div id="addFilesMed">

                </div>

                <div class="modal-footer text-center" style="padding: 5px;overflow-x:hidden">

                    <div class="col-xs-12" style="position: absolute;bottom: 0;">
                        <div class="text-center">
                            <button type="button" class="btn btn-danger" style="width: 20%;margin-bottom: 6px;"
                                onclick="canMed();">Cancel</button>
                            <button type="button" class="btn btn-warning" id="addMedicationButton"
                                style="width: 20%; margin-left: 6px; margin-bottom: 6px;"
                                onclick="addMedicationNew();">ADD</button>
                            <button type="button" class="btn btn-success" id="saveMedicationButton"
                                style="width: 20%; margin-left: 6px; margin-bottom: 6px; display: none;"
                                onclick="saveMedicationNew();">Save</button>
                            <button type="button" id="updateMedicationButton" class="btn btn-success"
                                style="width: 20%; display: none;">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>


    <div id="chief-complaint-Modal" name="addComplaintForm" class="modal modal-fullscreen fade" data-backdrop="static"
        style="height:100%;padding-top: 0px;z-index:9999" role="dialog" data-keyboard="false">

        <div class="modal-dialog" style="padding-top: 0px;height:100%;">
            <div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
                <div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
                    <h5 class="modal-title">
                        <b style="color:White">Chief Complaints</b>
                    </h5>
                </div>
                <div class="modal-body"
                    style="background: white;padding-bottom: 0px;padding-top: 0px;;overflow-x:hidden;height:200px;">
                    <div class="col-xs-12">

                        <div class="col-xs-12" style="margin-top: 0px; padding-left: 0px;">
                            <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                <div style="width: 100%; color: black"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input autocomplete="off" autocorrect="off" spellcheck="false"
                                        class="mdl-textfield__input error-popover bClass Newpolic2 getComplaint" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                                        id="complaintId" name="comments" minLength = "2" maxlength="100"> <label
                                        class="mdl-textfield__label" for="sample3">Add Complaint<i style="color: red">*</i>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer text-center" style="padding: 5px;overflow-x:hidden">
                    <div class="col-xs-12" style="position: absolute;bottom: 0;">
                        <div class="text-center">
                            <button type="button" class="btn btn-danger" style="width: 20%;margin-bottom: 6px;"
                                onclick="complaintModalClose();">Cancel</button>
                            <button type="button" class="btn btn-warning" id="saveComplaintButton"
                                style="width: 20%; margin-left: 6px; margin-bottom: 6px;"
                                onclick="addComplaintNew();">ADD</button>

                            <button type="button" id="updateComplaintButton" class="btn btn-success"
                                style="width: 20%; display: none;">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>


    <div id="addHistoryModal" name="addhistoryForm" class="modal modal-fullscreen fade" data-backdrop="static"
        style="height:100%;padding-top: 0px;z-index:9999" role="dialog" data-keyboard="false">

        <div class="modal-dialog" style="padding-top: 0px;height:100%;">
            <div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
                <div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
                    <h5 class="modal-title">
                        <b style="color:White">History</b>
                    </h5>
                </div>
                <div class="modal-body"
                    style="background: white;padding-bottom: 0px;padding-top: 0px;;overflow-x:hidden;height:200px;">
                    <div class="col-xs-12">
                        <div class="col-xs-12 " style="margin-top: 0px; padding-left: 0px;">
                            <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                <div style="width: 100%; color: black" id="isDirtyHistoryId"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                    <input spellcheck="false" autocomplete="off" autocorrect="off"
                                        class="mdl-textfield__input error-popover bClass Newpolic2" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                                        id="formHistory" name="prescribedBy" minLength = "2" maxlength="100"
                                        style="text-transform: capitalize;"> <label class="mdl-textfield__label"
                                        for="sample3">Add History<i style="color: red">*</i>
                                    </label>
                                </div>
                            </div>
                        </div>
                       </div>
                     </div>


                <div class="modal-footer text-center" style="padding: 5px;overflow-x:hidden">

                    <div class="col-xs-12" style="position: absolute;bottom: 0;">
                        <div class="text-center">
                            <button type="button" class="btn btn-danger" style="width: 20%;margin-bottom: 6px;"
                                onclick="canHistory();">Cancel</button>
                            <button type="button" class="btn btn-warning" id="saveHistoryButton"
                                style="width: 20%; margin-left: 6px; margin-bottom: 6px;"
                                onclick="addHistoryNew();">ADD</button>

                            <button type="button" id="updateHistoryButton" class="btn btn-success"
                                style="width: 20%; display: none;">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>



<div id="clinical-examination-modal" name="addExaminationForm" class="modal modal-fullscreen fade" data-backdrop="static"
        style="height:100%;padding-top: 0px;z-index:9999" role="dialog" data-keyboard="false">

        <div class="modal-dialog" style="padding-top: 0px;height:100%;">
            <div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
                <div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
                    <h5 class="modal-title">
                        <b style="color:White">Clinical Examination</b>
                    </h5>
                </div>
                <div class="modal-body"
                    style="background: white;padding-bottom: 0px;padding-top: 0px;;overflow-x:hidden;height:600px;">
                    <div class="col-xs-12">
                        <div style="padding-left: 0px;" class="col-xs-6 ">
                            <p style="margin-bottom: 0px; color: royalblue">Select RS</p>
                                 <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
                                      <div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                                           class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
                                               id="" data-upgraded=",MaterialTextfield">
                                                    <select class="form-control" id="rsExamine" name="RS" onchange='checkRSvalue(this.value)'
                                                       style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;" >
                                                       <option value="Clinically Normal">Clinically Normal</option>
                                                       <option value="Other Findings">Other Findings</option>

                                                    </select>
                                               </div>
                                           </div>
                                      </div>

                        <div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
                            <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                <div style="width: 100%; color: black"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                    <input autocomplete="off" autocorrect="off" spellcheck="false"
                                        class="mdl-textfield__input error-popover clearAll bClass Newpolic2"
                                       type="text" id="rsExamineText" name="medicationName" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                                        minLength = "2" maxlength="100" style="text-transform: capitalize;margin-bottom: 8px;" disabled>
                                    <label class="mdl-textfield__label" for="rsExamineText">Enter Here
                                        <span style="color:red;">*</span>
                                    </label>

                                </div>
                            </div>
                        </div>
                    </div>
  <div class="col-xs-12">
     <div style="padding-left: 0px;" class="col-xs-6 ">
        <p style="margin-bottom: 0px; color: royalblue">Select GE</p>
        <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
            <div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
                id="" data-upgraded=",MaterialTextfield">
                <select class="form-control" id="geExamine" name="frequency" onchange='checkGEvalue(this.value)'
                    style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
                    <option value="Clinically Normal">Clinically Normal</option>
                    <option value="Other Findings">Other Findings</option>

                </select>
            </div>
        </div>
    </div>
    <div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
         <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
             <div style="width: 100%; color: black"
                  class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                      <input autocomplete="off" autocorrect="off" spellcheck="false"
                       class="mdl-textfield__input error-popover clearAll bClass Newpolic2" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                       id="geExamineText" name="geExamine" minLength = "2" maxlength="100" style="text-transform: capitalize;margin-bottom: 8px;" disabled>
                       <label class="mdl-textfield__label"
                       for="geExamineText">Enter Here<i style="color: red">*</i>
                       </label>
             </div>
         </div>
    </div>
</div>

<div class="col-xs-12">

    <div style="padding-left: 0px;" class="col-xs-6 ">
        <p style="margin-bottom: 0px; color: royalblue">Select P/A</p>
        <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
            <div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
                id="" data-upgraded=",MaterialTextfield">
                <select class="form-control" id="paExamine" name="frequency" onchange='checkPAvalue(this.value)'
                    style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
                    <option value="Clinically Normal">Clinically Normal</option>
                    <option value="Other Findings">Other Findings</option>

                </select>
            </div>
        </div>
    </div>
    <div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
         <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
             <div style="width: 100%; color: black"
                  class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                      <input autocomplete="off" autocorrect="off" spellcheck="false"
                       class="mdl-textfield__input error-popover bClass Newpolic2" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                       id="PAExamineText" name="geExamine" minLength = "2" maxlength="100" style="text-transform: capitalize;margin-bottom: 8px;" disabled>
                       <label class="mdl-textfield__label"
                       for="PAExamineText">Enter Here<i style="color: red">*</i>
                       </label>
             </div>
         </div>
    </div>
</div>
  <div class="col-xs-12">

    <div style="padding-left: 0px;" class="col-xs-6 ">
        <p style="margin-bottom: 0px; color: royalblue">Select GUT</p>
        <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
            <div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
                id="" data-upgraded=",MaterialTextfield">
                <select class="form-control" id="gutExamine" name="frequency" onchange='checkGUTvalue(this.value)'
                    style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
                    <option value="Clinically Normal">Clinically Normal</option>
                    <option value="Other Findings">Other Findings</option>

                </select>
            </div>
        </div>
    </div>
    <div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
         <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
             <div style="width: 100%; color: black"
                  class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                      <input autocomplete="off" autocorrect="off" spellcheck="false"
                       class="mdl-textfield__input error-popover bClass Newpolic2" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                       id="gutExamineText" name="gutExamine" minLength = "2" maxlength="100" style="text-transform: capitalize;margin-bottom: 8px;" disabled>
                       <label class="mdl-textfield__label"
                       for="gutExamineText">Enter Here<i style="color: red">*</i>
                       </label>
             </div>
         </div>
    </div>
</div>

<div class="col-xs-12">

    <div style="padding-left: 0px;" class="col-xs-6 ">
        <p style="margin-bottom: 0px; color: royalblue">Select CVS</p>
        <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
            <div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
                id="" data-upgraded=",MaterialTextfield">
                <select class="form-control" id="cvsExamine" name="frequency" onchange='checkCVSvalue(this.value)'
                    style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
                    <option value="Clinically Normal">Clinically Normal</option>
                    <option value="Other Findings">Other Findings</option>

                </select>
            </div>
        </div>
    </div>
    <div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
         <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
             <div style="width: 100%; color: black"
                  class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                      <input autocomplete="off" autocorrect="off" spellcheck="false"
                       class="mdl-textfield__input error-popover bClass Newpolic2" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                       id="cvsExamineText" name="gutExamine" minLength = "2" maxlength="100" style="text-transform: capitalize;margin-bottom: 8px;" disabled>
                       <label class="mdl-textfield__label"
                       for="cvsExamineText">Enter Here<i style="color: red">*</i>
                       </label>
             </div>
         </div>
    </div>
</div>

                    <div class="col-xs-12">

                        <div style="padding-left: 0px;" class="col-xs-6 ">
                            <p style="margin-bottom: 0px; color: royalblue">Select CNS</p>
                            <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
                                <div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth"
                                    id="" data-upgraded=",MaterialTextfield">
                                    <select class="form-control" id="cnsExamine" name="frequency" onchange='checkCNSvalue(this.value)'
                                        style="display: block ! important;width: 100%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
                                        <option value="Clinically Normal">Clinically Normal</option>
                                        <option value="Other Findings">Other Findings</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6" style="margin-top: 0px; padding-left: 0px;">
                             <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                 <div style="width: 100%; color: black"
                                      class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                          <input autocomplete="off" autocorrect="off" spellcheck="false"
                                           class="mdl-textfield__input error-popover bClass Newpolic2" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                                           id="cnsExamineText" name="gutExamine" minLength = "2" maxlength="100" style="text-transform: capitalize;margin-bottom: 8px;" disabled>
                                           <label class="mdl-textfield__label"
                                           for="cnsExamineText">Enter Here<i style="color: red">*</i>
                                           </label>
                                 </div>
                             </div>
                        </div>
                    <div class="col-xs-12">

                         <div class="col-xs-12" style="margin-top: 0px; padding-left: 0px;">
                             <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                 <div style="width: 100%; color: black"
                                      class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                          <input autocomplete="off" autocorrect="off" spellcheck="false"
                                           class="mdl-textfield__input error-popover bClass Newpolic2" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                                           id="otherFindings" name="otherExamine" minLength = "2" maxlength="100" style="text-transform: capitalize;margin-bottom: 8px;">
                                           <label class="mdl-textfield__label"
                                           for="sample3">Other
                                           </label>
                                 </div>
                             </div>
                        </div>
                        </div>
                    </div>

                </div>


                <div class="modal-footer text-center" style="padding: 5px;overflow-x:hidden">

                    <div class="col-xs-12" style="position: absolute;bottom: 0;">
                        <div class="text-center">
                            <button type="button" class="btn btn-danger" style="width: 20%;margin-bottom: 6px;"
                                onclick="canClinicalExamin();">Cancel</button>
                            <button type="button" class="btn btn-warning" id="addClinicalExaminButton"
                                style="width: 20%; margin-left: 6px; margin-bottom: 6px;"
                                onclick="addClinicalExaminNew();">ADD</button>
                            <button type="button" class="btn btn-success" id="saveClinicalExaminButton"
                                style="width: 20%; margin-left: 6px; margin-bottom: 6px; display: none;"
                                onclick="saveClinicalExaminNew();">Save</button>
                            <button type="button" id="updateClinicalExaminButton" class="btn btn-success"
                                style="width: 20%; display: none;">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>



    <div id="addVisitForm" class="modal fade main-fade-modal " role="dialog" data-backdrop="static"
        data-keyboard="false">

        <div class="modal-dialog" id="visitModalId">
            <div class="modal-content" style="border-radius: 4px;">
                <div class="modal-header" style="background-color:#5498D2;">
                    <h5 class="modal-title"><b style="color:White">Advice</b></h5>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <div class="col-xs-12" style="margin-top: 0px; padding-left: 0px;">



                        <div class="col-xs-12 Newpolic3" style="margin-top: 0px; padding-left: 0px;">
                            <div style="padding-left: 0px;" class="col-xs-12 ">

                                <div style="width: 100%; color: black;"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input autocomplete="off" autocorrect="off" spellcheck="false"
                                        style="margin-bottom:8px;"
                                        class="mdl-textfield__input error-popover bClass Newpolic2 getAdvice" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                                        type="text" id="serviceTypeId" name="serviceid" maxlength="100"> <label
                                        class="mdl-textfield__label" for="serviceTypeId">General Advice<i
                                            style="color: red">*</i>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 Newpolic3" style="margin-top: 0px; padding-left: 0px;">
                            <div style="padding-left: 0px;" class="col-xs-12 ">
                                <div style="width: 100%; color: black;"
                                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
                                    <input autocomplete="off" autocorrect="off" spellcheck="false"
                                        style="margin-bottom:8px;display:none;"
                                        class="mdl-textfield__input error-popover bClass Newpolic2 getAdvice"
                                        type="text" id="advComments" name="Labcomments" maxlength="100"> <label
                                        style="display:none;" class="mdl-textfield__label" for="sample3">Referred By
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer text-center">

                        <!-- <button id="demo-show-snackbar" class="mdl-button mdl-js-button mdl-button--raised" style="display:none" type="button"></button> -->
                        <button type="button" class="btn btn-danger" data-dismiss="modal"
                            style="width: 25%; margin-left: 5px; margin-bottom: 0px;"
                            onclick="clearAdvModal()">Cancel</button>
                        <!-- <button type="button" class="btn btn-success" id="otpButton" onclick="generateOtp()">Generate OTP</button> -->
                        <!-- <button type="button" class="btn btn-warning" id="" style="width: 20%;" onclick="addVisit();" >ADD</button> -->
                        <button type="button" class="btn btn-success" id="saveAdviceButton" style="width: 25%;"
                            onclick="saveAdvice();">Save</button>
                        <button type="button" class="btn btn-success" id="updateAdviceButton"
                            style="width: 25%; display:none;">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Observation Modal  -->

    <!-- Notes Modal  -->

    <div class="example-modal">
        <div class="modal" id="addNotesModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header" style="background-color:#5498D2;">
                        <h5 class="modal-title"><b style="color:White">Preliminary Diagnosis</b></h5>
                    </div>
                    <div class="modal-body">
                        <textarea style="width:100%;height:100px;" id="noeComments" class="getNotes" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                            name="comments"></textarea>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" style="width: 25%;"
                            onclick="canNote();">Cancel</button>
                        <button type="button" id="savePrelimDiagButton" class="btn btn-success" style="width: 25%;"
                            onclick="savePrelimDiagnostics();">Save</button>
                        <button type="button" id="updatePrelimDiagButton" class="btn btn-success"
                            style="width: 25%; display: none;">Update</button>
                    </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>

    <div class="example-modal">
        <div class="modal" id="addNotesModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="addNotesForm" method="post" enctype='multipart/form-data'>
                        <input type="hidden" name="additionalDetailJson">

                        <input type="hidden" name="documentTypeId" value="8">
                        <input type="hidden" name="clinicId" id="clinicIdNote">
                        <input type="hidden" name="userId" id="idUserNote" value="">
                        <input type="hidden" name="ccpId" id="idCcpUP" value="">
                        <input type="hidden" class="episodeId" name="episodeId" value="" id="episodeIdUp">
                        <input type="hidden" id="medicalReportId" name="medicalReportId" value="-1">

                        <input type="hidden" name="deletedFiles" value="">
                        <input type="hidden" name="deletedRecords" value="">
                        <input type="hidden" name="episodeName" value="CCP">
                        <input type="hidden" name="dischargeStatus" value="1">
                        <input type="hidden" name="strPrescribedDate" value="30/03/2018">


                        <input type="hidden" name="documentTypeName" value="Preliminary Diagnosis">

                        <!-- <div class="modal-header">
			                    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                    		<span aria-hidden="true">&times;</span></button>
			                    		<h4 class="modal-title">Add Notes</h4>
			                  		</div> -->
                        <div class="modal-header" style="background-color:#5498D2;">
                            <h5 class="modal-title"><b style="color:White">Preliminary Diagnosis</b></h5>
                        </div>
                        <div class="modal-body">
                            <textarea style="width:100%;height:100px;" id="noeComments" class="getNotes"
                                name="comments"></textarea>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" style="width: 25%;"
                                onclick="canNote();">Cancel</button>
                            <button type="button" id="saveMedicationButton" class="btn btn-success" style="width: 25%;"
                                onclick="saveMedication(3);">Save</button>
                            <button type="button" id="updateMedicationButton" class="btn btn-success"
                                style="width: 25%; display: none;">Update</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>

    <div class="example-modal">
        <div class="modal" id="addFollowUpModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:#5498D2;">
                        <h5 class="modal-title"><b style="color:White">Follow Up</b></h5>
                    </div>
                    <div class="modal-body">
                        <div class="col-xs-12">
                            <div class="col-xs-12" style="margin-top: 0px; padding-left: 0px; margin-bottom: 17px;">
                                <div class="col-xs-11" style="padding-left: 0px; padding-right: 0px;">
                                    <div style="width: 100%; color: black"
                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                        <input autocomplete="off" autocorrect="off" spellcheck="false"
                                            class="mdl-textfield__input error-popover bClassM Newpolic1" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                                            id="followUpAfterTextId" name="strBeginString" maxlength="100"> <label
                                            class="mdl-textfield__label" for="sample3">Revisit after
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-1" style="padding-left: 15px; padding-right: 0px;">
                                    <h6>days</h6>
                                </div>
                            </div>
                            <span>
                                <p style="text-align:center">OR</p>
                            </span>
                            <div class="col-xs-12" style="margin-top: 0px; padding-left: 0px; margin-bottom: 17px;">
                                <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                    <div style="width: 100%; color: black"
                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                        <input autocomplete="off" autocorrect="off" spellcheck="false"
                                            class="mdl-textfield__input error-popover bClassM Newpolic1" type="date"
                                            id="followUpAfterId" name="strBeginDate" maxlength="100" > <label
                                            class="mdl-textfield__label" for="sample3">Select Date
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer" style="border-top :0px ;">
                        <button type="button" class="btn btn-danger" style="width: 25%;"
                            onclick="closeFollowUpModal();">Cancel</button>
                        <button type="button" id="saveFollowUpButton" class="btn btn-success" style="width: 25%;"
                            onclick="saveFollowUp();">Save</button>
                        <button type="button" id="updateFollowUpButton" class="btn btn-success"
                            style="width: 25%; display: none;">Update</button>
                    </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>

    <div class="example-modal">
        <div class="modal" id="addReportModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- <div class="modal-header">
			                    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                    		<span aria-hidden="true">&times;</span></button>
			                    		<h4 class="modal-title">Add Notes</h4>
			                  		</div> -->
                    <div class="modal-header" style="background-color:#5498D2;">
                        <h5 class="modal-title"><b style="color:White">Investigation</b></h5>
                    </div>
                    <div class="modal-body">
                        <div class="col-xs-12" style="margin-top: 0px; padding-left: 0px; margin-bottom: 17px;">
                            <div class="col-xs-12"
                                style="margin-top: 0px; margin-bottom: 20px; padding-left: 0px;display:inline-flex; height: 35px">
                                <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
                                    <div style="width: 100%; color: black"
                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label headerDown">
                                        <input autocomplete="off" autocorrect="off" spellcheck="false"
                                            class="mdl-textfield__input error-popover bClassM Newpolic1" type="text" onkeydown="return /[a-z0-9A-Z., !%#+=)($^_@,*,&,/]/i.test(event.key)"
                                            id="investigationTextId" name="investigationText" maxlength="100">
                                        <label class="mdl-textfield__label" for="sample3">Investigation<i style="color: red">*</i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="margin-top: 15%;">
                        <!-- <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button> -->
                        <button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 25%;"
                            onclick="clearRep();">Cancel</button>
                        <button type="button" id="saveInvestigationButton" class="btn btn-success" style="width: 25%;"
                            onclick="saveInvestigation();">Save</button>
                        <button type="button" id="updateInvestigationButton" class="btn btn-success"
                            style="width: 25%; display: none;">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>



<script>

    var episdeId = "";
    var userId = "";
    var episodeId = "";
    var loggedInUserId = '';
    var productId = '';
    var appointmentId = '';
    var test = 3;
    var vitalMap = new Map();
    var patGender = '', patAge = '';
    var prescriptionDetails = ''
    var doctorEpisodeId = 0;
    var VITALMASTER_SERVER_DATA = [];

    //var patientId =  localStorage.getItem("patientId");
    var patientId = localStorage.getItem("patientId");

    $(document).ready(function () {
        /*
        if (localStorage.getItem("userGroupCode") === 'NUR')
            $("#patListLogout").hide();
        */
        if (localStorage.getItem("userGroupCode") === 'DOC') {
            pingCallForVC();
        }

        $('#dashBoardHideShoeVideoIconId').hide();

        if (localStorage.getItem("userGroupCode") === 'NUR' || localStorage.getItem("userGroupCode") === 'ADMIN')
            $("#timeline").hide();

        getPatientDetails();
        episdeId = "";

        function previousDate(){
            var dtToday = new Date();
            var month = dtToday.getMonth() + 1;// getMonth() is zero-based
            var day = dtToday.getDate();
            var year = dtToday.getFullYear();
            if(month < 10)
                month = '0' + month.toString();
            if(day < 10)
                day = '0' + day.toString();
            var maxDate = year + '-' + month + '-' + day;
                // var minDate = day + '/' + month + '/' + year;
            $('#followUpAfterId').attr('min', maxDate);
            var maxDatePrsescrption = month + '-' + day + '-' + year;
            console.log('---PREV-DATE---', maxDate, ', PS: ',maxDatePrsescrption);
            //$('#startId').attr('min', minDate);
        }


        previousDate();



        var loggedInUserFullName = localStorage.getItem("loggedInUserFullName");
        loggedInUserId = localStorage.getItem("loggedInUserId");
        $("#loggedInUserFullName").text(loggedInUserFullName);
        $("#loggedInUserFullNameSpan").text(loggedInUserFullName);

        $("#loggedIncreatedBy").val(loggedInUserId);
        $("#idUser").val(loggedInUserId);
        $("#idUserNote").val(loggedInUserId);

        productId = localStorage.getItem("productId");
        var userGroupCode = localStorage.getItem("userGroupCode");
        if (userGroupCode == "DOC") {

            $("#dash_id").text("Doctor DashBoard");
            $(".dcbutton").show();
        } else if (userGroupCode == "ADMIN") {

            $("#dash_id").text("Admin DashBoard");
            $(".dcbutton").show();
        }
        else {
            $("#dash_id").text("Nurse DashBoard");
            $(".dcbutton").hide();
        }

        createEpisodeScrollBtns()

        //alert(episdeId);

        $('#addVisitForm').on('show.bs.modal', function (e) {

            $("#fromDate").mask("99/99/99", {});
        });
        $('#addMedicationModal').on('show.bs.modal', function (e) {

            //$("#medAdd").html('');
            //$("#dynTableMed").css("display","none");
        });

        $('#startId').datepicker({
            autoclose: true,
            todayHighlight: true,
            startDate: 'today',
            format: "dd/mm/yyyy"
        }).on('changeDate', function (e) {
            $("#startId").attr("readonly", true);
            $("#startId").parent().addClass('is-dirty');
            //$(this).datepicker('hide');
        });
        $('#fromDate').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: "dd/mm/yyyy",
        }).on('changeDate', function (e) {
            $("#fromDate").attr("readonly", true);
            $("#fromDate").parent().addClass('is-dirty');
            $(this).datepicker('hide');
        });

        // $('#serviceTypeId').typeahead({
        //     source: function (query, process) {
        //         var $url = serverPath + '/masterServiceList?serviceType=services';
        //         var $datas = new Array;
        //         $datas = [""];
        //         $.ajax({
        //             url: $url,
        //             headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
        //             dataType: "json",
        //             type: "GET",
        //             success: function (data) {

        //                 $.map(data.content, function (data) {
        //                     var group;
        //                     group = {
        //                         id: data.id,
        //                         name: data.value,

        //                         toString: function () {
        //                             return JSON.stringify(this);
        //                         },
        //                         toLowerCase: function () {
        //                             return this.name.toLowerCase();
        //                         },
        //                         indexOf: function (string) {
        //                             return String.prototype.indexOf.apply(this.name, arguments);
        //                         },
        //                         replace: function (string) {
        //                             var value = '';
        //                             value += this.name;
        //                             if (typeof (this.name) != 'undefined') {
        //                                 value += ' <span class="pull-right muted">';
        //                                 value += this.name;
        //                                 value += '</span>';
        //                             }
        //                             return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
        //                         }
        //                     };
        //                     $datas.push(group);
        //                 });
        //                 process($datas);
        //             },
        //             error: function (xhr, status, error) {
        //                 console.log('xhr1: ', xhr, ' -->Status: ', status, ' -->Error: ', error);
        //                 if (xhr.status == 0) window.location.href = './login';
        //                 if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
        //                     if (xhr.responseJSON.code === 401) {
        //                         (async () => {
        //                             toastr.error(xhr.responseJSON.message);
        //                             await sleep();
        //                             window.location.href = './login';
        //                         })()
        //                     }
        //                     toastr.error(xhr.responseJSON.message);
        //                 } else {
        //                     toastr.error("No Data Found!");
        //                 }
        //             }
        //         });
        //     },
        //     property: 'name',
        //     items: 10,
        //     minLength: 2,
        //     updater: function (item) {
        //         var item = JSON.parse(item);
        //         $('#investigationId').val('');
        //         //alert(item.id);
        //         // $('#serviceTypeIdFromDropdown').val(item.id);
        //         $('#investigationId').val(item.id);
        //         /* $("#servicesName").val(item.name);
        //         alert( $('#servicesName').val()); */
        //         return item.name;
        //     }
        // });

        // $('#medicationNameId').typeahead({
        //     source: function (query, process) {
        //         var searchParameter = $('#medicationNameId').val();
        //         var $url = serverPath + '/getMediactionList?searchParameter=' + searchParameter;
        //         var $datas = new Array;
        //         $datas = [""];
        //         $.ajax({
        //             url: $url,
        //             dataType: "json",
        //             type: "GET",
        //             headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
        //             success: function (data) {
        //                 //console.log(data.content);
        //                 if (data.content != undefined && data.content != 'undefined') {
        //                     $.map(data.content, function (data) {
        //                         var group;
        //                         group = {
        //                             id: data.id,
        //                             name: data.name,

        //                             toString: function () {
        //                                 return JSON.stringify(this);
        //                             },
        //                             toLowerCase: function () {
        //                                 return this.name.toLowerCase();
        //                             },
        //                             indexOf: function (string) {
        //                                 return String.prototype.indexOf.apply(this.name, arguments);
        //                             },
        //                             replace: function (string) {
        //                                 var value = '';
        //                                 value += this.name;
        //                                 if (typeof (this.name) != 'undefined') {
        //                                     value += ' <span class="pull-right muted">';
        //                                     value += this.name;
        //                                     value += '</span>';
        //                                 }

        //                                 return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
        //                             }
        //                         };
        //                         $datas.push(group);
        //                     });
        //                 }
        //                 else {
        //                     $datas = [""];
        //                 }
        //                 process($datas);
        //             },
        //             error: function (xhr, status, error) {
        //                 console.log('xhr1: ', xhr, ' -->Status: ', status, ' -->Error: ', error);
        //                 if (xhr.status == 0) window.location.href = './login';
        //                 if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
        //                     if (xhr.responseJSON.code === 401) {
        //                         (async () => {
        //                             toastr.error(xhr.responseJSON.message);
        //                             await sleep();
        //                             window.location.href = './login';
        //                         })()
        //                     }
        //                     toastr.error(xhr.responseJSON.message);
        //                 } else {
        //                     toastr.error("No Data Found!");
        //                 }
        //             }
        //         });
        //     },
        //     property: 'name',
        //     items: 10,
        //     minLength: 2,
        //     updater: function (item) {
        //         var item = JSON.parse(item);
        //         $('#medicationIdFromDropdown').val('');
        //         //alert(item.id);
        //         $('#medicationIdFromDropdown').val(item.id);
        //         return item.name;
        //     }
        // });

        $('#serviceNameId').typeahead({
            source: function (query, process) {
                var searchParameter = $('#serviceNameId').val();
                var $url = 'getServicesList?searchParamater=' + searchParameter;
                var $datas = new Array;
                $datas = [""];
                $.ajax({
                    url: $url,
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                        //console.log(data.content);
                        if (data.content != undefined && data.content != 'undefined') {
                            $.map(data.content, function (data) {
                                var group;
                                group = {
                                    id: data.id,
                                    name: data.name,

                                    toString: function () {
                                        return JSON.stringify(this);
                                    },
                                    toLowerCase: function () {
                                        return this.name.toLowerCase();
                                    },
                                    indexOf: function (string) {
                                        return String.prototype.indexOf.apply(this.name, arguments);
                                    },
                                    replace: function (string) {
                                        var value = '';
                                        value += this.name;
                                        if (typeof (this.name) != 'undefined') {
                                            value += ' <span class="pull-right muted">';
                                            value += this.name;
                                            value += '</span>';
                                        }

                                        return String.prototype.replace.apply('<div style="padding: 10px; font-size: 1em;">' + value + '</div>', arguments);
                                    }
                                };
                                $datas.push(group);
                            });
                        }
                        else {
                            $datas = [""];
                        }
                        process($datas);
                    }
                });
            },
            property: 'name',
            items: 10,
            minLength: 2,
            updater: function (item) {
                var item = JSON.parse(item);
                $('#serviceFromDropdown').val('');
                $('#serviceFromDropdown').val(item.id);
                return item.name;
            }
        });

    }); //END of READY

    function createEpisodeScrollBtns(refresh = false, prevIndex = 0) {
        console.log('Inside createEpisodeScrollBtns(): ', refresh);
        if (refresh) $("#chronologicalView").empty();
        $.ajax({
            type: "POST",
            url: serverPath + "/getEpisodeList?patientId=" + localStorage.getItem("patientId"),
            headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
            success: function (data) {
                if (data.code == "200") {
                    let { patientList } = data.content;
                    var size = 0;
                    if (patientList && patientList.length > 0) {
                        size = patientList.length;
                        s = size;
                        var appendText = "";
                        $.each(patientList, function (index, option) {
                            if (index == 0) {
                                episdeId = option.id;
                                episodeId = option.id;
                                userId = option.userId;
                                time = option.created;
                                doctorEpisodeId = option.drEpisodeId
                            }
                            if (size > 4) {
                                if (index < 4) {
                                    if (index == 0) {
                                        appendText += '<button type="button" id="prevId" style="margin-left:10px;background:#B8B8B8;display:none;" class="btn btn-primary normalCol" onclick="previous(' + option.id + ');">' + "PREV" + '</button>'
                                    }
                                    appendText += '<button type="button" id="' + index + '" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="patEpisodeChange(' + option.id + ',' + index + ',' + false + ',' + option.drEpisodeId + ');">' + option.episodeName + '</button>'
                                }
                                else {
                                    appendText += '<button type="button" id="' + index + '" style="margin-left:10px;background:#B8B8B8; display:none;" class="btn btn-primary normalCol" onclick="patEpisodeChange(' + option.id + ',' + index + ',' + false + ',' + option.drEpisodeId + ');">' + option.episodeName + '</button>'
                                    if (index == size - 1) {
                                        appendText += '<button type="button" id="nextId" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="next(' + option.id + ');">' + "NEXT" + '</button>'
                                    }
                                }
                            }
                            else {
                                appendText += '<button type="button" id="' + index + '" style="margin-left:10px;background:#B8B8B8;" class="btn btn-primary normalCol" onclick="patEpisodeChange(' + option.id + ',' + index + ',' + false + ',' + option.drEpisodeId + ');">' + option.episodeName + '</button>'
                            }
                        });
                        $("#chronologicalView").html(appendText);
                        //$( "#chronologicalView button" ).first().css( "background-color", "#367fa9" );
                        var tempListIndex = 0;
                        $("#" + prevIndex).css("background", "#367fa9");
                        getMasterVitals();
                        getVitalDocument(episdeId);
                        getDrEpisodeDetails(episdeId, prevIndex, doctorEpisodeId);
                    }
                    s = size;
                } else {
                    console.log('Error')
                }
            },
            error: function (xhr, status, error) {
                if (xhr.status == 0) window.location.href = './login';
                if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                    if (xhr.responseJSON.code === 401) {
                        (async () => {
                            toastr.error(xhr.responseJSON.message);
                            await sleep();
                            window.location.href = './login';
                        })()
                    }
                    //toastr.error(xhr.responseJSON.message);
                } else {
                    toastr.error("No Data Found!");
                }
            }
        });
    }


    function confPopUp(callerName, roomName, roomId, weconUrl) {
        var room = encodeURIComponent(roomName);
        videocalllink = weconUrl + roomId;
        var win;
        var r = confirm(callerName + " is calling you. Do you accept the call?");
        //	alert(r);
        if (r == true) {
            win = window.open(videocalllink, 'result', 'width=300,height=300');

            /* win.addEventListener("resize", function(){
                       win.resizeTo(1024, 768);
                 }); */
        }
        $.ajax({
            type: "POST",
            url: serverPath + "/intiateAndCloseCallv4?initiate=false&roomId=" + roomId,
            success: function (data) {
                //win.close();
            }
        });
    }


    function refreshPage() {
    console.log("loading table")
        location.reload();
    }

    function refreshVitalsPage() {
        $("#addVitalsDet").empty();
        //location.reload();
        //getMasterVitals(false);
        createVitalPlaceholders(false);
        getEpisodeVitals(episdeId);
        getVitalDocument(episdeId);
    }

    function sendPics() {
    }

    function clearAdvModal() {
        $("#addVisitForm").modal('hide');
        $("#serviceTypeId").val('1');
        $('.getAdvice').val('');
        $('.getAdvice').parent().removeClass('is-dirty');
    }
    function getMasterVitals(callEpisodeVitalAPI = true) {

        $('#load2').show();
        $.ajax({
            type: "POST",
            url: serverPath + "/getMasterVitals",
            headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
            success: function (data) {
                if (data.code == "200") {
                    let { vitalMaster } = data.content;
                    var size = 0;

                    VITALMASTER_SERVER_DATA = vitalMaster
                    createVitalPlaceholders();

                } if (data.errorCode == "1010") {
                    $('#load2').hide();
                    toastr.warning("Server Error");
                }
            },
            error: function (xhr, status, error) {
                if (xhr.status == 0) window.location.href = './login';
                if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                    if (xhr.responseJSON.code === 401) {
                        (async () => {
                            toastr.error(xhr.responseJSON.message);
                            await sleep();
                            window.location.href = './login';
                        })()
                    }
                    //toastr.error(xhr.responseJSON.message);
                } else {
                    toastr.error("No Data Found!");
                }
            }
        });
    }

    function createVitalPlaceholders(callEpisodeVitalAPI = true,) {
        var addVitals = "";
        $("#addVitalsDet").empty();
        if (VITALMASTER_SERVER_DATA && VITALMASTER_SERVER_DATA.length > 0) {
            $.each(VITALMASTER_SERVER_DATA, function (index, option) {
                //console.log("CODE-> ",option.code);

                var masterVitalName = option.name.toLowerCase();
                //console.log("NAME-> ",option.name,", LC=",option.name.toLowerCase());

                if (option.code == "Temp") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pTemp">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">TEMPERATURE</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="tempVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pTemp");

                }
                if (option.code == "PUL") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pPUL">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">PULSE</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="pulseVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pPUL");
                }
                if (option.code == "SPO2") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pSPO2">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">SPO2</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="spo2VitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pSPO2");
                }
                if (option.code == "BP") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pBP">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">BLOOD PRESSURE</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="bpVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pBP");
                }

                if (option.code == "BG") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pBG">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">BLOOD GLUCOSE</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="bldGluVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pBG");
                }

                if (option.code == "WEI") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pWEI">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">WEIGHT</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="weightVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pWEI");
                }

                if (option.code == "HEI") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pHEI">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">HEIGHT</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="heightVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pHEI");
                }

                if (option.code == "BMI") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pBMI">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">BMI</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="bmiVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pBMI");
                }
                if (option.code == "FAT") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pFAT">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">BODY FAT</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="fatVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pFAT");
                }

                if (option.code == "MUS") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pMUS">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">MUSCLE MASS</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="muscleVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pMUS");
                }
                /*
                if(option.code == "BONE"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pBONE">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">BONE</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="boneVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pBONE");
                }
                */

                if (option.code == "WATER") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pWATER">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">WATER CONTENT</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="watContVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pWATER");
                }


                if (option.code == "PRO") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pPRO">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">PROTEIN(INTAKE)</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="protienVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pPRO");
                }

                if (option.code == "hb") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="phb">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">HEMOGLOBIN</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="hbVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "phb");
                }

                /*
               if(option.code == "VFL"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pVFL">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">VISCERAL FAT LEVEL</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="visFatVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pVFL");
                }
                */

                if (option.code == "BM") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pBM">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">BASAL METABOLIC RATE</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="basalMetVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pBM");
                }


                if (option.code == "CHOLES") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pCHOLES">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">CHOLESTEROL</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="cholestrolVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pCHOLES");
                }


                if (option.code == "TRIGLY") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pTRIGLY">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">TRIGLYCERIDES</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="triglyVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pTRIGLY");
                }
                if (option.code == "HDL CHOLES") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pHDLCHOLES">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">HDL CHOLESTEROL</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="hdlCholVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pHDLCHOLES");
                }

                if (option.code == "LDL CHOLES") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pLDLCHOLES">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">LDL CHOLESTEROL</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="ldlCholVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pLDLCHOLES");
                }

                if (option.code == "CHOLES/HDL RATIO") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pCHOLESRATIO">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">CHOLESTEROL/HDL RATIO</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="CholRatioVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pCHOLESRATIO");
                }



                if (option.code == "HEPATITIES B") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pHEPAB">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">HEPATITIES B</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="hepaBVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pHEPAB");
                }

                if (option.code == "HIV") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pHIV">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">HIV</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="hivVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pHIV");
                }
                /*
                if(option.code == "COVID"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pCOVID">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">COVID 19 AG</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="covidVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pCOVID");
                  }
                if(option.code == "RSV_Ag"){
                   addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pRSV_Ag">  <div class="card-main-conatiner">'+
                   '<p class="vital-title">RSV AG</p>'+
                   '<div class="card-scroll-container"> <div class="card-inner" id="RSV_AgId"> </div>'+
                   '</div> </div> </div>';
                   vitalMap.set(masterVitalName, "pRSV_Ag");
               }
               if(option.code == "Legionella_Ag"){
                   addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pLegionella_Ag">  <div class="card-main-conatiner">'+
                   '<p class="vital-title">LEGIONELLA AG</p>'+
                   '<div class="card-scroll-container"> <div class="card-inner" id="Legionella_AgId"> </div>'+
                   '</div> </div> </div>';
                   vitalMap.set(masterVitalName, "pLegionella_Ag");
               }
               if(option.code == "S_pneumonia_Ag"){
                   addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pS_pneumonia_Ag">  <div class="card-main-conatiner">'+
                   '<p class="vital-title">S PNEUMONIA AG</p>'+
                   '<div class="card-scroll-container"> <div class="card-inner" id="S_pneumonia_AgId"> </div>'+
                   '</div> </div> </div>';
                   vitalMap.set(masterVitalName, "pS_pneumonia_Ag");
               }
                if(option.code == "strep_a_ag"){
                   addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pstrep_a_ag">  <div class="card-main-conatiner">'+
                   '<p class="vital-title">STREP A AG</p>'+
                   '<div class="card-scroll-container"> <div class="card-inner" id="strep_a_agId"> </div>'+
                   '</div> </div> </div>';
                   vitalMap.set(masterVitalName, "pstrep_a_ag");
               }
                */
                /*
                if(option.code == "CHIKUNGUNYA"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pCHIKANGUNYA">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">CHIKUNGUNYA IgM/IgG</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="chickenGunVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pCHIKANGUNYA");
                  }
                if(option.code == "DENGUE"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pDENGUE">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">DENGUE</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="dengueVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pDENGUE");
                  }
                if(option.code == "DENGUE IgG"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pDENGUEIgG">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">DENGUE IgG</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="dengueIgGVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pDENGUEIgG");
                  }

                if(option.code == "DENGUE IgM"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pDENGUEIgM">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">DENGUE IgM</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="dengueIgMVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pDENGUEIgM");
                  }
                if(option.code == "Dengue_IgM/IgG"){
                   addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pDengue_IgM_IgG">  <div class="card-main-conatiner">'+
                   '<p class="vital-title">DENGUE IgM/IgG</p>'+
                   '<div class="card-scroll-container"> <div class="card-inner" id="Dengue_IgMIgGId"> </div>'+
                   '</div> </div> </div>';
                   vitalMap.set(masterVitalName, "pDengue_IgM_IgG");
               }
               if(option.code == "Dengue_NS1"){
                   addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pDengue_NS1">  <div class="card-main-conatiner">'+
                   '<p class="vital-title">DENGUE NS1</p>'+
                   '<div class="card-scroll-container"> <div class="card-inner" id="Dengue_NS1Id"> </div>'+
                   '</div> </div> </div>';
                   vitalMap.set(masterVitalName, "pDengue_NS1");
               }
                */
                if (option.code == "MALARIA") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pMALARIA">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">MALARIA</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="malariaVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pMALARIA");
                }

                if (option.code == "NS- 1") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pNS-1">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">NS-1</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="ns1VitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pNS-1");
                }
                /*
                if(option.code == "VitD"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pVitD">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">VITAMIN D</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="vitDVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pVitD");
                  }
                if(option.code == "hCG"){
                   addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="phCG">  <div class="card-main-conatiner">'+
                   '<p class="vital-title">&#946;-hCG</p>'+
                   '<div class="card-scroll-container"> <div class="card-inner" id="hCGId"> </div>'+
                   '</div> </div> </div>';
                   vitalMap.set(masterVitalName, "phCG");
                }
                if(option.code == "LH"){
                   addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pLH">  <div class="card-main-conatiner">'+
                   '<p class="vital-title">LH</p>'+
                   '<div class="card-scroll-container"> <div class="card-inner" id="lhId"> </div>'+
                   '</div> </div> </div>';
                   vitalMap.set(masterVitalName, "pLH");
                }
                if(option.code == "T4"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pT4">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">T4</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="t4VitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pT4");
                }

                if(option.code == "TSH"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pTSH">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">TSH</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="tshVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pTSH");
                  }
                if(option.code == "TSH WB"){
                   addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pTSHWB">  <div class="card-main-conatiner">'+
                   '<p class="vital-title">TSH WB</p>'+
                   '<div class="card-scroll-container"> <div class="card-inner" id="tshWbId"> </div>'+
                   '</div> </div> </div>';
                   vitalMap.set(masterVitalName, "pTSHWB");
               }
                if(option.code == "fT4"){
                   addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pfT4">  <div class="card-main-conatiner">'+
                   '<p class="vital-title">fT4</p>'+
                   '<div class="card-scroll-container"> <div class="card-inner" id="fT4Id"> </div>'+
                   '</div> </div> </div>';
                   vitalMap.set(masterVitalName, "pfT4");
               }
                */

                /*
                if(option.code == "PCT"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pPCT">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">PCT</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="pctMVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pPCT");
                  }
                if(option.code == "CRP"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pCRP">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">CRP</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="crpVitId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pCRP");
                  }
                */

                if (option.code == "T3") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pT3">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">T3</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="t3MVitId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pT3");
                }

                /*
               if(option.code == "CK_MB"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pCK_MB">  <div class="card-main-conatiner">'+
                  '<p class="vital-title">CK MB</p>'+
                  '<div class="card-scroll-container"> <div class="card-inner" id="ckmbId"> </div>'+
                  '</div> </div> </div>';
                  vitalMap.set(masterVitalName, "pCK_MB");
              }
              if(option.code == "Troponin I"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pTroponinI">  <div class="card-main-conatiner">'+
                  '<p class="vital-title">TROPONIN I</p>'+
                  '<div class="card-scroll-container"> <div class="card-inner" id="tnlId"> </div>'+
                  '</div> </div> </div>';
                  vitalMap.set(masterVitalName, "pTroponinI");
              }
             if(option.code == "D-DIMER"){
               addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pD-DIMER">  <div class="card-main-conatiner">'+
               '<p class="vital-title">D-DIMER</p>'+
               '<div class="card-scroll-container"> <div class="card-inner" id="dTimerVitId"> </div>'+
               '</div> </div> </div>';
               vitalMap.set(masterVitalName, "pD-DIMER");
             }
              if(option.code == "NT proBNP"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pNTproBNP">  <div class="card-main-conatiner">'+
                  '<p class="vital-title">NT proBNP</p>'+
                  '<div class="card-scroll-container"> <div class="card-inner" id="ntProId"> </div>'+
                  '</div> </div> </div>';
                  vitalMap.set(masterVitalName, "pNTproBNP");
              }
              if(option.code == "hsCRP"){
                  addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="phsCRP">  <div class="card-main-conatiner">'+
                  '<p class="vital-title">hsCRP</p>'+
                  '<div class="card-scroll-container"> <div class="card-inner" id="hsCRPId"> </div>'+
                  '</div> </div> </div>';
                  vitalMap.set(masterVitalName, "phsCRP");
              }
                */

                /*
                if(option.code == "U_Albumin"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pU_Albumin">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">U ALBUMIN</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="U_AlbuminId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pU_Albumin");
                }
                if(option.code == "HBA1C"){
                     addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="pHBA1C">  <div class="card-main-conatiner">'+
                     '<p class="vital-title">HBA1C</p>'+
                     '<div class="card-scroll-container"> <div class="card-inner" id="hba1cVitId"> </div>'+
                     '</div> </div> </div>';
                     vitalMap.set(masterVitalName, "pHBA1C");
                  }
                */

                if (option.code == "lean_body_mass") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="plean_body_mass">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">LEAN BODY MASS</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="lean_body_massId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "plean_body_mass");
                }
                if (option.code == "relative_fat_mass") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="prelative_fat_mass">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">RELATIVE FAT MASS</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="relative_fat_massId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "prelative_fat_mass");
                }
                if (option.code == "waist") {
                    addVitals += '<div class="col-md-3 col-sm-6 col-xs-12" id="pwaist">  <div class="card-main-conatiner">' +
                        '<p class="vital-title">WAIST CIRCUMFERENCE</p>' +
                        '<div class="card-scroll-container"> <div class="card-inner" id="waistId"> </div>' +
                        '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "pwaist");
                }

                /*
                if(option.code == "iFOB"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="piFOB">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">iFOB</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="iFOBId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "piFOB");
                }
                if(option.code == "h_pylori"){
                    addVitals+='<div class="col-md-3 col-sm-6 col-xs-12" id="ph_pylori">  <div class="card-main-conatiner">'+
                    '<p class="vital-title">h PYLORI</p>'+
                    '<div class="card-scroll-container"> <div class="card-inner" id="h_pyloriId"> </div>'+
                    '</div> </div> </div>';
                    vitalMap.set(masterVitalName, "ph_pylori");
                }

                */

            });

            $("#addVitalsDet").append(addVitals);
            if (callEpisodeVitalAPI) {
                getEpisodeVitals(-1);
            }
        }
    }
    function promtfun(ob) {
        if (ob == 0) {
            var height = prompt("Please enter your height(ft)", "");
            var arr = { "Height": height };
        }
        if (ob == 1) {
            var weight = prompt("Please enter your Weight(Kg)", "");
            var arr = { "Weight": weight };
        }

        var actHeight = $("#heightId").html();
        var actWeight = $("#weightId").html();
        if (height != null && height != '' || weight != null && weight != '') {
            $.ajax({
                type: "POST",
                url: "saveEpisodeVitals?episodeId=" + episdeId + "&patientId=" + patientId + "&status=1&VitalArray=" + JSON.stringify(arr),
                headers: {
                    "deviceId": "ADMIN",
                },
                success: function (data) {
                    if (data.code == "200") {
                        if (height != null && height != '' || weight != null && weight != '') {
                            if (height != null && height != '') {
                                actHeight = height;
                            }
                            if (weight != null && weight != '') {
                                actWeight = weight;
                            }

                            var heightInMeter = actHeight * 0.3048;
                            var BMI = actWeight / (heightInMeter * heightInMeter);

                            if (actHeight != null && actHeight != '' && actWeight != null && actWeight != '') {
                                $("#bmihdr").css("display", "block");
                                $(".bmiValue").html('');
                                $(".bmiValue").html(BMI);
                                $("#bmiExt").html("Kg/m2");
                            }
                        }
                        getEpisodeVitals(episdeId);
                        getVitalDocument(episdeId);
                        toastr.success("succesfully Edited");
                    }
                },
                error: function (x, y, response) {
                    $('#load2').hide();
                    toastr.error("Server Error");
                }
            });
        } else {
            /* toastr.warning("Please Enter the field"); */
        }
    }

    function getEpisodeVitals(id) {
        console.log('Inside getEpisodeVitals: ', id)
        $('#load2').show();
        var epi = "";
        var useId = "";
        if (id == "-1") {
            epi = episdeId;
            /* Please dont change this this is not logged in user id */
            //alert("use"+useId);
            //alert("user"+userId);
        } else { epi = id; }
        useId = userId;
        // console.log("patientUserId", localStorage.getItem("patientUserId"));

        // console.log("patientId: ", localStorage.getItem("patientId"));
        $('.hereClass').html("--");
        $('.emptyExt').html("");
        $.ajax({
            type: "POST",
            url: serverPath + "/getConsumerVitals?userId=" + localStorage.getItem("patientUserId") + "&episodeId=" + epi,
            headers: {
                "deviceId": "ADMIN",
            },
            headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
            success: function (data) {
                console.log('getConsumerVitals: ', data)
                if (data.code == "200") {
               if (data.content.patientComplaint && data.content.patientComplaint === "NA") {
                 console.log('patientComplaint-NA: ', $('#patientComplaint').val())
                   $('#patientComplaint').hide();
                      } else {
                         console.log('patientComplaint: ', $('#patientComplaint').val(), ", val:", data.content.patientComplaint)
                           $('#patientComplaint').show();
                            $('#patientComplaint').text(data.content.patientComplaint);
                          }

                    var height = ""; var weight = ""; var vitalMeasuredTime = '';
                    let { vitals } = data.content;
                    // console.log('---getPatientListByClient?patientList --', patientList)
                    if (vitals && vitals.length > 0) {
                        const myTempMap = new Map(vitals.map(obj => [obj.vitalsName.toLowerCase(), obj.vitalsValue]));
                        vitalMap.forEach((values, keys) => {
                            //console.log(keys,"<br>",values, ",id=","#"+values);
                            if (!myTempMap.has(keys)) {
                                //console.log(keys,"<br>",values, ",id=","#"+values);
                                $("#" + values).remove();
                            }
                        })
                        updateWBioTable(vitals, patGender, patAge);
                        $("#addVitalsDet").show();

                        $.each(vitals, function (index, option) {
                            //console.log("VitalMongo= ", option.vitalsName, ", LC=",option.vitalsName.toLowerCase());
                            var mongoVitalName = option.vitalsName.toLowerCase();
                            //console.log("vitalMap.has(): ",mongoVitalName, " ? ",vitalMap.has(mongoVitalName));

                            if (option.strMonitoredDate != null && option.strMonitoredDate != "") {
                                //console.log("strMonitoredDate->",option.strMonitoredDate)
                                vitalMeasuredTime = getCustomTime(strToDate(option.strMonitoredDate));
                            }

                            if (option.vitalsName.toLowerCase() == "Weight".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' kg</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#weightVitId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "Height".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' cm</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#heightVitId').prepend(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "Temperature".toLowerCase()) {
                                var lowtemp = option.vitalsValue * 9 / 5 + 32;
                                var htmlNew = '<div class="card-floatLeft">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' C/' + parseFloat(lowtemp).toFixed(2) + ' F</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#tempVitId').prepend(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "Pulse".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' b/m</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#pulseVitId').prepend(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "BP".toLowerCase()) {
                                var splt = (option.vitalsValue).split(",");
                                var htmlNew = '<div class="card-floatLeft">' +
                                    '<p class="vital-actual-value">' + splt[0] + '/' + splt[1] + ' mmhg</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#bpVitId').prepend(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "BMI".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' kg/m2</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#bmiVitId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "BG".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' mg/dl</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#bldGluVitId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "SPO2".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' %</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#spo2VitId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "FAT LEVEL".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue.trim() + ' %</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#fatVitId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "WATER CONTENT".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' %</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#watContVitId').prepend(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "MUSCLE".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' kg/m2</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#muscleVitId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "PROTEIN".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' g/day</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#protienVitId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "hb".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' g/dl</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#hbVitId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "VISCERAL FAT LEVEL".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + '</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#visFatVitId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "BASAL METABOLISM".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' Kcal/day</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#basalMetVitId').prepend(htmlNew);
                            }
                            /*
                            if(option.vitalsName.toLowerCase() == "BONE".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-2digitValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' kg</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#boneVitId').prepend(htmlNew);
                            }
                            */
                            if (option.vitalsName.toLowerCase() == "lean_body_mass".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' kg</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#lean_body_massId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "relative_fat_mass".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' %</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#relative_fat_massId').prepend(htmlNew);
                            }
                            if (option.vitalsName.toLowerCase() == "waist".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-2digitValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' Inches</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#waistId').prepend(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "NS- 1".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + '</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#ns1VitId').prepend(htmlNew);
                                //$('#ns1VitId').html(htmlNew);
                            }
                            /*
                            if(option.vitalsName.toLowerCase() == "VitD".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/mL</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#vitDVitId').prepend(htmlNew);
                                //$('#vitDVitId').html(htmlNew);
                              }
                            if(option.vitalsName.toLowerCase() == "T4".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' nmol/L</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#t4VitId').prepend(htmlNew);
                                    //$('#t4VitId').html(htmlNew);
                              }
                            if(option.vitalsName.toLowerCase() == "TSH".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' miU/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#tshVitId').prepend(htmlNew);
                                    //$('#tshVitId').html(htmlNew);
                              }
                            if(option.vitalsName.toLowerCase() == "hCG".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' mIU/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#hCGId').prepend(htmlNew);
                                    //$('#hCGId').html(htmlNew);
                                }
                                if(option.vitalsName.toLowerCase() == "LH".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' mIU/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#lhId').prepend(htmlNew);
                                    //$('#lhId').html(htmlNew);
                                }
                                if(option.vitalsName.toLowerCase() == "fT4".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' pmol/L</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#fT4Id').prepend(htmlNew);
                                    //$('#fT4Id').html(htmlNew);
                                }
                                if(option.vitalsName.toLowerCase() == "TSH WB".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' mIU/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#tshWbId').prepend(htmlNew);
                                    //$('#tshWbId').html(htmlNew);
                                }
                            */

                            /*
                            if(option.vitalsName.toLowerCase() == "DENGUE IgG".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#dengueIgGVitId').prepend(htmlNew);
                                    //$('#dengueIgGVitId').html(htmlNew);
                              }
                            if(option.vitalsName.toLowerCase() == "DENGUE IgM".toLowerCase()){
                              var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#dengueIgMVitId').prepend(htmlNew);
                                    //$('#dengueIgMVitId').html(htmlNew);
                              }
                            if(option.vitalsName.toLowerCase() == "DENGUE".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#dengueVitId').prepend(htmlNew);
                                    //$('#dengueVitId').html(htmlNew);
                              }
                                if(option.vitalsName.toLowerCase() == "CHIKUNGUNYA".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#chickenGunVitId').prepend(htmlNew);
                                    //$('#chickenGunVitId').html(htmlNew);
                              }
                            if(option.vitalsName.toLowerCase() == "Dengue_IgM/IgG".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#Dengue_IgMIgGId').prepend(htmlNew);
                                    //$('#Dengue_IgMIgGId').html(htmlNew);
                                }
                                if(option.vitalsName.toLowerCase() == "Dengue_NS1".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#Dengue_NS1Id').prepend(htmlNew);
                                    //$('#Dengue_NS1Id').html(htmlNew);
                                }
                            */

                            /*
                            if(option.vitalsName.toLowerCase() == "PCT".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#pctMVitId').prepend(htmlNew);
                                    //$('#pctMVitId').html(htmlNew);
                              }
                            if(option.vitalsName.toLowerCase() == "CRP".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' mg/L</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#crpVitId').prepend(htmlNew);
                                    //$('#crpVitId').html(htmlNew);
                              }
                            */

                            if (option.vitalsName.toLowerCase() == "T3".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + ' ng/ml</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#t3VitId').prepend(htmlNew);
                                //$('#t3VitId').html(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "CHOLES".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + '</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#cholestrolVitId').prepend(htmlNew);
                                //$('#cholestrolVitId').html(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "TRIGLY".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + '</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#triglyVitId').prepend(htmlNew);
                                //$('#triglyVitId').html(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "HDL CHOLESTEROL".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + '</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#hdlCholVitId').prepend(htmlNew);
                                //$('#hdlCholVitId').html(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "LDL CHOLESTEROL".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + '</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#ldlCholVitId').prepend(htmlNew);
                                //$('#ldlCholVitId').html(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "CHOLESTEROL/HDL RATIO".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + '</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#CholRatioVitId').prepend(htmlNew);
                                //$('#CholRatioVitId').html(htmlNew);
                            }




                            if (option.vitalsName.toLowerCase() == "HEPATITIES B".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + '</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#hepaBVitId').prepend(htmlNew);
                                //$('#hepaBVitId').html(htmlNew);
                            }

                            if (option.vitalsName.toLowerCase() == "HIV".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + '</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#hivVitId').prepend(htmlNew);
                                //$('#hivVitId').html(htmlNew);
                            }




                            /*
                            if(option.vitalsName.toLowerCase() == "COVID".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#covidVitId').prepend(htmlNew);
                                    //$('#covidVitId').html(htmlNew);
                              }
                            if(option.vitalsName.toLowerCase() == "RSV_Ag".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' </p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#RSV_AgId').prepend(htmlNew);
                                    //$('#RSV_AgId').html(htmlNew);
                                }
                                if(option.vitalsName.toLowerCase() == "Legionella_Ag".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#Legionella_AgId').prepend(htmlNew);
                                    //$('#Legionella_AgId').html(htmlNew);
                                }
                                if(option.vitalsName.toLowerCase() == "S_pneumonia_Ag".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#S_pneumonia_AgId').prepend(htmlNew);
                                    //$('#S_pneumonia_AgId').html(htmlNew);
                                }
                            if(option.vitalsName.toLowerCase() == "strep_a_ag".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#strep_a_agId').prepend(htmlNew);
                                    //$('#strep_a_agId').html(htmlNew);
                                }
                            */
                            if (option.vitalsName.toLowerCase() == "MALARIA".toLowerCase()) {
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">' +
                                    '<p class="vital-actual-value">' + option.vitalsValue + '</p><p class="vital-recorded-time">' + vitalMeasuredTime + '</p></div>';
                                $('#malariaVitId').prepend(htmlNew);
                                //$('#malariaVitId').html(htmlNew);
                            }



                            /*
                            if(option.vitalsName.toLowerCase() == "CK_MB".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#ckmbId').prepend(htmlNew);
                                    //$('#ckmbId').html(htmlNew);
                                }
                            if(option.vitalsName.toLowerCase() == "Troponin I".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/mL</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#tnlId').prepend(htmlNew);
                                    //$('#tnlId').html(htmlNew);
                                }
                            if(option.vitalsName.toLowerCase() == "NT proBNP".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' pg/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#ntProId').prepend(htmlNew);
                                    //$('#ntProId').html(htmlNew);
                                }
                            if(option.vitalsName.toLowerCase() == "D-DIMER".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/mL</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#dTimerVitId').prepend(htmlNew);
                                    //$('#dTimerVitId').html(htmlNew);
                            }
                            if(option.vitalsName.toLowerCase() == "hsCRP".toLowerCase()){
                              var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                '<p class="vital-actual-value">'+option.vitalsValue+ ' mg/L</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                $('#hsCRPId').prepend(htmlNew);
                                //$('#hsCRPId').html(htmlNew);
                            }
                            */



                            /*
                            if(option.vitalsName.toLowerCase() == "iFOB".toLowerCase()){
                              var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                '<p class="vital-actual-value">'+option.vitalsValue+ ' ng/ml</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                $('#iFOBId').prepend(htmlNew);
                                //$('#iFOBId').html(htmlNew);
                            }
                            if(option.vitalsName.toLowerCase() == "h_pylori".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ '</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#h_pyloriId').prepend(htmlNew);
                                    //$('#h_pyloriId').html(htmlNew);
                                }
                            */


                            /*
                            if(option.vitalsName.toLowerCase() == "HBA1C".toLowerCase()){
                                var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' %</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#hba1cVitId').prepend(htmlNew);
                                    //$('#hba1cVitId').html(htmlNew);
                              }
                            if(option.vitalsName.toLowerCase() == "U_Albumin".toLowerCase()){
                                  var htmlNew = '<div class="card-floatLeft-bloodAnalyserValue">'+
                                    '<p class="vital-actual-value">'+option.vitalsValue+ ' mg/L</p><p class="vital-recorded-time">'+ vitalMeasuredTime +'</p></div>';
                                    $('#U_AlbuminId').prepend(htmlNew);
                                    //$('#U_AlbuminId').html(htmlNew);
                                }
                            */


                            $('#vit	Date').html(option.strCreatedDate);
                        });
                        $('#load2').hide();
                    } else {
                        console.log("Error loading Vital episode")
                        $("#addVitalsDet").hide();
                    }


                } else {
                    console.log('data.code: ', data)
                    $("#addVitalsDet").hide();
                }
                //$("#pTemp").remove();
                if (data.errorCode == "1009") {
                    $('#load2').hide();
                    //toastr.warning("No Vitals Found");
                } if (data.errorCode == "1010") {
                    $('#load2').hide();
                    toastr.warning("Server Error");
                }
            },
            error: function (xhr, status, error) {
                console.log('Inside getEpisodeVitals error: ', xhr);
                updateWBioTable([], patGender, patAge);
                $("#addVitalsDet").hide();
                if (xhr.status == 0) window.location.href = './login';
                if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                    if (xhr.responseJSON.code === 401) {
                        (async () => {
                            toastr.error(xhr.responseJSON.message);
                            await sleep();
                            window.location.href = './login';
                        })()
                    }
                    //toastr.error(xhr.responseJSON.message);
                } else {
                    toastr.error("No Data Found!");
                }
            }
        });

        $('#load2').hide();

    }

    var histId = "", tempListIndex = "";

    function patEpisodeChange(id, listIndex = 0, refresh = false, newDrEpisodeId) {
        console.log("Inside patEpisodeChange(): id=", id, ",listIndex= ", listIndex, ",PreId= ", episdeId, ", tempListIndex= ", tempListIndex, 'drOld: ', doctorEpisodeId, 'drNew: ', newDrEpisodeId);
        // alert(id);
        histId = "";

        if (id == null || id == "undefined") {
            //alert(episodeId);alert(episdeId);
            histId = episodeId;
        } else {
            if (id == episdeId && refresh == false) {
                console.log("Same episode button pressed!");
                return;
            }
            histId = id;
            //jk
            episdeId = id;

            $(".normalCol").css("background", "#B8B8B8");

            if (refresh == true) {
                $("#" + tempListIndex).css("background", "#367fa9");
            } else {
                console.log("Test = ", listIndex);
                $("#" + listIndex).css("background", "#367fa9");
                tempListIndex = listIndex;
            }
        }
        doctorEpisodeId = newDrEpisodeId;
        createVitalPlaceholders(false);
        getEpisodeVitals(episdeId);
        getVitalDocument(episdeId);
        getDrEpisodeDetails(episdeId, listIndex, newDrEpisodeId);
    }

    function getVitalDocument(id) {
        console.log('Inside getVitalDocument: ', id);
        var histIds = "";
        if (id == null || id == "-1") {
            histIds = episdeId;
        } else {
            histIds = id;
        }

        var addDoc = "";
        var addEcgDoc = "", addAliveCoreEcgDoc='', threeLeadEcg='', addImedrixDoc='';
        var ecg3LeadHeadr = false, ecg6LeadHeadr = false, ecgHdstethLeadHeadr = false, imedrixHeader = false;

        $.ajax({
            type: "POST",
            url: serverPath + "/getEcgReports?episodeId=" + histIds,
            headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
            success: function (data) {
                if (data.code == "200") {
                 $('#electroCardioHeader').show();
                    if (data.content != null && data.content.ecgReports) {
                        $.each(data.content.ecgReports, function (index, option) {
                            if(S3_MIGRATION) {
                        let ai_analysis = option.aiAnalysis && option.aiAnalysis!= ""? option.aiAnalysis: "NA";
                            if (option.filePath != null) {
                                //console.log("ECG:", option, option.fileType,option.filePath);
                                var createdDate = '';
                                      if (option.createdAt != null && option.createdAt != '') {
                                      //createdDate= new Date(option.createdAt).toLocaleString();
                                      //createdDate= new Date(option.createdAt).toISOString();
                                      var m = new Date(option.createdAt);
                                      var dateString = m.getUTCFullYear() + "/" + (m.getUTCMonth() + 1) + "/" + m.getUTCDate() + " " + m.getUTCHours() + ":" + m.getUTCMinutes() + ":" + m.getUTCSeconds();
                                      createdDate = m.getUTCFullYear() + "/" + (m.getUTCMonth() + 1) + "/" + m.getUTCDate() + " " + getCustomTime(strToDate(dateString));
                                  }
                                if (option.fileType != "SPO2" && option.fileType != "ecg" && option.fileType != "hdstethPdf" && option.fileType != "hdstethAudio" && option.fileType != "12Leadecg"
                                    && option.fileType != "HdEcg"  && option.fileType != "ecgAtc" || option.vendor == 'pc303') {

                                    var tagvar = option.fileType;

                                    var arr = tagvar.split(":");
                                    if (arr[1] == undefined || arr[1] == null || arr[1] == "null") {
                                        arr[1] = '';
                                    }
                                    if(ai_analysis === "NA") ai_analysis =arr[1];
                                    ecg3LeadHeadr = true;
                                    let reportId = option.id;
                                    let url = serverPath + "/downloadEcgReport?episodeId=" + histIds + "&vitalReportId=" + reportId + "&clientId=" + localStorage.getItem("clientId");
                                        addDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"> <p style="color:#000000; font-weight: bold; text-decoration: underline;  padding-left: 5px; " href="' +  imagePath + option.filePath + '><b style="text-transform:uppercase;">' + arr[0].trim() + '_ </b></p></span>' +
                                        '<span> Date: ' + createdDate + '</span>' +
                                        '<span> &emsp; AI_Analysis: ' + ai_analysis + '</span>' +
                                        '<div class="info-box" style="display: inline-flex; padding-bottom: 10px;">' +
                                        '<img style = "width:100%; height:100%" alt="ecg" src="' + url + '" >' +
                                        '</div>' +
                                        '</div>';

                                } else if (option.fileType != "ecgAtc" && (option.fileType == "ecg" || option.vendor == 'alivecore')) {

                                    let reportId = option.id;
                                    ecg6LeadHeadr = true;
                                    addAliveCoreEcgDoc += '<div class="col-xs-12">' +
                                    '<span style="display:inline-flex;"><div style="color:#0000FF; text-decoration: underline; cursor: pointer;" onclick="openAlivecoreReport(event, ' + histIds + ', ' + reportId + ')"><b style="text-transform:uppercase;">AliveCor_ECG Report_' + createdDate + '</b></div> &emsp; AI_Analysis: ' + ai_analysis + '</span>' +
                                                                            '</div>';
                                }else if (option.fileType == "hdstethPdf" && option.vendor == 'hdsteth') {
                                ecgHdstethLeadHeadr = true;
                                let reportId = option.id;
                                    addEcgDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"><div style="color:#0000FF; text-decoration: underline; cursor: pointer;" onclick="openHdstethReport(event, ' + histIds + ', ' + reportId + ')"><b style="text-transform:uppercase;">HDSTETH_REPORT' + createdDate +'</b></div></span>' +
                                        '</div>';
                                }else if (option.fileType == "hdstethAudio" && option.vendor == 'hdsteth') {
                                  ecgHdstethLeadHeadr = true;
                                  let reportId = option.id;
                                  let audioURL = serverPath + "/downloadEcgReport?episodeId=" + histIds + "&vitalReportId=" + reportId + "&clientId=" + localStorage.getItem("clientId");
                                  console.log("hd steth boolean value",ecgHdstethLeadHeadr);
                                  addEcgDoc += '<div class="col-xs-12"><div class="audio-link" onclick="playAudio(this);" data-file-path="' + audioURL + '">' +
                                        '<b style="text-transform: uppercase; cursor: pointer; color:#0000FF; text-decoration: underline;">HDSTETH_AUDIO' + createdDate +'</b>' +
                                        '</div></div>';
                                   }else if (option.fileType == "ecgFilePath") {
                                    addEcgDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="' + imagePath + option.filePath + '" target="_blank"><b style="text-transform:uppercase;">ECG Report</b></a></span>' +
                                        '</div>';
                                } else if (option.fileType == "12Leadecg") {
                                    let reportId = option.id;
                                imedrixHeader = true;
                                    addImedrixDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"><div style="color:#0000FF; text-decoration: underline; cursor: pointer;" onclick="openImedrixReport(event, ' + histIds + ', ' + reportId + ')"><b style="text-transform:uppercase;">6/12Lead ECG Report_'+ createdDate +'</b></div></span>' +
                                        '</div>';
                                } else if (option.fileType == "HdEcg") {
                                let reportId = option.id;
                                    addEcgDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"><div style="color:#0000FF; text-decoration: underline; cursor: pointer;" onclick="openHdEcgReport(event, ' + histIds + ', ' + reportId + ')"><b style="text-transform:uppercase;">HDSTETH ECG Report_'+ createdDate +'</b></div></span>' +
                                        '</div>';
                                }
                            }

                            $('#addDates').html(option.strCreatedDate);
                        }else if(!S3_MIGRATION){
                            let ai_analysis = option.aiAnalysis && option.aiAnalysis!= ""? option.aiAnalysis: "NA";
                            if (option.filePath != null) {
                                //console.log("ECG:", option, option.fileType,option.filePath);
                                var createdDate = '';
                                  if (option.createdAt != null && option.createdAt != '') {
                                  //createdDate= new Date(option.createdAt).toLocaleString();
                                  //createdDate= new Date(option.createdAt).toISOString();
                                  var m = new Date(option.createdAt);
                                  var dateString = m.getUTCFullYear() + "/" + (m.getUTCMonth() + 1) + "/" + m.getUTCDate() + " " + m.getUTCHours() + ":" + m.getUTCMinutes() + ":" + m.getUTCSeconds();
                                  createdDate = m.getUTCFullYear() + "/" + (m.getUTCMonth() + 1) + "/" + m.getUTCDate() + " " + getCustomTime(strToDate(dateString));
                                }
                                if (option.fileType != "SPO2" && option.fileType != "ecg" && option.fileType != "hdstethPdf" && option.fileType != "hdstethAudio" && option.fileType != "12Leadecg"
                                    && option.fileType != "HdEcg"  && option.fileType != "ecgAtc" || option.vendor == 'pc303') {

                                    var tagvar = option.fileType;

                                    var arr = tagvar.split(":");
                                    if (arr[1] == undefined || arr[1] == null || arr[1] == "null") {
                                        arr[1] = '';
                                    }
                                    if(ai_analysis === "NA") ai_analysis =arr[1];
                                    ecg3LeadHeadr = true;
                                        addDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"> <p style="color:#000000; font-weight: bold; text-decoration: underline;  padding-left: 5px; " href="' +  imagePath + option.filePath + '><b style="text-transform:uppercase;">' + arr[0].trim() + '_ </b></p></span>' +
                                        '<span> Date: ' + createdDate + '</span>' +
                                        '<span> &emsp; AI_Analysis: ' + ai_analysis + '</span>' +
                                        '<div class="info-box" style="display: inline-flex; padding-bottom: 10px;">' +
                                        '<img style = "width:100%; height:100%" alt="ecg" src="' + imagePath + option.filePath + '" >' +
                                        '</div>' +
                                        '</div>';

                                } else if (option.fileType != "ecgAtc" && (option.fileType == "ecg" || option.vendor == 'alivecore')) {
                                    ecg6LeadHeadr = true;
                                    addAliveCoreEcgDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="' +  imagePath + option.filePath + '" target="_blank"><b style="text-transform:uppercase;">AliveCor_ECG Report_'+ createdDate +'</b></a> &emsp; AI_Analysis: '+ ai_analysis +'</span>' +
                                        '</div>';
                                }else if (option.fileType == "hdstethPdf" && option.vendor == 'hdsteth') {
                                ecgHdstethLeadHeadr = true;

                                    addEcgDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="' +  imagePath + option.filePath + '" target="_blank"><b style="text-transform:uppercase;">HDSTETH_REPORT' + createdDate +'</b></a> </span>' +
                                        '</div>';
                                }else if (option.fileType == "hdstethAudio" && option.vendor == 'hdsteth') {
                                    ecgHdstethLeadHeadr = true;
                                    console.log("hd steth boolean value",ecgHdstethLeadHeadr);
                                      addEcgDoc += '<div class="col-xs-12">' +
                                      '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="' +  imagePath + option.filePath + '" target="_blank"><b style="text-transform:uppercase;">HDSTETH_AUDIO' + createdDate +'</b></a> </span>' +
                                       '</div>';
                                }else if (option.fileType == "ecgFilePath") {
                                    addEcgDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="' + imagePath + option.filePath + '" target="_blank"><b style="text-transform:uppercase;">ECG Report</b></a></span>' +
                                        '</div>';
                                } else if (option.fileType == "12Leadecg") {
                                imedrixHeader = true;
                                    addImedrixDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="' + imagePath + option.filePath + '" target="_blank"><b style="text-transform:uppercase;">6/12Lead ECG Report_'+ createdDate +'</b></a></span>' +
                                        '</div>';
                                } else if (option.fileType == "HdEcg") {
                                    addEcgDoc += '<div class="col-xs-12">' +
                                        '<span style="display:inline-flex;"><a style="color:#0000FF; text-decoration: underline;  padding-left: 5px; " href="' + imagePath + option.filePath + '" target="_blank"><b style="text-transform:uppercase;">HDSTETH ECG Report_'+ createdDate +'</b></a></span>' +
                                        '</div>';
                                }
                            }

                            $('#addDates').html(option.strCreatedDate);

                        }
                        });
                    }

                    if(ecg3LeadHeadr == false){
                     $('#add3LeadEcgsHeader').hide();
                     $('#add3LeadEcgs').hide();
                    }else if(ecg3LeadHeadr == true){
                        $('#add3LeadEcgsHeader').show();
                        $('#add3LeadEcgs').show();
                     $('#add3LeadEcgs').html(addDoc);
                    }

                   if(ecg6LeadHeadr == false){
                   $('#addEcgsHeader').hide();
                   $('#addEcgs').hide();
                   }else if(ecg6LeadHeadr == true){
                    $('#addEcgsHeader').show();
                   $('#addEcgs').show();
                   $('#addEcgs').html( addAliveCoreEcgDoc);
                   }

                    if(ecgHdstethLeadHeadr == false){
                    $('#addHdstethHeader').hide();
                    $('#addHdsteth').hide();
                    }else if(ecgHdstethLeadHeadr == true){
                        $('#addHdstethHeader').show();
                    $('#addHdsteth').show();
                    $('#addHdsteth').html( addEcgDoc);
                    }

                    if(imedrixHeader == false){
                    $('#addimedrixHeader').hide();
                    $('#addImedrix').hide();
                    }else if(imedrixHeader == true){
                        $('#addimedrixHeader').show();
                    $('#addImedrix').show();
                    $('#addImedrix').html(addImedrixDoc);
                    }

                }  if (data.errorCode == "1090") {
                    $('#load2').hide();
                    //toastr.warning("No ECG Found");
                } if (data.errorCode == "1010") {
                    $('#load2').hide();
                    toastr.warning("Server Error");
                }
            },
            error: function (xhr, status, error) {
                if (xhr.status == 0) window.location.href = './login';
                if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                    if (xhr.responseJSON.code === 401) {
                        (async () => {
                            toastr.error(xhr.responseJSON.message);
                            await sleep();
                            window.location.href = './login';
                        })()
                    } else if(xhr.responseJSON.code === 404) {
                    ecg3LeadHeadr == false
                    ecg6LeadHeadr == false
                    ecgHdstethLeadHeadr == false
                    imedrixHeader == false
                        //$('#add3LeadEcgsHeader').hide();
                        //$('#addEcgsHeader').hide();
                        //$('#addHdstethHeader').hide();
                        //$('#addimedrixHeader').hide();
                        //$('#addimedrixHeader').hide();
                        $('#electroCardioHeader').hide();

                    }
                    //toastr.error(xhr.responseJSON.message);
                } else {
                    toastr.error("No Data Found!");
                }
            }
        });
    }

    function logout() {
        localStorage.removeItem("DW_JWT_TOKEN");
        localStorage.removeItem("roleName");
        localStorage.removeItem("clientId");
        localStorage.removeItem("userGroupCode");
        localStorage.removeItem("loggedInUserFullName");
        localStorage.removeItem("kitPermission");
        localStorage.removeItem("patientId");
        localStorage.removeItem("patientUserId");
        window.location.href = "logout";
    }

    function EmrReport() {
        //var emrreport = serverPath + "/getEmrPdfReport?patientId=" + patientId + "&userId=" + userId + "&productId=" + productId + "&episodeId=" + episdeId;
        var emrreport = serverPath + "/getDrEmrPdfReport?userId=" + userId + "&episodeId=" + episdeId
        window.open(emrreport);
    }



    function getPatientDetails() {
        var addDoc = ""; var bg = ""; var gender = ""; var mob = ""; var address = ""; var email = ""; var addaDet = ""; var age = '';
        var uhid = ""; var city = "";  var village = "";  var empId = ""; var pinCodeNo = ""; var abhaNo = "";
        $.ajax({
            type: "POST",
            url: serverPath + "/getPatientDetails?patientId=" + patientId,
            headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
            success: function (data) {
                if (data.code == "200") {
                    var option = data.content.patientDetails;
                    var patName = "";
                    patGender = option.gender; patAge = option.age;
                    console.log(option);
                    console.log("EMPId",option.empId);
                    console.log("Village",option.village);
                    console.log("PIncode",option.pincode);

                    if (option.patientFirstName != null) {
                        patName += option.patientFirstName;
                    }
                    if (option.patientLastName != null) {
                        patName += " " + option.patientLastName;
                    }
                    if (option.bloodGroup != null) {
                        bg = option.bloodGroup;

                    } if (option.gender == "Male") {
                        gender = '<img src="resources/images/male.png" style="width:12px">';

                    } if (option.gender == "Female") {
                        gender = '<img src="resources/images/femenine.png" style="width:12px">';

                    } if (option.mobileNo != null) {
                        mob = option.mobileNo;
                    }
                    if (option.address != null) {
                        address = option.address;
                    }
                    if (option.emailId != null) {
                        email = option.emailId;
                    }
                    if (option.uhid != null) {
                        uhid = option.uhid;
                    }
                    if (option.city != null) {
                        city = option.city
                    } else city = "-";
                    if (option.village != null) {
                        village = option.village
                     } else village = "-";
                     if (option.pincode != null) {
                       pinCodeNo = option.pincode
                      }
                      if (option.empId != null) {
                            empId = option.empId
                      }
                      if (option.abhaNumber != null) {
                            abhaNo = option.identityProofNumber
                      }
                    age = option.age;

                    console.log('----UHID----', option.uhid)
                    $('#addPrfName').html(patName);
                    $('#uhidId').html(uhid);
                    $('#abhaNo').html(abhaNo);

                    if (email != "") {
                       addaDet += '<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">' +
                                   '<b>' + email + ' </b>' +
                                   '</li>';
                    }

                    if (bg != "") {
                        addaDet += '<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;font-size:12px">' +
                            '<b>' + age + ' / ' + bg + ' / ' + gender + ' </b>' +
                            '</li>';
                    } else {
                        addaDet += '<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;font-size:12px">' +
                            '<b>' + age + ' / ' + gender + ' </b>' +
                            '</li>';
                    }

                    if (mob != "") {
                        addaDet += '<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;font-size:12px">' +
                            '<b>' + mob + ' </b>' +
                            '</li>';
                    }
                    if (address != "") {
                        addaDet += '<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">' +
                            '<b>' + address + ' </b>' +
                            '</li>';
                    }
                    if (option.village != "" && option.city != "") {
                    addaDet += '<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">' +
                     '<b>' + city + ' / ' + village + ' </b>' +
                     '</li>';
                   }
                    if (option.pincode != "" ) {
                    addaDet += '<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">' +
                     '<b>'+ pinCodeNo + ' </b>' +
                     '</li>';
                   }
                     if (option.empIde != "" ) {
                    addaDet += '<li class="list-group-item text-center" style="border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom:15px;">'+
                     '<b>'  + empId + ' </b>' +
                     '</li>';
                   }
                    $('#addDet').html(addaDet);
                } if (data.errorCode == "1010") {
                    $('#load2').hide();
                    toastr.warning("Server Error");
                }
            },
            error: function (xhr, status, error) {
                if (xhr.status == 0) window.location.href = './login';
                if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                    if (xhr.responseJSON.code === 401) {
                        (async () => {
                            toastr.error(xhr.responseJSON.message);
                            await sleep();
                            window.location.href = './login';
                        })()
                    }
                    //toastr.error(xhr.responseJSON.message);
                } else {
                    toastr.error("No Data Found!");
                }
            }
        });
    }

    var histId = "", tempListIndex = "";


    function navigateHome() {
        if (localStorage.getItem("userGroupCode") === 'NUR')
            window.location.href = 'patientList';
        else
            window.location.href = 'home2';
    }

    function setFileName(input) {

        $("#showReportFileName").text($(input).val());
    }
    function openFileLocation(filePathLocation) {
        //alert(filePathLocation);
        window.open(imagePath + filePathLocation);
    }

    /* 	function initiateCall(calleProfileId){
            var proId = localStorage.getItem("productId");
            if(proId == 1310){
                alert("This Service is not enabled, Please contact Wenzins");
            } else {
                var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
                win.addEventListener("resize", function(){
                                win.resizeTo(1024, 768);
                });
            }
        } */


    function initiateCall(calleProfileId) {
        var proId = localStorage.getItem("productId");
        if (proId == 1310) {
            alert("This Service is not enabled, Please contact Wenzins");
        } else {
            /* var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
            win.addEventListener("resize", function(){
                            win.resizeTo(1024, 768);
            }); */
            $.ajax({
                type: "POST",
                url: serverPath + "/intiateAndCloseCallv4?profileId=" + localStorage.getItem("profileId") + "&calleProfileId=" + calleProfileId + "&serverKeyId=" + localStorage.getItem("serverKeyId") + "&initiate=" + true,
                success: function (data) {
                    //	console.log(data);
                    roomid = data.roomid;
                    //	alert(data);
                    //	console.log("new  "+data.content.weconurl);

                    videocalllink = data.content.weconurl + data.roomid;

                    /* 	 $.each(data.content,function(index, option) {
                             console.log(option);
                             var url = option.weconurl;
                        //	 alert(url);
                             console.log("link"+url);
                             var win = window.open(url);
                              videocalllink =  url + roomid ;
                    //	var win = window.open(videocalllink , 'result', 'width=1024,height=768');
                 }); */
                    //	 window.open(a);
                    var win = window.open(videocalllink, 'result', 'width=300,height=300');

                    win.addEventListener("resize", function () {
                        win.resizeTo(1024, 768);
                    });
                }
            });
        }
    }




    //=======================NEXT AND PREVIOUS BUTTON FOR EPISODE============================
    function next(id) {

        if (test == s - 2) {
            document.getElementById("nextId").disabled = true;
        }


        document.getElementById("prevId").disabled = false;

        test = test + 1;
        var previous = test - 4;

        document.getElementById(previous).style.display = "none";
        document.getElementById(test).style.display = '';

        document.getElementById("prevId").style.display = "";           //to display prev button
    }


    function previous(id) {

        document.getElementById("nextId").disabled = false;

        if (test == 4) {
            document.getElementById("prevId").disabled = true;
        }

        var previous = test - 4;

        document.getElementById(test).style.display = 'none';
        document.getElementById(previous).style.display = '';

        test = test - 1;
    }
    //============================================================================================================

    function canMed() {

        $("#addMedicationModal").modal('hide');
        //dosageID startId days commentsMedId
        $(".bClassM").val('');
        $(".bClassM").parent().removeClass('is-dirty');
        $('#fequencyId').val('0-0-1');
         $('#medicationType').val('Tablet');
        $('#instructionId').val('Before Food');
        $("#dynTableMed").hide();
        $("#medAdd").empty();


    }

    function canHistory() {

                     $("#addHistoryModal").modal('hide');
                     $(".bClassM").val('');
                     $(".bClassM").parent().removeClass('is-dirty');
                     $('#fequencyId').val('0-0-1');
                     $('#instructionId').val('Before Food');
                     $("#dynTableMed").hide();
                     $("#medAdd").empty();

                 }


function complaintModalClose() {

                     $("#chief-complaint-Modal").modal('hide');
                     //dosageID startId days commentsMedId
                     $(".bClassM").val('');
                     $(".bClassM").parent().removeClass('is-dirty');
                     $('#fequencyId').val('0-0-1');
                     $('#instructionId').val('Before Food');
                     $("#dynTableMed").hide();
                     $("#medAdd").empty();

                 }
function canClinicalExamin() {

                     $("#clinical-examination-modal").modal('hide');

                     $(".bClassM").val('');
                     $(".bClassM").parent().removeClass('is-dirty');
                     $('#fequencyId').val('0-0-1');
                     $('#instructionId').val('Before Food');

                     $("#rsExamine").val('Clinically Normal');
                      $("#geExamine").val('Clinically Normal');
                       $("#paExamine").val('Clinically Normal');
                        $("#gutExamine").val('Clinically Normal');
                         $("#cvsExamine").val('Clinically Normal');
                         $("#cnsExamine").val('Clinically Normal');
                         $("#otherFindings").empty();


                 }
    function checkRSvalue(val)
    {
        if(val==="Other Findings")
             $('#rsExamineText').attr('disabled', false);

        else
             $('#rsExamineText').attr('disabled', true);
             $('#rsExamineText').empty();

    }
    function checkGEvalue(val)
        {
            if(val==="Other Findings")
                 $('#geExamineText').attr('disabled', false);

            else
                 $('#geExamineText').attr('disabled', true);

        }
    function checkPAvalue(val)
            {
                if(val==="Other Findings")
                     $('#PAExamineText').attr('disabled', false);

                else
                     $('#PAExamineText').attr('disabled', true);

            }
    function checkGUTvalue(val)
                {
                    if(val==="Other Findings")
                         $('#gutExamineText').attr('disabled', false);

                    else
                         $('#gutExamineText').attr('disabled', true);

                }
    function checkCVSvalue(val)
                    {
                        if(val==="Other Findings")
                             $('#cvsExamineText').attr('disabled', false);

                        else
                             $('#cvsExamineText').attr('disabled', true);

                    }
    function checkCNSvalue(val)
         {
              if(val==="Other Findings")
                    $('#cnsExamineText').attr('disabled', false);
                        else
                    $('#cnsExamineText').attr('disabled', true);

         }

    function canNote() {
        //alert();
        $("#addNotesModal").modal('hide');
        //$('.getNotes').text('');
        $('textarea').val('')
    }
    function clearRep() {
        $("#addReportModal").modal('hide');
        $('#reportFileId').val('');
        $(".getReport").empty();
        $('#showReportFileName').text('');
    }
    function reportAdvice(episodeId, docId, id) {
        // alert("Fdgfg");
        var appendPrescriptionImage = "";
        if (docId == 1) {
            $("#addreport").html('');
            $(".preReport").each(function () {

                var spltAttr = $(this).attr("attr");
                if (spltAttr == id) {
                    var splitHere = $(this).val().split("/");
                    var splitHere1 = $(this).val().split(",");

                    appendPrescriptionImage += "<br><a class='text-left' path='&quot;" + splitHere1 + "&quot'' "
                        + "style='cursor:pointer;padding-top:0px; padding-bottom: 5px;color:rgb(63, 81, 181);"
                        + "font-size: 15px;' onclick='javascript:showInOtherPage(" + id + ",&quot;" + splitHere1 + "&quot;)' "
                        + " >" + splitHere[splitHere.length - 1] + "</a>";
                }
            });

            $("#addreport").html(appendPrescriptionImage);


            if (appendPrescriptionImage == "") {

                mes = "No records found";
                $('#demo-show-snackbar').trigger('click');
            } else {
                $("#reportAdvice").modal('show');
            }
        }


        if (docId == 2) {
            $("#addreport").html(''); var appendPrescriptionImage = "";
            $(".repReport").each(function () {
                var spltAttr = $(this).attr("attr");
                if (spltAttr == id) {
                    var splitHere = $(this).val().split("/");
                    var splitHere1 = $(this).val().split(",");



                    appendPrescriptionImage += "<br><a class='text-left' path='&quot;" + splitHere1 + "&quot'' "
                        + "style='cursor:pointer;padding-top:0px; padding-bottom: 5px;color:rgb(63, 81, 181);"
                        + "font-size: 15px;' onclick='javascript:showInOtherPage(" + id + ",&quot;" + splitHere1 + "&quot;)' "
                        + ">" + splitHere[splitHere.length - 1] + "</a>";
                }



            });

            $("#addreport").html(appendPrescriptionImage);

            if (appendPrescriptionImage == "") {
                console.log(appendPrescriptionImage);
                mes = "No records found";
                $('#demo-show-snackbar').trigger('click');
            } else {
                $("#reportAdvice").modal('show');
            }

        }


    }

    function openChiefComplaintModal(update=false) {
        if(update){
            $('#saveComplaintButton').hide();
            $('#updateComplaintButton').show();
    }else{
            $('#saveComplaintButton').show();
            $('#updateComplaintButton').hide();
    }

        $("#chief-complaint-Modal").modal('show');
    }
        function openHistoryModal(update=false) {
                 if(update){
                 $('#saveHistoryButton').hide();
                 $('#updateHistoryButton').show();
            }else{
        $('#saveHistoryButton').show();
        $('#updateHistoryButton').hide();
}

    $("#addHistoryModal").modal('show');
}
    function openclinicallyExamine(update=false) {
    console.log("Open clinical examin modal")
        if(update){
            $('#addClinicalExaminButton').hide();
            $('#updateClinicalExaminButton').show();
    }else{
            $('#addClinicalExaminButton').show();
            $('#updateClinicalExaminButton').hide();
    }

        $("#clinical-examination-modal").modal('show');
    }
    function openMedicationModal() {
            $("#addMedicationModal").modal('show');
            $('#saveMedicationButton').hide();
            $('#addMedicationButton').show();
            $('#updateMedicationButton').hide();

        var prescribedBy = $("#loggedInUserFullName").text();
        $("#prescribedByPre").parent().addClass('is-dirty');
        $("#prescribedByPre").val(prescribedBy);
    }

    function openObservationModal() {
        $("#addObservationModal").modal('show');

    }
    function openAdviceModal(update=false) {
        //$("#addObservationModal").modal('show');
        if(update){
            	$('#saveAdviceButton').hide();
            	$('#updateAdviceButton').show();
        }else{
            	$('#saveAdviceButton').show();
            	$('#updateAdviceButton').hide();
        }
        $("#addVisitForm").modal('show');
        // var prescribedBy = $("#loggedInUserFullName").text();
        // $("#prescribedByAdv").parent().addClass('is-dirty');
        // $("#prescribedByAdv").val(prescribedBy);
    }
    function closeAdviceModal() {
        $("#addVisitForm").modal('hide');
    }

    function openNotesModal(update=false) {
    if(update){
    	$('#savePrelimDiagButton').hide();
    	$('#updatePrelimDiagButton').show();
    }else{
    	$('#savePrelimDiagButton').show();
    	$('#updatePrelimDiagButton').hide();
    }
        $("#addNotesModal").modal('show');
    }
    function closeNotesModal() {
        $("#addNotesModal").modal('show');
    }

    function openFollowUpModal(update=false) {
    if(update){
    	$('#saveFollowUpButton').hide();
    	$('#updateFollowUpButton').show();
    }else{
    	$('#saveFollowUpButton').show();
    	$('#updateFollowUpButton').hide();
    }
        $("#addFollowUpModal").modal('show');
    }
    function closeFollowUpModal() {
        $("#addFollowUpModal").modal('hide');
    }

    function openReportModal(update=false) {
    if(update){
        	$('#saveInvestigationButton').hide();
        	$('#updateInvestigationButton').show();
    }else{
        	$('#saveInvestigationButton').show();
        	$('#updateInvestigationButton').hide();
            $('#investigationTextId').val("");
    }
        $("#addReportModal").modal('show');
        $("#firstmedfile").val('');
    }
    function closeReportModal() {
        $("#addReportModal").modal('hide');
    }

    function completeConsultation() {
        //alert(appointmentId);
        $.ajax({
            type: "POST",
            url: serverPath + "/completeConsultation?appointmentId=" + appointmentId,
            success: function (data) {
                if (data.code == '200') {
                    //alert(data.message);
                    window.location.href = 'list';
                    toastr.success("Appointment Completed Successfully");
                }
            }
        })
    }

    //Santhosh: Get Time from given string date
    function strToDate(dtStr) {
        //console.log("Inside strToDate()->",dtStr)
        if (!dtStr) return null
        let dateParts = dtStr.split("/");
        let timeParts = dateParts[2].split(" ")[1].split(":");
        dateParts[2] = dateParts[2].split(" ")[0];
        // month is 0-based, that's why we need dataParts[1] - 1
        return dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0], timeParts[0], timeParts[1], timeParts[2]);
    }

    function getCustomTime(time) {
        //console.log("Inside getCustomTime()->",time)
        let ampm = "am";

        var h = time.getHours();
        var m = time.getMinutes();
        // add a zero in front of numbers<10
        if (h >= 12) {
            ampm = "pm";
            h = h % 12 === 0 ? h : h % 12;
        }

        h = checkTime(h);
        m = checkTime(m);

        //return `${h}:${m} ${ampm}`;
        return h + ':' + m + ' ' + ampm;
    }

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

//ALIVECORE ECG REPORT
    function openAlivecoreReport(event, histIds, reportId) {
        event.preventDefault();
        $("#loader").show();
        //var exampleURL ="https://www.example.com"
       var reportURL = "https://docs.google.com/gview?embedded=true&url=" + encodeURIComponent(serverPath + "/downloadEcgReport?episodeId=" + histIds + "&vitalReportId=" + reportId + "&clientId=" + localStorage.getItem("clientId") + "&downloadFile=false");
        // Create the iframe
        var iframe = $('<iframe>', {
          src: reportURL,
          width: '100%',
          height: '480',
          type: 'application/pdf',
          target: '_blank',
          frameborder: 0
        });
          iframe.on("load", function () {
            $("#loader").hide();
          });
        // Create the close button
        var closeButton = $('<button>', {
          text: 'Close',
          click: function() {
            $('#iframeAlivecoreContainer').empty(); // Remove the iframe when the button is clicked
          }
        }).addClass('close-button');

        // Append the iframe and close button to the container
        $('#iframeAlivecoreContainer').html('').append(iframe, closeButton);
      }

//HDSTETH ECG REPORT

function openHdstethReport(event, histIds, reportId) {
        event.preventDefault();

         var reportURL = "https://docs.google.com/gview?embedded=true&url=" + encodeURIComponent(serverPath + "/downloadEcgReport?episodeId=" + histIds + "&vitalReportId=" + reportId + "&clientId=" + localStorage.getItem("clientId") + "&downloadFile=false");

        var iframe = $('<iframe>', {
          src: reportURL,
          width: '100%',
          height: '480',
          type: 'application/pdf',
          target: '_parent',
          frameborder: 0
        });

        // Create the close button
        var closeButton = $('<button>', {
          text: 'Close',
          click: function() {
            $('#iframeHdsteth').empty(); // Remove the iframe when the button is clicked
          }
        }).addClass('close-button');

        // Append the iframe and close button to the container
        $('#iframeHdsteth').html('').append(iframe, closeButton);
      }

 //OPEN HDSTETH AUDIO FILE
 function playAudio(link) {
  var audioURL = link.getAttribute("data-file-path");

  // Open the audio URL in a new tab
  window.open(audioURL, "_blank");
}
//  function playAudio(link) {
//   // Get the modal element
//   var modal = document.getElementById("audioModal");

//   // Get the audio player element
//   var audioPlayer = document.getElementById("audioPlayer");

//   // Get the close button element inside the modal
//   var closeButton = modal.querySelector(".close");

//   var audioFilePath = link.dataset.filePath;

//   // Set the audio source
//   audioPlayer.src = audioFilePath;

//   // Open the modal
//   modal.style.display = "block";

//   // Click event listener for the close button to close the modal
//   closeButton.addEventListener("click", function () {
//     modal.style.display = "none";
//   });

//   // Click event listener for clicking outside the modal to close it
//   window.addEventListener("click", function (event) {
//     if (event.target == modal) {
//       modal.style.display = "none";
//     }
//   });
// }


//IMEDRIX ECG REPORT

function openImedrixReport(event, histIds, reportId) {
    event.preventDefault();

    var reportURL = "https://docs.google.com/gview?embedded=true&url=" + encodeURIComponent(serverPath + "/downloadEcgReport?episodeId=" + histIds + "&vitalReportId=" + reportId + "&clientId=" + localStorage.getItem("clientId") + "&downloadFile=false");
    // Create the iframe
    var iframe = $('<iframe>', {
      src: reportURL,
      width: '100%',
      height: '480',
      type: 'application/pdf',
      target: '_parent',
      frameborder: 0
    });

    // Create the close button
    var closeButton = $('<button>', {
      text: 'Close',
      click: function() {
        $('#iframeImedrix').empty(); // Remove the iframe when the button is clicked
      }
    }).addClass('close-button');

    // Append the iframe and close button to the container
    $('#iframeImedrix').html('').append(iframe, closeButton);
  }


//HDECG REPORT

function openHdEcgReport(event, histIds, reportId) {
    event.preventDefault();

  var reportURL = "https://docs.google.com/gview?embedded=true&url=" + encodeURIComponent(serverPath + "/downloadEcgReport?episodeId=" + histIds + "&vitalReportId=" + reportId + "&clientId=" + localStorage.getItem("clientId") + "&downloadFile=false");
    // Create the iframe
    var iframe = $('<iframe>', {
      src: reportURL,
      width: '100%',
      height: '480',
      type: 'application/pdf',
      target: '_parent',
      frameborder: 0
    });

    // Create the close button
    var closeButton = $('<button>', {
      text: 'Close',
      click: function() {
        $('#iframeImedrix').empty(); // Remove the iframe when the button is clicked
      }
    }).addClass('close-button');

    // Append the iframe and close button to the container
    $('#iframeImedrix').html('').append(iframe, closeButton);
  }


</script>

<!-- <tr>
	<td>4.</td>
	<td>Fix and squish bugs</td>
	<td>
 		<div class="progress progress-xs progress-striped active">
   			<div class="progress-bar progress-bar-success" style="width: 90%"></div>
 		</div>
	</td>
	<td><span class="badge bg-green">90%</span></td>

</tr> -->

</html>
