<%@ include file="/WEB-INF/view/includes.jsp" %>
    <!DOCTYPE>
    <html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title>Patient List</title>

        <link rel="stylesheet" href="resources/css/bootstrap.min.css" />
        <link rel="stylesheet" href="resources/css/toastr.min.css" />
        <link href="resources/css/AdminLTE.css" rel="stylesheet" />
        <link href="resources/css/_all-skins.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="resources/css/font-awesome.min.css" />
        <link rel="stylesheet" href="resources/css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="resources/css/material.css">

        <script src="resources/js/jQuery-2.1.4.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/fastclick.min.js"></script>
        <script src="resources/customJS/videoCall.js"></script>
        <script src="resources/js/demo.js"></script>
        <script src="resources/js/customHTML.js"></script>
        <script src="resources/js/toastr.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>

        <style type="text/css">
            .backImage {
                background-image: url("resources/images/profile.jpg");
            }
        </style>
        <style>
            .toggleSwitch {
                position: relative;
                display: inline-block;
                width: 120px;
                height: 34px;
            }

            .toggleSwitch .toggleInput {
                display: none;
            }

            .dateSlider {
                position: absolute;
                cursor: pointer;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: #2ab934;
                -webkit-transition: .4s;
                transition: .4s;
            }

            .dateSlider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

            .toggleInput:checked+.dateSlider {
                background-color: #ca2222;
            }

            .toggleInput:focus+.slider {
                box-shadow: 0 0 1px #2196F3;
            }

            .toggleInput:checked+.dateSlider:before {
                -webkit-transform: translateX(85px);
                -ms-transform: translateX(85px);
                transform: translateX(85px);
            }

            /*------ ADDED CSS ---------*/
            .on {
                display: none;
            }

            .on,
            .off {
                color: white;
                position: absolute;
                transform: translate(-50%, -50%);
                top: 50%;
                left: 50%;
                font-size: 12px;
                font-family: Verdana, sans-serif;
            }

            .toggleInput:checked+.dateSlider .on {
                display: block;
            }

            .toggleInput:checked+.dateSlider .off {
                display: none;
            }

            /*--------- END --------*/

            /* Rounded sliders */
            .dateSlider.round {
                border-radius: 34px;
            }

            .dateSlider.round:before {
                border-radius: 50%;
            }

            .navbar-t-centered {
                position: absolute;
                left: 34%;
                display: block;
                text-align: center;
                color: #fff;
                top: 0px;
                padding: 15px;
                font-weight: 400;
                font-size: 12px;
            }

            @media screen and (min-width:768px) {
                .navbar-t-centered {
                    position: absolute;
                    left: 38%;
                    display: block;
                    /*  width: 160px; */
                    text-align: center;
                    color: #fff;
                    top: 0px;
                    padding: 15px;
                    font-weight: 400;
                    font-size: 22px;
                }
            }
        </style>
        <script type="text/javascript">
            //  var	serverPath ="http://192.168.1.225:9090/PopulationCarev2";
            var productId = localStorage.getItem("productId");
            var loggedInUserId = '';
            const DEF_DELAY = 1000;

            function sleep(ms) {
                return new Promise(resolve => setTimeout(resolve, ms || DEF_DELAY));
            }
            $(document)
                .ready(
                    function () {

                        if (localStorage.getItem("userGroupCode") === 'DOC') {
                            pingCallForVC();
                        }

                        var loggedInUserFullName = localStorage.getItem("loggedInUserFullName");
                        $("#loggedInUserFullName").text(loggedInUserFullName);
                        $("#loggedInUserFullNameSpan").text(loggedInUserFullName);

                        $.ajax({
                            type: "POST",
                            url: serverPath
                                // + "/getPatientList?productId="+ productId, &kitSerialNumber=localStorage.getItem("kitPermission")
                                + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId")+ '&size=10000',
                            headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
                            success: function (data) {
                                console.log('---getPatientListByClient?SUCCS --', data.content)

                                if (data.code == '200') {
                                    var listOfCards = "";
                                    $("#searchedInput").val("");
                                    let { patientList } = data.content;
                                    console.log('---getPatientListByClient?patientList --', patientList)
                                    if (patientList) {
                                        let { contentArray } = patientList;
                                        console.log('---getPatientListByClient?contentArray --', contentArray, ', leng: ', contentArray.length)
                                        if (contentArray && contentArray.length > 0) {
                                            $.each(contentArray, function (index, option) {
                                                var lastName = "";
                                                var uhid = option.uhid;
                                                if (option.uhid == null
                                                    || option.uhid == ""
                                                    || option.uhid == "undefined"
                                                    || option.uhid == "null") {
                                                    uhid = " ";
                                                }
                                                if (option.bloodGroup == null) {
                                                    option.bloodGroup = "";
                                                }
                                                if (option.maritalStatus == null) {
                                                    option.maritalStatus = "";
                                                }
                                                if (option.patientLastName != null) {
                                                    lastName = option.patientLastName;
                                                }
                                                listOfCards += '<div class="col-md-3 col-sm-6 col-xs-12" style="margin-top:10px;">'
                                                    + '<div class="box box-primary">'
                                                    + '<div class="box-body box-profile" style="height:304px;">'
                                                    + '<img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">'
                                                    + '<h5 style="font-size: 16px;" class="text-center">'
                                                    + option.patientFirstName
                                                    + ' '
                                                    + lastName
                                                    + '</h5>'
                                                    + '<p class="text-muted text-center" style="margin: 0 0 10px;">'
                                                    + uhid
                                                    + '</p>'

                                                    + '<ul class="list-group list-group-unbordered" style="font-size: 12px;">'

                                                    + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                    + '<b>'
                                                    + option.mobileNo
                                                    + '</b>'
                                                    + '</li>'
                                                    + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                    + '<b>'
                                                    + option.bloodGroup
                                                    + ' / '
                                                    + option.maritalStatus
                                                    + ' '
                                                    + '</b>'
                                                    + '</li>'

                                                    + '</ul>'
                                                    + '<a href="#" class="btn btn-primary btn-block" onclick="openDashboard('
                                                    + option.id + ',' + option.userId
                                                    + ');"><b>More</b></a>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>'
                                            });
                                            $("#patientListCard").html(listOfCards);
                                        } else {
                                            $("#patientListCard").html("<h5  class='text-center'>Patient table empty!</h5>");
                                        }
                                    } else {
                                        $("#patientListCard").html("<h5  class='text-center'>No Patients found</h5>");
                                    }
                                } else {
                                    $("#patientListCard").html("<h5  class='text-center'>No Patient Available for today !</h5>");
                                }

                            },
                            error: function (xhr, status, error) {
                                if (xhr.status == 0) window.location.href = './login';
                                if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                                    if (xhr.responseJSON.code === 401) {
                                        (async () => {
                                            toastr.error(xhr.responseJSON.message);
                                            await sleep();
                                            window.location.href = './login';
                                        })()
                                    }
                                    toastr.error(xhr.responseJSON.message);
                                } else {
                                    toastr.error("No Data Found!");
                                }
                            }
                        });
                    })

            function openDashboard(patientId, patUId) {
                localStorage.setItem("patientId", patientId);
                localStorage.setItem("patientUserId", patUId);
                window.location.href = "dashBoard";
            }
            function openRegistration() {
                window.location.href = "doctorRegistration";
            }
            function searchForPatient() {

                /* var servicePath = "http://localhost:9090/Doctor"; */
                var searchedInput = $("#searchedInput").val();
                console.log("search: ", searchedInput);
                $
                    .ajax({
                        type: "POST",
                        // url: serverPath + "/getPatientList?searchParameter="+ searchedInput + "&productId=" + productId,
                        url: serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId") + '&size=10000' + "&searchParameter=" + searchedInput,
                        headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
                        success: function (data) {
                            if (data.code == '200') {
                                var listOfCards = "";
                                $("#patientListCard").html("");
                                $("#searchedInput").val("");
                                $
                                    .each(
                                        data.content,
                                        function (index, option) {
                                            var uhid = option.uhid;
                                            if (option.episodeId != null) {
                                                var EpisodeId = option.episodeId
                                            }
                                            if (option.id != null) {
                                                var ProfileId = option.id;
                                            }
                                            if (option.uhid == null
                                                || option.uhid == ""
                                                || option.uhid == "undefined"
                                                || option.uhid == "null") {
                                                uhid = " ";
                                            }

                                            if (option.bloodGroup == null) {
                                                option.bloodGroup = "";
                                            }
                                            if (option.maritalStatus == null) {
                                                option.maritalStatus = "";
                                            }
                                            console.log("option: ", option);
                                            listOfCards += '<div class="col-md-3 col-sm-6 col-xs-12" style="margin-top:10px;">'
                                                + '<div class="box box-primary">'
                                                + '<div class="box-body box-profile" style="height:304px;">'
                                                + '<img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">'
                                                + '<h5 style="font-size: 16px;" class="text-center">'
                                                + option.patientFirstName
                                                + ' '
                                                + option.patientLastName
                                                + '</h5>'
                                                + '<p class="text-muted text-center" style="margin: 0 0 10px;">'
                                                + uhid
                                                + '</p>'

                                                + '<ul class="list-group list-group-unbordered" style="font-size: 12px;">'

                                                + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                + '<b>'
                                                + option.mobileNo
                                                + '</b>'
                                                + '</li>'
                                                + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                + '<b>'
                                                + option.bloodGroup
                                                + ' / '
                                                + option.maritalStatus
                                                + ' '
                                                + '</b>'
                                                + '</li>'

                                                + '</ul>'
                                                + '<a href="#" class="btn btn-primary btn-block" onclick="openDashboard('
                                                + option.id
                                                + ');"><b>More</b></a>'
                                                + '</div>'
                                                + '</div>'
                                                + '</div>'

                                            /*listOfCards += '<div id="" class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="padding:10px;" onclick="openDashboard();">'
                                                +'<div style="display:inline-flex;">'
                                                        +'<img class="user-image" alt="User Image" src="resources/images/profile.jpg" style="padding-top:3px;max-width:150px;max-height:200px;">'
                                                        +'<div id="cardBody" class="col-md-9" style="padding-left: 0px; padding-right: 0px;">'

                                                    +'<div class="" style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;height:200px;width:100%;padding-top:10px;">'
                                                        +'<div class="col-md-12" style="padding-left:5px;padding-right:0px;">'
                                                        +'<p style="background-color:red;width:100%;"></p>'
                                                            +'<p style="display:inline-flex;">Name : '+option.patientFirstName+' '+option.patientLastName+'</p><br>'
                                                            +'<p style="display:inline-flex;">Blood Group : '+option.bloodGroup+'</p><br>'
                                                            +'<p style="display:inline-flex;">Mobile No : '+option.mobileNo+'</p><br>'
                                                            +'<p style="display:inline-flex;">Gender : '+option.gender+'</p><br>'
                                                            +'<p style="display:inline-flex;">Marital Status : '+option.maritalStatus+'</p><br>'
                                                        +'</div>'
                                                    +'</div>'

                                                                +'</div>'

                                                +'</div>'
                                               +'</div>' */
                                        });
                                $("#patientListCard").html(listOfCards);

                            } else if (data.errorCode == '1009') {

                                var listOfCards = "";
                                $("#patientListCard").html("");
                                toastr.warning('NO DATA');
                            }

                        },
                        error: function (xhr, status, error) {
                            if (xhr.status == 0) window.location.href = './login';
                            if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                                if (xhr.responseJSON.code === 401) {
                                    (async () => {
                                        toastr.error(xhr.responseJSON.message);
                                        await sleep();
                                        window.location.href = './login';
                                    })()
                                }
                                toastr.error(xhr.responseJSON.message);
                            } else {
                                toastr.error("No Data Found!");
                            }
                        }
                    });

            }

            $(document).ready(function () {
                $("#patReportBtn").hide();

                /*
                if (localStorage.getItem("userGroupCode") === 'NUR')
                    $("#patListLogout").hide();
                if (localStorage.getItem("roleName") === 'Client Admin'){
                    $("#patReportBtn").show();
                }
                */
                $('#birthDat').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,//.date
                    endDate: '1d'
                }).on('changeDate', function (e) {
                    $("#birthDat").attr("readonly", true);
                    $('#birthDat').parent().addClass("is-dirty");
                    $(this).datepicker('hide');
                });
                // clearReg();
            });
            let DOWNLOAD_CSV_TOGGLE_STATE = 'TODAY';
                        $(document).ready(function () {
                            $(".toggleSwitch .toggleInput").on("change", function (e) {
                                const isOn = e.currentTarget.checked;
                                //console.log('Toggle btn change: ',isOn, DOWNLOAD_CSV_TOGGLE_STATE)

                                if (isOn) {
                                    $(".hideme").show();
                                    DOWNLOAD_CSV_TOGGLE_STATE = 'CUSTOM';
                                } else {
                                    $(".hideme").hide();
                                    DOWNLOAD_CSV_TOGGLE_STATE = 'TODAY';
                                }
                            });
                        });


            function searchForDate() {

                var searchInput = $("#birthDat").val();
                if(!searchInput){
                    toastr.error('Selected date is null');
                    return;
                }

                var values = searchInput.split("/");
                var fromDate = new Date(values[2], (values[1] - 1), values[0]).setHours(0, 0, 0, 0, 0);
                var toDate = new Date(values[2], (values[1] - 1), values[0]).setHours("23", "59", 59, 0);

                if (searchInput != '') {
                    $
                        .ajax({
                            type: "POST",
                            // url: serverPath + "/getByDatePatientList?date="+ searchInput + "&productId=" + productId,
                            url: serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId") + '&size=10000' + "&startDate=" + fromDate+ "&endDate=" + toDate,
                            headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
                            success: function (data) {
                                if (data.code == '200') {
                                    var listOfCards = "";
                                    $("#patientListCard").html("");
                                    $("#searchedInput").val("");
                                    $
                                        .each(
                                            data.content,
                                            function (index, option) {
                                                var uhid = option.uhid;
                                                if (option.episodeId != null) {
                                                    var EpisodeId = option.episodeId
                                                }
                                                if (option.id != null) {
                                                    var ProfileId = option.id;
                                                }
                                                if (option.uhid == null
                                                    || option.uhid == ""
                                                    || option.uhid == "undefined"
                                                    || option.uhid == "null") {
                                                    uhid = " ";
                                                }

                                                if (option.bloodGroup == null) {
                                                    option.bloodGroup = "";
                                                }
                                                if (option.maritalStatus == null) {
                                                    option.maritalStatus = "";
                                                }
                                                listOfCards += '<div class="col-md-3 col-sm-6 col-xs-12" style="margin-top:10px;">'
                                                    + '<div class="box box-primary">'
                                                    + '<div class="box-body box-profile" style="height:304px;">'
                                                    + '<img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">'
                                                    + '<h5 style="font-size: 16px;" class="text-center">'
                                                    + option.patientFirstName
                                                    + ' '
                                                    + option.patientLastName
                                                    + '</h5>'
                                                    + '<p class="text-muted text-center" style="margin: 0 0 10px;">'
                                                    + uhid
                                                    + '</p>'

                                                    + '<ul class="list-group list-group-unbordered">'

                                                    + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                    + '<b>'
                                                    + option.mobileNo
                                                    + '</b>'
                                                    + '</li>'
                                                    + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                    + '<b>'
                                                    + option.bloodGroup
                                                    + ' / '
                                                    + option.maritalStatus
                                                    + ' '
                                                    + '</b>'
                                                    + '</li>'

                                                    + '</ul>'
                                                    + '<a href="#" class="btn btn-primary btn-block" onclick="openDashboard('
                                                    + option.id
                                                    + ');"><b>More</b></a>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>'

                                            });
                                    $("#patientListCard").html(listOfCards);

                                } else if (data.errorCode == '1009') {

                                    var listOfCards = "";
                                    $("#patientListCard").html("");
                                    toastr.warning('NO DATA');
                                }

                            },
                            error: function (xhr, status, error) {
                                if (xhr.status == 0) window.location.href = './login';
                                if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                                    if (xhr.responseJSON.code === 401) {
                                        (async () => {
                                            toastr.error(xhr.responseJSON.message);
                                            await sleep();
                                            window.location.href = './login';
                                        })()
                                    }
                                    toastr.error(xhr.responseJSON.message);
                                } else {
                                    toastr.error("No Data Found!");
                                }
                            }
                        });
                } else {
                    toastr.warning('Please enter date');
                }

            }

            function confPopUp(callerName, roomName, roomId, weconUrl) {
                var room = encodeURIComponent(roomName);
                videocalllink = weconUrl + roomId;
                var win;
                var r = confirm(callerName + " is calling you. Do you accept the call?");
                //	alert(r);
                if (r == true) {
                    win = window.open(videocalllink, 'result', 'width=300,height=300');

                    /* win.addEventListener("resize", function(){
                        win.resizeTo(1024, 768);
                    }); */
                }
                $.ajax({
                    type: "POST",
                    url: serverPath + "/intiateAndCloseCallv4?initiate=false&roomId="
                        + roomId,
                    success: function (data) {
                        //win.close();
                    }
                });
            }

            function initiateCall(calleProfileId) {
                var proId = localStorage.getItem("productId");
                if (proId == 1310) {
                    alert("This Service is not enabled, Please contact Wenzins");
                } else {
                    /* var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
                    win.addEventListener("resize", function(){
                            win.resizeTo(1024, 768);
                    }); */
                    $.ajax({
                        type: "POST",
                        url: serverPath + "/intiateAndCloseCallv4?profileId="
                            + localStorage.getItem("profileId")
                            + "&calleProfileId=" + calleProfileId + "&serverKeyId="
                            + localStorage.getItem("serverKeyId") + "&initiate="
                            + true,
                        success: function (data) {
                            //	console.log(data);
                            roomid = data.roomid;
                            //	alert(data);
                            //	console.log("new  "+data.content.weconurl);

                            videocalllink = data.content.weconurl + data.roomid;

                            /* 	 $.each(data.content,function(index, option) {
                                     console.log(option);
                                     var url = option.weconurl;
                                //	 alert(url);
                                     console.log("link"+url);
                                     var win = window.open(url);
                                      videocalllink =  url + roomid ;
                            //	var win = window.open(videocalllink , 'result', 'width=1024,height=768');
                            }); */
                            //	 window.open(a);
                            var win = window.open(videocalllink, 'result',
                                'width=300,height=300');

                            win.addEventListener("resize", function () {
                                win.resizeTo(1024, 768);
                            });
                        }
                    });
                }
            }

            function logout() {
                localStorage.removeItem("DW_JWT_TOKEN");
                localStorage.removeItem("roleName");
                localStorage.removeItem("clientId");
                localStorage.removeItem("userGroupCode");
                localStorage.removeItem("loggedInUserFullName");
                localStorage.removeItem("kitPermission");
                localStorage.removeItem("patientId");
                localStorage.removeItem("patientUserId");
                window.location.href = "logout";
            }

            $(function () {
                $("#datepicker1").datepicker({
                 format: "dd/mm/yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    endDate: "today"
                }).datepicker('update', new Date());
            });

            $(function () {
                $("#datepicker2").datepicker({
                 format: "dd/mm/yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    endDate: "today"
                }).datepicker('update', new Date());
            });

function downloadPatientCsv() {

    console.log('downloadPatientCsv->', $('#datepicker1Val').val(), ', ', $('#datepicker2Val').val(), DOWNLOAD_CSV_TOGGLE_STATE)

    var profileID = localStorage.getItem("profileId");

    var serverKeyId2 = localStorage.getItem("serverKeyId");



    if (DOWNLOAD_CSV_TOGGLE_STATE === 'TODAY') {

        fromDate = new Date();

        toDate = new Date();

        fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(), 0, 0, 0, fromDate.getMilliseconds());

        toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(), 23, 59, 59, toDate.getMilliseconds());

        // console.log('---TODAY: ', fromDate, ', ', toDate);

    } else {



        var values = $('#datepicker1Val').val().split("/");

        fromDate = new Date(values[2], (values[1] - 1), values[0]);

        var values2 = $('#datepicker2Val').val().split("/");

        toDate = new Date(values2[2], (values2[1] - 1), values2[0]);



        // console.log('---CUSTOM: ', fromDate, ', ', toDate);



        var today = new Date().setHours("23", "59", 59, 0);

        if (toDate == null || toDate == 'Invalid Date' || fromDate == null || fromDate == 'Invalid Date') {

            toastr.error('Selected date is null');

            return;

        }



        if (toDate.getTime() > today || fromDate.getTime() > today) {

            toastr.error('Date Cannot Be Greater Than Todays Date');

            return;

        }

        if (toDate.getTime() < fromDate.getTime()) {

            toastr.error('To Date cannot be Lesser Than From Date');

            return;

        }

        fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),

            0, 0, 0, fromDate.getMilliseconds());

        toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),

            23, 59, 59, toDate.getMilliseconds());

        //fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),0, 0, 0, fromDate.getMilliseconds());

        //toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),23, 59, 59, toDate.getMilliseconds());

    }



    // console.log("----DATE-----", fromDate, toDate)

    //var fromDate, toDate, today = new Date().setHours("23", "59", 59, 0);

    fromDate = new Date(fromDate).getTime();

    toDate = new Date(toDate).getTime();



    console.log("downloadPatientCsv-> ", "From: ", fromDate, "To: ", toDate);

    //return;

    var newURL = serverPath + "/getPatientReportByClientId?clientId=" + localStorage.getItem("clientId") + "&startEpoch=" + fromDate + "&endEpoch=" + toDate;

    //console.log(newURL);

    // return newURL;

    window.location.href = newURL;

};
            function resetReportDatePicker() {
                $("#datepicker1").datepicker('update', new Date());
                $("#datepicker2").datepicker('update', new Date());

            };


      //LOGOUT WHEN BACK BUTTON CLICKED

      if (performance.navigation.type == 2) {

      let text = " Do you want to Logout ?...Your Session will be closed";

      if (confirm(text) == true){

                 localStorage.removeItem("DW_JWT_TOKEN");
                 localStorage.removeItem("roleName");
                 localStorage.removeItem("clientId");
                 localStorage.removeItem("userGroupCode");
                 localStorage.removeItem("loggedInUserFullName");
                 localStorage.removeItem("kitPermission");
                 localStorage.removeItem("patientId");
                 localStorage.removeItem("patientUserId");
                 localStorage.removeItem("productId");
                 window.location.href = "logout";
          }else {

                  window.location.href = "patientList";
                }

       }
        </script>


    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div>

            <header class="main-header">
                <nav class="navbar navbar-static-top" role="navigation"
                    style="margin-left: 0% !important; max-height: 50px; background-color: #3c8dbc">
                    <!-- Sidebar toggle button-->
                    <img src="resources/images/WriztoLogoBox.jpg" class="user-image"
                        onclick="window.location.href = 'home';" alt="User Image"
                        style="cursor: pointer;height: 50px; padding-left: 5px; margin-left: 5px;"> <span
                        class="navbar-t-centered">Wrizto Healthcare System </span>


                    <!-- <button type="button" class="btn btn-default.button4 {padding: 32px 16px;} px-3"><i class="fas fa-balance-scale" aria-hidden="true"></i></button>
	 -->


                    <div class="navbar-custom-menu">

                        <ul class="nav navbar-nav">
                            <!--  <li style="top: 8px;"><label class="switch"><input id="someSwitchOptionInfo" type="checkbox" checked> <span class="slider round"></span></label></li>
             -->
                            <!-- Messages: style can be found in dropdown.less-->

                            <!-- Patient Report csv download -->
                            <li>
                                <!-- Trigger the modal with a button -->

                                <button type="button" class="btn btn-lg" style="background-color: transparent;"
                                    id="patReportBtn" data-toggle="modal" data-target="#myModal">
                                    <img src="resources/images/file2.png"
                                        style="width: 25px; height: 25px; margin-top: 3px;" alt="FIle Icon" />
                                </button> <!-- Modal -->
                                <div id="myModal" class="modal fade" role="dialog" data-backdrop="false">
                                    <div class="modal-dialog modal-sm">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header" style="border-bottom: 3px solid #3c8dbc;">
                                                <button type="button" class="close" onClick="resetReportDatePicker()"
                                                    data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Report</h4>
                                            </div>
                                            <div class="modal-body">


                                                <label class="toggleSwitch"> <input type="checkbox" id="togBtn"
                                                        class="toggleInput">
                                                    <div class="dateSlider round">
                                                        <!--ADDED HTML -->
                                                        <span class="on">Custom</span> <span class="off">Today</span>
                                                        <!--END-->
                                                    </div>
                                                </label>

                                                <div class="hideme" style="display:none;">
                                                    <label>Start : </label>
                                                    <div id="datepicker1" class="input-group date"
                                                        data-date-format="dd/mm/yyyy">
                                                        <input id="datepicker1Val" class="form-control" type="text"
                                                            readonly /> <span class="input-group-addon"><i
                                                                class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>

                                                    <label>End : </label>
                                                    <div id="datepicker2" class="input-group date"
                                                        data-date-format="dd/mm/yyyy">
                                                        <input id="datepicker2Val" class="form-control" type="text"
                                                            readonly /> <span class="input-group-addon"><i
                                                                class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" style="background-color:#3c8dbc; color:white"
                                                    class="btn btn-default"
                                                    onClick="window.location.href =downloadPatientCsv();">Download</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </li>



                            <!-- User menu dropdown -->
                            <li class="dropdown user user-menu"><a href="#" class="dropdown-toggle"
                                    data-toggle="dropdown"> <img src="resources/images/avatar.png" class="user-image"
                                        alt="User Image"> <span class="hidden-xs" id="loggedInUserFullName"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->

                                    <li class="user-header"><img src="resources/images/avatar.png" class="img-circle"
                                            alt="User Image">
                                        <p>
                                            <span id="loggedInUserFullNameSpan"></span>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body"
                                        style="padding-top: 0px; padding-right: 0px; padding-left: 0px;">
                                        <!-- <div class="col-xs-7 text-center"><span style="font-size: 12px; margin-left: -33px;" id="nurseNameForActiveId"></span>
                                  <a id= "videoIconId" style="border-radius: 50%; ;margin-left: 5px;background-color: green" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                  				   <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a>
                                  </div>
                                  <div class="col-xs-5 text-center">
                                    <a href="patientList">Patient List</a>
                                  </div> activeUsersId  activeUsersId-->

                                        <ul class="col-xs-12 text-center" id="activeUsersId">
                                            <!-- <li class="form-control" style="border-radius:6px;" ><span style="font-size: 12px;text-transform: capitalize;margin-left: -33px;" id="nurseNameForActiveId">Wrizto Nurse</span>
                 			 <a  id= "videoIconId" style="border-radius: 50%;margin-left: 5px;float:right;background-color: green" class="btn btn-primary" title="videoCall" onclick="initiateCall(1358)"><b>
                 			 	<i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a></li>
                 				 -->

                                        </ul> <br>
                                        <ul>
                                            <br>
                                            <!--  <span style="top: 4px;"> <button style="padding: 10px 10px; type="button" class="btn btn-success"><i class="fa fa-video-camera fa-lg" aria-hidden="true"></i>&nbsp;<span><b>Wecon </b></span></button>
                 				  <button style="padding: 10px 10px; type="button" class="btn btn-success "><i class="fa fa-video-camera fa-lg" aria-hidden="true"></i>&nbsp;<span><b>Mscp</b></span></button>
                 				 </span> -->

                                        </ul>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left"></div>
                                        <div class="pull-right" id='patListLogout'>
                                            <a onclick="logout();" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->

                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->



            <div class="col-md-12"
                style="background-color: white !important; width: 100%; padding-bottom: 30px; display: inline-flex;">
                <div class="input-group" style="margin-top: 10px;">
                    <input type="text" id="searchedInput" class="form-control" placeholder="Search"> <span
                        class="input-group-btn">
                        <button class="btn btn-search" type="button" style="background-color: #3c8dbc; color: white;"
                            onclick="searchForPatient();">
                            <i class="fa fa-search fa-fw"></i> Search
                        </button>
                    </span>
                </div>

                <div class=""></div>

                <!-- <label>Date of Birth:<i style="color: red">*</i>
				</label> -->
                <div class="input-group" style="margin-top: 10px; padding-left: 40px;">
                    <div class="input-group-addon" style="background-color: #3c8dbc">
                        <i style="color: white" class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control clearAll" id="birthDat" name="dateOfBirth"
                        data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                    </input> <span class="input-group-btn">
                        <button class="btn btn-search" type="button" style="background-color: #3c8dbc; color: white;"
                            onclick="searchForDate();">
                            <i class="fa fa-search fa-fw"></i> Search
                        </button>
                    </span>
                </div>




            </div>

            <div class="col-md-12" id="patientListCard" style="padding-bottom: 60px;"></div>
            <!-- Content Wrapper. Contains page content -->


            <div class="content-wrapper" style="margin-left: 0% !important; margin-bottom: 10px;"></div>
            <input type="hidden" id="abcdefg" value="">



            <footer class="main-footer"
                style="margin-left: 0% !important; position: fixed; bottom: 0px; margin-right: auto; margin-left: auto; width: 100%;">
                <!--  <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>.</strong> All rights reserved. -->
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                Copyright &copy; 2021-2022 <a href="http://www.wrizto.com/" target=_blank
                    style="color: rgb(60, 141, 188);">Wrizto
                    Healthcare Pvt. Ltd.</a> All rights reserved.
            </footer>

            <!-- Control Sidebar -->

            <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
    </body>

    </html>