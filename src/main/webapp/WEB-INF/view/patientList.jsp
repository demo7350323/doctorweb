<%@include file="/WEB-INF/view/includes.jsp" %>
    <!DOCTYPE>
        <html>

        <head>

            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

            <title>Patient List</title>

            <link rel="stylesheet" href="resources/css/bootstrap.min.css" />
            <link rel="stylesheet" href="resources/css/toastr.min.css" />
            <link href="resources/css/AdminLTE.css" rel="stylesheet" />
            <link href="resources/css/_all-skins.min.css" rel="stylesheet" />
            <link rel="stylesheet" href="resources/css/font-awesome.min.css" />
            <link rel="stylesheet" href="resources/css/bootstrap-datepicker.css">
            <link rel="stylesheet" href="resources/css/material.css">

            <script src="resources/js/jQuery-2.1.4.min.js"></script>
            <script src="resources/js/bootstrap.min.js"></script>
            <script src="resources/js/fastclick.min.js"></script>
            <script src="resources/customJS/videoCall.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
            <!-------DATA TABLE---->
            <script src="resources/js/jquery.dataTables.min.js"></script>
            <script src="resources/js/dataTables.bootstrap.min.js"></script>

            <script src="resources/js/demo.js"></script>
            <script src="resources/js/customHTML.js"></script>
            <script src="resources/js/toastr.min.js"></script>
            <script src="resources/js/bootstrap-datepicker.js"></script>

            <style type="text/css">
                .backImage {
                    background - image: url("resources/images/profile.jpg");
                }
            </style>
            <style>
                .toggleSwitch {
                    position: relative;
                    display: inline-block;
                    width: 120px;
                    height: 34px;
                }

                .toggleSwitch .toggleInput {
                    display: none;
                }

                .dateSlider {
                    position: absolute;
                    cursor: pointer;
                    top: 0;
                    left: 0;
                    right: 0;
                    bottom: 0;
                    background-color: #2ab934;
                    -webkit-transition: .4s;
                    transition: .4s;
                }

                .dateSlider:before {
                    position: absolute;
                    content: "";
                    height: 26px;
                    width: 26px;
                    left: 4px;
                    bottom: 4px;
                    background-color: white;
                    -webkit-transition: .4s;
                    transition: .4s;
                }

                .toggleInput:checked+.dateSlider {
                    background - color: #ca2222;
                }

                .toggleInput:focus+.slider {
                    box - shadow: 0 0 1px #2196F3;
                }

                .toggleInput:checked+.dateSlider:before {
                    -webkit - transform: translateX(85px);
                    -ms-transform: translateX(85px);
                    transform: translateX(85px);
                }

                /*------ ADDED CSS ---------*/
                .on {
                    display: none;
                }

                .on,
                .off {
                    color: white;
                    position: absolute;
                    transform: translate(-50%, -50%);
                    top: 50%;
                    left: 50%;
                    font-size: 12px;
                    font-family: Verdana, sans-serif;
                }

                .toggleInput:checked+.dateSlider .on {
                    display: block;
                }

                .toggleInput:checked+.dateSlider .off {
                    display: none;
                }

                /*--------- END --------*/

                /* Rounded sliders */
                .dateSlider.round {
                    border - radius: 34px;
                }

                .dateSlider.round:before {
                    border - radius: 50%;
                }

                .navbar-t-centered {
                    position: absolute;
                    left: 34%;
                    display: block;
                    text-align: center;
                    color: #fff;
                    top: 0px;
                    padding: 15px;
                    font-weight: 400;
                    font-size: 12px;
                }

                @media screen and (min-width:768px) {
                    .navbar - t - centered {
                        position: absolute;
                        left: 38%;
                        display: block;
                        /*  width: 160px; */
                        text-align: center;
                        color: #fff;
                        top: 0px;
                        padding: 15px;
                        font-weight: 400;
                        font-size: 22px;
                    }
                }
            </style>
            <script type="text/javascript">
                //  var	serverPath ="http://192.168.1.225:9090/PopulationCarev2";
                var productId = localStorage.getItem("productId");
                var loggedInUserId = '';
                const DEF_DELAY = 1000;
                var statsDateButtonId = null;
                var clientId = localStorage.getItem("clientId");
                console.log("Client id local storage", clientId);
                var kitId = localStorage.getItem("kitPermission");
                var SEARCH_PARAM_PAT = "", SEARCH_DATE_PAT = "";
                var ACTIVE_TAB = "NORMAL";

                var fromDate, toDate
                var SEARCH_PARAM = "";
                function sleep(ms) {
                    return new Promise(resolve => setTimeout(resolve, ms || DEF_DELAY));
                }
                $(document)
                    .ready(
                        function () {
                            console.log("INSIDE PAT LIST:",localStorage.getItem("RESET_PASSWORD"))
                            if (localStorage.getItem("userGroupCode") === 'DOC') {
                                pingCallForVC();
                            }

                            if (localStorage.getItem("RESET_PASSWORD") === "true" && localStorage.getItem('DW_JWT_TOKEN') === 0){
                                window.location.href = "logout";
                            }

                            var loggedInUserFullName = localStorage.getItem("loggedInUserFullName");

                            $("#loggedInUserFullName").text(loggedInUserFullName);
                            $("#loggedInUserFullNameSpan").text(loggedInUserFullName);

                            $.ajax({
                                type: "GET",
                                url: serverPath
                                    // + "/getPatientList?productId="+ productId, &kitSerialNumber=localStorage.getItem("kitPermission")
                                    + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId") + '&size=10000',
                                headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
                                success: function (data) {
                                    console.log('---getPatientListByClient?SUCCS --', data.content)

                                    if (data.code == '200') {
                                        var listOfCards = "";
                                        $("#searchedInput").val("");
                                        let { patientList } = data.content;

                                        if (patientList) {
                                            let { contentArray } = patientList;
                                            let patientsLength = patientList.length;
                                            console.log('---getPatientListByClient?contentArray --', patientsLength)
                                            if (patientList && patientsLength > 0) {
                                                $.each(contentArray, function (index, option) {
                                                    var lastName = "";
                                                    var uhid = option.uhid;
                                                    if (option.uhid == null
                                                        || option.uhid == ""
                                                        || option.uhid == "undefined"
                                                        || option.uhid == "null") {
                                                        uhid = " ";
                                                    }
                                                    if (option.bloodGroup == null) {
                                                        option.bloodGroup = "";
                                                    }
                                                    if (option.maritalStatus == null) {
                                                        option.maritalStatus = "";
                                                    }
                                                    if (option.patientLastName != null) {
                                                        lastName = option.patientLastName;
                                                    }
                                                    listOfCards += '<div class="col-md-3 col-sm-6 col-xs-12" style="margin-top:10px;">'
                                                        + '<div class="box box-primary">'
                                                        + '<div class="box-body box-profile" style="height:304px;">'
                                                        + '<img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">'
                                                        + '<h5 style="font-size: 16px;" class="text-center">'
                                                        + option.patientFirstName
                                                        + ' '
                                                        + lastName
                                                        + '</h5>'
                                                        + '<p class="text-muted text-center" style="margin: 0 0 10px;">'
                                                        + uhid
                                                        + '</p>'

                                                        + '<ul class="list-group list-group-unbordered" style="font-size: 12px;">'

                                                        + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                        + '<b>'
                                                        + option.mobileNo
                                                        + '</b>'
                                                        + '</li>'
                                                        + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                        + '<b>'
                                                        + option.bloodGroup
                                                        + ' / '
                                                        + option.maritalStatus
                                                        + ' '
                                                        + '</b>'
                                                        + '</li>'

                                                        + '</ul>'
                                                        + '<a href="#" class="btn btn-primary btn-block" onclick="openDashboard('
                                                        + option.id + ',' + option.userId
                                                        + ');"><b>More</b></a>'
                                                        + '</div>'
                                                        + '</div>'
                                                        + '</div>'
                                                });
                                                $("#patientListCard").html(listOfCards);
                                            } else {
                                                $("#patientListCard").html("<h5  class='text-center'></h5>");
                                            }
                                        } else {
                                            $("#patientListCard").html("<h5  class='text-center'>No Patients found</h5>");
                                        }
                                    } else {
                                        $("#patientListCard").html("<h5  class='text-center'>No Patient Available for today !</h5>");
                                    }

                                },
                                error: function (xhr, status, error) {
                                    if (xhr.status == 0) window.location.href = './login';
                                    if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                                        if (xhr.responseJSON.code === 401) {
                                            (async () => {
                                                toastr.error(xhr.responseJSON.message);
                                                await sleep();
                                                window.location.href = './login';
                                            })()
                                        }
                                        toastr.error(xhr.responseJSON.message);
                                    } else {

                                    }
                                }
                            });
                            initializePatientList();
                        })

                function openDashboard(patientId, patUId) {
                    localStorage.setItem("patientId", patientId);
                    localStorage.setItem("patientUserId", patUId);
                    window.location.href = "dashBoard";
                }
                function openRegistration() {
                    window.location.href = "doctorRegistration";
                }

                function searchForPatient() {

                    /* var servicePath = "http://localhost:9090/Doctor"; */
                    var searchedInput = $("#searchedInput").val();
                    console.log("search: ", searchedInput);
                    $
                        .ajax({
                            type: "GET",
                            // url: serverPath + "/getPatientList?searchParameter="+ searchedInput + "&productId=" + productId,
                            url: serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId") + '&size=10000' + "&searchParameter=" + searchedInput,
                            headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
                            success: function (data) {
                                if (data.code == '200') {
                                    var listOfCards = "";
                                    $("#patientListCard").html("");
                                    $("#searchedInput").val("");
                                    $
                                        .each(
                                            data.content,
                                            function (index, option) {
                                                var uhid = option.uhid;
                                                if (option.episodeId != null) {
                                                    var EpisodeId = option.episodeId
                                                }
                                                if (option.id != null) {
                                                    var ProfileId = option.id;
                                                }
                                                if (option.uhid == null
                                                    || option.uhid == ""
                                                    || option.uhid == "undefined"
                                                    || option.uhid == "null") {
                                                    uhid = " ";
                                                }

                                                if (option.bloodGroup == null) {
                                                    option.bloodGroup = "";
                                                }
                                                if (option.maritalStatus == null) {
                                                    option.maritalStatus = "";
                                                }
                                                console.log("option: ", option);
                                                listOfCards += '<div class="col-md-3 col-sm-6 col-xs-12" style="margin-top:10px;">'
                                                    + '<div class="box box-primary">'
                                                    + '<div class="box-body box-profile" style="height:304px;">'
                                                    + '<img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">'
                                                    + '<h5 style="font-size: 16px;" class="text-center">'
                                                    + option.patientFirstName
                                                    + ' '
                                                    + option.patientLastName
                                                    + '</h5>'
                                                    + '<p class="text-muted text-center" style="margin: 0 0 10px;">'
                                                    + uhid
                                                    + '</p>'

                                                    + '<ul class="list-group list-group-unbordered" style="font-size: 12px;">'

                                                    + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                    + '<b>'
                                                    + option.mobileNo
                                                    + '</b>'
                                                    + '</li>'
                                                    + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                                    + '<b>'
                                                    + option.bloodGroup
                                                    + ' / '
                                                    + option.maritalStatus
                                                    + ' '
                                                    + '</b>'
                                                    + '</li>'

                                                    + '</ul>'
                                                    + '<a href="#" class="btn btn-primary btn-block" onclick="openDashboard('
                                                    + option.id
                                                    + ');"><b>More</b></a>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>'

                                                /*listOfCards += '<div id="" class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="padding:10px;" onclick="openDashboard();">'
                                +'<div style="display:inline-flex;">'
                                    +'<img class="user-image" alt="User Image" src="resources/images/profile.jpg" style="padding-top:3px;max-width:150px;max-height:200px;">'
                                        +'<div id="cardBody" class="col-md-9" style="padding-left: 0px; padding-right: 0px;">'

                                            +'<div class="" style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;height:200px;width:100%;padding-top:10px;">'
                                                +'<div class="col-md-12" style="padding-left:5px;padding-right:0px;">'
                                                    +'<p style="background-color:red;width:100%;"></p>'
                                                    +'<p style="display:inline-flex;">Name : '+option.patientFirstName+' '+option.patientLastName+'</p><br>'
                                                        +'<p style="display:inline-flex;">Blood Group : '+option.bloodGroup+'</p><br>'
                                                            +'<p style="display:inline-flex;">Mobile No : '+option.mobileNo+'</p><br>'
                                                                +'<p style="display:inline-flex;">Gender : '+option.gender+'</p><br>'
                                                                    +'<p style="display:inline-flex;">Marital Status : '+option.maritalStatus+'</p><br>'
                                                                        +'</div>'
                                                                    +'</div>'

                                                                +'</div>'

                                                            +'</div>'
                                                        +'</div>' */
                                            });
                                    $("#patientListCard").html(listOfCards);

                                } else if (data.errorCode == '1009') {

                                    var listOfCards = "";
                                    $("#patientListCard").html("");
                                    toastr.warning('NO DATA');
                                }

                            },
                            error: function (xhr, status, error) {
                                if (xhr.status == 0) window.location.href = './login';
                                if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                                    if (xhr.responseJSON.code === 401) {
                                        (async () => {
                                            toastr.error(xhr.responseJSON.message);
                                            await sleep();
                                            window.location.href = './login';
                                        })()
                                    }
                                    toastr.error(xhr.responseJSON.message);
                                } else {
                                    toastr.error("No Data Found!");
                                }
                            }
                        });

                }

                $(document).ready(function () {
                    $("#patReportBtn").hide();

                    /*
                    if (localStorage.getItem("userGroupCode") === 'NUR')
                        $("#patListLogout").hide();
                    if (localStorage.getItem("roleName") === 'Client Admin'){
                                                    $("#patReportBtn").show();
                    }
                                                */
                    $('#birthDat').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true,//.date
                        endDate: '1d'
                    }).on('changeDate', function (e) {
                        $("#birthDat").attr("readonly", true);
                        $('#birthDat').parent().addClass("is-dirty");
                        $(this).datepicker('hide');
                    });
                    // clearReg();
                });
                let DOWNLOAD_CSV_TOGGLE_STATE = 'TODAY';
                $(document).ready(function () {
                    $(".toggleSwitch .toggleInput").on("change", function (e) {
                        const isOn = e.currentTarget.checked;
                        //console.log('Toggle btn change: ',isOn, DOWNLOAD_CSV_TOGGLE_STATE)

                        if (isOn) {
                            $(".hideme").show();
                            DOWNLOAD_CSV_TOGGLE_STATE = 'CUSTOM';
                        } else {
                            $(".hideme").hide();
                            DOWNLOAD_CSV_TOGGLE_STATE = 'TODAY';
                        }
                    });
                });

                function confPopUp(callerName, roomName, roomId, weconUrl) {
                    var room = encodeURIComponent(roomName);
                    videocalllink = weconUrl + roomId;
                    var win;
                    var r = confirm(callerName + " is calling you. Do you accept the call?");
                    //	alert(r);
                    if (r == true) {
                        win = window.open(videocalllink, 'result', 'width=300,height=300');

                        /* win.addEventListener("resize", function(){
                                                    win.resizeTo(1024, 768);
                        }); */
                    }
                    $.ajax({
                        type: "POST",
                        url: serverPath + "/intiateAndCloseCallv4?initiate=false&roomId="
                            + roomId,
                        success: function (data) {
                            //win.close();
                        }
                    });
                }

                function initiateCall(calleProfileId) {
                    var proId = localStorage.getItem("productId");
                    if (proId == 1310) {
                        alert("This Service is not enabled, Please contact Wenzins");
                    } else {
                        /* var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
                        win.addEventListener("resize", function(){
                                win.resizeTo(1024, 768);
                        }); */
                        $.ajax({
                            type: "POST",
                            url: serverPath + "/intiateAndCloseCallv4?profileId="
                                + localStorage.getItem("profileId")
                                + "&calleProfileId=" + calleProfileId + "&serverKeyId="
                                + localStorage.getItem("serverKeyId") + "&initiate="
                                + true,
                            success: function (data) {
                                //	console.log(data);
                                roomid = data.roomid;
                                //	alert(data);
                                //	console.log("new  "+data.content.weconurl);

                                videocalllink = data.content.weconurl + data.roomid;

                                /* 	 $.each(data.content,function(index, option) {
                                         console.log(option);
                                         var url = option.weconurl;
                                    //	 alert(url);
                                         console.log("link"+url);
                                         var win = window.open(url);
                                          videocalllink =  url + roomid ;
                                //	var win = window.open(videocalllink , 'result', 'width=1024,height=768');
                                }); */
                                //	 window.open(a);
                                var win = window.open(videocalllink, 'result',
                                    'width=300,height=300');

                                win.addEventListener("resize", function () {
                                    win.resizeTo(1024, 768);
                                });
                            }
                        });
                    }
                }

                function logout() {
                    localStorage.removeItem("DW_JWT_TOKEN");
                    localStorage.removeItem("roleName");
                    localStorage.removeItem("clientId");
                    localStorage.removeItem("userGroupCode");
                    localStorage.removeItem("loggedInUserFullName");
                    localStorage.removeItem("kitPermission");
                    localStorage.removeItem("patientId");
                    localStorage.removeItem("patientUserId");
                    localStorage.removeItem("RESET_PASSWORD");
                    window.location.href = "logout";
                }

                $(function () {
                    $("#datepicker1").datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true,
                        todayHighlight: true,
                        endDate: "today"
                    }).datepicker('update', new Date());
                });

                $(function () {
                    $("#datepicker2").datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true,
                        todayHighlight: true,
                        endDate: "today"
                    }).datepicker('update', new Date());
                });

                function downloadPatientCsv() {

                    console.log('downloadPatientCsv->', $('#datepicker1Val').val(), ', ', $('#datepicker2Val').val(), DOWNLOAD_CSV_TOGGLE_STATE)

                    var profileID = localStorage.getItem("profileId");

                    var serverKeyId2 = localStorage.getItem("serverKeyId");



                    if (DOWNLOAD_CSV_TOGGLE_STATE === 'TODAY') {

                        fromDate = new Date();

                        toDate = new Date();

                        fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(), 0, 0, 0, fromDate.getMilliseconds());

                        toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(), 23, 59, 59, toDate.getMilliseconds());

                        // console.log('---TODAY: ', fromDate, ', ', toDate);

                    } else {



                        var values = $('#datepicker1Val').val().split("/");

                        fromDate = new Date(values[2], (values[1] - 1), values[0]);

                        var values2 = $('#datepicker2Val').val().split("/");

                        toDate = new Date(values2[2], (values2[1] - 1), values2[0]);



                        // console.log('---CUSTOM: ', fromDate, ', ', toDate);



                        var today = new Date().setHours("23", "59", 59, 0);

                        if (toDate == null || toDate == 'Invalid Date' || fromDate == null || fromDate == 'Invalid Date') {

                            toastr.error('Selected date is null');

                            return;

                        }



                        if (toDate.getTime() > today || fromDate.getTime() > today) {

                            toastr.error('Date Cannot Be Greater Than Todays Date');

                            return;

                        }

                        if (toDate.getTime() < fromDate.getTime()) {

                            toastr.error('To Date cannot be Lesser Than From Date');

                            return;

                        }

                        fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),

                            0, 0, 0, fromDate.getMilliseconds());

                        toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),

                            23, 59, 59, toDate.getMilliseconds());

                        //fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),0, 0, 0, fromDate.getMilliseconds());

                        //toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),23, 59, 59, toDate.getMilliseconds());

                    }



                    // console.log("----DATE-----", fromDate, toDate)

                    //var fromDate, toDate, today = new Date().setHours("23", "59", 59, 0);

                    fromDate = new Date(fromDate).getTime();

                    toDate = new Date(toDate).getTime();



                    console.log("downloadPatientCsv-> ", "From: ", fromDate, "To: ", toDate);

                    //return;

                    var newURL = serverPath + "/getPatientReportByClientId?clientId=" + localStorage.getItem("clientId") + "&startEpoch=" + fromDate + "&endEpoch=" + toDate;

                    //console.log(newURL);

                    // return newURL;

                    window.location.href = newURL;

                };
                function resetReportDatePicker() {
                    $("#datepicker1").datepicker('update', new Date());
                    $("#datepicker2").datepicker('update', new Date());

                };


            </script>


        </head>

        <body class="hold-transition skin-blue sidebar-mini">
            <div>

                <header class="main-header">
                    <nav class="navbar navbar-static-top" role="navigation"
                        style="margin-left: 0% !important; max-height: 50px; background-color: #3c8dbc">
                        <!-- Sidebar toggle button-->
                        <img src="resources/images/WriztoLogoBox.jpg" class="user-image"
                            onclick="window.location.href = 'home';" alt="User Image"
                            style="cursor: pointer;height: 50px; padding-left: 5px; margin-left: 5px;"> <span
                            class="navbar-t-centered">Wrizto Healthcare System </span>


                        <!-- <button type="button" class="btn btn-default.button4 {padding: 32px 16px;} px-3"><i class="fas fa-balance-scale" aria-hidden="true"></i></button>
	 -->


                        <div class="navbar-custom-menu">

                            <ul class="nav navbar-nav">
                                <!--  <li style="top: 8px;"><label class="switch"><input id="someSwitchOptionInfo" type="checkbox" checked> <span class="slider round"></span></label></li>
             -->
                                <!-- Messages: style can be found in dropdown.less-->

                                <!-- Patient Report csv download -->
                                <li>
                                    <!-- Trigger the modal with a button -->

                                    <button type="button" class="btn btn-lg" style="background-color: transparent;"
                                        id="patReportBtn" data-toggle="modal" data-target="#myModal">
                                        <img src="resources/images/file2.png"
                                            style="width: 25px; height: 25px; margin-top: 3px;" alt="FIle Icon" />
                                    </button> <!-- Modal -->
                                    <div id="myModal" class="modal fade" role="dialog" data-backdrop="false">
                                        <div class="modal-dialog modal-sm">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header" style="border-bottom: 3px solid #3c8dbc;">
                                                    <button type="button" class="close"
                                                        onClick="resetReportDatePicker()"
                                                        data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Report</h4>
                                                </div>
                                                <div class="modal-body">


                                                    <label class="toggleSwitch"> <input type="checkbox" id="togBtn"
                                                            class="toggleInput">
                                                        <div class="dateSlider round">
                                                            <!--ADDED HTML -->
                                                            <span class="on">Custom</span> <span
                                                                class="off">Today</span>
                                                            <!--END-->
                                                        </div>
                                                    </label>

                                                    <div class="hideme" style="display:none;">
                                                        <label>Start : </label>
                                                        <div id="datepicker1" class="input-group date"
                                                            data-date-format="dd/mm/yyyy">
                                                            <input id="datepicker1Val" class="form-control" type="text"
                                                                readonly /> <span class="input-group-addon"><i
                                                                    class="glyphicon glyphicon-calendar"></i></span>
                                                        </div>

                                                        <label>End : </label>
                                                        <div id="datepicker2" class="input-group date"
                                                            data-date-format="dd/mm/yyyy">
                                                            <input id="datepicker2Val" class="form-control" type="text"
                                                                readonly /> <span class="input-group-addon"><i
                                                                    class="glyphicon glyphicon-calendar"></i></span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" style="background-color:#3c8dbc; color:white"
                                                        class="btn btn-default"
                                                        onClick="window.location.href =downloadPatientCsv();">Download</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>



                                <!-- User menu dropdown -->
                                <li class="dropdown user user-menu"><a href="#" class="dropdown-toggle"
                                        data-toggle="dropdown"> <img src="resources/images/avatar.png"
                                            class="user-image" alt="User Image"> <span class="hidden-xs"
                                            id="loggedInUserFullName"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <!-- User image -->

                                        <li class="user-header"><img src="resources/images/avatar.png"
                                                class="img-circle" alt="User Image">
                                            <p>
                                                <span id="loggedInUserFullNameSpan"></span>
                                            </p>
                                        </li>
                                        <!-- Menu Body -->
                                        <li class="user-body"
                                            style="padding-top: 0px; padding-right: 0px; padding-left: 0px;">
                                            <!-- <div class="col-xs-7 text-center"><span style="font-size: 12px; margin-left: -33px;" id="nurseNameForActiveId"></span>
															<a id="videoIconId" style="border-radius: 50%; ;margin-left: 5px;background-color: green" class="btn btn-primary" title="videoCall" onclick="initiateCall();"><b>
																<i style="color:white;font-size:12px" class="fa fa-video-camera" id="showHideVideoIconId"></i></b></a>
														</div>
														<div class="col-xs-5 text-center">
															<a href="patientList">Patient List</a>
														</div> activeUsersId  activeUsersId-->

                                            <ul class="col-xs-12 text-center" id="activeUsersId">
                                                <!-- <li class="form-control" style="border-radius:6px;" ><span style="font-size: 12px;text-transform: capitalize;margin-left: -33px;" id="nurseNameForActiveId">Wrizto Nurse</span>
																<a id="videoIconId" style="border-radius: 50%;margin-left: 5px;float:right;background-color: green" class="btn btn-primary" title="videoCall" onclick="initiateCall(1358)"><b>
																	<i style="color:white;font-size:12px" class="fa fa-video-camera" id="showHideVideoIconId"></i></b></a></li>
                 				 -->

                                            </ul> <br>
                                            <ul>
                                                <br>
                                                <!--  <span style="top: 4px;"> <button style="padding: 10px 10px; type=" button" class="btn btn-success"><i class="fa fa-video-camera fa-lg" aria-hidden="true"></i>&nbsp;<span><b>Wecon </b></span></button>
																	<button style="padding: 10px 10px; type=" button" class="btn btn-success "><i class="fa fa-video-camera fa-lg" aria-hidden="true"></i>&nbsp;<span><b>Mscp</b></span></button>
															</span> -->

                                            </ul>
                                        </li>
                                        <!-- Menu Footer-->
                                        <li class="user-footer">
                                            <div class="pull-left"></div>
                                            <div class="pull-left" id='patListLogout'>
                                                <a onclick="logout();" class="btn btn-default btn-flat">Sign out</a>
                                            </div>
                                            <div class="pull-right"></div>
                                            <div class="pull-right" id='patListReset'>
                                                <a onclick="resetPassword()" class="btn btn-default btn-flat">Change Password</a>
                                            </div>
                                        </li>


                                    </ul>
                                </li>
                                <!-- Control Sidebar Toggle Button -->

                            </ul>
                        </div>
                    </nav>
                </header>
                <!-- Left side column. contains the logo and sidebar -->



                <div class="col-md-12"
                    style="background-color: white !important; width: 100%; padding-bottom: 10px;  display: inline-flex;">
                    <div class="input-group" style="margin-top: 10px;">
                        <input type="text" id="searchedInput" class="form-control" placeholder="Search By Name/UHID"> <span
                            class="input-group-btn">
                            <button class="btn btn-search" type="button"
                                style="background-color: #3c8dbc; color: white;" onclick="searchInput();">
                                <i class="fa fa-search fa-fw"></i> Search
                            </button>
                        </span>
                    </div>

                    <div class=""></div>

                    <!-- <label>Date of Birth:<i style="color: red">*</i>
							</label> -->
                    <div class="input-group" style="margin-top: 10px; padding-left: 40px;">
                        <div class="input-group-addon" style="background-color: #3c8dbc">
                            <i style="color: white" class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control clearAll" id="birthDat" name="dateOfBirth" autocomplete="off" placeholder="Date"
                            data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        </input> <span class="input-group-btn">
                            <button class="btn btn-search" type="button"
                                style="background-color: #3c8dbc; color: white;" onclick="searchForDate();">
                                <i class="fa fa-search fa-fw"></i> Search
                            </button>
                        </span>
                    </div>
                </div>

                <div class="col-md-12" id="patientListCard" style="padding-bottom: 60px;"></div>
                <!-- Content Wrapper. Contains page content -->



                <div class="content-wrapper" style="margin-left: 0px !important;">

                    <!-- Main content -->
                    <section class="content">

                        <div class="row">

                            <div class="col-md-12">
                                <div class="nav-tabs-custom" style="min-height:700px !important;">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a class="highLId" id="patConId" href="#history"
                                                onclick="initializePatientList()" data-toggle="tab">Normal</a></li>
                                        <li class="" onclick="initializePatientListAbnormal();"><a class="highLId"
                                                id="docConId" href="#myList" data-toggle="tab">Abnormal</a></li>
                                    </ul>
                                    <div class="tab-content" style="overflow-y:auto;min-height:500px;">
                                        <div class="active tab-pane" id="history">
                                            <!-- <div class="box box-default"> -->

                                            <div class="box-body">

                                                <div>
                                                    <div>
                                                      <label for="rows-per-page">Rows per Page:</label>
                                                      <select id="rows-per-page">
                                                       <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                <table class="table table-bordered table-striped" id="orderNormalTable">
                                                    <thead>
                                                        <tr style="height: 33px !important;">

                                                            <th style="text-align:left;width:100px" scope="col">Patient Name</th>
                                                            <th style="text-align:left;width:100px;"scope="col">UHID</th>
                                                            <th style="text-align:left;width:100px;"scope="col">Visit Date</th>
                                                            <th style="text-align:left;width:50px"scope="col">Age/Gender</th>
                                                            <th style="text-align:left;width:100px;"scope="col">Blood Group</th>

                                                            <th style="text-align:left;width:100px;"scope="col">City/Village</th>
                                                            <th style="text-align:left;width:50px;"scope="col">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="orderNormalPatDetails">
                                                    </tbody>
                                                </table>

                                                <div>
                                                    <div>
                                                        <p id="data-count"></p>

                                                      <p id="total-count"></p>
                                                    </div>
                                                    <div class="rows-per-page-container">
                                                        <div id="pagination-container"></div>
                                                    </div>
                                                  </div>
                                                    <div class="col-md-12 text-center" id="norrmalPatNoList"
                                                    style="display:none;padding-left: 0px;padding-right: 0px;">

                                                    <p style="padding-top: 10px;padding-bottom: 10px;border: 1px solid #f4f4f4;">
                                                    No Patients
                                                    </p>
                                                </div>
                                                <div id="normal-patient-table-spinner"
                                                style="position: fixed; left: 50%; top: 40%; width: 120px; height: 100%; z-index: 9999;display:none">
                                                <img style="" src="resources/images/icons/loading.gif"
                                                    alt="loading" />
                                                <br />
                                            </div>


                                                <div class="col-md-12 text-center" id="orderNormalNoList"
                                                    style="display:none;padding-left: 0px;padding-right: 0px;">
                                                    <p
                                                        style="padding-top: 10px;padding-bottom: 10px;border: 1px solid #f4f4f4;">
                                                        No Patients</p>
                                                </div>
                                            </div><!-- /.box-body -->
                                        </div><!-- /.tab-pane -->


                                        <!--   <div class="modal tab-content "
														role="dialog" style="padding:15px;margin-top: 50px;z-index: 1025;"> -->
                                        <div class="tab-pane" id="myList">
                                            <div class="box-body">

                                                <div>
                                                    <div>
                                                      <label for="rows-per-page-abnormal">Rows per Page:</label>
                                                      <select id="rows-per-page-abnormal">
                                                        <option value="10">10</option>
                                                         <option value="25">25</option>
                                                          <option value="50">50</option>
                                                          <option value="100">100</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                <table id="abnormalpatList" class="table table-bordered table-striped verticle-middle table-responsive-sm">
                                                    <thead>

                                                        <tr style="height: 33px !important;">

                                                            <th style="text-align:left;width:100px" scope="col">Patient Name</th>
                                                            <th style="text-align:left;width:100px;"scope="col">UHID</th>
                                                            <th style="text-align:left;width:100px;"scope="col">Visit Date</th>
                                                            <th style="text-align:left;width:50px"scope="col">Age/Gender</th>
                                                            <th style="text-align:left;width:100px;"scope="col">Blood Group</th>

                                                            <th style="text-align:left;width:100px;"scope="col">City/Village</th>
                                                            <th style="text-align:left;width:50px;"scope="col">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="abnormalPatientlistTable">

                                                    </tbody>
                                                </table>

                                                <div>
                                                    <div>
                                                        <p id="data-count-abnormal"></p>

                                                      <p id="total-count-abnormal"></p>
                                                    </div>
                                                    <div class="rows-per-page-container">
                                                        <div id="pagination-container-abnormal"></div>
                                                    </div>
                                                  </div>
                                                 <div class="col-md-12 text-center" id="patientsorderNoList"
                                                 style="display:none;padding-left: 0px;padding-right: 0px;">

                                                 <p style="padding-top: 10px;padding-bottom: 10px;border: 1px solid #f4f4f4;">
                                                  No Patients
                                                   </p>
                                                   </div>
                                                <div id="abNormal-Patient-table-spinner"
                                                    style="position: fixed; left: 50%; top: 40%; width: 120px; height: 100%; z-index: 9999;display:none">
                                                    <img style="" src="resources/images/icons/loading.gif"
                                                        alt="loading" />
                                                    <br />
                                                </div>

                                            </div><!-- /.box-body -->
                                        </div>
                                    </div>
                                    <!-- </div> --><!-- /.tab-content -->
                                </div><!-- /.nav-tabs-custom -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->

                    </section><!-- /.content -->
                </div><!-- /.content-wrapper -->




<!----RESET PASSWORD MODAL---->
<div class="modal fade" id="reset-password-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Reset Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="left: 50px;margin-top: -25px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="resetPaswd-modal" autocomplete="off">
                    <div class="card-body" id="changePaswdUser">

<div class="col-xs-12" style="padding-left: 10px; padding-right: 0px;">
    <div style="width: 100%; color: black" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input type="password" id="inviteNewPasswordChk" placeholder="New Password" class="form-control" maxlength="64">
        <span style="color:red" class="required">min 8 characters and (A-Z, a-z, 0-9, any special characters)</span>
        <span id="ShowHideNewPassword" class="glyphicon glyphicon-eye-close pull-right"
            style="margin-top: -6%; cursor: pointer; margin-right: 2%; color: black; font-size: 15px;"
            onclick="ShowHidePassword('inviteNewPasswordChk', 'ShowHideNewPassword');"></span>
    </div>
</div>

<div class="col-xs-12" style="padding-right: 0px;padding-left: 10px;">
    <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 100%; color: black">
            <input type="password" id="inviteConfirmPasswordChk" placeholder="Confirm Password" class="form-control"  maxlength="64">
            <span id="ShowHideConfirmPassword" class="glyphicon glyphicon-eye-close pull-right"
                style="margin-top: -6%; cursor: pointer; margin-right: 2%; color: black; font-size: 15px;"
                onclick="ShowHidePassword('inviteConfirmPasswordChk', 'ShowHideConfirmPassword');"></span>
        </div>
    </div>
</div>

</div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button id="resetPaswdUserBtn" onclick="resetPasswordChk();" type="button" class="btn btn-primary">
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"
                            style="display:none; text-align: center;" id="resetPaswdSpinner"></span>
                            Reset
                        </button>

                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-----------------ALERT MODAL-------------------->

<div class="modal fade" id="reset-password-alert-modal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>

            </div>
            <div class="modal-body">
                <form action="#" id="resetPaswd-alert-modal" autocomplete="off">
                    <div class="card-body" id="changePaswdUser">

                        <h4>Password Reset Completed, Please login again by entering new password</h4>


                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button id="resetPaswdOkBtn"  type="button" class="btn btn-success">
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"
                            style="display:none; text-align: center;" id="resetPaswdSpinner"></span>
                            OK
                        </button>

                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

                <footer class="main-footer"
                    style="margin-left: 0% !important; position: fixed; bottom: 0px; margin-right: auto; margin-left: auto; width: 100%;">
                    <!--  <div class="pull-right hidden-xs">
								<b>Version</b> 2.3.0
							</div>
							<strong>Copyright &copy; 2018-2019 <a href="#">Wrizto Technologies Ind Pvt Ltd.</a>.</strong> All rights reserved. -->
                    <div class="pull-right hidden-xs">
                        <b>Version</b> 1.0.0
                    </div>
                    Copyright &copy; 2021-2022 <a href="http://www.wrizto.com/" target=_blank
                        style="color: rgb(60, 141, 188);">Wrizto
                        Healthcare Pvt. Ltd.</a> All rights reserved.
                </footer>

                <!-- Control Sidebar -->

                <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
                <div class="control-sidebar-bg"></div>
            </div>
            <!-- ./wrapper -->

            <script type="text/javascript">

                function resetPassword(){
                    $("#reset-password-modal").modal('show')
                }

<!---RESET PASSWORD--->

var DW_JWT_TOKEN = localStorage.getItem('DW_JWT_TOKEN');
        var loggedInUserFullName = localStorage.getItem("loggedInUserFullName");
        var USER_NAME =  localStorage.getItem('loginUsername');
        var ROLL_NAME = localStorage.getItem("roleName");
        var RESET_PASSWORD = localStorage.getItem("RESET_PASSWORD");
        //RESET PASSWORD

function resetPasswordChk() {
             toastr.options.timeOut = 6000;
            let NEW_PASSWORD = $("#inviteNewPasswordChk").val();
            let CONFIRM_PASSWORD = $("#inviteConfirmPasswordChk").val();
var hasNumber = /\d/.test(NEW_PASSWORD);
    var hasUpperCase = /[A-Z]/.test(NEW_PASSWORD);
    var hasLowerCase = /[a-z]/.test(NEW_PASSWORD);
    var hasSpecialChar = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/.test(NEW_PASSWORD);
    var hasValidLength = NEW_PASSWORD.length >= 8;

var jsonData = JSON.stringify({
    userName: USER_NAME,
	newPassword: NEW_PASSWORD,
    confirmNewPassword: CONFIRM_PASSWORD,
    resetPassword: false
	});


 console.log("reset password")
    console.log("reset password json",jsonData)

    if($("#inviteNewPasswordChk").val() == ''){
        toastr.error("New Password field is Empty!");
        return;
    }else if(!hasNumber){
             toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
             return;
         }else if(!hasUpperCase){
          toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
          return;
         }else if(!hasLowerCase){
          toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
          return;
          }else if(!hasSpecialChar){
          toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
          return;
          }
          else if(!hasValidLength){
          toastr.error("Password must contain atleast 8 characters!");
          return;
          }
    if($("#inviteConfirmPasswordChk").val() == ''){
        toastr.error("Confirm Password field is Empty!")
        return;
    }




//$('#resetPasswdSpinner').show();
    //API CALL

    $.ajax({
        type:'POST',
        contentType: "application/json",
        url: serverPath + "/resetAdminPassword",
        data:jsonData,
        headers: { "Authorization": 'Bearer ' + DW_JWT_TOKEN },
        success: function(result) {


            let data = result.error;
            console.log('success: ',data);

            if(result.error == false){
                console.log("success block",ROLL_NAME, RESET_PASSWORD)
                $('#reset-password-alert-modal').modal('show')

                $('#resetPaswdOkBtn').click(function() {
                    // Redirect to the login page
                    if(ROLL_NAME == 'Doctor'){

                    window.location.href = "logout";

                    }else if(ROLL_NAME == 'Client Admin'){

                    window.location.href = "logout";

                    }else if(ROLL_NAME == 'Nurse'){

                    window.location.href = "logout";
                }
$('#felix-widget-button-btn-resetPaswd').prop('disabled', false);
                  });
                   $('#resetPaswdCancelBtn').click(function() {
                    // Redirect to the login page
                    var url = "./resetPassword ";
                    window.location.href= url;
                    $('#felix-widget-button-btn-resetPaswd').prop('disabled', false);
                  });
                //alert("Password Reset Completed, Please login again ")


            }else if(result.error == true){
            //$('#resetPasswdSpinner').hide();
                console.log("error block")
                toastr.error(result.message);
                var url = "./login ";
                window.location.href= url;
            }




            //console.log("Appointments and packages",appointments,packages)


        },
        error: function (xhr, status, error) {
            console.log("change password-->3 : ",error);

            $('#felix-widget-button-btn-resetPaswd').prop('disabled', false);
            //$('#resetPasswdSpinner').hide();
            if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                if (xhr.responseJSON.code !== 200) {
                    (async () => {
                        toastr.error(xhr.responseJSON.message);

                    })()
                } else {
                    toastr.error(xhr.responseJSON.message);
                }
            } else {
                toastr.error('Failed add user.Internet Disconnected/Server error');
            }

        }
    });

}

$(function () {
    $("#reset-password-modal").on("hidden.bs.modal", function (e) {
        console.log("Modal hidden");
        $("#resetPaswd-modal").trigger("reset");

        // Reset the eye icon to the closed state
        $("#ShowHideNewPassword").removeClass("glyphicon-eye-open").addClass("glyphicon-eye-close");
        $("#ShowHideConfirmPassword").removeClass("glyphicon-eye-open").addClass("glyphicon-eye-close");

        // Reset password input fields to type "password"
        $("#inviteNewPasswordChk").attr("type", "password");
        $("#inviteConfirmPasswordChk").attr("type", "password");
    });
});
//HIDE AND SHOW PASSWORD EYE ICON
function ShowHidePassword(inputId, iconId) {
        var inputElement = $('#' + inputId);
        var iconElement = $('#' + iconId);

        if (inputElement.attr('type') === 'password') {
            inputElement.attr('type', 'text');
            iconElement.removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
        } else {
            inputElement.attr('type', 'password');
            iconElement.removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
        }
    }
$("#inviteConfirmPasswordChk").keyup(function (event) {
                if (event.keyCode === 13) {
                    $("#felix-widget-button-btn-resetPaswd").click();
                }
            });

// VIEW NORMAL PATIENT LIST
let currentPageIndex = 0;
let normalPatientListQJson = { pagination: {} };

function initializePatientList() {
 // Reset the current page index to 0
 currentPageIndex = 0;
  normalPatientListQJson.pagination.currentPage = currentPageIndex;
  console.log("INSIDE initializePatientList()", currentPageIndex);
  // Call the main function to fetch and display the patient list
  viewNormalPatientList(currentPageIndex);
}

function viewNormalPatientList(page) {
  console.log("INSIDE viewNormalPatientList()",page)
const rowsPerPageSelect = document.getElementById('rows-per-page');
const selectedValue = parseInt(rowsPerPageSelect.value);
currentPageIndex = page
  $("#normal-patient-table-spinner").show();
  $('#normal-patient-table-spinner').css({ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '200px' });
  ACTIVE_TAB = "NORMAL";


  $("#orderNormalPatDetails").show();
  $("#orderNormalPatDetails").html('');

  let clientId = localStorage.getItem('clientId');
  let NormalPatientURL = serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId") + "&size=" + selectedValue + "&page=" + page +"&filterByAbnormal=0" + "&sortDirection=DESC";
  if (kitId) {
    NormalPatientURL += "&kitSerialNumber=" + kitId;
  }
  if (SEARCH_PARAM_PAT !== "") {
    NormalPatientURL += "&searchParameter=" + SEARCH_PARAM_PAT;
  }
  console.log("SEARCH_DATE_PAT-> ", SEARCH_DATE_PAT);
  if (SEARCH_DATE_PAT !== "") {
    var values = SEARCH_DATE_PAT.split("/");
    console.log("values", SEARCH_DATE_PAT);
    var fromDate = new Date(values[2], (values[1] - 1), values[0]);
    fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),
      0, 0, 0, fromDate.getMilliseconds());
    console.log("from date", fromDate);
    var toDate = new Date(values[2], (values[1] - 1), values[0]);
    toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),
      23, 59, 59, toDate.getMilliseconds());
    console.log("todate", toDate);

    NormalPatientURL += "&startDate=" + fromDate + "&endDate=" + toDate;
  }

  $.ajax({
    type: "GET",
    url: NormalPatientURL,
    headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
    success: function (data) {
      $("#normal-patient-table-spinner").hide();

      if (data.code == '200') {
        if (data.content && data.content.patientList) {
          console.log("DATA OF SUCCESS BLOCK");

          const receivedData = data.content.patientList;
          // Update the normalPatientListQJson object with the received data
          normalPatientListQJson.patientList = receivedData;
          normalPatientListQJson.pagination = data.content.pagination;


        }
      }
      console.log("Normal patients list", normalPatientListQJson.pagination);

      $("#orderNormalNoList").hide();
      $("#orderNormalTable").show();


      normalPatientListQJson.pagination.pageLimit = selectedValue;
      normalPatientListQJson.pagination.totalPages = Math.ceil(normalPatientListQJson.pagination.count / selectedValue);

      // Reload the current page to reflect the new rows per page
      console.log("--------------------------------->",normalPatientListQJson)
      generatePaginationButtons(normalPatientListQJson.pagination.totalPages, page);
      viewNormalPatientListPage(page, normalPatientListQJson);
      console.log("PAGINATION CURRENT PAGE", currentPageIndex);
    }
  });
}

// Event listener for rows per page change
const rowsPerPageSelect = document.getElementById('rows-per-page');
rowsPerPageSelect.addEventListener('change', handleRowsPerPageChange);

function handleRowsPerPageChange() {
  // Reset the current page index to 0
  currentPageIndex = 0;
  // Call the main function to update the patient list with the new rows per page value
  viewNormalPatientList(currentPageIndex);
}

function viewNormalPatientListPage(page, item) {
  console.log("INSIDE WHEN NEXT BUTTON CLICKED viewNormalPatientListPage------->",page,item);
  // Update the current page in the response object
  currentPageIndex = page;
  // Check if item is defined and has the necessary properties
  if (!item || !item.patientList || !item.pagination) {
    console.error('Invalid item data');
    return;
  }
  // Calculate the start and end index for the current page
  const startIndex = page * item.pagination.pageLimit;
  const endIndex = Math.min(startIndex + item.pagination.pageLimit, item.pagination.count);


  // Clear the existing data list
  const dataList = document.getElementById('orderNormalPatDetails');
  dataList.innerHTML = '';

  // Generate data table HTML for the current page
  if (item.patientList && item.patientList.length > 0) {
    let Patient_List = [];
    for (Patient_List of item.patientList) {

      //console.log('Patient List:', Patient_List);

      var user = Patient_List;


 // DISPLAY PATIENT NAME
let patName = "";
if (user && user.patientFullName && user.patientFullName !== "") {
  patName = user.patientFullName;
} else {
  var fname = user && user.patientFirstName && user.patientFirstName !== "" ? user.patientFirstName : "";
  var lastname = user && user.patientLastName && user.patientLastName !== "" ? user.patientLastName : "";
  patName = fname + " " + lastname;
}

// CREATED DATE
let createdDate = "";
if (user && user.createdAt) {
  var m = new Date(user.createdAt);
  createdDate = m.getUTCDate() + "/" + (m.getUTCMonth() + 1) + "/" + m.getUTCFullYear();
}

     // VILLAGE CITY
let villageCity = "";
if (user && user.city) villageCity = user.city + "/";
if (user && user.village) villageCity += user.village;


      // BLOOD GROUP

let bloodGrp = user && user.bloodGroup ? user.bloodGroup : '-';

let ageGender = "";
if (user && user.age) ageGender = user.age + "/";
if (user && user.gender) ageGender += user.gender;
    // UHID
let uhid = user && user.uhid ? user.uhid : '-';


      // Create a new row for each patient
      const row = document.createElement('tr');

      // Create table cells and set their content
      const nameCell = document.createElement('td');
      nameCell.textContent = patName;

      const uhidCell = document.createElement('td');
      uhidCell.textContent = uhid;

      const visitDateCell = document.createElement('td');
      visitDateCell.textContent = createdDate ? createdDate : '-';

      const ageGenderCell = document.createElement('td');
      ageGenderCell.textContent = ageGender;

      const bloodGroupCell = document.createElement('td');
      bloodGroupCell.textContent = bloodGrp;

      const cityVillageCell = document.createElement('td');
      cityVillageCell.textContent = villageCity;

      const actionCell = document.createElement('td');
    const button = createViewButton(user.id, user.userId);


      // Add the cells to the row
      actionCell.appendChild(button);
      row.appendChild(nameCell);
      row.appendChild(uhidCell);
      row.appendChild(visitDateCell);
      row.appendChild(ageGenderCell);
      row.appendChild(bloodGroupCell);
      row.appendChild(cityVillageCell);
      row.appendChild(actionCell);

      // Add the row to the data list
      dataList.appendChild(row);

}
  }else {
       // If syncTransactions is empty, display "No data available" message
       const row = document.createElement('tr');
       const noDataCell = document.createElement('td');
       noDataCell.textContent = 'No data available';
       noDataCell.colSpan = 5; // Set the colspan to the number of columns in the table
       row.appendChild(noDataCell);
       dataList.appendChild(row);
     }
  // Update the data count
    const start = startIndex + 1;
    const end = Math.min(startIndex + item.pagination.pageLimit, item.pagination.count);
    console.log("TOTAL ENTRIES", start, end, item.pagination.count);

    const dataCountElement = document.getElementById('data-count');
    dataCountElement.innerText = "Showing " + start + " to " + end + " of " + item.pagination.count + " entries";
  // Generate pagination buttons
  //generatePaginationButtons(item.pagination.totalPages, page);
}

function generatePaginationButtons(totalPages, currentPage) {
  const paginationContainer = document.getElementById('pagination-container');
  paginationContainer.innerHTML = '';

  const maxPageButtons = 6; // Maximum number of page buttons to display
  let startPage = Math.max(currentPage - 1, 0);
  let endPage = Math.min(startPage + maxPageButtons, totalPages - 1);
  startPage = Math.max(endPage - maxPageButtons + 1, 0);

  // Create the previous button
  const previousButton = document.createElement('button');
previousButton.textContent = 'Previous';
previousButton.classList.add('primary-button');
previousButton.style.backgroundColor = '#007bff';
previousButton.style.color = '#fff';
  if (currentPage === 0) {
    previousButton.disabled = true;
  }
  previousButton.onclick = function () {

    currentPage = Math.max(currentPage - 1, 0);
    console.log("previous button clicked",currentPage)
    normalPatientListQJson.pagination.currentPage = currentPage;
    viewNormalPatientList(currentPage)
    //viewNormalPatientListPage(currentPage - 1, normalPatientListQJson);
  };
  paginationContainer.appendChild(previousButton);

  // Create the page number buttons
  if (startPage > 0) {
    const firstPageButton = document.createElement('button');
    firstPageButton.textContent = '1';
    firstPageButton.classList.add('pagination-button');
    if (currentPage === 0) {
            firstPageButton.disabled = true;
       }
    firstPageButton.onclick = function () {

      currentPage = 0;
      console.log("First page number button clicked",currentPage)
      normalPatientListQJson.pagination.currentPage = currentPage;
      viewNormalPatientList(currentPage)
      //viewNormalPatientListPage(0, normalPatientListQJson);
    };
    paginationContainer.appendChild(firstPageButton);
  }
  for (let i = startPage; i <= endPage; i++) {
    const button = document.createElement('button');
    button.textContent = i + 1;
    button.classList.add('pagination-button');
    if (i === currentPage) {
      button.disabled = true;
    }
    button.onclick = function () {

      currentPage = i;
      console.log("page number button clicked",currentPage)
      normalPatientListQJson.pagination.totalPages = currentPage;
      viewNormalPatientList(currentPage)
    };
    paginationContainer.appendChild(button);
  }

  // Create the next button
  const nextButton = document.createElement('button');
nextButton.textContent = 'Next';
nextButton.classList.add('primary-button');
nextButton.style.backgroundColor = '#007bff';
nextButton.style.color = '#fff';
  if (currentPage === totalPages - 1) {
    nextButton.disabled = true;
  }
  nextButton.onclick = function () {
    currentPage++;
    if (currentPage >= normalPatientListQJson.pagination.totalPages) {
        // If it exceeds, set it to the last page

        currentPage = normalPatientListQJson.pagination.totalPages - 1;
    }
    viewNormalPatientList(currentPage);
    console.log("next button clicked",currentPage)
  };
  paginationContainer.appendChild(nextButton);
}

//VIEW BUTTON IN TABLE

function createViewButton(patientId, patUId) {
  const button = document.createElement('button');
  button.textContent = 'View';
  button.classList.add('primary-button');
  button.style.backgroundColor = '#007bff';
  button.style.color = '#fff';
  button.style.border = 'none';
  button.style.padding = '8px 16px';
  button.style.borderRadius = '4px';
  button.style.cursor = 'pointer';
  button.style.transition = 'background-color 0.3s ease';

  // Add hover effect
  button.addEventListener('mouseover', function() {
    button.style.backgroundColor = '#0056b3';
  });

  button.addEventListener('mouseout', function() {
    button.style.backgroundColor = '#007bff';
  });

  button.onclick = function () {
    openDashboard(patientId, patUId);
  };

  return button;
}

// Function to increment the CURRENT_PAGE_INDEX
var i = 0;
function incrementPageIndex(totalPages) {
 //console.log("inside increment function^^^^^^^^^^^^^^",totalPages)


 if (i < totalPages){
 //console.log("before increment=====>",i)
 i++;
 //console.log("after increment=====>",i)
 } else if(i = totalPages){
 i = 0
 }

 viewNormalPatientList(i);
}

// Function to decrement the CURRENT_PAGE_INDEX
function decrementPageIndex(page) {
  if (i > 0) {
    //console.log("before decrement=====>", i);
    i--;
    //console.log("after decrement=====>", i);
  } else if(i = page){
   i = page
 }
 viewNormalPatientList(i);
}



// VIEW ABNORMAL PATIENT LIST
let currentPageIndexAbnormal = 0;
let abnormalPatientListQJson = { pagination: {} };

function initializePatientListAbnormal() {
  // Reset the current page index to 0
  currentPageIndex = 0;
  normalPatientListQJson.pagination.currentPage = currentPageIndex;
  console.log("INSIDE initializePatientList()", currentPageIndex);
  // Call the main function to fetch and display the patient list

  viewAbnormalPatientList(currentPageIndex);
}

function viewAbnormalPatientList(page) {
 const rowsPerPageSelectAbnormal = document.getElementById('rows-per-page-abnormal');
 const selectedValue = parseInt(rowsPerPageSelectAbnormal.value);
    ACTIVE_TAB = "ABNORMAL";
    $("#normalPatEmpty").hide();
    $("#abNormal-Patient-table-spinner").show();
    $('#abNormal-Patient-table-spinner').css({ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '200px' });
    console.log("Inside viewAbnormalPatientList()")


    let clientId = localStorage.getItem('clientId');
    //let userId = localStorage.getItem("userId");localStorage.getItem("loggedInUserId")

    let abNormalPatientURL = serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId")  + "&size=" + selectedValue + "&page=" + page + "&filterByAbnormal=1" + "&sortDirection=DESC";
    if (kitId) {
        abNormalPatientURL += "&kitSerialNumber=" + kitId;
    }
    if (SEARCH_PARAM_PAT !== "") {
        abNormalPatientURL += "&searchParameter=" + SEARCH_PARAM_PAT;
    }
    console.log("SEARCH_DATE_PAT-> ", SEARCH_DATE_PAT);
    if (SEARCH_DATE_PAT !== "") {
        var values = SEARCH_DATE_PAT.split("/");
        var fromDate = new Date(values[2], (values[1] - 1), values[0]);
                                        fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),
                                        0, 0, 0, fromDate.getMilliseconds());

                                        var toDate = new Date(values[2], (values[1] - 1), values[0]);
                                        toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),
                                        23, 59, 59, toDate.getMilliseconds());

        abNormalPatientURL += "&startDate=" + fromDate + "&endDate=" + toDate;
    }


  $.ajax({
    type: "GET",
    url: abNormalPatientURL,
    headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
    success: function (data) {
      $("#abNormal-Patient-table-spinner").hide();

      if (data.code == '200') {
        if (data.content && data.content.patientList) {
          console.log("DATA OF SUCCESS BLOCK");
          const receivedData = data.content.patientList;
          abnormalPatientListQJson.patientList = receivedData;
          abnormalPatientListQJson.pagination = data.content.pagination;
        }
      }
      console.log("Normal patients list", abnormalPatientListQJson.pagination);

      $("#patientsorderNoList").hide();
      $("#abnormalPatientsList").show();


      abnormalPatientListQJson.pagination.pageLimit = selectedValue;
      abnormalPatientListQJson.pagination.totalPages = Math.ceil(abnormalPatientListQJson.pagination.count / selectedValue);

      // Reload the current page to reflect the new rows per page


      //generatePaginationButtonsAbnormal(normalPatientListQJson.pagination.totalPages, page);
      viewabNormalPatientListPage(page, abnormalPatientListQJson);
      console.log("PAGINATION CURRENT PAGE", currentPageIndexAbnormal);
    }
  });
}

// Event listener for rows per page change
const rowsPerPageSelectAbnormal = document.getElementById('rows-per-page-abnormal');
rowsPerPageSelectAbnormal.addEventListener('change', handleRowsPerPageChangeAbnormal);

function handleRowsPerPageChangeAbnormal() {
  // Reset the current page index to 0
  currentPageIndexAbnormal = 0;
  // Call the main function to update the patient list with the new rows per page value
  viewAbnormalPatientList(currentPageIndex);


}

function viewabNormalPatientListPage(page, item) {
  console.log("INSIDE WHEN NEXT BUTTON CLICKED viewabNormalPatientListPage------->", page);
  // Update the current page in the response object
  currentPageIndexAbnormal = page;
  // Check if item is defined and has the necessary properties
  if (!item || !item.patientList || !item.pagination) {
    console.error('Invalid item data');
    return;
  }
  // Calculate the start and end index for the current page
  const startIndex = page * item.pagination.pageLimit;
  const endIndex = Math.min(startIndex + item.pagination.pageLimit, item.pagination.count);

//   console.log('startIndex:', startIndex);
//   console.log('endIndex:', endIndex);
//   console.log('item.patientList:', item.patientList);

  // Clear the existing data list
  const dataList = document.getElementById('abnormalPatientlistTable');
  dataList.innerHTML = '';

  // Generate data table HTML for the current page
  if (item.patientList && item.patientList.length > 0) {
    let Patient_List = [];
    for (Patient_List of item.patientList) {

      if (i < item.patientList.length) {
      var user = Patient_List;
      //console.log('User:', user);

 // DISPLAY PATIENT NAME
let patName = "";
if (user && user.patientFullName && user.patientFullName !== "") {
  patName = user.patientFullName;
} else {
  var fname = user && user.patientFirstName && user.patientFirstName !== "" ? user.patientFirstName : "";
  var lastname = user && user.patientLastName && user.patientLastName !== "" ? user.patientLastName : "";
  patName = fname + " " + lastname;
}

// CREATED DATE
let createdDate = "";
if (user && user.createdAt) {
  var m = new Date(user.createdAt);
  createdDate = m.getUTCDate() + "/" + (m.getUTCMonth() + 1) + "/" + m.getUTCFullYear();
}

     // VILLAGE CITY
let villageCity = "";
if (user && user.city) villageCity = user.city + "/";
if (user && user.village) villageCity += user.village;


      // BLOOD GROUP

let bloodGrp = user && user.bloodGroup ? user.bloodGroup : '-';

let ageGender = "";
if (user && user.age) ageGender = user.age + "/";
if (user && user.gender) ageGender += user.gender;
    // UHID
let uhid = user && user.uhid ? user.uhid : '-';


      // Create a new row for each patient
      const row = document.createElement('tr');

      // Create table cells and set their content
      const nameCell = document.createElement('td');
      nameCell.textContent = patName;

      const uhidCell = document.createElement('td');
      uhidCell.textContent = uhid;

      const visitDateCell = document.createElement('td');
      visitDateCell.textContent = createdDate ? createdDate : '-';

      const ageGenderCell = document.createElement('td');
      ageGenderCell.textContent = ageGender;

      const bloodGroupCell = document.createElement('td');
      bloodGroupCell.textContent = bloodGrp;

      const cityVillageCell = document.createElement('td');
      cityVillageCell.textContent = villageCity;

      const actionCell = document.createElement('td');
    const button = createViewButtonAbnormal(user.id, user.userId);

      // Add the cells to the row
      actionCell.appendChild(button);
      row.appendChild(nameCell);
      row.appendChild(uhidCell);
      row.appendChild(visitDateCell);
      row.appendChild(ageGenderCell);
      row.appendChild(bloodGroupCell);
      row.appendChild(cityVillageCell);
      row.appendChild(actionCell);

      // Add the row to the data list
      dataList.appendChild(row);
    }else {
        // Handle the case when 'endIndex' exceeds the length of 'patientList'
      console.log('User:', 'No data');}
}
  }else {
       // If syncTransactions is empty, display "No data available" message
       const row = document.createElement('tr');
       const noDataCell = document.createElement('td');
       noDataCell.textContent = 'No data available';
       noDataCell.colSpan = 5; // Set the colspan to the number of columns in the table
       row.appendChild(noDataCell);
       dataList.appendChild(row);
     }
  // Update the data count
    const start = startIndex + 1;
    const end = Math.min(startIndex + item.pagination.pageLimit, item.pagination.count);
    console.log("TOTAL ENTRIES", start, end, item.pagination.count);

    const dataCountElement = document.getElementById('data-count-abnormal');
    dataCountElement.innerText = "Showing " + start + " to " + end + " of " + item.pagination.count + " entries";
  // Generate pagination buttons
  generatePaginationButtonsAbnormal(item.pagination.totalPages, page);
}

function generatePaginationButtonsAbnormal(totalPages, currentPage) {
      const paginationContainer = document.getElementById('pagination-container-abnormal');
      paginationContainer.innerHTML = '';

      const maxPageButtons = 6; // Maximum number of page buttons to display
      let startPage = Math.max(currentPage - 1, 0);
      let endPage = Math.min(startPage + maxPageButtons, totalPages - 1);
      startPage = Math.max(endPage - maxPageButtons + 1, 0);

      // Create the previous button
      const previousButton = document.createElement('button');
    previousButton.textContent = 'Previous';
    previousButton.classList.add('primary-button');
    previousButton.style.backgroundColor = '#007bff';
    previousButton.style.color = '#fff';
      if (currentPage === 0) {
        previousButton.disabled = true;
      }
      previousButton.onclick = function () {

        currentPage = Math.max(currentPage - 1, 0);
        console.log("previous button clicked",currentPage)
        abnormalPatientListQJson.pagination.currentPage = currentPage;
        viewAbnormalPatientList(currentPage)
        //viewNormalPatientListPage(currentPage - 1, normalPatientListQJson);
      };
      paginationContainer.appendChild(previousButton);

      // Create the page number buttons
      if (startPage > 0) {
        const firstPageButton = document.createElement('button');
        firstPageButton.textContent = '1';
        firstPageButton.classList.add('pagination-button-abnormal');
        if (currentPage === 0) {
                firstPageButton.disabled = true;
           }
        firstPageButton.onclick = function () {

          currentPage = 0;
          console.log("First page number button clicked",currentPage)
          abnormalPatientListQJson.pagination.currentPage = currentPage;
          viewAbnormalPatientList(currentPage)
          //viewNormalPatientListPage(0, normalPatientListQJson);
        };
        paginationContainer.appendChild(firstPageButton);
      }
      for (let i = startPage; i <= endPage; i++) {
        const button = document.createElement('button');
        button.textContent = i + 1;
        button.classList.add('pagination-button-abnormal');
        if (i === currentPage) {
          button.disabled = true;
        }
        button.onclick = function () {

          currentPage = i;
          console.log("page number button clicked",currentPage)
          abnormalPatientListQJson.pagination.totalPages = currentPage;
          viewAbnormalPatientList(currentPage)
        };
        paginationContainer.appendChild(button);
      }

      // Create the next button
      const nextButton = document.createElement('button');
    nextButton.textContent = 'Next';
    nextButton.classList.add('primary-button');
    nextButton.style.backgroundColor = '#007bff';
    nextButton.style.color = '#fff';
      if (currentPage === totalPages - 1) {
        nextButton.disabled = true;
      }
      nextButton.onclick = function () {
        currentPage++;
        if (currentPage >= abnormalPatientListQJson.pagination.totalPages) {
            // If it exceeds, set it to the last page

            currentPage = abnormalPatientListQJson.pagination.totalPages - 1;
        }
        viewAbnormalPatientList(currentPage);
        console.log("next button clicked",currentPage)
      };
      paginationContainer.appendChild(nextButton);
    }

//VIEW BUTTON IN TABLE

function createViewButtonAbnormal(patientId, patUId) {
  const button = document.createElement('button');
  button.textContent = 'View';
  button.classList.add('primary-button');
  button.style.backgroundColor = '#007bff';
  button.style.color = '#fff';
  button.style.border = 'none';
  button.style.padding = '8px 16px';
  button.style.borderRadius = '4px';
  button.style.cursor = 'pointer';
  button.style.transition = 'background-color 0.3s ease';

  // Add hover effect
  button.addEventListener('mouseover', function() {
    button.style.backgroundColor = '#0056b3';
  });

  button.addEventListener('mouseout', function() {
    button.style.backgroundColor = '#007bff';
  });

  button.onclick = function () {
    openDashboard(patientId, patUId);
  };

  return button;
}


// Function to increment the CURRENT_PAGE_INDEX
var i = 0;
function incrementAbnormalPageIndex(totalPages) {
 //console.log("inside increment function^^^^^^^^^^^^^^",totalPages)


 if (i < totalPages){
 //console.log("before increment=====>",i)
 i++;
 //console.log("after increment=====>",i)
 } else if(i = totalPages){
 i = 0
 }

 viewAbnormalPatientList(i);
}

// Function to decrement the CURRENT_PAGE_INDEX
function decrementAbonormalPageIndex(page) {
  if (i > 0) {
    //console.log("before decrement=====>", i);
    i--;
    //console.log("after decrement=====>", i);
  } else if(i = page){
   i = page
 }
 viewAbnormalPatientList(i);
}

 function searchInput() {
                    console.log("Search for input");
                    SEARCH_PARAM_PAT = $("#searchedInput").val();
                    if (ACTIVE_TAB === "NORMAL") {
                        console.log("Search for input Normal tab");
                       viewNormalPatientList(currentPageIndex);

                    } else {
                        console.log("Search for  Abnormal pat");
                        viewAbnormalPatientList(currentPageIndex);
                    }

                }

                function searchForDate() {
                                    SEARCH_DATE_PAT = $("#birthDat").val();
                                    if (ACTIVE_TAB === "NORMAL") {
                                        console.log("Search for input Normal tab");
                                         viewNormalPatientList(currentPageIndex);

                                    } else {
                                        console.log("Search for  Abnormal pat");
                                        viewAbnormalPatientList(currentPageIndex);
                                    }

                                }
            </script>
        </body>

        </html>
