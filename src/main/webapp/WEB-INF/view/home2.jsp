<%@ include file="/WEB-INF/view/includes.jsp" %>
    <!DOCTYPE>
    <html>

    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title>Statistics</title>

        <link rel="stylesheet" href="resources/css/bootstrap.min.css" />
        <link rel="stylesheet" href="resources/css/toastr.min.css" />
        <link href="resources/css/AdminLTE.css" rel="stylesheet" />
        <link href="resources/css/_all-skins.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="resources/css/font-awesome.min.css" />
        <link rel="stylesheet" href="resources/css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="resources/css/material.css">

        <!-- <link rel="stylesheet" href="resources/css/apexcharts.css"> -->
        <!-- <script src="resources/js/apexcharts.min.js"></script> -->

         <script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.0"></script>
          <script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>

        <script src="resources/js/jQuery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

        <!------Time zone----->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.33/moment-timezone.min.js"></script>
        <!------ DATA TABLE ---->

        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/fastclick.min.js"></script>
        <!-- <script src=".resources/js/app.min.js"></script> -->
        <script src="resources/js/demo.js"></script>
        <script src="resources/js/customHTML.js"></script>
        <script src="resources/js/toastr.min.js"></script>
        <script src="resources/js/bootstrap-datepicker.js"></script>


        <!-- DataTables  & Plugins -->

        <!----<script src="resources/js/dataTables.bootstrap.min.js"></script>----->
        <script src="resources/customJS/statistics1.js"></script>
        <script src="resources/customJS/videoCall.js"></script>
       <!----- <script src="resources/customJS/patientList.js"></script>---->
        <link rel="stylesheet" href="resources/customCSS/statistics.css">

        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>


        <style type="text/css">
            .backImage {
                background-image: url("resources/images/profile.jpg");
            }
        </style>
        <style>
            .toggleSwitch {
                position: relative;
                display: inline-block;
                width: 120px;
                height: 34px;
            }

            .toggleSwitch .toggleInput {
                display: none;
            }

            .dateSlider {
                position: absolute;
                cursor: pointer;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: #2ab934;
                -webkit-transition: .4s;
                transition: .4s;
            }

            .dateSlider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

            .toggleInput:checked+.dateSlider {
                background-color: #ca2222;
            }

            .toggleInput:focus+.slider {
                box-shadow: 0 0 1px #2196F3;
            }

            .toggleInput:checked+.dateSlider:before {
                -webkit-transform: translateX(85px);
                -ms-transform: translateX(85px);
                transform: translateX(85px);
            }

            /*------ ADDED CSS ---------*/
            .on {
                display: none;
            }

            .on,
            .off {
                color: white;
                position: absolute;
                transform: translate(-50%, -50%);
                top: 50%;
                left: 50%;
                font-size: 12px;
                font-family: Verdana, sans-serif;
            }

            .toggleInput:checked+.dateSlider .on {
                display: block;
            }

            .toggleInput:checked+.dateSlider .off {
                display: none;
            }

            /*--------- END --------*/

            /* Rounded sliders */
            .dateSlider.round {
                border-radius: 34px;
            }

            .dateSlider.round:before {
                border-radius: 50%;
            }

            .navbar-t-centered {
                position: absolute;
                left: 34%;
                display: block;
                text-align: center;
                color: #fff;
                top: 0px;
                padding: 15px;
                font-weight: 400;
                font-size: 12px;
            }




            @media screen and (min-width:768px) {
                .navbar-t-centered {
                    position: absolute;
                    left: 38%;
                    display: block;
                    /*  width: 160px; */
                    text-align: center;
                    color: #fff;
                    top: 0px;
                    padding: 15px;
                    font-weight: 400;
                    font-size: 22px;
                }
            }
        </style>
        <script type="text/javacript">



</script>
        <script type="text/javascript">
            //  var	serverPath ="http://192.168.1.225:9090/PopulationCarev2";
            var productId = localStorage.getItem("productId");
            var loggedInUserId = '';
            // const DEF_DELAY = 1000;
            // function sleep(ms) {
            //     return new Promise(resolve => setTimeout(resolve, ms || DEF_DELAY));
            // }

            var SEARCH_PARAM_PAT = "", SEARCH_DATE_PAT = "";
            var ACTIVE_TAB = "NORMAL";
            var kitId = localStorage.getItem("kitPermission");
            var DW_JWT_TOKEN = localStorage.getItem('DW_JWT_TOKEN');
            var loggedInUserFullName = localStorage.getItem('loggedInUserFullName');
            var ROLL_NAME = localStorage.getItem('roleName');
            var RESET_PASSWORD = localStorage.getItem('RESET_PASSWORD');
            $(document).ready(function () {


                  // Check if the user is logged in
                  if (DW_JWT_TOKEN === null) {
                    // Redirect to the logout page or login page
                    window.location.href = 'logout';
                  } else if (RESET_PASSWORD === 'true') {
                    // The user is logged in and needs to reset their password
                    console.log('RESET PASSWORD PAGE');
                  } else {
                    // The user is logged in and not resetting their password
                    console.log('OTHER PAGE');
                  }
                //$(window).off('beforeunload');

                $("#patReportBtn").hide();
                $('#dwndAllCharts').hide();
                $('#verticalLine').hide();
                if (localStorage.getItem("userGroupCode") === 'NUR') {

                    window.location.href = 'patientList';
                }
                if (localStorage.getItem("userGroupCode") === 'DOC') {

                    pingCallForVC();
                }
                if (localStorage.getItem("roleName") === 'Client Admin') {

                    $("#patReportBtn").show();
                    $("#dwnldPdfPulse").show();
                    $("#dwnldCsvPulse").show();
                    $("#dwnldPdfTemp").show();
                    $("#dwnldCsvTemp").show();
                    $("#dwnldPdfBp").show();
                    $("#dwnldCsvBp").show();
                    $("#dwnldPdfSp02").show();
                    $("#dwnldCsvSp02").show();
                    $("#dwnldPdfBmi").show();
                    $("#dwnldCsvBmi").show();
                    $("#dwnldPdfBg").show();
                    $("#dwnldCsvBg").show();
                    $("#dwndAllCharts").show();
                    $('#verticalLine').show();

                    $("#dwnldPdfPulseGender").show();
                    $("#dwnldCsvPulseGender").show();
                    $("#dwnldPdfTempGender").show();
                    $("#dwnldCsvTempGender").show();
                    $("#dwnldPdfBpGender").show();
                    $("#dwnldCsvBpGender").show();
                    $("#dwnldPdfSp02Gender").show();
                    $("#dwnldCsvSp02Gender").show();
                    $("#dwnldPdfBmiGender").show();
                    $("#dwnldCsvBmiGender").show();
                    $("#dwnldPdfBgGender").show();
                    $("#dwnldCsvBgGender").show();

                    //dwnld png buttons
                     $("#dwnldPngPulse").show();
                     $("#dwnldPngTemp").show();
                     $("#dwnldPngBp").show();
                     $("#dwnldPngPulseGender").show();
                     $("#dwnldPngTempGender").show();
                     $("#dwnldPngBpGender").show();
                     $("#dwnldPngSp02").show();
                     $("#dwnldPngBmi").show();
                     $("#dwnldPngBg").show();
                     $("#dwnldPngSp02Gender").show();
                     $("#dwnldPngBmiGender").show();
                     $("#dwnldPngBgGender").show();



                }
                if (localStorage.getItem("roleName") === 'Doctor' && ENABLE_CSV_DOCTOR == true){
                    console.log("ENABLE_CSV_DOCTOR",ENABLE_CSV_DOCTOR)
                    //$("#patReportBtn").show();
                }


                //var hash = window.location.hash;
                //console.log("On Home load:", hash)
                //$('#homeTab a[href="' + hash + '"]').tab('show');

                var loggedInUserFullName = localStorage
                    .getItem("loggedInUserFullName");
                $("#loggedInUserFullName").text(loggedInUserFullName);
                $("#loggedInUserFullNameSpan").text(
                    loggedInUserFullName);


                // Load Normal Patient table by default

                initializePatientList();



            })

            function openDashboard(patientId, patUId) {
                localStorage.setItem("patientId", patientId);
                localStorage.setItem("patientUserId", patUId);
                window.location.href = "dashBoard";
                return;
            }
            function openRegistration() {
                window.location.href = "doctorRegistration";
            }

            function logout() {
                localStorage.removeItem("DW_JWT_TOKEN");
                localStorage.removeItem("roleName");
                localStorage.removeItem("clientId");
                localStorage.removeItem("userGroupCode");
                localStorage.removeItem("loggedInUserFullName");
                localStorage.removeItem("kitPermission");
                localStorage.removeItem("patientId");
                localStorage.removeItem("patientUserId");
                 localStorage.removeItem("RESET_PASSWORD");
                window.location.href = "logout";
            }


            $(document).ready(function () {

                $('#birthDat').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,//.date
                    endDate: '1d'
                }).on('changeDate', function (e) {
                    $("#birthDat").attr("readonly", true);
                    $('#birthDat').parent().addClass("is-dirty");
                    $(this).datepicker('hide');
                });
                // clearReg();
            });
            let DOWNLOAD_CSV_TOGGLE_STATE = 'TODAY';
            $(document).ready(function () {
                $(".toggleSwitch .toggleInput").on("change", function (e) {
                    const isOn = e.currentTarget.checked;
                    //console.log('Toggle btn change: ',isOn, DOWNLOAD_CSV_TOGGLE_STATE)

                    if (isOn) {
                        $(".hideme").show();
                        DOWNLOAD_CSV_TOGGLE_STATE = 'CUSTOM';
                    } else {
                        $(".hideme").hide();
                        DOWNLOAD_CSV_TOGGLE_STATE = 'TODAY';
                    }
                });
            });


            function searchForPatient() {

                /* var servicePath = "http://localhost:9090/Doctor"; */
                var searchedInput = $("#searchedInput").val();
                //alert(searchedInput);
                console.log("Search for patients: ", searchedInput)

                let searchURL = serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId") + '&size=10000';

                if (kitId) {
                    searchURL += "&kitSerialNumber=" + kitId;
                }
                if ($("#searchedInput").val() != "") {
                    searchURL += "&searchParameter=" + searchedInput;
                }

                $.ajax({
                    type: "GET",
                    // url : serverPath + "/getPatientList?searchParameter=" + searchedInput + "&productId=" + productId,
                    url: searchURL,
                    headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
                    success: function (data) {
                        console.log('---getPatientListByClient?SUCCS --', data.content)

                        if (data.code == '200') {

                            var listOfCards = "";
                            $("#searchedInput").val("");
                            let { patientList } = data.content;
                            console.log('---getPatientListByClient?patientList --', patientList)
                            if (patientList) {

                                console.log('---getPatientListByClient?contentArray --', contentArray, ', leng: ', contentArray.length)
                                if (patientList && patientList.length > 0) {
                                    $.each(patientList, function (index, option) {
                                        var lastName = "";
                                        var uhid = option.uhid;
                                        if (option.uhid == null
                                            || option.uhid == ""
                                            || option.uhid == "undefined"
                                            || option.uhid == "null") {
                                            uhid = " ";
                                        }
                                        if (option.bloodGroup == null) {
                                            option.bloodGroup = "";
                                        }
                                        if (option.maritalStatus == null) {
                                            option.maritalStatus = "";
                                        }
                                        if (option.patientLastName != null) {
                                            lastName = option.patientLastName;
                                        }
                                        listOfCards += '<div class="col-md-3 col-sm-6 col-xs-12" style="margin-top:10px;">'
                                            + '<div class="box box-primary">'
                                            + '<div class="box-body box-profile" style="height:304px;">'
                                            + '<img class="profile-user-img img-responsive img-circle" src="resources/images/user-silhouette.png" alt="User profile picture">'
                                            + '<h5 style="font-size: 16px;" class="text-center">'
                                            + option.patientFirstName
                                            + ' '
                                            + lastName
                                            + '</h5>'
                                            + '<p class="text-muted text-center" style="margin: 0 0 10px;">'
                                            + uhid
                                            + '</p>'

                                            + '<ul class="list-group list-group-unbordered" style="font-size: 12px;">'

                                            + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                            + '<b>'
                                            + option.mobileNo
                                            + '</b>'
                                            + '</li>'
                                            + '<li class="list-group-item text-center" style="font-size: 12px;border-top-width: 0px; border-bottom-width: 0px; padding-top: 5px; padding-bottom: 5px;">'
                                            + '<b>'
                                            + option.bloodGroup
                                            + ' / '
                                            + option.maritalStatus
                                            + ' '
                                            + '</b>'
                                            + '</li>'

                                            + '</ul>'
                                            + '<a href="#" class="btn btn-primary btn-block" onclick="openDashboard('
                                            + option.id + ',' + option.userId
                                            + ');"><b>More</b></a>'
                                            + '</div>'
                                            + '</div>'
                                            + '</div>'
                                    });
                                    $("#patientListCard").html(listOfCards);
                                } else {
                                    $("#patientListCard").html("<h5  class='text-center'>Patient table empty!</h5>");
                                }
                            } else {
                                $("#patientListCard").html("<h5  class='text-center'>No Patients found</h5>");
                            }
                        } else {
                            $("#patientListCard").html("<h5  class='text-center'>No Patient Available for today !</h5>");
                        }

                    },
                    error: function (xhr, status, error) {
                        console.log('xhr: ', xhr, ' -->Status: ', status, ' -->Error: ', error)
                        if (xhr.status == 0) window.location.href = './login';
                        if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                            if (xhr.responseJSON.code === 401) {
                                (async () => {
                                    toastr.error(xhr.responseJSON.message);
                                    await sleep();
                                    window.location.href = './login';
                                })()
                            }
                            toastr.error(xhr.responseJSON.message);
                        } else {
                            toastr.error("No Data Found!");
                        }
                    }
                });

            }




            function confPopUp(callerName, roomName, roomId, weconUrl) {
                var room = encodeURIComponent(roomName);
                videocalllink = weconUrl + roomId;
                var win;
                var r = confirm(callerName + " is calling you. Do you accept the call?");
                //	alert(r);
                if (r == true) {
                    win = window.open(videocalllink, 'result', 'width=300,height=300');

                    /* win.addEventListener("resize", function(){
                        win.resizeTo(1024, 768);
                    }); */
                }
                $.ajax({
                    type: "POST",
                    url: serverPath + "/intiateAndCloseCallv4?initiate=false&roomId="
                        + roomId,
                    success: function (data) {
                        //win.close();
                    }
                });
            }

            function initiateCall(calleProfileId) {
                var proId = localStorage.getItem("productId");
                if (proId == 1310) {
                    alert("This Service is not enabled, Please contact Wenzins");
                } else {
                    /* var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
                    win.addEventListener("resize", function(){
                            win.resizeTo(1024, 768);
                    }); */
                    $.ajax({
                        type: "POST",
                        url: serverPath + "/intiateAndCloseCallv4?profileId="
                            + localStorage.getItem("profileId")
                            + "&calleProfileId=" + calleProfileId + "&serverKeyId="
                            + localStorage.getItem("serverKeyId") + "&initiate="
                            + true,
                        success: function (data) {
                            //	console.log(data);
                            roomid = data.roomid;
                            //	alert(data);
                            //	console.log("new  "+data.content.weconurl);

                            videocalllink = data.content.weconurl + data.roomid;

                            /* 	 $.each(data.content,function(index, option) {
                                     console.log(option);
                                     var url = option.weconurl;
                                //	 alert(url);
                                     console.log("link"+url);
                                     var win = window.open(url);
                                      videocalllink =  url + roomid ;
                            //	var win = window.open(videocalllink , 'result', 'width=1024,height=768');
                            }); */
                            //	 window.open(a);
                            var win = window.open(videocalllink, 'result',
                                'width=300,height=300');

                            win.addEventListener("resize", function () {
                                win.resizeTo(1024, 768);
                            });
                        }
                    });
                }
            }


            $(function () {
                $("#datepicker3Val").datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    endDate: "today"
                }).datepicker('update', new Date());
            });
            $(function () {
                $("#datepicker4Val").datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    endDate: "today"
                }).datepicker('update', new Date());
            });

            $(function () {
                $("#datepicker1").datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    endDate: "today"
                }).datepicker('update', new Date());
            });

            $(function () {
                $("#datepicker2").datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    todayHighlight: true,
                    endDate: "today"
                }).datepicker('update', new Date());
            });

            function downloadPatientCsv() {
            let option = $("#reportId").val();
            console.log("selected val:",option);

                console.log('downloadPatientCsv->', $('#datepicker1Val').val(), ', ', $('#datepicker2Val').val(), DOWNLOAD_CSV_TOGGLE_STATE)

                var profileID = localStorage.getItem("profileId");

                var serverKeyId2 = localStorage.getItem("serverKeyId");



                if (DOWNLOAD_CSV_TOGGLE_STATE === 'TODAY') {

                    fromDate = new Date();

                    toDate = new Date();

                    fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(), 0, 0, 0, fromDate.getMilliseconds());

                    toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(), 23, 59, 59, toDate.getMilliseconds());

                    // console.log('---TODAY: ', fromDate, ', ', toDate);

                } else {



                    var values = $('#datepicker1Val').val().split("/");

                    fromDate = new Date(values[2], (values[1] - 1), values[0]);

                    var values2 = $('#datepicker2Val').val().split("/");

                    toDate = new Date(values2[2], (values2[1] - 1), values2[0]);



                    // console.log('---CUSTOM: ', fromDate, ', ', toDate);



                    var today = new Date().setHours("23", "59", 59, 0);

                    if (toDate == null || toDate == 'Invalid Date' || fromDate == null || fromDate == 'Invalid Date') {

                        toastr.error('Selected date is null');

                        return;

                    }



                    if (toDate.getTime() > today || fromDate.getTime() > today) {

                        toastr.error('Date Cannot Be Greater Than Todays Date');

                        return;

                    }

                    if (toDate.getTime() < fromDate.getTime()) {

                        toastr.error('To Date cannot be Lesser Than From Date');

                        return;

                    }

                    fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),

                        0, 0, 0, fromDate.getMilliseconds());

                    toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),

                        23, 59, 59, toDate.getMilliseconds());

                    //fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),0, 0, 0, fromDate.getMilliseconds());

                    //toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),23, 59, 59, toDate.getMilliseconds());

                }



                // console.log("----DATE-----", fromDate, toDate)

                //var fromDate, toDate, today = new Date().setHours("23", "59", 59, 0);

                fromDate = new Date(fromDate).getTime();

                toDate = new Date(toDate).getTime();



                console.log("downloadPatientCsv-> ", "From: ", fromDate, "To: ", toDate,"Kit serial no", kitId);

                //GET CURRENT TIME ZONE

               var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
               console.log("CURRENT TIME ZONE",timezone);



               if($("#reportId").val() === "Patient Vital Report"){

                  var newVitalURL = serverPath + "/getPatientReportByClientId?clientId=" + localStorage.getItem("clientId") + "&startEpoch=" + fromDate + "&endEpoch=" + toDate + "&timeZone=" + timezone ;
                   if (localStorage.getItem("roleName") === 'Doctor' && ENABLE_CSV_DOCTOR == true) {
                       newVitalURL += "&kitSerialNumber=" + localStorage.getItem("kitPermission");
                    }
                       window.location.href = newVitalURL;
                    }else if($("#reportId").val() === "Patient Report"){
                    var newURL = serverPath + "/getPatientReport?clientId=" + localStorage.getItem("clientId") + "&startEpoch=" + fromDate + "&endEpoch=" + toDate + "&timeZone=" + timezone;
                        if (localStorage.getItem("roleName") === 'Doctor' && ENABLE_CSV_DOCTOR == true) {
                          newURL += "&kitSerialNumber=" + localStorage.getItem("kitPermission");
                        }
                        window.location.href = newURL;
                  }


            };

            //LOGOUT WHEN BACK BUTTON CLICKED

            // if (performance.navigation.type == 2) {

            // let text = " Do you want to Logout ?...Your Session will be closed";

            // if (confirm(text) == true){

            //            localStorage.removeItem("DW_JWT_TOKEN");
            //            localStorage.removeItem("roleName");
            //            localStorage.removeItem("clientId");
            //            localStorage.removeItem("userGroupCode");
            //            localStorage.removeItem("loggedInUserFullName");
            //            localStorage.removeItem("kitPermission");
            //            localStorage.removeItem("patientId");
            //            localStorage.removeItem("patientUserId");
            //            localStorage.removeItem("productId");
            //            window.location.href = "logout";
            //     }else {

            //             window.location.href = "home";
            //           }

            //  }
            //let table = $('#PatientlistTable').DataTable({sDom: 'lrtip'});


<!--------------UPCOMING MEETING---------------->






        </script>


    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div>

            <header class="main-header" style="padding-bottom:10px;background:#ecf0f5">
                <nav class="navbar navbar-static-top" role="navigation"
                    style="margin-left: 0% !important; max-height: 50px; background-color: #3c8dbc">
                    <!-- Sidebar toggle button-->
                    <img src="resources/images/WriztoLogoBox.jpg" onclick="navigateHome();" class="user-image"
                        alt="User Image" style="height: 50px; padding-left: 5px; margin-left: 5px;"> <span
                        class="navbar-t-centered">Wrizto Healthcare System </span>


                    <!-- <button type="button" class="btn btn-default.button4 {padding: 32px 16px;} px-3"><i class="fas fa-balance-scale" aria-hidden="true"></i></button>
	 -->


                    <div class="navbar-custom-menu">

                        <ul class="nav navbar-nav">
                            <!--  <li style="top: 8px;"><label class="switch"><input id="someSwitchOptionInfo" type="checkbox" checked> <span class="slider round"></span></label></li>
             -->
                            <!-- Messages: style can be found in dropdown.less-->

                            <!-- Patient Report csv download -->
                            <li>
                                <!-- Trigger the modal with a button -->

                                <button type="button" class="btn btn-lg" style="background-color: transparent;"
                                    id="patReportBtn" data-toggle="modal" data-target="#myModal">
                                    <img src="resources/images/file2.png"
                                        style="width: 25px; height: 25px; margin-top: 3px;" alt="FIle Icon" />
                                </button> <!-- Modal -->
                                <div id="myModal" class="modal fade" role="dialog" data-backdrop="false">
                                    <div class="modal-dialog modal-sm">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header" style="border-bottom: 3px solid #3c8dbc;">
                                                <button type="button" class="close" onClick="resetReportDatePicker()"
                                                    data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Report</h4>
                                            </div>
                                            <div class="modal-body">

                                                <p style="margin-bottom: 0px; color: royalblue">Select Report Type</p>
                                                    <div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
                                                        <div style="width: 70%; color: black; padding-top: 0px; padding-bottom: 20px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;"
                                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
                                                        <select class="form-control" id="reportId" name="instruction"
                                                        style="display: block ! important;width: 70%; border-left-width: 0px; border-top-width: 0px; border-right-width: 0px; padding-left: 0px;">
                                                        <option value="Patient Report">Patient Report</option>
                                                        <option value="Patient Vital Report">Patient Vital Report</option>
                                                        </select>
                                                </div>
                                        </div>


                                                <label class="toggleSwitch"> <input type="checkbox" id="togBtn"
                                                        class="toggleInput">
                                                    <div class="dateSlider round">
                                                        <!--ADDED HTML -->
                                                        <span class="on">Custom</span> <span class="off">Today</span>
                                                        <!--END-->
                                                    </div>
                                                </label>

                                                <div class="hideme" style="display:none;">
                                                    <label>Start : </label>
                                                    <div id="datepicker1" class="input-group date"
                                                        data-date-format="dd/mm/yyyy">
                                                        <input id="datepicker1Val" class="form-control" type="text"
                                                            readonly /> <span class="input-group-addon"><i
                                                                class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>

                                                    <label>End : </label>
                                                    <div id="datepicker2" class="input-group date"
                                                        data-date-format="dd/mm/yyyy">
                                                        <input id="datepicker2Val" class="form-control" type="text"
                                                            readonly /> <span class="input-group-addon"><i
                                                                class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" style="background-color:#3c8dbc; color:white"
                                                    class="btn btn-default"
                                                    onClick="downloadPatientCsv();">Download</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </li>



                            <!-- User menu dropdown -->
                            <li class="dropdown user user-menu"><a href="#" class="dropdown-toggle"
                                    data-toggle="dropdown"> <img src="resources/images/avatar.png" class="user-image"
                                        alt="User Image"> <span class="hidden-xs" id="loggedInUserFullName"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->

                                    <li class="user-header"><img src="resources/images/avatar.png" class="img-circle"
                                            alt="User Image">
                                        <p>
                                            <span id="loggedInUserFullNameSpan"></span>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body"
                                        style="padding-top: 0px; padding-right: 0px; padding-left: 0px;">
                                        <!-- <div class="col-xs-7 text-center"><span style="font-size: 12px; margin-left: -33px;" id="nurseNameForActiveId"></span>
                                  <a id= "videoIconId" style="border-radius: 50%; ;margin-left: 5px;background-color: green" class="btn btn-primary" title="videoCall" onclick ="initiateCall();"><b>
                  				   <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a>
                                  </div>
                                  <div class="col-xs-5 text-center">
                                    <a href="patientList">Patient List</a>
                                  </div> activeUsersId  activeUsersId-->

                                        <ul class="col-xs-12 text-center" id="activeUsersId">
                                            <!-- <li class="form-control" style="border-radius:6px;" ><span style="font-size: 12px;text-transform: capitalize;margin-left: -33px;" id="nurseNameForActiveId">Wrizto Nurse</span>
                 			 <a  id= "videoIconId" style="border-radius: 50%;margin-left: 5px;float:right;background-color: green" class="btn btn-primary" title="videoCall" onclick="initiateCall(1358)"><b>
                 			 	<i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a></li>
                 				 -->

                                        </ul> <br>
                                        <ul>
                                            <br>
                                            <!--  <span style="top: 4px;"> <button style="padding: 10px 10px; type="button" class="btn btn-success"><i class="fa fa-video-camera fa-lg" aria-hidden="true"></i>&nbsp;<span><b>Wecon </b></span></button>
                 				  <button style="padding: 10px 10px; type="button" class="btn btn-success "><i class="fa fa-video-camera fa-lg" aria-hidden="true"></i>&nbsp;<span><b>Mscp</b></span></button>
                 				 </span> -->

                                        </ul>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left"></div>
                                        <div class="pull-left">
                                            <a onclick="logout();" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                        <div class="pull-right"></div>
                                        <div class="pull-right" id='patListReset'>
                                        <a onclick="resetPassword()" class="btn btn-default btn-flat">Change Password</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->

                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->


            <!--
		<div class="col-md-12" id="patientListCard"
			style="padding-bottom: 60px;"></div>
		<!-- Content Wrapper. Contains page content
		<div class="content-wrapper"
			style="margin-left: 0% !important; margin-bottom: 10px;"></div>
		<input type="hidden" id="abcdefg" value="">
		-->

            <div>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" style="border-bottom:1px solid lightblue" id="homeTab">
                        <li onclick="openCity(event, 'Paris','activity'),initializePatientList()" id="patientTab"
                            class="tablinks active"> <a href="#activity" data-toggle="tab">Patients</a> </li>
                        <li onclick="openCity(event, 'London','timeline'), getPatientStats('d2')" id="statsTab"
                            class="tablinks"> <a href="#timeline" data-toggle="tab">Statistics</a> </li>

                    </ul>
                    <div id="London" class="tabcontent" style="display:none">
                        <div class="tab-pane" id="timeline">

                            <!--------------------------------------------------- Statistics Page content --------------------------------------------->
                            <div class="stats-conatiner container-fluid">

                                <div class="row">
                                    <div class="col-sm-12" style="display:flex;justify-content:end; margin:10px 0px">
                                        <div class="col-md-4" style=" width: 100%; display: inline-flex;">
                                            <!------ <div class="custom-control custom-radio custom-control-inline">
                                               <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                               <label class="custom-control-label" for="customRadioInline1">Vilage</label>
                                               <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                                               <label class="custom-control-label" for="customRadioInline2">Pincode</label>
                                             </div>------>
                                            <div class="float-right" style="display:block;">
                                                <div class="input-group "
                                                    style="margin-left:10px;justify-content:start;padding-right: 30px;">
                                                    <input type="text" id="searchedRegion" class="form-control"
                                                        placeholder="Search Village/Pincode">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-search " type="button"
                                                            style="background-color: #3c8dbc; color: white;"
                                                            onclick="searchStatsParameter();">
                                                            <i class="fa fa-search fa-fw"></i> Search
                                                        </button>
                                                    </span>

                                                    <span class="input-group-btn"  >
                                                    <span id="verticalLine" style="border-left: 1px solid gray; height: 30px; margin: 0 10px; display: inline-block; opacity:0.3"></span>
                                                    </span>

                                                    <span  class="input-group-btn"
                                               style="cursor:pointer;" id="dwndAllCharts"
                                               data-toggle="dropdown" aria-haspopup="true"
                                               aria-expanded="false">
                                               <button  type="button"  id="dwndAllCharts"  style=" outline:none; border: none;background-color: transparent;border-radius: 2px; padding: 0.5px;" title="Download All Charts" > <img src="resources/images/file6.png" style="width: 32px; height: 32px; margin-top: 3px;" alt="FIle Icon"></button>

                                                    </span>

                                     </div>
                                            </div>
                                        </div>

                                        <button type="button" id="d1" style="margin-left:10px;background:#B8B8B8;"
                                            class="btn btn-primary normalCol"
                                            onclick="getPatientStats('d1');">Today</button>
                                        <button type="button" id="d2" style="margin-left:10px;background:#367fa9;"
                                            class="btn btn-primary normalCol"
                                            onclick="getPatientStats('d2');">Weekly</button>
                                        <button type="button" id="d3" data-toggle="modal" data-target="#statsModal"
                                            style="margin-left:10px;background:#B8B8B8;"
                                            class="btn btn-primary normalCol"
                                            onclick="buttonTest('d3');">Custom</button>



                                    </div>
                                </div>

                                <!--Patient Statistics Table Modal -->
                                <div class="modal fade" id="tableModalCenter" tabindex="-1" role="dialog"
                                    aria-labelledby="tableModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="tableModalLongTitle"></h5>
                                                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button> -->
                                            </div>
                                            <div class="modal-body tableFixHead" style="padding:0;" id="statsTableId">
                                                <table id="patientStatsTable1" class="table table-striped">
                                                    <thead id="patientStatsHeader">
                                                    </thead>
                                                    <tbody id="patientStatsBody">
                                                    </tbody>
                                                </table>
                                                <div id="statsTableEmpty"
                                                    style="display:flex;justify-content:center;align-items:center;height:100%;">
                                                    No data!</div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="container"> </div>
                                 <div id="All charts">
                                <div class="row">
                                    <div class="chart1 col-sm-4" id="Pulse Rate Statistics">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Pulse Rate Statistics</div>
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip1"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (72 ~ 100 bpm)</p><p>- Abnormal (<72 / >100 bpm)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn1"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">

                                                        </span>

                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn1"
                                                            style="border: 1px solid black;">

                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('Pulse');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfPulse"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('Pulse','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvPulse"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('Pulse','csv');"
                                                                href="#">Download CSV</a>
                                                                <a class="dropdown-item" id="dwnldPngPulse"
                                                                 style="display: none;"
                                                                 onclick="downloadDivAsImage('Pulse Rate Statistics')"
                                                                 href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart1" class="chartContainer"></div>
                                            <div id="donutChart1-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chart2 col-sm-4" id="Temperature Statistics" >
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Temperature Statistics</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip2"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (36 ~ 38 <span>&#8451;</span>)</p><p>- Abnormal (<36 / >38 <span>&#8451;</span>)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn2"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>

                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn2"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('TEMPERATURE');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfTemp"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('TEMPERATURE','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvTemp"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('TEMPERATURE','csv');"
                                                                href="#">Download CSV</a>
                                                                <a class="dropdown-item" id="dwnldPngTemp"
                                                                 style="display: none;"
                                                                onclick="downloadDivAsImage('Temperature Statistics')"
                                                                href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart2" class="chartContainer"></div>
                                            <div id="donutChart2-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chart3 col-sm-4" id="Blood Pressure Statistics">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Blood Pressure Statistics</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip3"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>-Optimal (SYS <120, DIA <80 mm Hg)</p><p>- Normal (SYS <130, DIA <85 mm Hg)</p><p>High-Normal (SYS 130~139, DIA 85~89 mm Hg)</p>
										<p>-G1-Hypertension (SYS 140~159, DIA 90~99 mm Hg)</p><p>-G2-Hypertension (SYS 160~179, DIA 100~109 mm Hg)</p><p>-G3-Hypertension (SYS >=180, DIA >=110 mm Hg)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn3"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu list-right"
                                                            aria-labelledby="dropdownMenuBtn3"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BP');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBp"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BP','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBp"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BP','csv');"
                                                                href="#">Download CSV</a>
                                                                <a class="dropdown-item" id="dwnldPngBp"
                                                                style="display: none;"
                                                                onclick="downloadDivAsImage('Blood Pressure Statistics')"
                                                                ref="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart3" class="chartContainer"></div>
                                            <div id="donutChart3-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="chart4 col-sm-4" id="Pulse Rate Gender Wise">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Pulse Rate Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip7"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (72 ~ 100 bpm)</p><p>- Abnormal (<72 / >100 bpm)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn7"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn7"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('Pulse');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfPulseGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('Pulse','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvPulseGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('Pulse','csv');"
                                                                href="#">Download CSV</a>
                                                                 <a class="dropdown-item" id="dwnldPngPulseGender"
                                                                  style="display: none;"
                                                                 onclick="downloadDivAsImage('Pulse Rate Gender Wise')"
                                                                 href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart1" class="chartContainer"></div>
                                            <div id="barChart1-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chart5 col-sm-4" id="Temperature Gender Wise">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Temperature Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip8"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (36 ~ 38 <span>&#8451;</span>)</p><p>- Abnormal (<36 / >38 <span>&#8451;</span>)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn8"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn8"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('TEMPERATURE');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfTempGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('TEMPERATURE','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvTempGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('TEMPERATURE','csv');"
                                                                href="#">Download CSV</a>
                                                                 <a class="dropdown-item" id="dwnldPngTempGender"
                                                                 style="display: none;"
                                                                 onclick="downloadDivAsImage('Temperature Gender Wise')"
                                                                 href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart2" class="chartContainer"></div>
                                            <div id="barChart2-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chart6 col-sm-4" id="Blood Pressure Gender Wise">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Blood Pressure Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip9"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>-Optimal (SYS <120, DIA <80 mm Hg)</p><p>- Normal (SYS <130, DIA <85 mm Hg)</p><p>High-Normal (SYS 130~139, DIA 85~89 mm Hg)</p>
										<p>-G1-Hypertension (SYS 140~159, DIA 90~99 mm Hg)</p><p>-G2-Hypertension (SYS 160~179, DIA 100~109 mm Hg)</p><p>-G3-Hypertension (SYS >=180, DIA >=110 mm Hg)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn9"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu list-right"
                                                            aria-labelledby="dropdownMenuBtn9"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BP');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBpGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BP','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBpGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BP','csv');"
                                                                href="#">Download CSV</a>
                                                                <a class="dropdown-item" id="dwnldPngBpGender"
                                                                 style="display: none;"
                                                                onclick="downloadDivAsImage('Blood Pressure Gender Wise')"
                                                                href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart3" class="chartContainer"></div>
                                            <div id="barChart3-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="chart7 col-sm-4" id="SPO2 Statistics">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">SPO2 Statistics</div>
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip4"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (95 ~ 100 %)</p><p>- Abnormal (<95 / >100 %)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn4"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn4"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('SPO2');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfSp02"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('SPO2','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvSp02"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('SPO2','csv');"
                                                                href="#">Download CSV</a>
                                                                <a class="dropdown-item" id="dwnldPngSp02"
                                                                style="display: none;"
                                                                onclick="downloadDivAsImage('SPO2 Statistics')"
                                                                href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart4" class="chartContainer"></div>
                                            <div id="donutChart4-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chart8 col-sm-4" id="BMI Statistics">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">BMI Statistics</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip5"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Underweight(<18.5 kg/m<sup>2</sup>)</p><p>- Normal(18.5~24 kg/m<sup>2</sup>)</p><p>- Overweight(25~29 kg/m<sup>2</sup>)</p><p>- Obese(>=30 kg/m<sup>2</sup>)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn5"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn5"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BMI');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBmi"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BMI','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBmi"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BMI','csv');"
                                                                href="#">Download CSV</a>
                                                                <a class="dropdown-item" id="dwnldPngBmi"
                                                                     style="display: none;"
                                                                onclick="downloadDivAsImage('BMI Statistics')"
                                                                href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart5" class="chartContainer"></div>
                                            <div id="donutChart5-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chart9 col-sm-4" id="Blood Glucose Statistics">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Blood Glucose Statistics</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip6"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>-Lowsugar (1~70 mg/dL)</p><p>-Normal (71~100 mg/dL)</p><p>- Prediabetes (101-125 mg/dL)</p><p>- Diabetes (>126 mg/dL)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn6"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu list-right"
                                                            aria-labelledby="dropdownMenuBtn6"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BG');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBg"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BG','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBg"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BG','csv');"
                                                                href="#">Download CSV</a>
                                                                <a class="dropdown-item" id="dwnldPngBg"
                                                                style="display: none;"
                                                                onclick="downloadDivAsImage('Blood Glucose Statistics')"
                                                                href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="donutChart6" class="chartContainer"></div>
                                            <div id="donutChart6-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="chart10 col-sm-4" id="SPO2 Gender Wise">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">SPO2 Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip10"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Normal (95 ~ 100 %)</p><p>- Abnormal (<95 / >100 %)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn10"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn10"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('SPO2');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfSp02Gender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('SPO2','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvSp02Gender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('SPO2','csv');"
                                                                href="#">Download CSV</a>
                                                                <a class="dropdown-item" id="dwnldPngSp02Gender"
                                                                 style="display: none;"
                                                                onclick="downloadDivAsImage('SPO2 Gender Wise')"
                                                                href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart4" class="chartContainer"></div>
                                            <div id="barChart4-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chart11 col-sm-4" id="BMI Gender Wise">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">BMI Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip11"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>- Underweight(<18.5 kg/m<sup>2</sup>)</p><p>- Normal(18.5~24 kg/m<sup>2</sup>)</p><p>- Overweight(25~29 kg/m<sup>2</sup>)</p>
										<p>- Obese(>=30 kg/m<sup>2</sup>)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn11"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuBtn11"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BMI');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBmiGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BMI','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBmiGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BMI','csv');"
                                                                href="#">Download CSV</a>
                                                                 <a class="dropdown-item" id="dwnldPngBmiGender"
                                                                 style="display: none;"
                                                                 onclick="downloadDivAsImage('BMI Gender Wise')"
                                                                 href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart5" class="chartContainer"></div>
                                            <div id="barChart5-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chart12 col-sm-4" id="Blood Glucose Gender Wise">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">Blood Glucose Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip12"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>-Lowsugar (1~70 mg/dL)</p><p>-Normal (71~100 mg/dL)</p><p>- Prediabetes (101-125 mg/dL)</p><p>- Diabetes (>126 mg/dL)</p></div>">
                                                        </span>
                                                    </div>
                                                    <div class="stats-header-icon dropdown">
                                                        <span class="glyphicon glyphicon-option-vertical"
                                                            style="cursor:pointer;" id="dropdownMenuBtn12"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        </span>
                                                        <div class="dropdown-menu list-right"
                                                            aria-labelledby="dropdownMenuBtn12"
                                                            style="border: 1px solid black;">
                                                            <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#tableModalCenter"
                                                                onclick="getPatientStatsByVitalName('BG');"
                                                                href="#">View Details</a>
                                                            <a class="dropdown-item" id="dwnldPdfBgGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BG','pdf');"
                                                                href="#">Download PDF</a>
                                                            <a class="dropdown-item" id="dwnldCsvBgGender"
                                                                style="display: none;"
                                                                onclick="window.location.href = downloadStatsPdfOrCsv('BG','csv');"
                                                                href="#">Download CSV</a>
                                                                <a class="dropdown-item" id="dwnldPngBgGender"
                                                                style="display: none;"
                                                                onclick="downloadDivAsImage('Blood Glucose Gender Wise')"
                                                                href="#">Download PNG</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart6" class="chartContainer"></div>
                                            <div id="barChart6-ND" class="emptyChart" style="display:none"> No Data!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="chart13 col-sm-6">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">ECG Statistics</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip13"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>NIRF - No irregular rhythm found</p><p>SLFB - Suspected a little fast beat</p><p>SFB - Suspected fast beat</p><p>SSRFB - Suspected short run of fast beat</p><p>SLSB - Suspected a little slow beat</p><p>SSB - Suspected slow beat</p><p>SIBI - Suspected irregular beat interval</p><p>SFBWSBI - Suspected fast beat with short beat intervel</p><p>SSBWSB - Suspected slow beat with short beat</p><p>SSBWIBI - Suspected slow beat with ireegular beat interval</p><p>SSBI - Suspected short beat interval</p><p>SFBWBW - Suspected fast beat with baseline wander</p><p>SSBWBW - Suspected slow beat with baseline wander</p><p>SSBIWBW - Suspected short beat interval with baseline wander</p><p>SIBIWBW - Suspected irregular beat interval with baseline wander</p></div>">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="pieChart1" class="chartContainer"></div>
                                            <div id="pieChart1-ND" class="emptyChart" style="display:none;height:268">
                                                No Data!</div>
                                        </div>
                                    </div>

                                    <div class="chart14 col-sm-6">
                                        <div class="box-shadow-card">
                                            <div class="stats-chart-box">
                                                <div class="stats-header-box">
                                                    <div class="stats-header">ECG Gender Wise</div>
                                                    <!-- <div class="glyphicon glyphicon-print stats-header-icon"></div> -->
                                                    <div class="stats-header-icon">
                                                        <span class="glyphicon glyphicon-info-sign" id="toolTip14"
                                                            data-placement="left" data-toggle="tooltip" data-html="true"
                                                            title="<div class='tooltipTitle'><p>NIRF - No irregular rhythm found</p><p>SLFB - Suspected a little fast beat</p><p>SFB - Suspected fast beat</p><p>SSRFB - Suspected short run of fast beat</p><p>SLSB - Suspected a little slow beat</p><p>SSB - Suspected slow beat</p><p>SIBI - Suspected irregular beat interval</p><p>SFBWSBI - Suspected fast beat with short beat intervel</p><p>SSBWSB - Suspected slow beat with short beat</p><p>SSBWIBI - Suspected slow beat with ireegular beat interval</p><p>SSBI - Suspected short beat interval</p><p>SFBWBW - Suspected fast beat with baseline wander</p><p>SSBWBW - Suspected slow beat with baseline wander</p><p>SSBIWBW - Suspected short beat interval with baseline wander</p><p>SIBIWBW - Suspected irregular beat interval with baseline wander</p></div>">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="barChart7" class="chartContainer"></div>
                                            <div id="barChart7-ND" class="emptyChart" style="display:none;height:268">
                                                No Data!</div>
                                        </div>
                                    </div>

                                </div>
                                   </div>

                                <div style="margin:60px;"></div>

                            </div>
                        </div>
                    </div>

                    <!-- ************** Date picker Modal for Statistics Custom button **************** -->
                    <div id="statsModal" class="modal fade" role="dialog" data-backdrop="false">
                        <div class="modal-dialog modal-sm">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header" style="border-bottom: 3px solid #3c8dbc;">
                                    <button id="btnSubmit" type="button" class="close"
                                        data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Select Date</h4>
                                </div>
                                <div class="modal-body">

                                    <label>Start : </label>
                                    <div id="datepicker3" class="input-group date" data-date-format="dd/mm/yyyy">
                                        <input id="datepicker3Val" class="form-control" type="text" readonly /> <span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-calendar"></i></span>
                                    </div>

                                    <label>End : </label>
                                    <div id="datepicker4" class="input-group date" data-date-format="dd/mm/yyyy">
                                        <input id="datepicker4Val" class="form-control" type="text" readonly /> <span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-calendar"></i></span>
                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" style="background-color:#3c8dbc; color:white"
                                        class="btn btn-default" onClick="getPatientStats('d3');">Submit</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!--------------------------------------------------- Patient List content --------------------------------------------->
            <div id="Paris" class="tabcontent">
                <div class="content-wrapper" style="margin-left: 0px !important;">

                    <p id="normalPatEmpty"></p>
                    <div class="col-md-12" style=" width: 100%; padding-bottom: 30px; display: inline-flex;">
                        <div class="input-group" style="margin-top: 10px;">
                            <input type="text" id="searchedInput" class="form-control"
                                placeholder="Search By Name/UHID">
                            <span class="input-group-btn">
                                <button class="btn btn-search" type="button"
                                    style="background-color: #3c8dbc; color: white;" onclick="searchInput();">
                                    <i class="fa fa-search fa-fw"></i> Search
                                </button>
                            </span>
                        </div>

                        <div class=""></div>


                        <div class="input-group" style="margin-top: 10px; padding-left: 40px;">
                            <div class="input-group-addon" style="background-color: #3c8dbc">
                                <i style="color: white" class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control clearAll"  autocomplete="off" id="birthDat" name="dateOfBirth"
                                placeholder="Date" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                            </input> <span class="input-group-btn">
                                <button class="btn btn-search" type="button"
                                    style="background-color: #3c8dbc; color: white;" onclick="searchForDate();">
                                    <i class="fa fa-search fa-fw"></i> Search
                                </button>
                            </span>
                        </div>




                    </div>
                    <!-- Main content -->
                    <section class="content">

                        <div class="row">

                            <div class="col-md-12">
                                <div class="nav-tabs-custom" style="min-height: 535px !important;">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a class="highLId" id="patConId" href="#history"
                                                onclick="initializePatientList()" data-toggle="tab">Normal</a></li>
                                        <li class="" ><a class="highLId" onclick="initializePatientListAbnormal();"
                                                id="docConId" href="#myList" data-toggle="tab">Abnormal</a></li>
                                    </ul>
                                    <div class="tab-content" style="overflow-y:auto;min-height:500px;">
                                        <div class="active tab-pane" id="history">
                                            <div class="box-body">

                                                <div>
                                                    <div>
                                                      <label for="rows-per-page">Rows per Page:</label>
                                                      <select id="rows-per-page">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                          <option value="100">100</option>
                                                      </select>
                                                    </div>
                                                  </div>


                                                <table class="table table-bordered table-striped" id="orderNormalTable">
                                                    <thead>
                                                        <tr style="height: 33px !important;">

                                                            <th style="text-align:left;width:100px;" >Patient Name</th>
                                                            <th style="text-align:left;width:100px;" >UHID</th>
                                                            <th style="text-align:left;width:100px;" >Visit Date</th>
                                                            <th style="text-align:left;width:50px;">Age/Gender</th>
                                                            <th style="text-align:left;width:100px;" >Blood Group</th>

                                                            <th style="text-align:left;width:100px;"scope="col">City/Village</th>
                                                            <th style="text-align:left;width:50px;"scope="col">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="orderNormalPatDetails">
                                                    </tbody>
                                                </table>

                                                <div>
                                                    <div>
                                                        <p id="data-count"></p>

                                                      <p id="total-count"></p>
                                                    </div>
                                                    <div class="rows-per-page-container">
                                                        <div id="pagination-container"></div>
                                                    </div>
                                                  </div>
                                                <div class="col-md-12 text-center" id="orderNormalNoList"  <p style="padding-top: 10px;padding-bottom: 10px;border: 1px solid #f4f4f4;">
                                                  No Patients</p>
                                                </div>

                                                <div id="normal-patient-table-spinner"
                                                style="position: fixed; left: 50%; top: 40%; width: 120px; height: 100%; z-index: 9999;display:none">
                                                <img style="" src="resources/images/icons/loading.gif"
                                                    alt="loading" />
                                                <br />
                                            </div>


                                            </div><!-- /.box-body -->
                                        </div><!-- /.tab-pane -->

                                        <div class="tab-pane" id="myList">

<div class="box-body">

    <div>
        <div>
          <label for="rows-per-page-abnormal">Rows per Page:</label>
          <select id="rows-per-page-abnormal">
           <option value="10">10</option>
           <option value="25">25</option>
           <option value="50">50</option>
           <option value="100">100</option>
          </select>
        </div>
      </div>

        <table id="abnormalPatientsList" class="table table-bordered table-striped verticle-middle table-responsive-sm">
            <thead>

                <tr style="height: 33px !important;">

                    <th style="text-align:left;width:100px" scope="col">Patient Name</th>
                    <th style="text-align:left;width:100px;"scope="col">UHID</th>
                    <th style="text-align:left;width:50px;" scope="col">Visit Date</th>
                    <th style="text-align:left;width:50px"scope="col">Age/Gender</th>
                    <th style="text-align:left;width:100px;"scope="col">Blood Group</th>
                    <th style="text-align:left;width:100px;"scope="col">City/Village</th>
                    <th style="text-align:left;width:50px;"scope="col">Action</th>
                </tr>
            </thead>
                <tbody id="abnormalPatientlistTable">

                </tbody>
        </table>

        <div>
        <div>
            <p id="data-count-abnormal"></p>

          <p id="total-count-abnormal"></p>
        </div>
        <div class="rows-per-page-container">
            <div id="pagination-container-abnormal"></div>
        </div>
      </div>
        <div class="col-md-12 text-center" id="patientsorderNoList" style="display:none;padding-left: 0px;padding-right: 0px;">
        <p style="padding-top: 10px;padding-bottom: 10px;border: 1px solid #f4f4f4;">
            No Patients
        </p>
        </div>
        <div id="abNormal-Patient-table-spinner" style="position: fixed; left: 50%; top: 40%; width: 120px; height: 100%; z-index: 9999;display:none">
        <img style="" src="resources/images/icons/loading.gif" alt="loading" />
            <br />
        </div>
     </div><!-- /.box-body -->
                                        </div>
                                    </div>
                                    <!-- </div> --><!-- /.tab-content -->
                                </div><!-- /.nav-tabs-custom -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->

                    </section><!-- /.content -->
                </div><!-- /.content-wrapper -->

<!----RESET PASSWORD MODAL---->
<div class="modal fade" id="reset-password-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Reset Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="left: 50px;margin-top: -25px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="resetPaswd-modal" autocomplete="off">
                    <div class="card-body" id="changePaswdUser">

                    <div class="col-xs-12" style="padding-left: 10px; padding-right: 0px;">
    <div style="width: 100%; color: black" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input type="password" id="inviteNewPasswordChk" placeholder="New Password" class="form-control" maxlength="64">
        <span style="color:red" class="required">min 8 characters and (A-Z, a-z, 0-9, any special characters)</span>
        <span id="ShowHideNewPassword" class="glyphicon glyphicon-eye-close pull-right"
            style="margin-top: -6%; cursor: pointer; margin-right: 2%; color: black; font-size: 15px;"
            onclick="ShowHidePassword('inviteNewPasswordChk', 'ShowHideNewPassword');"></span>
    </div>
</div>

<div class="col-xs-12" style="padding-right: 0px;padding-left: 10px;">
    <div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 100%; color: black">
            <input type="password" id="inviteConfirmPasswordChk" placeholder="Confirm Password" class="form-control"  maxlength="64">
            <span id="ShowHideConfirmPassword" class="glyphicon glyphicon-eye-close pull-right"
                style="margin-top: -6%; cursor: pointer; margin-right: 2%; color: black; font-size: 15px;"
                onclick="ShowHidePassword('inviteConfirmPasswordChk', 'ShowHideConfirmPassword');"></span>
        </div>
    </div>
</div>

</div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button id="resetPaswdUserBtn" onclick="resetPasswordChk();" type="button" class="btn btn-primary">
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"
                            style="display:none; text-align: center;" id="resetPaswdSpinner"></span>
                            Reset
                        </button>

                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<!-----------------ALERT MODAL-------------------->

<div class="modal fade" id="reset-password-alert-modal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>

            </div>
            <div class="modal-body">
                <form action="#" id="resetPaswd-alert-modal" autocomplete="off">
                    <div class="card-body" id="changePaswdUser">

                        <h4>Password Reset Completed, Please login again by entering new password</h4>


                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button id="resetPaswdOkBtn"  type="button" class="btn btn-success">
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"
                            style="display:none; text-align: center;" id="resetPaswdSpinner"></span>
                            OK
                        </button>

                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

            </div>

            <script type="text/javascript">

                function resetPassword(){
                    $("#reset-password-modal").modal('show')
                }

<!---RESET PASSWORD--->

var DW_JWT_TOKEN = localStorage.getItem('DW_JWT_TOKEN');
        var loggedInUserFullName = localStorage.getItem("loggedInUserFullName");
         var USER_NAME =  localStorage.getItem('loginUsername');
        var ROLL_NAME = localStorage.getItem("roleName");
        var RESET_PASSWORD = localStorage.getItem("RESET_PASSWORD");
        //RESET PASSWORD

function resetPasswordChk() {
            let NEW_PASSWORD = $("#inviteNewPasswordChk").val();
            let CONFIRM_PASSWORD = $("#inviteConfirmPasswordChk").val();
var hasNumber = /\d/.test(NEW_PASSWORD);
    var hasUpperCase = /[A-Z]/.test(NEW_PASSWORD);
    var hasLowerCase = /[a-z]/.test(NEW_PASSWORD);
    var hasSpecialChar = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/.test(NEW_PASSWORD);
    var hasValidLength = NEW_PASSWORD.length >= 8;

var jsonData = JSON.stringify({
    userName: USER_NAME,
	newPassword: NEW_PASSWORD,
    confirmNewPassword: CONFIRM_PASSWORD,
    resetPassword: false
	});


 console.log("reset password")
    console.log("reset password json",jsonData)

    if($("#inviteNewPasswordChk").val() == ''){
        toastr.error("New Password field is Empty!");
        return;
    }else if(!hasNumber){
             toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
             return;
         }else if(!hasUpperCase){
          toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
          return;
         }else if(!hasLowerCase){
          toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
          return;
          }else if(!hasSpecialChar){
          toastr.error("Password must contain minimum 8 characters with one lower case,one upper case alphabet, one special character & one number!");
          return;
          }
          else if(!hasValidLength){
          toastr.error("Password must contain atleast 8 characters!");
          return;
          }
    if($("#inviteConfirmPasswordChk").val() == ''){
        toastr.error("Confirm Password field is Empty!")
        return;
    }




//$('#resetPasswdSpinner').show();
    //API CALL

    $.ajax({
        type:'POST',
        contentType: "application/json",
        url: serverPath + "/resetAdminPassword",
        data:jsonData,
        headers: { "Authorization": 'Bearer ' + DW_JWT_TOKEN },
        success: function(result) {


            let data = result.error;
            console.log('success: ',data);

            if(result.error == false){
                console.log("success block",ROLL_NAME, RESET_PASSWORD)
                $('#reset-password-alert-modal').modal('show')

                $('#resetPaswdOkBtn').click(function() {
                    // Redirect to the login page
                    if(ROLL_NAME == 'Doctor'){

                    window.location.href = "logout";

                    }else if(ROLL_NAME == 'Client Admin'){

                    window.location.href = "logout";

                    }else if(ROLL_NAME == 'Nurse'){

                    window.location.href = "logout";
                }
$('#felix-widget-button-btn-resetPaswd').prop('disabled', false);
                  });
                   $('#resetPaswdCancelBtn').click(function() {
                    // Redirect to the login page
                    var url = "./resetPassword ";
                    window.location.href= url;
                    $('#felix-widget-button-btn-resetPaswd').prop('disabled', false);
                  });
                //alert("Password Reset Completed, Please login again ")


            }else if(result.error == true){
            //$('#resetPasswdSpinner').hide();
                console.log("error block")
                toastr.error(result.message);
                var url = "./login ";
                window.location.href= url;
            }




            //console.log("Appointments and packages",appointments,packages)


        },
        error: function (xhr, status, error) {
            console.log("change password-->3 : ",error);

            $('#felix-widget-button-btn-resetPaswd').prop('disabled', false);
            //$('#resetPasswdSpinner').hide();
            if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                if (xhr.responseJSON.code !== 200) {
                    (async () => {
                        toastr.error(xhr.responseJSON.message);

                    })()
                } else {
                    toastr.error(xhr.responseJSON.message);
                }
            } else {
                toastr.error('Failed add user.Internet Disconnected/Server error');
            }

        }
    });

}
$(function () {
    $("#reset-password-modal").on("hidden.bs.modal", function (e) {
        console.log("Modal hidden");
        $("#resetPaswd-modal").trigger("reset");

        // Reset the eye icon to the closed state
        $("#ShowHideNewPassword").removeClass("glyphicon-eye-open").addClass("glyphicon-eye-close");
        $("#ShowHideConfirmPassword").removeClass("glyphicon-eye-open").addClass("glyphicon-eye-close");

        // Reset password input fields to type "password"
        $("#inviteNewPasswordChk").attr("type", "password");
        $("#inviteConfirmPasswordChk").attr("type", "password");
    });
});
//SHOW HIDE PASSWORD

function ShowHidePassword(inputId, iconId) {
        var inputElement = $('#' + inputId);
        var iconElement = $('#' + iconId);

        if (inputElement.attr('type') === 'password') {
            inputElement.attr('type', 'text');
            iconElement.removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
        } else {
            inputElement.attr('type', 'password');
            iconElement.removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
        }
    }
$("#inviteConfirmPassword").keyup(function (event) {
                if (event.keyCode === 13) {
                    $("#felix-widget-button-btn-resetPaswd").click();
                }
            });



// VIEW NORMAL PATIENT LIST
let currentPageIndex = 0;
let normalPatientListQJson = { pagination: {} };

function initializePatientList() {
 // Reset the current page index to 0
 currentPageIndex = 0;
  normalPatientListQJson.pagination.currentPage = currentPageIndex;
  console.log("INSIDE initializePatientList()", currentPageIndex);
  // Call the main function to fetch and display the patient list
  viewNormalPatientList(currentPageIndex);
}

function viewNormalPatientList(page) {
  console.log("INSIDE viewNormalPatientList()",page)
const rowsPerPageSelect = document.getElementById('rows-per-page');
const selectedValue = parseInt(rowsPerPageSelect.value);
currentPageIndex = page
  $("#normal-patient-table-spinner").show();
  $('#normal-patient-table-spinner').css({ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '200px' });
  ACTIVE_TAB = "NORMAL";


  $("#orderNormalPatDetails").show();
  $("#orderNormalPatDetails").html('');

  let clientId = localStorage.getItem('clientId');
  let NormalPatientURL = serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId") + "&size=" + selectedValue + "&page=" + page +"&filterByAbnormal=0" + "&sortDirection=DESC";
  if (kitId) {
    NormalPatientURL += "&kitSerialNumber=" + kitId;
  }
  if (SEARCH_PARAM_PAT !== "") {
    NormalPatientURL += "&searchParameter=" + SEARCH_PARAM_PAT;
  }
  console.log("SEARCH_DATE_PAT-> ", SEARCH_DATE_PAT);
  if (SEARCH_DATE_PAT !== "") {
    var values = SEARCH_DATE_PAT.split("/");
    console.log("values", SEARCH_DATE_PAT);
    var fromDate = new Date(values[2], (values[1] - 1), values[0]);
    fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),
      0, 0, 0, fromDate.getMilliseconds());
    console.log("from date", fromDate);
    var toDate = new Date(values[2], (values[1] - 1), values[0]);
    toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),
      23, 59, 59, toDate.getMilliseconds());
    console.log("todate", toDate);

    NormalPatientURL += "&startDate=" + fromDate + "&endDate=" + toDate;
  }

  $.ajax({
    type: "GET",
    url: NormalPatientURL,
    headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
    success: function (data) {
      $("#normal-patient-table-spinner").hide();

      if (data.code == '200') {
        if (data.content && data.content.patientList) {
          console.log("DATA OF SUCCESS BLOCK");

          const receivedData = data.content.patientList;
          // Update the normalPatientListQJson object with the received data
          normalPatientListQJson.patientList = receivedData;
          normalPatientListQJson.pagination = data.content.pagination;


        }
      }
      console.log("Normal patients list", normalPatientListQJson.pagination);

      $("#orderNormalNoList").hide();
      $("#orderNormalTable").show();


      normalPatientListQJson.pagination.pageLimit = selectedValue;
      normalPatientListQJson.pagination.totalPages = Math.ceil(normalPatientListQJson.pagination.count / selectedValue);

      // Reload the current page to reflect the new rows per page
      console.log("--------------------------------->",normalPatientListQJson)
      generatePaginationButtons(normalPatientListQJson.pagination.totalPages, page);
      viewNormalPatientListPage(page, normalPatientListQJson);
      console.log("PAGINATION CURRENT PAGE", currentPageIndex);
    }
  });
}

// Event listener for rows per page change
const rowsPerPageSelect = document.getElementById('rows-per-page');
rowsPerPageSelect.addEventListener('change', handleRowsPerPageChange);

function handleRowsPerPageChange() {
  // Reset the current page index to 0
  currentPageIndex = 0;
  // Call the main function to update the patient list with the new rows per page value
  viewNormalPatientList(currentPageIndex);
}

function viewNormalPatientListPage(page, item) {
  console.log("INSIDE WHEN NEXT BUTTON CLICKED viewNormalPatientListPage------->",page,item);
  // Update the current page in the response object
  currentPageIndex = page;
  // Check if item is defined and has the necessary properties
  if (!item || !item.patientList || !item.pagination) {
    console.error('Invalid item data');
    return;
  }
  // Calculate the start and end index for the current page
  const startIndex = page * item.pagination.pageLimit;
  const endIndex = Math.min(startIndex + item.pagination.pageLimit, item.pagination.count);


  // Clear the existing data list
  const dataList = document.getElementById('orderNormalPatDetails');
  dataList.innerHTML = '';

  // Generate data table HTML for the current page
  if (item.patientList && item.patientList.length > 0) {
    let Patient_List = [];
    for (Patient_List of item.patientList) {

      //console.log('Patient List:', Patient_List);

      var user = Patient_List;


 // DISPLAY PATIENT NAME
let patName = "";
if (user && user.patientFullName && user.patientFullName !== "") {
  patName = user.patientFullName;
} else {
  var fname = user && user.patientFirstName && user.patientFirstName !== "" ? user.patientFirstName : "";
  var lastname = user && user.patientLastName && user.patientLastName !== "" ? user.patientLastName : "";
  patName = fname + " " + lastname;
}

// CREATED DATE
let createdDate = "";
if (user && user.createdAt) {
  var m = new Date(user.createdAt);
  createdDate = m.getUTCDate() + "/" + (m.getUTCMonth() + 1) + "/" + m.getUTCFullYear();
}

     // VILLAGE CITY
let villageCity = "";
if (user && user.city) villageCity = user.city + "/";
if (user && user.village) villageCity += user.village;


      // BLOOD GROUP

let bloodGrp = user && user.bloodGroup ? user.bloodGroup : '-';

let ageGender = "";
if (user && user.age) ageGender = user.age + "/";
if (user && user.gender) ageGender += user.gender;
    // UHID
let uhid = user && user.uhid ? user.uhid : '-';


      // Create a new row for each patient
      const row = document.createElement('tr');

      // Create table cells and set their content
      const nameCell = document.createElement('td');
      nameCell.textContent = patName;

      const uhidCell = document.createElement('td');
      uhidCell.textContent = uhid;

      const visitDateCell = document.createElement('td');
      visitDateCell.textContent = createdDate ? createdDate : '-';

      const ageGenderCell = document.createElement('td');
      ageGenderCell.textContent = ageGender;

      const bloodGroupCell = document.createElement('td');
      bloodGroupCell.textContent = bloodGrp;

      const cityVillageCell = document.createElement('td');
      cityVillageCell.textContent = villageCity;

      const actionCell = document.createElement('td');
    const button = createViewButton(user.id, user.userId);


      // Add the cells to the row
      actionCell.appendChild(button);
      row.appendChild(nameCell);
      row.appendChild(uhidCell);
      row.appendChild(visitDateCell);
      row.appendChild(ageGenderCell);
      row.appendChild(bloodGroupCell);
      row.appendChild(cityVillageCell);
      row.appendChild(actionCell);

      // Add the row to the data list
      dataList.appendChild(row);

}
  }else {
       // If syncTransactions is empty, display "No data available" message
       const row = document.createElement('tr');
       const noDataCell = document.createElement('td');
       noDataCell.textContent = 'No data available';
       noDataCell.colSpan = 5; // Set the colspan to the number of columns in the table
       row.appendChild(noDataCell);
       dataList.appendChild(row);
     }
  // Update the data count
    const start = startIndex + 1;
    const end = Math.min(startIndex + item.pagination.pageLimit, item.pagination.count);
    console.log("TOTAL ENTRIES", start, end, item.pagination.count);

    const dataCountElement = document.getElementById('data-count');
    dataCountElement.innerText = "Showing " + start + " to " + end + " of " + item.pagination.count + " entries";
  // Generate pagination buttons
  //generatePaginationButtons(item.pagination.totalPages, page);
}

function generatePaginationButtons(totalPages, currentPage) {
  const paginationContainer = document.getElementById('pagination-container');
  paginationContainer.innerHTML = '';

  const maxPageButtons = 6; // Maximum number of page buttons to display
  let startPage = Math.max(currentPage - 1, 0);
  let endPage = Math.min(startPage + maxPageButtons, totalPages - 1);
  startPage = Math.max(endPage - maxPageButtons + 1, 0);

  // Create the previous button
  const previousButton = document.createElement('button');
previousButton.textContent = 'Previous';
previousButton.classList.add('primary-button');
previousButton.style.backgroundColor = '#007bff';
previousButton.style.color = '#fff';
  if (currentPage === 0) {
    previousButton.disabled = true;
  }
  previousButton.onclick = function () {

    currentPage = Math.max(currentPage - 1, 0);
    console.log("previous button clicked",currentPage)
    normalPatientListQJson.pagination.currentPage = currentPage;
    viewNormalPatientList(currentPage)
    //viewNormalPatientListPage(currentPage - 1, normalPatientListQJson);
  };
  paginationContainer.appendChild(previousButton);

  // Create the page number buttons
  if (startPage > 0) {
    const firstPageButton = document.createElement('button');
    firstPageButton.textContent = '1';
    firstPageButton.classList.add('pagination-button');
    if (currentPage === 0) {
            firstPageButton.disabled = true;
       }
    firstPageButton.onclick = function () {

      currentPage = 0;
      console.log("First page number button clicked",currentPage)
      normalPatientListQJson.pagination.currentPage = currentPage;
      viewNormalPatientList(currentPage)
      //viewNormalPatientListPage(0, normalPatientListQJson);
    };
    paginationContainer.appendChild(firstPageButton);
  }
  for (let i = startPage; i <= endPage; i++) {
    const button = document.createElement('button');
    button.textContent = i + 1;
    button.classList.add('pagination-button');
    if (i === currentPage) {
      button.disabled = true;
    }
    button.onclick = function () {

      currentPage = i;
      console.log("page number button clicked",currentPage)
      normalPatientListQJson.pagination.totalPages = currentPage;
      viewNormalPatientList(currentPage)
    };
    paginationContainer.appendChild(button);
  }

  // Create the next button
  const nextButton = document.createElement('button');
nextButton.textContent = 'Next';
nextButton.classList.add('primary-button');
nextButton.style.backgroundColor = '#007bff';
nextButton.style.color = '#fff';
  if (currentPage === totalPages - 1) {
    nextButton.disabled = true;
  }
  nextButton.onclick = function () {
    currentPage++;
    if (currentPage >= normalPatientListQJson.pagination.totalPages) {
        // If it exceeds, set it to the last page

        currentPage = normalPatientListQJson.pagination.totalPages - 1;
    }
    viewNormalPatientList(currentPage);
    console.log("next button clicked",currentPage)
  };
  paginationContainer.appendChild(nextButton);
}

//VIEW BUTTON IN TABLE

function createViewButton(patientId, patUId) {
  const button = document.createElement('button');
  button.textContent = 'View';
  button.classList.add('primary-button');
  button.style.backgroundColor = '#007bff';
  button.style.color = '#fff';
  button.style.border = 'none';
  button.style.padding = '8px 16px';
  button.style.borderRadius = '4px';
  button.style.cursor = 'pointer';
  button.style.transition = 'background-color 0.3s ease';

  // Add hover effect
  button.addEventListener('mouseover', function() {
    button.style.backgroundColor = '#0056b3';
  });

  button.addEventListener('mouseout', function() {
    button.style.backgroundColor = '#007bff';
  });

  button.onclick = function () {
    openDashboard(patientId, patUId);
  };

  return button;
}

// Function to increment the CURRENT_PAGE_INDEX
var i = 0;
function incrementPageIndex(totalPages) {
 //console.log("inside increment function^^^^^^^^^^^^^^",totalPages)


 if (i < totalPages){
 //console.log("before increment=====>",i)
 i++;
 //console.log("after increment=====>",i)
 } else if(i = totalPages){
 i = 0
 }

 viewNormalPatientList(i);
}

// Function to decrement the CURRENT_PAGE_INDEX
function decrementPageIndex(page) {
  if (i > 0) {
    //console.log("before decrement=====>", i);
    i--;
    //console.log("after decrement=====>", i);
  } else if(i = page){
   i = page
 }
 viewNormalPatientList(i);
}



// VIEW ABNORMAL PATIENT LIST
let currentPageIndexAbnormal = 0;
let abnormalPatientListQJson = { pagination: {} };

function initializePatientListAbnormal() {
  // Reset the current page index to 0
  currentPageIndex = 0;
  normalPatientListQJson.pagination.currentPage = currentPageIndex;
  console.log("INSIDE initializePatientList()", currentPageIndex);
  // Call the main function to fetch and display the patient list

  viewAbnormalPatientList(currentPageIndex);
}

function viewAbnormalPatientList(page) {
 const rowsPerPageSelectAbnormal = document.getElementById('rows-per-page-abnormal');
 const selectedValue = parseInt(rowsPerPageSelectAbnormal.value);
    ACTIVE_TAB = "ABNORMAL";
    $("#normalPatEmpty").hide();
    $("#abNormal-Patient-table-spinner").show();
    $('#abNormal-Patient-table-spinner').css({ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '200px' });
    console.log("Inside viewAbnormalPatientList()")


    let clientId = localStorage.getItem('clientId');
    //let userId = localStorage.getItem("userId");localStorage.getItem("loggedInUserId")

    let abNormalPatientURL = serverPath + "/getPatientListByClient?clientId=" + localStorage.getItem("clientId")  + "&size=" + selectedValue + "&page=" + page + "&filterByAbnormal=1" + "&sortDirection=DESC";
    if (kitId) {
        abNormalPatientURL += "&kitSerialNumber=" + kitId;
    }
    if (SEARCH_PARAM_PAT !== "") {
        abNormalPatientURL += "&searchParameter=" + SEARCH_PARAM_PAT;
    }
    console.log("SEARCH_DATE_PAT-> ", SEARCH_DATE_PAT);
    if (SEARCH_DATE_PAT !== "") {
        var values = SEARCH_DATE_PAT.split("/");
        var fromDate = new Date(values[2], (values[1] - 1), values[0]);
                                        fromDate = Date.UTC(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate(),
                                        0, 0, 0, fromDate.getMilliseconds());

                                        var toDate = new Date(values[2], (values[1] - 1), values[0]);
                                        toDate = Date.UTC(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(),
                                        23, 59, 59, toDate.getMilliseconds());

        abNormalPatientURL += "&startDate=" + fromDate + "&endDate=" + toDate;
    }


  $.ajax({
    type: "GET",
    url: abNormalPatientURL,
    headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
    success: function (data) {
      $("#abNormal-Patient-table-spinner").hide();

      if (data.code == '200') {
        if (data.content && data.content.patientList) {
          console.log("DATA OF SUCCESS BLOCK");
          const receivedData = data.content.patientList;
          abnormalPatientListQJson.patientList = receivedData;
          abnormalPatientListQJson.pagination = data.content.pagination;
        }
      }
      console.log("Normal patients list", abnormalPatientListQJson.pagination);

      $("#patientsorderNoList").hide();
      $("#abnormalPatientsList").show();


      abnormalPatientListQJson.pagination.pageLimit = selectedValue;
      abnormalPatientListQJson.pagination.totalPages = Math.ceil(abnormalPatientListQJson.pagination.count / selectedValue);

      // Reload the current page to reflect the new rows per page


      //generatePaginationButtonsAbnormal(normalPatientListQJson.pagination.totalPages, page);
      viewabNormalPatientListPage(page, abnormalPatientListQJson);
      console.log("PAGINATION CURRENT PAGE", currentPageIndexAbnormal);
    }
  });
}

// Event listener for rows per page change
const rowsPerPageSelectAbnormal = document.getElementById('rows-per-page-abnormal');
rowsPerPageSelectAbnormal.addEventListener('change', handleRowsPerPageChangeAbnormal);

function handleRowsPerPageChangeAbnormal() {
  // Reset the current page index to 0
  currentPageIndexAbnormal = 0;
  // Call the main function to update the patient list with the new rows per page value
  viewAbnormalPatientList(currentPageIndex);


}

function viewabNormalPatientListPage(page, item) {
  console.log("INSIDE WHEN NEXT BUTTON CLICKED viewabNormalPatientListPage------->", page);
  // Update the current page in the response object
  currentPageIndexAbnormal = page;
  // Check if item is defined and has the necessary properties
  if (!item || !item.patientList || !item.pagination) {
    console.error('Invalid item data');
    return;
  }
  // Calculate the start and end index for the current page
  const startIndex = page * item.pagination.pageLimit;
  const endIndex = Math.min(startIndex + item.pagination.pageLimit, item.pagination.count);

//   console.log('startIndex:', startIndex);
//   console.log('endIndex:', endIndex);
//   console.log('item.patientList:', item.patientList);

  // Clear the existing data list
  const dataList = document.getElementById('abnormalPatientlistTable');
  dataList.innerHTML = '';

  // Generate data table HTML for the current page
  if (item.patientList && item.patientList.length > 0) {
    let Patient_List = [];
    for (Patient_List of item.patientList) {

      if (i < item.patientList.length) {
      var user = Patient_List;
      //console.log('User:', user);

 // DISPLAY PATIENT NAME
let patName = "";
if (user && user.patientFullName && user.patientFullName !== "") {
  patName = user.patientFullName;
} else {
  var fname = user && user.patientFirstName && user.patientFirstName !== "" ? user.patientFirstName : "";
  var lastname = user && user.patientLastName && user.patientLastName !== "" ? user.patientLastName : "";
  patName = fname + " " + lastname;
}

// CREATED DATE
let createdDate = "";
if (user && user.createdAt) {
  var m = new Date(user.createdAt);
  createdDate = m.getUTCDate() + "/" + (m.getUTCMonth() + 1) + "/" + m.getUTCFullYear();
}

     // VILLAGE CITY
let villageCity = "";
if (user && user.city) villageCity = user.city + "/";
if (user && user.village) villageCity += user.village;


      // BLOOD GROUP

let bloodGrp = user && user.bloodGroup ? user.bloodGroup : '-';

let ageGender = "";
if (user && user.age) ageGender = user.age + "/";
if (user && user.gender) ageGender += user.gender;
    // UHID
let uhid = user && user.uhid ? user.uhid : '-';


      // Create a new row for each patient
      const row = document.createElement('tr');

      // Create table cells and set their content
      const nameCell = document.createElement('td');
      nameCell.textContent = patName;

      const uhidCell = document.createElement('td');
      uhidCell.textContent = uhid;

      const visitDateCell = document.createElement('td');
      visitDateCell.textContent = createdDate ? createdDate : '-';

      const ageGenderCell = document.createElement('td');
      ageGenderCell.textContent = ageGender;

      const bloodGroupCell = document.createElement('td');
      bloodGroupCell.textContent = bloodGrp;

      const cityVillageCell = document.createElement('td');
      cityVillageCell.textContent = villageCity;

      const actionCell = document.createElement('td');
    const button = createViewButtonAbnormal(user.id, user.userId);

      // Add the cells to the row
      actionCell.appendChild(button);
      row.appendChild(nameCell);
      row.appendChild(uhidCell);
      row.appendChild(visitDateCell);
      row.appendChild(ageGenderCell);
      row.appendChild(bloodGroupCell);
      row.appendChild(cityVillageCell);
      row.appendChild(actionCell);

      // Add the row to the data list
      dataList.appendChild(row);
    }else {
        // Handle the case when 'endIndex' exceeds the length of 'patientList'
      console.log('User:', 'No data');}
}
  }else {
       // If syncTransactions is empty, display "No data available" message
       const row = document.createElement('tr');
       const noDataCell = document.createElement('td');
       noDataCell.textContent = 'No data available';
       noDataCell.colSpan = 5; // Set the colspan to the number of columns in the table
       row.appendChild(noDataCell);
       dataList.appendChild(row);
     }
  // Update the data count
    const start = startIndex + 1;
    const end = Math.min(startIndex + item.pagination.pageLimit, item.pagination.count);
    console.log("TOTAL ENTRIES", start, end, item.pagination.count);

    const dataCountElement = document.getElementById('data-count-abnormal');
    dataCountElement.innerText = "Showing " + start + " to " + end + " of " + item.pagination.count + " entries";
  // Generate pagination buttons
  generatePaginationButtonsAbnormal(item.pagination.totalPages, page);
}

function generatePaginationButtonsAbnormal(totalPages, currentPage) {
      const paginationContainer = document.getElementById('pagination-container-abnormal');
      paginationContainer.innerHTML = '';

      const maxPageButtons = 6; // Maximum number of page buttons to display
      let startPage = Math.max(currentPage - 1, 0);
      let endPage = Math.min(startPage + maxPageButtons, totalPages - 1);
      startPage = Math.max(endPage - maxPageButtons + 1, 0);

      // Create the previous button
      const previousButton = document.createElement('button');
    previousButton.textContent = 'Previous';
    previousButton.classList.add('primary-button');
    previousButton.style.backgroundColor = '#007bff';
    previousButton.style.color = '#fff';
      if (currentPage === 0) {
        previousButton.disabled = true;
      }
      previousButton.onclick = function () {

        currentPage = Math.max(currentPage - 1, 0);
        console.log("previous button clicked",currentPage)
        abnormalPatientListQJson.pagination.currentPage = currentPage;
        viewAbnormalPatientList(currentPage)
        //viewNormalPatientListPage(currentPage - 1, normalPatientListQJson);
      };
      paginationContainer.appendChild(previousButton);

      // Create the page number buttons
      if (startPage > 0) {
        const firstPageButton = document.createElement('button');
        firstPageButton.textContent = '1';
        firstPageButton.classList.add('pagination-button-abnormal');
        if (currentPage === 0) {
                firstPageButton.disabled = true;
           }
        firstPageButton.onclick = function () {

          currentPage = 0;
          console.log("First page number button clicked",currentPage)
          abnormalPatientListQJson.pagination.currentPage = currentPage;
          viewAbnormalPatientList(currentPage)
          //viewNormalPatientListPage(0, normalPatientListQJson);
        };
        paginationContainer.appendChild(firstPageButton);
      }
      for (let i = startPage; i <= endPage; i++) {
        const button = document.createElement('button');
        button.textContent = i + 1;
        button.classList.add('pagination-button-abnormal');
        if (i === currentPage) {
          button.disabled = true;
        }
        button.onclick = function () {

          currentPage = i;
          console.log("page number button clicked",currentPage)
          abnormalPatientListQJson.pagination.totalPages = currentPage;
          viewAbnormalPatientList(currentPage)
        };
        paginationContainer.appendChild(button);
      }

      // Create the next button
      const nextButton = document.createElement('button');
    nextButton.textContent = 'Next';
    nextButton.classList.add('primary-button');
    nextButton.style.backgroundColor = '#007bff';
    nextButton.style.color = '#fff';
      if (currentPage === totalPages - 1) {
        nextButton.disabled = true;
      }
      nextButton.onclick = function () {
        currentPage++;
        if (currentPage >= abnormalPatientListQJson.pagination.totalPages) {
            // If it exceeds, set it to the last page

            currentPage = abnormalPatientListQJson.pagination.totalPages - 1;
        }
        viewAbnormalPatientList(currentPage);
        console.log("next button clicked",currentPage)
      };
      paginationContainer.appendChild(nextButton);
    }

//VIEW BUTTON IN TABLE

function createViewButtonAbnormal(patientId, patUId) {
  const button = document.createElement('button');
  button.textContent = 'View';
  button.classList.add('primary-button');
  button.style.backgroundColor = '#007bff';
  button.style.color = '#fff';
  button.style.border = 'none';
  button.style.padding = '8px 16px';
  button.style.borderRadius = '4px';
  button.style.cursor = 'pointer';
  button.style.transition = 'background-color 0.3s ease';

  // Add hover effect
  button.addEventListener('mouseover', function() {
    button.style.backgroundColor = '#0056b3';
  });

  button.addEventListener('mouseout', function() {
    button.style.backgroundColor = '#007bff';
  });

  button.onclick = function () {
    openDashboard(patientId, patUId);
  };

  return button;
}


// Function to increment the CURRENT_PAGE_INDEX
var i = 0;
function incrementAbnormalPageIndex(totalPages) {
 //console.log("inside increment function^^^^^^^^^^^^^^",totalPages)


 if (i < totalPages){
 //console.log("before increment=====>",i)
 i++;
 //console.log("after increment=====>",i)
 } else if(i = totalPages){
 i = 0
 }

 viewAbnormalPatientList(i);
}

// Function to decrement the CURRENT_PAGE_INDEX
function decrementAbonormalPageIndex(page) {
  if (i > 0) {
    //console.log("before decrement=====>", i);
    i--;
    //console.log("after decrement=====>", i);
  } else if(i = page){
   i = page
 }
 viewAbnormalPatientList(i);
}

function searchInput() {
     console.log("Search for input");
    SEARCH_PARAM_PAT = $("#searchedInput").val();
     if (ACTIVE_TAB === "NORMAL") {
    console.log("Search for input Normal tab");
    viewNormalPatientList(currentPageIndex);

     }else {
        console.log("Search for  Abnormal pat");
        viewAbnormalPatientList(currentPageIndex);
    }

    }


function searchForDate() {
                SEARCH_DATE_PAT = $("#birthDat").val();
                console.log("search date in text field",SEARCH_DATE_PAT);
                if (ACTIVE_TAB === "NORMAL") {
                    console.log("Search for input Normal tab");
                    viewNormalPatientList(currentPageIndex);

                } else {
                    console.log("Search for  Abnormal pat");
                    viewAbnormalPatientList(currentPageIndex);
                }

            }

            </script>
    </body>

    </html>