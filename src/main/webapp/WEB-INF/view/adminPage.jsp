<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ADMIN | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
   	<link href="resources/css/AdminLTE.css" rel="stylesheet" />
   	<link rel="stylesheet" href="resources/css/material.css" />
    <link href="resources/css/_all-skins.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="resources/css/toastr.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="resources/css/jquery-ui.css" />
    <link rel="stylesheet" href="resources/css/dataTables.bootstrap.css">
<link rel="stylesheet" href="resources/css/jquery.dataTables_themeroller.css">
<link rel="stylesheet" href="resources/css/bootstrap-datepicker.css"/>
    
    
    <!-- Bootstrap 3.3.5 -->
    <script src="resources/js/jQuery-2.1.4.min.js"></script>
   	<script src="resources/js/bootstrap.min.js"></script>
   	<script src="resources/js/fastclick.min.js"></script>
    <script src="resources/js/app.min.js"></script>
   	<script src="resources/js/demo.js"></script>
    <script defer src="resources/js/material.js"></script> 
	<script src="resources/js/bootstrap-select.js"></script>
    

	<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
    <script src="resources/js/bootstrap3-typeahead.js" 	type="application/javascript"></script> 
    <script src="resources/js/jquery.mask.js"></script>
    
     <script src="resources/js/toastr.min.js"></script>
     <script src="resources/js/customHTML.js"></script>
     
     <script src="resources/js/jquery.dataTables.min.js"></script>
     <script src="resources/js/dataTables.bootstrap.min.js"></script>
    <script defer src="resources/js/material.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="resources/js/bootstrap-datepicker.js" type="application/javascript"></script>
 	
 	<style>
 	 .navbar-t-centered {
      position: absolute;
left: 34%;
display: block;
text-align: center;
color: #fff;
top: 0px;
padding: 15px;
font-weight: 400;
font-size: 14px;

    }
    @media screen and (min-width:768px){
.navbar-t-centered {
        position: absolute;
        left: 38% ;
        display: block;
       /*  width: 160px; */
        text-align: center;
        color: #fff;
        top:0px;
        padding:15px;
        font-weight: 400;
        font-size:22px;

    }
    }
    
    @media screen and(max-width:600px){
    
    .selectC{
    -webkit-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    -o-transform: translate(0, 0);
    transform: translate(0, 0);
    width: 50px!important;
    z-index: 850;
    padding-top:50px;
    }
    }
    
 	</style>
 	
 	
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
      <nav class="navbar navbar-static-top" role="navigation"
		style="margin-left:0% !important ;max-height:50px; background-color:#3c8dbc">
	<!-- Sidebar toggle button--> 
	<img src="resources/images/logowhite.png" class="user-image" alt="User Image" style="height: 50px; padding-left: 5px; margin-left: 5px;">

	
		<span class="navbar-t-centered">Outreach Healthcare System</span>
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->

              <li class="dropdown user user-menu" style="margin-right: 15px;padding-right: 15px;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 5px;height:50px;">
                  <img src="resources/images/avatar.png" class="user-image" alt="User Image" style="background-color: white;margin-left: 15px;">
                
                <p class="hidden-xs" style="color: white;margin-left: -10px;cursor:pointer" id="loggedInUserFullName">Doctor</p>
                </a>
                <ul class="dropdown-menu" style="width:150px;">
                  <!-- User image -->
                  <li class="user-body" style="background-color:white;padding:0px;padding-bottom:5px;">
                    <!-- <img src="resources/images/avatar.png" class="img-circle" alt="User Image">
                    <p><span id="loggedInUserFullNameSpan"></span>
                    </p> -->
                    <div class="pull-left image">
              <img src="resources/images/avatar.png" class="img-circle" alt="User Image" style="padding:10px;padding-left:5px;padding-right:5px;">
            </div>
            <p class="pull-left" style="margin-bottom: 0px;margin-top: 15px;">
            <span id="loggedInUserFullNameSpan">Admin</span>
                    </p>
                    
                    <div class="text-center">
                      <a href="adminLogout" class="btn btn-default btn-flat" style="padding-top: 2px;margin-left: -10px;margin-top: 8px;padding-bottom: 2px;padding-left: 5px;padding-right: 5px;">Sign out</a>
                    </div>
                  </li>
                  <!-- Menu Body -->
                 <!-- <li class="user-body" style="padding:5px;border-top-width: 0px;">
                                  <ul class="col-xs-12 text-center" id="activeUsersId">
                                  </ul>
                                </li> -->
                  <!-- Menu Footer-->
                  <!-- <li class="user-footer text-center" style="text-align:center;">
                    <div class="pull-left">
                    </div>
                    
                  </li> -->
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar" id="navHeight">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <!-- <div class="user-panel">
            <div class="pull-left image">
              <img src="resources/images/avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Admin</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          search form
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->
          <ul class="sidebar-menu">
            <li onclick="viewDashboard()"><a href="#dashBoardId" class="active"><i class="fa fa-book" aria-hidden="true"></i>
			<span>Dashboard</span></a></li>
            
            <li onclick="viewUserList()"><a href="#tableId" >
<i class="fa fa-book" aria-hidden="true"></i>
<span>User Management</span></a></li>
            <li onclick="viewGraph();"><a href="#grpahShowId">
            <i class="fa fa-book"></i> <span>MIS Reports</span></a></li>
           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      
       <div class="content-wrapper">
        <section class="content-header">
          <h1>
            <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
            <li class="active"><i class="fa fa-dashboard" style="padding-right: 5px;cursor;pointer"></i>Dashboard</li>
            <li id="activPage"><!-- <a href="#"> --><!-- </a> --></li>	
          </ol>
            <!-- <small>Control panel</small> -->
          </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol> -->
        </section>
        <section class="content" style="padding-left: 05px;padding-right: 5px;padding-top: 0px;padding-bottom: 0px;">
         <div class="row" style="margin-left: -5px;">
         <div id="dashBoardId" class="listOp">
         
         
         </div>
         
       	<div id="tableId" class="listOp" style="display:none;margin-left: 10px;margin-right: 10px;">
       	
       	<div class="tab-content" style="overflow-y:auto;min-height:500px;">
                  <div class="tab-pane" id="history" style="display:block;margin-left: -5px;margin-right: -5px;">
                   <!-- <div class="box box-default"> -->
                 
              <div class="box-body">
                  <table class="table table-bordered table-striped" id="userTable" >
                    <thead>
                    <tr style="height: 33px !important;">
                      <th style="text-align:center;">#</th>
                      <th style="text-align:center;">Name</th>
                      <th style="text-align:center;">Contact/E-Mail</th>
                      <th style="text-align:center;">User Type</th>
                      <th style="text-align:center;">Status</th>
                       <th style="text-align:center;">Action</th>
                      </tr>
                      </thead>
                      <tbody id="userDetails" ></tbody>
                  </table>
                  
                  <div class="col-md-12 text-center" id="userDetailsNoList" style="display:none;padding-left: 0px;padding-right: 0px;">
                  <p style="padding-top: 10px;padding-bottom: 10px;border: 1px solid #f4f4f4;">No Patients</p>
                  </div>
                </div><!-- /.box-body -->
                  </div><!-- /.tab-pane -->

        
      <!--   <div class="modal tab-content " 
            role="dialog" style="padding:15px;margin-top: 50px;z-index: 1025;"> -->
           
        </div>
       	</div>
       
       <div class="col-md-12 listOp" id="grpahShowId" style="display:none;margin-top: -15px;" >
       <div class="box box-primary">
		<div class="box-header">
		<div class="col-md-12" style="padding: 5px;"><h6 class="box-title" style="font-size:14px;padding-bottom: 15px;">MIS Report</h6>
		<button class="pull-right btn-primary btn dcbutton" onclick="refreshModal();">
		<i class="fa fa-refresh" aria-hidden="true"></i></button></div>
		
		<div class="col-md-3">
		<input type="text" id="fromDateId" onchange="redirectToRepo();" name="fromDate" maxlength="100" placeholder="From Date" />
		</div>
		<div class="col-md-3">
		<input type="text" id="toDateId" onchange="redirectToRepo();" name="toDate" maxlength="100" placeholder="To Date" />
		
		</div>
		
		<div class="col-md-3"><!-- <p style="margin-bottom: 0px; color: royalblue;text-align:right;padding-top: 5px;">Select Kit</p>  -->
		</div>
		<div style="padding-left: 0px;" class="col-xs-3 pull-right dcbutton">
									 
										<div style="padding-left: 0px; padding-right: 0px;" class="col-xs-12">
											<div style="width: 100%; color: black; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px;" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" id="" data-upgraded=",MaterialTextfield">
												<select class="form-control" onchange="getReportData();" id="kitsId" name="kit" style="display: block ! important;width: 100%; border-left-width: 1px; border-top-width: 1px; border-right-width: 0px; padding-left: 0px;">
												<!-- <option selected>Select Kit</option> -->
												
												</select>
											</div>
										</div>
								</div>
		<!-- <span class="col-md-12" style="padding-top: 5px;" >This graph displays <b><h id="froId">16/01/2017</h> - <h  id="troId">16/01/2019</h></b> data</span> -->										
		<!-- <button class="pull-right btn-primary btn dcbutton" onclick="openMedicationModal();">
		
		Add Prescription</button> -->
		
		
		</div>
		<div class="box-body no-padding" style="overflow-y: auto;">
		<div >
       <div id="container"  style="min-width: 310px; height: 450px; margin: 0 auto;display:block;">
       
       </div>	
       </div>
	</div>
			 </div>
			 </div>

		
        </div>
         </section>
        </div>
   <!-- Change Password Modal -->     
      <div id="addPswdModal" name="" class="modal modal-fullscreen fade" style="padding-top: 100px;height:100%;z-index:9999;background: none;" role="dialog"   data-keyboard="false">
       		<form id ="savePasswordForm" method="post" enctype ='multipart/form-data' style="height:100%;">
			
			<!-- <input type="hidden" id="diagnoEpiId" name="episodeId" /> -->
					<input type="hidden" name="userId" id="pswdUId" value="" />
							
			<div >
                <div class="modal-dialog" style="padding-top: 0px;height:100%;" >
					<div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
						<div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
							<h5 class="modal-title" >
							<b style="color:White">Change Password</b></h5>
						</div>
						
                <div class="box-body" style="padding-top: 0px;">
                 
                  <div class="col-xs-12" style="padding-top: 10px; margin-top: 10px;padding-left: 0px;padding-bottom: 10px;display:block;">
					<div style="width: 100%; color: black;padding-bottom: 10px;">
					<input	class="mdl-textfield__input error-popover" placeholder="New Password"
						style="padding-left: 15px;" type="password" id="newPsId" name="" />
										</div>
										
				 <div class="col-xs-12" style="padding-top: 10px; margin-top: 10px;padding-left: 0px;display:block;" id="">
						<div style="width: 100%; color: black">
					<input	class="mdl-textfield__input error-popover" placeholder="Confirm Password"
						style="padding-left: 15px;" type="password" id="confiPsId" name="" onclick="getDate()" />
										</div>
										
									</div>
									
									</div> 					
                  
                </div><!-- /.box-body -->
               <div class="modal-footer" style="border-top-width: 0px;text-align:center;">
						<button type="button" class="btn btn-default hidePopup" onclick="clearAllPassword();" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="savePassword();" id="pswdSave">Save</button>
					</div>
					</div>
					</div>
					
              </div>
				</form>	
    	</div>
   <!-- Change Password Modal -->
 
 <!-- Change Password Modal -->     
      <div id="addEditModal" name="" class="modal modal-fullscreen fade" style="padding-top: 100px;height:100%;z-index:9999;background: none;" role="dialog"   data-keyboard="false">
       		<form id ="saveEditForm"  method="post" enctype ='multipart/form-data'  style="height:100%;">
			
			<input type="hidden" name="customerItemsId" id="produEdId"  /> 
			<input type="hidden" name="userId" id="editUId" value="" />
			<input type="hidden" name="userGrpId" id="editUGId" value="" />
			<input type="hidden" name="empId" id="profileEdId" value="" />
							
			<div>
                <div class="modal-dialog" style="padding-top: 0px;height:100%;" >
					<div class="modal-content modal-content" style="border-radius: 4px;overflow-x:hidden;">
						<div class="modal-header" style="background-color:#5498D2;padding-top: 15px;overflow-x:hidden">
							<h5 class="modal-title" >
							<b style="color:White">Edit User Details</b></h5>
						</div>
						
                <div class="box-body" style="padding-top: 0px;">
                 
                 <div class="col-xs-12" style="padding-top: 10px; margin-top: 10px;padding-left: 0px;padding-bottom: 10px;">
					<div style="width: 100%; color: black; padding-top: 15px;padding-bottom: 15px;">
					<input	class="mdl-textfield__input error-popover editList" placeholder="First Name"
						style="padding-left: 15px;" type="text" id="newFiNaId" name="firstName" />
										</div>
										
					<div style="width: 100%; color: black;padding-top: 15px;padding-bottom: 15px;">
					<input	class="mdl-textfield__input error-popover" placeholder="Last Name"
						style="padding-left: 15px;" type="text" id="newLaNaId" name="lastName" />
										</div>
										
				
					<div style="width: 100%; color: black;padding-top: 15px;padding-bottom: 15px;">
					<input	class="mdl-textfield__input error-popover editList" placeholder="UserName"
						style="padding-left: 15px;" type="text" id="newuseNaId" name="userName" />
										</div>
										
				
					<div style="width: 100%; color: black;padding-top: 15px;padding-bottom: 15px;">
					<input	class="mdl-textfield__input error-popover editList" placeholder="Mobile" maxlength="10"
						style="padding-left: 15px;" type="text" id="newMobileId" name="mobile" />
										</div>
										
				
					<div style="width: 100%; color: black;padding-top: 15px;padding-bottom: 15px;">
					<input	class="mdl-textfield__input error-popover editList" placeholder="E-Mail"
						style="padding-left: 15px;" type="text" id="newMailId" name="eMail" />
										</div>
										
				<!-- <div class="col-xs-12" style="padding-top: 10px; margin-top: 10px;padding-left: 0px;padding-bottom: 10px;">
					<div style="width: 100%; color: black;padding-bottom: 10px;">
					<input	class="mdl-textfield__input error-popover" placeholder="New Password"
						style="padding-left: 15px;" type="password" id="newPsId" name="" />
										</div> -->
										
				
				</div>																																																						
                </div><!-- /.box-body -->
               <div class="modal-footer" style="border-top-width: 0px;text-align:center;">
						<button type="button" class="btn btn-default hidePopup" onclick="clearAllEdit();" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="saveUpdate();">Save</button>
					</div>
					</div>
					</div>
					
              </div>
				</form>	
    	</div>
   <!-- Change Password Modal -->  
   
         </div><!-- ./wrapper -->
         
</body>
<script>

var productId = '';
$(document).ready(function(){
	productId = localStorage.getItem("productId");
	var loggedInUserFullName = localStorage.getItem("loggedInUserFullName");
	//graph();
	
	$("#loggedInUserFullName").text(loggedInUserFullName);
	$("#loggedInUserFullNameSpan").text(loggedInUserFullName); 
	/* Datepicker */
	$("#fromDateId").datepicker({
		autoclose: true,
		todayHighlight: true,
		//startDate: '0m',
		format: "dd/mm/yyyy"
	}).on('change',function(e){
		$("#fromDateId").attr('readonly',true);
		$(this).datepicker('hide');
	});
	
	$("#toDateId").datepicker({
		autoclose: true,
		todayHighlight: true,
		//startDate: '0m',
		format: "dd/mm/yyyy"
	}).on('change',function(e){
		$("#fromDateId").attr('readonly',true);
		$(this).datepicker('hide');
	});
	
	/*Datepicker  */
	
	/* Window height*/
	var windowHeight = $(window).height();
	$("#navHeight").css("height",windowHeight);
	
	/* WH */
});

function viewUserList(){
	//alert(productId);
	$(".listOp").css("display","none");
	$("#tableId").css("display","block");
	$("#activPage").html('User Management');
	viewUserMan();
}

function viewDashboard(){
	$(".listOp").css("display","none");
	$("#dashBoardId").css("display","block");
}

function viewGraph(){
	
	//alert();
	$("#dashBoardId").css("display","none");
	$("#tableId").css("display","none");
	$("#activPage").html('MIS Report');
	//alert();
	$("#grpahShowId").show();
	$("#fromDateId").val('');
	$("#toDateId").val('');
	$("aside").addClass("selectC");
	//graph();
	getReportData();
	getKitList();
	//alert();
}

function viewUserMan(){

				$('#userTable').DataTable().destroy();
				 $.ajax({
				 type : "POST",
				 url : serverPath+"/getUserListByProductId?productId="+productId,
				 success : function(data) {
				 if (data.code == "200"){
					 
				  $("#userDetails").html("");$("#userDetailsNoList").hide();
				 
				  var uhid = '-';var pName = '-';var age = '';var ageG = '';var gender = '';var rD = '-';
				 var city = '-';var cnt = 0;var appendOrderData="";
				 
				 if(data.content != '' && data.content != null){
				 $.each(data.content,function(id,option){
				city = '-';uhid = '-';pName = '-';age = '';ageG = '';gender = '';rD = '-';
				 cnt++;
				 if(option.firstname != null || option.lastname != null){
				 uhid = option.firstname + " "+ option.lastname;
				 }if(option.phone != null || option.email != null){
				 pName = option.phone +'<br>' + option.email;
				 }if(option.userGroupCode != null){
				 age = option.userGroupCode ;
				 }if(option.enabled != null){
				 gender = option.enabled;
				 //ageG = age + gender;
				 }
				 
				 rD = "<i class='fa fa-eye' style='cursor:pointer;' aria-hidden='true' onclick='editUser("+option.id+","+option.userGroupId+","+option.profileId+",&quot;"+option.userName+"&quot;,&quot;"+option.email+"&quot;,&quot;"+option.phone+"&quot;,&quot;"+option.firstname+"&quot;,&quot;"+option.lastname+"&quot;);'></i> | <i class='fa fa-key' style='cursor:pointer;' onclick='viewPsModal("+option.id+");' aria-hidden='true'></i>";
				 
				 
		 
			 appendOrderData+="<tr><td style='text-align:center;'>"+cnt+"</td><td style='text-transform:capitalize;text-align:center;'>"+uhid+"</td> "+
	          	" <td style='text-transform:capitalize;text-align:center;'>"+pName+"</td><td style='text-align:center;'>"+age+"</td>"+
	          	/* " <td style='text-align:center;'>"+city+"</td><td align='center' valign='middle'><div class='btn-group'> "+
	          	"<a href='#'  data-toggle='tooltip' class='btn btn-primary'> "+
	              " Consultation</a></td>"+ */
	              "<td> - </td><td align='center' valign='middle'> "+rD+" </td> </tr>";
     			
   						});
   						$("#userDetails").append(appendOrderData);
   						$("#userTable").DataTable( {
       				        "paging":   true,
       				        "ordering": true,
       				        "bLengthChange": false,
       				        "info":     false,
       				        "bDestroy": true,
       				    });
   						
   						//$('.dataTables_filter').css('display','none');
   						$('.dataTables_filter').addClass('pull-left');
   						
   						 var parentElement = $("#userTable_filter").parent();
   						 var grandpa = $(parentElement).parent();
   						 var elder= $(grandpa).children(":first");
   						 $(elder).css( "display", "none" );
   						
   					}else if(data.content == null || data.content == 'null'){
   						$("#userDetailsNoList").show();
          			      }
   					
   		        }
   		    }
           });
    }

function viewPsModal(userId){
	//alert(userId);
	$("#pswdUId").val(userId);
	$("#addPswdModal").modal();
	
}

function clearAllPassword(){
	
	$("#newPsId").val('');
	$("#confiPsId").val('');
}

function savePassword(){
	
	//alert($("#newPsId").val());alert($("#confiPsId").val());
	if($("#newPsId").val() == $("#confiPsId").val()){
		
		$.ajax({
			  type:"POST",
			  url:serverPath+"/changeUserPassword?password="+$("#newPsId").val()+"&userId="+$("#pswdUId").val(),
			  success: function(data){
				  if(data.code == '200'){
					  
					 //alert(data.code); 
					 toastr.success("Password Changed Successfully");
					 clearAllPassword();
					 $("#addPswdModal").modal('hide');
				  }
				  
			  }
			});
		
	}else{
		toastr.warning("New Password and Confirm Password aren't matching");
	}
}

function editUser(userId,userGrpId,profileId,userName,email,phone,firstname,lastname){
	//alert(userName);
	
	$("#newFiNaId").val(firstname);
	$("#newLaNaId").val(lastname);
	$("#newuseNaId").val(userName);
	$("#newuseNaId").attr("readOnly",true);
	$("#newMailId").val(email);
	$("#newMobileId").val(phone);
	//$("#newFiNaId").val(firstname);
	//alert(productId);
	$("#produEdId").val(productId);
	$("#editUId").val(userId);
	$("#editUGId").val(userGrpId);
	$("#profileEdId").val(profileId);
	
	$("#addEditModal").modal();
}

function clearAllEdit(){
	
	$("#addEditModal").modal('hide');
	$("#newFiNaId").val('');
	$("#newLaNaId").val('');
	$("#newuseNaId").val('');
	$("#newMailId").val('');
	$("#newMobileId").val('');
	
}

function saveUpdate(){
	//alert($(".editList").val());
	
	if($(".editList").val() != ''){
		
		var regMob = ^(\+?\d{1,4}[\s-])?(?!0+\s+,?$)\d{10}\s*,?$ ;
		var mob = $("#").val();
		
		var form1 = document.getElementById('saveEditForm');
		 var formData = new FormData(form1); 
		 
	$.ajax({
	  type : "POST",
	  headers:{"deviceId": "ADMIN"},
	  data :formData,
	  dataType:"json",
	    mimeType: "multipart/form-data",
		async : false,
	    cache : false,
		contentType : false,
	    processData : false,
	  url : serverPath + "/saveEmployee",
	  success:function(data){
		  if(data.code == '200'){
			 	
			  clearAllEdit();
			  viewUserMan();
			  toastr.success("User Details updated Successfully");
			  
		  }
	  }
	})
	}else{
		toastr.warning("Please Fill Mandatory Data")
	}
}

var kitNameFinal = [];var kitFemaleFinal = [];var kitMaleFinal = [];var URL = '';var kitId = -1;var from = '';var to = '';var kitName = '';
function getReportData(){
	//alert('kit'+ $("#kitsId").find('option:selected').attr('id'));
	//alert($("#fromDateId").val());alert($("#toDateId").val());
	kitNameFinal = []; kitFemaleFinal = []; kitMaleFinal = [];
	
	kitId = $("#kitsId").find('option:selected').attr('id');
	kitName = $("#kitsId").val();
			//alert(kitName);
			if(kitId != null || kitId != undefined){
		
		URL = serverPath + "/getMISReports?productId="+productId+"&kitLicenseId="+kitId+"&kitName="+kitName ;
	}else if($("#fromDateId").val() != '' && $("#toDateId").val() != ''){
		from=$("#fromDateId").val();to=$("#toDateId").val();
		URL = serverPath + "/getMISReports?productId="+productId+"&fromDate="+from+"&toDate="+to ;
	}else if($("#fromDateId").val() != '' && $("#toDateId").val() != '' &&(kitId != null || kitId != undefined)){
		URL = serverPath + "/getMISReports?productId="+productId+"&fromDate="+from+"&toDate="+to+"&kitLicenseId="+kitId;
	}else{
		from = '';to='';kitId=-1;
		URL = serverPath + "/getMISReports?productId="+productId+"&fromDate="+from+"&toDate="+to+"&kitLicenseId="+kitId;
	} 
	//alert(URL);
	$.ajax({ 	
	  type:"POST",
	  url: URL,
	  success: function(data){
		  var cnt = 0;
		  if(data.code == '200'){
			  //alert(data.code);
		  if(data.content != ''){
			  
				  $.each(data.content,function(index,option){
					  //alert(option.kitName);
					  kitNameFinal.push(option.kitName);
					 cnt++;
					 // alert(option.genderdto.maleCount);
					 /* if(option.genderdto != null && option.genderdto != 'null'){ */
						
						 if(option.genderdto.maleCount != null || option.genderdto.femaleCount != null){
							 if(option.genderdto.maleCount != null){
								 kitMaleFinal.push(option.genderdto.maleCount);
							 }else{
								 kitMaleFinal.push(0);
							 }
							 
							 if(option.genderdto.femaleCount != null){
								 kitFemaleFinal.push(option.genderdto.femaleCount);
								 }else{
									 kitFemaleFinal.push(0);
								 }
						 } else{
							if(cnt != '1'){
							 kitMaleFinal.push(0);
							 kitFemaleFinal.push(0);
							}
						 }
					 /* }else{
							alert('else');
						 kitMaleFinal.push(0);
						 kitFemaleFinal.push(0);
					 } */ 
					
				  });
			  }
			  graph();
		  }
	  }
	});
}

function getKitList(){
	optKit = '';
	 
	$.ajax({
		type:"POST",
		url: serverPath + "/getAllKitList?productId="+productId,
		success: function(data){
			$("#kitsId").html('');
			if(data.code == '200'){
				//$("#kitsId").html('');
				$("#kitsId").html('<option selected val="-1">Select Kit</option><option selected val="-1">All Kits</option>');
				 $.each(data.content,function(index,option){
					optKit+='<option id="'+option.kitId+'">'+option.kitName+'</option>';
					
				}); 
				 $("#kitsId").append(optKit);
				
			}
			
		}
	});
	
}

function redirectToRepo(){
	if($("#fromDateId").val() != '' && $("#toDateId").val() != ''){
		
		getReportData();
	}
	
}

function refreshModal(){
	
	
	window.location.href="adminPage";
	//
	setTimeout(function(){
		viewGraph();
	}, 3000);
}

function graph(){
	//$("#containerGrId").show();
	
	Highcharts.chart('container', {
	    chart: {
	        type: 'column'
	    },credits: {
	        enabled: false
	    },
	    title: {
	        //text: 'Footfall Based on Gender'
	    	text: ''
	    },
	    xAxis: {
	    	min: 0,
	    	title: {
	            text: 'Kits'
	        },
            minRange: 3,
           //lineWidth: 2,
           //minorTickLength: 0,
           //tickLength: 0,
	        //categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
	    	categories: kitNameFinal,
	    	//max: 8,
	        labels: {
	          formatter: function() {
	            return typeof this.value !== 'number' ? this.value : ''
	          },
	          
	        }
	    },
	    yAxis: {
	        min: 0,
	        allowDecimals: false,
	        title: {
	            text: 'Total FootFall'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	            }
	        }
	    },
	    legend: {
	    	align: 'right',
	        padding: 3,
	        itemMarginTop: 15,
	        itemMarginBottom: 15,
	        verticalAlign: 'bottom',
	        y: 25,
	       
	        x:0,
	        maxHeight: 50,
	        floating: true,
	        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
	        borderColor: '#CCC',
	        //borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	        	maxPointWidth: 50,
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true,
	                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
	            }
	        }
	    },
	    series: [{
	        name: 'Male',
	        //data: [5, 3, 4, 7, 2]
	        data: kitMaleFinal
	    }, {
	        name: 'Female',
	        //data: [3, 2, 3, 2, 1]
	        data: kitFemaleFinal
	    }]
	});
	//alert();	
	
}


</script>         
</html>