function pingCallForVC() {
    $.ajax({
        type: "GET",
        headers: { "deviceId": "ADMIN" },
        url: serverPath + "/checkForCallv5?clientId=" + localStorage.getItem("clientId") + "&userId=" + localStorage.getItem("loggedInUserId"),
        headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
        success: function (data) {
            if (data.code == '200') {
                //console.log('pingCallForVC-RESP?: ', data);
                $("#activeUsersId").html('');
                var name = ''; activeList = '';
                if (data.content.activeUsers) {
                    $.each(data.content.activeUsers, function (index, option) {
                        let tempBool = false
                        if(localStorage.getItem("inVideoCall") == null) localStorage.setItem("inVideoCall", JSON.stringify(tempBool));
                        let doctorBusy = JSON.parse(localStorage.getItem("inVideoCall"));
                        //console.log('Call Initiated?: ', option.callInitiated, doctorBusy, (option.callInitiated &&  !doctorBusy ));
                        var roomName = option.kitId;
                        if (option.callInitiated ) {
                            console.log('Call Initiated?YES : ', option.callInitiated);
                            var callerName = option.fullName;
                            var weconUrl = data.content.weconUrl;
                            console.log("videocall url",weconUrl)
                            //var weconUrl = "https://wecon3.wenzins.live/";
                            joinVideoCallPopUp(callerName, roomName, weconUrl);
                        }
                        activeList += '<div class="form-control" style="border-radius:6px;padding-right: 5px;padding-left: 5px;padding-top: 3px;padding-bottom:6px" ><div class="col-xs-9" style="padding-left:0px;padding-right:0px;padding-top:5px;"><span style="font-size: 12px;text-transform: capitalize;margin-left: 0px;" id="nurseNameForActiveId">' + option.fullName + '</span></div> ' +
                            '<div class="col-xs-3 text-right" style="padding-left:0px;padding-right:0px;"> <a id= "videoIconId" style="border-radius: 50%;margin-left: 5px;float:right;background-color: green" class="btn btn-primary" title="videoCall" onClick="initiateVideoCall(\'' + roomName + '\')"><b> ' +
                            ' <i style="color:white;font-size:12px" class="fa fa-video-camera"  id="showHideVideoIconId"></i></b></a></div></div> ';
                    });
                    $("#activeUsersId").html(activeList);
                } else {
                    //$('#showHideVideoIconId').show();
                    //$('#videoIconId').hide();
                    $("#activeUsersId").html("<li class='form-control' style='border-radius:6px;color:gray;'>No Active User</li>");
                }
            }
            // if (window.open('', 'DoctorVideoCallWindow', '')) {

            // } else {
            //     console.log('DoctorVideoCallWindow CLosed');
            //     localStorage.setItem('inVideoCall', false);
            // }
            timeoutId = setTimeout(pingCallForVC, 3000);

        },
        error: function (xhr, status, error) {
            console.log('xhr1: ', xhr, ' -->Status: ', status, ' -->Error: ', error);
            if (xhr.status == 0) window.location.href = './login';
            if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                if (xhr.responseJSON.code === 401) {
                    (async () => {
                        toastr.error(xhr.responseJSON.message);
                        await sleep();
                        window.location.href = './login';
                    })()
                }
                toastr.error(xhr.responseJSON.message);
            } else {
                toastr.error("No Data Found!");
            }
        }
    });
}

function joinVideoCallPopUp(callerName, roomName, weconUrl) {
    console.log("Inside joinVideoCallPopUp(): ",callerName, roomName, weconUrl)
    // var room = encodeURIComponent(roomName);
    videocalllink = weconUrl + roomName;
    var win;
    var r = confirm(callerName + " is calling you. Do you accept the call?");
    //	alert(r);
    if (r == true) {
        // localStorage.setItem('inVideoCall', true);
        win = window.open(videocalllink, 'DoctorVideoCallWindow', 'width=300,height=300');

        /* win.addEventListener("resize", function(){
                   win.resizeTo(1024, 768);
             }); */
    }
    // $.ajax({
    //     type: "POST",
    //     url: serverPath + "/intiateAndCloseCallv4?initiate=false&roomId=" + roomName,
    //     success: function (data) {
    //     }
    // });
}

function initiateVideoCall(roomName, initiateCall=true) {
  let meetingUrl = "https://wecon.wenzins.live/";
//GTM release
    //let meetingUrl = "https://wecon3.wenzins.live/";
    //let meetingUrl = "https://wecon.samvitakit.com/";

        /* var win = window.open('initiateCall?profileId='+localStorage.getItem("profileId")+'&calleProfileId='+calleProfileId+'&serverKeyId='+localStorage.getItem("serverKeyId"), 'result', 'width=1024,height=768');
        win.addEventListener("resize", function(){
                        win.resizeTo(1024, 768);
        }); */
        $.ajax({
            type: "GET",
            url: serverPath + "/intiateAndCloseCallv5?userId=" + localStorage.getItem("loggedInUserId") + "&kitId=" + roomName + "&callInitiate=" + initiateCall,
            headers: { "Authorization": 'Bearer ' + localStorage.getItem('DW_JWT_TOKEN') },
            success: function (data) {
                console.log('initiateVideoCall: ', data);
                if (data.code == 200) {

                    videocalllink = meetingUrl + roomName;
                    var win = window.open(videocalllink, 'result', 'width=300,height=300');
                    win.addEventListener("resize", function () {
                        win.resizeTo(1024, 768);
                    });
                    localStorage.setItem('inVideoCall', true);
                } else {
                    toastr.error("Initiate Video Call failed");
                }
            },
            error: function (xhr, status, error) {
                console.log('xhr1: ', xhr, ' -->Status: ', status, ' -->Error: ', error);
                if (xhr.status == 0) window.location.href = './login';
                if (xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')) {
                    if (xhr.responseJSON.code === 401) {
                        (async () => {
                            toastr.error(xhr.responseJSON.message);
                            await sleep();
                            window.location.href = './login';
                        })()
                    }
                    toastr.error(xhr.responseJSON.message);
                } else {
                    toastr.error("Initiate Video Call failed");
                }
            }
        });
        console.log('---------STATU:--',initiateCall)
        console.log(typeof initiateCall)
        if(initiateCall == true){
            console.log('---------STATU:INSIDE--',initiateCall)
            timeoutId = setTimeout('initiateVideoCall(\'' + roomName +'\', \''+ false +'\')',8000);
        }

    return;
}