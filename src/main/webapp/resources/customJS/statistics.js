  // === include 'setup' then 'config' above ===
  
  function openCity(evt, cityName) {
	console.log("openCity: ",evt, cityName);
	  var i, tabcontent, tablinks;
	  tabcontent = document.getElementsByClassName("tabcontent");
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	  }
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" active", "");
	  }
	  document.getElementById(cityName).style.display = "block";
	  evt.currentTarget.className += " active";
	}
	
  $(document).ready(
	
	
	function(){    
		console.log("HEllo");
      var ctx1 = document.getElementById("myChart1").getContext('2d');
	  var ctx2 = document.getElementById("myChart2").getContext('2d');
	  var ctx3 = document.getElementById("myChart3").getContext('2d');
	  var ctx4 = document.getElementById("myChart4").getContext('2d');
	  var ctx5 = document.getElementById("myChart5").getContext('2d');
	  var ctx6 = document.getElementById("myChart6").getContext('2d');

	var c1Data = [1, 2, 3];
    var c1Label = ["Normal","Prediabetes","Diabetes"];
    var c1dtatBG = ["#3fff00", "#007ba7","#ffa500"];
    //var pieChartCTX = pieChart(c1Data,c1Label,c1dtatBG,ctx1, "doughnut")

	//var myChart12 = pieChart(c1Data,c1Label,c1dtatBG,ctx1, "doughnut");

	function pieChart(chartData, chartLabel, chartDataBGcolor, context, chartType){
        return new Chart(context, {
            type: chartType,
            data: {
                labels: chartLabel,
                datasets: [{
                    label: '',
                    backgroundColor: chartDataBGcolor,
                    data: chartData,
                }],
            },
            options: {
              tooltips: {
                filter: function (tooltipItem, data) {
                    return false;
                }             
             },
              responsive: true,
 
              plugins: {
                datalabels: {              
                  formatter: (val) => (`${val}`),
                  labels: {
                    value: {
                      color: 'white'
                    }
                  },
                  title: {
		              display: true,
		              text: 'Blood Glucose Statistics',
		              color: 'black',
		              font: {
		                family: 'Roboto',
		                size: 22,
		                weight: 'bold',
		                lineHeight: 1.2,
		              },
		          }
        
                },
                legend: {
                  position: 'top',
                }
              }
            }
        });
    }

      //Chart-1 -> BG PIE
      var myChart1 = new Chart(ctx1, {
        type: 'doughnut',
        data: {
            labels: ["Normal","Prediabetes","Diabetes"],
            datasets: [{
                label: '',
                backgroundColor: ["#3fff00", "#007ba7","#ffa500"],
                data: [0, 0, 0],
                datalabels: {
			      color: 'white'
			    }
            }],
        },
        options: {
          tooltips: {
            filter: function (tooltipItem, data) {
                return false;
            }
         },
          responsive: true,
          maintainAspectRatio: false,
          plugins: {
            
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Blood Glucose Statistics',
              color: 'black',
              font: {
                family: 'Roboto',
                size: 22,
                weight: 'bold',
                lineHeight: 1.2,
              },
            }
          }
        },
        plugins: [ChartDataLabels],
		
    });
    
    //Chart-2 -> BP PIE
      var myChart2 = new Chart(ctx2, {
        type: 'doughnut',
        data: {
            labels: ["Normal","Prehypertension","Hypertension", "Hypertensioncrisis"],
            datasets: [{
                label: '',
                backgroundColor: ["#3fff00", "#007ba7","#ffa500", "red"],
                data: [0, 0, 0, 0],
                datalabels: {
			      color: 'white'
			    }
            }],
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          plugins: {
            datalabels: {              
              formatter: (val) => (`${val}`),
              labels: {
                value: {
                  color: 'white'
                }
              }
    
            },
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Blood Pressure Statistics',
              color: 'black',
              font: {
                family: 'Roboto',
                size: 22,
                weight: 'bold',
                lineHeight: 1.2,
              },
            }
          }
        },
        plugins: [ChartDataLabels],
		
    });
    
    //Chart-3 -> BMI PIE
    var myChart3 = new Chart(ctx3, {
      type: 'doughnut',
      data: {
          labels: ["Normal","Overweight","Obese"],
          datasets: [{
              label: '',
              backgroundColor: ["#3fff00", "#007ba7","#ffa500"],
              data: [0, 0, 0],
                datalabels: {
			      color: 'white'
			    }
          }],
      },
      options: {
        showAllTooltips: true,
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          datalabels: {              
            formatter: (val) => (`${val}`),
            labels: {
              value: {
                color: 'white'
              }
            }
  
          },
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: 'BMI Statistics',
            color: 'black',
              font: {
                family: 'Roboto',
                size: 22,
                weight: 'bold',
                lineHeight: 1.2,
              },
          }
        }
      },
      plugins: [ChartDataLabels],
  });

	 


var myChart4 = new Chart(ctx4, {
    type: 'bar',
      data: {
          labels: ["Normal","Prediabetes","Diabetes"],
          datasets: [{
              label: 'Male',
              backgroundColor: "#007ba7",
              data: [0, 0, 0],
                datalabels: {
			      color: 'white'
			    }
          }, {
              label: 'Female',
              backgroundColor: "#bd0958",
              data: [0, 0, 0],
                datalabels: {
			      color: 'white'
			    }
          }],
      },
	  options: {
	      tooltips: {
	        displayColors: true,
	        callbacks:{
	          mode: 'x',
	        },
	      },
	      scales: {
	        x: {
	            stacked: true,
	            title: {
	              display: true,
	              text: 'Blood Glucose Gender Wise',
	              color: 'black',
	              font: {
	                family: 'Roboto',
	                size: 22,
	                weight: 'bold',
	                lineHeight: 1.2,
	              },
	              position:'top'
	            }
	          },
	          y: {
	            stacked: true
	          }
	      },
	          responsive: true,
	       legend: { position: 'top' },
      },
      plugins: [ChartDataLabels],
   }
);

var myChart5 = new Chart(ctx5, {
    type: 'bar',
    data: {
          labels: ["Normal","Prehypertension","Hypertension", "Hypertensioncrisis"],
          datasets: [{
              label: 'Male',
              backgroundColor: "#007ba7",
              data: [0, 0, 0, 0],
              datalabels: {
			      color: 'white'
			    }
          }, {
              label: 'Female',
              backgroundColor: "#bd0958",
              data: [0, 0, 0, 0],
              datalabels: {
			      color: 'white'
			    }
          }],
      },
    options: {
     	animation: {
        	duration: 10,
        },
        tooltips: {
			mode: 'label',
	        callbacks: {
	          label: function(tooltipItem, data) { 
	          	return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
	        }
          }
         },
        scales: {
      x: {
            stacked: true,
            title: {
              display: true,
              text: 'Blood Pressure Gender Wise',
              color: 'black',
	              font: {
	                family: 'Roboto',
	                size: 22,
	                weight: 'bold',
	                lineHeight: 1.2,
	           },
            }
          },
          y: {
            stacked: true
          }
    },
        legend: {display: true},responsive: true,
    } ,
           plugins: [ChartDataLabels]
   }
);

var myChart6 = new Chart(ctx6, {
    type: 'bar',
          data: {
              labels: ["Normal","Overweight","Obese"],
              datasets: [{
                  label: 'Male',
                  backgroundColor: "#007ba7",
                  data: [0, 0, 0],
                datalabels: {
			      color: 'white'
			    }
              }, {
                  label: 'Female',
                  backgroundColor: "#bd0958",
                  data: [0, 0, 0],
                datalabels: {
			      color: 'white'
			    }
              }],
          },
      options: {
          tooltips: {
            displayColors: true,
            callbacks:{
              mode: 'x',
            },
          },
          scales: {
            x: {
                stacked: true,
                title: {
                  display: true,
                  text: 'BMI Gender Wise',
                  color: 'black',
	              font: {
	                family: 'Roboto',
	                size: 22,
	                weight: 'bold',
	                lineHeight: 1.2,
	              },
                }
              },
              y: {
                stacked: true
              }
          },
              responsive: true,
              legend: { position: 'top'},
          },
           plugins: [ChartDataLabels],
   }
);
  

   getPatientVitalAnalysis();
   
   function callStatsAPI(){
		
	}
   

  function getPatientVitalAnalysis() {
		// var profileID = localStorage.getItem("profileId");
		// var serverKeyId2 = localStorage.getItem("serverKeyId");
		// var fromDate1 = new Date($('#datepicker1Val').val()).getTime();
		// var toDate1 = new Date($('#datepicker2Val').val()).getTime();
		// var fromDate = new Date($('#datepicker1Val').val()).setHours(0, 0, 0,
		// 		0, 0);
		// var toDate = new Date($('#datepicker2Val').val()).setHours("23", "59",
		// 		59, 0);
		// var today = new Date().setHours("23", "59", 59, 0);

		// if (toDate == null || fromDate == null) {
		// 	toastr.error('Selected date is null');
		// 	return;
		// }

		// if (toDate > today || fromDate > today) {
		// 	toastr.error('Date Cannot Be Greater Than Todays Date');
		// 	return;
		// }
		// if (toDate < fromDate) {
		// 	toastr.error('To Date cannot be Lesser Than From Date');
		// 	return;
		// }
		
		// console.log("downloadPatientCsv-> ", "From: ", fromDate, "To: ", toDate,
		// 		"ProfileId: ", profileID, "ServerKey: ", serverKeyId2);
		// var newURL = serverPath
		// 		+ "/getPatientReportByServerId?serverKeyId="
		// 		+ localStorage.getItem("serverKeyId") + "&startEpoch="
		// 		+ fromDate + "&endEpoch=" + toDate;
		//console.log(newURL);
		//return newURL;

    var serverPath = "http://localhost:8081/sPopulationCare";
		$.ajax({
			type : "GET",
			url : serverPath + "/getPatientVitalAnalysis?serverKeyId="
					//+ localStorage.getItem("serverKeyId") + "&startEpoch="
          + 7 + "&startEpoch="
					+ 1636482601000 + "&endEpoch=" + 1636741799000,
			success : function(data) {
					console.log(data);
          if(data["error"] === false){
            if(data.content.hasOwnProperty("systolicBPStats")){
              var bpKey = data.content.systolicBPStats;
              //myChart1.data.labels= ["Normal:"+bpKey.normal,"Prehypertension:"+bpKey.prehypertension,"Hypertension:"+bpKey.hypertension, "Hypertensioncrisis:"+bpKey.hypertensionCrisis]
              myChart2.data.datasets[0].data =[bpKey.normal, bpKey.prehypertension, bpKey.hypertension, bpKey.hypertensionCrisis];
              myChart2.update();
            }
            if(data.content.hasOwnProperty("bloodGlucoseStats")){
              var bgKey = data.content.bloodGlucoseStats;
              //myChart5.data.labels= ["Normal:"+bgKey.normal,"Prediabetes:"+bgKey.prediabetes,"Diabetes:"+bgKey.diabetes]
              myChart1.data.datasets[0].data =[bgKey.normal, bgKey.prediabetes, bgKey.diabetes];
              myChart1.update();
            }

            if(data.content.hasOwnProperty("bmiStats")){
              var bmiKey = data.content.bmiStats;
              //myChart7.data.labels= ["Normal:"+bmiKey.normal,"Overweight:"+bmiKey.overweight,"Obese:"+bmiKey.obese],
              myChart3.data.datasets[0].data =[bmiKey.normal, bmiKey.overweight, bmiKey.obese];
              myChart3.update();
            }
            
            if(data.content.hasOwnProperty("bmiGenderStats")){
              var bmiGenderKey = data.content.bmiGenderStats;
              //myChart7.data.labels= ["Normal:"+bmiKey.normal,"Overweight:"+bmiKey.overweight,"Obese:"+bmiKey.obese],
              if(bmiGenderKey.hasOwnProperty("male") && bmiGenderKey.hasOwnProperty("female")){
				myChart6.data.datasets[0].data =[bmiGenderKey.male.normal, bmiGenderKey.male.overweight, bmiGenderKey.male.obese];
				myChart6.data.datasets[1].data =[bmiGenderKey.female.normal, bmiGenderKey.female.overweight, bmiGenderKey.female.obese];
			  }
              
              myChart6.update();
            }
            
            if(data.content.hasOwnProperty("bloodGlucoseGenderStats")){
              var bgGenderKey = data.content.bloodGlucoseGenderStats;
              //myChart7.data.labels= ["Normal:"+bmiKey.normal,"Overweight:"+bmiKey.overweight,"Obese:"+bmiKey.obese],
              if(bgGenderKey.hasOwnProperty("male") && bgGenderKey.hasOwnProperty("female")){
				myChart4.data.datasets[0].data =[bgGenderKey.male.normal, bgGenderKey.male.prediabetes, bgGenderKey.male.diabetes];
				myChart4.data.datasets[1].data =[bgGenderKey.female.normal, bgGenderKey.female.prediabetes, bgGenderKey.female.diabetes];
			  }
              
              myChart4.update();
            }
            if(data.content.hasOwnProperty("systolicBPGenderStats")){
              var bpKey = data.content.systolicBPGenderStats;
              //myChart1.data.labels= ["Normal:"+bpKey.normal,"Prehypertension:"+bpKey.prehypertension,"Hypertension:"+bpKey.hypertension, "Hypertensioncrisis:"+bpKey.hypertensionCrisis]
              if(bgGenderKey.hasOwnProperty("male") && bgGenderKey.hasOwnProperty("female")){
				myChart5.data.datasets[0].data =[bpKey.male.normal, bpKey.male.prehypertension, bpKey.male.hypertension, bpKey.male.hypertensionCrisis];
				myChart5.data.datasets[1].data =[bpKey.female.normal, bpKey.female.prehypertension, bpKey.female.hypertension, bpKey.female.hypertensionCrisis];
			  }
              
              myChart5.update();
            }
            
          }
          
          
			}
		});
    // $.ajax({
		// 	type: "POST", //GET, POST, PUT
		// 	url: url,  //the url to call
		// 	dataType: "json",     //Data sent to server
		// 	contentType: "application/json",
		// 	data: myJSON,
		// 	beforeSend: function(xhr) {   //Include the bearer token in header
		// 		xhr.setRequestHeader("Authorization", "Bearer " + authenticateDate.accessToken);
		// 	}
		// }).done(function(response) {
		// 	console.log("Response->  ", response);
		// 	if (response.error == false) {
		// 		toastr.success(response.message);
		// 		setTimeout(() => {  location.reload(); }, 5000);
		// 	}
		// 	else {
		// 		toastr.error(response.message);
		// 	}		
						
		// 	//Response ok. process reuslt
		// }).fail(function(err) {
		// 	//Error during request
		// 	//console.log("err->", err.responseJSON);
		// 	toastr.error(err.responseJSON.message);
		// });

	};

  });


  
  

  