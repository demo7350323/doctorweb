var normalImg = "resources/images/rsz_dry-clean.png", moderateImg = "resources/images/rsz_right-arrowhead.png", abnormalImg = "resources/images/rsz_rhombus.png",
	veryAbnormalImg = "resources/images/rsz_pentagon.png", NTproBNPImg = "resources/images/Nt-proBNP.jpg";


function updateWBioTable(vitalResp, patGender = "Male", patAge = 1) {
	//console.log("Inside updateWBioTable()----> : ",vitalResp);

	var bloodVitalHeader = "<tr> <th>#</th> <th>PARAMETER</th> <th>FULL FORM</th> <th>RESULT</th> <th>UNIT</th> <th>RANGE</th> <th style='width:5%;'>LEVEL</th></tr>";

	var basicHtmlMap = new Map(), basicVitals = "";
	var cardiacHtmlMap = new Map(), cardiacVitals = "";
	var harmoneHtmlMap = new Map(), harmoneVitals = "";
	var infectionHtmlMap = new Map(), infectionVitals = "";
	var diabeticHtmlMap = new Map(), diabeticVitals = "";
	var respiratoryHtmlMap = new Map(), respiratoryVitals = "";
	var vectorBorneHtmlMap = new Map(), vectorBorneVitals = "";
	var fecalHtmlMap = new Map(), fecalVitals = "";
	var basicAVitAbnormal1 = false, basicAVitAbnormal2 = false, basicAVitAbnormal3 = false, basicAVitAbnormal4 = false, basicAVitAbnormal5 = false, basicAVitAbnormal6 = false,basicAVitAbnormal7 = false;
	var cardiacMarkerAbnormal1 = false, cardiacMarkerAbnormal2 = false, cardiacMarkerAbnormal3 = false, cardiacMarkerAbnormal4 = false, cardiacMarkerAbnormal5 = false;
	var harmoneMarkerAbnormal1 = false, harmoneMarkerAbnormal2 = false, harmoneMarkerAbnormal3 = false, harmoneMarkerAbnormal4 = false, harmoneMarkerAbnormal5 = false, harmoneMarkerAbnormal6 = false, harmoneMarkerAbnormal7 = false;
	var infectionMarkerAbnormal1 = false, infectionMarkerAbnormal2 = false;
	var diabeticMarkerAbnormal1 = false, diabeticMarkerAbnormal2 = false;
	var respiratoryMarkerAbnormal1 = false, respiratoryMarkerAbnormal2 = false, respiratoryMarkerAbnormal3 = false, respiratoryMarkerAbnormal4 = false, respiratoryMarkerAbnormal5 = false;
	var vectorBorneAbnormal1 = false, vectorBorneAbnormal2 = false, vectorBorneAbnormal3 = false, vectorBorneAbnormal4 = false, vectorBorneAbnormal5 = false, vectorBorneAbnormal6 = false;
	var feacalMarkerAbnormal1 = false, feacalMarkerAbnormal2 = false;

	//console.log("**********************************",infectionVitals.vitalsName);
	$.each(vitalResp, function (index, option) {
		var vitalMeasuredTime = "", viatlName = option.vitalsName.toLowerCase(), vitalValue = option.vitalsValue;
		if (option.strMonitoredDate != null && option.strMonitoredDate != "") {
			vitalMeasuredTime = getCustomTime(strToDate(option.strMonitoredDate));
		}
		//console.log("------viatlName----:", option.vitalsName);
		//console.log("------vitalsValue----:", option.vitalsValue);


		//---------------- Basic Table Header & Body ---------------
		if (viatlName == "hb".toLowerCase()) {
			var statusImg = normalImg;
			if (vitalValue >= 13.2 && vitalValue <= 16.6 ) {
				statusImg = normalImg;
				    basicAVitAbnormal1 = false;
				}else{
				    statusImg = veryAbnormalImg;
                    basicAVitAbnormal1 = true;
				}

			//console.log("statusImg:",statusImg, ', value:',vitalValue)
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>basicCount</td> <td>HB</td> <td>HEMOGLOBIN</td> <td class='bvResult'>" + vitalValue + "</td> <td>ng/mL</td> <td><div>1. 13.2 - 16.6 C - Negative.</div><div>2. < 13.2 & >16.6 - Positive</div></td> <td style='text-align: center; vertical-align: middle; background:white;'>" + status + "</td></tr>";
			if (basicHtmlMap.has(viatlName)) {
				basicHtmlMap.delete(viatlName);
				basicHtmlMap.set(viatlName, html);
			} else {
				basicHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "Temperature".toLowerCase()) {
			var statusImg = normalImg;

            if (vitalValue > 38 || vitalValue < 36) {
            	statusImg = veryAbnormalImg;
            	basicAVitAbnormal2 = true;
            } else if (vitalValue >= 36 && vitalValue <= 38) {
            	statusImg = normalImg;
            	basicAVitAbnormal2 = false;
            }
			//console.log("statusImg:",statusImg, ', value:',vitalValue)
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>basicCount</td> <td>Temperature</td> <td>Temperature</td> <td class='bvResult'>" + vitalValue + "</td> <td><div> <span>&#8451;</span></div></td> <td><div>1.(36 ~ 38 <span>&#8451;</span>) - Normal</div><div>2.(<36 / >38 <span>&#8451;</span>) - Abnormal</div></td> <td style='text-align: center; vertical-align: middle; background:white;'>" + status + "</td></tr>";
			if (basicHtmlMap.has(viatlName)) {
				basicHtmlMap.delete(viatlName);
				basicHtmlMap.set(viatlName, html);
			} else {
				basicHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "Pulse".toLowerCase()) {

			var statusImg = normalImg;
			if (vitalValue >= 72 && vitalValue <= 100) {
            	statusImg = normalImg;
            	basicAVitAbnormal3 = false;
           }else if (vitalValue < 72 || vitalValue > 100) {
				statusImg = veryAbnormalImg;
				basicAVitAbnormal3 = true;
			}

			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>basicCount</td> <td>Pulse</td> <td>Pulse</td> <td class='bvResult'>" + vitalValue + "</td> <td>bpm</td> <td><div>1. (72 ~ 100bpm) - Normal.</div><div>2. (<72 / >100bpm) - Abnormal</div></td> <td style='text-align: center; vertical-align: middle; background:white;'>" + status + "</td></tr>";
			if (basicHtmlMap.has(viatlName)) {
				basicHtmlMap.delete(viatlName);
				basicHtmlMap.set(viatlName, html);
			} else {
				basicHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "BG".toLowerCase()) {

			var statusImg = normalImg;
			if (vitalValue >= 71 && vitalValue <= 100) {
				statusImg = normalImg;
				basicAVitAbnormal4 = false;
			} else if (vitalValue > 1 && vitalValue < 70) {
				statusImg = veryAbnormalImg;
				basicAVitAbnormal4 = true;
			} else if (vitalValue > 101 && vitalValue < 125) {
				statusImg = abnormalImg;
				basicAVitAbnormal4 = false;
			} else if (vitalValue > 126) {
				statusImg = veryAbnormalImg;
				basicAVitAbnormal4 = true;
			}

			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>basicCount</td> <td>BG</td> <td>Blood Glucose</td> <td class='bvResult'>" + vitalValue + "</td> <td>mg/dL</td> <td><div>1. (1 ~ 70 mg/dL) - Low Sugar.</div><div>2.(71 ~ 100 mg/dL) - Normal</div><div>3.(101 - 125 mg/dL) - Prediabetes</div><div>4. (> 126 mg/dL) - Diabetes</div></td> <td style='text-align: center; vertical-align: middle; background:white;'>" + status + "</td></tr>";
			if (basicHtmlMap.has(viatlName)) {
				basicHtmlMap.delete(viatlName);
				basicHtmlMap.set(viatlName, html);
			} else {
				basicHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "SPO2".toLowerCase()) {

			var statusImg = normalImg;
			if (vitalValue > 100 || vitalValue < 95) {
				statusImg = veryAbnormalImg;
				basicAVitAbnormal5 = true;
			} else if (vitalValue >= 95 && vitalValue <= 100) {
				statusImg = normalImg;
				basicAVitAbnormal5 = false;
			}

			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>basicCount</td> <td>SPO2</td> <td>Oxygen saturation</td> <td class='bvResult'>" + vitalValue + "</td> <td>%</td> <td><div>1.(95 ~ 100%) - Normal.</div><div>2.(<95 / >100%) - Abnormal</div></td> <td style='text-align: center; vertical-align: middle; background:white;'>" + status + "</td></tr>";
			if (basicHtmlMap.has(viatlName)) {
				basicHtmlMap.delete(viatlName);
				basicHtmlMap.set(viatlName, html);
			} else {
				basicHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "BP".toLowerCase()) {
			var statusImg = normalImg ;
		    let systolic= vitalValue.split(",");
			let bgValue = systolic[0];
            let bgValue2 = systolic[1];
            if( (bgValue >= 120 && bgValue <= 135) && (bgValue2 >= 80 && bgValue2 <= 90)) {
                statusImg = normalImg;
                basicAVitAbnormal6 = false;
            } else if( bgValue >= 135.1 || bgValue <= 119.99) {
                statusImg = veryAbnormalImg;
                basicAVitAbnormal6 = true;
            } else if( bgValue2 <= 79.99 || bgValue2 > 90) {
              statusImg = veryAbnormalImg;
              basicAVitAbnormal6 = true;
            }

			//console.log("statusImg:",statusImg, ', value:',vitalValue)
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>basicCount</td> <td>BP</td> <td>Blood Pressure</td> <td class='bvResult'>" + vitalValue + "</td> <td>mm/Hg</td> <td><div>1. SYS 120~135 and DIA 80~90 mm/Hg - Normal.</div><div>2.SYS <120 or > 135, DIA <80 or >90 mm/Hg  - Abnormal</div></td> <td style='text-align: center; vertical-align: middle; background:white;'>" + status + "</td></tr>";
			if (basicHtmlMap.has(viatlName)) {
				basicHtmlMap.delete(viatlName);
				basicHtmlMap.set(viatlName, html);
			} else {
				basicHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "BMI".toLowerCase()) {
			var statusImg = normalImg;

			if (vitalValue >= 18.5 && vitalValue <= 24) {
			//console.log("normal-----",basicAVitAbnormal1);
				statusImg = normalImg;
				basicAVitAbnormal7 = false;
			} else  {
				statusImg = veryAbnormalImg;
				basicAVitAbnormal7 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>basicCount</td> <td>BMI</td> <td>Body Mass Index</td> <td class='bvResult'>" + vitalValue + "</td> <td>kg/m<sup>2</sup></td> <td><div>1. (<18.5 kg/m<sup>2</sup>) - Underweight</div><div>2.(18.5~24 kg/m<sup>2</sup>) - Normal</div><div>3.(25~29 kg/m<sup>2</sup>) - Overweight</div>4.(>=30 kg/m<sup>2</sup>) - Obese</td> <td style='text-align: center; vertical-align: middle; background:white;'>" + status + "</td></tr>";
			if (basicHtmlMap.has(viatlName)) {
				basicHtmlMap.delete(viatlName);
				basicHtmlMap.set(viatlName, html);
			} else {
				basicHtmlMap.set(viatlName, html);
			}
		}

		//---------------- Cardiac Table Header & Body ----------------
		if (viatlName == "CK_MB".toLowerCase()) {
			var statusImg = normalImg;
			//var statusImg = vitalValue==="Negative"? normalImg : veryAbnormalImg;
			if (vitalValue === "Negative") {
				statusImg = normalImg;
				cardiacMarkerAbnormal1 = false;
			} else {
				statusImg = veryAbnormalImg;
				cardiacMarkerAbnormal1 = true;
			}

			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>cardiacCount</td> <td>CK-MB</td> <td>Creatine Kinase-Muscle Brain</td> <td class='bvResult'>" + vitalValue + "</td> <td>ng/mL</td> <td><div>1. Below or = to 5ng/ml - Negative.</div><div>2 Above 5 ng/ml - Positive</div></td> <td style='text-align: center; vertical-align: middle; background:white;'>" + status + "</td></tr>";
			if (cardiacHtmlMap.has(viatlName)) {
				cardiacHtmlMap.delete(viatlName);
				cardiacHtmlMap.set(viatlName, html);
			} else {
				cardiacHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "Troponin I".toLowerCase()) {
			var extractNum = vitalValue.match(/[\d\.]+/g)[0];
			//var statusImg = extractNum <0.05?  normalImg: extractNum ==0.05 && vitalValue.includes("Below")? normalImg: extractNum >0.05? veryAbnormalImg: extractNum == 0.05 && vitalValue.includes("Above")? veryAbnormalImg : moderateImg;
			var statusImg = normalImg;
			if (extractNum <= 0.05) {
				statusImg = normalImg;
				cardiacMarkerAbnormal2 = false;
			} else {
				if (extractNum == 0.05 && vitalValue.includes("Below")) {
					statusImg = normalImg;
					cardiacMarkerAbnormal2 = false;
				} else {
					if (extractNum > 0.05 && extractNum <= 0.5) {

						statusImg = abnormalImg;
						cardiacMarkerAbnormal2 = false;

					} else {
						if (extractNum == 0.05 && vitalValue.includes("Above")) {
							statusImg = veryAbnormalImg;
							cardiacMarkerAbnormal2 = true;
						} else statusImg = moderateImg;cardiacMarkerAbnormal2 = false;
					}
				}
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>cardiacCount</td> <td>TnI</td> <td>Troponin I</td> <td class='bvResult'>" + vitalValue + "</td> <td>ng/mL</td> <td><div>1. Less than 0.05 ng/ml - Negative</div>2. Between 0.05 - 0.5 ng/ml - Observational zone<div></div><div>3. Above 0.5 ng/ml - Positive</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (cardiacHtmlMap.has(viatlName)) {
				cardiacHtmlMap.delete(viatlName);
				cardiacHtmlMap.set(viatlName, html);
			} else {
				cardiacHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "D-DIMER".toLowerCase()) {
			//var statusImg = vitalValue <= 500 ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue <= 500) {
				statusImg = normalImg;
				cardiacMarkerAbnormal3 = false;
			} else {
				statusImg = veryAbnormalImg;
				cardiacMarkerAbnormal3 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>cardiacCount</td> <td>D Dimer</td> <td>D Dimer</td> <td class='bvResult'>" + vitalValue + "</td> <td>ng/mL</td> <td><div>1. &#8804 500 - Negative</div><div>2. >500 - Positive</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (cardiacHtmlMap.has(viatlName)) {
				cardiacHtmlMap.delete(viatlName);
				cardiacHtmlMap.set(viatlName, html);
			} else {
				cardiacHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "NT proBNP".toLowerCase()) {
			var extractNum = vitalValue.match(/[\d\.]+/g)[0];
			var statusImg = normalImg;
			if (patAge < 75) {
				//statusImg = extractNum < 125 ? normalImg : veryAbnormalImg;
				if (extractNum < 125) {
				statusImg = normalImg;
				cardiacMarkerAbnormal4 = false;
				}
				else {
					statusImg = veryAbnormalImg;
					 cardiacMarkerAbnormal4 = true;
				}
			} else {
				//statusImg = extractNum < 450 ? normalImg : veryAbnormalImg;
				if (extractNum < 450) statusImg = normalImg;
				else {
					statusImg = veryAbnormalImg; cardiacMarkerAbnormal4 = true;
				}
			}

			var ntBNPImg = '<img style="width:100%;" src=' + NTproBNPImg + ' alt="not found">';
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>cardiacCount</td> <td>NT proBNP</td> <td>N-terminal pro b- type natriuretic peptide</td> <td class='bvResult'>" + vitalValue + "</td> <td>pg/mL</td> <td>" + ntBNPImg + "</td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (cardiacHtmlMap.has(viatlName)) {
				cardiacHtmlMap.delete(viatlName);
				cardiacHtmlMap.set(viatlName, html);
			} else {
				cardiacHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "hsCRP".toLowerCase()) {
			//var statusImg = vitalValue < 0.05 ? normalImg : vitalValue > 0.5 ? veryAbnormalImg : abnormalImg;
			var statusImg = normalImg;
			if (vitalValue <= 1.0) {
				statusImg = normalImg;
				cardiacMarkerAbnormal5 = false;
			} else {
				if (vitalValue >= 1.0 && vitalValue <= 3.0 ) {
					statusImg = moderateImg;
					cardiacMarkerAbnormal5 = false;

				} else {
				statusImg = veryAbnormalImg;
				cardiacMarkerAbnormal5 = true;
				}
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>cardiacCount</td> <td>hsCRP</td> <td>High Sensitive C-Reactive protein</td> <td class='bvResult'>" + vitalValue + "</td> <td>mg/L</td> <td><div>1. Low risk : Less than 1.0 mg/l Average risk : 1.0 - 3.0 mg/l</div><div>2. High risk : Above 3.0 mg/l</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (cardiacHtmlMap.has(viatlName)) {
				cardiacHtmlMap.delete(viatlName);
				cardiacHtmlMap.set(viatlName, html);
			} else {
				cardiacHtmlMap.set(viatlName, html);
			}
		}

		//---------------- Harmone Table Header & Body ----------------
		if (viatlName == "VitD".toLowerCase()) {
			//var statusImg = vitalValue <= 5 ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue >= 30) {
				statusImg = normalImg;
				harmoneMarkerAbnormal1 = false;
			} else if(vitalValue >= 21 && vitalValue <= 29){
				statusImg = abnormalImg;
				harmoneMarkerAbnormal1 = true;
			} else {
			statusImg = veryAbnormalImg;
            harmoneMarkerAbnormal1 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>harmoneCount</td> <td>Vitamin D</td> <td>Total 25-hydroxy Vitamin D (25-OH Vitamin D)</td> <td class='bvResult'>" + vitalValue + "</td> <td>ng/L</td> <td><div>1. &#8805 30 ng/ml - Vitamin D sufficiency</div><div>2. 21 ~ 29 ng/ml - Vitamin D insufficiency</div><div>< 20 ng/ml - Vitamin D deficiency</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (harmoneHtmlMap.has(viatlName)) {
				harmoneHtmlMap.delete(viatlName);
				harmoneHtmlMap.set(viatlName, html);
			} else {
				harmoneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "hCG".toLowerCase()) {
			var extractNum = vitalValue.match(/[\d\.]+/g)[0];
			//var statusImg = extractNum < 5 ? normalImg : extractNum == 5 && vitalValue.includes("Below") ? normalImg : extractNum > 25 ? veryAbnormalImg : extractNum == 25 && vitalValue.includes("Above") ? veryAbnormalImg : moderateImg;
			var statusImg = normalImg;
			if (extractNum < 5) {
				statusImg = normalImg;
				harmoneMarkerAbnormal2 = false;
			} else {
				if (extractNum == 5 && vitalValue.includes("Below")) {
					statusImg = normalImg;
					harmoneMarkerAbnormal2 = false;
				} else {
					if (extractNum > 25) {

						statusImg = veryAbnormalImg;
						harmoneMarkerAbnormal2 = true;
					} else {
						if (extractNum == 25 && vitalValue.includes("Above")) {
							statusImg = veryAbnormalImg;
							harmoneMarkerAbnormal2 = true;
						} else statusImg = moderateImg;harmoneMarkerAbnormal2 = false;
					}
				}
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>harmoneCount</td> <td>B hCG</td> <td>B Human Chronionic Gonadotropin</td> <td class='bvResult'>" + vitalValue + "</td> <td>mIU/ml</td> <td><div>1. Less than 5 - Negative</div><div>2. Between 5 - 25 - Observational zone</div><div>3. Above 25 - Positive</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (harmoneHtmlMap.has(viatlName)) {
				harmoneHtmlMap.delete(viatlName);
				harmoneHtmlMap.set(viatlName, html);
			} else {
				harmoneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "LH".toLowerCase()) {
			//var statusImg = vitalValue <= 5 ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue <= 5) {
				statusImg = normalImg;
				harmoneMarkerAbnormal3 = false;
			} else {
				statusImg = veryAbnormalImg;
				harmoneMarkerAbnormal3 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>harmoneCount</td> <td>LH</td> <td>Luteinizing hormone</td> <td class='bvResult'>" + vitalValue + "</td> <td>mIU/mL</td> <td><div>MEN : 1.7 - 8.6 mIU/mL</div><div>Women:</div><div>1. Follicular phase : 2.4 - 12.6 mIU/mL</div><div>2. Ovulation phase : 14.0 - 95.6 mIU/mL</div><div>3. Luteal phase : 1.0 - 11.4 mIU/mL</div><div>4. Postmenopausal : 7.7 - 58.5 mIU/mL</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (harmoneHtmlMap.has(viatlName)) {
				harmoneHtmlMap.delete(viatlName);
				harmoneHtmlMap.set(viatlName, html);
			} else {
				harmoneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "T4".toLowerCase()) {
			//var statusImg = vitalValue <= 5 ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue >=66 && vitalValue <= 181) {
				statusImg = normalImg;
				harmoneMarkerAbnormal4 = false;
			} else {
				statusImg = veryAbnormalImg;
				harmoneMarkerAbnormal4 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>harmoneCount</td> <td>T4</td> <td>Thyroxine</td> <td class='bvResult'>" + vitalValue + "</td> <td>nmol/L or ng/mL</td> <td><div>66-181 nmol/L - Normal</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (harmoneHtmlMap.has(viatlName)) {
				harmoneHtmlMap.delete(viatlName);
				harmoneHtmlMap.set(viatlName, html);
			} else {
				harmoneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "fT4".toLowerCase()) {
			//var statusImg = vitalValue <= 5 ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue >= 12 && vitalValue <= 22) {
				statusImg = normalImg;
				harmoneMarkerAbnormal5 = false;
			} else {
				statusImg = veryAbnormalImg;
				harmoneMarkerAbnormal5 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>harmoneCount</td> <td>fT4</td> <td>Free thyroxine</td> <td class='bvResult'>" + vitalValue + "</td> <td>pmol/L</td> <td><div>12-22 pmol/L - Normal</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (harmoneHtmlMap.has(viatlName)) {
				harmoneHtmlMap.delete(viatlName);
				harmoneHtmlMap.set(viatlName, html);
			} else {
				harmoneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "TSH WB".toLowerCase()) {
			//var statusImg = vitalValue >= 0.45 && vitalValue <= 4.5 ? normalImg : vitalValue > 4.5 ? veryAbnormalImg : abnormalImg;
			var statusImg = normalImg;
			if (vitalValue >= 0.45 && vitalValue <= 4.5) {
				statusImg = normalImg;
				harmoneMarkerAbnormal6 = false;
			} else {
				if (vitalValue > 4.5) {
					statusImg = veryAbnormalImg;
					harmoneMarkerAbnormal6 = true;
				} else statusImg = abnormalImg;harmoneMarkerAbnormal6 = false;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>harmoneCount</td> <td>TSH WB</td> <td>Thyroid Stimulating Hormone</td> <td class='bvResult'>" + vitalValue + "</td> harmoneUnit harmoneRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (harmoneHtmlMap.has(viatlName)) {
				harmoneHtmlMap.delete(viatlName);
				harmoneHtmlMap.set(viatlName, html);
			} else {
				harmoneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "TSH".toLowerCase()) {
			//var statusImg = vitalValue >= 0.45 && vitalValue <= 4.5 ? normalImg : vitalValue > 4.5 ? veryAbnormalImg : abnormalImg;
			var statusImg = normalImg;
			if (vitalValue >= 0.45 && vitalValue <= 4.5) {
				statusImg = normalImg;
				harmoneMarkerAbnormal7 = false;
			} else {
				if (vitalValue > 4.5) {
					statusImg = veryAbnormalImg;
					harmoneMarkerAbnormal7 = true;
				} else statusImg = abnormalImg;harmoneMarkerAbnormal7 = false;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>harmoneCount</td> <td>TSH</td> <td>Thyroid Stimulating Hormone</td> <td class='bvResult'>" + vitalValue + "</td> harmoneUnit harmoneRange  <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (harmoneHtmlMap.has(viatlName)) {
				harmoneHtmlMap.delete(viatlName);
				harmoneHtmlMap.set(viatlName, html);
			} else {
				harmoneHtmlMap.set(viatlName, html);
			}
		}

		//---------------- Infection Table Header & Body ----------------
		if (viatlName == "CRP".toLowerCase()) {
			var extractNum = vitalValue.match(/[\d\.]+/g)[0];
			//var statusImg = extractNum < 10 ? normalImg : extractNum < 10 && vitalValue.includes("Below") ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (extractNum < 10) {
				statusImg = normalImg;
				infectionMarkerAbnormal1 = false;
			} else {
				if (extractNum < 10 && vitalValue.includes("Below")) {
					statusImg = normalImg;
					infectionMarkerAbnormal1 = false;
				} else {
					statusImg = veryAbnormalImg;
					infectionMarkerAbnormal1 = true;
				}
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>infectionCount</td> <td>CRP</td> <td>C-Reactive protein</td> <td class='bvResult'>" + vitalValue + "</td> <td>mg/L</td> <td><div>&#8805 10mg/l - Positive</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
            //console.log("infectionHtmlMap---------------------",infectionHtmlMap)
			if (infectionHtmlMap.has(viatlName)) {

				infectionHtmlMap.delete(viatlName);
				infectionHtmlMap.set(viatlName, html);
			} else {
				infectionHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "PCT".toLowerCase()) {
			//var statusImg = vitalValue < 0.5 ? normalImg : vitalValue >= 0.5 && vitalValue < 2.0 ? moderateImg : vitalValue >= 2.0 && vitalValue < 10.0 ? abnormalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue < 0.5) {
				statusImg = normalImg;
				infectionMarkerAbnormal2 = false;
			} else {
				if (vitalValue >= 0.5 && vitalValue < 2.0) {
					statusImg = moderateImg;
					infectionMarkerAbnormal2 = false;
				} else {
					if (vitalValue >= 2.0 && vitalValue < 10.0) {
						statusImg = abnormalImg;
						infectionMarkerAbnormal2 = false;
					} else {
						statusImg = veryAbnormalImg;
						infectionMarkerAbnormal2 = true;
					}
				}
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>infectionCount</td> <td>PCT</td> <td>Procalcitonin</td> <td class='bvResult'>" + vitalValue + "</td> <td>ng/mL</td> <td><div>1. < 0.05 Healthy individual</div><div>2. < 0.5 Low risk or local bacterial infection</div><div>3. > 0.5 ~ 2.0 Moderate risk for progression to severe systemic infection (Sepsis)</div><div>4. > 2.0 ~ 10.0 High risk for progression to severe systemic infection (Severe Sepsis)</div><div>5. > 10.0 High likelihood of severe sepsis or septic shock</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (infectionHtmlMap.has(viatlName)) {
                //console.log("=========================>:",vitalValue)
				infectionHtmlMap.delete(viatlName);
				infectionHtmlMap.set(viatlName, html);
			} else {
				infectionHtmlMap.set(viatlName, html);
			}
		}

		//---------------- Diabetic Table Header & Body ----------------
		if (viatlName == "HBA1C".toLowerCase()) {
			//var statusImg = vitalValue <= 5.6 ? normalImg : vitalValue > 5.6 && vitalValue <= 6.49 ? moderateImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue <= 5.6) {
				statusImg = normalImg;
				diabeticMarkerAbnormal1 = false;
			} else {
				if (vitalValue >= 5.6 && vitalValue <= 6.49) {
					statusImg = moderateImg
					diabeticMarkerAbnormal1 = false;
				} else {
					statusImg = veryAbnormalImg;
					diabeticMarkerAbnormal1 = true;
				}
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>diabeticCount</td> <td>HbA1c</td> <td>Glycalated Haemoglobin</td> <td class='bvResult'>" + vitalValue + "</td> <td>%</td> <td><div>1. &#8804 5.6% - Normal</div><div>2. 5.7 ~ 6.4% - Pre-diabetes</div><div>3. &#8805 6.5% - Diabetes</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (diabeticHtmlMap.has(viatlName)) {
				diabeticHtmlMap.delete(viatlName);
				diabeticHtmlMap.set(viatlName, html);
			} else {
				diabeticHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "U_Albumin".toLowerCase()) {
			//var statusImg = vitalValue <= 20 ? normalImg : vitalValue > 20 && vitalValue <= 200 ? moderateImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue <= 20) {
				statusImg = normalImg;
				diabeticMarkerAbnormal2 = false;
			} else {
				if (vitalValue > 20 && vitalValue <= 200) {
					statusImg = abnormalImg;
					diabeticMarkerAbnormal2 = false;
				} else {
					statusImg = veryAbnormalImg;
					diabeticMarkerAbnormal2 = true;
				}
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>diabeticCount</td> <td>U-Albumin</td> <td>Helicobacter pylori Antigen</td> <td class='bvResult'>" + vitalValue + "</td> <td>mg/L</td> <td><div>1. < 20mg/L  Normal</div><div>2. 20 ~ 200mg/L  Microalbuminuria</div><div>3. > 200mg/L  Macroalbuminuria</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (diabeticHtmlMap.has(viatlName)) {
				diabeticHtmlMap.delete(viatlName);
				diabeticHtmlMap.set(viatlName, html);
			} else {
				diabeticHtmlMap.set(viatlName, html);
			}
		}

		//---------------- Respiratory Borne Table Header & Body ----------------
		if (viatlName == "COVID".toLowerCase()) {
			//var statusImg = vitalValue.includes("Positive") || vitalValue.includes("positive") ? veryAbnormalImg : normalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Positive")) {
				statusImg = veryAbnormalImg;
				respiratoryMarkerAbnormal1 = true;
			}else {
			statusImg = normalImg;
            respiratoryMarkerAbnormal1 = false;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>respiratoryCount</td> <td>COVID 19 Ag</td> <td>Corona Virus 2019</td> <td class='bvResult'>" + vitalValue + "</td> respiratoryUnit respiratoryRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (respiratoryHtmlMap.has(viatlName)) {
				respiratoryHtmlMap.delete(viatlName);
				respiratoryHtmlMap.set(viatlName, html);
			} else {
				respiratoryHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "RSV_Ag".toLowerCase()) {
			//var statusImg = vitalValue.includes("Positive") || vitalValue.includes("positive") ? veryAbnormalImg : normalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Positive") || vitalValue.includes("positive")) {
				statusImg = veryAbnormalImg;
				respiratoryMarkerAbnormal2 = true;
			} else {
			statusImg = normalImg;
            respiratoryMarkerAbnormal2 = false;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>respiratoryCount</td> <td>RSV Ag</td> <td>Respiratory Syncytial Virus</td> <td class='bvResult'>" + vitalValue + "</td> respiratoryUnit respiratoryRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td </tr>";
			if (respiratoryHtmlMap.has(viatlName)) {
				respiratoryHtmlMap.delete(viatlName);
				respiratoryHtmlMap.set(viatlName, html);
			} else {
				respiratoryHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "Legionella_Ag".toLowerCase()) {
			//var statusImg = vitalValue.includes("Positive") || vitalValue.includes("positive") ? veryAbnormalImg : normalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Positive") || vitalValue.includes("positive")) {
				statusImg = veryAbnormalImg;
				respiratoryMarkerAbnormal3 = true;
			}else {
			statusImg = normalImg;
            respiratoryMarkerAbnormal3 = false;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>respiratoryCount</td> <td>Legionella Ag</td> <td>Legionella</td> <td class='bvResult'>" + vitalValue + "</td> respiratoryUnit respiratoryRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (respiratoryHtmlMap.has(viatlName)) {
				respiratoryHtmlMap.delete(viatlName);
				respiratoryHtmlMap.set(viatlName, html);
			} else {
				respiratoryHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "S_pneumonia_Ag".toLowerCase()) {
			//var statusImg = vitalValue.includes("Positive") || vitalValue.includes("positive") ? veryAbnormalImg : normalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Positive") || vitalValue.includes("positive")) {
				statusImg = veryAbnormalImg;
				respiratoryMarkerAbnormal4 = true;
			}else {
			statusImg = normalImg;
            respiratoryMarkerAbnormal4 = false;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>respiratoryCount</td> <td>S.pneumonia Ag</td> <td>Streptococcus pneumoniae</td> <td class='bvResult'>" + vitalValue + "</td> respiratoryUnit respiratoryRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (respiratoryHtmlMap.has(viatlName)) {
				respiratoryHtmlMap.delete(viatlName);
				respiratoryHtmlMap.set(viatlName, html);
			} else {
				respiratoryHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "strep_a_ag".toLowerCase()) {
			//var statusImg = vitalValue.includes("Positive") || vitalValue.includes("positive") ? veryAbnormalImg : normalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Positive") || vitalValue.includes("positive")) {
				statusImg = veryAbnormalImg;
				respiratoryMarkerAbnormal5 = true;
			}else {
			statusImg = normalImg;
            respiratoryMarkerAbnormal5 = false;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>respiratoryCount</td> <td>Strep A Ag</td> <td>Streptococcus Group A</td> <td class='bvResult'>" + vitalValue + "</td> respiratoryUnit respiratoryRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (respiratoryHtmlMap.has(viatlName)) {
				respiratoryHtmlMap.delete(viatlName);
				respiratoryHtmlMap.set(viatlName, html);
			} else {
				respiratoryHtmlMap.set(viatlName, html);
			}
		}

		//---------------- Vector Borne Table Header & Body ----------------
		if (viatlName == "Dengue_NS1".toLowerCase()) {
			//var statusImg = vitalValue.includes("Negative") || vitalValue.includes("negative") ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Negative") || vitalValue.includes("negative")) {
				statusImg = normalImg;
				vectorBorneAbnormal1 = false;
			} else {
				statusImg = veryAbnormalImg;
				vectorBorneAbnormal1 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>vectorBorneCount</td> <td>Dengue NS1 Ag</td> <td>Dengue NS1</td> <td class='bvResult'>" + vitalValue + "</td> vectorBorneUnit vectorBorneRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (vectorBorneHtmlMap.has(viatlName)) {
				vectorBorneHtmlMap.delete(viatlName);
				vectorBorneHtmlMap.set(viatlName, html);
			} else {
				vectorBorneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "Dengue_IgM/IgG".toLowerCase()) {
			//var statusImg = vitalValue.includes("Negative") || vitalValue.includes("negative") ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Negative") || vitalValue.includes("negative")) {
				statusImg = normalImg;
				vectorBorneAbnormal2 = false;
			} else {
				statusImg = veryAbnormalImg;
				vectorBorneAbnormal2 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>vectorBorneCount</td> <td>Dengue IgM/IgG</td> <td>Dengue IgM/IgG</td> <td class='bvResult'>" + vitalValue + "</td> vectorBorneUnit vectorBorneRange  <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (vectorBorneHtmlMap.has(viatlName)) {
				vectorBorneHtmlMap.delete(viatlName);
				vectorBorneHtmlMap.set(viatlName, html);
			} else {
				vectorBorneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "CHIKUNGUNYA".toLowerCase()) {
			//var statusImg = vitalValue.includes("Negative") || vitalValue.includes("negative") ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Negative") || vitalValue.includes("negative")) {
				statusImg = normalImg;
				vectorBorneAbnormal3 = false;
			} else {
				statusImg = veryAbnormalImg;
				vectorBorneAbnormal3 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>vectorBorneCount</td> <td>Chikungunya IgM/IgG</td> <td>Chikungunya IgM/IgG</td> <td class='bvResult'>" + vitalValue + "</td> vectorBorneUnit vectorBorneRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (vectorBorneHtmlMap.has(viatlName)) {
				vectorBorneHtmlMap.delete(viatlName);
				vectorBorneHtmlMap.set(viatlName, html);
			} else {
				vectorBorneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "DENGUE".toLowerCase()) {
			//var statusImg = vitalValue.includes("Negative") || vitalValue.includes("negative") ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Negative") || vitalValue.includes("negative")) {
				statusImg = normalImg;
				vectorBorneAbnormal4 = false;
			} else {
				statusImg = veryAbnormalImg;
				vectorBorneAbnormal4 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>vectorBorneCount</td> <td>DENGUE</td> <td>Chikungunya IgM/IgG</td> <td class='bvResult'>" + vitalValue + "</td> vectorBorneUnit vectorBorneRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (vectorBorneHtmlMap.has(viatlName)) {
				vectorBorneHtmlMap.delete(viatlName);
				vectorBorneHtmlMap.set(viatlName, html);
			} else {
				vectorBorneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "DENGUE IgM".toLowerCase()) {
			//var statusImg = vitalValue.includes("Negative") || vitalValue.includes("negative") ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Negative") || vitalValue.includes("negative")) {
				statusImg = normalImg;
				vectorBorneAbnormal5 = false;
			} else {
				statusImg = veryAbnormalImg;
				vectorBorneAbnormal5 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>vectorBorneCount</td> <td>DENGUE IgM</td> <td>Chikungunya IgM/IgG</td> <td class='bvResult'>" + vitalValue + "</td> vectorBorneUnit vectorBorneRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (vectorBorneHtmlMap.has(viatlName)) {
				vectorBorneHtmlMap.delete(viatlName);
				vectorBorneHtmlMap.set(viatlName, html);
			} else {
				vectorBorneHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "DENGUE IgG".toLowerCase()) {
			//var statusImg = vitalValue.includes("Negative") || vitalValue.includes("negative") ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Negative") || vitalValue.includes("negative")) {
				statusImg = normalImg;
				vectorBorneAbnormal6 = false;
			} else {
				statusImg = veryAbnormalImg;
				vectorBorneAbnormal6 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>vectorBorneCount</td> <td>DENGUE IgG> <td>Chikungunya IgM/IgG</td> <td class='bvResult'>" + vitalValue + "</td> vectorBorneUnit vectorBorneRange <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (vectorBorneHtmlMap.has(viatlName)) {
				vectorBorneHtmlMap.delete(viatlName);
				vectorBorneHtmlMap.set(viatlName, html);
			} else {
				vectorBorneHtmlMap.set(viatlName, html);
			}
		}

		//---------------- Fecal Table Header & Body ----------------
		if (viatlName == "iFOB".toLowerCase()) {
			//var statusImg = vitalValue <= 5 ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue < 100) {
				statusImg = normalImg;
				feacalMarkerAbnormal1 = false;
			} else {
				statusImg = veryAbnormalImg;
				feacalMarkerAbnormal1 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>fecalCount</td> <td>iFOB</td> <td>Fecal occult blood</td> <td class='bvResult'>" + vitalValue + "</td> <td>ng/ml or ugHb/g feces</td> <td><div>< 100 ng/ml -  Negative or</div><div>< 20 ugHb/g feces - Negative</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (fecalHtmlMap.has(viatlName)) {
				fecalHtmlMap.delete(viatlName);
				fecalHtmlMap.set(viatlName, html);
			} else {
				fecalHtmlMap.set(viatlName, html);
			}
		}
		if (viatlName == "h_pylori".toLowerCase()) {
			//var statusImg = vitalValue.includes("Negative") || vitalValue.includes("negative") ? normalImg : veryAbnormalImg;
			var statusImg = normalImg;
			if (vitalValue.includes("Negative") || vitalValue.includes("negative")) {
				statusImg = normalImg;
				feacalMarkerAbnormal2 = false;
			} else {
				statusImg = veryAbnormalImg;
				feacalMarkerAbnormal2 = true;
			}
			var status = '<img class="bvLevelImg" src=' + statusImg + ' alt="User profile picture">';
			var html = "<tr> <td>fecalCount</td> <td>H. pylori</td> <td>Helicobacter pylori Antigen</td> <td class='bvResult'>" + vitalValue + "</td> <td>Positive or Negative with COI</td> <td><div>1. COI &#8805 1.0 - Positive</div><div>2. COI < 1.0 -  Negative</div></td> <td style='text-align: center; vertical-align: middle;background:white;'>" + status + "</td></tr>";
			if (fecalHtmlMap.has(viatlName)) {
				fecalHtmlMap.delete(viatlName);
				fecalHtmlMap.set(viatlName, html);
			} else {
				fecalHtmlMap.set(viatlName, html);
			}
		}

	});


	//---- Basic Vital html Insertion ----
	var index = 1;
	basicHtmlMap.forEach((value, key) => {
		value = value.replace('basicCount', index);
		basicVitals = basicVitals + value;
		index++;
	});
    //console.log("///////Basic Vital////", basicHtmlMap.size, basicAVitAbnormal1, basicAVitAbnormal2, basicAVitAbnormal3 , basicAVitAbnormal4 , basicAVitAbnormal5 , basicAVitAbnormal6 , basicAVitAbnormal7);
	if (basicHtmlMap.size > 0) {
		$("#basicHeader").html(bloodVitalHeader);
		$(".accBtnBasic").css({ "opacity": 1 });
		//console.log("-------------->", basicAVitAbnormal1);
		if (basicAVitAbnormal1 || basicAVitAbnormal2 || basicAVitAbnormal3 || basicAVitAbnormal4 || basicAVitAbnormal5 || basicAVitAbnormal6 || basicAVitAbnormal7) {
		//console.log("abnormal red@@@@@@@@@@@@@@@@")
		    $('#basicVitalAccordin').css({ "color": 'red' })
		}else {$('#basicVitalAccordin').css({ "color": 'blue' })}
	} else {
		$("#basicHeader").html("");
		$(".accBtnBasic").css({ "opacity": 0.2 });

	}
	$("#basicBody").html(basicVitals);


	//---- Cardiac html Insertion ----
	var index = 1;
	cardiacHtmlMap.forEach((value, key) => {
		value = value.replace('cardiacCount', index);
		cardiacVitals = cardiacVitals + value;
		index++;
	});
	if (cardiacHtmlMap.size > 0) {
		$("#cardiacHeader").html(bloodVitalHeader);
		$(".accBtnCardiac").css({ "opacity": 1 });
		if (cardiacMarkerAbnormal1 || cardiacMarkerAbnormal2 || cardiacMarkerAbnormal3 || cardiacMarkerAbnormal4 || cardiacMarkerAbnormal5) { $("#cardiacMarkerAccordin").css({ "color": 'red' }) }
	} else {
		$("#cardiacHeader").html("");
		$(".accBtnCardiac").css({ "opacity": 0.2 });
	}
	$("#cardiacBody").html(cardiacVitals);

	//---- Harmone html Insertion ----
	index = 1; var harmoneUnitRangeUpdated = false;
	var harmoneUnit = "<td rowspan='2' style='vertical-align: middle;'>mIU/ml</td>";
	var harmoneRange = "<td rowspan='2' style='vertical-align: middle;'><div>1. 0.45 to 4.5 mIU/L - Normal</div><div>2. Level below 0.45 mIU/L indicative of hyperthyroidism</div><div>3. Level above 4.5 mIU/L indicative of hypothyroidism</div></td>";
	var harmoneHtmlMapSize = harmoneHtmlMap.size;
	//console.log("harmoneHtmlMap size before: ", harmoneHtmlMap.size);
	harmoneHtmlMap.forEach((value, key) => {
		value = value.replace('harmoneCount', index);


		if (key == "tsh" || key == "tsh wb") {
			//console.log("Harmone check: ", key);
		} else {
			harmoneVitals = harmoneVitals + value;
			index++;
			harmoneHtmlMap.delete(key);
		}
	});
	//console.log("harmoneHtmlMap size after: ", harmoneHtmlMap.size);
	harmoneHtmlMap.forEach((value, key) => {
        //console.log("-------------->",value)
		value = value.replace('harmoneCount', index);

		// Insert Unit & Range common for all 2 vitals.
		if (harmoneUnitRangeUpdated == false && value.includes("harmoneUnit harmoneRange")) {
			value = value.replace('harmoneUnit', harmoneUnit);
			value = value.replace('harmoneRange', harmoneRange);
			harmoneUnitRangeUpdated = true;
		} else {
			value = value.replace('harmoneUnit', "");
			value = value.replace('harmoneRange', "");
		}
		harmoneVitals = harmoneVitals + value;
		index++;

	});
	if (harmoneHtmlMapSize > 0) {
		$("#harmoneHeader").html(bloodVitalHeader);
		$(".accBtnHarmone").css({ "opacity": 1 });
		if (harmoneMarkerAbnormal1 || harmoneMarkerAbnormal2 || harmoneMarkerAbnormal3 || harmoneMarkerAbnormal4 || harmoneMarkerAbnormal5 ||
            harmoneMarkerAbnormal6 || harmoneMarkerAbnormal7) {
                $('#harmoneMarkerAccordin').css({ "color": 'red' })
        }
	} else {
		$("#harmoneHeader").html("");
		$(".accBtnHarmone").css({ "opacity": 0.2 });
	}
	//console.log("harmoneVitals--------------------------->",harmoneVitals)
	$("#harmoneBody").html(harmoneVitals);

	//---- Infection html Insertion ----
	index = 1;
	infectionHtmlMap.forEach((value, key) => {
	//console.log("INFECTION VITALS inside foreach=======>",infectionHtmlMap)
		value = value.replace('infectionCount', index);
		infectionVitals = infectionVitals + value;
		//console.log("INFECTION VITALS=======>",index)
		index++;
	});
    //console.log("INFECTION VITALS=======>",infectionHtmlMap)
	if (infectionHtmlMap.size > 0) {
		$("#infectionHeader").html(bloodVitalHeader);
		$(".accBtnInfection").css({ "opacity": 1 });
		//console.log("----------------------infectionMarkerAbnormal--------",infectionMarkerAbnormal);
		if (infectionMarkerAbnormal1 || infectionMarkerAbnormal2) { $('#infectionMarkerAccordin').css({ "color": 'red' }) }

	} else {
        //console.log("-----------ELSE---------------->",infectionHtmlMap)
		$("#infectionHeader").html("");
		$(".accBtnInfection").css({ "opacity": 0.2 });

	}

		console.log("--------------------------->",infectionVitals)
	$("#infectionBody").html(infectionVitals);

	//---- Diabetic html Insertion ----
	index = 1;
	diabeticHtmlMap.forEach((value, key) => {
		value = value.replace('diabeticCount', index);
		diabeticVitals = diabeticVitals + value;
		index++;
	});
	if (diabeticHtmlMap.size > 0) {
		$("#diabeticHeader").html(bloodVitalHeader);
		$(".accBtnDiabetic").css({ "opacity": 1 });
		if (diabeticMarkerAbnormal1 | diabeticMarkerAbnormal2) { $('#diabeticMarkerAccordin').css({ "color": 'red' }) }
	} else {
		$("#diabeticHeader").html("");
		$(".accBtnDiabetic").css({ "opacity": 0.2 });
	}
	$("#diabeticBody").html(diabeticVitals);

	//---- Respiratory html Insertion ----
	index = 1; var respiratoryUnitRangeUpdated = false;
	var vectBorUnit = "<td rowspan='5' style='vertical-align: middle;'>Positive or Negative with COI</td>";
	var vectBorRange = "<td rowspan='5' style='vertical-align: middle;'><div>1. COI &#8805 1.0 - Positive</div><div>2. COI < 1.0 -  Negative</div></td>";
	respiratoryHtmlMap.forEach((value, key) => {
		value = value.replace('respiratoryCount', index);
		// Insert Unit & Range common for all 3 vitals.
		if (respiratoryUnitRangeUpdated == false) {
			value = value.replace('respiratoryUnit', vectBorUnit);
			value = value.replace('respiratoryRange', vectBorRange);
			respiratoryUnitRangeUpdated = true;
		} else {
			value = value.replace('respiratoryUnit', "");
			value = value.replace('respiratoryRange', "");
		}
		respiratoryVitals = respiratoryVitals + value;
		index++;
	});
	if (respiratoryHtmlMap.size > 0) {
		$("#respiratoryHeader").html(bloodVitalHeader);
		$(".accBtnRespiratory").css({ "opacity": 1 });
		if (respiratoryMarkerAbnormal1 || respiratoryMarkerAbnormal2 || respiratoryMarkerAbnormal3 || respiratoryMarkerAbnormal4 || respiratoryMarkerAbnormal5) {
            $('#respiratoryMarkerAccordin').css({ "color": 'red' })
        }
	} else {
		$("#respiratoryHeader").html("");
		$(".accBtnRespiratory").css({ "opacity": 0.2 });
	}
	$("#respiratoryBody").html(respiratoryVitals);

	//---- Vector html Insertion ----
	index = 1; var unitRangeUpdated = false;
	var vectBorUnit = "<td rowspan='6' style='vertical-align: middle;'>Positive or Negative with COI</td>";
	var vectBorRange = "<td rowspan='6' style='vertical-align: middle;'><div>1. COI &#8805 1.0 - Positive</div><div>2. COI < 1.0 -  Negative</div></td>";
	vectorBorneHtmlMap.forEach((value, key) => {
		value = value.replace('vectorBorneCount', index);
		// Insert Unit & Range common for all 3 vitals.
		if (unitRangeUpdated == false) {
			value = value.replace('vectorBorneUnit', vectBorUnit);
			value = value.replace('vectorBorneRange', vectBorRange);
			unitRangeUpdated = true;
		} else {
			value = value.replace('vectorBorneUnit', "");
			value = value.replace('vectorBorneRange', "");
		}
		vectorBorneVitals = vectorBorneVitals + value;
		index++;
	});
	if (vectorBorneHtmlMap.size > 0) {
		$("#vectorBorneHeader").html(bloodVitalHeader);
		$(".accBtnVector").css({ "opacity": 1 });
		if (vectorBorneAbnormal1 || vectorBorneAbnormal2 || vectorBorneAbnormal3 || vectorBorneAbnormal4 || vectorBorneAbnormal5 || vectorBorneAbnormal6) {
             $('#vectorBorneAccordin').css({ "color": 'red' })
        }
	} else {
		$("#vectorBorneHeader").html("");
		$(".accBtnVector").css({ "opacity": 0.2 });
	}
	$("#vectorBorneBody").html(vectorBorneVitals);

	//---- Fecal html Insertion ----
	index = 1;
	fecalHtmlMap.forEach((value, key) => {
		value = value.replace('fecalCount', index);
		fecalVitals = fecalVitals + value;
		index++;
	});
    // console.log("///////Fecal html////", fecalHtmlMap.size, feacalMarkerAbnormal1, feacalMarkerAbnormal2);
	if (fecalHtmlMap.size > 0) {
		$("#fecalHeader").html(bloodVitalHeader);
		$(".accBtnFecal").css({ "opacity": 1 });
		if (feacalMarkerAbnormal1 || feacalMarkerAbnormal2) { $('#faecalMarkerAccordin').css({ "color": 'red' }) }
	} else {
        $("#fecalHeader").html("");
		$(".accBtnFecal").css({ "opacity": 0.2 });
	}
	$("#fecalBody").html(fecalVitals);
}




//-------------------- When Document is Ready ---------------------------
$(document).ready(
	function () {
		$('#toolTipDb1').tooltip();
		$('#toolTipDb2').tooltip();
		var bloodVitalHeader = "<tr> <th>#</th> <th>PARAMETER</th> <th>FULL FORM</th> <th>RESULT</th> <th>UNIT</th> <th>RANGE</th> <th>LEVEL</th></tr>";
	}
);